import Component from "./components/Component.react";

import {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Modal,
    ButtonGroup,
    ActionGroup,
    Panel,
    Toolbar
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Modal,
    ButtonGroup,
    ActionGroup,
    Panel,
    Toolbar
};

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


