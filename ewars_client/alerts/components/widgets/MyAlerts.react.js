import Moment from "moment";

import {
    Spinner,
    Button
} from "../../../common";

var SitItem = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onClick: function () {
        var uri = "/alerts#/alert/" + this.props.data.alert.uuid;
        document.location = uri;
    },

    render: function () {
        var alarmName = ewars.formatters.I18N_FORMATTER(this.props.data.alert.alarm_name);
        var locationName = ewars.formatters.I18N_FORMATTER(this.props.data.alert.location_name);
        var last_trigger = ewars.formatters.DATE_FORMATTER(Moment().toDate());

        return (
            <div className="item situation" onClick={this._onClick}>
                <div className="sitName">{alarmName}</div>
                <div className="lastTrigger">{last_trigger}</div>
                <div className="sitLoc">{locationName}</div>
            </div>
        )
    }
});

var MyAlerts = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            data: [],
            filter: "ACTIVE"
        }
    },

    componentWillMount: function () {
        this._init();
    },

    _init: function () {
        ewars.tx("com.ewars.user.alerts", [this.state.filter])
            .then(function (resp) {
                this.state.data = resp;
                this._hasLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _swapFilter: function (data) {
        this.setState({
            filter: data.filter
        });
        this._init();
    },

    render: function () {

        var items = _.map(this.state.data, function (item) {
            return <SitItem data={item}/>
        }, this);

        var view;
        if (this.state.data.length <= 0 && this._hasLoaded) {
            view = <p className="placeholder">You currently do not have any alerts</p>;
        } else if (this.state.data.length > 0 && this._hasLoaded) {
            view = items;
        } else if (!this._hasLoaded) {
            view = <Spinner/>;
        }

        return (
            <div className="widget">
                <div className="widget-header">
                    <span>My Alerts</span>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-bell"
                            label="Active"
                            active={this.state.filter == "ACTIVE"}
                            onClick={this._swapFilter}
                            data={{filter: "NOT OK"}}/>
                        <ewars.d.Button
                            icon="fa-bell-slash"
                            label="Inactive"
                            active={this.state.filter == "INACTIVE"}
                            onClick={this._swapFilter}
                            data={{filter: "OK"}}/>
                    </div>
                </div>
                <div className="body no-pad">
                    <div className="situation-list" style={{maxHeight: 400, overflowY: "auto"}}>
                        {view}
                        <div className="filler"></div>
                    </div>

                </div>
            </div>
        )
    }
});

export default MyAlerts;
