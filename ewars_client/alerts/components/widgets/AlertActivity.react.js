import Moment from "moment";

import { Spinner } from "../../../common";

var ICONS = {
    "USER_ADD": ["fa fa-user-plus", "amber"],
    "USER_ADDED": ["fa fa-user-plus", "amber"],
    "TRIPPED": ["fa fa-bell", "red"],
    "RECOVERED": ['fa fa-bell-slash', 'green'],
    "CREATED": ["fa fa-bell", "red"],
    "SUPPRESSION": ["fa fa-bell-slash", "green"]
};

var HistItem = React.createClass({
    render: function () {
        var content = ewars.formatters.I18N_FORMATTER(this.props.data.content);
        var time = Moment(this.props.data.history_date).format("LL");

        var icon = "fa fa-bullhorn";
        icon = ICONS[this.props.data.event_type][0];
        var style = {backgroundColor: "#424142"};
        style.backgroundColor = ICONS[this.props.data.event_type][1];

        var locationName = ewars.formatters.I18N_FORMATTER(this.props.alert.location_name);
        var alarmName = ewars.formatters.I18N_FORMATTER(this.props.alert.alarm_name);

        if (Moment(this.props.data.history_date).isSame(Moment(), 'day')) {
            time = Moment(this.props.data.history_date).fromNow();
        }

        return (
            <tr>
                <td width="38px">
                    <div className="sit-icon" style={style}><i className={icon}></i></div>
                </td>
                <td>
                    <div className="sit-date">{alarmName} - {locationName} - {time}</div>
                    <div className="sit-stream-info" dangerouslySetInnerHTML={{__html: content}}></div>
                </td>
            </tr>
        )
    }
});

var AlertActivity = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            data: [],
            skip: 0,
            alerts: {},
            loadingItems: true
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.user.alert.activity", [this.state.skip])
            .then(function (resp) {
                this.state.data = resp.data;
                _.each(resp.alerts, function (alert) {
                    if (!this.state.alerts[alert.uuid]) {
                        this.state.alerts[alert.uuid] = alert;
                    }
                }, this);
                this.state.loadingItems = false;
                this._hasLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _loadMore: function () {
        this.state.loadingItems = true;
        this.state.skip += 10;
        if (this.isMounted()) this.forceUpdate();

        ewars.tx("com.ewars.user.alert.activity", [this.state.skip])
            .then(function (resp) {
                this.state.data.push.apply(this.state.data, resp.data);
                _.each(resp.alerts, function (alert) {
                    if (!this.state.alerts[alert.uuid]) {
                        this.state.alerts[alert.uuid] = alert;
                    }
                }, this);
                this.state.loadingItems = false;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    render: function () {

        var items = _.map(this.state.data, function (item) {
            var alert = this.state.alerts[item.uuid];
            return <HistItem data={item} alert={alert}/>
        }, this);

        var loadMore = <div className="load-more" onClick={this._loadMore}>Load More</div>;
        if (this.state.loadingItems) {
            loadMore =
                <div className="load-more" onClick={this._loadMore}><i className="fal fa-spin fa-spinner"></i>&nbsp;Load
                    More</div>;
        }

        var view;

        if (this.state.data.length <= 0 && this._hasLoaded) {
            view = <p className="placeholder">You currently do not belong to any alerts</p>;
        } else if (this.state.data.length > 0 && this._hasLoaded) {
            view = <table width="100%">{items}</table>
        } else if (!this._hasLoaded) {
            view = <Spinner/>
        }

        return (
            <div className="widget">
                <div className="widget-header"><span>Alert Activity Feed</span></div>
                <div className="body no-pad">
                    <div className="sit-feed">
                        <div className="sit-stream">
                            {view}
                            {loadMore}
                            <div className="clearer"></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default AlertActivity;
