import { DataTable } from "../../../common";
import Alert from "../../../alert/components/Alert.react";

var RISK_NAMES = {
    LOW: "Low Risk",
    MODERATE: "Moderate Risk",
    HIGH: "High Risk",
    SEVERE: "Very High Risk",
    OUTCOME: "Outcome",
    RISK_CHAR: "Risk Characterisation",
    RISK_ASSESS: "Risk Assessment",
    VERIFICATION: "Verification",
    AUTODISCARDED: "Auto-Discarded",
    PENDING: "Pending",
    COMPLETED: "Completed",
    ACTIVE: "In Progress",
    DISCARD: "Discarded",
    RESPOND: "Respond",
    RESPONSE: "Respond",
    MONITOR: "Monitor",
    DISCARDED: "Discarded"
};

let COLUMNS = [
    {
        name: "alarm_name",
        fmt: ewars.I18N,
        config: {
            type: "select",
            label: "Alarm",
            filterKey: "alarm_id",
            optionsSource: {
                resource: "alarm",
                valSource: "uuid",
                labelSource: "name",
                query: {}
            }
        }
    },
    {
        name: "eid",
        config: {
            type: "text",
            label: "EID"
        }
    },
    {
        name: "trigger_end",
        config: {
            type: "date",
            label: "Period of Alert"
        },
        fmt: (value, row) => {
            return ewars.DATE(value, row.ds_interval);
        }
    },
    {
        name: "created",
        fmt: function (val) {
            return ewars.DATE(val, "DAY")
        },
        config: {
            label: "Triggered On",
            type: "date"
        }
    },
    {
        name: "location_full_name",
        config: {
            label: "Location",
            type: "location",
            filterKey: "location_id",
            sortKey: "location_name"
        },
        fmt: function (value) {
            return value.reverse().join(", ")
        }
    },
    {
        name: "location_name",
        config: {
            label: "Location (Exact)",
            type: "location",
            filterKey: "location_id",
            sortKey: "location_name"
        },
        fmt: ewars.I18N
    },
    {
        name: "st_name",
        fmt: ewars.I18N,
        config: {
            label: "Location Type",
            filterKey: "site_type_id",
            type: "select",
            optionsSource: {
                resource: "location_type",
                valSource: "id",
                labelSource: "name",
                query: {}
            }
        }
    },
    {
        name: "state",
        config: {
            label: "State",
            type: "select",
            options: [
                ["OPEN", "Open"],
                ["CLOSED", "Closed"],
                ["DELETED", "Deleted"],
                ["AUTODISCARDED", "Auto-Discarded"]
            ]
        }
    },
    {
        name: "outcome",
        config: {
            label: "Outcome",
            type: "select",
            options: [
                ["DISCARDED", "Discard"],
                ["MONITOR", "Monitor"],
                ["RESPONSE", "Response"],
                ["RESPOND", "Response"],
                ["AUTODISCARDED", "Auto-Discarded"],
                ["", "No Outcome"]
            ]
        }
    },
    {
        name: "stage",
        config: {
            label: "Stage",
            type: "select",
            options: [
                ["VERIFICATION", "Verification"],
                ["OUTCOME", "Outcome"],
                ["RISK_CHAR", "Risk Characterization"],
                ["RISK_ASSESS", "Risk Assessment"]
            ]
        }
    },
    {
        name: "stage_state",
        config: {
            label: "Stage State",
            type: "select",
            options: [
                ["PENDING", "Pending"],
                ["COMPLETED", "Completed"],
                ["DISCARD", "Discarded"],
                ["ACTIVE", "In Progress"],
                ["MONITOR", "Monitor"]
            ]
        }
    },
    {
        name: "risk",
        config: {
            label: "Risk",
            type: "select",
            options: [
                ["SEVERE", "Very High Risk"],
                ["HIGH", "High Risk"],
                ["MODERATE", "Moderate Risk"],
                ["LOW", "Low Risk"]
            ]
        }
    }

];


const actions = [
    {label: "View", action: "VIEW", icon: "fa-eye"}
    //{label: "Re-Evaluate", action: "EVALUATE", icon: "fa-refresh"},
    //{label: "Close", action: "CLOSE", icon: "fa-close"},
    //{label: "Delete", action: "DELETE", icon: "fa-trash"}
];

var FILTER = {};


class Data extends React.Component {
    static defaultProps = {
        closed: false
    };

    constructor(props) {
        super(props);

        this.state = {
            active_alert: null
        }
    }

    _onCellAction = (action, cell, row) => {
        switch (action) {
            case "VIEW":
            case "CLICK":
                this.setState({
                    active_alert: row.uuid
                });
                // window.open("/alert#?uuid=" + row.uuid);
                break;
            case "REVAL":
                this._evaluateAlert(row.uuid);
                break;
            default:
                break;
        }
    };

    _evaluateAlert = (alertUUID) => {
        ewars.prompt('fa-sync', 'Re-evaluate alert?', 'Are you sure you want to re-evlauate this alert? If it does not meet the alarm criteria or the original triggering report can not be found this alert will automatically be deleted.', () => {
            let blocker = new ewars.Blocker(null, "Revaling alert...");
            ewars.tx('com.ewars.alert.reevaluate', [alertUUID])
                .then((resp) => {
                    blocker.destroy();
                    if (resp.keep == true) {
                        ewars.growl("Alert re-evaluated, alert kept intact.");
                    } else {
                        ewars.growl("Alert re-evaluate, deleted from system.")
                    }
                    ewars.emit("RELOAD_DT");
                })
                .catch(err => {
                    blocker.destroy();
                    ewars.growl("Error re-evaluating alert.");
                    ewars.emit("RELOAD_DT");
                })
        })
    };

    _onShadeAction = (action) => {
        if (action == "CLOSE") {
            this.setState({
                active_alert: null
            });
            ewars.emit("RELOAD_DT");
        }
    };

    render() {

        let columns = COLUMNS.slice();

        if (window.user.user_type == "REGIONAL_ADMIN") {
            FILTER.lineage = {
                under: window.user.location_id
            }
        }

        let gridId = "ALERTS_OPEN";
        // Check if we're showing open or closed alerts lists
        if (this.props.closed) {
            FILTER.state = this.props.closed ? {in: ["CLOSED", "AUTODISCARDED"]} : {eq: "OPEN"};
            gridId = "ALERTS_CLOSED";
        }
        FILTER.state = this.props.closed ? {in: ["CLOSED", "AUTODISCARDED"]} : {eq: "OPEN"};

        let baseOrder = {
            "trigger_end": "DESC"
        };

        let lActions = ewars.copy(actions);
        if (!this.props.closed && window.user.role == 'ACCOUNT_ADMIN') {
            lActions.push({label: "Re-evaluate alert", action: "REVAL", icon: "fa-sync"});
        }

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <DataTable
                            resource="alerts_full"
                            filter={FILTER}
                            id={gridId}
                            initialOrder={{created: "DESC"}}
                            onCellAction={this._onCellAction}
                            columns={columns}
                            actions={lActions}
                        />
                    </div>
                </div>
                <ewars.d.Shade
                    toolbar={false}
                    onAction={this._onShadeAction}
                    shown={this.state.active_alert}>
                    <Alert uuid={this.state.active_alert}/>
                </ewars.d.Shade>
            </div>
        )
    }
}

export default Data;
