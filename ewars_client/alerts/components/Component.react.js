import {
    Modal
} from "../../common";

import Dashboard from "./views/Dashboard.react";

var Component = React.createClass({
    getInitialState: function () {
        return {
            view: "dashboard",
            filter: null,
            data: {},
            showModal: false
        }
    },

    _onViewChange: function () {

    },

    _showDashboard: function () {
        document.location = "/alerts#";
        this.setState({
            view: "dashboard",
            alert: null
        })
    },

    _showAlerts: function () {
        this.setState({
            view: "alerts"
        })
    },

    _showAlert: function (alert) {
        document.location = "http://" + ewars.domain + "/alert#?uuid=" + alert.uuid;
    },

    _viewReport: function (report) {
        this.setState({
            showModal: true,
            report: report
        })
    },

    hideModal: function () {
        this.setState({
            showModal: false,
            report: null
        })
    },

    render: function () {
        var view, reportView;

        if (this.state.view == "dashboard") view = <Dashboard/>;
        if (this.state.report) reportView = <ReportView data={this.state.report}/>;

        var modalButtons = [
            {label: "Close", onClick: this.hideModal}
        ];

        return (
            <div className="ide">
                {view}
            </div>
        )
    }
});

export default Component;
