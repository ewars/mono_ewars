module.exports = {
    source_spec: {
        type: "header",
        label: {
            en: "Source Specification"
        }
    },
    dataType: {
        type: "select",
        label: {
            en: "Data Source"
        },
        options: [
            ["SERIES", "Default"],
            ["COMPLEX", "Complex"]
        ]
    },
    interval: {
        type: "select",
        label: {
            en: "Target Interval"
        },
        options: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Year"]
        ]
    },
    indicator_header: {
        type: "header",
        label: {
            en: "Indicator Source Data"
        }
    },
    indicator: {
        type: "indicator",
        label: {
            en: "Indicator Source"
        }
    },
    location_spec_header: {
        type: "header",
        label: {
            en: "Location Spec"
        }
    },
    loc_spec: {
        type: "select",
        label: {
            en: "Location Spec."
        },
        options: [
            ["MANUAL", "Specific Locations"],
            ["GENERATOR", "Location Type"]
        ]
    },
    locations: {
        type: "location",
        label: {
            en: "Locations"
        },
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "MANUAL"]
            ]
        }
    },
    generator_locations_parent: {
        type: "location",
        label: {
            en: "Country"
        },
        conditions: [
            ["loc_spec", "eq", "GENERATOR"]
        ]
    },
    generator_locations_type: {
        type: "select",
        label: {
            en: "Target Location Type"
        },
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            query: {},
            labelSource: "name"
        },
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_location_status: {
        type: "select",
        label: {
            en: "Target Location Status"
        },
        options: [
            ["ACTIVE", "Active"],
            ["INACTIVE", "Inactive"]
        ],
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    source_date: {
        type: "header",
        label: {
            en: "Date Range"
        }
    },
    start_date: {
        type: "date",
        label: {
            en: "Start Date"
        }
    },
    end_date: {
        type: "date",
        label: {
            en: "End Date"
        }
    },
    style_header: {
        type: "header",
        label: {
            en: "Styling"
        }
    },
    type: {
        type: "select",
        label: {
            en: "Display Mode"
        },
        options: [
            ["DEFAULT", "Default"],
            ["HEATMAP", "Heatmap"],
            ["MOVEMENT", "Movement"]
        ]
    },
    scale: {
        type: "select",
        label: {
            en: "Scale"
        },
        options: [
            ["DEFAULT", "Default"],
            ["RED_BLUE", "Red -> Blue"],
            ["BLUE_RED", "Blue -> Red"]
        ]
    },
    opacity: {
        type: "number",
        label: {
            en: "Layer Opacity"
        }
    },
    adv_header: {
        type: "header",
        label: {en: "Advanced"}
    },
    reduction: {
        type: "select",
        label: {
            en: "Reduction"
        },
        options: [
            ["SUM", "Sum"],
            ["AVG", "Average"],
            ["STD_DEV", "Standard Deviation"],
            ["QUARTILE", "Quartile"]
        ]
    }


};