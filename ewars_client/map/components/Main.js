import DateUtils from "../../documents/utils/DateUtils";
import chroma from "chroma-js";
import Moment from "moment";

import MapBrowser from "./MapBrowser";

import Map from "../../common/analysis/Map";

const STYLES = {
    new: {
        position: "absolute",
        top: "50%",
        marginTop: "-200px",
        textAlign: "center",
        width: "100%"
    },
    newIcon: {
        fontSize: "60px",
        color: "#CCC",
        marginBottom: "20px"
    }
};
class NewCreator extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="map-new" style={STYLES.new}>
                <div className="icon" style={STYLES.newIcon}><i className="fal fa-map"></i></div>
                <div className="text" style={{marginBottom: 20}}>"Open" to view an existing map or click "Create" below
                    to create your own.
                </div>
                <div className="button">
                    <ewars.d.Button
                        icon="fa-plus"
                        onClick={this.props.onClick}
                        label="Create Map"/>
                </div>
            </div>
        )
    }
}

class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }
    }

    _browse = () => {
        this.setState({show: true})
    };

    _onModalAction = (action) => {
        this.setState({show: false});
    };

    _create = () => {
        ewars.z.dispatch("MAP", "SET_MAP", {
            data: {
                uuid: null,
                name: "",
                definition: {
                    indicator: null,
                    source_type: "SLICE",
                    location: null,
                    site_type_id: null,
                    start_date_spec: "MANUAL",
                    end_date_spec: "MANUAL",
                    loc_spec: "GENERATOR",
                    start_date: Moment.utc().format("YYYY-MM-DD"),
                    end_date: Moment.utc().format("YYYY-MM-DD"),
                    reduction: "SUM",
                    scales: []
                },
                created_by: window.user.id,
                shared: false,
                description: null
            }
        })
    };

    _onSelect = (data) => {
        this.setState({show: false});
        ewars.z.dispatch("MAP", "SET_MAP", {
            data: data
        })
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Mapping" icon="fa-map">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-folder-open"
                            onClick={this._browse}
                            label="Open"/>
                        <ewars.d.Button
                            icon="fa-plus"
                            label="Create New"
                            onClick={this._create}/>

                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <NewCreator
                            onClick={this._create}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    title="Map Browser"
                    icon="fa-map"
                   shown={this.state.show}
                    onAction={this._onModalAction}>
                    <MapBrowser onSelect={this._onSelect}/>
                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default Main;
