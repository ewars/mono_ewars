class Map extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {

        if (this.props.data.created_by != window.user.id) {
            return (
                <div className="block hoverable" onClick={this._onClick}>
                    <div className="block-content">
                        <div className="ide-row">
                            <div className="ide-col">{this.props.data.name}</div>
                            <div className="ide-col" style={{textAlign: "right"}}>
                                {this.props.data.user_name} [{this.props.data.user_email}]
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="block hoverable" onClick={this._onClick}>
                    <div className="block-content">{this.props.data.name}</div>
                </div>
            )
        }
    }
}

class MapBrowser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this._query("USER");
    };

    _query = (type) => {
        let com = "com.ewars.maps";
        if (type == "SHARED") com = "com.ewars.maps.shared";

        ewars.tx(com)
            .then((resp) => {
                this.setState({
                    data: resp,
                    view: type
                })
            })
    };

    showShared = () => {
        this._query("SHARED");
    };

    showUsers = () => {
        this._query("USER");
    };

    _onSelect = (data) => {
        this.props.onSelect(data);
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width={200}>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                <div className="block hoverable" onClick={this.showUsers}>
                                    <div className="block-content">Saved Map(s)</div>
                                </div>
                                <div className="block hoverable" onClick={this.showShared}>
                                    <div className="block-content">Shared</div>
                                </div>
                            </div>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                {this.state.data.map(function (item) {
                                    return <Map data={item} onClick={this._onSelect}/>
                                }.bind(this))}
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default MapBrowser;