import ComplexManager from "../../../analysis/components/editor/data/views/SlideComplexManager.react";

var ComplexEditor = React.createClass({
    getInitialState: function () {
        return {
            series: []
        }
    },

    render: function () {
        return <ComplexManager data={this.state}/>;
    }
});

export default ComplexEditor;
