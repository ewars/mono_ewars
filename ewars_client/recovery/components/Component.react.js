import Validator from "../../common/utils/Validator";

import { Form, Button } from "../../common";

var ERRORS = [];
ERRORS[0] = "Please provide a valid email.";
ERRORS[1] = "Email provided is not a valid user account.";
ERRORS[2] = "The email provided is for an account which is not approved, please await approval.";
ERRORS[3] = "The email provided belongs to an inactive account, please contact support@ewars.ws for more information.";
ERRORS[4] = "The token provided is invalid, please provide a valid token or request a new token.";
ERRORS[5] = "Please provide a valid new password.";
ERRORS[6] = "New passwords do not match.";
ERRORS[7] = "The requested password has been used in the past, please provide a new password.";

var form = {
    email: {
        type: "email",
        label: {
            en: "Email Address"
        },
        required: true
    }
};

var tokenForm = {
    token: {
        type: "text",
        label: {
            en: "Recovery Token"
        },
        required: true
    },
    password: {
        type: "password",
        label: {
            en: "New Password"
        },
        required: true
    },
    confirm_pass: {
        type: "password",
        label: {
            en: "Confirm New Password"
        },
        required: true
    }
};

var Component = React.createClass({
    getInitialState: function () {
        return {
            data: {},
            errors: [],
            view: "DEFAULT"
        }
    },

    _handleSubmit: function () {
        var errors = Validator(form, this.state.data);

        if (errors) {
            this.state.errors = errors;
            this.forceUpdate();
            return;
        }

        fetch("/authentication/recovery", {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
            },
            redirect: "follow",
            referrer: "no-referrer",
            body: JSON.stringify({email: this.state.data.email})
        }).then(res => {
            let data = res.json();
            this.state.error = [];
            if (data.failed) {
                this.setState({
                    error: [],
                    code: data.code
                });
            } else {
                this.setState({
                    view: "TOKEN"
                });
            }
        }).catch(err => {
            ewars.growl("There was an error requesting a password reset");
        });

    },

    _handleChange: function (data, prop, value) {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        });
    },

    returnLogin: function () {
        document.location = "/login";
    },

    render: function () {
        var error;
        if (this.state.code) {
            error = <div className="error">
                <i className="fal fa-warning"></i>&nbsp;
                {ERRORS[this.state.code]}
            </div>
        }



        if (this.state.view == "TOKEN") {
            return (
                <div className="recovery-view">
                    <div className="grid">

                        <div className="widget">
                            <div className="widget-header"><span>Password Recovery</span></div>
                            <div className="body">

                                <p>An email has been sent to the address you provided with details on how to reset your password.</p>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        if (this.state.view == "DEFAULT") {
            return (
                <div className="recovery-view">
                    <div className="grid">
                        <div className="widget">
                            <div className="widget-header"><span>Password Recovery</span></div>
                            <div className="body">
                                <p>Please enter the details below and click submit and a password recovery email will be
                                    emailed to your email address on account</p>
                                <br />
                                {error}
                                <Form
                                    updateAction={this._handleChange}
                                    data={this.state.data}
                                    definition={form}
                                    readOnly={false}/>

                                <div className="btn-group">
                                    <Button
                                        colour="green"
                                        label="Request Password"
                                        onClick={this._handleSubmit}/>
                                    <Button
                                        label="Login"
                                        onClick={this.returnLogin}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
});

export default Component;
