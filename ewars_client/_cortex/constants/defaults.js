const DEFAULTS = {
    id: null,
    name: "",
    logo: null,
    icon_flag: null,
    default_language: "en",
    status: "INACTIVE",
    domain: "",
    created_by: window.user.id,
    location_id: null,
    flag_code: "",
    description: "",
    account_logo: "",
    account_flag: ""
};

export default DEFAULTS;