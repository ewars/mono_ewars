import DataTable from "../../common/DataTable/DataTable.react";

const COLUMNS = [
    {
        name: "name",
        config: {
            label: "Account Name"
        }
    },
    {
        name: "domain",
        config: {
            label: "Account Domain"
        }
    },
    {
        name: "status",
        config: {
            label: "Status"
        }
    },
    {
        name: "created",
        config: {
            label: "Created"
        }
    },
    {
        name: "last_modified",
        config: {
            label: "Last Modified"
        }
    },
    {
        name: "uuid",
        config: {
            label: "UUID"
        }
    },
    {
        name: "tki",
        config: {
            label: "TKI"
        }
    }
];

const ACTIONS = [
    {label: "Edit", action: "EDIT", icon: "fa-pencil"},
    {label: "Delete", action: "DELETE", icon: "fa-trash"}
];


class AccountList extends React.Component {
    constructor(props) {
        super(props)
    }

    _onAction = (action, cell, row) => {
    };

    render() {
        return (
            <DataTable
                resource="account"
                onCellAction={this._onAction}
                actions={ACTIONS}
                columns={COLUMNS}/>
        )
    }
}

export default AccountList;