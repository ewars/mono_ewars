import COLS_RELEASES from "../../tables/releases";
import DataTable from "../../../common/DataTable/DataTable.react";

const JOINS = [
    "c_project:pid:uuid:uuid,name"
]

class ReleaseManager extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            release: null
        }
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Releases">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            resource="c_release"
                            columns={COLS_RELEASES}
                            joins={JOINS}
                            paused={this.state.shown}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ReleaseManager;