import FORM_PROJECT from "../../forms/project";
import COLS_PROJECTS from "../../tables/project";
import DataTable from "../../../common/DataTable/DataTable.react";

class ProjectManager extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            project: null
        }
    }

    _new = () => {
        this.setState({
            project: {},
            shown: true
        })
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Projects">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            onClick={this._new}
                            icon="fa-plus"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            resource="c_project"
                            id="PROJECTS"
                            paused={this.state.shown}
                            columns={COLS_PROJECTS}
                            onCellAction={this._onAction}/>

                    </ewars.d.Cell>
                </ewars.d.Row>
                {this.state.project ?
                    <ewars.d.Shade
                        shown={this.state.shown}>
                        <ewars.d.SystemForm
                            definition={FORM_PROJECT}
                            data={this.state.project}
                            enabled={true}/>
                    </ewars.d.Shade>
                    : null}
            </ewars.d.Layout>
        )
    }
}

export default ProjectManager;