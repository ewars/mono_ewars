const COLS_ISSUES = [
    {
        name: "title",
        config: {
            label: "Title",
            type: "text"
        }
    },
    {
        name: "priority",
        config: {
            label: "Priority",
            type: "select",
            options: [
                ["0", "Low"],
                ["1", "Moderate"],
                ["2", "High"],
                ["3", "Severe"],
                ["999", "HOLY %$#@"]
            ]
        }
    },
    {
        name: "tags",
        config: {
            label: "Tags",
            type: "text"
        }
    },
    {
        name: "type",
        config: {
            label: "Issue Type",
            type: "select",
            options: [
                ["BUG", "Bug"],
                ["FEATURE", "Feature Request"],
                ["TASK", "Task"],
                ["SUPPORT", "Support"],
                ["QUESTION", "Question"]
            ]
        }
    },
    {
        name: "project_name",
        config: {
            label: "Project",
            type: "select",
            options: []
        },
        filterKey: "project_id"
    },
    {
        name: "user_name",
        config: {
            label: "Created by",
            type: "user",
            filterKey: "created_by"
        }
    }
]

export default COLS_ISSUES;