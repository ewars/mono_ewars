const COLS_RELEASES = [
    {
        name: "project.name",
        config: {
            label: "Project",
            type: "select",
            options: [],
        },
        filterKey: "pid"
    },
    {
        name: "version_no",
        config: {
            label: "Version",
            type: "text"
        }
    },
    {
        name: "cur",
        config: {
            label: "Current",
            type: "select",
            options: [
                [false, "No"],
                [true, "Yes"]
            ]
        }
    },
    {
        name: "created",
        config: {
            label: "Created",
            type: "date"
        }
    },
    {
        name: "Released",
        config: {
            label: "Released",
            type: "date"
        }
    }
]

export default COLS_RELEASES;