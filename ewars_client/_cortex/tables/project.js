const COLS_PROJECTS = [
    {
        name: "name",
        config: {
            label: "Name",
            type: "text"
        }
    },
    {
        name: "status",
        config: {
            label: "Status",
            type: "text"
        }
    },
    {
        name: "created",
        config: {
            label: "Created",
            type: "date"
        }
    }
]

export default COLS_PROJECTS;