import {
    Form,
    Button,
    Switch,
    ButtonGroup,
    Shade
} from "../../common";

import WidgetEditor from "../../common/editors/widgets/c.widget_editor";

const EDIT_MODES = {
    options: [
        ["EDIT", "Edit"],
        ["VIEW", "View"]
    ]
};


import Notebook from "./Notebook";


const FORM = {
    name: {
        type: "text",
        label: "Name"
    },
    description: {
        type: "textarea",
        label: "Description"
    },
    shared: {
        type: "switch",
        label: "Shared?"
    }
};

const STYLES = {
    DROPPER: {
        display: "none",
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        textAlign: "center",
        background: "rgba(0,0,0,0.5)",
        zIndex: 9
    },
    CHART: {
        textAlign: "center",
        cursor: "pointer",
        background: "#F2F2F2",
        position: "relative",
        display: "block",
        margin: 10
    }
};

const DEFAULTS = {
    PIE: {
        type: "PIE"
    },
    CATEGORY: {
        type: "CATEGORY"
    },
    SERIES: {
        type: "SERIES"
    },
    MAP: {
        type: "MAP"
    },
    TABLE: {
        type: "TABLE"
    },
    TEXT: {
        type: "TEXT"
    }
};


const NAMES = {
    SERIES: "Series Chart",
    CATEGORY: "Category Chart",
    TABLE: "Data Table",
    MAP: "Map",
    TEXT: "Text"

};

class Cell extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            edit: false
        }
    }

    _edit = () => {
        this.setState({
            edit: true
        })
    };

    _cancel = () => {
        this.setState({
            edit: false
        })
    };

    _onSave = (row, col, widget) => {
        this.props.onChange(this.props.index, widget);
        this.setState({
            edit: false
        })
    };

    _onRemove = () => {
        this.props.onCellRemove(this.props.index);
    };

    _onShadeAction = () => {
        this.setState({
            edit: false
        })
    };

    render() {

        let style = Object.assign({}, STYLES.CHART);

        style.height = this.props.data.height || 300;

        let title;

        if (this.props.data.title) title = ewars.I18N(this.props.data.title);

        return (
            <div className="row">
                <div className="col-12">
                    <div className="ide-tbar">
                        <div className="ide-tbar-text">{title}</div>
                        <div className="btn-group pull-right">
                            <Button
                                onClick={this._edit}
                                icon="fa-cog"/>
                            <Button
                                onClick={() => {
                                    this.props.onRemove(this.props.index);
                                }}
                                icon="fa-trash"/>
                        </div>
                    </div>
                </div>
                <div className="col-12">
                    <div className="chart" ref="chart" onClick={this._edit} style={style}>
                        <div className="chart-name">{NAMES[this.props.data.type]}</div>
                    </div>
                </div>
                {this.state.edit ?
                    <Shade
                        toolbar={false}
                        onAction={this._onShadeAction}
                        shown={this.state.edit}>
                        <WidgetEditor
                            visible={this.state.edit}
                            data={this.props.data}
                            onCancel={this._cancel}
                            onSave={this._onSave}/>
                    </Shade>
                    : null}
            </div>
        )
    }
}

class Row extends React.Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        ewars.subscribe("SHOW_DROPPER", this._showDropper);
    }

    _showDropper = () => {
        if (this.refs.dropper) {
            this.refs.dropper.style.display = "block";
        }
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        e.preventDefault();

        this.refs.dropper.style.display = "none";

        this.props.onAdd(e.dataTransfer.getData("d"));
    };

    _onChange = (index, data) => {
        this.props.onCellChange(index, data);
    };

    _remove = (index) => {
        let cells =  ewars.copy(this.props.data.definition);
        cells.splice(index, 1);

        this.props.onRebuild(cells);
    };

    render() {
        let items;
        if (this.props.data.definition.length <= 0) {
            items = <div className="placeholder">Drag a widget into the notebook to get started</div>
        } else {
            items = this.props.data.definition.map(function (item, index) {
                return <Cell data={item}
                             onRemove={this._remove}
                             onChange={this._onChange}
                             index={index}/>
            }.bind(this))
        }


        return (
            <div className="ide-panel ide-panel-absolute ide-scroll" onDragOver={this._onDragOver}
                 onDrop={this._onDrop}>
                <div className="dropper" ref="dropper" style={STYLES.DROPPER}>Drop Widget here</div>
                <div className="grid">
                    {items}
                </div>
            </div>
        )
    }
}

class Block extends React.Component {
    constructor(props) {
        super(props);
    }

    _dragStart = (e) => {
        ewars.emit("SHOW_DROPPER");
        e.dataTransfer.setData("d", this.props.cellType)
    };

    render() {
        return (
            <div className="block" draggable={true} onDragStart={this._dragStart}>
                <div className="block-content">{this.props.label}</div>
            </div>
        )
    }
}

class NotebookEditor extends React.Component {
    static defaultProps = {
        onClose: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            edit: 'EDIT',
            data: ewars.copy(this.props.data || {created_by: window.user.id})
        }

    }

    componentWillMount() {
        this.state.data = ewars.copy(this.props.data);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.uuid != this.props.data.uuid) {
            this.state.data = ewars.copy(nextProps.data || {created_by: window.user.id});

        }
    }

    _onAdd = (type) => {
        let data = ewars.copy(this.state.data.definition || []);

        data.push(DEFAULTS[type]);
        this.setState({
            data: {
                ...this.state.data,
                definition: data
            }
        })
    };

    _onCellChange = (index, cell) => {
        let data = ewars.copy(this.state.data.definition || []);
        data[index] = cell;

        this.setState({
            data: {
                ...this.state.data,
                definition: data
            }
        })
    };

    _onSettingChange = (data, prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _save = () => {
        if (!this.state.data.name) {
            ewars.growl("Please provide a title for your notebook");
            return;
        }

        if (this.state.data.uuid) {
            let bl = new ewars.Blocker(null, "Saving changes...");
            ewars.tx("com.ewars.notebook.update", [this.state.data.uuid, this.state.data])
                .then(resp => {
                    this.setState({
                        data: resp
                    }, () => {
                        bl.destroy();
                        ewars.growl("Changes saved!")
                    })
                })
        } else {
            let bl = new ewars.Blocker(null, "Creating notebook...");
            ewars.tx("com.ewars.notebook.create", [this.state.data])
                .then((resp) => {
                    bl.destroy();
                    ewars.growl("Notebook saved");
                    this.setState({
                        data: resp
                    })
                })
        }
    };

    _edit = (prop, value) => {
        ewars._queue.clear();
        this.setState({
            ...this.state,
            edit: value
        })
    };

    _getNotebook = () => {
        return <Notebook key={ewars.utils.uuid()} data={this.state.data} editor={true}/>;
    };

    _delete = () => {
        ewars.prompt("fa-trash", "Delete notebook?", "Are you sure you want to delete this notebook?", () => {
            let bl = new ewars.Blocker(null, "Deleting notebook");
            ewars.tx('com.ewars.notebook.delete', [this.state.data.uuid])
                .then((resp) => {
                    bl.destroy();
                    this.props.onClose();
                })
        })
    };

    _onRebuild = (data) => {
        this.setState({
            data: {
                ...this.state.data,
                definition: data
            }
        })
    };

    render() {

        let canEdit = false;
        if (!this.state.data.uuid) canEdit = true;
        if (this.state.data.created_by == window.user.id) canEdit = true;

        if (this._notebook) delete this._notebook;
        if (this.state.view != "EDIT") this._notebook = this._getNotebook();

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Notebook">
                    <div className="btn-group pull-right">
                        {this.state.edit == "EDIT" ?
                            <ewars.d.Button
                                icon="fa-save"
                                label={_l("SAVE_CHANGES")}
                                onClick={this._save}/>
                            : null}
                        <ewars.d.Button
                            icon="fa-times"
                            label={_l("CLOSE")}
                            onClick={() => {
                                this.props.onClose()
                            }}/>
                        {canEdit ?
                            <ewars.d.Button
                                icon="fa-trash"
                                label="Delete"
                                onClick={this._delete}/>
                            : null}
                    </div>

                    {canEdit ?
                        <div style={{width: 125, float: "right", marginLeft: 10}}>
                            <div className="ide-row">
                                <div className="ide-col" style={{maxWidth: 105}}>
                                    <ButtonGroup
                                        name="edit"
                                        value={this.state.edit}
                                        config={EDIT_MODES}
                                        onUpdate={this._edit}/>
                                </div>
                            </div>
                        </div>
                        : null}
                </ewars.d.Toolbar>
                <ewars.d.Row>

                    {this.state.edit == "EDIT" ?
                        <ewars.d.Cell borderRight={true} width="300">
                            <div className="heading">
                                <span>Widgets</span>
                            </div>
                            <div className="block-tree" style={{position: "relative"}}>
                                <Block label="Series Chart" cellType="SERIES"/>
                                <Block label="Category Chart" cellType="CATEGORY"/>
                                <Block label="Map" cellType="MAP"/>
                                <Block label="Table" cellType="TABLE"/>
                                <Block label="Text" cellType="TEXT"/>
                            </div>
                        </ewars.d.Cell>
                        : null}

                    <ewars.d.Cell borderRight={true}>
                        {this.state.edit == "EDIT" ?

                            <Row data={this.state.data}
                                 onRebuild={this._onRebuild}
                                 onCellChange={this._onCellChange}
                                 onRemove={this._onRemove}
                                 onAdd={this._onAdd}/>
                            :
                            this._notebook
                        }
                    </ewars.d.Cell>

                    {this.state.edit == "EDIT" ?
                        <ewars.d.Cell width="250">
                            <div className="ide-panel ide-panel-absolute ide-scroll"
                                 style={{padding: "10px 10px 0 10px"}}>
                                <Form
                                    definition={FORM}
                                    readOnly={false}
                                    updateAction={this._onSettingChange}
                                    data={this.state.data}/>
                            </div>
                        </ewars.d.Cell>
                        : null}

                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default NotebookEditor;
