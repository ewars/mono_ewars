import Dashboard from "./Dashboard";
import NotebookEditor from "./NotebookEditor";

function setNotebook(state, data) {
    return data;
}

function _removeCell(state, index) {
    state.definition.splice(index, 1);
    return state;
}

let reducer = (state, action, data) => {
    let procState = ewars.copy(state);

    switch (action) {
        case "SET_NOTEBOOK":
            procState = data;
            return procState;
        case "UNSET_NB":
            return null;
        case "UPDATE_PROP":
            procState[data.prop] = data.value;
            return procState;
        case "REMOVE_CELL":
            return _removeCell(procState, data.index);
        default:
            return state;
    }
};

class Component extends React.Component {
    constructor(props) {
        super(props);

        ewars.z.register("ANALYSIS", reducer, {});

        this.state = {
            view: "DEFAULT",
            notebook: null
        }
    }

    componentWillMount() {
        ewars.z.subscribe("ANALYSIS", this._onStoreChange);
    }

    componentWillUnmount() {
        ewars.z.unsubscribe("ANALYSIS", this._onStoreChange);
    }

    _onStoreChange = () => {
        this.setState({
            ...this.state,
            notebook: ewars.z.getState("ANALYSIS")
        })
    };

    render() {
        let view;

        if (!this.state.notebook) view = <Dashboard/>;
        if (this.state.notebook) view = <NotebookEditor data={this.state.notebook}/>;

        return (
            <div className="ide">
                {view}
            </div>
        )
    }
}

export default Component;