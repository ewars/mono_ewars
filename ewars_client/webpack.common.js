const path = require("path");
const { styles } = require("@ckeditor/ckeditor5-dev-utils");

module.exports = {
    mode: "development",
    resolve: {
        extensions: [".js", ".vue", ".json"],
        alias: {
            "mapbox-gl": path.join(__dirname, './node_modules/mapbox-gl/dist/mapbox-gl.js'),
            "webworkify": "webworkify-webpack"
        }
    },


    output: {
        path: path.join(__dirname, "../ewars/ewars/static/bundles"),
        filename: "[name].bundle.js",
        sourceMapFilename: "[name].map",
        globalObject: "self"
    },


    module: {
        noParse: /(mapbox-gl)\.js$/,
        rules: [
            {
                test: /\.svg$/,
                use: ['raw-loader']
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader",
                        options: {
                            singleton: true
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: styles.getPostCssConfig( {
                            themeImporter: {
                                themePath: require.resolve("@ckeditor/ckeditor5-theme-lark")
                            },
                            minify: true
                        })
                    }
                ]
            },
            {
                test: /\.js$/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            ["@babel/preset-env", {
                                targets: {
                                    browsers: [
                                        'last 2 Chrome major versions',
                                        'last 2 ChromeAndroid major versions',
                                        'last 2 Edge major versions',
                                        'last 2 Firefox major versions',
                                        'last 3 Safari major versions',
                                        'last 3 iOS major versions'
                                    ]
                                }
                            }],
                            "@babel/preset-flow",
                            "@babel/preset-react"
                        ],
                        plugins: [
                            // Stage 0
                            "@babel/plugin-proposal-function-bind",

                            // stage 1
                            "@babel/plugin-proposal-export-default-from",
                            "@babel/plugin-proposal-logical-assignment-operators",
                            ["@babel/plugin-proposal-optional-chaining", { "loose": false}],
                            ["@babel/plugin-proposal-pipeline-operator", { "proposal": "minimal" }],
                            ["@babel/plugin-proposal-nullish-coalescing-operator", { "loose": false }],
                            "@babel/plugin-proposal-do-expressions",

                            // stage 2
                            ["@babel/plugin-proposal-decorators", { "legacy": true }],
                            "@babel/plugin-proposal-function-sent",
                            "@babel/plugin-proposal-export-namespace-from",
                            "@babel/plugin-proposal-numeric-separator",
                            "@babel/plugin-proposal-throw-expressions",

                            // Stage 3
                            ["@babel/plugin-proposal-class-properties", { loose: false }],
                            "@babel/plugin-proposal-async-generator-functions",
                            "@babel/plugin-proposal-object-rest-spread",
                            "@babel/plugin-transform-classes"
                        ]
                    }
                }
            }
        ]
    },

    //devtool: "cheap-module-source-map",

    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    }
}
