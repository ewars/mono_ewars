import en from "./lang/en";
__.register(en);

import Component from "./components/Component.react";

import {
    Shade,
    Layout,
    ActionGroup,
    Row,
    Cell,
    Panel,
    Modal,
    Button,
    Toolbar
} from "../common";

ewars.d = {
    Shade,
    Layout,
    Row,
    Cell,
    Panel,
    Modal,
    Button,
    Toolbar,
    ActionGroup
}


ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


