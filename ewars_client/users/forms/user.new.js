module.exports = {
    name: {
        order: 0,
        label: "Name",
        type: "string",
        required: true,
        help: "Enter your full name",
        placeholder: ""
    },
    email: {
        order: 1,
        label: "Email Address",
        type: "string",
        required: true,
        help: "Enter a valid email address for the user"
    },
    confirm_email: {
        order: 2,
        label: "Confirm Email Address",
        type: "text",
        required: true,
        help: "Enter the email address"
    },
    password: {
        order: 3,
        type: "password",
        label: {
            en: "Password"
        },
        required: true
    },
    confirm_password: {
        order: 4,
        type: "password",
        label: {
            en: "Confirm Password"
        },
        required: true
    },
    language: {
        order: 5,
        label: "Default Language",
        type: "select",
        required: true,
        options: [
            ["en", "English"],
            ["fr", "French"]
        ]
    },
    account_id: {
        order: 6,
        type: "select",
        label: "Account",
        required: true,
        optionsSource: {
            resource: "Account",
            query: {},
            valSource: "id",
            labelSource: "name"
        }
    },
    organization_id: {
        type: "select",
        order: 7,
        label: {
            en: "Organization"
        },
        required: true,
        optionsSource: {
            resource: "TaxonomyItem",
            query: {
                $filter: ["tax_id eq '3'"],
                $orderby: ["name->>'en' ASC"]
            },
            valSource: "uuid",
            labelSource: "name"
        }
    },
    group_id: {
        order: 7,
        label: "User Group",
        type: "select",
        required: true,
        options: [
            [1, "Super Administrator"],
            [2, 'Global Administrator'],
            [3, 'Global User'],
            [4, 'Global Guest'],
            [5, 'Country Administrator'],
            [6, 'Country User'],
            [7, 'Country Guest'],
            [11, "Lab User"]
        ]
    },
    send_email: {
        order: 8,
        label: {
            en: "Send email to user with email address and password?"
        },
        type: "select",
        required: true,
        options: [
            [false, "No"],
            [true, "Yes"]
        ]
    }
};
