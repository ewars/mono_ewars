import { TextField } from "../../common/fields";

var PasswordEditor = React.createClass({
    _updateField: function (prop, value) {
        this.props.onChange("password", value);
    },

    render: function () {
        let canChange = false;
        if (this.props.user.system) canChange = true;
        if (this.props.user.email == window.user.email) canChange = true;


        if (canChange === false) {
            return (
                <div className="ide-panel ide-panel-absolute ide-scroll">
                    <div className="article" style={{paddingLeft: 16, paddingRight: 16}}>
                        <p>The user you are attempting to change the password of is a root-level user, you can only
                            change
                            your own password, or the password of users who are marked as "account controlled". If a
                            user
                            needs to change their password, they must use the password recovery feature at <a href="http://ewars.ws.recovery" target="_blank">http://ewars.ws/recovery</a> or log in
                            and change their password from their profile.</p>
                        <p>If you are seeing this message in error, please contact a support representative.</p>
                    </div>
                </div>
            )
        }

        return (
            <div className="ide-settings-panel">
                <div className="ide-settings-content">
                    <div className="ide-settings-form">
                        <div className="ide-section-basic">
                            <div style={{padding: 14}}>
                                <p className="info">The new password will be emailed to the user.</p>
                            </div>
                            <div className="hsplit-box">
                                <div className="ide-setting-label">New Password *</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="password"
                                        onUpdate={this._updateField}
                                        value={this.props.data}/>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
});

export default PasswordEditor;
