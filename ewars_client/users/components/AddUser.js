import {
    TextField,
    TextAreaField,
    SelectField,
    LocationField as LocationSelectField,
    ButtonGroup
} from "../../common/fields";

import {ROLES, STATUS} from "../constants/fields";

const SYSTEMIC = {
    options: [
        [false, "No"],
        [true, "Yes"]
    ],
    context_help: {
        true: "TRUE_SYSTEM",
        false: "FALSE_SYSTEM"
    }
};

const ERR_CODES = {
    ACTIVE_INVITE_EXISTS: "An invite has already been sent for the email address specified.",
    USER_EXISTS_IS_ACTIVE: "The email specified belongs to an already active user.",
    USER_EXISTS_IS_INACTIVE: "The email specified belongs to an already existing user whose status is Inactive.",
    USER_EXISTS_IS_REVOKED: "The email specified belongs to an already existing user whose access is currently revoked.",
    USER_IS_SYSTEM: "A user already exists with this email address and is controlled by another account, account-controlled users can not be invited to other accounts at this time."
};

const ORG_CONFIG = {
    optionsSource: {
        resource: "organization",
        valSource: "uuid",
        labelSource: "name",
        query: {}
    }
};

class ManualAddition extends React.Component {
    _onChange = (e) => {
        this.props.onChange(e.target.name, e.target.value);
    };

    render() {
        let error;

        if (this.props.error) {
            switch (this.props.error) {
                case "USER_EXISTS_IS_REVOKED":
                    error = (
                        <div className="warning">
                            <ewars.d.Row>
                                <ewars.d.Cell style={{flex: 3}}>
                                    <p><i className="fal fa-exclamation-triangle"></i>{__("USER_EXISTS_IS_REVOKED")}</p>

                                </ewars.d.Cell>
                                <ewars.d.Cell style={{flex: 1}}>
                                    <div className="btn-group pull-right" style={{display: "block"}}>
                                        <ewars.d.Button
                                            label={__("REINSTATE_USER")}
                                            onClick={this.props.reinstate}/>
                                        <ewars.d.Button
                                            label={__("CANCEL")}
                                            onClick={this._clear}/>
                                    </div>
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </div>
                    );
                    break;
                default:
                    let errorText = ERR_CODES[this.props.error] || this.props.error;
                    error = <p className="error"><i
                        className="fal fa-exclamation-triangle"></i>&nbsp;{errorText}</p>;
                    break;
            }
        }

        return (
            <div className="ide-settings-content">

                <div className="ide-settings-form">
                    {error}


                    <div className="form light">
                        <div className="form-field header">
                            <div className="label"><label>{__("USER")}</label></div>
                            <div className="field-wrapper">
                                <div className="form-header style-title">{__("USER")}</div>
                            </div>
                        </div>


                        <div className="form-field">
                            <div className="label"><label htmlFor="email">{__("EMAIL")} *</label></div>
                            <div className="field-wrapper">
                                <input type="text" name="email" onChange={this._onChange}
                                       value={this.props.data.email}/>
                            </div>
                            <div className="field-suffix">&nbsp;@ewars.ws</div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="name">{__("EMAIL")} *</label></div>
                            <div className="field-wrapper">
                                <input type="text" name="name" onChange={this._onChange} value={this.props.data.name}/>
                            </div>
                            <div className="field-suffix">&nbsp;@ewars.ws</div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="password">{__("PASSWORD")} *</label></div>
                            <div className="field-wrapper">
                                <input type="password" name="password" onChange={this._onChange}
                                       value={this.props.data.password}/>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="password_confirm">{__("CONFIRM_PASSWORD")} *</label>
                            </div>
                            <div className="field-wrapper">
                                <input type="password" name="password_confirm" onChange={this._onChange}
                                       value={this.props.data.password_confirm}/>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="organization">{__("ORGANIZATION")} *</label></div>
                            <div className="field-wrapper">
                                <SelectField
                                    name="org_id"
                                    config={ORG_CONFIG}
                                    value={this.props.data.org_id}
                                    onUpdate={this.props.onChange}/>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="phone">{__("PHONE")}</label></div>
                            <div className="field-wrapper">
                                <input type="text" name="phone" onChange={this._onChange}
                                       value={this.props.data.phone}/>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="occupation">{__("OCCUPATION")}</label></div>
                            <div className="field-wrapper">
                                <input type="text" name="occupation" onChange={this._onChange}
                                       value={this.props.data.occupation}/>
                            </div>
                        </div>

                        <div className="form-field header">
                            <div className="label"><label htmlFor="">{__("ACCOUNT_PERMISSIONS")}</label></div>
                            <div className="field-wrapper">
                                <div className="form-header style-title">{__("ACCESS_DETAILS")}</div>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="role">{__("ROLE")} *</label></div>
                            <div className="field-wrapper">
                                <SelectField
                                    name="role"
                                    config={{
                                        options: ROLES
                                    }}
                                    value={this.props.data.role}
                                    onUpdate={this.props.onChange}/>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="status">{__("STATUS")} *</label></div>
                            <div className="field-wrapper">
                                <SelectField
                                    name="status"
                                    config={{options: STATUS}}
                                    value={this.props.data.status}
                                    onUpdate={this.props.onChange}/>
                            </div>
                        </div>

                        {this.props.data.role == "REGIONAL_ADMIN" ?
                            <div className="form-field">
                                <div className="label"><label htmlFor="location">{__("LOCATION")} *</label></div>
                                <div className="field-wrapper">
                                    <LocationSelectField
                                        name="location"
                                        onUpdate={this.props.onChange}
                                        value={this.props.data.location}/>
                                </div>
                            </div>
                            : null}

                        <ewars.d.Toolbar>
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    onClick={this.props.add}
                                    color="green"
                                    label={__("ADD_USER")}/>
                            </div>
                        </ewars.d.Toolbar>
                    </div>
                </div>
            </div>

        )
    }
}


class InviteForm extends React.Component {
    _onChange = (e) => {
        this.props.onChange(e.target.name, e.target.value);
    };

    _onFieldChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {

        let error;

        if (this.props.error) {
            switch (this.props.error) {
                case "USER_EXISTS_IS_REVOKED":
                    error = (
                        <div className="warning">
                            <ewars.d.Row>
                                <ewars.d.Cell style={{flex: 3}}>
                                    <p><i className="fal fa-exclamation-triangle"></i>{__("USER_EXISTS_IS_REVOKED")}</p>

                                </ewars.d.Cell>
                                <ewars.d.Cell style={{flex: 1}}>
                                    <div className="btn-group pull-right" style={{display: "block"}}>
                                        <ewars.d.Button
                                            label={__("REINSTATE_USER")}
                                            onClick={this.props.reinstate}/>
                                        <ewars.d.Button
                                            label={__("CANCEL")}
                                            onClick={this._clear}/>
                                    </div>
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </div>
                    );
                    break;
                default:
                    let errorText = ERR_CODES[this.props.error] || this.props.error;
                    error = <p className="error"><i
                        className="fal fa-exclamation-triangle"></i>&nbsp;{errorText}</p>;
                    break;
            }
        }

        return (
            <div className="ide-settings-content">

                <div className="ide-settings-form">
                    {error}


                    <div className="form light">
                        <div className="form-field header">
                            <div className="label"><label>{__("USER")}</label></div>
                            <div className="field-wrapper">
                                <div className="form-header style-title">User</div>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="email">Email *</label></div>
                            <div className="field-wrapper">
                                <input type="text" name="email" value={this.props.data.email}
                                       onChange={this._onChange}/>
                            </div>
                        </div>

                        <div className="form-field header">
                            <div className="label"><label htmlFor="">Account Permissions</label></div>
                            <div className="field-wrapper">
                                <div className="form-header style-title">Access Details</div>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="role">Role *</label></div>
                            <div className="field-wrapper">
                                <SelectField
                                    name="role"
                                    config={{
                                        options: ROLES
                                    }}
                                    value={this.props.data.role}
                                    onUpdate={this._onFieldChange}/>
                            </div>
                        </div>

                        <div className="form-field">
                            <div className="label"><label htmlFor="status">Status *</label></div>
                            <div className="field-wrapper">
                                <SelectField
                                    name="status"
                                    value={this.props.data.status}
                                    config={{options: STATUS}}
                                    onUpdate={this._onFieldChange}/>
                            </div>
                        </div>

                        {this.props.data.role == "REGIONAL_ADMIN" ?
                            <div className="form-field">
                                <div className="label"><label htmlFor="location">Location *</label></div>
                                <div className="field-wrapper">
                                    <LocationSelectField
                                        name="location"
                                        value={this.props.data.location}
                                        onUpdate={this._onFieldChange}/>
                                </div>
                            </div>
                            : null}

                        <ewars.d.Toolbar>
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    onClick={this.props.invite}
                                    label="Send Invite"/>
                            </div>
                        </ewars.d.Toolbar>
                    </div>
                </div>
            </div>
        )
    }
}

const P_STYLE = {
    padding: 16
};

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

class AddUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "INVITE",
            errors: null,
            data: {
                email: "",
                password: "",
                password_confirm: "",
                role: null,
                phone: "",
                status: "ACTIVE",
                location: null,
                system: false
            },
            inviteError: null
        }
    }

    _setInvite = () => {
        this.setState({view: "INVITE"});
    };

    _setManual = () => {
        this.setState({view: "MANUAL"});
    };

    _setBulk = () => {
        this.setState({view: "BULK"});
    };

    _reinstate = () => {
        let blocker = new ewars.Blocker(null, "Reinstating user...");
        ewars.tx("com.ewars.user.reinstate", [this.state.data.email])
            .then((resp) => {
                blocker.destroy();
                this.props.close();
                ewars.growl(__("USER_REINSTATED"));
            })

    };

    _invite = () => {
        // No email address provided
        if (!this.state.data.email || this.state.data.email == "") {
            this.setState({error: __("ERROR_EMAIL")});
            this.setState({error: "Please provide a valid email address"});
            return;
        }

        // Invalid email address
        if (!validateEmail(this.state.data.email)) {
            this.setState({error: "The address provided is not a valid email"});
            return;
        }

        // No role provided
        if (!this.state.data.role || this.state.data.role == "") {
            this.setState({error: "Please select a role for the user"});
            return;
        }

        // No status provided
        if (!this.state.data.status || this.state.data.status == "") {
            this.setState({error: "Please select a status for the user"});
            return;
        }

        // Is this going to be an adminsitrative user
        if (this.state.data.role == "REGIONAL_ADMIN") {
            // No location selected for administration
            if (!this.state.data.location || this.state.data.location == "") {
                this.setState({error: "Please select a valid administrative location"});
                return;
            }
        }

        let blocker = new ewars.Blocker(null, "Processing invite...");
        ewars.tx("com.ewars.invite", [this.state.data])
            .then((resp) => {
                blocker.destroy();
                if (resp.err) {
                    this.setState({
                        error: resp.code
                    })
                } else {
                    ewars.growl("Invite Sent");
                    this.setState({
                        data: {
                            email: "",
                            role: null,
                            status: "ACTIVE",
                            location: null
                        }
                    })
                }
            })


    };

    _add = () => {
        // No email address provided
        if (!this.state.data.email || this.state.data.email == "") {
            this.setState({error: "Please provide a valid email address"});
            return;
        }


        // No role provided
        if (!this.state.data.role || this.state.data.role == "") {
            this.setState({error: "Please select a role for the user"});
            return;
        }

        // No status provided
        if (!this.state.data.status || this.state.data.status == "") {
            this.setState({error: "Please select a status for the user"});
            return;
        }

        // Is this going to be an adminsitrative user
        if (this.state.data.role == "REGIONAL_ADMIN") {
            // No location selected for administration
            if (!this.state.data.location || this.state.data.location == "") {
                this.setState({error: "Please select a valid administrative location"});
                return;
            }
        }

        if (!this.state.data.password || this.state.data.password == "") {
            this.setState({errors: "Please provide a valid password"});
            return;
        }

        if (!this.state.data.password_confirm || this.state.data.password_confirm == "") {
            this.setState({error: "Please confirm the password"});
            return;
        }

        if (this.state.data.password != this.state.data.password_confirm) {
            this.setState({error: "Passwords do not match"});
            return;
        }

        if (this.state.data.password.length < 6) {
            this.setState({error: "Password must be at least 6 characters long and contain one (1) number or non-standard character."});
            return;
        }

        if (!this.state.data.org_id || this.state.data.org_id == "") {
            this.setState({error: 'Please specify the organization this user belongs to or select "Other" if the organization isn\'t listed'});
            return;
        }

        let bl = new ewars.Blocker(null, "Creating user...");
        ewars.tx("com.ewars.user.create", [this.state.data])
            .then((resp) => {
                bl.destroy();
                if (resp.err) {
                    // An error occurred
                    this.setState({
                        error: resp.code
                    })
                } else {
                    this.props.close();
                    ewars.growl("User created");
                }
            })


    };

    _onChange = (prop, value) => {
        this.setState({
            ...this.state,
            error: null,
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    render() {

        let inviteClass = "ux-tab",
            manualClass = "ux-tab",
            bulkClass = "ux-tab",
            view,
            description;
        if (this.state.view == "INVITE") inviteClass += " active";
        if (this.state.view == "MANUAL") manualClass += " active";
        if (this.state.view == "BULK") bulkClass += " active";

        view = (
            <InviteForm
                error={this.state.error}
                data={this.state.data}
                reinstate={this._reinstate}
                onChange={this._onChange}
                invite={this._invite}/>
        );

        if (this.state.view == "MANUAL") {
            view = <ManualAddition
                error={this.state.error}
                data={this.state.data}
                reinstate={this._reinstate}
                onChange={this._onChange}
                add={this._add}/>;
        }

        if (this.state.view == "BULK") view = null;

        description =
            <p style={P_STYLE}>Send an invite to a user by email, the user will be asked to enter their details (name,
                password,
                etc...)</p>;

        if (this.state.view == "MANUAL") description =
            <p style={P_STYLE}>Create an account-controlled user, these users do not receive email notifications and
                their email addresses must end in @ewars.ws. Custom users can only be used in the account they were created in, these should be used as a temporary measure when a user account is required to expedite setup.</p>

        if (this.state.view == "BULK") description = <p style={P_STYLE}>Feature coming soon...</p>;

        return (
            <ewars.d.Panel>
                <div className="widget">
                    <div className="widget-header">
                        <div className="ux-tabs">
                            <div onClick={this._setInvite} className={inviteClass}><i
                                className="fal fa-envelope"></i>&nbsp;Send Invitation
                            </div>
                            <div
                                onClick={this._setManual}
                                className={manualClass}>
                                <i className="fal fa-user"></i>&nbsp;Custom User
                            </div>
                        </div>
                    </div>

                    <div className="body no-pad">
                        {description}
                        {view}
                    </div>
                </div>
            </ewars.d.Panel>
        )
    }
}

export default AddUser;
