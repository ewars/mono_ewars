import {
    LocationField as LocationSelector,
    SelectField
} from "../../common/fields";

const STATUS = {
    ACTIVE: "Active",
    INACTIVE: "Inactive"
};

const ITEM_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit assignment'],
    ['fa-trash', 'DELETE', 'Delete assignment']
];

const EDIT_ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-times', 'CANCEL_EDIT', 'Cancel']
];

const FORM_FIELD_CONFIG = {
    optionsSource: {
        resource: "form",
        query: {
            status: {eq: "ACTIVE"}
        },
        order: {
            "name.en": "DESC"
        },
        labelSource: "name",
        valSource: "id"
    }
};

const ASSIGN_STATUS_FIELD = {
    options: [
        ['ACTIVE', 'Active'],
        ['INACTIVE', 'Inactive']
    ]
}

class AssignmentItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            form: null,
            groups: []
        }

        ewars.tx("com.ewars.location.groups", [])
            .then(res => {
                this.setState({
                    groups: res
                })
            })
            .catch(err => {
                ewars.error("Could not load location groups")
            })
    }

    _action = (action) => {
        this.props.onAction(action, this.props.data);
    };

    _onChange = (prop, value, data) => {
        this.props.onChange(prop, value);
    };

    _onDefChange = (prop, value) => {
        this.props.onDefChange(prop, value);
    };

    render() {
        let locationName = null;
        if (this.props.data.location_name) locationName = this.props.data.location_name.en || this.props.data.location_name;

        let hasLocation = false;

        let form,
            locationFilter;

        if (this.props.data.form_id) {
            form = this.props.forms.filter(item => {
                return item.id == this.props.data.form_id;
            })[0] || null;


            hasLocation = form.features.LOCATION_REPORTING != null;
            if (hasLocation) {
                locationFilter = form.features.LOCATION_REPORTING.site_type_id || null;
            }
        }

        if (this.props.data.type == "UNDER") {
            locationFilter = null;
        }


        if (this.props.editing) {

            let showLocationSelector = false,
                showGroupSelector = false;

            if (hasLocation) {
                switch (this.props.data.type) {
                    case "GROUP":
                        showGroupSelector = true;
                        break;
                    case 'DEFAULT':
                        showLocationSelector = true;
                        break;
                    case "UNDER":
                        showLocationSelector = true;
                        break;
                    default:
                        break;
                }
            }

            return (
                <tr>
                    <td>
                        <div className="select-custom">
                            <select
                                onChange={(e) => {
                                    this.props.onChange("form_id", e.target.value);
                                }}
                                value={this.props.data.form_id}
                                name="" id="">
                                <option value="">No form selected</option>

                                {this.props.formOptions.map(item => {
                                    return <option value={item[0]}>{item[1]}</option>
                                })}
                            </select>
                        </div>
                    </td>
                    <td>
                        {hasLocation ?
                            <div className="select-custom">
                                <select

                                    className="select-custom"
                                    onChange={(e) => {
                                        this._onChange("type", e.target.value)
                                    }}
                                    value={this.props.data.type}
                                    name="" id="">
                                    <option value="DEFAULT">Specific location</option>
                                    <option value="GROUP">Location group</option>
                                    <option value="UNDER">Reporting locations within</option>
                                </select>
                            </div>
                            : null}
                    </td>
                    <td>
                        {showLocationSelector ?
                            <LocationSelector
                                config={{
                                    hideInactive: true,
                                    location_type: locationFilter,
                                    selectionTypeId: locationFilter
                                }}
                                onUpdate={(prop, value) => {
                                    this.props.onChange("location_id", value);
                                }}
                                value={this.props.data.location_id || null}/>
                            : null}
                        {showGroupSelector ?
                            <div className="select-custom">
                                <select
                                    onChange={(e) => {
                                        this.props.onDefChange("group", e.target.value);
                                    }}
                                    value={this.props.data.definition.group}>
                                    <option value="">None selected</option>
                                    {this.state.groups.map(item => {
                                        return <option value={item}>{item}</option>
                                    })}
                                </select>
                            </div>
                            : null}
                    </td>
                    <td>
                        <div className="select-custom">
                            <select
                                onChange={(e) => {
                                    this.props.onChange("status", e.target.value)
                                }}
                                value={this.props.data.status}
                                name="" id="">
                                <option value="">None selected</option>
                                <option value="ACTIVE">Active</option>
                                <option value="INACTIVE">Inactive</option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <ewars.d.ActionGroup
                            actions={EDIT_ACTIONS}
                            onAction={this._action}
                            right={true}/>
                    </td>
                </tr>
            )
        }

        let locationLabel = "";
        if (this.props.data.type == "DEFAULT" && !this.props.data.location_id && this.props.data.location_name) {
            locationLabel = "N/A";
        }

        if (this.props.data.type == "DEFAULT" && this.props.data.location_id && this.props.data.location_name) {
            locationLabel = this.props.data.location_name.en || this.props.data.location_name;
        }

        if (this.props.data.type == "GROUP") {
            locationLabel = "Group: " + this.props.data.definition.group;
        }

        if (this.props.data.type == "UNDER" && this.props.data.location_name) {
            locationLabel = "Locations under: " + (this.props.data.location_name.en || this.props.data.location_name);
        }

        return (
            <tr>
                <td>{this.props.data.form_name.en || this.props.data.form_name}</td>
                <td>{locationLabel}</td>
                <td></td>
                <td>{STATUS[this.props.data.status]}</td>
                <td>
                    <ewars.d.ActionGroup
                        actions={ITEM_ACTIONS}
                        onAction={this._action}
                        right={true}/>
                </td>
            </tr>
        )
    }
}

class AssignmentsControl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            assignments: [],
            editing: null,
            temp: null,
            formOptions: [],
            forms: null
        }

        this._loadAssigns(props);
    }

    _loadAssigns = (props) => {
        let blocker = new ewars.Blocker(null, "Loading assignments...");
        let assignments;
        ewars.tx("com.ewars.assignments.user", [props.data.id])
            .then(res => {
                this._assignments = res;
                return ewars.tx("com.ewars.forms.active", [["id", "name", "features"]])
            })
            .catch(err => {
                console.log(err);
                ewars.growl("Error loading assignments")
            })
            .then(res => {
                blocker.destroy();
                this.setState({
                    assignments: this._assignments,
                    forms: res,
                    formOptions: res.map(item => {
                        return [item.id, item.name.en || item.name];
                    }).sort((a, b) => {
                        return a[1].localeCompare(b[1]);
                    })
                })
            })
            .catch(err => {
                console.log(err);
                ewars.growl("Error loading forms");
            })

    };

    _reload = () => {
        ewars.tx("com.ewars.assignments.user", [this.props.data.id])
            .then(res => {
                console.log(res);
                this.setState({
                    editing: null,
                    temp: null,
                    assignments: res
                })
            })
            .catch(err => {
                console.log(err);
                ewars.growl("Error reloading assignments");
            })
    };

    _cancelEdit = () => {
        this.setState({
            editing: null,
            temp: null
        })
    };

    _saveEditing = () => {
        let blocker;

        let isDuplicate = false;
        let dupes = this.state.assignments.filter(item => {
            return item.location_id == this.state.editing.location_id && item.form_id == this.state.editing.form_id && item.uuid != this.state.editing.uuid;
        });
        if (dupes.length > 0) {
            ewars.error("An assignment of this type already exists");
            return;
        }

        if (this.state.editing.isNew) {
            blocker = new ewars.Blocker(null, "Creating assignment...");
            ewars.tx("com.ewars.assignment.create", [this.props.data.id, this.state.editing])
                .then(res => {
                    blocker.destroy();
                    ewars.growl("Assignment created");
                    this._reload();
                })
                .catch(err => {
                    console.log(err);
                    blocker.destroy();
                    ewars.growl("Assignment creation failed");
                })
        } else {

            blocker = new ewars.Blocker(null, "Updating assignment...");
            ewars.tx("com.ewars.assignment.update", [this.state.editing.uuid, this.state.editing])
                .then(res => {
                    blocker.destroy();
                    ewars.growl("Assignment updated");
                    this._reload();
                })
                .catch(err => {
                    blocker.destroy();
                    ewars.growl("Assignment update failed");
                })
        }

    };

    _addNew = () => {
        this.setState({
            editing: {
                isNew: true,
                uuid: ewars.utils.uuid(),
                location_id: null,
                form_id: null,
                status: "ACTIVE",
                type: "DEFAULT",
                definition: {}
            }
        })
    };

    _action = (action, data) => {
        switch (action) {
            case 'CANCEL_EDIT':
                this._cancelEdit();
                break;
            case 'SAVE':
                this._saveEditing();
                break;
            case 'DELETE':
                this._deleteAssignment(data);
                break;
            case 'EDIT':
                this.setState({
                    editing: ewars.copy(data),
                    temp: data
                });
                break;
            default:
                break;
        }
    };

    _deleteAssignment = (data) => {
        ewars.prompt("fa-trash", "Delete Assignments?", "Are you sure you want to delete this assignment?", () => {
            if (data.isNew) {
                this.setState({
                    editing: null
                })
            } else {
                let blocker = new ewars.Blocker(null, "Deleting assignment...");
                ewars.tx("com.ewars.assignment.delete", [data.uuid])
                    .then(res => {
                        blocker.destroy();
                        ewars.growl("Assignment deleted");
                        this._reload();
                    })
                    .catch(err => {
                        blocker.destroy();
                        console.log(err);
                        ewars.growl("Error deleting assignment");
                    })
            }
        });
    };

    _fieldChange = (prop, value) => {
        this.setState({
            editing: {
                ...this.state.editing,
                [prop]: value
            }
        })
    };

    _fieldDefChange = (prop, value) => {
        this.setState({
            editing: {
                ...this.state.editing,
                definition: {
                    ...this.state.definition,
                    [prop]: value
                }
            }
        })
    };

    render() {
        if (this.props.data.role != 'USER') {
            return (
                <div className="ide-settings-panel">
                    <div className="placeholder">Assignments are not available for this user type</div>
                </div>
            )
        }

        if (!this.state.forms) {
            return (
                <div className="ide-settings-panel">
                    <div className="placeholder">Loading forms...</div>
                </div>
            )
        }

        let editUUID = this.state.editing ? this.state.editing.uuid : null;
        let assigns = this.state.assignments.sort((a, b) => {
            return a.uuid.localeCompare(b.uuid);
        })

        return (
            <div className="ide-settings-panel">

                <div className="ide-settings-content">
                    <div className="ide-settings-form">

                        <div className="ide-section-basic" style={{display: "flex", flex: 1, flexDirection: "column", position: "absolute", width: "100%", height: "100%"}}>
                            <div className="header"  style={{flex: 1, maxHeight: "30px", height: "30px"}}>
                                <i className="fal fa-clipboard"></i>&nbsp;
                                Assignments
                                <div className="btn-group pull-right" style={{marginTop: "-6px"}}>
                                    <ewars.d.Button
                                        label="Add New Assignment"
                                        icon="fa-plus"
                                        onClick={this._addNew}/>
                                </div>
                            </div>

                            <div className="ide-inline-grid" style={{flex: 2, overflowY: "auto"}}>
                                <table width="100%">
                                    <thead>
                                    <tr>
                                        <th width="50%">Form</th>
                                        <th width="50%" colSpan={2}>Location</th>
                                        <th width="120px">Status</th>
                                        <th width="150px" style={{minWidth: "100px"}}>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.editing && this.state.editing.isNew == true ?
                                        <AssignmentItem
                                            onDefChange={this._fieldDefChange}
                                            onAction={this._action}
                                            onChange={this._fieldChange}
                                            formOptions={this.state.formOptions}
                                            forms={this.state.forms}
                                            data={this.state.editing}
                                            editing={true}/>
                                        : null}
                                    {assigns.map(item => {
                                        return <AssignmentItem
                                            onAction={this._action}
                                            data={item}
                                            onDefChange={this._fieldDefChange}
                                            formOptions={this.state.formOptions}
                                            forms={this.state.forms}
                                            onChange={this._fieldChange}
                                            editing={editUUID == item.uuid}/>
                                    })}
                                    </tbody>

                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )

    }
}

export default AssignmentsControl;
