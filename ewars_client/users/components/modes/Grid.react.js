var Button = require("../../../common/components/ButtonComponent.react");
var Modal = require("../../../common/components/ModalComponent.react");
const SelectField = require("../../../common/components/fields/SelectField.react");

var UserEditor = require("../UserEditor.react");
var PasswordEditor = require("../facets/PasswordEditor.react");
var AssignmentManager = require("../facets/AssignmentManager.react");

var UserWizard = require("../../../common/components/wizards/user/UserWizard.react");
var SearchField = require("../../../common/components/ide/SearchField.react");
var USER_TYPES = require("../../../common/constants/user_types");

var CONSTANTS = require("../../../common/constants");


import DataTable from "../../../common/DataTable/DataTable.react";

const USER_OPTIONS = _.map(USER_TYPES, function (item, key) {
    return [key, ewars.I18N(item.name)];
});

function noDef(item) {
    if (!item) return "";
    return ewars.I18N(item);
}

const COLUMNS = [
    {name: "name", config: {type: "text", label: "User Name"}},
    {name: "email", config: {type: "text", label: "Email"}},
    {
        name: "status",
        config: {
            type: "select",
            label: "Status",
            options: [["ACTIVE", "Active"], ["INACTIVE", "Inactive"], ["PENDING", "Pending"]]
        }
    },
    {
        name: "account.name",
        filterKey: "account_id",
        config: {
            label: "Account",
            type: "select",
            optionsSource: {
                resource: "account",
                valSource: "id",
                labelSource: "name",
                query: {}
            }
        }
    },
    {
        name: "location.name",
        config: {label: "Location"},
        fmt: noDef
    },
    {name: "user_type", config: {type: "select", label: "User Type", options: USER_OPTIONS}},
    {
        name: "organization.name",
        filterKey: "org_id",
        width: 300,
        config: {
            label: "Organization",
            type: "select",
            optionsSource: {
                resource: "organization",
                valSource: "uuid",
                labelSource: "name",
                query: {}
            }
        },
        fmt: noDef
    },
    {name: "registered", config: {type: "date", label: "Registered"}, fmt: ewars.DATE},
    {name: "language", config: {type: "select", label: "Language", options: [["en", "en - Default"]]}}
];

const ACTIONS = [
    {label: "Edit", action: "EDIT", icon: "fa-pencil"},
    {label: "Manage Assignments", action: "EDIT_ASSIGNS", icon: "fa-clipboard"},
    {label: "Change Password", action: "CHANGE_PASS", icon: "fa-lock"},
    {label: "View Profile", action: "PROFILE", icon: "fa-user"},
    {label: "Send Message", action: "MESSAGE", icon: "fa-paper-plane"},
    {label: "Revoke Access", action: "REVOKE", icon: "fa-ban"}
];

const JOINS = [
    "account:account_id:id",
    "location:location_id:uuid",
    "organization:org_id:uuid"
];

var UserManagementComponent = React.createClass({
    _blocker: null,

    getInitialState: function () {
        return {}
    },

    onEdit: function (node) {
        this.state.editing = JSON.parse(JSON.stringify(node));
        this.state.showEditor = true;
        this.forceUpdate();
    },

    onSortChange: function (sorting) {
        this.state.ordering = sorting;
        if (this.isMounted()) this.forceUpdate();
        this.query();
    },

    _onCreate: function () {
        this.state.showCreator = true;
        this.forceUpdate();
    },
    _cancelCreation: function () {
        this.state.showCreator = false;
        this.forceUpdate();
    },

    _changePage: function (data) {
        switch (data.page) {
            case "FF":
                if (this.state.offset < this.state.count) {
                    var pages = this.state.count / 10;
                    this.state.page = Math.ceil(pages);
                    this.state.offset = (this.state.page - 1) * 10;
                }
                break;
            case "F":
                if (this.state.offset < this.state.count) {
                    this.state.offset = (this.state.page + 1) * 10;
                    this.state.page++;
                }
                break;
            case "B":
                this.state.offset = (this.state.page - 1) * 10;
                this.state.page--;
                break;
            case "BB":
                this.state.offset = 0;
                this.state.page = 0;
                break;
            default:
                break;
        }

        if (this.state.page < 0) this.state.page = 0;
        if (this.state.page <= 0) this.state.offset = 0;

        this.forceUpdate();
        this.query();
    },

    _onCancelEdit: function () {
        this.state.showEditor = false;
        this.state.showPasswordEditor = false;
        this.state.password = null;
        this.state.editing = null;
        this.state.showAssignmentEditor = false;
        this.forceUpdate();
    },

    _updateUserProp: function (prop, value) {
        if (prop.indexOf("profile") >= 0) {
            var path = prop.split(".");
            if (!this.state.editing.profile) this.state.editing.profile = {};
            this.state.editing.profile[path[1]] = value;
        } else {
            this.state.editing[prop] = value;
        }
        this.forceUpdate();
    },

    _onUserSave: function () {
        var blocker = new ewars.Blocker(null, "Updating user...");
        ewars.tx("com.ewars.user.action", ["UPDATE", this.state.editing.id, this.state.editing])
            .then(function (resp) {
                this.state.editing = resp;
                blocker.destroy();
                ewars.growl("User Updated");
                this.forceUpdate();
            }.bind(this))
    },

    _onPasswordSave: function () {
        var blocker = new ewars.Blocker(null, "Updating password...");
        ewars.tx("com.ewars.user.action", ["UPDATE_PASSWORD", this.state.editing.id, {password: this.state.password}])
            .then(function (resp) {
                blocker.destroy();
                this.state.password = null;
                ewars.growl("Password updated");
                this.forceUpdate();
            }.bind(this))
   },

    _changeUserPassword: function (user) {
        this.state.editing = user;
        this.state.showPasswordEditor = true;
        this.forceUpdate();
    },

    _passwordChange: function (value) {
        this.state.password = value;
        this.forceUpdate();
    },

    _editAssignments: function (node) {
        this.state.editing = node;
        this.state.showAssignmentEditor = true;
        this.forceUpdate();
    },

    swapViewMode: function () {
        this.setState({
            viewMode: this.state.viewMode == "GRID" ? "ORG" : "GRID"
        })
    },

    _onCellAction: function (action, cell, row) {
        if (action == "EDIT") this.onEdit(row);
        if (action == "CHANGE_PASS") this._changeUserPassword(row);
        if (action == "EDIT_ASSIGNS") this._editAssignments(row);
    },

    render: function () {
        var modalButtons = [
            {label: "Save Change(s)", onClick: this._onUserSave, icon: "fa-disk"},
            {label: "Cancel", onClick: this._onCancelEdit, icon: 'fa-cancel'}
        ];

        var passwordButtons = [
            {label: "Update Password", onClick: this._onPasswordSave, icon: "fa-disk"},
            {label: "Cancel", onClick: this._onCancelEdit, icon: 'fa-cancel'}
        ];

        var assignmentButtons = [
            {label: "Close", onClick: this._onCancelEdit, icon: 'fa-cancel'}
        ];

        let isAdmin = [CONSTANTS.SUPER_ADMIN, CONSTANTS.GLOBAL_ADMIN].indexOf(window.user.user_type) >= 0 ? true : false;

        return (
            <div className="ide ide-dark ide-v">
                <div className="ide-panel">

                    <DataTable
                        columns={COLUMNS}
                        actions={ACTIONS}
                        join={JOINS}
                        onCellAction={this._onCellAction}
                        resource="user"/>

                    <UserWizard
                        visible={this.state.showCreator}
                        onCancel={this._cancelCreation}
                        onSaved={this._onCreateSave}/>

                    {this.state.showEditor ?
                        <Modal
                            height={500}
                            title="Edit User"
                            icon="fa-user"
                            buttons={modalButtons}
                            visible={this.state.showEditor}>
                            <UserEditor
                                data={this.state.editing}
                                onUpdate={this._updateUserProp}/>
                        </Modal>
                        : null}

                    {this.state.showPasswordEditor ?
                        <Modal
                            height={200}
                            title="Change Password"
                            icon="fa-lock"
                            buttons={passwordButtons}
                            visible={this.state.showPasswordEditor}>
                            <PasswordEditor
                                onChange={this._passwordChange}
                                data={this.state.password}/>
                        </Modal>
                        : null}

                    {this.state.showAssignmentEditor ?
                        <Modal
                            height={500}
                            title="Assignments"
                            icon="fa-clipboard"
                            buttons={assignmentButtons}
                            visible={this.state.showAssignmentEditor}>
                            <AssignmentManager
                                data={this.state.editing}/>
                        </Modal>
                        : null}
                </div>
            </div>
        )

    }
});

module.exports = UserManagementComponent;