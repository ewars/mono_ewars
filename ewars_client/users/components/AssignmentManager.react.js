import {
    SelectField,
    LocationField as LocationSelector,
    DateField
} from "../../common/fields";

const STATUS_CONFIG = {
    options: [
        ["ACTIVE", _l("ACTIVE")],
        ["INACTIVE", _l("INACTIVE")]
    ]
};


var fields = {
    form_id: {
        optionsSource: {
            resource: "form",
            query: {
                status: {eq: "ACTIVE"}
            },
            order: {
                "name.en": "DESC"
            },
            labelSource: "name",
            valSource: "id"
        }
    }
};


var Assignment = React.createClass({

    _delete: function () {
        ewars.prompt("fa-trash", "Delete Assignment?", "Are you sure you want to delete this assignment? The user will no longer be able to submit or review any items associated with this assignment.", function () {
            this.props.onDelete(this.props.data);
        }.bind(this))
    },

    _edit: function () {
        this.props.onEdit(this.props.data);
    },

    render: function () {
        var formName = ewars.I18N(this.props.data.form_name);
        var locationName = ewars.I18N(this.props.data.location_name);

        return (
            <tr className="row">
                <td className="cell">{locationName}</td>
                <td className="cell">{formName}</td>
                <td className="cell">{_l(this.props.data.status)}</td>
                <td className="cell">
                    <ewars.d.ActionButton
                        i="fa-pencil"
                        onClick={this._edit}/>
                    <ewars.d.ActionButton
                        i="fa-trash"
                        onClick={this._delete}/>
                </td>
            </tr>
        )
    }
});

var AssignmentManager = React.createClass({
    getInitialState: function () {
        return {
            assignments: [],
            assignment: {},
            showEditor: false,
            form: null
        }
    },

    _onEdit: function (assignment) {
        this.state.assignment = ewars.copy(assignment);
        this.state.assignment = {
            uuid: assignment.uuid || null,
            user_id: assignment.user_id || null,
            location_uuid: assignment.location_id || assignment.definition.location_uuid,
            form_id: assignment.form_id || assignment.definition.form_id,
            status: assignment.status
        };
        this.state.showEditor = true;
        this.forceUpdate();
    },

    _newAssign: function () {
        this.state.assignment = {
            user_id: this.props.data.id,
            form_id: null,
            location_uuid: null,
            start_date: null,
            end_date: null
        };
        this.state.showEditor = true;
        this.forceUpdate();
    },

    componentWillMount: function () {
        var blocker = new ewars.Blocker(null, "Loading assignments...");
        ewars.tx("com.ewars.assignments.user", [this.props.data.id])
            .then(function (resp) {
                this.state.assignments = resp;
                blocker.destroy();
                this.forceUpdate();
            }.bind(this))
    },

    _reload: function () {
        var blocker = new ewars.Blocker(null, "Reloading users assignments...");
        ewars.tx("com.ewars.assignments.user", [this.props.data.id])
            .then(function (resp) {
                this.state.assignments = resp;
                blocker.destroy();
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _cancelEdit: function () {
        this.state.assignment = {};
        this.state.showEditor = false;
        this.state.form = null;
        this.state.form = null;
        this.forceUpdate();
    },

    _updateEdit: function (prop, value) {
        if (prop == "form_id" && value) {
            // Need to load the forms location_type
            var blocker = new ewars.Blocker(null, "Loading form details...");
            ewars.tx("com.ewars.resource", ["form", value, ["features"], null])
                .then(function (resp) {
                    this.state.form = resp;
                    blocker.destroy();
                    this.state.assignment[prop] = value;
                    this.forceUpdate();
                }.bind(this))
        } else {
            this.state.assignment[prop] = value;
            this.forceUpdate();
        }
    },

    _save: function () {
        if (this.state.assignment.uuid) {
            this._updateAssignment();
        } else {
            if (!this.state.assignment.form_id || this.state.assignment.form_id == "") {
                ewars.growl("Please select a form");
                return;
            }

            if (!this.state.assignment.location_uuid || this.state.assignment.location_uuid == "") {
                ewars.growl("Please select a valid location");
                return;
            }

            if (!this.state.assignment.status || this.state.assignment.status == "") {
                ewars.growl("Please select a status for the assignment");
                return;
            }
            this._createAssignment();
        }
    },

    _updateAssignment: function () {
        var blocker = new ewars.Blocker(null, "Updating assignment...");
        ewars.tx("com.ewars.assignment.update", [this.state.assignment.uuid, this.state.assignment])
            .then(function (resp) {
                this.state.assignment = null;
                this.state.showEditor = false;
                blocker.destroy();
                this._reload();
                ewars.growl("Assignment updated");
            }.bind(this))
    },

    _deleteAssignment: function (assignment) {
        var blocker = new ewars.Blocker(null, "Deleting assignment...");
        ewars.tx("com.ewars.assignment.delete", [assignment.uuid])
            .then(function (resp) {
                blocker.destroy();
                this._reload();
                ewars.growl("Assignment deleted");
            }.bind(this))
    },

    _createAssignment: function () {
        var blocker = new ewars.Blocker(null, "Creating assignment...");
        ewars.tx("com.ewars.assignment.create", [this.props.data.id, this.state.assignment])
            .then(function (resp) {
                this.state.assignment = null;
                this.state.showEditor = false;
                blocker.destroy();
                this._reload();
                ewars.growl("Assignment created");
            }.bind(this))
    },

    render: function () {
        if (this.props.data.role != "USER") {
            return (
                <div className="placeholder">Assignments are only available for Reporting Users</div>
            )
        }

        const getName = (d) => {
            return d.form_name.en || d.form_name;
        };

        let m_rows = this.state.assignments.sort((a, b) => {
            if (getName(a) > getName(b)) return 1;
            if (getName(a) < getName(b)) return -1;
            return 0;
        });

        let rows = m_rows.map((item) => {
            if (item.status != 'PENDING') {
                return (
                    <Assignment
                        onDelete={this._deleteAssignment}
                        onEdit={this._onEdit}
                        key={item.uuid}
                        data={item}/>

                )
            }
        });

        var location_config = {
            location_type: null,
            hideInactive: true
        };
        if (this.state.form) {
            location_config.selectionTypeId = this.state.form.features.LOCATION_REPORTING.site_type_id;
        }

        var readOnly = true;
        if (this.state.showEditor) {
            if (!this.state.assignment.uuid) {
                readOnly = false;
            }
        }


        return (
            <div className="ide-settings-panel">
                <div className="ide-settings-content">
                    <div className="ide-settings-form">

                        {this.state.showEditor ?
                            <div className="ide-section-basic">
                                <div className="header">
                                    Edit Assignment
                                    <div className="btn-group pull-right" style={{marginTop: -6}}>
                                        <ewars.d.Button
                                            icon="fa-save"
                                            onClick={this._save}
                                            label="Save Change(s)"/>
                                        <ewars.d.Button
                                            icon="fa-times"
                                            onClick={this._cancelEdit}
                                            label="Cancel"/>
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">Form*</div>
                                    <div className="ide-setting-control">
                                        <SelectField
                                            name="form_id"
                                            readOnly={readOnly}
                                            value={this.state.assignment.form_id}
                                            config={fields.form_id}
                                            onUpdate={this._updateEdit}/>
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">Status*</div>
                                    <div className="ide-setting-control">
                                        <SelectField
                                            name="status"
                                            value={this.state.assignment.status}
                                            config={STATUS_CONFIG}
                                            onUpdate={this._updateEdit}/>
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">Location*</div>
                                    <div className="ide-setting-control">
                                        <LocationSelector
                                            name="location_uuid"
                                            readOnly={readOnly}
                                            config={location_config}
                                            hideInactive={true}
                                            onUpdate={this._updateEdit}
                                            value={this.state.assignment.location_uuid}/>
                                    </div>
                                </div>



                            </div>
                            : null}

                        {!this.state.showEditor ?
                            <div className="ide-section-basic" style={{display: "flex", flex: 1, flexDirection: "column", position: "absolute", width: "100%", height: "100%"}}>
                                <div className="header" style={{flex: 1, maxHeight: "30px", height: "30px"}}>
                                    Assignments
                                    <div className="btn-group pull-right" style={{marginTop: -6}}>
                                        <ewars.d.Button
                                            icon="fa-plus"
                                            onClick={this._newAssign}
                                            label="Add Assignment"/>
                                    </div>
                                </div>

                                <div className="ide-inline-grid" style={{flex: 2, overflowY: "auto"}}>
                                    <table width="100%">
                                        <thead>
                                        <tr>
                                            <th>Location</th>
                                            <th>Form</th>
                                            <th>Status</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {rows}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            : null}

                    </div>
                </div>
            </div>
        )
    }
});


export default AssignmentManager;
