export const ROLES = [
    ["USER", _l("USER")],
    ["REGIONAL_ADMIN", _l("REGIONAL_ADMIN")],
    ["ACCOUNT_ADMIN", _l("ACCOUNT_ADMIN")]
];

export const STATUS = [
    ["ACTIVE", _l("ACTIVE")],
    ["INACTIVE", _l("INACTIVE")]
];