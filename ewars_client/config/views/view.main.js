import {state, ComponentState} from '../cmp_state';

import ViewRoot from './view.root';

import TabLocations from '../tabs/tab.locations';


const TABS = {
    'Locations': TabLocations
};

class ViewMain extends React.Component {
    constructor(props) {
        super(props)

        this.state = new ComponentState();

        ewars.subscribe('VIEW_UPDATED', this._onChange);
    }

    _onChange = () => {
        this.setState(this.state);
    };

    _selectView = (view) => {
        state.setView(view.n);
    };

    render() {
        let view;

        if (state.activeTab != 'DEFAULT') {
            let tab = state.getActiveTab();
            if (TABS[tab.n]) {
                let Cmp = TABS[tab.n];
                view = <Cmp/>;
            }
        } else {
            view = <ViewRoot/>;
        }

        let tabs = this.state.tabs.map(item => {
            return (
                <ewars.d.Button
                    highlight={item._ == state.activeTab}
                    label={item.name || item.n}
                    icon={item.i || undefined}
                    closable={true}
                    onClose={() => {
                        state.removeTab(item._);
                    }}
                    onClick={() => {
                        state.setActiveTab(item._);
                    }}/>
            )
        })

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>
                    <div className="btn-group btn-tabs">
                        <ewars.d.Button
                            highlight={state.activeTab == 'DEFAULT'}
                            onClick={() => {
                                state.setActiveTab('DEFAULT');
                            }}
                            label="Config"
                            icon="fa-cogs"/>
                        {tabs}
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewMain;