class ViewUsage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {data: {}}

        ewars.tx('com.ewars.account.usage', [])
            .then(resp => {
                this.setState({data: resp});
            })
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Usage">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <table className="data-table">
                            <thead>
                            <tr>
                                <th>Metric</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Database size</th>
                                <td>{this.state.data.db_size || '0 MB'}</td>
                            </tr>
                            <tr>
                                <th>Emails sent</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Users</th>
                                <td>{this.state.data.users || 0}</td>
                            </tr>
                            <tr>
                                <th>Locations</th>
                                <td>{this.state.data.locations || 0}</td>
                            </tr>
                            <tr>
                                <th>Forms</th>
                                <td>{this.state.data.forms || 0}</td>
                            </tr>
                            <tr>
                                <th>Devices</th>
                                <td>{this.state.data.devices || 0}</td>
                            </tr>
                            <tr>
                                <th>Dataset records</th>
                                <td>{this.state.data.records_ds || 0}</td>
                            </tr>
                            <tr>
                                <th>Form records</th>
                                <td>{this.state.data.records || 0}</td>
                            </tr>
                            <tr>
                                <th>Alerts</th>
                                <td>{this.state.data.alerts || 0}</td>
                            </tr>
                            <tr>
                                <th>Event logs</th>
                                <td>{this.state.data.events || 0}</td>
                            </tr>
                            </tbody>
                        </table>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewUsage;