import * as TABLES from '../constants/tables/';
import Utils from '../utils';

const DEFAULT_ACTIONS = [
    ['fa-sync', 'RELOAD'],
    ['fa-filter', 'FILTER'],
    ['fa-ellipsis-v', 'NOOPA'],
    ['fa-fast-backward', 'BB'],
    ['fa-backward', 'B'],
    ['fa-forward', 'F'],
    ['fa-fast-forward', 'FF']
]

class TableHeaderCell extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <th>{this.props.data.l}</th>
        )
    }
}

class TableHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let headers = [];
        if (TABLES[this.props.table]) {
            headers = TABLES[this.props.table].columns.map(col => {
                return (
                    <TableHeaderCell
                        key={col.n}
                        sorts={this.props.sorts}
                        filters={this.props.filters}
                        data={col}/>
                )
            })
        }

        return (
            <thead>
            <tr>
                {headers}
            </tr>
            </thead>
        )
    }
}

class TableRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                {this.props.columns.map(col => {
                    return (
                        <td key={col.n}>{this.props.data[col.n] || ''}</td>
                    )
                })}
            </tr>
        )
    }
}

class FilterPanel extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="filter-panel">
                <ewars.d.Panel>

                </ewars.d.Panel>
            </div>
        )
    }
}

class ViewTable extends React.Component {
    constructor(props) {
        super(props);

        let table_def = TABLES[this.props.data._];

        this.state = {
            data: [],
            limit: 50,
            total: 'unknown',
            offset: 0,
            sorts: table_def.initialSort || {},
            filters: table_def.initialFilter || {},
            search: table_def.search || false,
            searchTerm: '',
            def: table_def,
            columns: table_def.columns,
            map: table_def.map || {},
            actions: table_def.actions || null,
            showFilters: false
        };

        if (table_def) {
            table_def.query(this.state.limit, this.state.offset, this.state.sorts, this.state.filters)
                .then(resp => {
                    this.setState({
                        data: resp.results,
                        total: resp.total,
                        limit: this.state.limit,
                        offset: this.state.offset,
                        sorts: this.state.sorts,
                        filters: this.state.filters
                    })
                })
        }
    }

    _onAction = (action, data) => {

    };

    componentWillReceiveProps(props) {
        let table_def = TABLES[props.data._];

        this.state = {
            data: [],
            limit: 50,
            total: 'unknown',
            offset: 0,
            sorts: table_def.initialSort || {},
            filters: table_def.initialFilter || {},
            search: table_def.search || false,
            searchTerm: '',
            def: table_def,
            columns: table_def.columns,
            map: table_def.map || {},
            actions: table_def.actions || null,
            showFilters: false
        };

        if (table_def) {
            table_def.query(this.state.limit, this.state.offset, this.state.sorts, this.state.filters)
                .then(resp => {
                    this.setState({
                        data: resp.results,
                        total: resp.total,
                        limit: this.state.limit,
                        offset: this.state.offset,
                        sorts: this.state.sorts,
                        filters: this.state.filters
                    })
                })
        }
    };

    render() {

        let actions = Utils.copy(DEFAULT_ACTIONS);

        if (this.state.actions) {
            actions.push(['fa-ellipsis-v', 'NOOPB']);
            this.state.actions.forEach(item => {
                actions.push(item);
            })
        }


        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label={this.props.data.n} icon={this.props.data.i || null}>
                    <ewars.d.ActionGroup
                        right={true}
                        actions={actions}/>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="data-table">
                                <table className="data-table">
                                    <TableHeader
                                        table={this.props.data._}
                                        filters={this.state.filters}
                                        sorts={this.state.sorts}/>
                                    <tbody>
                                        {this.state.data.map(row => {
                                            return (
                                                <TableRow
                                                    key={row.id || row.uuid}
                                                    map={this.state.map}
                                                    data={row}
                                                    columns={this.state.columns}/>
                                            )
                                        })}

                                    </tbody>
                                </table>
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Toolbar>
                    <span>Showing __ to ___ of {this.state.total}</span>
                </ewars.d.Toolbar>
                {this.state.showFilters ?
                    <FilterPanel
                        filters={this.state.filters}
                        table={this.props.data._}/>
                : null}
            </ewars.d.Layout>
        )
    }
}

export default ViewTable;