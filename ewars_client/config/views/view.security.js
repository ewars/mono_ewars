const ACTIONS = [
    ['fa-save', 'SAVE']
]

class ViewSecurity extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Security & Authentication">
                    <ewars.d.ActionGroup
                        right={true}
                        actions={ACTIONS}/>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>

                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewSecurity;