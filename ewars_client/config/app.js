import ViewMain from './views/view.main';

ReactDOM.render(
    <ViewMain/>,
    document.getElementById("application")
);