export default {
    query: (limit, offset, sorts, filters) => {
        return ewars.tx('com.ewars.alarms', [limit, offset, sorts, filters])
    },
    actions: [
        ['fa-plus', 'CREATE']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT'],
        ['fa-eye', 'VIEW_ALERTS'],
        ['fa-ellipsis-v', 'NOOP'],
        ['fa-trash', 'DELETE']
    ],
    columns: [
        {n: 'name', l: 'Name', sort: true},
        {n: 'status', l: 'Status', sort: true},
        {n: 'created', l: 'Created', t: 'DATE', sort: true},
        {n: 'created_by', l: 'Created by'},
        {n: 'modified', l: 'Modified', t: 'DATE', sort: true},
        {n: 'modified_by', l: 'Modified by'}
    ],
    initialSort: {
        'name': 'DESC'
    },
    search: true,
    filters: {
        'status': {
            t: 'SELECT',
            o: ['ACTIVE', 'INACTIVE']
        }
    },
    map: {
        ACTIVE: 'Active',
        INACTIVE: 'Inactive'
    }
}