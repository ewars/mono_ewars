import ALARMS from './table.alarms';
import ASSIGNMENTS from './table.assignments';
import LOCATION_TYPES from './table.location_types';
import ROLES from './table.roles';
import TEAMS from './table.teams';
import USERS from './table.users';
import LOCATIONS from './table.locations';
import DEVICES from './table.devices';

export {
    ALARMS,
    ASSIGNMENTS,
    LOCATION_TYPES,
    ROLES,
    TEAMS,
    USERS,
    LOCATIONS,
    DEVICES
}



