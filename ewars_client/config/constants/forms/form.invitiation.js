import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Invitation',
    fields: [
        {
            _: 0,
            n: 'email',
            l: 'Email',
            t: FIELD_TYPES.TEXT,
            r: true
        },
        {
            _: 1,
            n: 'role',
            l: 'Role',
            t: FIELD_TYPES.ROLE
        },
        {
            _: 2,
            n: 'location_id',
            l: 'Location',
            t: FIELD_TYPES.LOCATION
        },
        {
            _: 3,
            n: 'status',
            l: 'Status',
            t: FIELD_TYPES.SELECT,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        }
    ],
    logic: [],
    validation: []
}