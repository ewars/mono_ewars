import FIELD_TYPES from '../const.field_types';

export default {
    name: 'General settings',
    fields: [
        // General settings
        {
            _: 0,
            t: FIELD_TYPES.SECTION,
            l: 'General'
        },
        {
            _: '0.1',
            t: FIELD_TYPES.TEXT,
            n: 'name',
            l: 'Account name'
        },
        {
            _: '0.2',
            t: FIELD_TYPES.TEXT,
            n: 'domain',
            l: 'Domain'
        },
        {
            _: '0.3',
            t: FIELD_TYPES.TEXT,
            n: 'description',
            multiline: true,
            l: 'Description'
        },
        {
            _: '0.3',
            n: 'iso2',
            t: FIELD_TYPES.TEXT,
            l: 'ISO2'
        },
        {
            _: '0.4',
            n: 'iso3',
            t: FIELD_TYPES.TEXT,
            l: 'ISO3'
        },
        {
            _: 1,
            l: 'System default',
            t: FIELD_TYPES.SECTION
        },
        {
            _: 1.1,
            l: 'Default language',
            t: FIELD_TYPES.SELECT,
            n: 'default_lang',
            o: [
                ['en', 'English'],
                ['fr', 'French']
            ]
        },
        {
            _: 1.2,
            l: 'Default timezone',
            n: 'timezone',
            t: FIELD_TYPES.SELECT,
            o: []
        },
        {
            _: 1.3,
            l: 'Default date format',
            n: 'def_date_format',
            t: FIELD_TYPES.SELECT,
            o: []
        },
        {
            _: 1.4,
            l: 'Default time format',
            n: 'def_time_format',
            t: FIELD_TYPES.SELECT,
            o: []
        },
        {
            _: 1.5,
            l: 'Translation',
            n: 'translation',
            t: FIELD_TYPES.SELECT,
            o: []
        },
        {
            _: 2,
            l: 'Advanced',
            t: FIELD_TYPES.SECTION
        },
        {
            _: 2.1,
            n: 'allow_location_request',
            l: 'Allow requesting new locations',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 2.2,
            n: 'allow_user_alarms',
            l: 'Allow users to specify own alarms',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 2.3,
            n: 'allow_user_data_sets',
            l: 'Allow user-created data sets',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 2.4,
            n: 'allow_temp_orgs',
            l: 'Allow entry of organization',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 2.5,
            n: 'allow_assignment_request',
            l: 'Allow assignment request',
            t: FIELD_TYPES.SWITCH
        }
    ],
    logic: [],
    validation: {}
}
