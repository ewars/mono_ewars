import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Field',
    fields: [
        {
            _: 0,
            n: 'label',
            l: 'Label',
            t: FIELD_TYPES.TEXT,
            multilang: true
        },
        {
            _: 1,
            n: 'type',
            l: 'Field type',
            t: FIELD_TYPES.SELECT,
            o: [
                ['text', 'Text'],
                ['date', 'Date'],
                ['numeric', 'Numeric']
            ]
        },
        {
            _: 2,
            n: 'name',
            l: 'Field name',
            t: FIELD_TYPES.TEXT
        },
        {
            _: 3,
            n: 'required',
            l: 'Required',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 4,
            n: 'instructions',
            l: 'Instructions',
            t: FIELD_TYPES.TEXT,
            multline: true
        },
        {
            _: 5,
            n: 'options',
            l: 'Options',
            t: FIELD_TYPES.OPTIONS
        },
        {
            _: 6,
            n: 'default',
            l: 'Default value',
            t: null
        },
        {
            _: 7,
            n: 'redacted',
            l: 'Redacted',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 8,
            n: 'multiselect',
            l: 'Multi-Select',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 9,
            n: 'b_conditions',
            l: 'Conditions',
            t: FIELD_TYPES.SWITCH
        },
        {
            _: 10,
            n: 'conditions',
            l: 'Conditions',
            t: FIELD_TYPES.CONDITIONS
        },
        {
            _: 11,
            n: 'validations',
            l: 'Validations',
            t: FIELD_TYPES.VALIDATIONS
        }
    ]
}