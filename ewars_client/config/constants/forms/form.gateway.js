import FIELD_TYPES from '../const.field_types';

export default {
    name: 'SMS gateway',
    fields: [
        {
            _: 0,
            t: FIELD_TYPES.TEXT,
            n: 'name',
            l: 'Name'
        }
    ],
    logic: [],
    validation: []
}