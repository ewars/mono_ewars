import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Form',
    fields: [
        // General settings
        {
            _: 0,
            l: 'General settings',
            t: FIELD_TYPES.SECTION
        },
        {
            _: '0.1',
            l: 'Name',
            n: 'name',
            t: FIELD_TYPES.TEXT,
            multilang: true,
            r: true
        },
        {
            _: '0.2',
            l: 'Status',
            n: 'status',
            t: FIELD_TYPES.SELECT,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            _: '0.3',
            n: 'description',
            l: 'Description',
            multiline: true,
            multilang: true,
            t: FIELD_TYPES.TEXT
        },
        {
            _: '0.4',
            n: 'eid_prefix',
            l: 'EID prefix',
            t: FIELD_TYPES.TEXT
        },
        // Form restrictions
        {
            _: 1,
            l: 'Form restrictions',
            t: FIELD_TYPES.SECTION
        },
        {
            _: 1.1,
            n: 'allow_export',
            l: 'Allow export',
            t: FIELD_TYPES.SWITCH
        }
    ]
}