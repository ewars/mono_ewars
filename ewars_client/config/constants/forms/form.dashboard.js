import FIELD_TYPES from '../const.field_types';

export default [
    {
        _: 0,
        n: 'name',
        t: FIELD_TYPES.TEXT,
        l: 'Name',
        r: true
    },
    {
        _: 1,
        n: 'status',
        t: FIELD_TYPES.SELECT,
        l: 'Status',
        r: true,
        o: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive']
        ]
    },
    {
        _: 2,
        n: 'color',
        t: FIELD_TYPES.COLOR,
        l: 'Color'
    },
    {
        _: 3,
        n: 'description',
        t: FIELD_TYPES.text,
        multilang: true,
        multiline: true,
        l: 'Description'
    }
]