import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Location',
    fields: [
        // General
        {
            _: '0',
            t: FIELD_TYPES.SECTION,
            l: 'General'
        },
        {
            _: '0.1',
            t: FIELD_TYPES.TEXT,
            n: 'name',
            r: true,
            l: 'Name',
            multilang: true
        },
        {
            _: '0.2',
            t: FIELD_TYPES.DISPLAY,
            n: 'uuid',
            l: 'UUID'
        },
        {
            _: '0.3',
            t: FIELD_TYPES.SELECT,
            n: 'status',
            l: 'Status',
            r: true,
            o: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ]
        },
        {
            _: '0.4',
            t: FIELD_TYPES.TEXT,
            n: 'pcode',
            l: 'PCODE',
            r: true
        },
        {
            _: '0.5',
            t: FIELD_TYPES.LOCATION_TYPE,
            n: 'site_type_id',
            l: 'Location type',
            r: true
        },
        {
            _: '0.6',
            t: FIELD_TYPES.LOCATION_GROUP,
            n: 'location_groups',
            l: 'Location group(s)'
        },
        // Mapping
        {
            _: '1',
            t: FIELD_TYPES.SECTION,
            l: 'Mapping'
        },
        {
            _: '1.1',
            t: FIELD_TYPES.SELECT,
            l: 'Geometry type',
            r: true,
            o: [
                ['ADMIN', 'Geometry'],
                ['POINT', 'Point']
            ]
        },
        {
            _: '1.2',
            t: FIELD_TYPES.GEOM_EDITOR,
            l: 'Geometry Editor'
        },
        {
            _: '1.3',
            t: FIELD_TYPES.LNG_LAT,
            l: 'Default center (lng/lat)'
        },
        {
            _: '1.4',
            t: FIELD_TYPES.NUMERIC,
            l: 'Default zoom'
        },
        {
            _: '1.5',
            t: FIELD_TYPES.TEXT,
            multitline: true,
            l: 'GeoJSON'
        }
    ],
    logic: [],
    validations: []
}