import FIELD_TYPES from '../const.field_types';

export default {
    name: 'Registration settings',
    fields: [
        {
            _: 0,
            n: 'enabled',
            t: FIELD_TYPES.SWITCH,
            l: 'Allow registration'
        },
        {
            _: 1,
            n: 'org_required',
            t: FIELD_TYPES.SWITCH,
            l: 'Organization required'
        },
        {
            _: 2,
            n: 'restrict_domain',
            t: FIELD_TYPES.TEXT,
            l: 'Email domain restriction'
        }
    ],
    logic: [],
    validation: []
}