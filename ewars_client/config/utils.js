class Utils {
    constructor() {}

    static copy = (data) => {
        return JSON.parse(JSON.stringify(data));
    }
}

export default Utils;