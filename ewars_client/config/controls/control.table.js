class DatatableRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>

            </tr>
        )
    }
}

class DatatableHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <th></th>
        )
    }
}

class Datatable extends React.Component {
    static defaultProps = {
        query: (limit, offset, filters, sorts) => {return [];},
        columns: [],
        actions: [],
        rowActions: [],
        selectable: false, // Checkbox click individual rows and perform actions
        selectableActions: [],
        filters: null,
        search: null,
        defaultSorts: null,
        defaultFilters: null
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            limit: 60,
            offset: 0,
            filters: [],
            sorts: []
        }
    }

    render() {
        return (
            <ewars.d.Panel>
                <div className="data-table">
                    <thead>
                    <tr>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </div>
            </ewars.d.Panel>
        )
    }
}

export default Datable;