class Row extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-row">

            </div>
        )
    }
}

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-header">

            </div>
        )
    }
}

class Fieldset extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-fieldset">
                <fieldset>

                </fieldset>
            </div>
        )
    }
}

class Field extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-field">

            </div>
        )
    }
}

class Form extends React.Component {
    static defaultProps = {
        definition: [],
        data: {}
    };

    constructor(props) {
        super(props);
    }

    onChange = (prop, value) => {

    };

    render() {
        return (
            <div className="form">

            </div>
        )
    }
}

export default Form;
