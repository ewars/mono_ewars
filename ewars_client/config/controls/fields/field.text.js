class FieldText extends React.Component {
    static defaultProps = {
        name: null,
        value: null,
        multiline: false,
        password: false,
        max: null,
        min: 1,
        validations: [],
        errors: [],
        visible: true
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <input type="text"/>
        )
    }
}

export default FieldText;