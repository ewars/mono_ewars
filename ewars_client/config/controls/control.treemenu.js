import TREE_MENU from '../constants/const.tree';

import {state} from '../cmp_state';

const TYPES = {
    VIEW: 0,
    TABLE: 1,
    TAB: 2
}

class TreeNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {show: false}
    }

    _toggleShow = () => {
        this.setState({show: !this.state.show})
    };

    _onClick = () => {
        if (this.props.data.t == TYPES.TAB) {
            state.addTab(this.props.data);
        } else {
            state.setView(this.props.data);
        }
    };

    render() {
        let icon;

        if (this.props.data.i) icon = <i className={'fal ' + this.props.data.i}></i>;

        let hasChilds = this.props.data.c ? true : false;
        let caretIcon = 'fal fa-caret-right';
        if (this.state.show) caretIcon = 'fal fa-caret-down';

        return (
            <li>
                <ewars.d.Row>
                    {hasChilds ?
                        <ewars.d.Cell
                            onClick={this._toggleShow}
                            width="20px"><i className={caretIcon}></i></ewars.d.Cell>
                        : null}
                    {!hasChilds ?
                        <ewars.d.Cell width="20px">&nbsp;</ewars.d.Cell>
                        : null}
                    {icon ?
                        <ewars.d.Cell
                            onClick={this._onClick}
                            width="18">{icon}</ewars.d.Cell>
                        : null}
                    <ewars.d.Cell
                        onClick={this._onClick}
                        style={{cursor: 'pointer'}}>{this.props.data.n}</ewars.d.Cell>
                </ewars.d.Row>
                {this.props.data.c && this.state.show ?
                    <ul>
                        {this.props.data.c.map(item => {
                            return <TreeNode key={item._} data={item}/>
                        })}
                    </ul>
                    : null}
            </li>
        )
    }
}

class ControlTreeMenu extends React.Component {
    render() {
        return (
            <ewars.d.Panel>
                <ul className="config-menu" style={{marginTop: '8px', marginLeft: '8px'}}>
                    {TREE_MENU.map(item => {
                        return <TreeNode key={item._} data={item}/>
                    })}
                </ul>
            </ewars.d.Panel>
        )
    }

}

export default ControlTreeMenu;