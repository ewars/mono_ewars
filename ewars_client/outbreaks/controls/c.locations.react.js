import { LocationField } from "../../common/fields";
import LocationTreeView from "../../locations/components/LocationTreeView.react";

const ACTIONS = [
    ['fa-trash', 'REMOVE']
]

class Location extends React.Component {
    constructor(props) {
        super(props);

        this.state = {data: null};
    }

    componentDidMount() {
        ewars.tx("com.ewars.location", [this.props.data])
            .then(res => {
                this.setState({data: res});
            }).catch(err => {
                ewars.growl("Error loading location");
            });
    };

    _action = (action) => {
        this.props.onRemove(this.props.data);
    };

    render() {
        let label = "Loading...";
        if (this.state.data) {
            label = __(this.state.data.name);
            if (this.state.data.lti_name) label += " (" + __(this.state.data.lti_name) + ")";
        }

        return (
            <div className="out-loc-item">
                <div className="out-loc-label">{label}</div>
                <div className="out-loc-controls">
                    <ewars.d.ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

class LocationsEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action, data) => {
        switch (action) {
            case "CHECK":
                let locs = this.props.data || [];
                if (locs.indexOf(data.uuid) >= 0) {
                    let index = locs.indexOf(data.uuid);
                    locs.splice(index, 1);
                } else {
                    locs.push(data.uuid);
                }
                this.props.onChange(locs);
                break;
            default:
                break;
        }
    };

    _remove = (uuid) => {
        let locs = this.props.data || [];
        let index = locs.indexOf(uuid);
        locs.splice(index, 1);
        this.props.onChange(locs);
    };

    render() {
        return (
            <ewars.d.Row>
                <ewars.d.Cell borderRight={true} width="50%">
                    <LocationTreeView
                        allowCheck={true}
                        checked={this.props.data || []}
                        hideInactive={true}
                        onAction={this._action}
                        draggable={true}/>
                </ewars.d.Cell>
                <ewars.d.Cell style={{position: "relative"}}>
                    <ewars.d.Panel>
                        {this.props.data.map(item => {
                            return <Location data={item} onRemove={this._remove}/>;
                        })}

                    </ewars.d.Panel>
                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}

export default LocationsEditor;
