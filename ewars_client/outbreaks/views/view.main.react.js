import ViewData from "./view.data.react";
import ViewEditor from "./view.editor.react";

const OUTBREAK_DEFAULTS = {
    title: "New outbreak",
    description: null,
    status: "INACTIVE",
    start_date: null,
    end_date: null,
    forms: [],
    locations: []
}

class ViewMain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            outbreak: null
        }
    }

    _createNew = () => {
        this.setState({outbreak: ewars.copy(OUTBREAK_DEFAULTS)});
    };

    _close = () => {
        this.setState({outbreak: null});
    };

    _edit = (data) => {
        this.setState({outbreak: data});
    };

    render() {

        if (this.state.outbreak) {
            return <ViewEditor
                        onClose={this._close}
                        data={this.state.outbreak}/>;
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Outbreaks">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            label="Create New"
                            icon="fa-plus"
                            onClick={this._createNew}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ViewData
                            onEdit={this._edit}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewMain;
