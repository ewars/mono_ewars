import ViewMain from "./views/view.main.react";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Toolbar,
    Form,
    ActionGroup,
    Panel
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Toolbar,
    Form,
    ActionGroup,
    Panel
}

ReactDOM.render(
    <ViewMain/>,
    document.getElementById("application")
);
