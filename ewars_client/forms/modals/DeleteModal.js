import {
    Layout, Row, Cell,
    Button
} from "../../common";

import { TextField } from "../../common/fields";

class DeleteModal extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDataChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let formName = ewars.I18N(this.props.data.name);

        return (
            <Layout>
                <Row>
                    <Cell padding={14}>
                        <p>This action <strong>CANNOT</strong> be undone. This will delete the <strong>{formName}</strong> form as well as any submissions, assignments and other tertiary data..</p>
                        <p>Please type in the name of the form to confirm.</p>


                        <TextField
                            name="form_name"
                            onUpdate={this._onDataChange}
                            value={this.props.value}/>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}

export default DeleteModal;
