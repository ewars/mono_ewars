import Actions from "../actions/Actions";

export default {
    getItems: function () {
        $.ajax({
            url: "/api/Form?$inlinecount=all",
            context: this,
            dataType: "json",
            xhrFields: {
                withCredentials: true
            },
            success: function (response) {
                Actions.receiveForms(response.d);
            }
        });
    }
};
