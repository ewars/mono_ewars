/*
 * Copyright (c) 2015 JDU Software & Consulting Limited.
 * ALl Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of JDU Software &
 * Consulting Limited and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to JDU Software & Consulting Limited and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from JDU Software & Consulting Limited.
 */

// the WAMP connection to the Router
//
var when = require("when");

var Actions = require("../actions/Actions");

module.exports = {
    queryForms: function (top, limit) {
        ewars.tx("com.ewars.query", ["Form", {
            $expand: ["user", "entities"],
            $orderby: "name DESC",
            $top: top,
            $skip: limit
        }]).then(
            function (resp) {
                Actions.receiveForms(resp.d);
            }
        )
    },
    updateForm: function (data) {
        var copy = JSON.parse(JSON.stringify(data));
        delete copy.user;
        delete copy['@count'];

        ewars.tx("com.ewars.form.update", [data.id, copy])
            .then(
                function (resp) {
                    Actions.setCurrentForm(resp);
                    ewars.notifications.notification("fa-bell", "Form Updated", "The form was updated successfully");
                }
            )
    },
    saveForm: function (data) {
        ewars.tx("com.ewars.form.create", [data])
            .then(
                function (resp) {
                    Actions.setCurrentForm(resp.d);
                    ewars.notifications.notification("fa-bell", "Form Saved", "The form was saved successfully");
                }
            )
    },
    deleteForm: function (form) {
        var self = this;
        ewars.tx("com.ewars.form.delete", [form.id])
            .then(
                function (resp) {
                    ewars.notifications.notification("fa-bell", "Deleted Successfully", "The form was deleted successfully");
                    ewars.emit("GRID_REFRESH");
                }
            )
    }
};