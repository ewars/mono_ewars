export default {
    operationType: {
        order: 0,
        type: "select",
        label: "Operation Type",
        options: [
            ["INCREMENT_INDICATOR", "Increment Indicator"],
            ["DECREMENT_INDICATOR", "Decrement Indicator"],
            ["INDICATOR_ADDITION", "Add value to indicator"],
            ["INDICATOR_SUBTRACTION", "Subtract value from indicator"]
        ],
        required: true
    },
    indicatorUUID: {
        order: 1,
        type: "indicator",
        label: "Target Indicator",
        required: true
    },
    indicatorConfiguration: {
        order: 2,
        type: "group",
        fields: {}
    }
};
