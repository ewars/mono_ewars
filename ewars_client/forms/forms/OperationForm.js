export default {
    operation_type: {
        type: "select",
        label: {en: "Operation Type"},
        defaultValue: "SUM",
        options: [
            ["SUM", "Sum"],
            ["AVG", "Average"],
            ["COMPLEX", "Complex"]
        ]
    }
};
