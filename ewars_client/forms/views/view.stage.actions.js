import ViewActionSettings from "./view.action.settings";

const ACTION_CONTROLS = [
    ["fa-caret-right", "TOGGLE"],
    ["fa-copy", "COPY"]
]

const ACTION_CONTROLS_OPEN = [
    ["fa-caret-down", "TOGGLE"],
    ["fa-copy", "COPY"]
];

class Action extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        }
    }

    _action = (action) => {
        switch (action) {
            case "TOGGLE":
                this.setState({open: !this.state.open});
                break;
            default:
                break;
        }
    };

    render() {
        let actions = ACTION_CONTROLS;
        if (this.state.open) actions = ACTION_CONTROLS_OPEN;
        let style = {
            display: "none"
        };
        if (this.state.open) style.display = "block";

        return (
            <div className="action-item">
                <div className="action-header">
                    <div className="action-icon">
                        <i className="fal fa-cog"></i>
                    </div>
                    <div className="action-status">
                        <i className="fal fa-circle"></i>
                    </div>
                    <div className="action-label">
                        Some label [action type] [target_stage]
                    </div>
                    <div className="action-controls">
                        <ewars.d.ActionGroup
                            actions={actions}
                            onAction={this._action}
                            right={true}/>
                    </div>
                </div>
                <div className="action-body" style={style}>
                    <ViewActionSettings data={this.props.data} onChange={this._onSettingsChange}/>
                </div>
            </div>
        )
    }
}


class ViewStageActions extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                actions: []
            }
        }
    }

    _add = () => {
        let actions = this.state.data.actions;
        actions.push({
            uuid: ewars.utils.uuid(),
            action: "ADVANCE",
            definition: {}
        });
        this.setState({
            data: {
                actions: actions
            }
        });
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            onClick={this._add}
                            label="Add Action"
                            icon="fa-plus"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell style={{position: "relative"}}>
                        <ewars.d.Panel>
                            {(this.state.data.actions || []).map(item => {
                                return <Action data={item} form={this.props.form}/>;
                            })}
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewStageActions;
