import {
    TextField,
    SelectField,
    TextAreaField
} from "../../common/fields";

import ViewStageActions from "./view.stage.actions";

const STYLES = {
    ICON: {
        maxWidth: "30px",
        display: "block",
        padding: "8px 0px 8px 5px"
    },
    HEAD_STYLE: {
        padding: 0,
        background: "rgba(0,0,0,0.1)",
        border: "1px solid transparent"
    },
    INNER: {
        padding: 0
    }
}


const ITEM_ACTIONS = [
    ["fa-pencil", 'EDIT'],
    ["fa-trash", "DELETE"],
    ['fa-copy', "COPY"]
];

const LOCKED_STAGE_ACTIONS = [
    ['fa-pencil', 'EDIT']
]

class InitialStage extends React.Component {
    render() {
        return (
            <div className="stage-list-item" style={{background: "rgba(0, 0, 0, 0.9)"}}>
                <div className="stage-order">-</div>
                <div className="stage-label">Initial Submission</div>
                <div className="stage-controls">
                    <ewars.d.ActionGroup
                        actions={LOCKED_STAGE_ACTIONS}
                        right={true}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

class EndStage extends React.Component {
    render() {
        return (
            <div className="stage-list-item" style={{background: "rgba(0,0,0,0.9)"}}>
                <div className="stage-order">-</div>
                <div className="stage-label">End of Workflow</div>
                <div className="stage-controls">
                    <ewars.d.ActionGroup
                        actions={LOCKED_STAGE_ACTIONS}
                        right={true}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

class Stage extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch(action) {
            case "EDIT":
                this.props.onEdit(this.props.data);
                break;
            case "DELETE":
                this.props.onDelete(this.props.id);
                break;
            case "COPY":
                this.props.onCopy(this.props.data);
                break;
            default:
                break;
        }
    };

    render() {
        let order = 0,
            label = this.props.data.name;
        return (
            <div className="stage-list-item">
                <div className="stage-order">{order}</div>
                <div className="stage-label">{label}</div>
                <div className="stage-controls">
                    <ewars.d.ActionGroup
                        onAction={this._action}
                        right={true}
                        actions={ITEM_ACTIONS}/>
                </div>

            </div>
        )
    }
};

class StageSettings extends React.Component {

    render() {
        return (
            <ewars.d.Panel>
                    <div className="ide-settings-content">
                        <div className="ide-settings-form">
                            <div className="ide-section-basic">
                                <div className="header"><i className="fal fa-cog"></i>&nbsp;General</div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">Uuid</div>
                                    <div className="ide-setting-control">

                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">Stage Name</div>
                                    <div className="ide-setting-control">
                                        <TextField value={this.props.data.name} onChange={this._onChange} name="name"/>
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">Next Stage</div>
                                    <div className="ide-setting-control">
                                        <SelectField
                                            name="next_stage"
                                            config={{
                                                options: []
                                            }}
                                            value={this.props.data.next_stage}/>
                                    </div>
                                </div>

                                <div className="vsplit-box">
                                    <div className="ide-setting-label">Description</div>
                                    <div className="ide-setting-control">
                                        <TextAreaField
                                            name="description"
                                            config={{
                                                i18n: false
                                            }}
                                            value={this.props.data.description}/>
                                    </div>
                                </div>

                            </div>

                            <div className="ide-section-basic">
                                <div className="header"><i className="fal fa-code-branch"></i>&nbsp;Actions</div>
                            </div>

                            <div className="ide-section-basic">
                                <div className="header">
                                    <i className="fal fa-users"></i>&nbsp;Access
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">User Types</div>
                                    <div className="ide-setting-control">

                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">Notify</div>
                                    <div className="ide-setting-control">

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
            </ewars.d.Panel>
        )
    }
}

class StageEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT"
        }
    }

    render() {

        let view;

        if (this.state.view == "DEFAULT") view = <StageSettings data={this.props.data} onChange={this._onChange}/>;
        if (this.state.view == "ACTIONS") view = <ViewStageActions data={this.props.data}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} style={{position: "relative", overflow: "hidden"}} width="34px">
                        <div className="ide-tabs">
                            <div className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}
                                onClick={() => this.setState({view: "GENERAL"})}>
                                <i className="fal fa-cog"></i>&nbsp;General
                            </div>
                            <div className={"ide-tab" + (this.state.view == "ACTIONS" ? " ide-tab-down" : "")}
                                onClick={() => this.setState({view: "ACTIONS"})}>
                                <i className="fal fa-code-branch"></i>&nbsp;Actions
                            </div>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}



class ViewStages extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            definition: props.data.stages || {},
            stage: null
        }
    }

    _addStage = () => {
        let stages = this.state.definition || {};
        let new_id = ewars.utils.uuid();

        stages[new_id] = {
            uuid: new_id,
            name: "New Stage",
            next: "END",
            actions: []
        };

        this.setState({
            definition: stages,
            stage: stages[new_id]
        });
    };

    _edit = (data) => {
        this.setState({
            stage: data
        });
    };

    _delete = (uuid) => {
        if (window.confirm("Are you sure you want to delete this stage?")) {
            let stages = this.state.definition;
            delete stages[uuid];
            this.setState({definition: stages});
        }
    };

    _copy = (data) => {
        let stages = this.state.definition;
        let newUuid = ewars.utils.uuid();
        stages[newUuid] = ewars.copy(data);
        stages[newUuid].name += " Copy";
        this.setState({definition: stages});
    };

    render() {

        let stages = [];

        for (let i in this.state.definition) {
            stages.push(
                <Stage
                    onDelete={this._delete}
                    onEdit={this._edit}
                    onCopy={this._copy}
                    data={this.state.definition[i]} id={i}/>
            );
        }

        let stage;
        if (this.state.stage) {
            stage = (
                <StageEditor data={this.state.stage} onChange={this._onChange}/>
            );
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="30%">
                        <ewars.d.Toolbar>
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    label="Add Stage"
                                    icon="fa-plus"
                                    onClick={this._addStage}/>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row style={{display: "block", position: "relative"}}>
                            <ewars.d.Panel>
                                <InitialStage/>
                                {stages}
                                <EndStage/>
                            </ewars.d.Panel>
                        </ewars.d.Row>
                    </ewars.d.Cell>
                    <ewars.d.Cell style={{position: "relative"}}>
                        <ewars.d.Panel>
                            {stage}
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewStages;
