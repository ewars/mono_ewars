class TargetForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Cell style={{display: "flex"}}>
                <ewars.d.Toolbar label="Target">

                </ewars.d.Toolbar>
            </ewars.d.Cell>
        )
    }
}

class SourceForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Cell width="50%" borderRight={true}>
                <ewars.d.Toolbar label="Source">

                </ewars.d.Toolbar>

            </ewars.d.Cell>
        )
    }
}

class FormMapper extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            forms: []
        }

        ewars.tx('com.ewars.forms.active', [])
            .then(res => {
                console.log(res);
                this.setState({
                    forms: res
                })
            })
            .catch(err => {
                ewars.error("Could not retrieve forms");
            })
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>
                    <div className="select-custom">

                        <select>
                            <option value="">None target form selected</option>
                            {this.state.forms.map(item => {
                                return (
                                    <option value={item.id}>{item.name.en || item.name}</option>
                                )
                            })}
                        </select>
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <SourceForm/>
                    <TargetForm/>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default FormMapper;