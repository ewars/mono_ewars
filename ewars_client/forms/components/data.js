import DataGrid from "../../common/datagrid/c.datagrid";

const STATUS_OPTIONS = [
    ["ACTIVE", "Active"],
    ["DRAFT", "Draft"]
];

const COLUMNS = [
    {name: "name", fmt: ewars.__, label: "Name", type: "text"},
    {name: "status", label: "Status", type: "select", options: STATUS_OPTIONS},
    {name: "last_modified", label: "Modified", type: "date", fmt: "YYYY-MM-DD HH:mm:ss"},
    {name: "created", label: "Created", type: "date", fmt: "YYYY-MM-DD HH:mm:ss"}
]

class DataView extends React.Component {
    componentDidMount() {
        this.refs['dg'].addEventListener("recordselection", this._handleRecordSelect);
    }

    componentWillUnmount() {
        this.refs['dg'].removeEventListener("recordselection", this._handleRecordSelect);
    }

    _handleRecordSelect = (e) => {
        console.log("FIRED", e);
    };

    render() {
        return <data-grid
            columns={JSON.stringify(COLUMNS)}
            ref="dg"/>;
    }
}

export default DataView;
