import CONSTANTS from "../../../common/constants";

import GeneralPanel from "./panels/GeneralPanel.react";
import Features from "./panels/Features.react";
import GuidanceEditor from "./GuidanceEditor.react";

const VIEWS = {
    GENERAL: "GENERAL",
    FEATURES: "FEATURES",
    GUIDANCE: "GUIDANCE",
    INTEGRATIONS: "INTEGRATIONS"
};

var SettingsSection = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-settings-section";
        if (this.props.active) className += " ide-settings-section-active";
        var iconClass = "fa fa-caret-right";
        if (this.props.active) iconClass = "fa fa-caret-down";

        var subs = [];
        if (this.props.subs) {
            subs = this.props.subs.map(function (sub) {
                return <div className="section-item">{sub}</div>
            })
        }

        var hasSubs = subs.length > 0;

        return (
            <div className={className}>
                <div className="section-header" onClick={this._onClick}>
                    <div className="section-title">{this.props.label}</div>
                </div>
                {hasSubs ?
                    <div className="section-items">
                        {subs}
                    </div>
                    : null}
            </div>
        )
    }
});

var GeneralSettingsViewComponent = React.createClass({
    getInitialState: function () {
        return {
            view: "GENERAL"
        }

    },

    _changeSection: function (data) {
        this.setState({
            view: data
        })
    },

    _onChange: function(prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {


        let view;

        if (this.state.view == VIEWS.GENERAL) {
            view = <GeneralPanel
                onChange={this._onChange}
                errors={this.props.errors}
                data={this.props.data}/>;
        }

        if (this.state.view == VIEWS.GUIDANCE) {
            view = <GuidanceEditor
                onUpdate={this._onFormUpdate}
                data={this.props.data}/>;
        }

        if (this.state.view == VIEWS.FEATURES) {
            view = <Features data={this.props.data}
                             errors={this.props.errors}
                             onChange={this._onChange}/>;
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }

});

export default GeneralSettingsViewComponent;
