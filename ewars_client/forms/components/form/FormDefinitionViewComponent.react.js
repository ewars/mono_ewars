import FormDefinitionComponent from "../definition/FormDefinitionComponent.react";

var FormDefinitionViewComponent = React.createClass({
    getInitialState: function () {
        return {
            form: {},
            definition: null
        };
    },

    componentWillMount: function () {
        this.state.form = JSON.parse(JSON.stringify(this.props.data));
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.form = JSON.parse(JSON.stringify(nextProps.data));
    },


    definitionChange: function (definition) {
        ewars.store.dispatch({type: "SET_FORM_DEFINITION", data: definition})
    },

    render: function () {
        return (
            <FormDefinitionComponent
                definition={this.props.data.version.definition}
                form={this.props.data}
                onChange={this.definitionChange}/>
        )
    }
});

export default FormDefinitionViewComponent;
