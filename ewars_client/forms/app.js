import Component from "./components/Component.react";
import {combineReducers, createStore} from "redux";
import {
    Shade,
    Modal,
    Layout,
    Cell,
    Row,
    Panel,
    Button,
    Toolbar,
    ActionGroup,
    Form
} from "../common";

ewars.d = {
    Form,
    Shade,
    Modal,
    Layout,
    Cell,
    Row,
    Panel,
    Button,
    Toolbar,
    ActionGroup
};

import reducers from "./reducers";

ewars.store = createStore(reducers);

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


