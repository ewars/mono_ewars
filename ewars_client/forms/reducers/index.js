import {combineReducers} from "redux";

import form from "./form";
import errors from "./errors";


export default combineReducers({
    form,
    errors
})
