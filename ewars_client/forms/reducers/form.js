import {
    SET_FORM_PROP,
    SET_FORM,
    SET_FEATURE_PROP,
    ADD_FEATURE,
    REMOVE_FEATURE,
    SET_FORM_DEFINITION,
    SET_FORM_LOGIC,
    UPDATE_CIRCUIT,
    REMAP_CIRCUIT,
    CIRCUIT_REMOVE_ITEM,
    CIRCUIT_UPDATE_REDUCTION,
    SET_CHANGE_RULE,
    ADD_SET,
    SET_ADD_FIELD,
    REMOVE_SET,
    UPDATE_SET_ITEM
} from "../actions";

const DEFAULT = {
    id: null,
    version: {},
    features: {}
};

function addSet(data, id, path) {
    let item = data.version.etl[id].n;
    for (var i in String(path).split(".")) {
        let idx = String(path).split(".")[i];
        item = item[idx];
    }
    item.push(["ALL"]);
    return data;
}

function addField(data, id, path) {
    let item = data.version.etl[id].n;
    for (var i in String(path).split(".")) {
        let idx = String(path).split(".")[i];
        item = item[idx];
    }
    let newId = ewars.utils.uuid().split('-')[4];
    item.push(`null:EQ:null:${newId}`);
    return data;
}

function removeSet(data, id, orgPath) {
    let item = data.version.etl[id].n;
    let path = String(orgPath).split(".");
    let lastIndex = path.pop();

    for (var i in path) {
        let idx = path[i];
        item = item[idx];
    }
    item.splice(lastIndex, 1);

    return data;
}

function updateSetItem(data, id, nid, orgPath, prop, value) {
    console.log(prop, value);

    if (nid) {
        data.version.etl[id].n.forEach((itemSet, index) => {
            if (Array.isArray(itemSet)) {
                itemSet.forEach((item, subIndex) => {
                    if ((item.split(':')[3] || null) == nid) {
                        let changeIndex = subIndex;
                        let field = item.split(':');

                        switch (prop) {
                            case 'field':
                                field[0] = value;
                                break;
                            case 'operator':
                                field[1] = value;
                                break;
                            case 'value':
                                if (Array.isArray(value)) {
                                    value = value.join(',')
                                }
                                field[2] = value;
                                break;
                            default:
                                break;
                        }

                        if (changeIndex != null && changeIndex != undefined) {
                            data.version.etl[id].n[index][subIndex] = field.join(':');
                        }
                    }
                })
            }
        })

    }


    return data;
}

function updateRootItem(data, id, index, prop, value) {
    console.log(data, id, index, prop, value);
    let item = data.version.etl[id].n;
    let circuit = item[index].split(":");

    if (prop == "field") circuit[0] = value;
    if (prop == "operator") circuit[1] = value;
    if (prop == "value") circuit[2] = value;

    data.version.etl[id].n[index] = circuit.join(":");

    return data;
}

function moveItemUp(data, index) {
    let asArray = [];

    for (var p in data.version.etl) {
        asArray.push({
            ...data.version.etl[p],
            i: p
        })
    }

    asArray.forEach((item) => {
        if (item.o == undefined || item.o == null) {
            item.o = -1;
        }
    });

    asArray = asArray.sort((a, b) => {
        if (a.o > b.o) return 1;
        if (a.o < b.o) return -1;
        return 0;
    });

    let counter = 0;
    asArray.forEach((item) => {
        item.o = counter;
        counter++;
    });

    let tmp = asArray[index];
    asArray[index] = asArray[index - 1];
    asArray[index - 1] = tmp;

    counter = 0;
    asArray.forEach((item) => {
        item.o = counter;
        counter++;
    });

    let etl = {};
    asArray.forEach((item) => {
        etl[item.i] = item;
        delete etl[item.i].i;
    });

    data.version.etl = etl;

    console.log(data);
    return ewars.copy({...data});


}

function moveItemDown(data, index) {
    let asArray = [];

    for (var p in data.version.etl) {
        asArray.push({
            ...data.version.etl[p],
            i: p
        })
    }

    asArray.forEach((item) => {
        if (item.o == undefined || item.o == null) {
            item.o = -1;
        }
    });

    asArray = asArray.sort((a, b) => {
        if (a.o > b.o) return 1;
        if (a.o < b.o) return -1;
        return 0;
    });

    // Set sane defaults for ordering
    let counter = 0;
    asArray.forEach((item) => {
        item.o = counter;
        counter++;
    });

    // Do the swap
    let tmp = asArray[index];
    asArray[index] = asArray[index + 1];
    asArray[index + 1] = tmp;

    // Re-set ordering values after shuffle
    counter = 0;
    asArray.forEach((item) => {
        item.o = counter;
        counter++;
    });

    let etl = {};
    asArray.forEach((item) => {
        etl[item.i] = item;
        delete etl[item.i].i;
    });

    data.version.etl = etl;

    return ewars.copy({...data});
}

const form = (state = DEFAULT, action) => {
    let data = ewars.copy(state);
    switch (action.type) {
        case SET_FORM_PROP:
            return Object.assign({}, state, {
                [action.prop]: action.value
            });
        case SET_FORM:
            // fix for logic missing ids
            if (action.data.version) {
                if (action.data.version.etl != null) {
                    for (let ind in action.data.version.etl) {

                        action.data.version.etl[ind].n.forEach((item, index) => {
                            if (Array.isArray(item)) {
                                // this is a complex block
                                item.forEach((fieldNode, fieldNodeIndex) => {
                                    if (fieldNode.indexOf(':') >= 0) {
                                        if ((fieldNode.match(/:/g) || []).length == 2) {
                                            item[fieldNodeIndex] = `${fieldNode}:${ewars.utils.uuid().split('-')[4]}`;
                                        }
                                    }
                                })

                            }
                        })
                    }
                }
            }
            return Object.assign({}, action.data);
        case SET_FEATURE_PROP:
            data.features[action.id][action.prop] = action.value;
            return data;
        case ADD_FEATURE:
            if (action.boolean) {
                data.features[action.id] = true;
            } else {
                data.features[action.id] = {};
            }
            return data;
        case REMOVE_FEATURE:
            if (data.features[action.id]) delete data.features[action.id];
            return data;
        case SET_FORM_DEFINITION:
            data.version.definition = action.data;
            return data;
        case SET_FORM_LOGIC:
            console.log(data);
            data.version.etl = action.data;
            return data;
        case UPDATE_CIRCUIT:
            data.version.etl[action.id] = action.data;
            return data;
        case REMAP_CIRCUIT:
            data.version.etl[action.new] = data.version.etl[action.old];
            delete data.version.etl[action.old];
            return data;
        case CIRCUIT_REMOVE_ITEM:
            data.version.etl[action.id].n.splice(action.index, 1);
            return data;
        case CIRCUIT_UPDATE_REDUCTION:
            data.version.etl[action.id].r = action.data;
            return data;
        case SET_CHANGE_RULE:
            let item = data.version.etl[action.id].n;
            for (var i in String(action.path).split(".")) {
                let idx = String(action.path).split(".")[i];
                item = item[idx];
            }
            item[0] = action.value;
            return data;
        case ADD_SET:
            return addSet(data, action.id, action.path);

        case SET_ADD_FIELD:
            return addField(data, action.id, action.path);
        case REMOVE_SET:
            return removeSet(data, action.id, action.path);
        case UPDATE_SET_ITEM:
            return updateSetItem(
                data,
                action.id,
                action.nid,
                action.path,
                action.prop,
                action.value
            );
        case "UPDATE_ROOT_ITEM":
            return updateRootItem(
                data,
                action.id,
                action.index,
                action.prop,
                action.value
            );
        case "MOVE_UP":
            return moveItemUp(data, action.id);
        case "MOVE_DOWN":
            return moveItemDown(data, action.id);
        default:
            return state
    }
};

export default form;
