import Dispatcher from "../../CoreDispatcher";
import { EventEmitter } from "events";
import Constants from "../constants/Constants";
import assign from "object-assign";

import WebSocketService from "../utils/WebSocketService";

const CHANGE_EVENT = "change";

var _form = {};

function _setForm(form) {
    _form = form;
    if (!_form.version) _form.version = {logic: [], definition: {}};
}

function _updateFormDefinition(definition) {
    _form.version.definition = definition;
}

function _setFormProperty(name, value) {
    if (name == "definition") {
        _form.version.definition = value;
    } else if (name == "version") {
        _form.version.version = value;
        _form.version.uuid = null;
    } else {
        _form[name] = value;
    }
}

function _setFormFieldProperty(fieldName, prop, value) {
    _form.version.definition[fieldName][prop] = value;
    if (prop == "label") {
        // We need to change the field name
        var newName = value.toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '_');

        _form.version.definition[newName] = JSON.parse(JSON.stringify(_form.version.definition[fieldName]));
        delete _form.version.definition[fieldName];
    }
}

function _updateLogic(logic, dia) {
    _form.version.logic = logic;
    _form.version.dia = dia;
}

function _deleteForm(form) {
    WebSocketService.deleteForm(form);
}

function _updateVersion(version) {
    _form.version = version;
}

function _updateDefinition (definition) {
    _form.version.definition = definition;
}

function _updateLogic (logic, dia) {
    _form.version.logic = logic;
    _form.version.dia = dia;
}

var FormStore = assign({}, EventEmitter.prototype, {

    getForm: function () {
        return _form;
    },

    /**
     * Checks to see if the form has a specific entity set on it
     * @param uuid
     * @returns {boolean}
     */
    hasEntity: function (uuid) {
        if (!_form.entities) return false;
        if (_form.entities) {
            if (_form.entities.indexOf(uuid) >= 0) return true;
            return false;
        }
        return false;
    },

    /**
     * Change emitter
     */
    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * Add a change listener to the collection
     * @param callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * Remove a change listener from the collection
     * @param callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

Dispatcher.register(function (payload) {
    var action = payload.action;

    switch (action.actionType) {
        case Constants.RECEIVE_FORM:
            _setForm(action.data);
            break;
        case Constants.RECEIVE_FORM_DEFINITION_UPDATE:
            break;
        case Constants.RECEIVE_OPERATION_DUPLICATE:
            _addDuplicate(action.data);
            break;
        case Constants.SET_CURRENT_FORM:
            _setForm(action.data);
            break;
        case Constants.RECEIVE_FORM_PROP_UPDATE:
            _setFormProperty(action.data[0], action.data[1]);
            break;


        case Constants.DELETE_FORM:
            _deleteForm(action.data);
            break;

        case Constants.RECEIVE_LOGIC_UPDATE:
            _updateLogic(action.data.logic, action.data.dia);
            break;
        case Constants.RECEIVE_VERSION_SET:
            _updateVersion(action.data);
            break;
        case Constants.RECEIVE_FORM_DEFINITION:
            _updateDefinition(action.data);
            break;
        case Constants.RECEIVE_LOGIC_DEFINITION:
            _updateLogic(actions.data.logic, action.data.dia);
            break;
        default:
            return true;
    }

    FormStore.emitChange();
    return true;
});

module.exports = FormStore;
