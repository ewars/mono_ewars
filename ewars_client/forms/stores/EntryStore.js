import Dispatcher from "../../common/CoreDispatcher";
import { EventEmitter } from "events";
import Constants from "../constants/Constants";
import assign from "object-assign";

const CHANGE_EVENT = "change";

import WebSocketSerive from "../utils/WebSocketService";

var _entry = {
    definition: {}
};
var _errors = [];

function _setField(name, value) {
    _entry[name] = value;
}

function _setEntry(data) {
    _entry = data;
}

function _setErrors(errors) {
    _errors = errors;
}

function _saveDraft() {
    if (_entry.uuid) {
        WebSocketService.updateDraft(_entry);
    } else {
        WebSocketService.saveNewDraft(_entry);
    }
}

function _clearLogic() {
    _entry.logic = [];
}

var EntryStore = assign({}, EventEmitter.prototype, {
    /**
     * Return the collection
     * @returns {{}}
     */
    getEntry: function () {
        return _entry;
    },

    getErrors: function () {
        return _errors;
    },

    setEntry: function (record) {
        _entry = record;
    },

    /**
     * Change emitter
     */
    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * Add a change listener to the collection
     * @param callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * Remove a change listener from the collection
     * @param callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

Dispatcher.register(function (payload) {
    var action = payload.action;

    switch (action.actionType) {
        case Constants.RECEIVE_ENTRY:
            _setEntry(action.data);
            break;
        case Constants.RECEIVE_ENTRY_UPDATE:
            _setField(action.data.prop, action.data.value);
            break;
        case Constants.RECEIVE_FORM_DEFINITION_UPDATE:
            _setField("definition", action.data);
            break;
        case Constants.SAVE_CURRENT_DRAFT:
            _saveDraft();
            break;
        case Constants.SAVE_NEW_DRAFT:
            _saveDraft();
            break;
        case Constants.SET_ENTRY_EDIT:
            _setEntry(action.data);
            break;
        case Constants.CLEAR_LOGIC:
            _clearLogic();
            break;
        default:
            return true;
    }

    EntryStore.emitChange();

    return true;
});

export default EntryStore;
