import Component from "./components/Component.react";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Panel,
    Form,
    Toolbar,
    Button
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Panel,
    Form,
    Toolbar,
    Button
}

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


