var CompletenessDefinition = React.createClass({
    render: function () {
        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col" style={{maxWidth: "33%", padding: 16}}>
                        <h1 style={{marginBottom: 8}}><strong>Formula</strong></h1>
                        <p><strong>Completeness of reporting</strong> is calculated as:</p>
                        <br />
                        <table>
                            <tbody>
                            <tr>
                                <td>
                                    Number of reports submitted<br />
                                    <hr />
                                    Number of reports expected
                                </td>
                                <td style={{verticalAlign: "middle", paddingLeft: 8}}>
                                    x 100%
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="ide-col" style={{maxWidth: "33%", padding: 16}}>
                        <h3 style={{marginBottom: 8}}><strong>Standard</strong></h3>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td className="red legend-cell">&lt; 60%</td>
                                <td className="red legend-cell">Poor</td>
                            </tr>
                            <tr>
                                <td className="yellow legend-cell">&ge; 60% &lt; 80%</td>
                                <td className="yellow legend-cell">Fair</td>
                            </tr>
                            <tr>
                                <td className="orange legend-cell">&ge; 80% &lt; 100%</td>
                                <td className="orange legend-cell">Good</td>
                            </tr>
                            <tr>
                                <td className="green legend-cell">100%</td>
                                <td className="green legend-cell">Excellent</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="ide-col" style={{padding: 16}}>
                        <h3 style={{marginBottom: 8}}><strong>Source</strong></h3>
                        <p>WHO EWARS Project</p>
                    </div>
                </div>
            </div>
        )
    }
});

export default CompletenessDefinition;
