import Moment from "moment";
import {
    Button,
    Spinner
} from "../../common";
import CONSTANTS from "../../common/constants";

const METRICS = {
    COMPLETENESS: {
        format: function (value) {
            let perc;
            if (value[1].s <= 0 && value[1].e <= 0) {
                perc = "0%";
            } else if (value[0].s == 0 && value[1].s > 0) {
                perc = "0%";
            } else {
                perc = ewars.NUM(value[1].s / value[1].e, "0%");
            }

            return value[1].s + " / " + value[1].e + " (" + perc + ")";
        },
        getColor: function (dataPoint) {
            var perc = (dataPoint[1].s / dataPoint[1].e) * 100;
            if (perc < 60) return "red";
            if (perc >= 100) return "green";
            if (perc >= 60 && perc < 80) return "yellow";
            if (perc >= 80 && perc < 100) return "orange";
        }
    },
    TIMELINESS: {
        format: function (value) {
            let dt = value[1];

            let perc = "0%";
            perc = ewars.NUM(dt[0] / dt[1], "%");
            if (dt[1] <= 0) perc = "0%";

            return `${dt[0]} / ${dt[1]} (${perc})`;
        },
        getColor: function (value) {
            let dt = value[1];

            let perc = dt[0] / dt[1] * 100;
            if (perc < 60) return "red";
            if (perc >= 100) return "green";
            if (perc >= 60 && perc < 80) return "yellow";
            if (perc >= 80 && perc < 100) return "orange";
        }
    }
};

var Row = React.createClass({
    render: function () {
        var formatFn = METRICS[this.props.metric].format;
        var colourFn = METRICS[this.props.metric].getColor;

        let cells = this.props.headers.map(item => {
            var view = "N/A";
            var colorClass = "grey";
            if (this.props.values) {
                let value= this.props.values.find(val => {
                    return (val[0] == item);
                });
                if (value && value[1]) view = formatFn(value);
                if (value && value[1]) colorClass = colourFn(value);
            }
            return <td className={colorClass}>{view}</td>
        })

        return (
            <tr>
                {cells}
            </tr>
        )
    }
});

var recursiveRowCreator = function (locations, data, vis, metric, headers) {
    var rows = [];
    for (let i in locations) {
        let location = locations[i];
        location.s_lineage = location.lineage.join(",") || location.uuid;
        if (!location.parent_id || location.uuid == window.user.location_id) rows.push(location);
        if (location.parent_id && vis[location.uuid] && location.uuid != window.user.location_id) rows.push(location);
    }
    rows = rows.sort((a, b) => {
        if (a.s_lineage > b.s_lineage) return 1;
        if (a.s_lineage < b.s_lineage) return -1;
        return 0;
    });

    let results = rows.map(row => {
        return <Row metric={metric} headers={headers} data={row} values={data[row.uuid]}/>;
    });

    return results;
};

var LocationTree = React.createClass({
    getInitialState: function () {
        return {
            showChildren: false,
            children: null
        }
    },

    _onClick: function () {
        this.state.showChildren = !this.state.showChildren;

        if (!this.state.children) {
            ewars.tx("com.ewars.query", ["location", ["uuid", "name", "parent_id", "site_type_id", "lineage", "@children"], {
                parent_id: {eq: this.props.data.uuid},
                status: {eq: 'ACTIVE'}
            }, null, null, null, ["location_type:site_type_id:id"]])
                .then(function (resp) {
                    this.state.children = resp;
                    // this.forceUpdate();
                    this.props.addLocations(this.props.data.uuid, resp);
                }.bind(this))
        } else {
            this.props.onToggle(this.props.data.uuid, this.state.showChildren);
        }
    },

    getRows: function () {
        return [0];
    },

    render: function () {
        var locationName = ewars.I18N(this.props.data.name);
        locationName += " (" + ewars.I18N(this.props.data.location_type.name) + ")";
        var relName = locationName;
        if (locationName.length > 28) locationName = locationName.substr(0, 28) + "...";

        var children;
        if (this.state.showChildren && this.state.children) {
            children = this.state.children.sort((a, b) => {
                let aO = a.lineage.join(","),
                    bO = b.lineage.join(",");
                if (aO > bO) return 1;
                if (aO < bO) return -1;
                return 0;
            });

            children = children.map(child => {
                var path = this.props.path + "." + child.uuid;
                return <LocationTree
                    key={child.uuid}
                    data={child}
                    path={path}
                    onToggle={this.props.onToggle}
                    addLocations={this.props.addLocations}/>;
            });
        }

        var handleIcon = "fal fa-plus-square";
        if (this.state.showChildren) handleIcon = "fal fa-minus-square";

        var hasChildren = false;
        if (this.props.data.children > 0) hasChildren = true;

        return (
            <tbody>
            <tr>
                <th width="12px">
                    {hasChildren ?
                        <div className="handle" onClick={this._onClick}>
                            <i className={handleIcon}></i>
                        </div>
                        :
                        <div className="handle">
                            <i className="fal fa-map-marker"></i>
                        </div>
                    }
                </th>

                <th style={{textAlign: 'left'}} title={relName}>{locationName}</th>
            </tr>
            {this.state.showChildren ?
                <tr>
                    <td colSpan={2} style={{padding: 0}}>
                        <table width="100%" className="info-list row-click" style={{borderLeft: "18px solid #CCCCCC"}}>
                            {children}
                        </table>
                    </td>
                </tr>
                : null}
            </tbody>
        )
    }
});

var AuditTableView = React.createClass({
    _isLoading: false,

    getInitialState: function () {
        return {
            data: {},
            locations: {},
            location: null,
            path: [],
            visibility: {},
            start_date: '2016-01-01',
            child_map: {},
            vis: {},
            end_date: Moment().toDate()
        }
    },

    componentWillMount: function () {
        var query = {
            status: {eq: "ACTIVE"}
        };

        if ([CONSTANTS.SUPER_ADMIN, CONSTANTS.INSTANCE_ADMIN, CONSTANTS.GLOBAL_ADMIN].indexOf(window.user.role) >= 0) query.parent_id = {eq: "NULL"};
        if (window.user.role == CONSTANTS.ACCOUNT_ADMIN) query.uuid = {eq: window.user.clid};
        if (window.user.role == CONSTANTS.REGIONAL_ADMIN) query.uuid = {eq: window.user.location_id};

        ewars.tx("com.ewars.query", ["location", ["uuid", "name", "lineage", "parent_id", "site_type_id", "@children"], query, null, null, null, ["location_type:site_type_id:id"]])
            .then(function (resp) {
                resp.forEach(location => {
                    this.state.locations[location.uuid] = location;
                    this.state.visibility[location.uuid] = true;
                });
                this.forceUpdate();
            }.bind(this))
    },

    componentWillReceiveProps: function (nextProps) {
        if (this.props.form_id != nextProps.form_id) {
            if (nextProps.form_id) {
                this.state.data = {};
                this._reload(nextProps, Object.keys(this.state.locations).length, this.state.locations, this.state.child_map, this.state.vis);
            }
        }

        if (nextProps.metric != this.props.metric) {
            if (nextProps.metric) {
                this.state.data = {};
                this._reload(nextProps, Object.keys(this.state.locations).length, this.state.locations, this.state.child_map, this.state.vis);
            }
        }

        if (nextProps.start_date != this.props.start_date || nextProps.end_date != this.props.end_date) {
            this.state.data = {};
            this._reload(nextProps, Object.keys(this.state.locations).length, this.state.locations, this.state.child_map, this.state.vis);
        }
    },

    _reload: function (props, locationCount, locations, child_map, vis) {

        if (!props.form_id) return;
        if (!props.start_date) return;
        if (!props.end_date) return;

        var locationsLoaded = 0;
        let blocker;
        if (locationCount != null) {
            blocker = new ewars.Blocker(null, "Loading data...");
        }

        let data = this.state.data;
        if (props.metrics != this.props.metric && props.metric) data = {};
        for (let i in locations) {
            let location = locations[i];
            if (!data[location.uuid]) {
                ewars.tx("com.ewars.performance",
                    [props.metric, location.uuid, props.start_date.format("YYYY-MM-DD"), props.end_date.format("YYYY-MM-DD"), props.form_id])
                    .then((resp) => {
                        if (blocker) blocker.destroy();
                        data[location.uuid] = resp;
                        locationsLoaded++;
                        if (locationsLoaded >= locationCount) {
                            //blocker.destroy();
                            this.setState({
                                data: data,
                                locations: locations,
                                child_map: child_map,
                                vis: vis
                            })
                        }
                    })
            }
        }
    },

    _addLocations: function (parent_id, additions) {
        let locations = this.state.locations,
            child_map = this.state.child_map,
            vis = this.state.vis;
        additions.forEach((location) => {
            locations[location.uuid] = location;
            child_map[parent_id] = location;
            vis[location.uuid] = true;
        });
        this._reload(this.props, additions.length, locations, child_map, vis);
    },

    _toggleShown: function (parent_id, visibility) {
        this.state.locations.forEach(location => {
            if (!visibility) {
                if (location.lineage.indexOf(parent_id) >= 0) {
                    if (location.uuid != parent_id) this.state.vis[location.uuid] = visibility;
                }
            } else {
                if (location.parent_id == parent_id) this.state.vis[location.uuid] = visibility;
            }
        });
        this.forceUpdate();
    },

    _export: function () {
        var blocker = new ewars.Blocker(null, "Exporting data...");

        var export_data = "data:text/csv;charset=utf-8,";
        var cols = ["parent_id", "location", "lineage"];
        var row = [];

        var joiner = ",";

        // export

        let sets = [];
        for (let i in this.state.locations) {
            let dates = this.state.data[i].map(dataSet => {
                return dataSet[0];
            });
            sets.push.apply(sets, dates);
        }

        let rowHeaders = [...new Set(sets)];
        cols.push.apply(cols, rowHeaders);

        var rows = [cols];
        var dataRows = [];
        var formatFn = METRICS[this.props.metric].format;

        for (let i in this.state.locations) {
            let location = this.state.locations[i];
            if (this.state.vis[i] || this.state.vis[i] == undefined) {

                var pre_line = "";
                if (location.lineage.length > 1) {
                    pre_line = location.lineage.map(() => {
                        return "-"
                    });
                    pre_line = pre_line.join("");
                }

                var row = [location.parent_id, pre_line + " " + ewars.I18N(location.name), location.lineage.join("-")];
                var data = this.state.data[location.uuid];

                let cells = rowHeaders.map(item => {
                    var view = "N/A";
                    if (data) {
                        let value = data.find(value => {
                            return (value[0] == item);
                        });
                        if (value && value[1]) view = formatFn(value);
                    }
                    return view
                });

                row.push.apply(row, cells);
                dataRows.push(row);
            }
        };


        dataRows = dataRows.sort(function (a, b) {
            if (a[2] < b[2]) return -1;
            if (a[2] > b[2]) return 1;
            return 0;
        });

        rows.push.apply(rows, dataRows);

        rows.forEach(row => {
            export_data += row.join(joiner) + "\n";
        });

        var encoded = encodeURI(export_data);

        window.open(encoded);

        blocker.destroy();
    },

    render: function () {

        let rootLocations = [];
        for (let i in this.state.locations) {
            let loc = this.state.locations[i];
            if (!loc.parent_id || loc.uuid == window.user.location_id) rootLocations.push(loc);
        }
        rootLocations = rootLocations.sort((a, b) => {
            let aO = a.lineage.join(","),
                bO = b.lineage.join(",");
            if (aO > bO) return 1;
            if (aO < bO) return -1;
            return 0;
        });

        var sets = [];
        for (let i in this.state.locations) {
            let loc = this.state.locations[i];
            let dates = (this.state.data[loc.uuid] || []).map(dataSet => {
                return dataSet[0];
            });
            sets.push.apply(sets, dates);
        }

        let rawHeaders = [...new Set(sets)];
        let rows = rootLocations.map(location => {
            return <Row headers={rawHeaders} key={location.uuid} data={location}
                        values={this.state.data[location.uuid]}/>;
        });

        var count = -1;
        let rowHeaders = rootLocations.map(location => {
            return <LocationTree
                key={location.uuid}
                onToggle={this._toggleShown}
                addLocations={this._addLocations}
                path={location.uuid}
                data={location}/>
        });

        let cellHeaders = rawHeaders.map(item => {
            return (
                <th>{ewars.DATE(item, this.props.interval)}</th>
            )
        });

        rows = recursiveRowCreator(this.state.locations, this.state.data, this.state.vis, this.props.metric, rawHeaders);
        console.log(rows);

        var view;
        if (!this.props.form_id) {
            view = (
                <div className="placeholder" style={{height: "auto", padding: "16px 0"}}>Please select a form.</div>
            )
        } else {
            view = (
                <table width="100%" className="info-list row-click">
                    <thead>
                    <tr>
                        {cellHeaders}
                    </tr>
                    </thead>
                    <tbody>
                    {rows}
                    </tbody>
                </table>
            )
        }

        return (
            <div className="widget">
                <div className="widget-header">
                    <span>Results</span>
                    <div className="btn-group pull-right" style={{marginTop: 5}}>
                        <ewars.d.Button
                            icon="fa-download"
                            label="Export"
                            onClick={this._export}/>
                    </div>
                </div>
                <div className="body no-pad">

                    <table width="100%" className="pivot-wrapper">
                        <tbody>
                        <tr>
                            <td width="300" style={{position: "relative"}}>
                                <div className="pivot-row-headers">
                                    <table width="100%" height="100%" className="info-list row-click">
                                        <thead>
                                        <tr>
                                            <th width="150px" colSpan="2">Location Name</th>
                                        </tr>
                                        </thead>
                                        {rowHeaders}
                                    </table>
                                </div>
                            </td>
                            <td style={{position: "relative"}}>
                                <div className="pivot-scroll">
                                    {view}
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }
});

export default AuditTableView;
