import Moment from "moment";
import CONSTANTS from "../../common/constants";
import {
    Spinner,
    Button
} from "../../common";

const METRICS = {
    COMPLETENESS: {
        format: function (value) {
            let perc;
            if (value[1].s <= 0 && value[1].e <= 0) {
                perc = "0%";
            } else if (value[0].s == 0 && value[1].e > 0) {
                perc = "0%";
            } else {
                perc = ewars.NUM(value[1].s / value[1].e, "0%");
            }

            return value[1].s + " / " + value[1].e + " (" + perc + ")";
        },
        getColor: function (dataPoint) {
            var perc = (dataPoint[1].s / dataPoint[1].e) * 100;
            if (perc < 60) return "red";
            if (perc >= 100) return "green";
            if (perc >= 60 && perc < 80) return "yellow";
            if (perc >= 80 && perc < 100) return "orange";
        }
    },
    TIMELINESS: {
        format: function (value) {
            let dt = value[1];

            let perc = "0%";
            perc = ewars.NUM(dt[0] / dt[1], "%");
            if (dt[1] <= 0) perc = "0%";

            return `${dt[0]} / ${dt[1]} (${perc})`;
        },
        getColor: function (value) {
            let dt = value[1];

            let perc = dt[0] / dt[1] * 100;
            if (perc < 60) return "red";
            if (perc >= 100) return "green";
            if (perc >= 60 && perc < 80) return "yellow";
            if (perc >= 80 && perc < 100) return "orange";
        }
    }
};

var Row = React.createClass({
    render: function () {
        var formatFn = METRICS[this.props.metric].format;
        var colourFn = METRICS[this.props.metric].getColor;

        var cells = _.map(this.props.headers, function (item) {
            var cellDate = Moment(item);
            var view = "N/A";
            var colorClass = "grey";

            var isValid = true;
            if (this.props.min.isAfter(cellDate, "d")) {
                isValid = false;
            }

            if (this.props.values && isValid) {
                var value = _.find(this.props.values, function (value) {
                    if (value[0] == item) return true;
                }, this);
                if (value && value[1]) view = formatFn(value);
                if (value && value[1]) colorClass = colourFn(value);
            }
            return <td className={colorClass}>{view}</td>
        }, this);

        return (
            <tr>
                {cells}
            </tr>
        )
    }
});

var Assignment = React.createClass({
    getInitialState: function () {
        return {}
    },

    render() {
        var locationName = ewars.I18N(this.props.data.location_name);
        var formName = ewars.I18N(this.props.data.form_name);
        return (
            <tbody>
            <tr>
                <th style={{textAlign: "left"}}>{formName}<br/>{locationName}</th>
            </tr>
            </tbody>
        )
    }
});

var AuditTableView = React.createClass({
    _isLoading: false,

    getInitialState: function () {
        return {
            data: {},
            locations: {},
            location: null,
            path: [],
            visibility: {},
            child_map: {},
            vis: {},
            assignments: null,
            start_date: Moment.utc().format("YYYY-MM-DD"),
            end_date: Moment.utc().format("YYYY-MM-DD")
        }
    },

    componentWillMount: function () {
        var query = {
            status: {eq: "ACTIVE"}
        };

        ewars.tx("com.ewars.assignments.user", [window.user.id])
            .then(function (resp) {
                this.state.assignments = [];
                resp.forEach((assign) => {
                    if (assign.form_features.INTERVAL_REPORTING) {
                        this.state.assignments.push(assign);
                    }
                });
                let locationCount = this.state.assignments.length;
                this._reload(this.props, locationCount);
            }.bind(this))
    },

    componentWillReceiveProps: function (nextProps) {
        let locationCount = this.state.assignments.length;
        this._reload(nextProps, locationCount);
    },

    _reload: function (props, locationCount) {
        var locationsLoaded = 0;
        if (locationCount != null) {
            var blocker = new ewars.Blocker(null, "Loading data...");
        }

        let data = {};

        this.state.assignments.forEach(assign => {

            ewars.tx(
                `com.ewars.performance`,
                [
                    props.metric,
                    assign.location_id,
                    props.start_date.format("YYYY-MM-DD"),
                    props.end_date.format("YYYY-MM-DD"),
                    assign.form_id
                ]
            ).then((resp) => {
                data[assign.uuid] = resp;
                locationsLoaded++;
                if (locationsLoaded >= locationCount) {
                    blocker.destroy();
                    this.setState({
                        data: data
                    })
                }
            })

        })
    },

    _addLocations: function (parent_id, locations) {
        this._reload(this.props, locations);
    },

    _export: function () {
        var blocker = new ewars.Blocker(null, "Exporting data...");

        var export_data = "data:text/csv;filename=export.csv;charset=utf-8,";
        var cols = ["form", "location", "start_date", "end_date"];
        var row = [];

        var joiner = ",";

        var sets = [];

        _.each(this.state.assignments, function (assign) {
            let assignUUID = assign.uuid;
            let dates = this.state.data[assignUUID].map(item => {
                return item[0];
            });
            sets.push.apply(sets, dates);
        }, this);

        var rawHeaders = _.uniq(sets);
        cols.push.apply(cols, rawHeaders);

        var rows = [cols];
        var dataRows = [];
        var formatFn = METRICS[this.props.metric].format;

        this.state.assignments.forEach(assign => {
            assign.locations.forEach(location => {
                let row = [
                    ewars.I18N(assign.form_name),
                    ewars.I18N(location.name),
                    ewars.DATE(location.start_date),
                    ewars.DATE(location.end_date)
                ];
                var data = this.state.data[location.uuid];

                let cells = rawHeaders.map(item => {
                    let view = "N/A";
                    if (data) {
                        let value;
                        data.forEach(value => {
                            if (value[0] == item) value = value[0];
                            if (value && value[1]) view = formatFn(value);
                        })
                    }
                    return view;
                });

                row.push.apply(row, cells);
                dataRows.push(row)

            })
        });

        rows.push.apply(rows, dataRows);

        _.each(rows, function (row) {
            export_data += row.join(joiner) + "\n";
        });

        var encoded = encodeURI(export_data);
        window.open(encoded);

        blocker.destroy();
    },

    render: function () {
        console.log(this.state);
        if (!this.state.assignments) return (<div className="dummy"></div>);
        var sets = [];

        this.state.assignments.forEach(assign => {
            if (this.state.data[assign.uuid]) {
                let dates = this.state.data[assign.uuid].map(item => {
                    return item[0];
                });
                sets.push.apply(sets, dates);
            }
        });
        var rawHeaders = _.uniq(sets);

        let rowHeaders = [];

        this.state.assignments.forEach(assign => {
            rowHeaders.push(
                <Assignment
                    key={assign.form_id + "_" + assign.location_id}
                    location={assign}
                    data={assign}/>
            )
        });

        let cellHeaders = rawHeaders.map(header => {
            return <th>{header}</th>
        });

        let dataRows = [];

        this.state.assignments.forEach(assign => {
            let assignStart = Moment(assign.start_date);
            dataRows.push(
                <Row
                    metric={this.props.metric}
                    headers={rawHeaders}
                    min={assignStart}
                    data={assign}
                    values={this.state.data[assign.uuid]}/>
            )
        });

        var view;
        if (!this.state.assignments) {
            view = (
                <div className="placeholder" style={{height: "auto", padding: "16px 0"}}>Loading</div>
            )
        } else {
            view = (
                <table width="100%" className="info-list row-click">
                    <thead>
                    <tr>
                        {cellHeaders}
                    </tr>
                    </thead>
                    <tbody>
                    {dataRows}
                    </tbody>
                </table>
            )
        }

        return (
            <div className="widget">
                <div className="widget-header">
                    <span>Results</span>
                    <div className="btn-group pull-right" style={{marginTop: 5}}>
                        <ewars.d.Button
                            label="Export"
                            onClick={this._export}
                            icon="fa-download"/>
                    </div>
                </div>
                <div className="body no-pad">

                    <table width="100%" className="pivot-wrapper">
                        <tbody>
                        <tr>
                            <td width="300" style={{position: "relative"}}>
                                <div className="pivot-row-headers">
                                    <table width="100%" height="100%" className="info-list row-click">
                                        <thead>
                                        <tr>
                                            <th width="150px" colSpan="2">Location Name</th>
                                        </tr>
                                        </thead>
                                        {rowHeaders}
                                    </table>
                                </div>
                            </td>
                            <td style={{position: "relative"}}>
                                <div className="pivot-scroll">
                                    {view}
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }
});

export default AuditTableView;
