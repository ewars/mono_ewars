const ADMIN_ACTIONS = [
    {
        n: "action",
        l: "Action",
        t: "SELECT",
        o: [
            ["DELETE_PROJECT", "Delete Project"]
        ]
    },
    {
        n: "remove_imported",
        l: "Remove already imported data?",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ALL",
            "action:=:DELETE_PROJECT"
        ]
    }
];

export default ADMIN_ACTIONS