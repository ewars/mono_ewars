/*
columns
  - name
  - type
    - numeric
    - text
    - select
    - multiselect
    - period
    - location
    - date
    - colour
    - user
  - safe name
  - description
  - redacted
  - Access Controls
  - Validation rules
  - options / options source
  - location mapping definition (only applies to location columns)
- Columns to use to calculate unique hash for id

 */