import Component from "./layout/Component";
import Browser from './layout/view.browser';
import {EN} from "./lang/en";

__.register(EN);

import {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Modal,
    ButtonGroup,
    Panel,
    Toolbar,
    ActionGroup,
    SystemForm
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Modal,
    ButtonGroup,
    Panel,
    Toolbar,
    ActionGroup,
    SystemForm
}

ReactDOM.render(
    <Browser/>,
    document.getElementById('application')
);


