export const DATE_FORMATS = [
    ["%Y-%m-%d", "ISO8601 2017-12-31 (%Y-%m-%d)"],
    ["%y-%m-%d", "17-12-31 (%y-%m-%d)"],
    ["%y/%m/%d", "17/12/31 (%y/%m/%d)"],
    ["%d-%m-%Y", "31-12-2017 (%d-%m-%Y)"],
    ["%d/%m/%Y", "31/12/2017 (%d/%m/%Y)"],
    ["%Y/%m/%d", "2017/12/31 (%Y/%m/%d)"],
    ["%Y.%m.%d", "2017.12.31 (%Y.%m.%d)"],
    ["%Y W%W", "2017 W52 (%Y W%W)"],
    ["CUSTOM", "Custom"]
];

export const LOC_MATCHES = [
    ["UUID", "UUID"],
    ["PCODE", "PCode"],
    ["NAME", "Location Name"]
]