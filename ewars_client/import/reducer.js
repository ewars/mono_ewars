const _updateSetting = (state, prop, value) => {
    if (prop.startsWith("meta_map")) {
        let key = prop.split(".")[1];
        if (!state.data.meta_map) state.data.meta_map = {};
        state.data.meta_map[key] = value;
    } else {
        state.data[prop] = value;
    }

    let removals = [];
    state.errors.forEach((item, index) => {
        if (item[0] == prop && item[2] == true) {
           removals.push(index);
        }
    });

    removals.forEach((item) => {
        state.errors.splice(item, 1);
    });

    return state;
};

const _updateMapping = (state, column, target) => {
    for (let v in state.data.mapping) {
        if (v == column) {
            state.data.mapping[v].target = target;
        }
    }
    return state;
};

const _updateView = (state, view) => {
    state.mainView = view;
    return state;
};

const _updateSecondaryView = (state, view) => {
    state.secondaryView = view;
    return state;
};

const _updateMappingProp = (state, col, prop, value) => {
    for (let v in state.data.mapping) {
        if (v == col) {
            state.data.mapping[v][prop] = value;
        }
    }
    return state;
};

let _extrapolateFields = (definition, parentKey, parentName) => {
    let fields = [];

    for (let i in definition) {
        let key = i;
        let name = ewars.I18N(definition[i].label);
        if (parentKey) key = `${parentKey}.${i}`;
        if (parentName) name = `${parentName} \\ ${ewars.I18N(definition[i].label)}`;


        if (definition[i].fields) {
            let subs = _extrapolateFields(definition[i].fields, key, name);
            fields.push.apply(fields, subs);
        } else {
            let def = definition[i];
            if (["header", "matrix", "row", "display"].indexOf(def.type) < 0) {
                fields.push({
                    ...definition[i],
                    name: key,
                    longName: `${key} - ${name} [${def.type}]`
                })
            }
        }
    }

    fields.sort((a, b) => {
        if (a.name > b.name) return 1;
        if (a.name < b.name) return -1;
        return 0;
    })

    return fields;
};

const _setForm = (state, data) => {
    state.form = data;
    state.fields = _extrapolateFields(data.version.definition);

    return state;
};

const _updateAdmin = (state, prop, value) => {
    state.admin[prop] = value;
    return state;
};

const _unsetProject = (state) => {
    state.fields = [];
    state.form = null;
    state.data = {};
    state.errors = [];
    state.mainView = "DEFAULT";
    state.admin = {};
    return state;
};

const _setErrors = (state, errors) => {
    state.errors = errors || [];
    return state;
};

const _setProject = (state, data) => {
    state.data = data;
    state.errors = [];
    state.form = null;
    state.mainView = "DEFAULT";
    state.admin = {};
    return state;
}

const reducer = function (state, action, data) {
    let newState = ewars.copy(state);

    switch(action) {
        case "UPDATE_SETTING":
            return _updateSetting(newState, data[0], data[1]);
        case "UPDATE_MAPPING":
            return _updateMapping(newState, data[0], data[1]);
        case "CHANGE_VIEW":
            return _updateView(newState, data);
        case "CHANGE_SECONDARY_VIEW":
            return _updateSecondaryView(newState, data);
        case "SET_FORM":
            return _setForm(newState, data);
        case "UPDATE_MAPPING_CUSTOM":
            return _updateMappingProp(newState, data[0], data[1], data[2]);
        case "UPDATE_ADMIN":
            return _updateAdmin(newState, data[0], data[1]);
        case "UNSET_PROJECT":
            return _unsetProject(newState);
        case "SET_ERRORS":
            return _setErrors(newState, data);
        case "SET_PROJECT":
            return _setProject(newState, data);
        default:
            return state;
    }
};

export default reducer;