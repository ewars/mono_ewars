import Mappings from "../components/Mappings";
import ProjectDataTable from "../components/ProjectDataTable";

import Settings from "../components/Settings";
import reducer from "../reducer";
import ADMIN_ACTIONS from "../forms/admin.actions";
import ReportBrowser from "../../common/c.report_browser";
import Delete from './view.delete';


const STATUS_LINE = {
    textAlign: 'center',
    position: 'absolute',
    left: '50%',
    top: '5px',
    borderRadius: '11px',
    background: 'rgba(0,0,0,0.9)',
    display: 'block',
    padding: '5px 16px'
}

var TabButton = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-htab-btn";
        if (this.props.current == this.props.view) className += " ide-htab-btn-active";

        var iconClass = "fal " + this.props.icon;

        return (
            <div xmlns="http://www.w3.org/1999/xhtml" onClick={this._onClick}
                 className={className}><i className={iconClass}></i>&nbsp;{this.props.label}
            </div>
        )
    }
});

const ADMIN_ACTS = [
    {label: "Execute", action: "EXECUTE", icon: "fa-send"},
    {label: "Cancel", action: "CLOSE", icon: "fa-times"}
];

const required_fields = [
    "name",
    "target_type",
    "form_id",
    "meta_map.lid_key",
    "meta_map.lid_match_type",
    "meta_map.dd_key",
    "meta_map.dd_format",
    "meta_map.sub_key"
];

class Project extends React.Component {
    _checking = false;

    constructor(props) {
        super(props);

        if (ewars.z.exists("PROJECT_EDITOR")) {
            ewars.z.force("PROJECT_EDITOR", {
                mainView: "MAPPING",
                secondaryView: "SETTINGS",
                data: props.data,
                form: null,
                errors: [],
                fields: [],
                admin: {},
                validating: props.data.status == 'VALIDATING' ? true : false,
                importing: props.data.status == 'IMPORTING' ? true : false
            })
        } else {
            ewars.z.register("PROJECT_EDITOR", reducer, {
                mainView: "MAPPING",
                secondaryView: "SETTINGS",
                data: props.data,
                form: null,
                errors: [],
                fields: [],
                admin: {},
                validating: props.data.status == 'VALIDATING' ? true : false,
                importing: props.data.status == 'IMPORTING' ? true : false
            });
        }

        this.state = ewars.z.getState("PROJECT_EDITOR");

        ewars.z.subscribe("PROJECT_EDITOR", this._onChange);
        ewars.registerSave(this._save);

        let isProcessing = this.state.validating || this.state.importing;

        if (isProcessing) {
            this._interval = setInterval(this._checkStatus, 3000);
        }
    }

    _checkStatus = () => {
        if (this._checking) return;
        this._checking = true;
        ewars.tx('com.ewars.import.status', [this.state.data.uuid])
            .then(resp => {
                let validating = this.state.validating,
                    importing = this.state.importing;

                if (resp == 'LOADED') {
                    validating = false;
                    importing = false;
                }

                if (resp == 'IMPORTING') {
                    validating = false;
                    importing = true;
                }

                if (resp == 'VALIDATING') {
                    validating = true;
                    importing = false;
                }

                this._checking = false;
                this.setState({
                    validating: validating,
                    importing: importing
                })
            })
    }

    _onChange = () => {
        let newState = ewars.z.getState("PROJECT_EDITOR");
        this.setState(ewars.z.getState("PROJECT_EDITOR"));
        if (newState.data.form_id != this.state.data.form_id) {
            if (newState.data.form_id != null) {
                this._loadForm(newState.data.form_id);
            }
        }
    };

    componentWillUnmount() {
        ewars.z.dispatch("PROJECT_EDITOR", "UNSET_PROJECT");
        ewars.deregisterSave(this._save);
        ewars.z.unsubscribe("PROJECT_EDITOR", this._onChange);
        if (this._interval) {
            window.clearInterval(this._interval);
            this._interval = null;
        }
    }

    componentWillMount() {
        if (this.state.data.form_id) {
            this._loadForm(this.state.data.form_id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.uuid != this.state.data.uuid) {
            ewars.z.dispatch("PROJECT_EDITOR", "SET_PROJECT", nextProps.data);

            window.clearInterval(this._interval);
            this._interval = setInterval(this._checkStatus, 3000);
        }
        if (nextProps.data.form_id) {
            this._loadForm(this.state.data.form_id);
        }
    }

    _loadForm = (form_id) => {
        const join = [
            "version:version_id:uuid:uuid,definition"
        ];
        ewars.tx("com.ewars.resource", ['form', form_id, null, join])
            .then((data) => {
                ewars.z.dispatch("PROJECT_EDITOR", "SET_FORM", data);
            });
    };

    _onTabChange = (view) => {
        ewars.z.dispatch("PROJECT_EDITOR", "CHANGE_VIEW", view);
    };

    _onPanelChange = (view) => {
        ewars.z.dispatch("PROJECT_EDITOR", "CHANGE_SECONDARY_VIEW", view);
    };

    _setInterval = () => {
        if (!this._interval) {
            this._checking = false;
            this._interval = setInterval(this._checkStatus, 3000);
        }
    };

    _save = () => {
        if (!ewars.isSet(this.state.data.name)) {
            this.setState({
                error: "Please provide a valid name"
            });
            return;
        }

        if (!ewars.isSet(this.state.data.target_type)) {
            this.setState({
                error: "Please provide a valid target type"
            });
            return;
        }

        let bl = new ewars.Blocker(null, "Updating project settings...");
        ewars.tx("com.ewars.import.update", [this.state.data.uuid, this.state.data])
            .then((resp) => {
                bl.destroy();
                ewars.growl("Project settings updated");
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _close = () => {
        this.props.onClose();
    };

    _validate = () => {
        this._setInterval();

        let errors = [];

        for (let i in this.state.data.mapping) {
            let def = this.state.data.mapping[i];

            if (["", null].indexOf(def.target) >= 0) {
                errors.push([i, "MISSING_MAP"]);
            } else {

                let targetField = this.state.fields.find((item) => {
                    return def.target == item.name;
                })
                if (targetField) {
                    if (targetField.type == "date") {
                        if (!def.date_format) {
                            errors.push([i, "MISSING_DATE_FORMAT"]);

                        }
                    }
                }
            }
        }

        required_fields.forEach((item) => {
            let val = ewars.getKeyPath(item, this.state.data);
            if (["", null, undefined].indexOf(val) >= 0) {
                errors.push([item, "REQUIRED", true]);
            }
        });

        if (errors.length > 0) {
            ewars.z.dispatch("PROJECT_EDITOR", "SET_ERRORS", errors);
            ewars.growl("Please resolve issues in mapping");
            return;
        }

        this.setState({
            validating: true
        }, () => {
            ewars.tx("com.ewars.import.validate", [this.state.data.uuid])
                .then((resp) => {
                    ewars.growl('Validation running...');

                })
        })
    };

    _validate_run = () => {
        this._setInterval();
        let errors = [];

        for (let i in this.state.data.mapping) {
            let def = this.state.data.mapping[i];

            if (["", null].indexOf(def.target) >= 0) {
                errors.push([i, "MISSING_MAP"]);
            } else {

                let targetField = this.state.fields.find((item) => {
                    return def.target == item.name;
                })
                if (targetField) {
                    if (targetField.type == "date") {
                        if (!def.date_format) {
                            errors.push([i, "MISSING_DATE_FORMAT"]);

                        }
                    }
                }
            }

        }

        required_fields.forEach((item) => {
            let val = ewars.getKeyPath(item, this.state.data);
            if (["", null, undefined].indexOf(val) >= 0) {
                errors.push([item, "REQUIRED", true]);
            }
        });

        if (errors.length > 0) {
            ewars.z.dispatch("PROJECT_EDITOR", "SET_ERRORS", errors);
            ewars.growl("Please resolve issues in mapping");
            return;
        }

        this.setState({
            importing: true
        }, () => {
            ewars.prompt("fa-upload", __("VALIDATE_RUN"), __("VALIDATE_RUN_TXT"), () => {
                ewars.growl('Import started');
                ewars.tx("com.ewars.import.validate", [this.state.data.uuid])
                    .then((resp) => {
                        return ewars.tx("com.ewars.import.run", [this.state.data.uuid])
                    })
                    .catch(err => {
                        console.log(err);
                    })
                    .then(() => {
                        this.setState({
                            importing: false
                        })
                    })
                    .catch(err => {
                        console.log(err);
                    })
            })
        })
    };

    _onAction = (action) => {
        if (action == "CLOSE") {
            this.setState({
                showAdmin: false
            })
        }

        if (action == "EXECUTE") {
            this._executeAdminAction();
        }
    };

    render() {
        // Figure out if the controls need to be hidden
        let showControls = true;
        if (this.state.data.meta_map.imported >= this.state.data.meta_map.total_rows) showControls = false;
        if (this.state.validating) showControls = false;
        if (this.state.importing) showControls = false;

        let label;
        if (this.state.validating) label = "Validating...";
        if (this.state.importing) label = 'Importing...';

        let mainView, secondaryView;


        if (this.state.mainView === "MAPPING") {
            let mappingErrors = this.state.errors.filter((item) => {
                return item[2] == undefined;
            })
            let errors = this.state.errors.filter((item) => {
                return item[2] == true;
            })
            mainView = <Mappings
                data={this.state.data}
                readOnly={!showControls}
                errors={mappingErrors}
                settingsErrors={errors}
                fields={this.state.fields}
                form={this.state.form}/>;
        }
        if (this.state.mainView === "DATA") {
            mainView = <ProjectDataTable
                data={this.state.data}
                readOnly={!showControls}
                fields={this.state.fields}/>;
        }

        if (this.state.mainView == 'DELETE') {
            mainView = <Delete
                data={this.state.data}
                onDelete={this._close}/>;
        }


        if (this.state.mainView == "RESULTS") {
            if (["", null, undefined].indexOf(this.state.data.form_id) >= 0) {
                mainView = (
                    <div className="placeholder">{__("NO_DEST")}</div>
                )
            } else {
                mainView = (
                    <ReportBrowser
                        form_id={this.state.data.form_id || null}
                        id={"RESULTS_" + this.state.data.uuid}
                        filter={{import_set: {eq: this.state.data.uuid}}}/>
                )
            }
        }

        return (
            <div className="window-buffer">
                <div className="inner">
                    <ewars.d.Layout>
                        <ewars.d.Toolbar label={this.props.data.name}>
                            {showControls ?
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        onClick={this._save}
                                        icon="fa-save"
                                        label="Save Change(s)"/>
                                    <ewars.d.Button
                                        onClick={this._close}
                                        icon="fa-times"
                                        label="Close"/>
                                </div>
                                : null}

                            {label ?
                                <span style={STATUS_LINE}>{label}</span>
                                : null}

                            {!showControls ?
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        onClick={this._close}
                                        icon="fa-times"
                                        label="Close"/>
                                </div>
                                : null}

                            {showControls ?
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        onClick={this._validate_run}
                                        label="VALIDATE_RUN"/>
                                    <ewars.d.Button
                                        onClick={this._validate}
                                        label="VALIDATE"/>
                                </div>
                                : null}
                        </ewars.d.Toolbar>
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <ewars.d.Row height="36px">
                                    <ewars.d.Cell>
                                        <div xmlns="http://www.w3.org/1999/xhtml" className="ide-htabs">
                                            <TabButton
                                                label="Settings"
                                                icon="fa-cog"
                                                current={this.state.mainView}
                                                onClick={this._onTabChange}
                                                view="MAPPING"/>
                                            <TabButton
                                                label="Data"
                                                icon="fa-list"
                                                current={this.state.mainView}
                                                onClick={this._onTabChange}
                                                view="DATA"/>
                                            <TabButton
                                                label="Results"
                                                icon="fa-clipboard"
                                                current={this.state.mainView}
                                                onClick={this._onTabChange}
                                                view="RESULTS"/>
                                            {showControls ?
                                                <TabButton
                                                    label="Delete"
                                                    icon="fa-trash"
                                                    current={this.state.mainView}
                                                    onClick={this._onTabChange}
                                                    view="DELETE"/>
                                                : null}
                                        </div>
                                    </ewars.d.Cell>
                                </ewars.d.Row>
                                <ewars.d.Row>
                                    <ewars.d.Cell>
                                        {mainView}
                                    </ewars.d.Cell>
                                </ewars.d.Row>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </div>
            </div>
        )
    }
}

export default Project;
