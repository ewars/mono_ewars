import { FileField as FileUploadField } from "../../common/fields";
import Project from './Project';
import Moment from "moment";

const ACTIONS = [
    ['fa-plus', 'ADD']
];

const STATUS = {
    COLLECTING_DATA: 'Processing data...',
    LOADED: 'Data loaded',
    DELETING: 'Deleting...',
    VALIDATING: 'Validating...',
    IMPORTING: 'Importing...'
}

const ROW_ACTIONS = [
    ['fa-pencil', 'EDIT'],
    ['fa-trash', 'DELETE']
];

class ProjectChunk extends React.Component {
    static defaultProps = {
        completed: false
    };

    constructor(props) {
        super(props);

        this.state = {
            cmp: null,
            deleting: false
        }
    }

    _action = (action) => {
        switch (action) {
            case 'EDIT':
                this.props.stopInterval();
                this.setState({
                    cmp: <Project
                        onClose={this._closeChunk}
                        data={ewars.copy(this.props.data)}/>
                });
                break;
            case 'DELETE':
                this._delete();
                break;
            default:
                break;
        }
    };

    _delete = () => {
        ewars.prompt('fa-trash', 'Delete import project?', 'Are you sure you want to delete this project? All data (including imported) associated with this project will be removed.', () => {
            let bl = new ewars.Blocker(null, 'Deleting project...');
            ewars.tx('com.ewars.import.delete', [this.props.data.uuid])
                .then(resp => {
                    ewars.growl('Project deletion initiated...');
                    bl.destroy();
                    this.setState({
                        deleting: true
                    })
                })
        })
    };

    _closeChunk = () => {
        this.props.startInterval();
        this.setState({cmp: null});
    };

    render() {
        let status;
        if (STATUS[this.props.data.status]) status = STATUS[this.props.data.status];

        if (this.state.deleting) status = 'Deleting...';

        let label = Moment.utc(this.props.data.last_modified).format('YYYY-MM-DD HH:mm');
        label += ' - ' + this.props.data.name;

        let style = {};
        if (this.props.completed) style.background = '';

        let showDelete = true;
        if (this.props.data.status == 'VALIDATING') showDelete = false;
        if (this.props.data.status == 'IMPORTING') showDelete = false;

        let actions = ewars.copy(ROW_ACTIONS);
        if (!showDelete) actions.pop();

        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell style={{flex: '20 1 auto', lineHeight: '22px'}}>{label}</ewars.d.Cell>
                        <ewars.d.Cell style={{flex: '1 1 auto', lineHeight: '22px'}}>{status}</ewars.d.Cell>
                        {this.props.data.status == 'LOADED' && !this.state.deleting ?
                            <ewars.d.Cell
                                style={{flex: '1 1 auto', lineHeight: '22px'}}>{this.props.data.meta_map.imported}
                                / {this.props.data.meta_map.total_rows}</ewars.d.Cell>
                            : null}
                        {this.props.data.status != 'COLLECTING_DATA' && !this.state.deleting ?
                            <ewars.d.Cell width="60px" style={{flex: '1 1 auto', display: 'block'}}>
                                <ewars.d.ActionGroup
                                    actions={actions}
                                    onAction={this._action}
                                    right={true}
                                    height="23px"/>
                            </ewars.d.Cell>
                            : null}
                    </ewars.d.Row>
                </div>
                {this.state.cmp}
            </div>
        )
    }
}

class Imports extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            chunks: []
        }

        this._query(this.props.data.id);
        this._interval = window.setInterval(this._get, 5000);

        window.addEventListener('blur', this._removeInterval);
        window.addEventListener('focus', this._addInterval);
    }

    componentWillReceiveProps(nextProps) {
        this._removeInterval();
        this._query(nextProps.data.id, () => {
            this._addInterval();
        });
    }

    _removeInterval = () => {
        if (this._interval) {
            clearInterval(this._interval);
            this._interval = null;
        }
    };

    _addInterval = () => {
        if (!this._interval) {
            this._interval = window.setInterval(this._get, 5000);
        }
    };

    componentWillUnmount() {
        clearInterval(this._interval);
        this._interval = null;
        // Remove timer intervals
        window.removeEventListener('blur', this._removeInterval);
        window.removeEventListener('focus', this._addInterval);
    }

    _get = () => {
        this._query(this.props.data.id);
    };

    _query = (id, cb) => {
        ewars.tx('com.ewars.import.chunks', [id])
            .then(resp => {
                this.setState({chunks: resp}, () => {
                    if (cb) cb();
                })
            })
    };

    _action = (action) => {

    };

    _fileUpped = (name, value) => {
        this._removeInterval();
        let bl = new ewars.Blocker(null, "Creating import project...");
        ewars.tx("com.ewars.import.create", [value, this.props.data.id])
            .then((resp) => {
                this._query(this.props.data.id, () => {
                    bl.destroy();
                    this._addInterval();
                });
            })
            .catch((err) => {
                console.log(err);
            })
    };

    render() {
        let completeItems = this.state.chunks.filter(item => {
            if (item.status != 'COLLECTING_DATA') {
                if (item.meta_map.imported == item.meta_map.total_rows) return true;
            }
            return false;
        });

        let incompleteItems = this.state.chunks.filter(item => {
            if (item.status == 'COLLECTING_DATA') {
                return true;
            } else {
                if (item.meta_map.imported != item.meta_map.total_rows) return true;
            }
            return false;
        });

        completeItems = completeItems.sort((a, b) => {
            if (a.last_modified > b.last_modified) return -1;
            if (a.last_modified < b.last_modified) return 1;
            return 0;
        });

        incompleteItems = incompleteItems.sort((a, b) => {
            if (a.last_modified > b.last_modified) return -1;
            if (a.last_modified < b.last_modified) return 1;
            return 0;
        })

        return (
            <ewars.d.Cell>
                <ewars.d.Layout>
                    <ewars.d.Toolbar label={this.props.data.name}>
                        <div style={{float: 'right', marginTop: '-4px', marginRight: '8px'}}>
                            <FileUploadField
                                name="file"
                                local={true}
                                showInput={false}
                                showPreview={false}
                                label="Upload CSV for new Project"
                                onUpdate={this._fileUpped}/>
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <ewars.d.Row>
                                <div className="block-tree" style={{position: 'relative'}}>
                                    {incompleteItems.map(item => {
                                        return <ProjectChunk
                                            key={item.uuid}
                                            startInterval={this._addInterval}
                                            stopInterval={this._removeInterval}
                                            data={item}/>
                                    })}
                                    {completeItems.map(item => {
                                        return <ProjectChunk
                                            key={item.uuid}
                                            startInterval={this._addInterval}
                                            stopInterval={this._removeInterval}
                                            completed={true}
                                            data={item}/>
                                    })}
                                </div>
                            </ewars.d.Row>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </ewars.d.Cell>
        )
    }
}

export default Imports;
