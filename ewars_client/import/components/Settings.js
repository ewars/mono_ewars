import {
    TextField,
    SelectField,
    TextAreaField
} from "../../common/fields";

import {DATE_FORMATS, LOC_MATCHES} from "../constants/formats";

const TARGET_TYPE_CONFIG = {
    options: [
        ["FORM", "Form"],
        ["DATASET", "Dataset"]
    ]
};

const TARGET_FORM_CONFIG = {
    optionsSource: {
        resource: "form",
        valSource: "id",
        labelSource: "name",
        query: {
            status: {neq: "DELETED"}
        }
    }
};

const FORM = [
    {
        _o: 0,
        n: "name",
        l: "PROJECT_NAME",
        t: "TEXT",
        r: true
    },
    {
        _o: 1,
        n: "description",
        l: "DESCRIPTION",
        t: "TEXT",
        ml: true
    },
    {
        _o: 4,
        n: "meta_map.lid_key",
        l: "Location Column",
        t: "SELECT",
        r: true,
        o: []
    },
    {
        _o: 5,
        n: "meta_map.lid_match_type",
        l: "Location Match Type",
        t: "SELECT",
        r: true,
        o: LOC_MATCHES
    },
    {
        _o: 5.1,
        n: "meta_map.lid_nonstd",
        t: "BUTTONSET",
        l: "Override location type match?",
        o: [
            [false, "No"],
            [true, "Yes"]
        ],
        c: [
            "ALL",
            "meta_map.lid_match_type:=:NAME"
        ]
    },
    {
        _o: 5.2,
        n: "meta_map.lid_match_sti",
        l: "Location type match",
        t: "SELECT",
        r: true,
        os: [
            "location_type",
            "id",
            "name",
            {}
        ],
        c: [
            "ALL",
            "meta_map.lid_nonstd:=:true",
            "meta_map.lid_match_type:=:NAME"
        ]
    },
    {
        _o: 6,
        n: "meta_map.dd_key",
        l: "Report Date Column",
        t: "SELECT",
        r: true,
        o: []
    },
    {
        _o: 7,
        n: "meta_map.dd_format",
        l: "Report Date Format",
        r: true,
        t: "SELECT",
        o: DATE_FORMATS
    },
    {
        _o: 8,
        n: "meta_map.dd_custom_format",
        l: "Custom Date Format",
        t: "TEXT",
        r: true,
        c: [
            "ALL",
            "dd_format:=:CUSTOM"
        ]
    },
    {
        _o: 10,
        n: "meta_map.sub_key",
        l: "Submitted Date Column",
        t: "SELECT",
        o: [
            ["TODAY", "System \\ Today"],
            ["DATA_DATE", "System \\ Report Date"]
        ],
        r: true,
        h: "This value can not be null"
    },
    {
        _o: 11,
        n: "meta_map.sub_format",
        l: "Submitted Date Format",
        t: "SELECT",
        o: DATE_FORMATS,
        c: [
            "ALL",
            "meta_map.sub_key:!=:TODAY",
            "meta_map.sub_key:!=:DATA_DATE"
        ]
    },
    {
        _o:12,
        t: "H",
        l: "Advanced"
    },
    {
        _o: 13,
        l: "Allow future?",
        t: "BUTTONSET",
        n: "meta_map.allow_future",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    }
];

const FIELD_SEL_NAMES = [
    "meta_map.sub_key",
    "meta_map.dd_key",
    "meta_map.lid_key"
];

class Settings extends React.Component {
    static defaultProps = {
        fields: [],
        errors: null,
        readOnly: false
    };

    constructor(props) {
        super(props);
    }

    _onChange = (prop, value) => {
        ewars.z.dispatch("PROJECT_EDITOR", "UPDATE_SETTING", [
            prop,
            value
        ])
    };

    render() {
        let fieldOptions = [];
        for (let i in this.props.data.mapping) {
            fieldOptions.push([i, i]);
        }

        fieldOptions.sort();

        let form_def = ewars.copy(FORM);
        form_def.forEach((item) => {
            if (FIELD_SEL_NAMES.indexOf(item.n) >= 0) {
                item.o.push.apply(item.o, fieldOptions);
            }
        })

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll" style={{padding: 8}}>
                <ewars.d.SystemForm
                    definition={form_def}
                    vertical={true}
                    ro={this.props.readOnly}
                    errors={this.props.errors}
                    onChange={this._onChange}
                    data={this.props.data}
                    enabled={true}/>
            </div>
        )
    }
}

export default Settings;
