class ProjectItem extends React.Component {
    render() {
        return (
            <div className="block hoverable" onClick={() => {this.props.onSelect(this.props.data)}}>
                <div className="block-content">{this.props.data.name}</div>
            </div>
        )
    }
}

class ProjectList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentWillMount() {
        ewars.tx("com.ewars.query", ["import_project", null, {}, null, null, null, null])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    _select = (data) => {
        this.props.onSelectProject(data);
    };

    render() {
        return (
            <div className="block-tree" style={{position: "relative"}}>
                {this.state.data.map((item) => {
                    return <ProjectItem data={item} onSelect={this._select}/>
                })}
            </div>
        )
    }
}

export default ProjectList;