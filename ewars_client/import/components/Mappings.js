import {
    TextField,
    DisplayField
} from "../../common/fields";
import Settings from './Settings';

import {DATE_FORMATS} from "../constants/formats";

const ERROR_LABELS = {
    MISSING_MAP: "Field is not mapped",
    MISSING_DATE_FORMAT: "Please specify a date format"
}

let FORM = [
    {
        n: "target",
        l: "Target Form Field",
        t: "SELECT",
        o: []
    },
    {
        n: "date_format",
        l: "Source Date Format",
        t: "SELECT",
        o: DATE_FORMATS,
        c: [
            "ALL",
            "type:=:date"
        ]
    },
    {
        n: "custom_date_format",
        l: "Custom date format",
        t: "TEXT",
        r: true,
        c: [
            "ALL",
            "date_format:=:CUSTOM"
        ]
    },
    {
        n: "null_",
        l: "Missing Data Plan",
        t: "SELECT",
        o: [
            ["NULL", "Set NULL"],
            ["ZERO", "Set Zero (0)"],
            ["IGNORE_ROW", "Ignore Row"]
        ],
        c: [
            "ANY",
            "type:=:text",
            "type:=:number",
            "type:=:textarea",
            "type:=:date"
        ]
    }
];

let DATE_OPTIONS = [
    ["Y-M-D", "2016-12-31 (YYYY-MM-DD)"],
    ["Y/M/D", "2016/12-31 (YYYY/MM/DD"]
]

class MappingEditor extends React.Component {
    static defaultProps = {
        readOnly: false
    };

    constructor(props) {
        super(props);
    }

    render() {
        let fieldForm = ewars.copy(FORM);
        let def = this.props.data[1];
        let targetField;
        if (def.target) {
            switch (def.target) {
                case "location_id":
                    targetField = {
                        type: "location"
                    };
                    break;
                case "data_date":
                    targetField = {
                        type: "date",
                        date_type: "DAY"
                    };
                    break;
                default:
                    this.props.fields.forEach((item) => {
                        if (item.name == def.target) {
                            targetField = item;
                        }
                    });
                    break;
            }
        }

        let fieldOptions = [
            ["IGNORE", "Ignore"],
            ["location_id", "System \\ Report Location"],
            ["data_date", "System \\ Report Date"]
        ];
        if (this.props.fields) {
            let formFields = this.props.fields.map((item) => {
                return [item.name, item.longName];
            });
            fieldOptions.push.apply(fieldOptions, formFields);
        }

        fieldForm[0].o = fieldOptions;
        let data = this.props.data[1];
        if (targetField) {
            data.type = targetField.type;
        }

        return (
            <ewars.d.SystemForm
                ro={this.props.readOnly}
                definition={fieldForm}
                enabled={true}
                onChange={this.props.onChange}
                data={data}/>
        )
    }
}

class Mapping extends React.Component {
    static defaultProps = {
        errors: [],
        readOnly: false
    };

    constructor(props) {
        super(props);

        this.state = {
            shown: false
        }
    }

    _show = () => {
        this.setState({
            shown: !this.state.shown
        })
    };

    _onChange = (prop, value) => {
        if (value == "IGNORE") {
            this.setState({shown: false})
        }

        if (prop == "target") {
            ewars.z.dispatch("PROJECT_EDITOR", "UPDATE_MAPPING", [
                this.props.data[0],
                value
            ]);
        } else {
            ewars.z.dispatch("PROJECT_EDITOR", "UPDATE_MAPPING_CUSTOM", [
                this.props.data[0],
                prop,
                value
            ])
        }
    };

    render() {
        let iconClass = "fal fa-square";
        let style = {};
        let fieldMappedTo;

        if (this.props.data[1].target) {
            iconClass = "fal fa-check-square";

            if (this.props.data[1].target == "IGNORE") {
                fieldMappedTo = "Ignored";
            } else {
                switch (this.props.data[1].target) {
                    case "location_id":
                        fieldMappedTo = "Location";
                        break;
                    case "data_date":
                        fieldMappedTo = "Report Date";
                        break;
                    default:
                        if (this.props.fields) {
                            this.props.fields.forEach((item) => {
                                if (item.name == this.props.data[1].target) {
                                    fieldMappedTo = __(item.longName);
                                }
                            })
                        }
                        break;
                }
            }
        }

        let hasError = this.props.errors.length > 0;

        return (
            <div className="block hoverable" style={style}>
                <div className="block-content" onClick={this._show}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={30}>
                            <i className={iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            {this.props.data[0]}
                        </ewars.d.Cell>
                        {fieldMappedTo ?
                            <ewars.d.Cell style={{textAlign: "right", display: "block"}}>
                                {fieldMappedTo}&nbsp;
                            </ewars.d.Cell>
                            : null}
                        {hasError ?
                            <ewars.d.Cell width="20px" style={{textAlign: "center"}}>
                                <i className="fal fa-circle" style={{color: "red"}}></i>
                            </ewars.d.Cell>
                            : null}
                        <ewars.d.Cell width="30px">
                            <i className={"fal " + (this.state.shown ? "fa-minus-square" : "fa-plus-square")}></i>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.shown ?
                    <div style={{border: "1px solid #252525"}}>
                        {hasError ?
                            <div className="map-errors">
                                {this.props.errors.map((item) => {
                                    return (
                                        <div className="map-error">{__(item[1])}</div>
                                    )
                                })}
                            </div>
                            : null}
                        <MappingEditor
                            readOnly={this.props.readOnly}
                            fields={this.props.fields}
                            availableFields={this.props.availableFields}
                            close={this._close}
                            save={this._save}
                            errors={this.props.errors}
                            onChange={this._onChange}
                            data={this.props.data}/>
                    </div>
                    : null}
            </div>
        )
    }
}

let _extrapolateFields = (definition, parentKey, parentName) => {
    let fields = [];

    for (let i in definition) {
        let key = i;
        let name = ewars.I18N(definition[i].label);
        if (parentKey) key = `${parentKey}.${i}`;
        if (parentName) name = `${parentName} \\ ${ewars.I18N(definition[i].label)}`;


        if (definition[i].fields) {
            fields.push.apply(fields, _extrapolateFields(definition[i].fields, key, name));
        } else {
            fields.push({
                name: key,
                longName: name,
                ...definition[i]
            })
        }
    }
    //
    // fields.sort((a, b) => {
    //     if (a.order > b.order) return -1;
    //     if (a.order < b.order) return 1;
    //     return 1;
    // })

    return fields;
};

class Mappings extends React.Component {
    static defaultProps = {
        errors: [],
        readOnly: false
    };

    constructor(props) {
        super(props);

        this.state = {
            fields: null
        }
    }

    componentWillMount() {
        if (this.props.form) {
            this.state.fields = _extrapolateFields(this.props.form.version.definition);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.form) {
            if (this.props.form) {
                if (nextProps.form.id != this.props.form.id) {
                    this.state.fields = _extrapolateFields(nextProps.form.version.definition);
                }
            } else {
                this.state.fields = _extrapolateFields(nextProps.form.version.definition);
            }
        } else {
            this.state.fields = [];
        }

    }

    render() {
        let items = [];
        for (let i in this.props.data.mapping) {
            items.push([i, this.props.data.mapping[i]]);
        }

        items.sort((a, b) => {
            if (a[0] > b[0]) return 1;
            if (a[0] < b[0]) return -1;
            return 0;
        })

        let fieldsAvailable = [];
        if (this.props.fields) {
            let usedFields = [];
            for (let key in this.props.data.mapping) {
                if (this.props.data.mapping[key].target) usedFields.push(this.props.data.mapping[key].target);
            }

            this.props.fields.forEach((item) => {
                if (usedFields.indexOf(item.name) < 0) {
                    fieldsAvailable.push(item);
                }
            })
        }

        let mappings = items.map((data) => {
            let errors = this.props.errors.filter((item) => {
                return item[0] == data[0]
            })

            return (
                <Mapping
                    data={data}
                    key={data[0]}
                    errors={errors}
                    readOnly={this.props.readOnly}
                    availableFields={fieldsAvailable}
                    fields={this.props.fields}/>
            )
        })

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="300px" borderRight={true}>
                        <Settings
                            readOnly={this.props.readOnly}
                            errors={this.props.settingsErrors}
                            fields={this.props.fields}
                            data={this.props.data}/>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree" style={{position: "relative"}}>
                                {mappings}
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Mappings;
