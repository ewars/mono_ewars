export const EN = {
    REQUIRED: "Please provide a valid value",
    MISSING_MAP: "Column is missing a mapping",
    IMPORT_PROJECTS: "Import projects",
    UPLOAD: "Upload CSV for new project",
    PROJECT_NAME: "Project name",
    TARGET_TYPE: "Target import type",
    TARGET_FORM_ID: "Target form",
    LOCATION_COLUMN: "Location column",
    LOCATION_MATCH_TYPE: "Location match type",
    DD_COLUMN: "Report date column",
    DD_FORMAT: "Report date format",
    SUB_COLUMN: "Submitted date column",
    SUB_FORMAT: "Submitted date format",
    MAPPING: "Mapping",
    DATA: "Data",
    RESULTS: "Results",
    VALIDATE_RUN: "Validate & Run",
    VALIDATE: "Validate",
    ACTIONS: "Actions",
    TARGET_FORM_FIELD: "Target form field",
    MISSING_PLAN: "Missing data plan",
    NONE_SELECTED: "No selection",

    DELETE_PROJECT: "Delete project?",
    DELETE_PROJECT_TXT: "Are you sure you want to delete this project? Any associated data may be lost.",

    VALIDATE_RUN_TXT: "Are you sure you want to run this import, any valid data will be imported into the system and removed from the source.",
    NO_DEST: "Import has no target, can not display results."


}