const STYLES = {
    PLOT_COLS: {
        border: "1px solid #CCCCCC",
        background: "#FFFFFF",
        margin: "8px",
        overflowY: "hidden"
    },
    DIM_LABEL: {
        borderRight: "1px solid #CCCCCC",
        background: "#F2F2F2",
        display: "block",
        padding: "8px"
    },
    CELL: {
        margin: "8px",
        border: "1px solid #CCCCCC",
        padding: "8px",
        background: "#FFFFFF"
    },
    CELL_HEADER: {},
    DROP_ITEM: {
        display: "block",
        float: "left",
        borderRadius: "3px",
        background: "#333333",
        padding: "5px",
        color: "#F2F2F2",
        marginTop: "3px",
        marginLeft: "5px"
    }
}

export default STYLES;