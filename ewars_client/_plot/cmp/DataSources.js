const RESOURCES = {}

const DIMENSIONS = [
    ["DIM", ["Date", "DATE"]],
    ["DIM", ["Location", "LOCATION"]],
    ["DIM", ["Location Type", "STI"]],
    ["DIM", ["Time", "TIME"]],
    ["DIM", ["Submitted", "SUBMITTED"]],
]

class ItemNode extends React.Component {
    static defaultProps = {
        res: null,
        data: {}
    };

    constructor(props) {
        super(props)

        this.state = {
            shown: false
        }
    }

    _onDragStart = (e) => {
        e.dataTransfer.setData("e", JSON.stringify([this.props.res, this.props.data]))
        ewars.emit("DRAG_START");
    };

    _onDragEnd = (e) => {
        ewars.emit("DRAG_DEAD");
    };

    _expand = () => {
        switch (this.props.res) {
            case "dimensions":
                this.setState({shown: !this.state.shown});
                break;
            case "indicator":
                if (this._loaded) {
                    this.setState({
                        shown: !this.state.shown
                    })
                } else {
                    let pid;
                    if (this.props.data) {
                        if (this.props.data.uuid) pid = this.props.data.uuid;
                        if (this.props.data.id) pid = this.props.data.id;
                    }
                    ewars.tx("com.ewars.indicators", [pid])
                        .then((resp) => {
                            this._loaded = true;
                            this.setState({
                                shown: true,
                                data: resp
                            })
                        })
                }
                break;
            default:
                break;
        }
    };

    render() {
        let hasChildren = false,
            label,
            draggable = true;


        switch (this.props.res) {
            case "dimensions":
                label = this.props.data[1][0];
                draggable = true;
                break;
            case "indicator":
                if (this.props.data.context == "FOLDER") {
                    hasChildren = true;
                    label = __(this.props.data.name);
                    draggable = false;
                } else {
                    hasChildren = false;
                    label = __(this.props.data.name);
                    draggable = true;
                }
                break;
            default:
                break;
        }

        let items = [];
        if (hasChildren && this.state.shown) {
            items = this.state.data.map((item) => {
                return <ItemNode data={item} res={this.props.res}/>
            })

        }

        return (
            <div
                className="block"
                style={{background: "#FFFFFF"}}>
                <div
                    draggable={draggable}
                    onDragEnd={this._onDragEnd}
                    onDrop={this._onDragEnd}
                    onDragStart={this._onDragStart}
                    className="block-content"
                    onClick={this._expand}>
                    <ewars.d.Row>
                        {hasChildren ?
                            <ewars.d.Cell onClick={this._expand} width="15px">
                                <i className={"fa " + (this.state.shown ? "fa-caret-down" : "fa-caret-right")}></i>
                            </ewars.d.Cell>
                            : null}
                        <ewars.d.Cell>{label}</ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.shown ?
                    <div className="block-children">
                        {items}
                    </div>

                    : null}
            </div>
        )
    }
}

class RootNode extends React.Component {
    static defaultProps = {
        name: "None",
        icon: null,
        resource: null,
        items: []
    };

    constructor(props) {
        super(props);

        this.state = {
            shown: false
        }
    }

    _expand = () => {
        switch (this.props.resource) {
            case "dimensions":
                this.setState({shown: !this.state.shown});
                break;
            case "indicator":
                if (this._loaded) {
                    this.setState({
                        shown: !this.state.shown
                    })
                } else {
                    let pid;
                    if (this.props.data) {
                        if (this.props.data.uuid) pid = this.props.data.uuid;
                        if (this.props.data.id) pid = this.props.data.id;
                    }
                    ewars.tx("com.ewars.indicators", [pid])
                        .then((resp) => {
                            this._loaded = true;
                            this.setState({
                                shown: true,
                                data: resp
                            })
                        })
                }
                break;
            default:
                break;
        }
    };

    render() {

        let items = [];
        if (this.state.shown) {
            switch (this.props.resource) {
                case "dimensions":
                    items = DIMENSIONS.map((item) => {
                        return <ItemNode data={item} res="dimensions" childs={this.state.childs}/>
                    })
                    break;
                default:
                    items = this.state.data.map((item) => {
                        return <ItemNode data={item} res={this.props.resource}/>;
                    })
                    break;
            }
        }

        return (
            <div className="block">
                <div className="block-content" onClick={this._expand}>
                    <ewars.d.Row>
                        <ewars.d.Cell onClick={this._expand} width="15px">
                            <i className={"fa " + (this.state.shown ? "fa-caret-down" : "fa-caret-right")}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>{this.props.name}</ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.shown ?
                    <div className="block-children">
                        {items}
                    </div>
                    : null}
            </div>
        )
    }
}

class DataSources extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Panel>
                <div className="block-tree" style={{position: "relative"}}>
                    <RootNode name="Dimensions" resource="dimensions"/>
                    <RootNode name="Indicators" resource="indicator"/>
                    <RootNode name="Forms" resource="form"/>
                    <RootNode name="Data" resource="data"/>
                </div>
            </ewars.d.Panel>
        )
    }
}

export default DataSources;