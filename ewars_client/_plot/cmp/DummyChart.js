class DummyChart extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row height="60px">
                    <ewars.d.Cell
                        borderBottom={true}
                        borderRight={true}
                        width="50px">

                    </ewars.d.Cell>
                    <ewars.d.Cell
                        ref={(el) => {
                            this._trCell = el
                        }}
                        borderBottom={true}>
                        [Drop field here]
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell
                        ref={(el) => {
                            this._blCell = el
                        }}
                        borderRight={true}
                        width="50px">
                        [Drop field here]
                    </ewars.d.Cell>
                    <ewars.d.Cell
                        ref={(el) => {
                            this._brCell = el;
                        }}>
                        [Drop field here]
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default DummyChart;