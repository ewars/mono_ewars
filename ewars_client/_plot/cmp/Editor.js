import reducer from "../reducer";

import PeriodSelector from "../../common/cmp/fields/PeriodSelector";

import DataSources from "./DataSources";
import Analysis from "./Analysis";
import DropArea from "./DropArea";

import DummyChart from "./DummyChart";
import Chart from "./Chart";


const STYLES = {
    PLOT_COLS: {
        border: "1px solid #CCCCCC",
        background: "#FFFFFF",
        margin: "8px"
    },
    DIM_LABEL: {
        borderRight: "1px solid #CCCCCC",
        background: "#F2F2F2",
        display: "block",
        padding: "8px"
    },
    CELL: {
        margin: "8px",
        border: "1px solid #CCCCCC",
        padding: "8px",
        background: "#FFFFFF"
    },
    CELL_HEADER: {}
}

class Editor extends React.Component {
    constructor(props) {
        super(props);

        this.state = ewars.z.register("E", reducer, {
            definition: {
                rows: [],
                columns: []
            },
            view: "DEFAULT"
        })

        ewars.z.subscribe("E", this._onChange);
        ewars.subscribe("DRAG_START", this._onDragAlive);
        ewars.subscribe("DRAG_DEAD", this._onDragDead);
    }

    componentWillUnmount() {
        ewars.z.unsubscribe("E", this._onChange);
        ewars.z.deregister("E");
    }

    _onChange = () => {
        this.setState(ewars.z.getState("E"));
    };

    _onDragAlive = () => {
        // this._trCell.style.background = "blue";
        // this._blCell.style.background = "blue";
        // this._brCell.style.background = "blue";
    };

    _onDragDead = () => {
        // this._trCell.style.background = "transparent";
        // this._blCell.style.background = "transparent";
        // this._brCell.style.background = "transparent";
    };

    _onMetaChange = (prop, value) => {

    };

    render() {
        let view;
        if (this.state.view == "DEFAULT") view = <DataSources/>;
        if (this.state.view == "ANALYSIS") view = <Analysis/>;

        let cmp = <Chart data={this.state.definition}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="300px" borderRight={true} borderTop={true}>
                        <ewars.d.Layout>
                            <ewars.d.Toolbar style={{paddingLeft: 0}}>
                                <div className="btn-group btn-tabs" style={{marginLeft: 0}}>
                                    <ewars.d.Button
                                        highlight={this.state.view == "DEFAULT"}
                                        onClick={() => {
                                            ewars.z.dispatch("E", "SET_VIEW", "DEFAULT");
                                        }}
                                        label="Data"/>
                                    <ewars.d.Button
                                        onClick={() => {
                                            ewars.z.dispatch("E", "SET_VIEW", "ANALYSIS");
                                        }}
                                        highlight={this.state.view == "ANALYSIS"}
                                        label="Analysis"/>
                                </div>
                            </ewars.d.Toolbar>
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    {view}
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>
                    </ewars.d.Cell>
                    <ewars.d.Cell borderTop={true} style={{background: "#F2F2F2"}}>
                        <ewars.d.Layout>
                            <ewars.d.Toolbar>
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        icon="fa-chart-series"/>
                                    <ewars.d.Button
                                        icon="fa-chart-pie"/>
                                    <ewars.d.Button
                                        icon="fa-map"/>
                                </div>
                            </ewars.d.Toolbar>
                            <DropArea
                                name="Columns"
                                target="COL"
                                activeDrop={this.state.activeDrop}
                                items={this.state.definition.columns || []}
                                icon="fa-list"/>
                            <DropArea
                                target="ROW"
                                name="Rows"
                                activeDrop={this.state.activeDrop}
                                items={this.state.definition.rows || []}
                                icon="fa-list fa-rotate"/>
                            <ewars.d.Row>
                                <ewars.d.Cell
                                    style={{background: "#FFFFFF", margin: "0px 8px 8px 8px", padding: "8px"}}>
                                    {cmp}
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Editor;