import Omni from "../../common/cmp/charting/Omni";
import Plot from "./Plot";

const DATE_RANGE = [
    "2016-01-01",
    "2016-01-02",
    "2016-01-03",
    "2016-01-04"
]

class TableData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {}
        };

        this._process(props);
    }

    componentWillReceiveProps(nextProps) {
        this._process(nextProps);
    }

    _process = (props) => {
        props.data.rows.forEach((item) => {
            console.log(item);
        })

        props.data.columns.forEach((item) => {
            console.log(item);
        })

    };

    render() {
        let headers = [];
        headers.push(<th>&nbsp;</th>);
        if (this.props.data.columns.length <= 0) {
            headers.push(<th>Abc</th>)
        } else {
            DATE_RANGE.forEach((item) => {
                headers.push(<th>{item}</th>)
            })
        }


        if (this.props.data.rows) {
            // what type of row item is it?
        }
        return (
            <table width="100%">
                <thead>
                    <tr>
                        {headers}
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>

                </tfoot>
            </table>
        )
    }

}

class Chart extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
    }

    render() {
        console.log(this.props);
        return (
            <div
                style={{width: "100%", height: "100%"}}
                ref={(el) => {
                    this._el = el;
                }}
                className="chart">
                <TableData data={this.props.data}/>

            </div>
        )
    }
}

export default Chart;