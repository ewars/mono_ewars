const FORM = [
    {
        _o: 0,
        n: "type",
        t: "SELECT",
        l: "Visualization Type",
        r: true,
        o: [
            ["SERIES", "Time Series"],
            ["CATEGORY", "Categorical"],
            ["MAP", "Map"],
            ["GAUGE", "Gauge"]
        ]
    }
]

class Style extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Panel>
                <ewars.d.SystemForm
                    definition={FORM}
                    data={{}}
                    vertical={true}
                    onChange={this._onChange}
                    enabled={true}/>
            </ewars.d.Panel>
        )
    }
}

export default Style;