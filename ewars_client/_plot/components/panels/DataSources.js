class IndicatorDataSource extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
            children: null
        }
    }

    render() {
        return (
            <div className="block">
                <div className="block-content">[ITEM]</div>
            </div>
        )
    }
}

export default class DataSources extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"
                            onClick={this.props.onAdd}/>
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="block-tree" style={{position: "relative"}}>
                                <div className="block hoverable">
                                    <div className="block-content" style={{padding: 0}}>
                                        <ewars.d.Row>
                                            <ewars.d.Cell width="5px" borderRight={true}>

                                            </ewars.d.Cell>
                                            <ewars.d.Cell style={{padding: 8}}>
                                                <ewars.d.Row>INdicator Nam4</ewars.d.Row>
                                                <ewars.d.Row>Indicator Location</ewars.d.Row>
                                                <ewars.d.Row>Time Period</ewars.d.Row>
                                            </ewars.d.Cell>
                                            <ewars.d.Cell width="20px" style={{paddingTop: 8, paddingBottom: 8}}>
                                                <ewars.d.Row><i className="fal fa-eye"></i></ewars.d.Row>
                                                <ewars.d.Row>

                                                </ewars.d.Row>
                                                <ewars.d.Row>
                                                    <i className="fal fa-trash"></i>
                                                </ewars.d.Row>
                                            </ewars.d.Cell>
                                        </ewars.d.Row>
                                    </div>
                                </div>
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>


            </ewars.d.Layout>
        )
    }
}