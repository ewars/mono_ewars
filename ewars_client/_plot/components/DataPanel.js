export default class DataPanel extends React.Component {
    render() {
        let height = "30%";
        if (!this.props.shown) height = "35px";

        let iconClass = "fa-chevron-down";
        if (!this.props.shown) iconClass = "fa-chevron-up";

        return (
            <ewars.d.Row height={height}>
                <ewars.d.Cell>
                    <ewars.d.Toolbar label="Data">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                onClick={this.props.toggle}
                                icon={iconClass}/>
                        </div>
                    </ewars.d.Toolbar>

                </ewars.d.Cell>
            </ewars.d.Row>
        )
    }
}