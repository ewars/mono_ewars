const INPUT_STYLE = {
    border: "none",
    borderColor: "transparent",
    background: "transparent"
};

export default class PlotTitle extends React.Component {
    render() {
        return (
            <div className="plot-title" style={{width: "50%", float: "left", marginTop: "-3px"}}>
                <input
                    style={INPUT_STYLE}
                    type="text"
                    onChange={(e) => {this.props.onTitleChange(e.target.value)}}/>
            </div>
        )
    }
}