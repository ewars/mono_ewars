const Moment = require('moment');


const Store = require("../utils/Store");

const SeriesChart = require("../../common/analysis/SeriesChart");
const CategoryChart = require("../../common/analysis/PieChart");
const Gauge = require("../../common/analysis/Gauge");
const Bullet = require("../../common/analysis/Bullet");
const Calendar = require("../../common/analysis/Calendar");
const Stream = require("../../common/analysis/Stream");
const Sunburst = require("../../common/analysis/Sunburst");
import ButtonGroup from "../../common/cmp/fields/ButtonGroup";

import ChartTypeSelector from "./ChartTypeSelector";
import PlotTitle from "./PlotTitle";
import DataPanel from "./DataPanel";
import DataSources from "./panels/DataSources";
import DataSelector from "./DataSelector";
import General from "./panels/General";
import Style from "./panels/Style";


const CHARTS = {
    SERIES: SeriesChart,
    CATEGORY: CategoryChart,
    GAUGE: Gauge,
    BULLET: Bullet,
    CALENDAR: Calendar,
    STREAM: Stream,
    SUNBURST: Sunburst
};


class Chart extends React.Component {
    chart = null;

    constructor(props) {
        super(props);

        this.state = {
            config: {}
        }

    }

    componentDidMount() {
        let ChartCmp = CHARTS[this.props.data.type];

        let config = ewars.copy(this.props.data);

        if (config.series) {
            config.series.forEach(function (item) {
                item.location = config.location;
                item.line_width = 2;
            }.bind(this))
        }

        if (this.chart) {
            this.chart.updateDefinition(config);
        } else {
            this.chart = ChartCmp(this.refs.chart, config, null, null, null, null);
        }

    }


    componentDidUpdate() {
        let config = ewars.copy(this.props.data);

        if (config.series) {
            config.series.forEach(function (item) {
                item.location = config.location;
                item.line_width = 2;
            }.bind(this))
        }

        this.chart.updateDefinition(config);
    }

    render() {
        return (
            <div className="chart" ref="chart" style={{padding: 16}}>
                <svg></svg>
            </div>
        )
    }
}

function isUsable(config) {
    let canUse = true;

    if (!config.type) canUse = false;

    if (config.type == "SERIES") {
        if (!config.interval) canUse = false;
        if (!config.series) canUse = false;
        if (config.series) {
            if (config.series.length <= 0) canUse = false;
        }
    }

    if (!config.start_date) canUse = false;
    if (!config.end_date) canUse = false;
    if (!config.location) canUse = false;

    if (config.type == "GAUGE") {
        if (!config.indicator) canUse = false;
        if (!config.reduction) canUse = false;
    }

    return canUse;
}

class ControlTabs extends React.Component {
    constructor(props) {
        super(props);

        this._buttons = [
            <ewars.d.ToolbarButton icon="fa-file" onClick={() => {
                this._on("GENERAL")
            }}/>,
            <ewars.d.ToolbarButton icon="fa-paint-brush" onClick={() => {
                this._on("STYLE");
            }}/>,
            <ewars.d.ToolbarButton icon="fa-database" onClick={() => {
                this._on("DATA")
            }}/>,
            <ewars.d.ToolbarButton icon="fa-table" onClick={() => {
                this._on("TABLE")
            }}/>,
            <ewars.d.ToolbarButton icon="fa-info" onClick={() => {
                this._on("INFO")
            }}/>,
            <ewars.d.ToolbarButton icon="fa-exclamation-triangle" onClick={() => {
                this._on("ERRORS")
            }}/>,
            <ewars.d.ToolbarButton icon="fa-map" onClick={() => {
                this._on("MAP")
            }}/>
        ]
    }

    _on = (view) => {
        console.log(view);
        this.props.onChangeView(view);
    };

    render() {
        return (
            <ewars.d.Toolbar buttons={this._buttons}/>
        )
    }
}

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "GENERAL",
            chartType: "LINE",
            mode: "CHART",
            chart: {
                type: "SERIES",
                series: [],
                start_date: Moment.utc().format("YYYY-MM-DD"),
                end_date: Moment.utc().format("YYYY-MM-DD"),
                height: "400px"
            },
            showData: false,
            showShade: false,
            shadeView: null
        }

    }

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({showBrowser: false})
    };

    _onChange = (prop, value) => {
        this.setState({
            chart: {
                ...this.state.chart,
                [prop]: value
            }
        })
    };

    _onChangeView = (view) => {
        console.log(view);
        this.setState({view: view});
    };

    _toggleData = () => {
        this.setState({
            showData: !this.state.showData
        })
    };

    _showDataSelector = () => {
        this.setState({
            showShade: true,
            shadeView: "DATA"
        })
    };

    render() {

        let chart;

        if (isUsable(this.state.chart)) {

            chart = <Chart
                data={this.state.chart}/>
        } else {
        }

        let view;

        if (this.state.view == "GENERAL") view = <General data={this.state.data}/>;
        if (this.state.view == "DATA") view = <DataSources onAdd={this._showDataSelector}/>;
        if (this.state.view == "STYLE") view = <Style data={this.state.data}/>;


        let shadeView;
        if (this.state.showShade) {
            if (this.state.shadeView == "DATA") shadeView = <DataSelector/>;
        }


        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="25%" borderRight={true}>
                        <ControlTabs
                            onChangeView={this._onChangeView}/>

                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <ewars.d.Panel>
                                    {view}
                                </ewars.d.Panel>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        <ewars.d.Toolbar>
                            <div className="btn-group pull-right">
                                <ewars.d.Button icon="fa-comment"/>
                                <ewars.d.Button icon="fa-share"/>
                                <ewars.d.Button icon="fa-file-pdf"/>
                                <ewars.d.Button icon="fa-print"/>
                                <ewars.d.Button icon="fa-save"/>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                [CHART]
                            </ewars.d.Cell>
                        </ewars.d.Row>
                        <DataPanel
                            toggle={this._toggleData}
                            shown={this.state.showData}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    toolbar={false}
                    shown={this.state.showShade}>
                    {shadeView}
                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default Component;