export default class ChartTypeSelector extends React.Component {
    render() {
        return (
            <div className="btn-group pull-right">
                <ewars.d.Button
                    icon="fa-line-chart"/>
                <ewars.d.Button
                    icon="fa-pie-chart"/>
                <ewars.d.Button
                    icon="fa-map"/>
                <ewars.d.Button
                    icon="fa-table"/>
                <ewars.d.Button
                    icon="fa-caret-down"/>
            </div>
        )
    }
}