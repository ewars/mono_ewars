import Form from "./form.DataSelector";
import Custom from "./Custom";

class Indicator extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            _show: false,
            items: [],
            _isLoaded: false
        }
    }

    _expand = () => {
        if (this.state._isLoaded) {
            this.setState({
                _show: !this.state._show
            })
        } else {
            this.refs.icon.className = "fa fa-cog fa-spin";
            ewars.tx("com.ewars.indicators", [this.props.data.id])
                .then((resp) => {
                    this.setState({
                        _isLoaded: true,
                        _show: true,
                        items: resp
                    })
                })
        }
    };

    _onDragStart = (e) => {
        e.dataTransfer.setData("n", JSON.stringify(this.props.data));
    };

    render() {
        console.log(this.props.data);
        let view;
        if (this.props.data.context == "FOLDER") {
            view = (
                <div className="block-content" onClick={this._expand}>
                    <ewars.d.Row>
                        <ewars.d.Cell width="15px" style={{display: "block"}}>
                            <i ref="icon"
                               className={"fa " + (this.state._show ? "fa-caret-down" : "fa-caret-right")}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            {__(this.props.data.name)}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            )
        }

        if (this.props.data.context == "INDICATOR") {
            view = (
                <div className="block-content" onDragStart={this._onDragStart} draggable={true}>
                    {__(this.props.data.name)}
                </div>
            )
        }

        return (
            <div className="block">
                {view}
                {this.state._show ?
                    <div className="block-children">
                        {this.state.items.map((item) => {
                            return <Indicator data={item}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class IndicatorTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            _show: false
        }

        ewars.tx('com.ewars.indicators', [null])
            .then((resp) => {
                console.log(resp);
                this.setState({items: resp})
            })
    }

    _show = () => {
        this.setState({
            _show: !this.state._show
        })
    };

    render() {
        let iconClass = this.state._show ? "fa-caret-down" : "fa-caret-right";
        return (
            <div className="block">
                <div className="block-content" onClick={this._show}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={15}>
                            <i className={"fa " + iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell width={15}>
                            <i className="fal fa-code-branch"></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            Indicators
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state._show ?
                    <div className="block-children">
                        {this.state.items.map((item) => {
                            return <Indicator data={item}/>
                        })}

                    </div>
                    : null}
            </div>
        )
    }
}

class FormNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    render() {
        let view;

        if (this.props.data.id) {
            view =  (
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell width="15px" style={{display: "block"}}>
                            <i className={"fa " + (this.state._show ? "fa-caret-right" : "fa-caret-down")}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            {__(this.props.data.name)}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            )
        }

        return (
            <div className="block">
                {view}
            </div>
        )
    }
}

class FormTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            _show: false,
            forms: [],
            _isLoaded: false
        }
    }

    _show = () => {
        if (this.state._isLoaded) {
            this.setState({
                _show: !this.state._show
            })
        } else {
            this.refs.icon.className = "fa fa-cog fa-spin";
            ewars.tx("com.ewars.query", ["form", ["id", "name"], {"status": {eq: "ACTIVE"}}, null, null, null, null])
                .then((resp) => {
                    this.setState({
                        forms: resp,
                        _isLoaded: true,
                        _show: true
                    })
                })
        }
    };

    render() {

        return (
            <div className="block">
                <div className="block-content" onClick={this._show}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={15}>
                            <i ref="icon" className={"fa " + (this.state._show ? "fa-caret-down" : "fa-caret-right")}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell width={15}>
                            <i className="fal fa-list"></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            Form Fields
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state._show ?
                    <div className="block-children">
                        {this.state.forms.map((item) => {
                            return <FormNode data={item}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class DataSetTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            _show: false,
            sets: []
        }
    }

    _show = () => {
        this.setState({
            _show: !this.state._shown
        })
    };

    render() {
        let iconClass = this.state._show ? "fa-caret-down" : "fa-caret-right";
        return (
            <div className="block">
                <div className="block-content" onClick={this._show}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={15}>
                            <i className={"fa " + iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell width={15}>
                            <i className="fal fa-database"></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            Data Set
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state._show ?
                    <div className="block-children">

                    </div>
                    : null}
            </div>
        )
    }
}

class DataSourceTree extends React.Component {
    constructor(props) {
        super(props)
    }

    _startDrag = (e) => {

    };

    _onVarDrag = (e) => {
        e.dataTransfer.setData("n", "VAR");
    };

    _onCustomDrag = (e) => {
        e.dataTransfer.setData("n", "CUSTOM");
    };

    render() {
        return (
            <div className="block-tree" style={{position: "relative"}}>
                <IndicatorTree/>
                <FormTree/>
                <DataSetTree/>
                <div className="block" draggable={true} onDragStart={this._onVarDrag}>
                    <div className="block-content">
                        <ewars.d.Row>
                            <ewars.d.Cell width={15}>
                                <i className="fal fa-gear"></i>
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                Variable
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                </div>
                <div className="block" draggable={true} onDragStart={this._onCustomDrag}>
                    <div className="block-content" onClick={this.props.onCustom}>
                        <ewars.d.Row>
                            <ewars.d.Cell width={15}>
                                <i className="fal fa-code"></i>
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                Custom Data
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                </div>
            </div>
        )
    }
}

export default class DataSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                location_spec: "SPECIFIC",
                period_source: "INHERIT",
                reduction: "SUM"
            }
        }
    }

    _onSelect = (source) => {

    };

    _onChange = (data, prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _custom = () => {
        this.setState({
            selectedType: "CUSTOM"
        })
    };

    render() {
        let view;

        view = (
            <ewars.d.SystemForm
                enabled={true}
                definition={Form}
                vertical={true}
                onChange={this._onChange}
                data={this.state.data}/>
        )

        if (this.state.selectedType == "CUSTOM") {
            view = <Custom/>
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Data Sources">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"
                            onClick={this._add}/>
                        <ewars.d.Button
                            icon="fa-close"
                            onClick={this._close}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="300px">
                        <DataSourceTree
                            onCustom={this._custom}
                            onSelect={this._onSelect}/>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}