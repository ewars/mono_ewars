export default {
    type: {
        type: "hidden"
    },
    data_set_id: {
        type: "select",
        label: {
            en: "Data Set"
        },
        options: [
            ["None", "None"]
        ]
    },
    colour: {
        type: "text",
        label: "Colour"
    },
    reduction: {
        type: "select",
        label: "Reduction",
        options: [
            ["SUM", "Sum"],
            ["AVG", "Average/Median"]
        ],
        conditions: {
            application: "all",
            rules: [
                ["type", "eq", "SLICE"]
            ]
        }
    }
}