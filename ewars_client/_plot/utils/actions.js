var AppDispatcher = require("./AppDispatcher");

var Actions = function (action, data) {
    AppDispatcher.handleViewAction({
        actionType: action,
        data: data
    })
};


module.exports = Actions;