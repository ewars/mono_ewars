import {
    Layout, Row, Cell,
    Toolbar,
    Button,
    Form
} from "../../common";
import { LocationField, DateField } from "../../common/fields";
import FormUtils from "../../common/utils/FormUtils";


const styles = {
    fieldWrap: {
        display: "block",
        marginBottom: "14px"
    }
};

class Report extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data:{}
        }
    }

    componentWillMount() {
        this.state.data = ewars.copy(this.props.data);
    }

    componentWillUnmount() {
        this.state.data = {};
    }

    _onChange = (data, prop, value, path) => {
        this.setState({
            data: {
                ...this.state.data,
                data: {
                    ...this.state.data.data,
                    [path]: value
                }
            }
        })
    };

    _onFieldChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _save = () => {
        let bl = new ewars.Blocker(null, "Updating submissions...");
        ewars.tx("com.ewars.collection.update", [this.state.data.uuid, this.state.data])
            .then((resp) => {
                bl.destroy();
                this.props.onCancel(true);
            })
    };


    render() {
        let date, location;

        if (this.props.data.location_id) {
            location = (
                <div style={styles.fieldWrap}>
                    <label htmlFor="">Location</label>
                    <LocationField
                        name="location_id"
                        onUpdate={this._onFieldChange}
                        value={this.state.data.location_id}/>
                </div>
            )
        }

        date = (
            <div style={styles.fieldWrap}>
                <label htmlFor="">Report Date</label>
                <DateField
                    name="data_date"
                    value={this.state.data.data_date}
                    onUpdate={this._onFieldChange}/>
            </div>
        );


        return (
            <Layout>
                <Toolbar label="Editor">
                    <div className="btn-group pull-right">
                        <Button
                            icon="fa-save"
                            color="green"
                            label="Save change(s)"
                            onClick={this._save}/>
                        <Button
                            icon="fa-close"
                            label="Close"
                            color="amber"
                            onClick={this.props.onCancel}/>
                    </div>
                </Toolbar>
                <Row>
                    <Cell>
                        <div style={{padding: 16}} className="ide-panel ide-panel-absolute ide-scroll">
                            {location}
                            {date}
                            <Form
                                readOnly={false}
                                definition={this.props.form.definition}
                                updateAction={this._onChange}
                                data={this.state.data.data}/>
                        </div>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}

export default Report;
