module.exports = {
    export_type: {
        type: "select",
        label: {
            en: "Data Type"
        },
        options: [
            ["FORM", "Form Submission Data"],
            ["INDICATOR", "Indicator Data"],
            ["ALERTS", "Alerts"]
        ],
        defaultValue: "FORM",
        help: {
            en: "Select the type of data that you would like to export, complete forms or calculated indicator data"
        }
    },
    alert_id: {
        type: "select",
        label: {
            en: "Alerts of Alarm Type"
        },
        multiple: true,
        optionsSource: {
            resource: "alarm",
            query: {status: {eq: true}},
            valSource: 'uuid',
            labelSource: 'name',
            prefixed: ['ALL', 'All']
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["export_type", "eq", "ALERTS"]
            ]
        }
    },
    form: {
        type: "select",
        label: {
            en: "Form"
        },
        optionsSource: {
            resource: "form",
            query: {
                status: {eq: "ACTIVE"}
            },
            orderby: ["name->>'en' ASC"],
            valSource: "id",
            labelSource: "name"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["export_type", "eq", "FORM"]
            ]
        },
        required: true
    },
    indicator: {
        type: "indicator",
        label: {
            en: "Indicator"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["export_type", "eq", "INDICATOR"]
            ]
        },
        required: true
    },
    time_interval: {
        type: "select",
        label: {
            en: "Time Interval"
        },
        options: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Annual"]
        ],
        defaultValue: "DAY",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["export_type", "eq", "INDICATOR"]
            ]
        },
        required: true
    },
    location_uuid: {
        type: "location",
        label: {
            en: "Location"
        },
        allowCheck: true,
        required: true
    },
    start_date: {
        type: "date",
        label: {
            en: "Start Date"
        },
        restrictCurYear: false,
        required: true
    },
    end_date: {
        type: "date",
        label: {
            en: "End Date"
        },
        restrictCurYear: false,
        required: true
    },
    date_format: {
        type: "select",
        label: {
            en: "Date Format"
        },
        defaultValue: "YYYY-MM-DD",
        options: [
            ["YYYY-MM-DD", "YYYY-MM-DD"]
        ],
        required: true
    }
};