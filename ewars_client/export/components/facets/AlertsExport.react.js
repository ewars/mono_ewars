import Moment from "moment";

var EXPORT_FORM = {
    start_date: {
        type: "date",
        label: "Start Date",
        required: true
    },
    end_date: {
        type: "date",
        label: "End date",
        required: true
    },
    location_id: {
        type: "location",
        label: "Location",
        required: true
    },
    alarm_id: {
        type: "select",
        label: "Alarm",
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            prefixed: [
                ['ALL', 'All alarms']
            ]
        },
        required: true
    }
};

class AlertsExport extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "LOCATIONS",
            location: [],
            export: {
                start_date: Moment.utc().format("YYYY-MM-DD"),
                end_date: Moment.utc().format("YYYY-MM-DD"),
                alarm_id: null,
                location_id: null
            },
            errors: null
        }
    }

    _onLocationAction = (action, data) => {
        let location = this.state.location;

        if (location.indexOf(data.uuid) >= 0) {
            let index = location.indexOf(data.uuid);
            location.splice(index, 1);
        } else {
            location.push.apply(location, [data.uuid]);
        }
        this.setState({
            location: location
        })
    };

    _onChange = (data, prop, value, additional) => {
        if (prop == "location_id" && window.user.role == "USER") {
            this.setState({
                ...this.state,
                errors: null,
                export: {
                    ...this.state.export,
                    location_id: additional[1].location_id,
                    form_id: additional[0].form_id
                }
            })
        } else {
            this.setState({
                ...this.state,
                errors: null,
                export: {
                    ...this.state.export,
                    [prop]: value
                }
            })
        }
    };

    _export = () => {

        let valid = true;
        let errors = {};

        if (!this.state.export.alarm_id) {
            valid = false;
            errors.alarm_id = "Please select a valid alarm";
        }
        if (!this.state.export.start_date) {
            valid = false;
            errors.start_date = "Please select a valid start date";
        }
        if (!this.state.export.end_date) {
            valid = false;
            errors.end_date = "Please select a valid end date";
        }
        if (!this.state.export.location_id) {
            valid = false;
            errors.location_id = "Please select a valid location"
        }

        if (!valid) {
            this.setState({
                errors: errors
            });
            return;
        }

        let bl = new ewars.Blocker(null, "Building data...");

        let definition = {
            alarm_id: this.state.export.alarm_id,
            start_date: this.state.export.start_date,
            end_date: this.state.export.end_date,
            location_id: this.state.export.location_id
        };
        ewars.tx('com.ewars.export', ["ALERTS", definition])
            .then(resp => {
                bl.destroy();
                if (resp.err) {
                    ewars.growl("No data available for the specified parameters");
                } else {
                    this._download(resp.fn);
                }
            })
    };

    _download = (uri) => {
        window.location.href = "http://" + ewars.domain + "/download/" + uri;
        // window.open("/download/" + uri);
    };

    render() {
        let data = ewars.copy(this.state.export);
        if (window.user.role == "USER") {
            EXPORT_FORM.location_id = {
                type: "assignment",
                label: "Assigned Location",
                required: true
            };
            data.location_id = {
                location_id: this.state.export.location_id,
                form_id: this.state.export.form_id
            }
        }

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 35}}>
                                <div className="ide-col">
                                    <div className="ide-tbar">
                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                label="Export"
                                                icon="fa-download"
                                                onClick={this._export}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="widget">
                                            <div className="widget-header"><span>Export Options</span></div>
                                            <div className="body">
                                                <ewars.d.Form
                                                    definition={EXPORT_FORM}
                                                    data={data}
                                                    readOnly={false}
                                                    errors={this.state.errors}
                                                    updateAction={this._onChange}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AlertsExport;
