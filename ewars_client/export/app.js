import ExportComponent from "./components/ExportComponent.react";

import {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Toolbar,
    Form
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Toolbar,
    Form
}

ReactDOM.render(
    <ExportComponent/>,
    document.getElementById("application")
);
