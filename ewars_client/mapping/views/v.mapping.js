import {
    LocationField,
    SelectField,
    FormFieldField,
    IndicatorField as IndicatorSelectorComponent,
    ColourThresholdsField as ColourThresholds
} from "../../common/fields";

import {
    Layout, Row, Cell, Toolbar, ActionGroup,
    PeriodSelector
} from "../../common";

import MapControl from "../controls/c.map.leaflet";
import MapBrowser from "../controls/c.browser";

const INPUT_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "left"
};

const STYLES = {
    MAP_WRAPPER: {
        position: "relative",
        display: "block",
        paddingTop: "8px"
    },
    MAP_INNER: {
        position: "absolute",
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: 0,
        overflowY: "auto",
        background: "#90ccff"
    },
    MAP_CONTROLS: {
        position: 'absolute',
        top: 20,
        left: 20,
        bottom: 20,
        width: "250px",
        background: "#ffffff",
        borderRadius: '6px',
        zIndex: 2,
        borderRight: "3px solid #90ccff"
    },
    MAP_ZOOM: {
        position: "absolute",
        top: 20,
        right: 20,
        width: "30px",
        background: "#F2F2F2",
        height: "60px",
        zIndex: 1
    },
    HEADER: {
        display: "block",
        textTransform: "uppercase",
        padding: "0 8px 0 8px",
        marginBottom: "5px",
        background: "#F2F2F2",
        paddingTop: "5px",
        paddingBottom: "5px",
        borderTop: "1px solid #CCCCCC"
    },
    LAYER_ITEM: {
        display: "block",
        marginBottom: "5px",
        marginLeft: "3px",
        marginRight: "3px",
        minHeight: "20px",
        border: "1px solid #CCCCCC",
        borderRadius: "3px",
        cursor: "pointer"
    },
    LAYER_INNER: {
        position: "relative",
        display: "block",
        paddingTop: "8px",
        paddingLeft: "18px"
    },
    LAYER_TITLE: {
        padding: "3px",
        color: "#333333"
    },
    PADDER: {
        display: "block",
        height: "20px"
    },
    FIELD: {
        display: "block",
        marginBottom: "8px",
        marginLeft: "8px",
        marginRight: "8px",
        fontSize: "11px"
    },
    LAYER_EDITOR: {
        position: "absolute",
        left: "250px",
        top: "20px",
        bottom: "20px",
        width: "350px",
        background: "#ffffff",
        zIndex: 1,
        borderTopRightRadius: "6px",
        borderBottomRightRadius: "6px",
        display: "flex",
        flexDirection: "column"
    },
    LAYER_SCROLL: {
        display: "block",
        overflowY: "auto",
        flex: 2,
        paddingLeft: "20px"
    },
    MAP_SCROLL: {
        display: "block",
        overflowY: "auto",
        flex: 2
    },
    PANEL_CONTROLS: {
        display: "block",
        height: "30px",
        minHeight: "38px"
    },
    HEADER_BUTTON: {
        float: "right",
        padding: "2px 0",
        marginTop: "-3px",
        fontSize: "10px",
        height: "auto",
        minWidth: "none",
        background: "transparent",
        border: "none"
    },
    LABEL: {
        padding: "8px",
        fontSize: "10px"
    },
    THRESH_ITEM: {
        display: "block",
        border: "1px solid #CCCCCC",
        marginBottom: "5px",
        marginLeft: "8px",
        marginRight: "8px"
    }
};

const TABLE_STYLE = {
    root: {
        borderBottom: "1px solid rgba(0,0,0,0.2)"
    },
    t_container_style: {
        l: "Container Styling",
        t: "H"
    },
    border: {
        l: "Border",
        reg: "{size} {style} {color}",
        // This property can break out into other properties
        c: {
            size: ["SIZE", "0px", "Stroke"],
            style: ["OPTION", "solid", "Style", "solid|dotted|dashed"],
            color: ["COLOR", "#000000", "Color"]
        },
        __: [
            "borderTop",
            "borderRight",
            "borderBottom",
            "borderLeft"
        ]
    },
    margin: {
        l: "Margin",
        reg: "{top} {right} {bottom} {left}",
        __: [
            "marginTop",
            "marginRight",
            "marginBottom",
            "marginLeft"
        ]
    },
    padding: {
        l: "Padding",
        reg: "{top} {right} {bottom} {left}",
        __: [
            "paddingTop",
            "paddingRight",
            "paddingBottom",
            "paddingLeft"
        ]
    },
    boxShadow: {
        l: "Box Shadow",
        reg: "{xOffset} {yOffset} {blur} {color}",
        c: {
            xOffset: ["SIZE", "0px", "X"],
            yOffset: ["SIZE", "0px", "y"],
            blur: ["SIZE", "0px", "blur"],
            color: ["COLOR", "0px", "color"]
        }
    },
    backgroundColor: {
        l: "Bgd Colour",
        reg: "{color}",
        c: {
            color: ["COLOR", "transparent", "Color"]
        }
    }
};

const LABEL_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    borderRight: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "right",
    width: "25%",
    background: "rgba(0,0,0,0.2)"
};


class Placeholder extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="placeholder" style={{color: "#CCCCCC"}}>
                <i className="fal fa-map" style={{fontSize: "60px"}}></i>
                <div className="clearer"></div>
                <h3>Mapping</h3>
            </div>
        )
    }
}

const LAYER_ACTIONS = [
    ['fa-eye', 'TOGGLE', 'Toggle'],
    ['fa-pencil', 'EDIT', 'Edit layer'],
    ['fa-trash', 'TRASH', 'Delete layer']
];

const DATA_FIELDS = [
    "type",
    "source",
    "ind_id",
    "lid",
    "sti",
    "group",
    "formula"
]

class LayerItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        this.props.onAction(action, this.props.data);
    };

    render() {
        return (
            <div style={STYLES.LAYER_ITEM}>
                <Row>
                    <Cell style={{padding: "4px", fontSize: "10px"}}>
                        {this.props.data.title || "New Layer"}
                    </Cell>
                    <Cell style={{display: "block"}}>
                        <ActionGroup
                            right={true}
                            onAction={this._action}
                            actions={LAYER_ACTIONS}/>
                    </Cell>
                </Row>
            </div>
        )
    }
}

const THRESH_ACTIONS = [
    ['fa-trash', 'DELETE', 'Delete threshold']
]

class ThresholdItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={STYLES.THRESH_ITEM}>
                <Row>
                    <Cell>
                        <button>∞</button>
                    </Cell>
                    <Cell>
                        <input
                            placeholder="From"
                            type="number"/>
                    </Cell>
                    <Cell>
                        <input
                            placeholder="To"
                            type="number"/>
                    </Cell>
                    <Cell>
                        <button>∞</button>
                    </Cell>
                    <Cell style={{display: "block"}}>
                        <input
                            style={{
                                float: "right"
                            }}
                            type="color"/>
                    </Cell>
                    <Cell width="25px" style={{display: "block"}}>
                        <ActionGroup
                            actions={THRESH_ACTIONS}
                            height="25px"
                            right={true}/>
                    </Cell>
                </Row>
            </div>
        )
    }
}

class FieldItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Row style={{marginBottom: "5px"}}>
                <Cell style={{paddingRight: "8px", paddingLeft: "8px"}}>
                    {this.props.children}
                </Cell>
                <Cell width="100px" style={STYLES.LABEL}>
                    {this.props.label}
                </Cell>
            </Row>
        )
    }
}

class ViewMapping extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            hidden: [],
            layer: null,
            showMenu: true
        }

        if (!ewars.g.forms) {
            ewars.tx("com.ewars.mapping.layer_options", [])
                .then(res => {
                    ewars.g.forms = res.forms;
                    ewars.g.location_types = res.location_types;
                    ewars.g.location_groups = res.groups;
                })
                .catch(err => {
                    ewars.error("Could not retrieve base app metadata");
                })
        }

        this._pos = [0, 0];

        ewars.subscribe("POS_UPDATED", this._posUpdated);
    }

    _posUpdated = (data) => {
        console.log(data);
        this._pos = data;
    };

    componentWillUnmount() {
        ewars.unsubscribe("POS_UPDATED", this._posUpdated);
    }


    _layerAction = (action, data) => {
        switch (action) {
            case 'EDIT':
                this.setState({
                    layer: ewars.copy(data)
                });
                break;
            case "TRASH":
                let layers = ewars.copy(this.state.data.layers || []);
                let rIndex;
                layers.forEach((item, index) => {
                    if (item.uuid == data.uuid) {
                        rIndex = index;
                    }
                })
                layers.splice(rIndex, 1);
                this.setState({
                    ...this.state,
                    data: {
                        ...this.state.data,
                        layers: layers
                    }
                }, () => {
                    ewars.emit("UPDATE_MAP", this.state.data);
                });
                break;
            default:
                break;
        }
    };

    _closeLayer = () => {
        this.setState({
            layer: null
        })
    };

    _addLayer = () => {
        let newLayer = {
            uuid: ewars.utils.uuid(),
            type: "TYPE",
            source: "INDICATOR"
        }
        this.setState({
            layer: newLayer
        })
    };

    _layerChange = (e) => {
        this.setState({
            ...this.state,
            layer: {
                ...this.state.layer,
                [e.target.name]: e.target.value
            }
        })
    };

    _showMenu = () => {
        this.setState({
            showMenu: true
        })
    };

    _closeMenu = () => {
        this.setState({
            showMenu: false
        })
    };

    _createNew = () => {
        let isFirst = false;
        if (!this.state.data) isFirst = true;
        this.setState({
            showMenu: false,
            data: {
                layers: [],
                annotations: [],
                title: "New map",
                shared: false,
                description: "",
                created_by: window.user.id,
                config: {
                    ctrl_zoom: true,
                    bgd_color: "blue"
                }
            }
        }, () => {
            if (!isFirst) ewars.emit("CHANGE_MAP", this.state.data);
        })
    };

    _rootChange = (e) => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                [e.target.name]: e.target.value
            }
        })
    };

    _configChange = (e) => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                config: {
                    ...this.state.config,
                    [e.target.name]: e.target.value
                }
            }
        })
    };


    _saveLayer = () => {
        let layers = this.state.data.layers;
        let lIndex;
        layers.forEach((item, index) => {
            if (item.uuid == this.state.layer.uuid) lIndex = index;
        })

        let editedLayer = this.state.layer.uuid;

        let dataChanged = false,
            styleChange = false;

        if (lIndex != null) {
            layers[lIndex] = ewars.copy(this.state.layer);
        } else {
            layers.push(ewars.copy(this.state.layer))
        }
        this.setState({
            layer: null,
            data: {
                ...this.state.data,
                layers: layers
            }
        }, () => {
            ewars.emit("UPDATE_MAP", [this.state.data, [editedLayer]]);
        })
    };

    _save = () => {
        let blocker = new ewars.Blocker(null, "Saving map...");
        if (this.state.data.uuid) {
            ewars.tx("com.ewars.mapping.update", [this.state.data.uuid, this.state.data])
                .then(res => {
                    blocker.destroy();
                    this.setState({
                        data: res
                    })
                })
                .catch(err => {
                    blocker.destroy();
                    ewars.error("There was an error saving this map");
                })
        } else {
            ewars.tx("com.ewars.mapping.create", [this.state.data])
                .then(res => {
                    blocker.destroy();
                    this.setState({
                        data: res
                    })
                })
                .catch(err => {
                    blocker.destroy();
                    ewars.error("There was an error saving this map")
                })
        }
    };

    _delete = (id) => {
        let blocker = new ewars.Blocker(null, "Deleting map...");
        ewars.tx("com.ewars.mapping.delete", [id])
            .then(res => {
                blocker.destroy();
                ewars.growl("Map deleted");
            })
            .catch(err => {
                blocker.destroy();
                ewars.error("An error occurred while deleting this map");
            })
    };

    _openMap = (data) => {
        let isFirst = false;
        if (!this.state.data) isFirst = true;
        this.setState({
            layer: null,
            showMenu: false,
            data: data
        }, () => {
            if (!isFirst) ewars.emit("CHANGE_MAP", data);
        })
    };

    _setPosition = () => {
        console.log(this._pos);
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                config: {
                    ...this.state.data.config,
                    center: {
                        zoom: this._pos[0],
                        lat: this._pos[1].lat,
                        lng: this._pos[1].lng
                    }
                }
            }
        })
    };

    render() {
        let formFieldOptions = [];

        if (this.state.layer && this.state.layer.form_id) {
            let form = ewars.g.forms.filter(item => {
                return item.id == this.state.layer.form_id;
            })[0] || null;

            if (form) {
                for (let i in form.definition) {
                    if (form.definition[i].type == "lat_long") {
                        formFieldOptions.push([
                            form.definition[i].name,
                            __(form.definition[i].label)
                        ])
                    }
                }
            }
        }

        return (
            <Layout>
                {this.state.data && window.user.id != this.state.data.created_by ?
                    <Toolbar label={this.state.data.title}>
                        <div
                            className="btn-group pull-right">
                            <button onClick={this._showMenu}><i className="fal fa-list"></i>&nbsp;Menu</button>
                        </div>
                    </Toolbar>
                    : null}
                <Cell borderTop={true} style={{position: "relative"}}>

                    {this.state.data && window.user.id == this.state.data.created_by ?
                        <div style={STYLES.MAP_CONTROLS} className="editor-controls">
                            <div style={STYLES.PANEL_CONTROLS}>
                                <div
                                    style={{marginTop: "8px"}}
                                    className="btn-group pull-right">
                                    <button onClick={this._save}><i className="fal fa-save"></i></button>
                                    <button onClick={this._showMenu}><i className="fal fa-folder-open"></i></button>
                                </div>
                            </div>
                            <div style={STYLES.MAP_SCROLL}>
                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-cog"></i>&nbsp;Settings
                                </div>

                                <FieldItem
                                    label="Title">
                                    <input
                                        onChange={this._rootChange}
                                        name="title"
                                        value={this.state.data.title || ""}
                                        type="text"/>
                                </FieldItem>

                                <FieldItem
                                    label="Shared">
                                    <select
                                        onChange={this._rootChange}
                                        name="shared"
                                        value={this.state.data.shared}>
                                        <option value="NO">Not shared</option>
                                        <option value="YES">Shared</option>
                                    </select>
                                </FieldItem>

                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-align-justify"></i>&nbsp;Layers
                                    <button
                                        onClick={this._addLayer}
                                        style={STYLES.HEADER_BUTTON}><i className="fal fa-plus"></i></button>
                                </div>

                                {this.state.data.layers.map(item => {
                                    return (
                                        <LayerItem
                                            data={item}
                                            onAction={this._layerAction}/>
                                    )
                                })}


                                {/*<div style={STYLES.HEADER}>*/}
                                {/*<i className="fal fa-tags"></i>&nbsp;Annotations*/}
                                {/*<button*/}
                                {/*style={STYLES.HEADER_BUTTON}><i className="fal fa-plus"></i></button>*/}
                                {/*</div>*/}

                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-paint-brush"></i>&nbsp;Style
                                </div>

                                <FieldItem
                                    label="Background color">
                                    <input
                                        onChange={this._configChange}
                                        name="bgd_color"
                                        value={this.state.data.bgd_color || ""}
                                        type="color"/>
                                </FieldItem>

                                {/*<FieldItem*/}
                                {/*label="Time controls">*/}
                                {/*<select*/}
                                {/*onChange={this._configChange}*/}
                                {/*name="ctrl_time"*/}
                                {/*value={this.state.data.ctrl_time}>*/}
                                {/*<option value={false}>Hide</option>*/}
                                {/*<option value={true}>Show</option>*/}
                                {/*</select>*/}
                                {/*</FieldItem>*/}

                                {/*<FieldItem*/}
                                {/*label="Zoom controls">*/}
                                {/*<select*/}
                                {/*onChange={this._configChange}*/}
                                {/*name="ctrl_zoom"*/}
                                {/*value={this.state.data.ctrl_zoom}>*/}
                                {/*<option value={false}>Hide</option>*/}
                                {/*<option value={true}>Show</option>*/}
                                {/*</select>*/}
                                {/*</FieldItem>*/}

                                {/*<FieldItem*/}
                                {/*label="Layer controls">*/}
                                {/*<select*/}
                                {/*onChange={this._configChange}*/}
                                {/*name="ctrl_layers"*/}
                                {/*value={this.state.data.ctrl_layers}>*/}
                                {/*<option value={false}>Hide</option>*/}
                                {/*<option value={true}>Show</option>*/}
                                {/*</select>*/}
                                {/*</FieldItem>*/}

                                {/*<FieldItem*/}
                                {/*label="Mapbox Layer">*/}
                                {/*<select*/}
                                {/*onChange={this._configChange}*/}
                                {/*value={this.state.data.mapbox}*/}
                                {/*name="mapbox" id="">*/}
                                {/*<option value={true}>Yes</option>*/}
                                {/*<option value={false}>No</option>*/}
                                {/*</select>*/}
                                {/*</FieldItem>*/}

                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-map"></i>&nbsp;Default
                                </div>

                                <FieldItem
                                    label="Default center">
                                    <button onClick={this._setPosition}>
                                        <i className="fal fa-map-marker"></i>&nbsp;Set position
                                    </button>
                                </FieldItem>

                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-gamepad"></i>&nbsp;Controls
                                </div>

                            </div>

                        </div>
                        : null}
                    {this.state.layer ?
                        <div style={STYLES.LAYER_EDITOR} className="editor-controls">
                            <div style={STYLES.PANEL_CONTROLS}>
                                <div
                                    style={{paddingTop: "5px"}}
                                    className="btn-group pull-right">
                                    <button onClick={this._saveLayer}>
                                        <i className="fal fa-save"></i>
                                    </button>
                                    <button onClick={this._closeLayer}>
                                        <i className="fal fa-times"></i>
                                    </button>
                                </div>
                            </div>

                            <div style={STYLES.LAYER_SCROLL}>

                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-map"></i>&nbsp;Visualization
                                </div>

                                <FieldItem
                                    label="Layer title">
                                    <input
                                        onChange={this._layerChange}
                                        name="title"
                                        value={this.state.layer.title || ""}
                                        type="text"/>
                                </FieldItem>

                                <FieldItem
                                    label="Visualization">
                                    <div className="select-custom">
                                        <select
                                            onChange={this._layerChange}
                                            value={this.state.layer.type || ""}
                                            name="type" id="">
                                            <option value="TYPE">Locations of Type</option>
                                            <option value="SINGLE">Single Location</option>
                                            <option value="FORM_POINT">Collected Lat/Lng</option>
                                            <option value="GROUP">Locations of Group</option>
                                            <option value="GEOJSON">Custom geojson</option>
                                        </select>
                                    </div>
                                </FieldItem>

                                {["TYPE", "SINGLE"].indexOf(this.state.layer.type) >= 0 ?
                                    <FieldItem
                                        label="Location">
                                        <LocationField
                                            onChange={(prop, value) => {
                                                this._layerChange({
                                                    target: {
                                                        name: "lid",
                                                        value: value
                                                    }
                                                })
                                            }}
                                            value={this.state.layer.lid || null}/>
                                    </FieldItem>
                                    : null}

                                {this.state.layer.type == "TYPE" ?
                                    <FieldItem
                                        label="Location type">
                                        <div className="select-custom">
                                            <select
                                                onChange={this._layerChange}
                                                value={this.state.layer.loc_type || ""}
                                                name="loc_type" id="">
                                                <option value=""></option>
                                                {ewars.g.location_types.map(item => {
                                                    return (
                                                        <option value={item.id}>{item.name.en || item.name}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                    </FieldItem>
                                    : null}

                                {this.state.layer.type == "FORM_POINT" ?
                                    <FieldItem
                                        label="Source Field">
                                        <FormFieldField
                                            value={this.state.layer.form_field}
                                            required_type="lat_long"
                                            config={{
                                                required_type: "lat_long"
                                            }}
                                            onUpdate={(prop, value) => {
                                                this._layerChange({
                                                    target: {
                                                        name: "form_field",
                                                        value: value
                                                    }
                                                })
                                            }}/>
                                    </FieldItem>
                                    : null}

                                {this.state.layer.type == "GROUP" ?
                                    <FieldItem
                                        label="Group">
                                        <div className="select-custom">
                                            <select
                                                onChange={this._layerChange}
                                                value={this.state.layer.group || ""}
                                                name="group" id="">
                                                {ewars.g.location_groups.map(item => {
                                                    return <option value={item}>{item}</option>
                                                })}
                                            </select>
                                        </div>
                                    </FieldItem>
                                    : null}

                                {["TYPE", "GROUP", "SINGLE"].indexOf(this.state.layer.type) >= 0 ?
                                    <div>
                                        <div style={STYLES.HEADER}>
                                            <i className="fal fa-database"></i>&nbsp;Data Source
                                        </div>

                                        <FieldItem
                                            label="Data Source">
                                            <div className="select-custom">
                                                <select
                                                    value={this.state.layer.source}
                                                    onChange={this._layerChange}
                                                    name="source" id="">
                                                    <option value="INDICATOR">Indicator</option>
                                                    <option value="COMPLEX">Calc.</option>
                                                    <option value="NONE">No data</option>
                                                </select>
                                            </div>
                                        </FieldItem>

                                        {this.state.layer.source != "NONE" ?
                                            <FieldItem
                                                label="Indicator">
                                                <IndicatorSelectorComponent
                                                    value={this.state.layer.ind_id}
                                                    onUpdate={(prop, value) => {
                                                        this._layerChange({
                                                            target: {
                                                                name: "ind_id",
                                                                value: value
                                                            }
                                                        })
                                                    }}/>
                                            </FieldItem>
                                            : null}

                                        {this.state.layer.source == "COMPLEX" ?
                                            <FieldItem
                                                label="Formula">
                                                <input
                                                    onChange={this._layerChange}
                                                    value={this.state.layer.formula}
                                                    name="formula"
                                                    placeholder="Formula..."
                                                    type="text"/>
                                            </FieldItem>
                                            : null}

                                        {this.state.layer.source != "NONE" ?
                                            <FieldItem
                                                label="Source Period">
                                                <PeriodSelector
                                                    onChange={(prop, value) => {
                                                        this._layerChange({
                                                            target: {
                                                                name: "period",
                                                                value: value
                                                            }
                                                        })
                                                    }}
                                                    vertical={true}/>
                                            </FieldItem>
                                            : null}

                                        <div style={STYLES.HEADER}>
                                            <i className="fal fa-sliders-h"></i>&nbsp;Thresholds
                                        </div>

                                        <ColourThresholds
                                            onUpdate={(prop, value) => {
                                                this._layerChange({
                                                    target: {
                                                        name: "scales",
                                                        value: value
                                                    }
                                                });
                                            }}
                                            value={this.state.layer.scales || []}/>

                                        <div style={{display: "block", height: "8px"}}></div>

                                        <FieldItem
                                            label="Show legend">
                                            <div className="select-custom">
                                                <select
                                                    onChange={this._layerChange}
                                                    value={this.state.layer.leg}
                                                    name="ctrl_scale" id="">
                                                    <option value={false}>No</option>
                                                    <option value={true}>Yes</option>
                                                </select>
                                            </div>
                                        </FieldItem>

                                        <FieldItem
                                            label="Legend pos.">
                                            <div className="select-custom">
                                                <select
                                                    onChange={this._layerChange}
                                                    value={this.state.layer.scale_pos}
                                                    name="scale_pos" id="">
                                                    <option value="bottomright">Bottom-Right</option>
                                                    <option value="bottomcenter">Bottom-Center</option>
                                                    <option value="bottomleft">Bottom-Left</option>
                                                    <option value="middlecenter">Middle-Center</option>
                                                    <option value="topleft">Top-Left</option>
                                                    <option value="topcenter">Top-Center</option>
                                                    <option value="topright">Top-Right</option>
                                                    <option value="middlecenter">Middle-Center</option>
                                                </select>
                                            </div>
                                        </FieldItem>

                                    </div>
                                    : null}

                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-paint-brush"></i>&nbsp;Style
                                </div>

                                <FieldItem
                                    label="Stroke color">
                                    <input
                                        onChange={this._layerChange}
                                        name="stroke_color"
                                        value={this.state.layer.stroke_color}
                                        type="color"/>
                                </FieldItem>

                                <FieldItem
                                    label="Stroke width">
                                    <input
                                        onChange={this._layerChange}
                                        name="stroke_width"
                                        value={this.state.layer.stroke_width}
                                        type="number"/>
                                </FieldItem>

                                <FieldItem
                                    label="Stroke style">
                                    <div className="select-custom">
                                        <select
                                            onChange={this._layerChange}
                                            value={this.state.layer.stroke_style}
                                            name="stroke_style" id="">
                                            <option value="">Solid</option>
                                            <option value="">Dotted</option>
                                            <option value="">Dashed</option>
                                        </select>
                                    </div>
                                </FieldItem>

                                <FieldItem
                                    label="Fill color">
                                    <input
                                        onChange={this._layerChange}
                                        value={this.state.layer.fill_color}
                                        name="fill_color"
                                        type="color"/>
                                </FieldItem>

                                <FieldItem label="Fill opacity">
                                    <input
                                        onChange={this._layerChange}
                                        name="fill_opacity"
                                        value={this.state.layer.fill_opacity}
                                        min={0}
                                        max={1}
                                        type="number"/>
                                </FieldItem>

                                <FieldItem
                                    label="Marker type">
                                    <div className="select-custom">
                                        <select
                                            onChange={this._layerChange}
                                            value={this.state.layer.marker_type}
                                            name="marker_type" id="">
                                            <option value="circle">Circle</option>
                                            <option value="cross">Cross</option>
                                            <option value="diamond">Diamond</option>
                                            <option value="square">Square</option>
                                            <option value="triangle">Triangle</option>
                                            <option value="wye">Wye</option>
                                        </select>
                                    </div>
                                </FieldItem>

                                <FieldItem
                                    label="Marker size">
                                    <input
                                        onChange={this._layerChange}
                                        value={this.state.marker_size}
                                        name="marker_size"
                                        type="number"/>
                                </FieldItem>





                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-paint-brush"></i>&nbsp;Labels
                                </div>

                                <FieldItem
                                    label="Label type">
                                    <div className="select-custom">
                                        <select
                                            value={this.state.layer.lab_type}
                                            onChange={this._layerChange}
                                            name="lab_type" id="">
                                            <option value="IN">In-place</option>
                                            <option value="NUM">Numbered</option>
                                            <option value="NONE">No labels</option>
                                        </select>
                                    </div>
                                </FieldItem>

                                {this.state.layer.lab_type != 'NONE' ?
                                    <FieldItem label="Label value">
                                        <div className="select-custom">
                                            <select
                                                value={this.state.layer.lab_val}
                                                onChange={this._layerChange}
                                                name="lab_val" id="">
                                                <option value={false}>Hide</option>
                                                <option value={true}>Show</option>
                                            </select>
                                        </div>
                                    </FieldItem>
                                    : null}

                                    <FieldItem label="Label color">
                                        <input
                                            onChange={this._layerChange}
                                            name="label_color"
                                            value={this.state.layer.label_color || "#000000"}
                                            type="color"/>
                                    </FieldItem>

                                <FieldItem label="Label size">
                                    <input
                                        onChange={this._layerChange}
                                        name="label_size"
                                        value={this.state.layer.label_size || "12"}
                                        type="number"/>
                                </FieldItem>

                                <div style={STYLES.HEADER}>
                                    <i className="fal fa-gamepad"></i>&nbsp;Interactivity
                                </div>

                                <FieldItem
                                    label="onClick">
                                    <div className="select-custom">
                                        <select
                                            onChange={this._layerChange}
                                            value={this.state.layer.onClick}
                                            name="onClick">
                                            <option value="">Do nothing</option>
                                            <option value="">Show time series</option>
                                            <option value="">Show chart</option>
                                            <option value="">Show measure</option>
                                            <option value="">Custom</option>
                                        </select>
                                    </div>
                                </FieldItem>
                            </div>
                        </div>
                        : null}

                    <div style={STYLES.MAP_INNER}>
                        {this.state.data ?
                            <MapControl
                                data={this.state.data || {}}/>
                            : null}
                    </div>

                    {this.state.showMenu ?
                        <MapBrowser
                            onSelect={this._openMap}
                            onCloseMenu={this._closeMenu}
                            onCreateNew={this._createNew}/>
                        : null}

                </Cell>
            </Layout>
        )
    }
}

export default ViewMapping;
