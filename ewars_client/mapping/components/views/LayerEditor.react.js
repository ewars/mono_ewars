import { Form, Button } from "../../../common";

import formSpec from "../../constants/series_form";

const form = {
    indicator: {
        type: "indicator",
        label: {
            en: "Indicator"
        }
    },
    parent_id: {
        type: "location",
        label: {
            en: "Parent Location"
        }
    },
    site_type_id: {
        type: "select",
        label: "Target Location Type",
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    },
    start_date: {
        type: "date",
        label: "Start Date"
    },
    end_date: {
        type: "date",
        label: "End Date"
    }
};

var Variable = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        return <div className="dummy"></div>
    }
});

var LayerEditor = React.createClass({
    getInitialState: function () {
        return {}
    },

    _update: function (form, prop, value) {
        this.props.onUpdate(this.props.index, prop, value);
    },

    render: function () {


        var vars = _.map(this.props.data.series, function (item) {
            return <Variable data={item}/>
        }, this);

        return (
            <div className="layer-editor">
                <h3>Layer Editor</h3>
                <Form
                    definition={form}
                    data={this.props.data}
                    updateAction={this._update}
                    readOnly={false}/>
            </div>
        )
    }
});


export default LayerEditor;
