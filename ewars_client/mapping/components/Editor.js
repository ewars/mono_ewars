import { Form, Button } from "../../common";
import Map from "../../common/analysis/Map";
import Map2 from "../../common/analysis/Map2";
import CONSTANTS from "../../common/constants";

const FORM = {
    name: {
        type: "text",
        label: "Name"
    },
    description: {
        type: "textarea",
        label: "Description"
    },
    source_type: {
        nameOverride: "definition.source_type",
        type: "select",
        label: "Data Source",
        options: [
            ["SLICE", "Indicator"],
            ["SLICE_COMPLEX", "Complex"]
        ]
    },
    indicator: {
        nameOverride: "definition.indicator",
        type: "indicator",
        label: "Indicator",
        conditions: {
            application: "all",
            rules: [
                ["definition.source_type", "eq", "SLICE"]
            ]
        }
    },
    series: {
        nameOverride: "definition.series",
        type: "indicator_definition",
        label: "Complex Data Source",
        conditions: {
            application: "all",
            rules: [
                ["definition.source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    location: {
        nameOverride: "definition.location",
        type: "location",
        label: "Location"
    },
    site_type_id: {
        nameOverride: "definition.site_type_id",
        type: "select",
        label: "Site Type",
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    },
    reduction: {
        nameOverride: "definition.reduction",
        type: "select",
        label: "Aggregation",
        options: [
            ["SUM", "Sum"],
            ["AVG", "Median/Average"]
        ]
    },
    start_date: {
        nameOverride: "definition.start_date",
        type: "date",
        label: "Start Date",
        date_type: "DAY"
    },
    end_date: {
        nameOverride: "definition.end_date",
        type: "date",
        label: "End Date",
        date_type: "DAY"
    },
    shared: {
        type: "switch",
        label: "Shared"
    }
};

class Progress extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="mask">
                <div className="map-loading">
                    {this.props.label}
                </div>
            </div>
        )
    }
}

class Editor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dirty: false,
            edit: false
        }
    }

    componentWillMount() {
        if (!this.props.data.uuid) this.state.edit = true;
    }

    componentDidMount() {
        let width = this.refs.map.clientWidth,
            height = this.refs.map.clientHeight;
        let newDef = {
            ...this.props.data.definition,
            bgd_colour: "#F2F2F2",
            b_labels: true,
            lbl_style: "DEFAULT",
            height: height,
            width: width
        };
        this.map = new ewars.a.MAP(this.refs.map, newDef);
    }


    _edit = () => {
        this.setState({
            edit: true
        })
    };

    _onChange = (data, prop, value, path) => {
        this.setState({dirty: true});
        ewars.z.dispatch("MAP", "SET_MAP_PROP", {
            prop: prop,
            value: value
        })
    };

    componentDidUpdate() {
        if (this.map) {
            let width = this.refs.wrapper.clientWidth,
                height = this.refs.wrapper.clientHeight;

            if (this.state.edit) width = width - 350;

            let newDef = {
                ...this.props.data.definition,
                bgd_colour: "#F2F2F2",
                b_labels: true,
                lbl_style: "DEFAULT",
                height: height,
                width: width
            };
            this.map.updateDefinition(newDef);
        }
    }

    _update = () => {
        this.setState({dirty: false});
    };

    _addScale = () => {
        this.setState({dirty: true});
        ewars.z.dispatch("MAP", "MAP_ADD_SCALE", {
            data: [0, 0, "#FFFFFF"]
        })
    };

    removeScale = (index) => {
        this.setState({dirty: true});
        ewars.z.dispatch("MAP", "MAP_REMOVE_SCALE", {
            index: index
        })
    };

    _thresholdChange = (e) => {
        this.setState({
            dirty: true
        });
        let name = parseInt(e.target.name.split("_")[0]);
        let index = parseInt(e.target.name.split("_")[1]);

        let data = ewars.copy(this.props.data.definition.scales);

        data[name][index] = e.target.value;
        ewars.z.dispatch("MAP", "MAP_SET_SCALES", {
            data: data
        })
    };

    _save = () => {
        let bl = new ewars.Blocker(null, "Saving map...");

        if (this.props.data.uuid) {
            ewars.tx('com.ewars.map.update', [this.props.data.uuid, this.props.data])
                .then((resp) => {
                    bl.destroy();
                    ewars.z.dispatch("MAP", "SET_MAP", {data: resp});
                    ewars.growl("Map updated");
                })
        } else {
            ewars.tx("com.ewars.map.create", [this.props.data])
                .then((resp) => {
                    bl.destroy();
                    ewars.z.dispatch("MAP", "SET_MAP", {data: resp});
                    ewars.growl("Map saved");
                })
        }
    };

    _delete = () => {
        ewars.prompt("fa-trash", "Delete Map?", "Are you sure you want to delete this map?", function () {
            let bl = new ewars.Blocker(null, "Deleting map...");
            ewars.tx("com.ewars.map.delete", [this.props.data.uuid])
                .then((resp) => {
                    bl.destroy();
                    ewars.z.dispatch("MAP", "SET_MAP", {
                        data: null
                    });
                    ewars.growl("Map deleted");
                })
        }.bind(this))
    };

    _close = () => {
        ewars.z.dispatch("MAP", "SET_MAP", {
            data: null
        })
    };


    render() {
        let canEdit = false;
        if (this.props.data.created_by == window.user.id) canEdit = true;

        return (
            <ewars.d.Layout ref="wrapper">
                <ewars.d.Toolbar label="Map Editor">
                    <div className="btn-group pull-right">
                        {this.state.edit && canEdit ?
                            <ewars.d.Button
                                icon="fa-save"
                                onClick={this._save}
                                label="Save"/>
                            : null}
                        <ewars.d.Button
                            icon="fa-times"
                            label="Close"
                            onClick={this._close}/>
                    </div>
                    {canEdit ?
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-trash"
                                onClick={this._delete}
                                label="Delete"/>
                            {!this.state.edit ?
                                <ewars.d.Button
                                    icon="fa-pencil"
                                    onClick={this._edit}
                                    label="Edit"/>
                                : null}
                        </div>
                        : null}
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true}>
                        <ewars.d.Layout style={{position: "relative"}}>
                            {this.state.dirty ?
                                <ewars.d.Row
                                    style={{
                                        maxHeight: 35,
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        width: "100%",
                                        zIndex: 999
                                    }}>
                                    <ewars.d.Cell>
                                        <ewars.d.Toolbar label="Settings Changed!">
                                            <div className="btn-group pull-right">
                                                <Button
                                                    label="Update Map"
                                                    icon="fa-sync"
                                                    onClick={this._update}/>
                                            </div>
                                        </ewars.d.Toolbar>
                                    </ewars.d.Cell>
                                </ewars.d.Row>
                                : null}
                            <ewars.d.Row>
                                <ewars.d.Cell>
                                    <div ref="map" style={{height: "100%", width: "100%"}}></div>
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Layout>
                    </ewars.d.Cell>
                    {this.state.edit ?
                        <ewars.d.Cell width="350px">
                            <ewars.d.Panel style={{padding: "8px"}}>
                                <ewars.d.Form
                                    definition={FORM}
                                    updateAction={this._onChange}
                                    readOnly={false}
                                    data={this.props.data}/>

                                <ewars.d.Toolbar label="Thresholds">
                                    <div className="btn-group">
                                        <ewars.d.Button
                                            onClick={this._addScale}
                                            icon="fa-plus"/>
                                    </div>

                                </ewars.d.Toolbar>
                                <div className="thresholds" style={{position: "relative"}}>
                                    <div className="block-tree" style={{height: 200, padding: 0, margin: 0}}>
                                        {this.props.data.definition.scales.map(function (item, index) {
                                            return (
                                                <div className="block" style={{padding: 0}}>
                                                    <div className="block-content" style={{padding: 0}}>
                                                        <div className="ide-row" style={{padding: 0}}>

                                                            <div className="ide-col border-right"
                                                                 style={{maxWidth: "33%", padding: 5}}>
                                                                <input type="text"
                                                                       value={item[0]}
                                                                       name={index + "_0"}
                                                                       onChange={this._thresholdChange}/>
                                                            </div>
                                                            <div className="ide-col border-right"
                                                                 style={{maxWidth: "33%", padding: 5}}>
                                                                <input type="text" value={item[1]}
                                                                       name={index + "_" + 1}
                                                                       onChange={this._thresholdChange}/>
                                                            </div>
                                                            <div className="ide-col border-right" style={{padding: 5}}>
                                                                <input type="color"
                                                                       name={index + "_" + 2}
                                                                       onChange={this._thresholdChange}
                                                                       value={item[2]}/>
                                                            </div>
                                                            <div className="ide-col" style={{maxWidth: 30, padding: 5}}
                                                                 onClick={() => {
                                                                     this.removeScale(index)
                                                                 }}>
                                                                <i className="fal fa-trash"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        }.bind(this))}
                                    </div>
                                </div>
                            </ewars.d.Panel>
                        </ewars.d.Cell>
                        : null}
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Editor;
