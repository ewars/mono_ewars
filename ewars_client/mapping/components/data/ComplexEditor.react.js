import ComplexManager from "../../../analsysi/components/editor/data/views/SliceComplexManager.react";

var ComplexEditor = React.createClass({
    getInitialState: function () {
        return {
            series: []
        }
    },

    render: function () {
        return <ComplexManager data={this.state}/>;
    }
});

export default ComplexEditor;
