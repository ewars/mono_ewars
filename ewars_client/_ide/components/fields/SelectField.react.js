export const SelectFieldOptions = {
    uuid: {
        type: "hidden"
    },
    name: {
        type: "text",
        label: {
            en: "Label"
        }
    },
    default: {
        type: "select",
        label: __("Default Value"),
    }
};

export default class SelectField extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="ide-input-select">

            </div>
        )
    }
}