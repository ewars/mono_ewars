/**
 * Settings Section UI Component
 */
export class SettingsSection extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-settings-section"></div>
        )
    }
}

export class SettingsSectionHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-settings-header">

            </div>
        )
    }
}

export default class SettingsPanel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-settings-panel"></div>
        )
    }
}