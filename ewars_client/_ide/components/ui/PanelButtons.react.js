import PanelButton from "./PanelButton.react";

export default class PanelButtons extends React.Component {
    constructor(props) {
        super(props)
    }

    _onClick(data){
        this.props.onChange(data);
    }

    render() {
        let tabs = _.map(this.props.tabs, function (data) {
            console.log(data);
            return <PanelButton
                onClick={this._onClick}
                id={data[0]}
                active={this.props.active}
                icon={data[2]}
                label={data[1]}/>
        }, this);

        return (
            <div className="ide-panel">
                <div className="ide-tabs">
                    {tabs}
                </div>
            </div>
        )
    }
}

module.exports = PanelButtons;