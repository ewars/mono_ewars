const data = {
    errors: {
        invalid_value: `Please provide a valid value for ${label}`,
        server_error: `A critical error has occurred, please contact a system administrator`
    },
    reporting: {
        invalid_date: `Please provide a valid date for ${label}`,
        duplicate_report: `A report already exists with this details.`
    },
    notifications: {
        saved: `Changes saved`,
        created: `${name} created`
    }
};