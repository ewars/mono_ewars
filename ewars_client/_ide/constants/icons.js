export default {
    DASHBOARD: "fa fa-tachometer",
    LOCATION: "fa fa-map-marker",
    DEFAULT: "fa fa-circle"
}