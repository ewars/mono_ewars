import ICONS from "./icons";

const USERS = {
    SUPER_ADMIN: {
        name: "Super Administrator"
    },
    INSTANCE_ADMIN: {
        name: "Instance Administrator"
    },
    GLOBAL_ADMIN: {
        name: "Global Administrator"
    },
    ACCOUNT_ADMIN: {
        name: "Account Administrator"
    },
    REGIONAL_ADMIN: {
        name: "Location Administrator"
    },
    USER: {
        name: "Reporting User"
    },
    LAB_ADMIN: {
        name: "Laboratory Administrator"
    },
    LAB_USER: {
        name: "Laboratory User"
    },
    ORG_ADMIN: {
        name: "Organization Administrator"
    }
};

export default USERS;