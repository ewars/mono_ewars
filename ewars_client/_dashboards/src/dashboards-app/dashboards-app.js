import {html, LitElement} from '@polymer/polymer/polymer-element.js';


/**
 * @customElement
 * @polymer
 */
class DashboardsApp extends LitElement {
    render() {
        return html`
          <style>
            :host {
              display: block;
              position: absolute;
              top: 0;
              left: 0;
              height: 100%;
              width: 100%;
            }

            .ide-layout {
                display: flex;
                height: 100%;
                width: 100%;
                background: #333;
            }

            .ide-tbar {
                display: block;
                border-bottom: 1px solid #000;
                height: 25px;
                max-height: 35px;
                flex: 1;
                background: rgba(255, 255, 255, 0.1);
                border-top: 1px solid rgba(255, 255, 255, 0.2);
            }
            </style>
          <div class="ide-layout">
            <div class="ide-tbar">

            </div>
            <row>
            </row>
          </div>
        `;
    }
}

window.customElements.define('dashboards-app', DashboardsApp);
