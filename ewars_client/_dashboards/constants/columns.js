var COLUMNS = [
    {
        name: "name",
        config: {
            label: "Name",
            type: "text"
        }
    },
    {
        name: "status",
        config: {
            label: "Status",
            type: "select",
            options: [
                ["ACTIVE", _l("ACTIVE")],
                ["INACTIVE", _l("INACTIVE")]
            ]
        }
    },
    {
        name: "description",
        config: {
            label: "Description"
        }
    },
    {
        name: "last_modified",
        config: {
            label: "Last Modified",
            type: "date"
        },
        fmt: ewars.DATE
    },
    {
        name: "created",
        config: {
            label: "Created Date",
            type: "date"
        },
        fmt: ewars.DATE
    },

    {
        name: "user.name",
        config: {
            type: "user",
            label: "Created By",
            filterKey: "created_by"
        }
    }
];

export default COLUMNS;