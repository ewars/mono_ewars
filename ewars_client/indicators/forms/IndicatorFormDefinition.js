export default {
    name: {
        order: 0,
        label: "Name",
        type: "language_string",
        required: true,
        help: "Enter a valid name for the account",
        placeholder: ""
    },
    group_id: {
        order: 1,
        type: "select",
        label: "Group",
        required: true,
        optionsSource: {
            resource: "IndicatorGroup",
            query: {
                $select: ["id", "name", "parent_id"]
            },
            valSource: "id",
            labelSource: "name",
            hierarchical: true,
            hierarchyProp: "parent_id"
        }
    },
    status: {
        order: 2,
        label: "Indicator Status",
        type: "select",
        options: [
            ["DRAFT", "Draft"],
            ["DISABLED", "Disabled"],
            ["ACTIVE", "Enabled"]
        ],
        required: true
    },
    itype: {
        order: 3,
        type: "select",
        label: {
            en: "Indicator Type"
        },
        help: {
            en: "The type of indicator, a compound indicator has no data of it's own and is defined as a complex map reduce function against multiple series across time, aggregate "
        },
        required: true,
        options: [
            ["DEFAULT", "Default"],
            ["AGGREGATE", "Aggregate"],
            ["COMPOUND", "Compound"]
        ]
    },
    definition: {
        type: "indicator_definition",
        label: {
            en: "Compound Definition"
        },
        help: {
            en: "Define your Aggregate or Compound indicator definition"
        },
        required: true,
        conditions: {
            application: "any",
            rules: [
                ["itype", "eq", "COMPOUND"],
                ['itype', 'eq', 'AGGREGATE']
            ]
        }
    },
    colour: {
        order: 4,
        type: "text",
        label: {
            en: "Indicator Colour"
        },
        help: {
            en: "Indicator colour specified here will be used throughout the application unless the colour is explicitly overridden by the user in the local context"
        }
    },
    ranked: {
        order: 5,
        label: {
            en: "Ranked"
        },
        type: "switch",
        help: {
            en: "If set to On, this indicator will be used in ranking algorithms for displaying items like top 5 diseases, etc..."
        }
    },
    description: {
        order: 9,
        label: "Description",
        type: "textarea",
        required: true,
        help: "Enter a description of this indicator",
        placeholder: "Describe the indicator",
        i18n: true
    }
};
