import {
    SelectField,
    DisplayField,
    SwitchField,
    IndicatorDefinition,
    NumericField as NumberField
} from "../../../../common/fields";

var fieldConfigs = {
    value_type: {
        options: [
            ["NUMERIC", "Numeric"],
            ["JSON", "JSON"],
            ["TEXT", "Text"]
        ]
    },
    itype: {
        options: [
            ["DEFAULT", "Default"],
            ["STATIC", "Static"]
        ]
    }
};

var DefinitionSettings = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onUpdate: function (prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {

        let typeLabel = "Default";
        if (this.props.data.itype == "STATIC") typeLabel = "Static Value";


        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="ide-settings-content">
                    <div className="ide-settings-header">
                        <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                        <div className="ide-settings-title">Definition</div>
                    </div>
                    <div className="ide-settings-intro">
                        <p>Settings which dictate how this indicator operates within the overall EWARS system</p>
                    </div>
                    <div className="ide-settings-form">

                        <div className="ide-section-basic">
                            <div className="header">General Settings</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Indicator Type*</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="itype"
                                        value={this.props.data.itype}
                                        onUpdate={this._onUpdate}
                                        config={fieldConfigs.itype}/>
                                </div>
                            </div>


                        </div>

                        {this.props.data.itype == "STATIC" ?
                            <div className="ide-section-basic">
                                <div className="header">{typeLabel || ""}</div>

                                <div style={{paddingLeft: "16px", paddingRight: "16px", marginTop: "5px", marginBottom: "5px"}}>
                                    <NumberField
                                        value={this.props.data.definition ? (this.props.data.definition.value || 0) : ""}
                                        name="value"
                                        onUpdate={this._onUpdate}/>
                                </div>

                            </div>
                            : null}
                    </div>
                </div>
            </div>
        )
    }
});

export default DefinitionSettings;
