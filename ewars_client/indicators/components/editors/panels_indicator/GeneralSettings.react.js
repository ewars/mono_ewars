import GroupSelector from "../../facets/GroupSelector";

import {
    TextField,
    LanguageStringField as I18NString,
    TextAreaField as TextArea,
    SelectField,
    DisplayField
} from "../../../../common/fields";

import Inbound from "../../facets/Inbound.react";

var fieldConfigs = {
    status: {
        options: [
            ["ACTIVE", "Active"],
            ["INACTIVE", "Inactive"]
        ]
    },
    group_id: {
        optionsSource: {
            resource: "indicator_group",
            query: {},
            valSource: "id",
            labelSource: "name",
            select: ["id", "name", "parent_id"],
            additional: [
                ["null", "No Parent"]
            ],
            hierarchical: true,
            hierarchyProp: "parent_id"
        }
    },
    account_id: {
        optionsSource: {
            resource: "account",
            query: {
                status: {eq: "ACTIVE"}
            },
            valSource: "id",
            labelSource: "name"
        }
    },
    value_type: {
        options: [
            ["NUMERIC", "Numeric"],
            ["TEXT", "Text"]
        ]
    }
};

var GeneralSettings = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onUpdate: function (prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="ide-settings-content">
                    <div className="ide-settings-header">
                        <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                        <div className="ide-settings-title">General Settings</div>
                    </div>
                    <div className="ide-settings-intro">
                        <p>Basic information about the indicator</p>
                    </div>
                    <div className="ide-settings-form">

                        <div className="ide-section-basic">
                            <div className="header">Description</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Name*</div>
                                <div className="ide-setting-control">
                                    <I18NString
                                        name="name"
                                        value={this.props.data.name}
                                        onUpdate={this._onUpdate}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">ICODE</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="icode"
                                        value={this.props.data.icode}
                                        onUpdate={this._onUpdate}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Status*</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="status"
                                        value={this.props.data.status}
                                        onUpdate={this._onUpdate}
                                        config={fieldConfigs.status}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Value Type *</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="value_type"
                                        value={this.props.data.value_type}
                                        onUpdate={this._onUpdate}
                                        config={fieldConfigs.value_type}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Group/Folder*</div>
                                <div className="ide-setting-control">
                                    <GroupSelector
                                        name="group_id"
                                        value={this.props.data.group_id}
                                        onUpdate={this._onUpdate}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Description</div>
                                <div className="ide-setting-control">
                                <TextArea
                                    name="description"
                                    value={this.props.data.description}
                                    onUpdate={this._onUpdate}
                                    config={{i18n: true}}/>
                                </div>
                            </div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Style</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Colour</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="colour"
                                        value={this.props.data.colour}
                                        onUpdate={this._onUpdate}/>
                                </div>
                            </div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Derivation</div>

                            <div className="description">The following are ACTIVE forms which are configured to feed
                                data into this indicator.
                            </div>

                            <Inbound id={this.props.data.uuid}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default GeneralSettings;
