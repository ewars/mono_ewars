class Inbound extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inbound: []
        }
    }

    componentWillMount() {
        this._load(this.props.id);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.id != nextProps.id) {
            this._load(nextProps.id);
        }
    }

    _load = (id) => {
        if (!id) return;
        ewars.tx("com.ewars.indicator.inbound", [id])
            .then((resp) => {
                this.setState({
                    inbound: resp
                })
            })
    };

    render() {
        return (
            <div className="inline-list">
                {this.state.inbound.map((item) => {
                    return <div className="list-item">
                        <div className="ide-row">
                            <div className="ide-col">{ewars.I18N(item.name)}</div>
                        </div>
                    </div>
                })}
            </div>
        )
    }
}

export default Inbound;