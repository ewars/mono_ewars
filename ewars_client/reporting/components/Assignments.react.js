import {
    Spinner
} from "../../common";
import CONSTANTS from "../../common/constants";

import Actions from "../utils/Actions";

const STYLES = {
    ASSIGN: {
        margin: "8px",
        border: "1px solid rgba(0,0,0,0.1)",
        padding: "8px",
        cursor: "pointer"
    }
};


class FormList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            assignments: []
        }
    }

    componentWillMount() {
        ewars.tx("com.ewars.user.assignments")
            .then(function (resp) {
                ewars.g.assignments = resp;
                this.setState({
                    assignments: resp
                });
                ewars.emit("ASSIGNS_LOADED");
            }.bind(this))
    }

    render() {
        let items = <Spinner/>;

        if (this.state.assignments) {
            if (this.props.formGroups) {
                let form_lookup = {};
                this.state.assignments.forEach(item => {
                    form_lookup[item.form_uuid] = item;
                });

                let groups = this.props.formGroups.sort((a, b) => {
                    if (a.order > b.order) return 1;
                    if (a.order < b.order) return -1;
                    return 0;
                });

                items = [];
                let used = [];

                groups.forEach(item => {
                    if (!item.pinned && item.forms.length > 0) {
                        items.push(
                            <div className="form-group-header">{__(item.name)}</div>
                        );
                    }

                    let forms = item.forms.sort((a, b) => {
                        if (a.order > b.order) return 1;
                        if (a.order < b.order) return -1;
                        return 0;
                    })
                    forms.forEach(item => {
                        let id = `com.ewars.browser.${item.uuid}`;
                        let form_def = form_lookup[item.uuid];
                        if (form_def) {
                            used.push(item.uuid);
                            items.push(
                                <FormListItem
                                    key={id}
                                    onClick={this.props.onSelect}
                                    id={id}
                                    data={form_def}/>
                            );
                        }
                    })
                });

                let unused = this.state.assignments.filter(item => {
                    return used.indexOf(item.form_uuid) < 0;
                });

                if (unused.length > 0) {
                    items.push(<div className="form-group-header">Uncategorized</div>);

                    unused.forEach(item => {
                        let id = `com.ewars.browser.${item.form_uuid}`;
                        items.push(
                            <FormListItem
                                key={id}
                                onClick={this.props.onSelect}
                                id={id}
                                data={item}/>
                        );
                    });
                }
            } else {
                let m_items = this.state.assignments.sort((a, b) => {
                    return getName(a).localeCompare(getName(b));
                });
                items = m_items.map(function (item) {
                    let id = "com.ewars.browser." + item.form_id;
                    return <FormListItem
                        onClick={this.props.onSelect}
                        id={id}
                        data={item}/>
                }.bind(this))
            }
        }

        return (
            <div className="assign-list">
                {items}
            </div>
        )
    }
}

const getName = (d) => {
    return d.form_name.en || d.form_name;
};

class AdminFormList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            assignments: null
        }
    }

    componentWillMount() {
        let query = {
            status: {eq: "ACTIVE"}
        };
        ewars.tx("com.ewars.query", ["form", ["uuid", "id", "name", "version_id"], query, null, null, null, null])
            .then((resp) => {
                let assigns = resp.map((item) => {
                    return {
                        form_name: item.name,
                        form_id: item.id,
                        uuid: item.uuid
                    }
                });
                ewars.g.assignments = assigns;
                this.setState({
                    assignments: assigns
                })
            }).catch(err => {
                console.log(err);
            })
    }

    render() {
        let items = <Spinner/>;

        if (this.state.assignments) {
            if (this.props.formGroups) {
                let form_lookup = {};
                this.state.assignments.forEach(item => {
                    form_lookup[item.uuid] = item;
                });
                console.log(form_lookup);

                let groups = this.props.formGroups.sort((a, b) => {
                    if (a.order > b.order) return 1;
                    if (a.order < b.order) return -1;
                    return 0;
                });

                items = [];
                let used = [];

                groups.forEach(item => {
                    if (!item.pinned && item.forms.length > 0) {
                        items.push(
                            <div className="form-group-header">{__(item.name)}</div>
                        );
                    }

                    let forms = item.forms.sort((a, b) => {
                        if (a.order > b.order) return 1;
                        if (a.order < b.order) return -1;
                        return 0;
                    })
                    forms.forEach(item => {
                        let id = `com.ewars.browser.${item.uuid}`;
                        let form_def = form_lookup[item.uuid];
                        used.push(item.uuid);
                        if (form_def) {
                            items.push(
                                <FormListItem
                                    key={id}
                                    onClick={this.props.onSelect}
                                    id={id}
                                    data={form_def}/>
                            );
                        }
                    })
                });

                let unused = this.state.assignments.filter(item => {
                    return used.indexOf(item.uuid) < 0;
                });

                if (unused.length > 0) {
                    items.push(<div className="form-group-header">Uncategorized</div>);

                    unused.forEach(item => {
                        let id = `com.ewars.browser.${item.uuid}`;
                        items.push(
                            <FormListItem
                                key={id}
                                onClick={this.props.onSelect}
                                id={id}
                                data={item}/>
                        );
                    });
                }

            } else {
                let m_items = this.state.assignments.sort((a, b) => {
                    return getName(a).localeCompare(getName(b));
                });

                items = m_items.map((item) => {
                    let id = `com.ewars.browser.${item.form_id})`;
                    return <FormListItem
                        key={id}
                        onClick={this.props.onSelect}
                        id={id}
                        data={item}/>;
                });
            }
        }

        return (
            <div className="assign-list">
                {items}
            </div>
        )
    }
}

const ASSIGN_ACTIONS = [
    ['fa-plus', 'CREATE', "Create new record"],
    ['fa-folder', 'BROWSE', "Browse records"]
]

class FormListItem extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick = () => {
        ewars.z.dispatch("REPORTING", "ADD_TAB", {
            type: "BROWSE",
            title: ewars.I18N(this.props.data.form_name),
            icon: "fa-database",
            uuid: ewars.utils.uuid(),
            grid: null,
            definition: null,
            form_id: this.props.data.form_id
        })
    };

    onCreateClick = (e) => {
        ewars.z.dispatch("REPORTING", "ADD_TAB", {
            title: `Draft - ${ewars.I18N(this.props.data.form_name)}`,
            icon: "fa-clipboard",
            form: {
                id: this.props.data.form_id,
                name: this.props.data.form_name
            },
            collection: {
                form_id: this.props.data.form_id,
                created_by: window.user.id,
                data: {},
                location_id: null,
                data_date: null
            },
            uuid: ewars.utils.uuid(),
            type: "DRAFT"
        })
    };

    render() {
        let name = ewars.I18N(this.props.data.form_name);

        let canCreate = true;

        if (this.props.data.locations) {
            let hasser = [];
            this.props.data.locations.forEach((item) => {
                hasser.push(item.status);
            });

            if (hasser.indexOf("ACTIVE") < 0) canCreate = false;
        }

        return (
            <div className="assign-list-item" style={{display: "flex", flex: 1, flexDirection: "row"}}>
                    <ewars.d.Cell style={{padding: "5px 8px 8px 3px"}} onClick={this.onClick}>
                        {name}
                    </ewars.d.Cell>
                    <ewars.d.Cell width="80px" style={{display: "block"}}>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-plus"
                                onClick={this.onCreateClick}/>
                            <ewars.d.Button
                                icon="fa-folder-open"
                                onClick={this.onClick}/>
                        </div>
                    </ewars.d.Cell>
            </div>
        )
    }
}


class Assignments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            collapsed: false
        }
    }

    _toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    };

    render() {
        let view;
        if ([CONSTANTS.ACCOUNT_ADMIN, CONSTANTS.REGIONAL_ADMIN].includes(window.user.role)) {
            view = <AdminFormList
                formGroups={this.props.formGroups}
                onSelect={this.onSelect}/>;
        }

        if (window.user.role == CONSTANTS.USER) {
            view = <FormList
                formGroups={this.props.formGroups}
                onSelect={this.onSelect}/>;
        }


        if (this.state.collapsed) {
            return (
                <ewars.d.Cell width="44px" borderRight={true}>
                    <ewars.d.Layout>
                        <ewars.d.Toolbar>
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    icon="fa-angle-double-right"
                                    onClick={this._toggle}/>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row>
                            <ewars.d.Cell></ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>

                </ewars.d.Cell>
            )
        }

        return (
            <ewars.d.Cell borderRight={true} width="350px">
                <ewars.d.Layout>
                    <ewars.d.Toolbar>
                        <div className="btn-group">
                            <ewars.d.Button
                                icon="fa-clipboard"
                                onClick={this.props.onDrafts}
                                label="Drafts"/>
                        </div>

                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                onClick={this._toggle}
                                icon="fa-angle-double-left"/>
                        </div>
                    </ewars.d.Toolbar>
                    {this.props.canRequestAssignment ?
                        <ewars.d.Row height="35px">
                            <ewars.d.Cell onClick={this.props.onRequestAssign}>
                                <div className="req-assign-hover">
                                    <i className="fal fa-plus"></i>&nbsp;Request Assignment
                                </div>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                        : null}
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <ewars.d.Panel>
                                {view}
                            </ewars.d.Panel>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </ewars.d.Cell>
        )

    }
}

export default Assignments;
