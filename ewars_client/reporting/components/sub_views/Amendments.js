class AmendView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            html: null
        };

        ewars.tx("com.ewars.collection.differential", [this.props.data.id])
            .then((resp) => {
                this.setState({
                    html: resp
                })
            })
    }

    render() {
        if (!this.state.html) {
            return (
                <div className="placeholder">Loading differential...</div>
            )
        }

        return (
            <div className="differential" dangerouslySetInnerHTML={{__html: this.state.html}}>

            </div>
        )
    }
}

class Amendment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            diff: false
        }
    }

    _toggleDiff = () => {
        this.setState({
            diff: !this.state.diff
        })
    };

    render() {
        let iconClass = "fal fa-caret-right";
        if (this.state.diff) iconClass = "fal fa-caret-down";

        return (
            <div className="block">
                <div className="block-content" style={{padding: 0}}>
                    <ewars.d.Row>
                        <ewars.d.Cell style={{padding: 8}}>
                            {this.props.data.user_name} on {this.props.data.amended_date}

                        </ewars.d.Cell>
                        <ewars.d.Cell onClick={this._toggleDiff} borderLeft={true} width="30px"
                                      style={{textAlign: "center", padding: 8}}>
                            <i className={iconClass}></i>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.diff ?
                    <div className="block-children">
                        <AmendView data={this.props.data}/>
                        <div style={{paddingTop: 8}}>
                            <p><strong>Reason:</strong> {this.props.data.amendment_reason}</p>
                        </div>
                    </div>
                    : null}
            </div>

        )
    }
}

export default class Amendments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        };

        ewars.tx('com.ewars.collection.amendments', [this.props.data.collection.uuid])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Amendments">

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="block-tree" style={{position: "relative"}}>
                                {this.state.data.map((item) => {
                                    return (
                                        <Amendment data={item}/>
                                    )
                                })}
                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}