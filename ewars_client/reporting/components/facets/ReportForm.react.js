import {
    DateField,
    NumericField as NumberField,
    SelectField,
    TextField,
    DisplayField,
    HeaderField
} from "../../../common/fields";

const TYPES = {
    "date": DateField,
    "number": NumberField,
    "select": SelectField,
    "text": TextField,
    "display": DisplayField,
    "header": HeaderField
};

function comparator(a, b) {
    if (a.order < b.order) return -1;
    if (a.order > b.order) return 1;
    return 0;
}

function _getFieldsAsArray(sourceFields) {
    let fields = [];

    for (var token in sourceFields) {
        let field = ewars.copy(sourceFields[token]);
        field.name = token;

        if (field.fields) {
            let children = _getFieldsAsArray(field.fields);
            field.fields = children;
        }

        fields.push(field);
    }

    fields.sort(comparator);

    return fields;
}

/**
 * Determine whether a field should be visible
 * @param data
 * @param field
 * @returns {boolean}
 * @private
 */
function _isShown(data, field) {
    var result = false;

    if (!field.conditions) return true;
    if (!field.conditions.rules) return true;

    if (field.conditions.application == "any") {
        // Only one of the rules has to pass

        for (var conIdx in field.conditions.rules) {
            var rule = field.conditions.rules[conIdx];

            var tmpResult = _getRuleResult(rule, definition);
            if (result != true && tmpResult == true) result = true;
        }

    } else {
        var ruleCount = field.conditions.rules.length;
        var rulePassCount = 0;

        for (var ruleIdx in field.conditions.rules) {
            var rule = field.conditions.rules[ruleIdx];
            var ruleResult = _getRuleResult(rule, definition);
            if (ruleResult) rulePassCount++;
        }

        if (ruleCount == rulePassCount) result = true;

    }
    return result;


}

/**
 * Evaluate a rule for a given fields conditions
 * @param rule
 * @param definition
 * @returns {*}
 * @private
 */
function _getRuleSet(rule, definition) {
    var result;

    if (!ewars.getKeyPath(rule[0], definition)) return false;

    var sourceValue = ewars.getKeyPath(rule[0], definition);
    sourceValue = sourceValue.value;
    if (sourceValue == null || sourceValue == undefined) return false;

    switch (rule[1]) {
        case "eq":
            result = (sourceValue == rule[2] ? true : false);
            break;
        case "ne":
            result = (sourceValue != rule[2] ? true : false);
            break;
        case "gt":
            result = (sourceValue > rule[2] ? true : false);
            break;
        case "lt":
            result = (sourceValue < rule[2] ? true : false);
            break;
        default:
            result = false;
            break
    }

    return result;
}


class Field extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-field">
                <label className="label">{ewars.I18N(this.props.data.label)}</label>
                <div className="field-wrapper">
                    <div className="ide-row">
                        <div className="ide-col">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Matrix extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="matrix">
                <table className="matrix-table">
                    <tbody>
                    {this.props.children}
                    </tbody>
                </table>
            </div>
        )
    }
}

class MatrixRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dummy">
                {this.props.children}
            </div>
        )
    }
}


class ReportForm extends React.Component {
    static defaultProps = {
        readOnly: false,
        data: {},
        definition: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: null
        }
    }

    componentWillMount() {
        if (this.props.data) this.state.data = ewars.copy(this.props.data);
    }

    componentWillReceiveProps(nextProps) {
        this.state.data = ewars.copy(nextProps.data);
    }

    getData = () => {
        return this.state.data;
    };

    _onFieldChange = (prop, value, path, additionalData) => {
        this.props.onUpdate(null, prop, value, path, additionalData);
    };

    render() {
        let fields = _getFieldsAsArray(this.props.definition);

        let nodes = fields.map(function (field) {
            if (field.type == "matrix") {
                return (
                    <Matrix>
                        {field.fields.map(function (row) {
                            return (<tr>
                                    <th>{ewars.I18N(row.label)}</th>
                                    {row.fields.map(function (cell) {
                                        let Control = TYPES[cell.type];
                                        let path = `${field.name}.${row.name}.${cell.name}`;
                                        let value = this.state.data[path];
                                        return (
                                            <td className="cell">
                                                <div className="cell-label">{ewars.I18N(cell.label)}</div>
                                                <div className="cell-input">
                                                    <Control
                                                        readOnly={this.props.readOnly}
                                                        config={cell}
                                                        name={cell.name}
                                                        value={value}
                                                        path={path}
                                                        onUpdate={this._onFieldChange}/>
                                                </div>
                                            </td>
                                        )
                                    }.bind(this))}
                                </tr>
                            )
                        }.bind(this))}
                    </Matrix>
                )

            } else if (field.type == "header") {
                return (
                    <HeaderField config={field}/>
                )
            } else {
                let Control = TYPES[field.type];
                let value = this.state.data[field.name];
                let shown = _isShown(this.state.data, field);
                return (
                    <Field
                        data={field}>
                        <Control
                            config={field}
                            readOnly={this.props.readOnly}
                            name={field.name}
                            path={field.name}
                            value={value}
                            formData={this.state.data}
                            onUpdate={this._onFieldChange}/>
                    </Field>
                )
            }
        }.bind(this));


        let formClass = "form";
        if (this.props.readOnly) formClass += " read-only";

        return (
            <div className={formClass}>
                {nodes}
            </div>
        )
    }
}

export default ReportForm;
