import {
    Button,
    Modal
} from "../../common";
import CONSTANTS from "../../common/constants";

import Assignments from "./Assignments.react";

import Dashboard from "./tab_types/Dashboard.react";
import ReportBrowser from "./tab_types/ReportBrowser.react";
import Report from "./tab_types/Report.react";
import Drafts from "./tab_types/Drafts";
import AssignRequest from "../../common/c.assignment_requestor";

import Reducer from "../reducers/tabs";

const CMPS = {
    "DASHBOARD": Dashboard,
    "REPORT": Report,
    "DRAFT": Report,
    "BROWSE": ReportBrowser,
    DRAFTS: Drafts
};

const ASSIGN_ACTIONS = [
    {label: "Close", icon: "fa-times", action: "CANCEL"}
];

const styles = {
    tabCloser: {
        maxWidth: 20,
        flexBasis: 0,
        flexGrow: 1,
        paddingLeft: 5,
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 5,
        textAlign: 'center',
        borderTopRightRadius: 3
    },
    tabIcon: {
        maxWidth: 20,
        flexBasis: 0,
        flexGrow: 1,
        paddingTop: 8,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 8,
        textAlign: 'center',
        borderTopLeftRadius: 3
    },
    tab: {
        padding: 8,
        textAlign: "left"
    },
    label: {
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        overflowX: "hidden"
    },
    tabOverflow: {
        cursor: "pointer"
    }
};

class Tab extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    onClick = (e) => {
        ewars.z.dispatch("REPORTING", "SET_ACTIVE_TAB", {index: this.props.data.order});
    };

    close = () => {
        ewars.z.dispatch("REPORTING", "REMOVE_TAB", {index: this.props.data.order});
    };

    render() {
        let className = "panelsbar-btn";
        if (this.props.data.order == this.props.activeTab) className += " panelsbar-btn-active";

        let data,
            label,
            icon,
            closeable = true;

        if (this.props.data.type == "DASHBOARD") closeable = false;
        label = this.props.data.title;
        icon = "fal " + this.props.data.icon;

        let width;
        if (this.props.total > 1) {
            width = (100 / this.props.total ) + "%";
        }

        return (
            <div xmlns="http://www.w3.org/1999/xhtml"
                 style={{padding: 0, width: width}}
                 className={className}>
                <ewars.d.Row>
                    <ewars.d.Cell style={styles.tab} onClick={this.onClick} width={25}>
                        <i className={icon}></i>
                    </ewars.d.Cell>
                    <ewars.d.Cell style={{...styles.tab, ...styles.label}} onClick={this.onClick}>
                        {label}
                    </ewars.d.Cell>
                    <ewars.d.Cell onClick={this.close} style={{...styles.tab, ...styles.tabOverflow}} width={25}>
                        <i className="fal fa-times"></i>
                    </ewars.d.Cell>
                </ewars.d.Row>

            </div>
        )
    }
}

class ReportingComponent extends React.Component {
    constructor(props) {
        super(props);

        let def = {
            activeTab: 0,
            tabs: [{
                order: 0,
                title: "Dashboard",
                icon: "fa-globe",
                type: "DASHBOARD"
            }]
        };

        if (this.props.defaults) def = this.props.defaults;

        this.state = {
            show: false,
            ...ewars.z.register("REPORTING", Reducer, def)
        };
    }

    static defaultProps = {
        initialTabs: null,
        initialIndex: 0
    };

    componentDidMount() {
        ewars.z.subscribe("REPORTING", this._onStoreChange);
    }

    _onStoreChange = () => {
        this.setState(ewars.z.getState("REPORTING"));
    };

    componentDidUpdate() {
    }

    _showDrafts = () => {
        ewars.z.dispatch("REPORTING", "ADD_TAB", {
            title: "Drafts",
            icon: "fa-clipboard",
            type: "DRAFTS",
            order: null
        })
    };

    _requestAssignment = () => {
        this.setState({show: true})
    };

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({show: false})
    };

    render() {
        let canRequestAssign = false;
        if (window.user.role == CONSTANTS.USER) canRequestAssign = true;

        let tabs = this.state.tabs.map(function (tab, index) {
            return <Tab
                total={this.state.tabs.length}
                activeTab={this.state.activeTab}
                data={tab}/>
        }.bind(this));

        let activeTab = this.state.tabs[this.state.activeTab];

        let view;

        if (activeTab) {
            let Cmp = CMPS[activeTab.type];
            view = <Cmp
                data={activeTab}/>
        }

        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row">
                        <Assignments
                            canRequestAssignment={canRequestAssign}
                            onRequestAssign={this._requestAssignment}
                            formGroups={this.props.form_groups}
                            onDrafts={this._showDrafts}
                            onSelect={this.onSelect}/>
                        <div className="ide-col">
                            <div className="ide-layout">
                                <div className="ide-row" style={{maxHeight: 35}}>
                                    <div className="ide-col">
                                        <div className="report-tabs-wrapper"
                                             style={{display: "block", maxWidth: "100%"}}>
                                            <div className="report-tabs-scroll"
                                                 style={{position: "absolute", top: 0, left: 0, width: "100%"}}>
                                                <div xmlns="http://www.w3.org/1999/xhtml" className="iw-tabs"
                                                     ref="tabs">
                                                    {tabs}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="ide-row">
                                    <div className="ide-col">
                                        {view}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ewars.d.Shade
                    title="Assignment Request"
                    icon="fa-clipboard"
                    onAction={this._onModalAction}
                    shown={this.state.show}>
                    <AssignRequest/>
                </ewars.d.Shade>
            </div>
        )
    }
}

export default ReportingComponent;
