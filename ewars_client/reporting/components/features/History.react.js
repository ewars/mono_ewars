class History extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            history: this.props.data.history || []
        }
    }

    componentWillMount() {
        if (this.state.history.length <= 0 && this.props.data.uuid) {
            ewars.tx("com.ewars.resource", ["collection", this.props.data.uuid, ["history"], null])
                .then(function (resp) {
                    this.setState({
                        history: resp.history
                    })
                }.bind(this))
        }

        ewars.subscribe("RELOAD_HIST", this._reload)
    }

    _reload = () => {
        ewars.tx("com.ewars.resource", ["collection", this.props.data.uuid, ["history"], null])
            .then(function (resp) {
                this.setState({
                    history: resp.history
                })
            }.bind(this))
    };

    render() {

        let items = this.state.history.map(item => {
            switch (item.event_type) {
                case "SUBMITTED":
                    return `Submitted on ${ewars.DATE(item.created)}`;
                case "AMENDED":
                    return `Amended on ${ewars.DATE(item.created)} by ${item.name}`;
                default:
                    return ""
            }
        });

        let list = items.map(row => {
            return (
                <div className="block">
                    <div className="block-content" style={{cursor: "inherit"}}>{row}</div>
                </div>
            )
        });

        if (list.length <= 0) list = <div className="placeholder">This submission currently has no history.</div>;

        return (
            <div className="widget">
                <div className="widget-header"><span>History</span></div>
                <div className="body no-pad">
                    <div className="block-tree" style={{position: "relative", overflowY: "auto"}}>
                        {list}
                    </div>
                </div>
            </div>
        )
    }
}

export default History;
