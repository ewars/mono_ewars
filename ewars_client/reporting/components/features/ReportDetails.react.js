import CONSTANTS from "../../../common/constants";

import { Form } from "../../../common";
import Actions from "../../utils/Actions";

import Store from "../../utils/Store";

const FEATURES = [
    "LOCATION_REPORTING",
    "INTERVAL_REPORTING"
];

let REPORT_DATE_FIELD = {
    type: "date",
    label: _l("REPORT_DATE"),
    required: true,
    allow_future: false,
    block_future: true,
    date_type: CONSTANTS.DAY,
    help: {
        en: "Select the date that this report is for."
    }
};

let LOCATION_FIELD = {
    type: "location",
    label: _l("LOCATION"),
    required: true,
    parent_id: null,
    help: {en: "Select the location this report belongs to."},
    form_id: null,
    selectionTypeId: null,
    hideInactive: true
};

let ASSIGNMENT_FIELD = {
    type: "select",
    label: _l("ASSIGNMENT"),
    required: true,
    help: {en: "Select the assignment you are submitting this report for"},
    options: [],
    template: "assignment"
};


class ReportDetails extends React.Component {
    constructor(props) {
        super(props);

    }

    onChange = (data, prop, value, path) => {
        ewars.z.dispatch("REPORTING", "SET_COLLECTION_PROP", {
            index: this.props.index,
            prop: prop,
            value: value
        })
    };

    render() {
        let details = {};

        FEATURES.forEach((feature) => {
            if (this.props.form.features[feature]) {
                let featDef = this.props.form.features[feature];
                if (feature == "LOCATION_REPORTING") {
                    if (window.user.role == "USER") {
                        details.location_id = ASSIGNMENT_FIELD;
                        let assign;
                        ewars.g.assignments.forEach(assignment => {
                            if (assignment.form_id == this.props.form.id) assign = assignment;
                        });
                        details.location_id.options = [];
                        assign.locations.forEach((loc) => {
                            if (loc.status == 'ACTIVE') details.location_id.options.push([loc.location_id, ewars.I18N(loc.name[0]), loc.name]);
                        });
                        details.location_id.form_id = this.props.form.id;
                    } else {
                        details.location_id = LOCATION_FIELD;
                        details.location_id.selectionTypeId = featDef.site_type_id;
                        details.location_id.lineageRoot = window.user.location_id || window.user.clid || null;
                    }
                }

                if (feature == "INTERVAL_REPORTING") {
                    details.data_date = REPORT_DATE_FIELD;
                    details.data_date.date_type = featDef.interval;
                }

            }
        });


        if (this.props.form.id == 4 && window.user.tki == '_21bce401c88d') {
            details.data_date.offsetAvailability = true;
        }

        if (!details.data_date) {
            let label = 'Report date';
            if (this.props.form.id == 11 && window.user.tki == '_21bce401c88d') {
                label = 'Date of admission';
            }
            details.data_date = {
                label: label,
                type: "date",
                date_type: "DAY",
                block_future: true
            }
        }

        return (
            <Form
                data={this.props.data}
                definition={details}
                errors={this.props.errors}
                updateAction={this.onChange}
                readOnly={this.props.readOnly}/>
        )
    }
}

export default ReportDetails;
