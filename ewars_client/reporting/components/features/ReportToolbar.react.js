import { Button } from "../../../common";

class ReportToolbar extends React.Component {
    constructor(props) {
        super(props);
    }

    onAction = (data) => {
        this.props.onAction(data.action);

    };

    render() {
        let canSaveDraft = true,
            canSubmit = true,
            canClaim = true,
            canAmend = true,
            hasGuidance = true,
            canDelete = true,
            canPrint = false;

        if (this.props.data.status == "SUBMITTED") {
            canSaveDraft = false;
            canSubmit = false;
            canAmend = true;
            canDelete = true;
            canPrint = true;

            if (window.user.id == this.props.data.created_by) {
                canClaim = false;
            }

            if (["SUPER_ADMIN", "ACCOUNT_ADMIN", "REGIONAL_ADMIN"].indexOf(window.user.role) >= 0) {
                canClaim = true;
            }
        } else {
            canSaveDraft = true;
            canSubmit = true;
            canClaim = false;
            canAmend = false;
            canDelete = false;
        }

        if (this.props.amend) {
            canClaim = false;
            canAmend = false;
            canDelete = false;
        }

        return (
            <div className="ide-tbar">
                {this.props.amend ?
                    <div className="ide-tbar-text">Amendment Mode</div>
                    : null}

                <div className="btn-group">
                    {canSaveDraft ?
                        <ewars.d.Button
                            label="Save Draft"
                            colour="green"
                            onClick={this.onAction}
                            data={{action: "SAVE"}}
                            icon="fa-save"/>
                        : null}
                    {canSubmit ?
                        <ewars.d.Button
                            label="Submit"
                            colour="green"
                            onClick={this.onAction}
                            data={{action: "SUBMIT"}}
                            icon="fa-paper-plane"/>
                        : null}
                    {canPrint ?
                        <ewars.d.Button
                            label="Print"
                            icon="fa-print"
                            onClick={this.onAction}
                            data={{action: 'PRINT'}}/>
                        : null}
                </div>

                <div className="btn-group">
                    {canAmend ?
                        <ewars.d.Button
                            label="Amend Report"
                            onClick={this.onAction}
                            data={{action: "AMEND"}}
                            icon="fa-pencil"/>
                        : null}
                    {canDelete ?
                        <ewars.d.Button
                            label="Delete Report"
                            onClick={this.onAction}
                            data={{action: "RETRACT"}}
                            icon="fa-trash"/>
                        : null}
                </div>

                {this.props.amend ?
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-paper-plane"
                            label="Submit Amendment"
                            onClick={this.onAction}
                            data={{action: "SUBMIT_AMEND"}}/>
                        <ewars.d.Button
                            icon="fa-times"
                            label="Cancel"
                            onClick={this.onAction}
                            data={{action: "CANCEL_AMEND"}}/>
                    </div>
                    : null}


            </div>
        )
    }
}

export default ReportToolbar;
