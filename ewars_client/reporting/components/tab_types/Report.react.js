import Moment from "moment";

import CONSTANTS from "../../../common/constants";
import {
    Form,
    Modal,
    Spinner
} from "../../../common";

import FormUtils from "../../../common/utils/FormUtils";
import Validator from "../../../common/utils/Validator";
import { TextAreaField } from "../../../common/fields";

import Actions from "../../utils/actions";


// Editing Modes
import ReportToolbar from "../features/ReportToolbar.react";
import ReportDetails from "../features/ReportDetails.react";

import Attachments from "../features/Attachments.react";
import History from "../features/History.react";
import sub_views from "../sub_views";



const FEATURES = [
    "ATTACHMENTS"
];

const FEATURE_MAP = {
    ATTACHMENTS: Attachments
};

const ROOT_PROPS = ["data_date", "location_id", "created_by"];

function isEmpty(obj) {
    if (obj == null) return true;
    if (obj == "") return true;
    return false;
}


const RETRACT_BUTTONS = [
    {label: "Submit", icon: "fa-send", action: "SUBMIT_RETRACT"},
    {label: "Cancel", icon: "fa-times", action: "CANCEL_RETRACT"}
];

class Report extends React.Component {
    _bl = null;

    constructor(props) {
        super(props);

        this.state = {
            amend: false,
            retract: false,
            amendReason: "",
            retractReason: "",
            claim: false,
            subView: null
        }
    }

    componentWillMount() {
        if (!this.props.data.form.version) {
            this.query(this.props)
        }

        ewars.subscribe("ASSIGNS_LOADED", this._assignLoaded);
    }

    componentWillUnmount() {
        ewars.unsubscribe("ASSIGNS_LOADED", this._assignLoaded);
    }

    _assignLoaded = () => {
        this.forceUpdate();
    };

    query = (props) => {
        if (!props.data.form.version) {
            this._bl = new ewars.Blocker(null, "Loading form...");
            ewars.tx("com.ewars.resource", [
                "form",
                props.data.form.id,
                ["id", "name", "features", "version_id"],
                ['version:version_id:uuid:uuid,definition']
            ])
                .then(function (resp) {
                    this._bl.destroy();
                    ewars.z.dispatch("REPORTING", "SET_FORM", {
                        index: props.data.order,
                        form: resp
                    })

                }.bind(this))
        }

    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.order != this.props.data.order) {
            this.query(nextProps)
        }
    }

    _onChange = () => {
        this.setState({
            ...this.state,
            ...Store.getReportTabState(this.props.data.id)
        })
    };

    onReportAction = (action) => {
        switch (action) {
            case "CANCEL_AMEND":
                ewars.z.dispatch("REPORTING", "RESTORE_BACKUP", {
                    index: this.props.data.order
                });
                this.setState({
                    amend: false
                });
                break;
            case "SUBMIT_AMEND":
                this._submitAmendment();
                break;
            case "AMEND":
                ewars.z.dispatch("REPORTING", "SET_BACKUP", {
                    index: this.props.data.order
                });
                this.setState({
                    amend: true
                });
                break;
            case "RETRACT":
                this.setState({
                    retract: true
                });
                break;
            case "CANCEL_RETRACT":
                this.setState({
                    retract: false,
                    retractReason: ""
                });
                break;
            case "SUBMIT_RETRACT":
                this._submitRetraction();
                break;
            case "SUBMIT":
                this.submit();
                break;
            case 'PRINT':
                window.open(`/print/record/${this.props.data.collection.uuid}`);
                break;
            case "CLAIM":
                this._claim();
                break;
            case "SAVE":
                this._saveDraft();
                break;
            default:
                break;
        }
    };

    _saveDraft = () => {
        let bl = new ewars.Blocker(null, "Saving changes...");
        ewars.tx("com.ewars.collection.draft.create", [this.props.data.collection.uuid, this.props.data.collection])
            .then(resp => {
                ewars.z.dispatch("REPORTING", "SET_COLLECTION", {
                    index: this.props.data.order,
                    data: resp
                });
                bl.destroy();
                ewars.growl("Changes saved");
            })
    };

    _claim = () => {
        ewars.prompt("fa-exclamation-triangle", _l("CLAIM_REPORT"), _l("CLAIM_REPORT_MESSAGE"), function () {
            let bl = new ewars.Blocker(null, "Claiming report...");
            ewars.tx("com.ewars.collection", ["CLAIM", this.props.data.collection.uuid, null])
                .then(function (resp) {
                    bl.destroy();
                    ewars.growl(_l("CLAIM_SUCCESS"));
                    ewars.z.dispatch("REPORTING", "SET_COLLECTION", {
                        index: this.props.data.order,
                        data: resp
                    })
                }.bind(this))
        }.bind(this))
    };

    _submitRetraction = () => {
        if (isEmpty(this.state.redactReason)) {
            ewars.growl("Please provide a reason for the deletion");
            return;
        }

        let bl;
        if (window.user.role == "ACCOUNT_ADMIN") {
            bl = new ewars.Blocker(null, "Deleting form submission...");
        } else {
            bl = new ewars.Blocker(null, "Sending deletion request...");
        }
        ewars.tx("com.ewars.collection.retract", [this.props.data.collection.uuid, this.state.redactReason])
            .then(function (resp) {
                bl.destroy();
                ewars.z.dispatch("REPORTING", "REMOVE_TAB", {
                    index: this.props.data.order
                });
                ewars.growl("Deletion submitted");
            }.bind(this))
    };

    _submitAmendment = () => {
        if (!this.validate()) {
            return;
        }

        if (isEmpty(this.state.amendReason)) {
            ewars.growl("Please provide a reason for the amendment");
            return;
        }

        let bl = new ewars.Blocker(null, "Submitting amendment...");
        ewars.tx("com.ewars.collection.amend", [this.props.data.collection.uuid, this.props.data.collection, this.state.amendReason])
            .then(function (resp) {
                bl.destroy();
                ewars.z.dispatch("REPORTING", "REMOVE_TAB", {index: this.props.data.order});
                ewars.emit("RELOAD_HIST");
                ewars.growl("Amendment submitted");
            }.bind(this))
    };

    validate = () => {
        let errors = {};

        if (this.props.data.form.features.LOCATION_REPORTING) {
            if (isEmpty(this.props.data.collection.location_id)) {
                errors.location_id = ["Please provide a valid location"]
            }
        }

        if (isEmpty(this.props.data.collection.data_date)) {
            errors.data_date = ["Please provide a valid report date"];
        }

        if (!isEmpty(this.props.data.collection.data_date)) {
            let comparator = Moment.utc();
            if (this.props.data.form.id == 4 && window.user.tki == '_21bce401c88d') {
                comparator.add(7, 'days')
            }
            // Check for future dates
            if (Moment.utc(this.props.data.collection.data_date).isAfter(comparator)) {
                if (!errors.data_date) errors.data_date = [];
                errors.data_date = ["You cannot submit reports for a date in the future."];
            }
        }


        let flatDef = FormUtils.flatten(this.props.data.form.version.definition);

        let err = FormUtils.validate(flatDef, this.props.data.collection.data, true);

        if (Object.keys(errors).length > 0) {
            err.hasErrors = true;
            for (var p in errors) {
                err.errors[p] = errors[p];
            }
        }

        if (err.hasErrors) {
            ewars.z.dispatch("REPORTING", "SET_ERRORS", {
                index: this.props.data.order,
                errors: err.errors
            });
            ewars.growl("There are errors in the form, please review and submit again.");
            return false;
        }

        return true;
    };

    submit() {
        if (!this.validate()) {
            return;
        }

        let blocker = new ewars.Blocker(null, "Submitting...");
        ewars.tx("com.ewars.collection.submit", [this.props.data.form.id, this.props.data.form.version_id, this.props.data.collection])
            .then(function (resp) {
                blocker.destroy();
                if (resp.result) {
                    ewars.z.dispatch("REPORTING", "SET_COLLECTION", {
                        index: this.props.data.order,
                        data: resp.report
                    });
                    ewars.growl("Submitted successfully");
                } else {
                    ewars.growl(resp.error);
                }
            }.bind(this))
    }

    _onFieldUpdate = (prop, value) => {
        this.setState({
            [prop]: value
        })
    };

    _onDataChange = (data, prop, value, path) => {
        ewars.z.dispatch("REPORTING", "SET_COLLECTION_DATA", {
            index: this.props.data.order,
            prop: path || prop,
            value: value
        })
    };

    _onRedactChange = (prop, value) => {
        this.setState({
            redactReason: value
        })
    };

    _onAmendChange = (prop, value) => {
        this.setState({
            amendReason: value
        })
    };

    _onAction = (action) => {
        this.setState({
            subView: action
        })
    };

    _onModalAction = () => {
        this.setState({
            subView: null
        })
    };

    render() {
        if (!ewars.g.assignments) return <Spinner/>;
        if (!this.props.data.form.version) return <Spinner/>;

        let readOnly = true;

        if ([CONSTANTS.SUPER_ADMIN, CONSTANTS.ACCOUNT_ADMIN, CONSTANTS.REGIONAL_ADMIN].includes(window.user.role)) readOnly = false;

        if (CONSTANTS.USER == window.user.role && !this.props.data.collection.uuid) {
            readOnly = false;
        }

        if (this.props.data.collection.created_by == window.user.id && this.props.data.collection.status == 'DRAFT') {
            readOnly = false;
        }

        if (["PENDING_AMENDMENT", "PENDING_AMEND", "SUBMITTED", "PENDING_RETRACTION"].indexOf(this.props.data.collection.status) >= 0) {
            readOnly = true;
        }
        if (this.state.amend) readOnly = false;

        let features = [];

        FEATURES.forEach((item) => {
            if (this.props.data.form.features[item]) {
                let Cmp = FEATURE_MAP[item];
                features.push(<Cmp form={this.props.data.form} data={this.props.data.data}/>)
            }
        });

        let submitDate = ewars.DATE(this.props.data.collection.submitted, "dddd, MMMM Do YYYY, h:mm a");

        let userName;
        if (this.props.data.collection.creator_name) userName = this.props.data.collection.creator_name;
        if (this.props.data.collection.user) userName = this.props.data.collection.user.name;

        let subView;
        if (this.state.subView) {
            let Cmp = sub_views[this.state.subView];
            subView = <Cmp data={this.props.data}/>
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row height={35}>
                    <ewars.d.Cell>
                        <ReportToolbar
                            amend={this.state.amend}
                            retract={this.state.retract}
                            claim={this.state.claim}
                            readOnly={readOnly}
                            rid={this.props.data.collection.uuid}
                            form={this.props.data.form}
                            onAction={this.onReportAction}
                            data={this.props.data.collection}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div className="form-error-bar">

                            </div>
                            <div className="report-header">
                                <div className="report-title">{ewars.I18N(this.props.data.form.name)}</div>
                                {this.props.data.collection.uuid ?
                                    <div className="report-sub-title">Submitted by <a
                                        href={"/user#/" + this.props.data.collection.created_by}
                                        target="_blank">{userName}</a>
                                        &nbsp;on {submitDate}</div>
                                    : null}
                            </div>

                            <div className="report-info">
                                <p className="small">Fields with an * are required.</p>
                            </div>


                            {this.state.amend ?
                                <div className="widget">
                                    <div className="body">
                                        <div className="article">
                                            <p>Amendment Reason</p>
                                            <label htmlFor="">Please provide a reason for the amendments</label>
                                            <TextAreaField
                                                name="amend_reason"
                                                value={this.state.amendReason}
                                                config={{i18n: false}}
                                                onUpdate={this._onAmendChange}/>
                                        </div>
                                    </div>
                                </div>
                                : null}

                            <div style={{paddingLeft: 25, paddingRight: 25}}>

                                <ReportDetails
                                    readOnly={readOnly}
                                    errors={this.props.data.errors}
                                    index={this.props.data.order}
                                    data={this.props.data.collection}
                                    form={this.props.data.form}/>

                                <Form
                                    definition={ewars.copy(this.props.data.form.version.definition)}
                                    data={this.props.data.collection.data}
                                    readOnly={readOnly}
                                    errors={this.props.data.errors}
                                    updateAction={this._onDataChange}/>

                            </div>

                            {features}

                        </ewars.d.Panel>
                    </ewars.d.Cell>

                    {this.props.data.collection.uuid && this.props.data.collection.status == "SUBMITTED" ?
                        <ewars.d.Cell borderLeft={true} width="30px">
                            <ewars.d.Button
                                onClick={() => {
                                    this._onAction("Discussion")
                                }}
                                icon="fa-comment"/>
                            <div className="clearer"></div>
                            {/*<ewars.d.Button*/}
                                {/*onClick={() => {*/}
                                    {/*this._onAction("History")*/}
                                {/*}}*/}
                                {/*icon="fa-history"/>*/}
                            {/*<div className="clearer"></div>*/}
                            <ewars.d.Button
                                onClick={() => {
                                    this._onAction("Amendments")
                                }}
                                icon="fa-code-branch"/>
                            <div className="clearer"></div>
                            <ewars.d.Button
                                onClick={() => {
                                    this._onAction("Alerts")
                                }}
                                icon="fa-bell"/>
                            <div className="clearer"></div>
                            {/*<ewars.d.Button*/}
                                {/*onClick={() => {*/}
                                    {/*this._onAction("Info")*/}
                                {/*}}*/}
                                {/*icon="fa-info-circle"/>*/}
                            {/*<div className="clearer"></div>*/}
                            <ewars.d.Button
                                onClick={() => {
                                    this._onAction("UserView")
                                }}
                                icon="fa-user"/>
                            <div className="clearer"></div>
                            <ewars.d.Button
                                onClick={() => {
                                    this._onAction("Location")
                                }}
                                icon="fa-map-marker"/>
                        </ewars.d.Cell>
                        : null}
                </ewars.d.Row>
                {this.state.retract ?
                    <Modal
                        title="Delete Report"
                        icon="fa-trash"
                        onAction={this.onReportAction}
                        visible={this.state.retract}
                        buttons={RETRACT_BUTTONS}>
                        {window.user.role == "USER" ?
                            <div className="article" style={{height: 200, padding: 16}}>
                                <p>Your deletion request will be completed once an administrator has reviewed and
                                    approved
                                    the deletion. Once this deletion is submitted, the report will no longer be visible
                                    in
                                    your list of reports.</p>

                                <label>Reason for deletion:</label>
                                <TextAreaField
                                    name="retractReason"
                                    config={{i18n: false}}
                                    onUpdate={this._onRedactChange}
                                    value={this.state.redactReason}/>
                            </div>
                            :
                            <div className="article" style={{height: 200, padding: 16}}>
                                <p>Are you sure you want to remove this form submission from the system? Any associated
                                    data
                                    will be removed as well.</p>
                                <label>Reason for deletion:</label>
                                <TextAreaField
                                    name="retractReason"
                                    config={{i18n: false}}
                                    onUpdate={this._onRedactChange}
                                    value={this.state.redactReason}/>
                            </div>
                        }
                    </Modal>
                    : null}
                <ewars.d.Shade
                    shown={this.state.subView}
                    title="Comething"
                    onAction={this._onModalAction}
                    toolbar={false}>
                    {subView}

                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default Report;
