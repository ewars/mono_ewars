import {
    ADD_TAB,
    REMOVE_TAB,
    SET_FORM,
    SET_COLLECTION,
    SET_COLLECTION_PROP,
    SET_ACTIVE_TAB,
    SET_COLLECTION_DATA,
    SET_FORM_GRID,
    SET_BACKUP,
    RESTORE_BACKUP,
    DESTROY_BACKUP,
    SET_ERRORS
} from "../actions";

import Moment from "moment";

let DEFAULT = {
    activeTab: 0,
    tabs: [{
        order: 0,
        title: "Dashboard",
        icon: "fa-globe",
        type: "DASHBOARD"
    }]
};

let curLoc = document.location.toString();

if (curLoc.indexOf("#?") > 0) {
    // Specifying a form to load up
    let data = (curLoc.split("#?")[1]).split("&");
    let dict = {};
    data.forEach(item => {
        let spec = item.split("=");
        dict[spec[0]] = spec[1];
    });

    if (dict.form_id) {

        ewars.z.dispatch("REPORTING", "ADD_TAB", {
            title: "New Report",
            icon: "fa-clipboard",
            form: {
                id: dict.form_id,
                name: null,
                version: null
            },
            collection: {
                form_id: dict.form_id,
                created_by: window.user.id,
                data: {},
                location_id: dict.location_id || null,
                data_date: dict.data_date || null
            },
            uuid: ewars.utils.uuid(),
            type: "REPORT"
        });
    }

    if (dict.uuid) {
        let joins = [
            "form:form_id:id:id,name",
            "user:created_by:id:id,name,email"
        ];
        ewars.tx("com.ewars.resource", ["collection", dict.uuid, null, joins])
            .then(resp => {
                ewars.z.dispatch("REPORTING", "ADD_TAB", {
                    title: `${ewars.I18N(resp.form.name)}`,
                    icon: "fa-clipboard",
                    form: {
                        id: resp.form_id,
                        version: null
                    },
                    collection: resp,
                    uuid: ewars.utils.uuid(),
                    type: "REPORT"
                })
            })
    }
}

let FORMS = {};
let FORM_VERSIONS = {};
let BACKUPS = {};

function removeTab(state, index) {
    let removalTab = state.tabs[index];



    state.tabs.splice(index, 1);
    let order = 0;

    state.tabs.forEach(item => {
        item.order = order;
        order++;
    });

    state.activeTab = index - 1 || 0;


    return state;
}

/**
 * Get default values out of a form definition
 * @param definition
 * @returns {{}}
 */
const getDefaults = (definition) => {
    let defaults = {};

    for (let i in definition) {
        if (definition[i].default) {
            defaults[i] = definition[i].default;
        }


        if (definition[i].fields) {
            // This field has children,
            // need to get it's children as well
            // iterate rows
            for (let v in definition[i].fields) {
                // iterate fieds

                for (let x in definition[i].fields[v].fields) {
                    if (definition[i].fields[v].fields[x].default) {
                        defaults[`${i}.${v}.${x}`] = definition[i].fields[v].fields[x].default;
                    }
                }
            }
        }
    }

    return defaults;
};

function addTab(state, data) {
    state.tabs.push(data);

    let order = 0;
    state.tabs.forEach(item => {
        item.order = order;
        order++;
    });


    if (data.type == "DRAFT") {
        if (FORMS[data.form.id]) {
            data.form = FORMS[data.form.id];

            data.collection.data = {
                ...getDefaults(data.form.version.definition),
                ...data.collection.data,
            };
        }
    }

    state.activeTab = order - 1;

    return state;
}

function setForm(state, index, form) {
    FORMS[form.id] = form;

    state.tabs[index].form = form;

    if (state.tabs[index].type == 'DRAFT') {
        state.tabs[index].collection.data = {
            ...getDefaults(form.version.definition),
            ...state.tabs[index].collection.data
        }
    }

    return state;
}

function setCollection(state, index, data) {
    if (data.status == "SUBMITTED" && data.uuid != null) {
        state.tabs[index].title = state.tabs[index].title.replace("Draft", "Submitted");
    }
    state.tabs[index].collection = data;
    state.tabs[index].errors = null;

    return state;
}

function setRootProp(state, index, prop, value) {
    let orgValue = value;
    if (prop == "data_date") orgValue = Moment(value).format("YYYY-MM-DD");
    state.tabs[index].collection[prop] = orgValue;
    state.tabs[index].errors = null;

    return state;
}

function setData(state, index, prop, value) {
    console.log("STATE", prop, value);
    state.tabs[index].collection.data[prop] = value;
    state.tabs[index].errors = null;

    return state;
}

function setFormGrid(state, index, form_id, data) {
    state.tabs[index].grid = data;
    return state;
}

function setBackup(state, index) {
    BACKUPS[index] = ewars.copy(state.tabs[index].collection);

    return state;
}


function restoreBackup(state, index) {
    state.tabs[index].collection = BACKUPS[index];

    delete BACKUPS[index];

    return state;
}

function setErrors(state, index, data) {
    state.tabs[index].errors = data;

    return state;
}

export default (state = DEFAULT, action, data) => {
    let t_state = ewars.copy(state);

    switch (action) {
        case ADD_TAB:
            return addTab(t_state, data);
        case REMOVE_TAB:
            return removeTab(t_state, data.index);
        case SET_FORM:
            return setForm(t_state, data.index, data.form);
        case SET_COLLECTION:
            return setCollection(t_state, data.index, data.data);
        case SET_COLLECTION_PROP:
            return setRootProp(t_state, data.index, data.prop, data.value);
        case SET_ACTIVE_TAB:
            t_state.activeTab = data.index;
            return t_state;
        case SET_COLLECTION_DATA:
            return setData(t_state, data.index, data.prop, data.value);
        case SET_FORM_GRID:
            return setFormGrid(t_state, data.index, data.form_id, data.data);
        case SET_BACKUP:
            return setBackup(t_state, data.index);
        case RESTORE_BACKUP:
            return restoreBackup(t_state, data.index);
        case SET_ERRORS:
            return setErrors(t_state, data.index, data.errors);
        default:
            return t_state;
    }
}

