let state;

class MainState {
    constructor(props) {
        state = this;
        this.state = props;
        this.user = window.user;
        this.assignments = null;
        this.forms = null;
        this.subscriptions = {};
        this.subStates = {};

        this.tabs = [];
        this.activeTab = {id: "DEFAULT"};

        this.menu = "DEFAULT";
        this.viewMain = "DEFAULT";

        this.listeners = {};
    }

    on = () => {

    };

    off = () => {

    };

    register = (id, state) => {

    };
}

export {state, MainState};
export default MainState;