const GROUP_ACTIONS = [
    ['fa-cog', 'SETTINGS', "Team settings"]
];

class ViewTeam extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} style={{display: "flex", flexDirection: "column"}}>
                        <ewars.d.Toolbar label="Team Name">
                            <ewars.d.ActionGroup
                                right={true}
                                actions={GROUP_ACTIONS}/>
                        </ewars.d.Toolbar>
                        <ewars.d.Row style={{display: "block", overflowY: "auto"}}>

                        </ewars.d.Row>
                        <ewars.d.Row borderTop={true} height="60px" style={{display: "block"}}>

                        </ewars.d.Row>

                    </ewars.d.Cell>
                    <ewars.d.Cell width="250px" style={{display: "flex", flexDirection: "column"}}>
                        <ewars.d.Toolbar style={{padding: "0"}}>
                            <div className="ux-tabs">
                                <div className="ux-tab active">
                                    <i className="fal fa-users"></i>
                                </div>
                                <div className="ux-tab">
                                    <i className="fal fa-file"></i>
                                </div>
                                <div className="ux-tab">
                                    <i className="fal fa-cog"></i>
                                </div>
                            </div>

                        </ewars.d.Toolbar>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewTeam;