class TasksHandle extends React.Component {
    state = {
        task_count: 0
    }

    constructor(props) {
        super(props);
    }

    render() {
        let className = "dash-handle";
        if (this.props.active) className += " active";
        return (
            <div
                onClick={this.props.onClick}
                className={className}>
                <i className="fal fa-tasks"></i>&nbsp;Tasks
            </div>
        )
    }
}

class UserHandle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let className = "dash-handle";
        if (this.props.active) className += " active";
        return (
            <div
                onClick={this.props.onClick}
                className={className}>
                <i className="fal fa-user"></i>&nbsp;{window.user.name}
            </div>
        )
    }
}


class DashHandle extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let className = "dash-handle sub-item";

        let iconClass, label;
        if (!Array.isArray(this.props.data)) {
            if (this.props.activeId == this.props.data.uuid) className += " active";

            iconClass = `fa ${this.props.data.icon || ""}`;
            label = this.props.data.name.replace(" (Default)", "");
        }

        if (Array.isArray(this.props.data)) {
            return (
                <div className={className}>
                    <div style={style.section}>{this.props.data[0]}</div>

                    <div style={style.childs}>
                        {this.props.data[2].map((item) => {
                            return (
                                <DashHandle
                                    key={item.uuid}
                                    child={true}
                                    activeId={this.props.activeId}
                                    onClick={this.props.onClick}
                                    data={item}/>
                            )
                        })}
                    </div>
                </div>
            )
        } else {
            let style = {padding: 4};
            return (
                <div className={className} onClick={() => {
                    this.props.onClick(this.props.data.uuid)
                }}>
                    {label}
                </div>
            )
        }
    }
}

ewars.g.dashboard = {};

class MainMenu extends React.Component {
    state = {
        dashboards: []
    };

    constructor(props) {
        super(props);

    }

    render() {
        let dashboards = this.props.dashboards.sort((a, b) => {
            if (a.order > b.order) return 1;
            if (a.order < b.order) return -1;
            return 0;
        })
        return (
            <ewars.d.Row style={{overflowY: "auto", display: "block", background: "#333333", paddingTop: "8px"}}>
                {/*
                <UserHandle
                    active={this.state.view == "USER"}
                    onClick={() => this.props.onViewChange("USER", {})}/>
                   */}

                <TasksHandle
                    onClick={() => this.props.onViewChange("TASKS", {})}
                    active={this.state.view == "TASKS"}/>

                <div className="dash-section">
                    <i className="fal fa-tachometer"></i>&nbsp;Dashboards
                </div>

                {dashboards.map((dash) => {
                    return <DashHandle
                        activeId={this.state.activeDashboardId}
                        data={dash}
                        onClick={(data) => this.props.onViewChange("DASHBOARD", dash)}
                        key={dash.uuid || null}/>
                })}

                {/*
                <div className="dash-section">
                    <i className="fal fa-users"></i>&nbsp;Teams
                </div>

                <div
                    onClick={() => this.props.onViewChange("TEAM", {})}
                    className="dash-handle sub-item">
                    Malaria team
                </div>
                */}

            </ewars.d.Row>
        )
    }
}

export default MainMenu;
