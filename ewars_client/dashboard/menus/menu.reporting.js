class ReportingMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            assignments: []
        };

        ewars.ws("com.ewars.user.assignments", [])
            .then(res => {
                console.log(res);
                this.setState({
                    assignments: res
                })
            })
            .catch(err => {
                console.log(err);
                ewars.error("Error loading your assignments")
            })
    }

    render() {
        return (
            <ewars.d.Row style={{overflowY: "auto", display: "block", background: "#333333", paddingTop: "8px"}}>
                <div className="dash-handle">
                    <i className="fal fa-clock"></i>&nbsp;Overdue records
                </div>

                <div className="dash-handle">
                    <i className="fal fa-clipboard"></i>&nbsp;Drafts
                </div>

                {this.state.assignments.map(item => {
                    return (
                        <div className="dash-handle">{item.form_name.en || item.form_name}</div>
                    )
                })}

                <div className="dash-handle">
                    <i className="fal fa-plus"></i>&nbsp;Request Assignment
                </div>

            </ewars.d.Row>
        )
    }
}

export default ReportingMenu;