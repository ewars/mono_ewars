class MenuAdmin extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Row style={{overflowY: "auto", display: "block", background: "#333333", paddingTop: "8px"}}>
                <div className="dash-handle">
                    <i className="fal fa-clipboard"></i>&nbsp;Forms
                </div>
                <div
                    onClick={() => this.props.onViewChange("ALARMS", {})}
                    className="dash-handle">
                    <i className="fal fa-bell"></i>&nbsp;Alarms
                </div>
                <div
                    onClick={() => this.props.onViewChange("LOCATIONS", {})}
                    className="dash-handle">
                    <i className="fal fa-map-marker"></i>&nbsp;Locations
                </div>
                <div className="dash-handle">
                    <i className="fal fa-users"></i>&nbsp;Users
                </div>

                <div className="dash-handle">
                    <i className="fal fa-upload"></i>&nbsp;Data import
                </div>

                <div className="dash-handle">
                    <i className="fal fa-cog"></i>&nbsp;Settings
                </div>

                <div className="dash-handle">
                    <i className="fal fa-mobile"></i>&nbsp;Devices
                </div>

                <div className="dash-handle">
                    <i className="fal fa-code-branch"></i>&nbsp;Indicators
                </div>

                <div className="dash-handle">
                    <i className="fal fa-users"></i>&nbsp;Teams
                </div>
                <div className="dash-handle">
                    <i className="fal fa-tag"></i>&nbsp;Contexts
                </div>
                <div className="dash-handle">
                    <i className="fal fa-globe"></i>&nbsp;Sites
                </div>
                <div className="dash-handle">
                    <i className="fal fa-book"></i>&nbsp;Document templates
                </div>

                <div className="dash-handle">
                    <i className="fal fa-tachometer"></i>&nbsp;Dashboards
                </div>
                <div className="dash-handle">
                    <i className="fal fa-clipboard"></i>&nbsp;Submissions
                </div>
                <div className="dash-handle">
                    <i className="fal fa-graduation-cap"></i>&nbsp;Guidance
                </div>
                <div
                    onClick={() => this.props.onViewChange("ROLES", {})}
                    className="dash-handle">
                    <i className="fal fa-users"></i>&nbsp;Roles
                </div>

            </ewars.d.Row>
        )
    }
}

export default MenuAdmin;