class AlertsMenu extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ewars.d.Row style={{display: "block", paddingTop: "8px", background: "#333333"}}>
                <div className="dash-handle">
                    <i className="fal fa-bell"></i>&nbsp;All Alerts
                </div>

                <div className="dash-section">
                    <i className="fal fa-bell"></i>&nbsp;EWARS Alerts
                </div>
                <div className="dash-handle sub-item">Malaria</div>

                <div className="dash-section">
                    <i className="fal fa-bell"></i>&nbsp;Personal Alerts
                </div>
            </ewars.d.Row>
        )
    }
}

export default AlertsMenu;