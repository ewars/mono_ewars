class AnalysisMenu extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <ewars.d.Row style={{overflowY: "auto", display: "block", background: "#333333"}}>
                <div className="dash-section">
                    <i className="fal fa-book"></i>&nbsp;Documents
                </div>

                <div className="dash-handle sub-item">
                    Weekly Epi Bulletin
                </div>

                <div className="dash-handle">
                    <i className="fal fa-book"></i>&nbsp;Notebooks
                </div>
                <div className="dash-handle">
                    <i className="fal fa-map"></i>&nbsp;Mapping
                </div>
                <div className="dash-handle">
                    <i className="fal fa-chart-line"></i>&nbsp;Plots
                </div>
                <div className="dash-handle">
                    <i className="fal fa-download"></i>&nbsp;Data export
                </div>
                <div className="dash-handle">
                    <i className="fal fa-tachometer"></i>&nbsp;Dashboards
                </div>
                <div className="dash-handle">
                    <i className="fal fa-search"></i>&nbsp;M&E Auditor
                </div>
                <div className="dash-handle">
                    <i className="fal fa-leaf"></i>&nbsp;HUDs
                </div>
            </ewars.d.Row>
        )
    }
}

export default AnalysisMenu;