# Building individual clients

We use a homegrown build tool called `packer` built with python in order to run watchers for development and build/version individual client-side applications.

The `common` folder contains ui and library components common to all sub-applications. 

## Repo Setup

We use `yarn` to install and manage depdencies here. Instructions for how to install `yarn` can be found here https://yarnpkg.com/en/docs/install#mac-stable

## Future Changes

This repo has a lot of churn, UI components are changing constantly and individual components change frequently as well or get merged or split apart depending on the requirements.

We're currently in the process of making the `common` folder the defacto application folder and removing the individual `<folder>/app.js` files to top-level `<app_name>.js` files to simplify the build process and make managing the application a little easier.

## `ewars` sub-application

The `ewars` sub-application is a globals library with functionality common to all the client-side applications. Any change in the `ewars` sub-application impacts all sub-applications. So be **careful** making changes to this library components. Efforts are being made to move this functionality into a common `Application` outer component that wraps all applications and uses a combination of globals and context props to provide it's functionality.

## Packer

### Usage

`./packer [options] target`

### Help

`./packer -h`

```
usage: packer [-h] [-w] [-b] [-i] [-d] [-c] [target]

Client-side build

positional arguments:
  target           The target(s) to take action on

optional arguments:
  -h, --help       show this help message and exit
  -w, --watch
  -b, --build      Build a set of client applications
  -i, --increment  Increment the version number for a client application
  -d, --dev        Build a dev bundle for the application
  -c, --clean      Clean up bundle directory
```

### Watch

`./packer -w <target>`

Hit `Ctrl+c` to cancel watch

To watch multiple targets, use multiple terminals

### Build

Will only build components whose major version number is > 0 and enabled
is set to true

Build specific target: `./packer -b <target>`  
Build all: `./packer -b -`  
Build subset `./packer -b <target>,<target>`

Pass `-i` to auto-increment the minor version number

Generates a `<target>-<version>.js.gz` and `<target>-<version>.map` for
the given target.

Compressed using Zopfli compression.

### Dev Builds

Build specific target: `./packer -d <target>`  
Build all: `./packer -d -`  
Build subset `./packer -d <target>,<target>`

Generates a `<target>-dev.js` and `<target>-dev.map` file

### Clean

`./packer -c`

