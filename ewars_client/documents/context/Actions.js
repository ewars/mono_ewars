var AppDispatcher = require("./AppDispatcher");
var CONSTANTS = require("../../common/constants");

var Actions = {
    addTab: function (tab) {
        AppDispatcher.handleViewAction({
            actionType: "ADD_TAB",
            data: tab
        })
    }
};

module.exports = Actions;
