import Moment from "moment";
import DateUtils from "./DateUtils";

var TEMPLATE_CODES = [
    [/{year}/g, function (templateDefinition) {
        return Moment(templateDefinition.doc_date).format("YYYY");
    }],
    [/{report_date}/g, function (templateDefinition) {
        return ewars.formatters.DATE_FORMATTER(templateDefinition.doc_date, templateDefinition.interval || "DAY");
    }],
    [/{report_start_date}/g, function (templateDefinition) {
        let startDate = DateUtils.getStartDate(templateDefinition.doc_date, templateDefinition.interval || "DAY");
        return Moment(startDate).format("DD MMMM YYYY");
    }],
    [/{report_end_date}/g, function (templateDefinition) {
        return Moment(templateDefinition.doc_date).format("DD MMMM YYYY");
    }],
    [/{month}/g, function (templateDefinition) {
        return Moment(templateDefinition.doc_date).format("MM");
    }],
    [/{day}/g, function (templateDefinition) {
        return Moment(templateDefinition.doc_date).format("DD");
    }],
    [/{location_name}/g, function (templateDefinition) {
        if (templateDefinition.location) return ewars.I18N(templateDefinition.location.name);
        return "";
    }],
    [/{day_week}/g, function (templateDefinition) {
        return Moment(templateDefinition.doc_date).format("dddd");
    }],
    [/{week_no}/g, function (templateDefinition) {
        return Moment(templateDefinition.doc_date).format("W");
    }],
    [/{week}/g, function (templateDefinition) {
        return Moment(templateDefinition.doc_date).format("W");
    }]
];

const ReportUtils = {
    applyFormattingTags: function (stringToFormat, templateDefinition) {
        let content = stringToFormat;
        TEMPLATE_CODES.forEach(function (tCode) {
            let value = tCode[1](templateDefinition);
            content = content.replace(tCode[0], value);
        }.bind(this));

        return content;
    }
};

export default ReportUtils;
