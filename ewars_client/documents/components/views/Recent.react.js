import Moment from "moment"

import DateUtils from "../../utils/DateUtils";
import ReportUtils from "../../utils/ReportUtils";
import { DateField, SelectField } from "../../../common/fields";
import { Spinner } from "../../../common";
import CONSTANTS from "../../../common/constants";


const STYLES = {
    new: {
        position: "absolute",
        top: "50%",
        marginTop: "-200px",
        textAlign: "center",
        width: "100%"
    },
    newIcon: {
        fontSize: "60px",
        color: "#CCC",
        marginBottom: "20px"
    }
};


var Dashboard = React.createClass({
    _isLoading: true,

    getInitialState: function () {
        return {
            documents: []
        }
    },

    render: function () {
        let reports;
        if (this._isLoading) {
            reports = <Spinner/>;
        } else {
            reports = this.state.documents.sort(function (a, b) {
                if (a.doc_date > b.doc_date) return -1;
                if (a.doc_date < b.doc_date) return 1;
                return 0;
            });
            reports = reports.map(function (document) {
                return <RecentReportItem data={document} onClick={this.props.onReport}/>;
            }.bind(this));

            if (reports.length <= 0) {
                reports = <p className="placeholder">There are no recently published documents available.</p>
            }
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>


                        <div className="map-new" style={STYLES.new}>
                            <div className="icon" style={STYLES.newIcon}><i className="fal fa-book"></i></div>
                            <div className="text" style={{marginBottom: 20}}>Browse documents and reports, select an item from left to begin.</div>
                        </div>

                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
});

export default Dashboard;
