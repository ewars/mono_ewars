import DateUtils from "../../utils/DateUtils";
import Moment from "moment";
import ReportUtils from "../../utils/ReportUtils";
import { Spinner } from "../../../common";

import Generated from "../template_types/Generated";
import OneOff from "../template_types/OneOff";
import PerReport from "../template_types/PerReport";

import CONSTANTS from "../../../common/constants";

const LCONSTANTS = {
    DESC: "DESC",
    ASC: "ASC"
};

var ReportItem = React.createClass({
    onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.template.instance_name),
            this.props.data
        );

        if (this.props.data.status == CONSTANTS.DRAFT) instanceName = "DRAFT - " + instanceName;

        return (
            <div className="template-report-item" onClick={this.onClick}>
                {instanceName}
            </div>
        )
    }
});

var ReportBrowser = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        return {
            SORT_DATE: LCONSTANTS.DESC,
            locations: [],
            documents: [],
            total: 0
        }
    },

    componentWillMount: function () {
        var blocker = new ewars.Blocker(null, "Loading Reports...");
        if (this.props.data.template_type != "ONE_OFF") {
            ewars.tx("com.ewars.document", ["REPORTS", this.props.data.uuid, {offset: 0, limit: 50}])
                .then(function (resp) {
                    blocker.destroy();
                    this._isLoaded = true;
                    this.setState({
                        documents: resp.documents,
                        total: resp.total
                    })
                }.bind(this))
        }
    },

    componentWillReceiveProps: function (nextProps) {
        var blocker = new ewars.Blocker(null, "Loading Reports...");
        if (this.props.data.template_type != "ONE_OFF") {
            ewars.tx("com.ewars.document", ["REPORTS", nextProps.data.uuid, {offset: 0, limit: 50}])
                .then(function (resp) {
                    blocker.destroy();
                    this._isLoaded = true;
                    this.setState({
                        documents: resp.documents,
                        total: resp.total
                    })
                }.bind(this))
        }
    },

    _onReportSelect: function (data) {
        this.props.onReportView(data);
    },

    render: function () {
        if (!this._isLoaded) return <Spinner/>;

        if (this.state.documents.length <= 0) return <div className="placeholder">There are currently no documents
            available of this type</div>;


        if (!this._isLoaded) return <div className="dummy"></div>;

        var reportName = ewars.I18N(this.props.data.template_name);

        let reports = [];
        if (this.state.documents) {
            reports = this.state.documents.map(function (document) {
                return <ReportItem data={document} onClick={this._onReportSelect} template={this.props.data}/>
            }.bind(this))
        }



        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="ide-tbar-text">{reportName}</div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="template-report-list" id="dms-browser-list">
                                {reports}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">

                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default ReportBrowser;
