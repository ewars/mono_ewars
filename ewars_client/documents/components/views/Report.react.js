import Moment from "moment";
import DateUtils from "../../utils/DateUtils";
import ReportUtils from "../../utils/ReportUtils";

import {
    Layout as IDELayout,
    Row as IDERow,
    Cell as IDEColumn,
    Modal,
    Button,
    Spinner
} from "../../../common";
import { TextField } from "../../../common/fields";

import Utils from "../../utils";

var CONSTANTS = require("../../../common/constants");

var REPORT_DATA;
window.receiveReportData = function (reportData) {
    REPORT_DATA = reportData;
    ewars.emit("RECEIVED_REPORT_DATA");
};

window.logger = function (item) {
};


var ReportFrame = React.createClass({
    getDefaultProps: function () {
        return {
            uri: null
        }
    },

    shouldComponentUpdate: function (nextProps) {
        if (nextProps.uri != this.props.uri) return true;
        return false;
    },

    getFrame: function () {
        return this.refs.iframeTool;
    },

    render: function () {

        // Add cache bust
        var endURI = this.props.uri + "&cache=" + Date.now();
        return (
            <iframe ref="iframeTool" src={endURI} frameBorder="0" width="100%" height="100%"
                    style={{
                        height: "100%",
                        width: "100%",
                        overflow: "hidden",
                        top: 0,
                        left: 0,
                        position: "absolute"
                    }}></iframe>
        )
    }
});

var Report = React.createClass({
    _isLoading: true,
    _widgets: [],
    _widgetsLoaded: 0,
    _sheets: [],

    getInitialState: function () {
        return {
            isShareVisible: false,
            template: null,
            report_date: null,
            canDownload: false,
            share: {
                recipients: ""
            }
        }
    },

    componentWillMount: function () {
        this.state.report_date = this.props.data.doc_date;
        this._isLoading = true;
        this._widgets = [];
        this._widgetsLoaded = 0;

        ewars.subscribe("DATA_LOADED", this._canDownload);
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.report_date = nextProps.data.report_date;
    },

    componentWillUnmount: function () {
        ewars.unsubscribe("DATA_LOADED", this._canDownload);
        ewars._queue.clear();
    },

    _canDownload: function (data) {
        let resultant = Utils.formatData(data);

        ewars.emit("DATA_AVAILABLE", resultant);
    },

    shouldComponentUpdate: function () {
        return false;
    },

    componentWillUpdate: function () {
        return false;
    },

    _onBrowse: function () {
        this.props.onBrowse(this.props.data.report_template);
    },

    _share: function () {
        this.state.isShareVisible = true;
        this.forceUpdate();
    },

    _cancelShare: function () {
        this.state.isShareVisible = false;
        this.forceUpdate();
    },

    _submitShare: function () {
        let reportURI = "/report?";
        let segments = [];

        if (this.props.data.doc_date) segments.push("rid=" + Moment(this.props.data.doc_date).format("YYYY-MM-DD"));
        if (this.props.data.uuid) segments.push("tid=" + this.props.data.uuid);
        if (this.props.data.report_uuid) {
            if (this.props.data.report_uuid) segments.push("cid=" + this.props.data.report_uuid);
        }
        if (this.props.data.location) {
            if (this.props.data.location.uuid) segments.push("lid=" + this.props.data.location.uuid);
        }
        if (this.props.data.template_type) {
            segments.push("tt=" + this.props.data.template_type);
        }

        reportURI = reportURI + segments.join("&");

        if (!this.state.share.recipients) {
            ewars.growl("Please provide valid recipient(s)");
            return;
        }

        if (this.state.share.recipients.length <= 0) {
            ewars.growl("Please provide valid recipient(s)");
            return;
        }

        var reportName = ewars.I18N(this.props.data.template_name);
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.data.instance_name),
            this.props.data
        );

        var blocker = new ewars.Blocker(null, "Sharing...");

        let data = {
            uri: reportURI,
            recipients: this.state.share.recipients,
            name: instanceName
        };

        ewars.tx("com.ewars.document", ["SHARE", null, data])
            .then(function (resp) {
                this.state.isShareVisible = false;
                this.state.share.recipients = "";
                this.forceUpdate();
                blocker.destroy();
                ewars.growl("Report shared");
            }.bind(this))
    },

    _onShareSettingsChange: function (prop, value) {
        this.state.share[prop] = value;
        this.forceUpdate();
    },

    _download: function () {
        var instanceName = ReportUtils.applyFormattingTags(ewars.formatters.I18N_FORMATTER(this.props.data.report_template.instance_name),
            this.props.data.report_template,
            this.state.report_date,
            this.props.data.location);


        $.download("http://" + ewars.domain + "/excel", {
            sourceData: JSON.stringify(REPORT_DATA),
            fileName: instanceName + ".xlsx"
        }, "POST");

    },

    _print: function () {
        var instanceName = ReportUtils.applyFormattingTags(ewars.formatters.I18N_FORMATTER(this.props.data.report_template.instance_name),
            this.props.data.report_template,
            this.state.report_date,
            this.props.data.location);


        $.download("http://" + ewars.domain + "/report/pdf", {
            tid: this.props.data.report_template.uuid,
            rid: Moment(this.state.report_date).format("YYYY-MM-DD"),
            lid: this.props.data.location.uuid,
            fileName: instanceName,
            orientation: this.state.report_template.orientation
        }, "POST");
    },

    _pdf: function () {
        var blocker = new ewars.Blocker(null, "Generating pdf...");

        let reportURI = "/document/pdf/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        if (this.props.data.doc_date) segments.d = ewars.DATE(this.props.data.doc_date, "DAY");
        if (this.props.data.report_uuid) segments.c = this.props.data.report_uuid;
        if (this.props.data.location) segments.l = this.props.data.location.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        reportURI = reportURI + window.btoa(segs.join(";"));

        reportURI = reportURI + "?ts=" + (new Date()).getTime();

        $.ajax({
            url: reportURI,
            type: "GET",
            contentType: "application/json",
            dataType: "json",
            success: function (resp) {
                window.open("http://" + ewars.domain + "/document/download/" + resp.name);
                blocker.destroy();
            },
            error: function () {
                blocker.destroy();
                ewars.growl("There was an error generating your PDF");
            }
        })
    },

    _onNext: function () {
        var nextDate = DateUtils.getNextPeriod(this.state.report_date, this.props.data.report_template.generation.interval);
        this.state.report_date = nextDate;
        this.forceUpdate();
    },

    _onPrevious: function () {
        var prevDate = DateUtils.getPreviousPeriod(this.state.report_date, this.props.data.report_template.generation.interval);
        this.state.report_date = prevDate;
        this.forceUpdate();
    },

    render: function () {
        var reportName = ewars.I18N(this.props.data.template_name);
        let instanceName = ReportUtils.applyFormattingTags(reportName, this.props.data);


        if (this.props.data.status == CONSTANTS.DRAFT) instanceName = "DRAFT - " + instanceName;

        var buttons = [
            {label: "Share", colour: "green", onClick: this._submitShare, icon: "fa-share"},
            {label: "Cancel", onClick: this._cancelShare, icon: "fa-times"}
        ];

        var report = (
            <div className="report-template-wrapper" id="report-content">
            </div>
        );

        let reportURI = "/document/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
        };

        console.log(this.props);
        if (this.props.data.doc_date) segments.d = ewars.DATE(this.props.data.doc_date, "DAY");
        if (this.props.data.report_uuid) segments.c = this.props.data.report_uuid;
        if (this.props.data.location) segments.l = this.props.data.location.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        reportURI = reportURI + window.btoa(segs.join(";"));

        return (
            <div className="ide-panel ide-panel-absolute">
                <div className="ide">
                    <div className="ide-layout">
                        {this.props.data.status == CONSTANTS.DRAFT ?
                            <div className="ide-row" style={{maxHeight: 20}}>
                                <div className="ide-col"
                                     style={{
                                         backgroundColor: "rgb(255, 0, 32)",
                                         color: "white",
                                         textAlign: "center",
                                         paddingTop: 2
                                     }}>
                                    DRAFT REPORT - UNDER ACTIVE DEVELOPMENT
                                </div>
                            </div>
                            : null}
                        <div className="ide-row">
                            <div className="ide-col">
                                <ReportFrame uri={reportURI} ref="iframeDoc"/>
                            </div>
                        </div>
                    </div>

                    {this.state.isShareVisible ?
                        <Modal
                            wizard={true}
                            light={true}
                            title="Share Document"
                            buttons={buttons}
                            visible={this.state.isShareVisible}>
                            <div className="content" style={{padding: 16}}>

                                <p>Share this document with other users.</p>

                                <div className="padded">
                                    <label htmlFor="">Recipients*</label>
                                    <p>Enter the email address of each user below to send a link to them.</p>
                                    <p>Note: use a comma to separate multiple email addresses</p>
                                    <TextField
                                        name="recipients"
                                        placeholder="e.g. support@ewars.ws,test@ewars.ws"
                                        onUpdate={this._onShareSettingsChange}
                                        value={this.state.share.recipients}/>
                                </div>
                            </div>
                        </Modal>
                        : null}
                </div>
            </div>
        )
    }
});

export default Report;
