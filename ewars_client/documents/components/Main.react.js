import Recent from "./views/Recent.react";
import ReportBrowser from "./views/ReportBrowser.react";
import Report from "./views/Report.react";
import { Spinner } from "../../common";
import ReportUtils from "../utils/ReportUtils";

import Browser from "./navigation/Browser.react";
import SideView from "./navigation/SideView";
import Repository from "./Repository";

const CONSTANTS = {
    MAIN: "MAIN",
    BROWSER: "BROWSER",
    REPORT: "REPORT"
}


const Main = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        return {
            template: [],
            view: CONSTANTS.MAIN,
            reportTemplate: null,
            report: {},
            shareEmails: "",
            collapsed: false
        }
    },

    componentWillUnmount() {
        ewars.unsubscribe("DATA_AVAILABLE", this._onDownloadData);
    },

    _onDownloadData: function (data) {
        if (this.state.view != CONSTANTS.BROWSER) {
            this.setState({
                reportData: data,
                canDownload: true
            })
        }
    },

    _downloadData: function () {
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.state.document.instance_name),
            {
                location: this.state.document.location,
                doc_date: this.state.document.doc_date,
                interval: this.state.document.interval
            }
        );

        let bl = new ewars.Blocker(null, "Generating excel...");

        ewars.tx("com.ewars.document.data", [instanceName, this.state.reportData[0], this.state.reportData[1]])
            .then((resp) => {
                bl.destroy();
                window.open("http://" + ewars.domain + "/document/download/" + resp.n);
            })
    },

    componentWillMount() {
        ewars.subscribe("DATA_AVAILABLE", this._onDownloadData);
        let uri = document.location;

        let hash = String(uri).split("#")[1];
        if (hash) hash = hash.split("&");

        let dict = {};
        if (hash) {
            hash.forEach(item => {
                let varg = item.split("=");
                dict[varg[0]] = varg[1];
            })
        }

        if (dict['uuid']) {
            // We need to initialize to a specific document type
            ewars.tx("com.ewars.resource", ["template", dict.uuid, null, null])
                .then(resp => {
                    this._isLoaded = true;
                    this.setState({
                        template: resp,
                        view: CONSTANTS.BROWSER
                    })
                })
        } else {
            this._isLoaded = true;
        }
    },

    _onEmailsChange: function (e) {
        this.setState({
            shareEmails: e.target.value
        })
    },

    _renderBrowser: function (reportTemplate) {
        this.state.view = CONSTANTS.BROWSER;
        this.state.report = {};
        this.forceUpdate();
    },

    _onMain: function () {
        this.state.view = CONSTANTS.MAIN;
        this.state.template = null;
        this.state.report = {};
        this.forceUpdate();
    },

    _renderReport: function (document) {
        this.state.view = CONSTANTS.REPORT;
        this.setState({
            document: document,
            canDownload: false
        })
    },

    _onTemplateSelect: function (data) {
        this.setState({
            template: data,
            view: "BROWSER"
        })
    },

    _onClear: function () {
        this.setState({
            template: null,
            navView: "DOCUMENTS",
            view: CONSTANTS.MAIN
        })
    },

    _onDocumentPeriodSelect(year, month) {
    },

    _share: function (report) {
        this.setState({
            showShade: true,
            report: report
        });
    },

    _sendShare: function () {
        let bl = new ewars.Blocker(null, "Sharing document...");

        let uri = "/document/";

        let segments = {
            a: window.user.aid,
            t: this.state.document.uuid,
            k: window.user.tki
        };

        segments.d = ewars.DATE(this.state.document.doc_date, "DAY");
        segments.l = this.state.document.location_id;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        // Get the document name
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.state.document.instance_name),
            {
                location: this.state.document.location,
                doc_date: this.state.document.doc_date,
                interval: this.state.document.interval
            }
        );

        ewars.tx('com.ewars.document.share', [uri, this.state.shareEmails, instanceName])
            .then((resp) => {
                bl.destroy();
                this.setState({
                    showShare: false,
                    report: null,
                    shareEmails: ""
                });
                ewars.growl("Document shared");

            })
    },

    _download: function () {
        let bl = new ewars.Blocker(null, "Generating pdf...");

        let uri = "/document/pdf/";

        let segments = {
            a: window.user.aid,
            t: this.state.document.uuid,
            k: window.user.tki
        };

        segments.d = ewars.DATE(this.state.document.doc_date, "DAY");
        segments.l = this.state.document.location_id || this.state.document.location;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        let r = new XMLHttpRequest();
        r.open("GET", uri);
        r.onreadystatechange = () => {
            if (r.readyState == 4 && r.status == 200) {
                let resp = JSON.parse(r.responseText);
                bl.destroy();
                window.open("http://" + ewars.domain + "/document/download/" + resp.name);
            }
        };

        r.send();
    },

    _onModalAction: function (action) {
        this.setState({
            showShade: false
        })
    },

    _toggleCollapse: function () {
        this.setState({
            ...this.state,
            collapsed: !this.state.collapsed
        })
    },

    render: function () {
        if (!this._isLoaded) return (
            <Spinner/>
        );
        var view;

        if (this.state.view == CONSTANTS.MAIN) view =
            <Recent
                renderBrowser={this._renderBrowser}
                onReport={this._renderReport}/>;
        if (this.state.view == CONSTANTS.BROWSER) view =
            <Browser
                onReportView={this._renderReport}
                key={this.state.template.uuid}
                onBack={this._onMain}
                data={this.state.template}/>;
        if (this.state.view == CONSTANTS.REPORT) {
            view = (
                <ewars.d.Layout>
                    <ewars.d.Toolbar>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-file-pdf"
                                onClick={() => {
                                    this._download()
                                }}/>
                            <ewars.d.Button
                                icon="fa-share"
                                onClick={this._share}/>
                            {this.state.canDownload ?
                                <ewars.d.Button
                                    icon="fa-download"
                                    onClick={this._downloadData}/>
                                : null}
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <Report data={this.state.document} onBrowse={this._renderBrowser} onMain={this._onMain}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>

                    <ewars.d.Shade
                        title="Share Document"
                        onAction={this._onModalAction}
                        shown={this.state.showShade}>
                        <div className="article" style={{padding: 16}}>
                            <p>Enter an email address (or multiple email addresses separated by coommas (,)) to share
                                this document.</p>

                            <label>Email Addresses</label>
                            <input type="text" value={this.state.shareEmails} onChange={this._onEmailsChange}/>
                            <div className="clearer" style={{height: 20}}></div>

                            <ewars.d.Button
                                label="Share"
                                onClick={this._sendShare}/>
                        </div>
                    </ewars.d.Shade>
                </ewars.d.Layout>
            )

        }



        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <SideView onSelect={this._onTemplateSelect}/>
                    <ewars.d.Cell borderTop={true}>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        );
    }
});

export default Main;
