import Moment from "moment";

import { Spinner } from "../../../common";
import CONSTANTS from "../../../common/constants";

var LANG_INTERVAL = {
    "DAY": "Daily",
    "WEEK": "Weekly",
    "MONTH": "Monthly",
    "YEAR": "Annual"
};

let getLastCompletedWeek = function (sDate) {
    if (!sDate) return null;

    let utc = Moment.utc(sDate);

    if (utc.isoWeekday() == 7) {
        // end of week return this date
        return utc.format("YYYY-MM-DD");
    } else {
        let prevWeek = utc.clone().subtract(7, 'd');
        prevWeek.isoWeekday(7);
        return prevWeek.format("YYYY-MM-DD");
    }
};

class ReportTemplateListItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = (e) => {
        this.props.onClick(this.props.data);
    };

    render() {
        var templateName = ewars.formatters.I18N_FORMATTER(this.props.data.template_name);
        if (this.props.data.status == CONSTANTS.DRAFT) templateName = "DRAFT - " + templateName;

        return (
            <div className="block hoverable" onClick={this._onClick}>
                <div className="block-content">
                    <div className="title">{templateName}</div>
                </div>
            </div>
        )
    }
}

class Documents extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            templates: []
        }
    }

    componentWillMount() {
        this._isLoading = true;
        var hasBrowse = false;
        if (document.location.hash.indexOf("uuid") >= 0) {
            var hash = document.location.hash.replace("#uuid=", "");

            this.state.view = CONSTANTS.BROWSER;
            this.state.reportTemplate = {uuid: hash};
        }

        if (ewars.g.templates) {
            this.state.templates = ewars.g.templates;
            this._isLoading = false;
        } else {

            var blocker = new ewars.Blocker(null, "Loading reports...");
            ewars.tx("com.ewars.user.documents", [])
                .then(function (resp) {
                    this._isLoading = false;
                    ewars.g.templates = resp;
                    blocker.destroy();
                    this.setState({
                        templates: resp
                    })
                }.bind(this));
        }
    }

    componentWillUnmount() {
        this._isLoading = true;
    }

    templateClick = (data) => {
        this.props.onTemplateSelect(data);
    };

    render() {
        var templates = this.state.templates.map(function (item) {
            return <ReportTemplateListItem onClick={this.templateClick} data={item.template}/>
        }.bind(this));

        if (templates.length <= 0 && this._isLoading) templates = <Spinner/>;
        if (templates.length <= 0 && !this._isLoading) templates =
            <p className="placeholder">No documents currently available.</p>;

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="block-tree">
                    {templates}
                </div>
            </div>
        )
    }
}

export default Documents;
