import Moment from "moment";

import ReportUtils from "../../utils/ReportUtils";
import { Panel, Spinner } from "../../../common";
import { ButtonGroupField as ButtonGroup } from "../../../common/fields";

import SelectField from "../../../common/c.cmp.select";


const MONTHS = {
    1: __("JAN"),
    2: __("FEB"),
    3: __("MAR"),
    4: __("APR"),
    5: __("MAY"),
    6: __("JUN"),
    7: __("JUL"),
    8: __("AUG"),
    9: __("SEP"),
    10: __("OCT"),
    11: __("NOV"),
    12: __("DEC")
}

const getDays = (year, month, start_date, end_date) => {
    let date = Moment(`${year}-${month}-01`, 'YYYY-MM-DD');
    let results = [];


    let end = Moment(end_date);
    if (Moment().isBefore(end, 'd')) end = Moment();

    let daysInMonth = date.daysInMonth();

    for (let i = 0; i < daysInMonth; i++) {
        let dt = Moment(`${year}-${month}-${i+1}`, 'YYYY-MM-DD');
        if (dt.isBefore(end, 'd')) {
            results.push(dt.format("YYYY-MM-DD"))
        }
    }

    return results.reverse();
}


function range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    }

    if (typeof step == 'undefined') {
        step = 1;
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }

    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }

    return result;
};

import Report from "../views/Report.react";

const getWeeks = (year, end_date) => {
    let isoWeeksInYear = Moment.utc(year).isoWeeksInYear();

    // For each week in the year, generate a list item.
    var weeks2 = [];

    for (var i = 0; i <= isoWeeksInYear; i++) {
        let dt = Moment.utc().isoWeekYear(year).isoWeek(i + 1).isoWeekday(7);
        if (dt.isBefore(Moment.utc())) {
            if (end_date) {
                if (dt.isBefore(Moment.utc(end_date))) {
                    weeks2.push(dt);
                }
            } else {
                weeks2.push(dt)
            }
        }
    }

    weeks2.reverse();

    return weeks2;
};

const O_MONTHS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
const getMonths = (year, start_date, end_date) => {
    let today = Moment(),
        sDate = Moment(start_date),
        eDate = Moment(end_date || null);
    let results = [];

    O_MONTHS.forEach(item => {
        let monthEnd = Moment(`${year}-${item}-01`).endOf("month");
        if (monthEnd.isBefore(today, 'day') && monthEnd.isAfter(sDate, 'd') && monthEnd.isBefore(eDate, 'd')) {
            results.push(monthEnd.format("YYYY-MM-DD"));
        }
    });

    return results.reverse();
};

const getMonthsDays = (year, start_date, end_date) => {
    let thisMonth = Moment().month();
    let thisYear = Moment().year();
    if (thisYear > year) {
        if (end_date.year() == year) thisMonth = end_date.month();
        if (end_date.year() > year) thisMonth = 12;
    }
    let eDate = Moment(end_date);
    let sDate = Moment(start_date);

    let results = [];

    O_MONTHS.forEach(item => {
        let mEnd = Moment(`${year}-${item}-01`).endOf('m');
        if (mEnd.isBefore(eDate, 'd')) {
            if (mEnd.month() <= thisMonth) {
                if (mEnd.isAfter(sDate, 'd')) {
                    results.push(mEnd.format("YYYY-MM-DD"));
                }
            }
        }
    })


    return results.reverse();
};


class Week extends React.Component {
    _download = () => {
        let bl = new ewars.Blocker(null, "Generating pdf...");

        let uri = "/document/pdf/";

        let segments = {
            a: window.user.aid,
            t: this.props.template.uuid,
            k: window.user.tki
        };

        if (this.props.data) segments.d = ewars.DATE(this.props.data, "DAY");
        if (this.props.data.report_uuid) segments.c = this.props.data.report_uuid;
        if (this.props.meta) segments.l = this.props.meta.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));
        uri = uri + "?ts=" + (new Date()).getTime();

        let r = new XMLHttpRequest();
        r.open("GET", uri);
        r.onreadystatechange = () => {
            if (r.readyState == 4 && r.status == 200) {
                let resp = JSON.parse(r.responseText);
                bl.destroy();
                window.location.href = 'http://' + ewars.domain + '/document/download/' + resp.name;
                // window.open("http://" + ewars.domain + "/document/download/" + resp.name);
            }
        };

        r.send();
    };

    _data = () => {

        let bl = new ewars.Blocker(null, "Generating data...");

        let data = {
            lid: this.props.meta.uuid,
            dd: this.props.data
        };

        ewars.tx("com.ewars.document.data", [this.props.template.uuid, data])
            .then((resp) => {
                bl.destroy();
            })

    };

    _share = () => {
        this.props.share(this.props.data, this.props.meta);
    };

    _view = () => {
        if (Moment.isMoment(this.props.data)) {
            this.props.onClick(this.props.data.format("YYYY-MM-DD"), this.props.meta);
        } else {
            this.props.onClick(this.props.data, this.props.meta);
        }
    };

    _external = () => {
        let reportURI = "/document/";

        let segments = {
            a: window.user.aid,
            t: this.props.template.uuid,
        };

        if (this.props.data) {
            if (this.props.data.format) {
                segments.d = this.props.data.format("YYYY-MM-DD");
            } else {
                segments.d = this.props.data;
            }
        }
        if (this.props.meta.uuid) segments.l = this.props.meta.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        reportURI = reportURI + window.btoa(segs.join(";"));
        window.open(reportURI);
    };

    render() {
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.template.instance_name),
            {
                location: this.props.meta,
                doc_date: this.props.data,
                interval: this.props.template.generation.interval
            }
        );

        return (
            <div className="block hoverable" onClick={this._view}>
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell style={{flexBasis: "auto", lineHeight: "24px"}}>
                            {instanceName}
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{flexBasis: "auto", flexGrow: 0}}>
                            <div className="btn-group">
                                <ewars.d.Button
                                    icon="fa-file-pdf"
                                    onClick={this._download}/>
                                <ewars.d.Button
                                    icon="fa-external-link"
                                    onClick={this._external}/>
                                <ewars.d.Button
                                    icon="fa-share"
                                    onClick={this._share}/>
                                <ewars.d.Button
                                    icon="fa-eye"
                                    onClick={this._view}/>
                            </div>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

class ListMultipleLocation extends React.Component {
    static defaultProps = {
        weeks: [],
        days: [],
        template: null,
        locations: null,
        limit: 60,
        filter: "ALL"
    }

    constructor(props) {
        super(props);

        this.state = {
            renderDay: null,
            renderLocationIndex: 0,
            locations: this.props.locations.sort((a, b) => {
                if (__(a.name) > __(b.name)) return 1;
                if (__(a.name) < __(b.name)) return -1;
                return 0;
            }),
            page: 0,
            offset: 0,
            limit: null,
            filter: null
        }

        let items = [];
        if (this.props.template.generation.interval == "WEEK") items = this.props.weeks;
        if (this.props.template.generation.interval == "DAY") items = this.props.days;


        let totalReports = this.state.locations.length * items.length;
        let chunks, remainder;
        if (totalReports <= 0) chunks = 0;
        if (totalReports <= this.props.limit) chunks = 1;
        if (totalReports > this.props.limit) {
            chunks = parseFloat(totalReports) / 60.0;
            remainder = parseFloat(totalReports) % 60.0;
        }

        this.state.totalReports = totalReports;

        let reports = [];
        items.forEach((itemDate) => {
            this.state.locations.forEach((location) => {
                reports.push([itemDate, location])
            })
        });
        this.state.documents = reports;

    }

    componentWillReceiveProps(nextProps) {
        let locations = this.props.locations;
        if (nextProps.filter != "ALL") {
            locations = this.props.locations.filter((item) => {
                return item.uuid == nextProps.filter;
            })
        }
        this.state.locations = locations;

        let items = [];
        if (this.props.template.generation.interval == "WEEK") items = this.props.weeks;
        if (this.props.template.generation.interval == "DAY") items = this.props.days;


        let totalReports = locations.length * items.length;
        let chunks, remainder;
        if (totalReports <= 0) chunks = 0;
        if (totalReports <= this.props.limit) chunks = 1;
        if (totalReports > this.props.limit) {
            chunks = parseFloat(totalReports) / 60.0;
            remainder = parseFloat(totalReports) % 60.0;
        }

        this.state.totalReports = totalReports;

        let reports = [];
        items.forEach((itemDate) => {
            locations.forEach((location) => {
                reports.push([itemDate, location])
            })
        });
        this.state.documents = reports;


    };

    componentDidMount() {
        this.state.height = this._el.getBoundingClientRect().height;
        this.state.limit = Math.round((this.state.height - 16) / (42 - 8));
        this.setState({
            height: this.state.height,
            limit: this.state.limit
        })
    };

    _start = () => {
        this.setState({
            offset: 0
        })
    };

    _end = () => {
        this.setState({
            offset: this.state.totalReports
        })
    };

    _next = () => {
        this.setState({
            offset: this.state.offset + this.state.limit
        })
    };

    _prev = () => {
        if (this.state.offset > 0) {
            this.setState({
                offset: this.state.offset - this.state.limit
            })
        }
    };

    render() {
        let items = [];
        if (this.state.limit != null) {
            items = this.state.documents.slice(this.state.offset, this.state.offset + this.state.limit);
        }

        let pageStart = this.state.offset > 0 ? this.state.offset + 1 : 1;
        let pageEnd = this.state.offset > 0 ? this.state.offset + this.state.limit + 1 : this.state.limit + 1;
        if (pageEnd > this.state.totalReports) pageEnd = this.state.totalReports;
        let pagination = `Showing ${pageStart} to ${pageEnd} of ${this.state.totalReports} documents`;
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div ref={(el) => {
                            this._el = el;
                        }} style={{height: "100%", width: "100%"}}>
                            <div className="block-tree" style={{position: "relative"}}>
                                {items.map((item) => {
                                    return <Week
                                        data={item[0]}
                                        mode={this.props.template.generation.interval}
                                        onClick={this.props.onClick}
                                        share={this.props.share}
                                        meta={item[1]}
                                        template={this.props.template}/>;
                                })}
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Toolbar label={pagination}>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            onClick={this._prev}
                            icon="fa-backward"/>
                        <ewars.d.Button
                            onClick={this._start}
                            icon="fa-fast-backward"/>
                        <ewars.d.Button
                            onClick={this._end}
                            icon="fa-fast-forward"/>
                        <ewars.d.Button
                            onClick={this._next}
                            icon="fa-forward"/>

                    </div>
                </ewars.d.Toolbar>
            </ewars.d.Layout>
        )

    }
}

class ReportFlatList extends React.Component {
    static defaultProps = {
        weeks: [],
        days: [],
        template: null,
        locations: null
    };

    render() {
        let items = [];
        if (this.props.template.generation.interval == "WEEK") items = this.props.weeks;
        if (this.props.template.generation.interval == "DAY") items = this.props.days;
        if (this.props.template.generation.interval == "MONTH") items = this.props.months.reverse();
        if (this.props.template.generation.interval == "YEAR") items = this.props.years;


        return (
            <Panel>
                <div className="block-tree" style={{position: "relative"}}>
                    {items.map((date) => {
                        return <Week
                            data={date}
                            mode={this.props.template.generation.interval}
                            onClick={this.props.onClick}
                            share={this.props.share}
                            meta={this.props.meta}
                            template={this.props.template}/>;
                    })}
                </div>
            </Panel>
        )
    }
}


class Generated extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            years: [],
            year: null,
            days: [],
            day: null,
            weeks: [],
            meta: null,
            view: "DEFAULT",
            shareEmails: "",
            filter: "ALL"
        }

        ewars.subscribe("DATA_AVAILABLE", this._dataAvailable);

    }

    _init = (props) => {
        let startDate = Moment.utc(props.data.generation.start_date);
        let endDate = Moment.utc(props.data.generation.end_date || undefined);
        let today = Moment.utc();

        let startYear = startDate.year(),
            endYear = endDate.year(),
            startMonth = startDate.month(),
            endMonth = endDate.month(),
            month = null,
            years = [],
            months = [],
            days = [],
            weeks = [];

        let year = today.year() > endYear ? endYear : today.year();
        months = getMonths(year, startDate, endDate);

        if (months[months.length - 1] > today.month()) {
            month = today.month();
        } else if (months[months.length - 1] <= today.month()) {
            month = months[months.length - 1];
        }

        if (props.data.generation.interval === "WEEK") {
            weeks = getWeeks(year, endDate);
        }

        if (props.data.generation.interval == "DAY") {
            let mEndDate = endDate.clone();
            if (Moment().isBefore(mEndDate, 'd')) mEndDate = Moment();
            months = getMonthsDays(year, startDate, mEndDate.add(30,'d'));
            month = parseInt(months[0].split('-')[1]);
            days = getDays(year, month, startDate, endDate);
        }

        // get months
        years = range(startYear, year + 1, 1);

        this.state.years = years;
        this.state.year = year;
        this.state.days = days;
        this.state.months = months.reverse();
        this.state.month = parseInt(month);
        this.state.view = "DEFAULT";
        this.state.shareEmails = "";
        this.state.startDate = startDate;
        this.state.endDate = endDate;
        this.state.weeks = weeks;

        if (props.data.generation.location_spec === "SPECIFIC") {
            this._getLocation(props.data);
        } else if (props.data.generation.location_spec == "TYPE") {
            this._getLocations(props.data);
        }
    };


    componentWillMount() {
        this._init(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this._init(nextProps);
    }

    componentWillUnmount() {
        ewars.unsubscribe("DATA_AVAILABLE", this._dataAvailable);
    }

    _dataAvailable = (data) => {
        this.setState({
            reportData: data,
            canDownload: true
        })
    };

    _downloadData = () => {
        // Get the document name
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.data.instance_name),
            {
                location: this.props.meta || this.state.report.location,
                doc_date: this.state.report.doc_date,
                interval: this.props.data.generation.interval
            }
        );

        let bl = new ewars.Blocker(null, "Generating excel...");

        ewars.tx("com.ewars.document.data", [instanceName, this.state.reportData[0], this.state.reportData[1]])
            .then((resp) => {
                bl.destroy();
                document.location.href = 'http://' + ewars.domain + '/document/download/' + resp.n;
            })

    };

    _getLocations = (data) => {
        console.log("HERE", data)
        let bl = new ewars.Blocker(null, "Loading locations...");
        ewars.tx("com.ewars.query", [
            "location",
            ['uuid', 'name', 'status'],
            {
                uuid: {under: data.generation.location},
                site_type_id: {eq: data.generation.location_type},
                status: {eq: "ACTIVE"}
            },
            null,
            null,
            null,
            null])
            .then((resp) => {
                bl.destroy();
                this.setState({
                    locations: resp,
                    meta: null
                })
            })
    };


    _getLocation = (data) => {
        let bl = new ewars.Blocker(null, "Loading document data...");

        let location_id = data.generation.location || data.generation.location_uuid;
        ewars.tx("com.ewars.resource", ["location", location_id, ["uuid", "name"], null])
            .then((resp) => {
                bl.destroy();
                this.setState({
                    meta: resp,
                    locations: null
                });
            });
    };

    _changeYear = (prop, value) => {
        let startDate = Moment.utc(this.props.data.generation.start_date);
        let endDate = Moment.utc(this.props.data.generation.end_date || undefined);
        let weeks = [],
            months = [],
            month = this.state.month,
            days = [];

        months = getMonths(value, this.state.startDate, this.state.endDate);
        if (this.props.data.generation.interval == "WEEK") {
            weeks = getWeeks(value, this.state.endDate);
        }

        if (this.props.data.generation.interval === "DAY") {
            let startDate = Moment.utc(this.props.data.generation.start_date);
            let endDate = Moment.utc(this.props.data.generation.end_date || undefined);
            months = getMonthsDays(value, startDate, endDate);
            month = parseInt(months[0].split("-")[1]);
            days = getDays(value, month, startDate, endDate);
        }

        if (this.props.data.generation.interval == "MONTH") {
            months = getMonths(value, this.state.startDate, this.state.endDate);
        }



        this.setState({
            year: value,
            weeks: weeks,
            months: months.reverse(),
            month: month,
            days: days
        });


    };

    _changeMonth = (prop, value) => {
        let startDate = Moment.utc(this.props.data.generation.start_date);
        let endDate = Moment.utc(this.props.data.generation.end_date || undefined);
        let days = getDays(this.state.year, value, startDate, endDate);
        this.setState({
            month: value,
            days: days
        })
    };

    _share = (report, location) => {
        if (report) {
            let doc_date = report;
            if (Moment.isMoment(doc_date)) {
                doc_date = doc_date.format("YYYY-MM-DD");
            }
            this.setState({
                showShade: true,
                report: {
                    doc_date: doc_date,
                    location: location
                }
            });
        } else {
            this.setState({
                showShade: true
            })
        }
    };

    _onEmailsChange = (e) => {
        this.setState({
            shareEmails: e.target.value
        })
    };

    _showReport = (doc_date, location) => {
        this.setState({
            view: "REPORT",
            canDownload: false,
            reportData: null,
            report: {
                doc_date: doc_date,
                location: this.state.meta || location
            }
        })
    };

    _sendShare = () => {
        let bl = new ewars.Blocker(null, "Sharing document...");

        let uri = "/document/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        segments.d = ewars.DATE(this.state.report.doc_date, "DAY");
        segments.l = this.state.meta ? this.state.meta.uuid : this.state.report.location.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        // Get the document name
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.data.instance_name),
            {
                location: this.props.meta || this.state.report.location,
                doc_date: this.state.report.doc_date ? this.state.report.doc_date : this.state.report,
                interval: this.props.data.generation.interval
            }
        );

        ewars.tx('com.ewars.document.share', [uri, this.state.shareEmails, instanceName])
            .then((resp) => {
                bl.destroy();
                this.setState({
                    showShare: false,
                    shareEmails: ""
                });
                ewars.growl("Document shared");

            })
    };

    _clear = () => {
        this.setState({
            view: "DEFAULT",
            report: {}
        })
    };

    _download = (report) => {
        let bl = new ewars.Blocker(null, "Generating pdf...");

        let uri = "/document/pdf/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        if (report) segments.d = ewars.DATE(report.doc_date, "DAY");
        segments.l = report.location.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        let r = new XMLHttpRequest();
        r.open("GET", uri);
        r.onreadystatechange = () => {
            if (r.readyState == 4 && r.status == 200) {
                let resp = JSON.parse(r.responseText);
                bl.destroy();
                document.location.href = 'http://' + ewars.domain + '/document/download/' + resp.name;
            }
        };

        r.send();
    };

    _onModalAction = (action) => {
        this.setState({
            showShade: false
        })
    };

    _onFilterChange = (prop, value) => {
        this.setState({
            filter: value
        })
    };

    render() {
        if (this.props.data.generation.location_spec == "SPECIFIC") {
            if (!this.state.meta) {
                return <Spinner/>;
            }
        } else if (!this.state.locations) {
            return <Spinner/>
        }

        let view;

        if (this.state.view == "DEFAULT" && this.props.data.generation.location_spec != "TYPE") {
            view = (
                <ReportFlatList
                    locations={this.state.locations}
                    data={this.state.year}
                    weeks={this.state.weeks}
                    months={this.state.months}
                    days={this.state.days}
                    meta={this.state.meta}
                    share={this._share}
                    onClick={this._showReport}
                    template={this.props.data}/>
            );
        }

        if (this.state.view == "DEFAULT" && this.props.data.generation.location_spec == "TYPE") {
            view = (
                <ListMultipleLocation
                    filter={this.state.filter}
                    locations={this.state.locations}
                    data={this.state.year}
                    months={this.state.months}
                    weeks={this.state.weeks}
                    days={this.state.days}
                    meta={this.state.meta}
                    share={this._share}
                    onClick={this._showReport}
                    template={this.props.data}/>
            )
        }

        if (this.state.view == "REPORT") {
            view = (
                <Report
                    data={{
                        doc_date: this.state.report.doc_date,
                        location: this.state.meta || this.state.report.location,
                        uuid: this.props.data.uuid,
                        template_type: this.props.data.template_type
                    }}/>
            )
        }

        let toolbar;
        if (this.state.view == "DEFAULT") {

            let locations = [
                ["ALL", "All Locations"]
            ];
            if (this.props.data.generation.location_spec == "TYPE") {
                let options = this.state.locations.map((item) => {
                    return [item.uuid, __(item.name)];
                })
                options = options.sort((a, b) => {
                    if (a[1] > b[1]) return 1;
                    if (a[1] < b[1]) return -1;
                    return 0;
                })
                locations.push.apply(locations, options);
            }

            toolbar = (
                <ewars.d.Toolbar>
                    {this.props.data.generation.location_spec == "TYPE" ?
                        <div style={{width: "200px", float: "left", marginRight: "8px", marginTop: "-2px"}}>
                            <SelectField
                                onChange={this._onFilterChange}
                                value={this.state.filter}
                                searchProp="n"
                                data={{
                                    n: "location_id",
                                    o: locations
                                }}/>
                        </div>
                        : null}
                    <ButtonGroup
                        name="year"
                        value={this.state.year}
                        onUpdate={this._changeYear}
                        config={{
                            options: this.state.years.map((item) => {
                                return [item, String(item)]
                            })
                        }}/>

                    {this.props.data.generation.interval == "DAY" ?
                        <ButtonGroup
                            name="month"
                            value={this.state.month}
                            onUpdate={this._changeMonth}
                            config={{
                                options: this.state.months.map((item) => {
                                    let val = parseInt(item.split("-")[1]);
                                    return [val, MONTHS[val]]
                                })
                            }}/>
                        : null}

                </ewars.d.Toolbar>
            )
        } else {
            toolbar = (
                <ewars.d.Toolbar>
                    <div className="btn-group">
                        <ewars.d.Button
                            icon="fa-caret-left"
                            label="Back"
                            onClick={this._clear}/>
                    </div>

                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-file-pdf"
                            onClick={() => {
                                this._download(this.state.report)
                            }}/>
                        <ewars.d.Button
                            icon="fa-share"
                            onClick={this._share}/>
                        {this.state.canDownload ?
                            <ewars.d.Button
                                icon="fa-download"
                                onClick={this._downloadData}/>
                            : null}
                    </div>
                </ewars.d.Toolbar>
            )
        }

        return (
            <ewars.d.Layout>
                {toolbar}
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    title="Share Document"
                    onAction={this._onModalAction}
                    shown={this.state.showShade}>
                    <div className="article" style={{padding: 16}}>
                        <p>Enter an email address (or multiple email addresses separated by commas (,) to share this
                            document.</p>

                        <label>Email Addresses</label>
                        <input type="text" value={this.state.shareEmails} onChange={this._onEmailsChange}/>
                        <div className="clearer" style={{height: 20}}></div>

                        <ewars.d.Button
                            label="Share"
                            icon="fa-share"
                            onClick={this._sendShare}/>
                    </div>
                </ewars.d.Shade>
            </ewars.d.Layout>

        )
    }
}

export default Generated;
