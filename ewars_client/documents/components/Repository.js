import moment from "moment";

import ReportUtils from "../utils/ReportUtils";

const styles = {
    HEADER: {
        display: "block",
        paddingLeft: 18,
        paddingRight: 18,
        paddingTop: 25,
        paddingBottom: 8,
        borderBottom: "1px solid #CCCCCC"
    },
    HEADER_TEXT: {
        fontSize: "12px"
    },
    TITLE: {
        display: "block",
        paddingLeft: 18,
        paddingRight: 18,
        paddingTop: 25,
        paddingBottom: 8
    },
    TITLE_TEXT: {
        fontSize: "16px"
    }
};

function _getYears(startDate, endDate) {
    let startYear = parseInt(startDate.split("-")[0]);
    let endYear = new Date().getUTCFullYear();
    if (endDate) {
        endYear = parseInt(endDate.split("-")[0]);
    }

    let years = [];
    let curYear = startYear;
    while (curYear <= endYear) {
        years.push(curYear++);
    }

    return years;
}

function _getWeeks(year, endDate) {
    let end = endDate ? moment.utc(endDate) : moment.utc();
    let weeks = moment.utc().year(year).isoWeeksInYear();

    let curWeek = moment.utc().year(year).isoWeek(1).day(7);

    let end_weeks = [curWeek.toDate()];
    let isoW = 1;

    while (curWeek.isBefore(end, "d") || isoW < weeks) {
        curWeek = curWeek.clone().add(7, 'd');
        isoW++;
        end_weeks.push(curWeek);
    }

    return end_weeks;
}

class Header extends React.Component {
    render() {
        return (
            <div style={styles.HEADER}>
                <h3 style={styles.HEADER_TEXT}>{this.props.title}</h3>
            </div>
        )
    }
}

class Title extends React.Component {
    render() {
        return (
            <div style={styles.TITLE}>
                <h1 style={styles.TITLE_TEXT}>{this.props.title}</h1>
            </div>
        )
    }
}

class Folder extends React.Component {
    _expand = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <div className="block">
                <div className="block-content" onClick={this._expand}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={20}><i className="fal fa-folder"></i></ewars.d.Cell>
                        <ewars.d.Cell>{this.props.data}</ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

class Document extends React.Component {
    render() {
        let instanceName = ReportUtils.applyFormattingTags(
            ewars.I18N(this.props.template.instance_name),
            {
                ...this.props.template,
                doc_date: this.props.data
            });

        if (this.props.template.status == "DRAFT") instanceName = "DRAFT - " + instanceName;
        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell>{instanceName}</ewars.d.Cell>
                        <ewars.d.Cell width={40}>
                            <ewars.d.Button
                                icon="fa-file-pdf"/>
                        </ewars.d.Cell>
                        <ewars.d.Cell width={40}>
                            <ewars.d.Button
                                icon="fa-eye"/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}

class Repository extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shade: false,
            report: null,
            root: true,
            view: "YEARS",
            views: {},
            context: null
        }
    }

    componentWillMount() {
        this._setup(this.props.data);
    }

    componentWillReceiveProps(nextProps) {
        this._setup(nextProps.data);
    }

    _setup = (config) => {
        let stack = [],
            views = {},
            view = "YEARS";

        if (config.template_type == "GENERATED") {
            views['YEARS'] = this._getYears;
            stack = ['YEARS'];

            if (config.generation.location_spec == "SPECIFIC") {
                views["REPORTS"] = this._getReportsForYear;
                stack.push("REPORTS")
            }

            if (config.generation.location_spec == "LOCATION_TYPE") {
                views["LOCATIONS"] = this._getLocationsForSpec;
                views["REPORTS"] = this._getReportsForLocationYear;

                stack.push.apply(stack, [
                    "LOCATIONS",
                    "REPORTS"
                ])
            }

        } else if (config.template_type == "PER_REPORT") {
            views['YEARS'] = _getYears(config.generation.start_date, config.generation.end_date);
            views['LOCATIONS'] = this._getLocationsForSpec;
            views['REPORTS'] = this._getReportsForLocationYear;
            stack = ["YEARS", "LOCATIONS", "REPORTS"];
        } else if (config.template_type == "ONE_OFF") {
            stack = null;
            view = "REPORT";
        }

        this.state.stack = stack;
        this.state.views = views;
        this.state.view = view;
    };

    _shiftStack = (data) => {
        let curIndex = this.state.stack.indexOf(this.state.view);
        curIndex++;

        this.setState({
            view: this.state.stack[curIndex],
            context: data
        })
    };

    _getYears = () => {
        let years = _getYears(
            this.props.data.generation.start_date,
            this.props.data.generation.end_date
        ).reverse();

        return years.map((item) => {
            return <Folder data={item} onClick={this._shiftStack}/>
        })
    };


    _getReportsForYear = (year) => {
        let years = _getWeeks(
            this.state.context,
            this.props.data.generation.end_date
        ).reverse();

        return years.map((item) => {
            return <Document
                template={this.props.data}
                data={item}
                onClick={this._shiftStack}/>
        })
    };

    _backStack = () => {
        let curIndex = this.state.stack.indexOf(this.state.view);
        curIndex--;

        this.setState({
            view: this.state.stack[curIndex]
        })
    };

    _showMonthsForYear = (month) => {

    };

    render() {

        let items = this.state.views[this.state.view](this.state.context);

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <Title title={ewars.I18N(this.props.data.template_name)}/>

                            <Header title="MOST RECENT"/>

                            <Header title="BROWSE"/>
                            <div className="block-tree" style={{position: "relative", maxHeight: 250}}>

                                {this.state.view != "YEARS" ?
                                    <Folder data={"..."} onClick={this._backStack}/>
                                    : null}

                                {items}

                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    toolbar={false}
                    shown={false}>

                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default Repository;
