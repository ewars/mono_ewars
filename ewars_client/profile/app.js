import Component from "./components/Component.react";

import {
    Button,
    Layout,
    Row,
    Cell,
    Panel,
    Toolbar
} from "../common";

ewars.d = {
    Button,
    Layout,
    Row,
    Cell,
    Panel,
    Toolbar
}

ReactDOM.render(
    <Component/>,
    document.getElementById("application")
);
