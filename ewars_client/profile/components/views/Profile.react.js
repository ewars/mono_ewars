import {
    TextField,
    SelectField,
    TextAreaField
} from "../../../common/fields";


const ORG_OPTIONS = {
    optionsSource: {
        resource: "organization",
        query: {
            status: {eq: "ACTIVE"}
        },
        valSource: "uuid",
        labelSource: "name"
    }
};

class DirtyControls extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.dirty[this.props.name]) return null;

        return (
            <div className="ide-col" style={{maxWidth: 75}}>
                <div className="btn-group pull-right">
                    <ewars.d.Button
                        icon="fa-save"
                        onClick={() => {
                            this.props.onSaveEdit(this.props.name)
                        }}/>
                    <ewars.d.Button
                        icon="fa-times"
                        onClick={() => {
                            this.props.onCancelEdit(this.props.name)
                        }}/>

                </div>
            </div>
        )
    }
}

class Profile extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        if (!this.props.user.profile) this.props.user.profile = {};

        let isSystem = this.props.user.system;

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="article" style={{paddingRight: 150, paddingLeft: 30, paddingTop: 30}}>
                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">

                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>Your Details</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{
                                             maxWidth: 150,
                                             fontWeight: "bold",
                                             textAlign: "right",
                                             paddingRight: 10,
                                             paddingTop: 8
                                         }}>
                                        Name
                                    </div>
                                    <div className="ide-col">
                                        <TextField
                                            name="name"
                                            onUpdate={this.props.onChange}
                                            value={this.props.user.name}/>
                                    </div>
                                    <DirtyControls
                                        name="name"
                                        dirty={this.props.dirty}
                                        onCancelEdit={this.props.onCancelEdit}
                                        onSaveEdit={this.props.onSaveEdit}/>

                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{
                                             maxWidth: 150,
                                             fontWeight: "bold",
                                             textAlign: "right",
                                             paddingRight: 10,
                                             paddingTop: 8
                                         }}>
                                        Email
                                    </div>
                                    <div className="ide-col">
                                        <TextField
                                            name="email"
                                            readOnly={isSystem}
                                            onUpdate={this.props.onChange}
                                            value={this.props.user.email}/>
                                    </div>
                                    <DirtyControls
                                        name="email"
                                        dirty={this.props.dirty}
                                        onCancelEdit={this.props.onCancelEdit}
                                        onSaveEdit={this.props.onSaveEdit}/>

                                </div>
                                {isSystem ?
                                    <ewars.d.Row style={{marginBottom: 8}}>
                                        <ewars.d.Cell style={{maxWidth: 150, paddingRight: 10, paddingTop: 8}}>
                                        </ewars.d.Cell>
                                        <ewars.d.Cell>
                                            <div className="warning">You are logged in as a system user, system users
                                                email address can not be modified.
                                            </div>

                                        </ewars.d.Cell>
                                    </ewars.d.Row>
                                    : null}

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{
                                             maxWidth: 150,
                                             fontWeight: "bold",
                                             textAlign: "right",
                                             paddingRight: 10,
                                             paddingTop: 8
                                         }}>
                                        Organization
                                    </div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="org_id"
                                            config={ORG_OPTIONS}
                                            onUpdate={this.props.onChange}
                                            value={this.props.user.org_id}/>
                                    </div>
                                    <DirtyControls
                                        name="org_id"
                                        dirty={this.props.dirty}
                                        onCancelEdit={this.props.onCancelEdit}
                                        onSaveEdit={this.props.onSaveEdit}/>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>About You</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{
                                             maxWidth: 150,
                                             fontWeight: "bold",
                                             textAlign: "right",
                                             paddingRight: 10,
                                             paddingTop: 8
                                         }}>
                                        Phone
                                    </div>
                                    <div className="ide-col">
                                        <TextField
                                            name="phone"
                                            onUpdate={this.props.onChange}
                                            value={this.props.user.phone}/>
                                    </div>
                                    <DirtyControls
                                        name="phone"
                                        dirty={this.props.dirty}
                                        onCancelEdit={this.props.onCancelEdit}
                                        onSaveEdit={this.props.onSaveEdit}/>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{
                                             maxWidth: 150,
                                             fontWeight: "bold",
                                             textAlign: "right",
                                             paddingRight: 10,
                                             paddingTop: 8
                                         }}>
                                        Occupation
                                    </div>
                                    <div className="ide-col">
                                        <TextField
                                            name="profile.occupation"
                                            onUpdate={this.props.onChange}
                                            value={this.props.user.profile.occupation || ""}/>
                                    </div>
                                    <DirtyControls
                                        name="profile.occupation"
                                        dirty={this.props.dirty}
                                        onCancelEdit={this.props.onCancelEdit}
                                        onSaveEdit={this.props.onSaveEdit}/>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{
                                             maxWidth: 150,
                                             fontWeight: "bold",
                                             textAlign: "right",
                                             paddingRight: 10,
                                             paddingTop: 8
                                         }}>
                                        Bio
                                    </div>
                                    <div className="ide-col">
                                        <TextAreaField
                                            name="profile.bio"
                                            config={{i18n: false}}
                                            onUpdate={this.props.onChange}
                                            value={this.props.user.profile.bio || ""}/>
                                    </div>
                                    <DirtyControls
                                        name="profile.bio"
                                        dirty={this.props.dirty}
                                        onCancelEdit={this.props.onCancelEdit}
                                        onSaveEdit={this.props.onSaveEdit}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Profile;
