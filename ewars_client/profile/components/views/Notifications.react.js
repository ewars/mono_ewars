import { SelectField } from "../../../common/fields";

const NOTIFICATION_SETTINGS = [
    {
        name: "Alerts",
        items: [
            ["ALERT_TRIGGERED", "An alert is triggered", "IGNORE"],
            ["ALERT_CLOSED", "An alert is closed", "IGNORE"],
            ["ALERT_REOPENED", "An alert is re-opened", "IGNORE"],
            ["ALERT_COMMENT", "Someone comments on an alert", "IGNORE"],
            ["ALERT_AUTOCLOSED", "An alert is automatically closed due to expiration", "IGNORE"],
            ["ALERT_OTHER", "Other alert activity not listed above occurs.", "IGNORE"]
        ]
    },
    {
        name: "Form Submissions",
        items: [
            ["SUCCESS_SUBMIT", "A new form is submitted", "IGNORE"],
            ["AMENDMENT_REJECTED", "Your amendment is rejected", "IGNORE"],
            ["AMENDMENT_APPROVED", "Your amendment is approved", "IGNORE"],
            ["RETRACT_APPROVED", "Your submission retraction request is approved", "IGNORE"],
            ["RETRACT_REJECTED", "Your submission retraction request is rejected", "IGNORE"],
            ["OVERDUE_REPORT", "You have a report overdue", "IGNORE"],
            ["REPORT_COMMENT", "Someone comments on a report", "IGNORE"],
            ["SUBMISSION_OTHER", "Other form submission activity not listed above occurs", "IGNORE"]
        ]
    },
    {
        name: "Scheduled",
        items: [
            ["WEEKLY_DIGEST", "Weekly Digest", "IGNORE"],
            ["DOCUMENT_AVAILABLE", "New Documents Digest", "IGNORE"],
            ["OTHER_DIGEST", "Other digest activity not listed above occurs", "IGNORE"]
        ]
    },
    {
        name: "Forms",
        items: [
            ["FORM_REVISION", "A revision is created", "IGNORE"],
            ["FORM_NEW", "A new form becomes available", "IGNORE"],
            ["FORM_OTHER", "Other form activity not listed above occurs", "IGNORE"]
        ]
    },
    {
        name: "Indicators",
        items: [
            ["NEW_INDICATOR", "A new indicator becomes available", "IGNORE"],
            ["INDICATOR_OTHER", "Other indicator activity not listed above occurs", "IGNORE"]
        ]
    },
    {
        name: "Tasks",
        items: [
            ["NEW_TASK", "A new task becomes available", "IGNORE"],
            ["TASK_STATUS_CHANGE", "A tasks status changes", "IGNORE"],
            ["TASK_OWNER_CHANGE", "A tasks owner changes", "IGNORE"],
            ["TASK_COMMENT", "Someone comments on a task", "IGNORE"],
            ["OTHER_TASK", "Other task activity not listed above occurs", "IGNORE"]
        ]
    },
    {
        name: "Locations",
        items: [
            ["NEW_LOCATION", "A new location becomes available", "IGNORE"],
            ["NEW_LOCATION_REPORTING", "A new reporting period for a location opens", "IGNORE"],
            ["CLOSED_LOC_REPORTING", "A locations reporting period closes", "IGNORE"],
            ["LOC_DELETED", "A location is deleted", "IGNORE"],
            ["OTHER_LOCATION", "Other location activity not listed above occurs", "IGNORE"]

        ]
    }
];

const options = [
    ["IGNORE", "Ignore"],
    ["NOTIFY", "Notify"],
    ["EMAIL", "Email"]
];

const emailOptions = [
    ["DEFAULT", "Default (Send HTML Email)"],
    ["HTML", "Send HTML Email"],
    ["PLAINTEXT", "Send plain text email"]
];

const EMAIL_OPTIONS = [
    ["DEFAULT", "Default (Enable Email Notifications)"],
    ["ENABLED", "Enable Email Notifications"],
    ["DISABLED", "Disable Email Notifications"]
];

const DESKTOP_OPTIONS = [
    ["DISABLED", "Send Application Notifications Only"],
    ["ENABLED", "Send Desktop Notifications Too"]
];

class NotificationSettting extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-row" style={{marginBottom: 8}}>
                <div className="ide-col" style={{maxWidth: 200}}>
                    <SelectField
                        name={this.props.data[0]}
                        value={this.props.user.notifications[this.props.data[0]]}
                        config={{options: options}}
                        onUpdate={(prop, value) => this.props.onChange(this.props.data[0], value)}/>

                </div>
                <div className="ide-col" style={{paddingLeft: 5, paddingTop: 8}}>
                    {this.props.data[1]}
                </div>
            </div>
        )
    }
}

class NotificationSettingSection extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let settings = this.props.data.items.map(function (setting) {
            return <NotificationSettting data={setting} user={this.props.user} onChange={this.props.onChange}/>
        }.bind(this));

        return (
            <div className="ide-row" style={{marginBottom: 8}}>
                <div className="ide-col"
                     style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>{this.props.data.name}</div>
                <div className="ide-col">
                    <div className="ide-layout">{settings}</div>
                </div>
            </div>
        )
    }
}


class Notifications extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: ewars.copy(this.props.user || {})
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state.user = ewars.copy(this.props.user || {})
    }

    onNotChange = (prop, value) => {

        let bl = new ewars.Blocker(null, "Updating settings...");
        ewars.tx("com.ewars.user.patch", [`notifications.${prop}`, value])
            .then(function (resp) {
                bl.destroy();

                this.props.onChange(prop, value);
                ewars.growl("Setting updated");
            }.bind(this))

    };

    onRootChange = (prop, value) => {

        let bl = new ewars.Blocker(null, "Updating settings...");
        ewars.tx("com.ewars.user.patch", [prop, value])
            .then(function (resp) {
                bl.destroy();
                this.props.onChange(prop, value);
                ewars.growl("Setting updated");
            }.bind(this))
    };

    render() {

        let sections = NOTIFICATION_SETTINGS.map(function (notSet) {
            return <NotificationSettingSection data={notSet} user={this.state.user} onChange={this.onNotChange}/>
        }.bind(this));

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="article" style={{paddingRight: 150, paddingLeft: 30, paddingTop: 30}}>
                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>You can adjust <strong>Application Settings</strong> here to customize when
                                            you are
                                            emailed and notified.</p>

                                        <table width="100%" className="article-table">
                                            <thead>
                                            <tr>
                                                <th>Setting</th>
                                                <th>Effect</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Email</td>
                                                <td>You will receive an email and a notification, but the notification
                                                    will be
                                                    marked "read".
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Notify</td>
                                                <td>You will receive an unread notification only.</td>
                                            </tr>
                                            <tr>
                                                <td>Ignore</td>
                                                <td>You will receive nothing</td>
                                            </tr>
                                            </tbody>

                                        </table>

                                        <p>If an update makes several changes (like adding CCs to a task, closing it,
                                            and adding
                                            a
                                            comment) you will receive the strongest notification any of the changes is
                                            configured to
                                            deliver.</p>
                                        <p>These preferences <strong>only</strong> apply to objects you are connected to
                                            (for
                                            example, amendments where you are the submitter or tasks you have access
                                            to). </p>
                                    </div>
                                </div>


                                {sections}


                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>You can opt to receive plain text format email from EWARS instead of HTML
                                            email. Plain text email requires less data to download and may work better
                                            on some email clients/applications.</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>
                                        Email Format
                                    </div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="email_format"
                                            value={this.props.user.email_format}
                                            config={{options: emailOptions}}
                                            onUpdate={this.onRootChange}/>
                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>Should EWARS send desktop notifications? These are sent in addition to
                                            notifications within the EWARS application.</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>
                                        Desktop Notifications
                                    </div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="desk_not_status"
                                            value={this.props.user.desk_not_status}
                                            config={{options: DESKTOP_OPTIONS}}
                                            onUpdate={this.onRootChange}/>

                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>If you disable <strong>Email Notifications</strong>, EWARS will never send
                                            email to notify you about events. This preference overrides all your other
                                            settings.</p>
                                        <p><i>You will still receive some administrative email, like password reset
                                            emails.</i></p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>
                                        Email Notifications
                                    </div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="email_status"
                                            value={this.props.user.email_status}
                                            config={{options: EMAIL_OPTIONS}}
                                            onUpdate={this.onRootChange}/>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Notifications;
