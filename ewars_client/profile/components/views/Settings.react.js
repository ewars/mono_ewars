import { SelectField } from "../../../common/fields";
import TIMEZONES from "../../../common/constants/timezones";

const DATE_FORMAT = [
    ["DEFAULT", "Default (ISO 8601: 200-02-28)"],
    ["ISO8601", "ISO 8601: 2000-02-28"],
    ["US", "2/28/2000"],
    ["EU", "Europe: 28-02-2000"]
];

const TIME_FORMAT = [
    ["DEFAULT", "Default (12 Hour, 2:34 PM)"],
    ["12", "12 Hour, 2:34 PM"],
    ["24", "24 Hour, 14:34"]
];

const ACCESSIBILITY = [
    ["DEFAULT", "Default (Use Standard Colors)"],
    ["STANDARD", "Use Standard Colors"],
    ["HIGH", "Use High Contrast Colors"],
    ["LARGE", "User Larger Font Size"]
];

const TRANSLATION = [
    ["en_US", "English (US)"],
    ["fr_FR", "French (France)"],
    ["en_A*", "ENGLISH (US, ALL CAPS)"],
    ["en_R*", "English (Raw Strings)"]
];

const PRONOUN = [
    ["AMBI", window.user.name + " updated their profile"],
    ["MALE", window.user.name + " updated his profile"],
    ["FEMALE", window.user.name + " updated her profile"]
];

let timezoneOptions = [];

for (var i in TIMEZONES) {
    TIMEZONES[i].forEach(location => {
        timezoneOptions.push([
            location + " " + i,
            location + " [" + i + "]"
        ])
    })
}

class NotificationSettting extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-row" style={{marginBottom: 8}}>
                <div className="ide-col" style={{maxWidth: 200}}>
                    <SelectField
                        name={this.props.data[0]}
                        value={null}
                        config={{options: options}}
                        onUpdate={(prop, value) => this.props.onChange(this.props.data[0], value)}/>

                </div>
                <div className="ide-col" style={{paddingLeft: 5, paddingTop: 8}}>
                    {this.props.data[1]}
                </div>
            </div>
        )
    }
}

class NotificationSettingSection extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let settings = this.props.data.items.map(function (setting) {
            return <NotificationSettting data={setting}/>
        }.bind(this));

        return (
            <div className="ide-row" style={{marginBottom: 8}}>
                <div className="ide-col"
                     style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>{this.props.data.name}</div>
                <div className="ide-col">
                    <div className="ide-layout">{settings}</div>
                </div>
            </div>
        )
    }
}


class Settings extends React.Component {
    constructor(props) {
        super(props)

    }

    _onChange = (prop, value) => {
        let bl = new ewars.Blocker(null, "Updating settings...");

        ewars.tx("com.ewars.user", ["PATCH", window.user.id, {prop: prop, value:value}])
            .then(function (resp) {
                bl.destroy();

                this.props.onChange(prop, value);
                ewars.growl("Setting updated")
            }.bind(this))
    };

    render() {

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="article" style={{paddingRight: 150, paddingLeft: 30, paddingTop: 30}}>
                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">

                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>Select your local timezone.</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col"
                                         style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>
                                        Timezone
                                    </div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="timezone"
                                            value={this.props.user.timezone}
                                            config={{options: timezoneOptions}}
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>Select the format you prefer for dates.</p>
                                        <p><i>Note: Some dates will still be formatted according to the data they are
                                            associated to.</i></p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col" style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>Date Format</div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="date_format"
                                            value={this.props.user.date_format}
                                            config={{options: DATE_FORMAT}}
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>Select the format you prefer for displaying time.</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col" style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>Time Format</div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="time_format"
                                            value={this.props.user.time_format}
                                            config={{options: TIME_FORMAT}}
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>Choose which language you would like the EWARS UI to use.</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col" style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>Translation</div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="language"
                                            value={this.props.user.language}
                                            config={{options: TRANSLATION, groups: true}}
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>Choose the pronoun you prefer.</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col" style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>Pronoun</div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="pronoun"
                                            value={this.props.user.pronoun}
                                            config={{options: PRONOUN}}
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}></div>
                                    <div className="ide-col">
                                        <p>If you have difficulty reading the EWARS UI, this setting may make EWARS more accessible.</p>
                                    </div>
                                </div>

                                <div className="ide-row" style={{marginBottom: 8}}>
                                    <div className="ide-col" style={{maxWidth: 150, fontWeight: "bold", textAlign: "right", paddingRight: 10, paddingTop: 8}}>Accessibility</div>
                                    <div className="ide-col">
                                        <SelectField
                                            name="accessibility"
                                            value={this.props.user.accessibility}
                                            config={{options: ACCESSIBILITY}}
                                            onUpdate={this._onChange}/>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Settings;
