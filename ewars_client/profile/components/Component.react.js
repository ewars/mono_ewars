import { Tab, Spinner, Modal } from "../../common";

import Assignments from "./views/Assignments.react";
import Notifications from "./views/Notifications.react";
import Settings from "./views/Settings.react";
import Profile from "./views/Profile.react";
import PasswordChanger from "./components/PasswordChanger";

class Component extends React.Component {
    isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT",
            showPassword: false,

            password: "",
            confirm_password: "",
            passError: null,
            dirty: {}
        }
    }

    componentWillMount() {
        ewars.tx('com.ewars.user.profile', [window.user.id])
            .then(function (resp) {
                this.isLoaded = true;
                this.setState({
                    user: resp
                })
            }.bind(this))
    }

    _onChangeView = (view) => {
        this.setState({
            view: view.view
        })
    };

    _onChange = (prop, value) => {
        if (prop.indexOf(".") >= 0) {
            this.setState({
                user: {
                    [prop.split(".")[0]]: {
                        ...this.state.user[prop.split(".")[0]],
                        [prop.split(".")[1]]: value
                    }
                }
            })
        } else {
            this.setState({
                user: {
                    ...this.state.user,
                    [prop]: value
                }
            })
        }
    };

    _showPasswordChanger = () => {
        this.setState({
            showPassword: true
        })
    };

    _submitPasswordChange = () => {
        if (!this.state.password || this.state.password == "") {
            this.setState({
                passError: "Please provide a new password"
            });
            return;
        }

        if (!this.state.confirm_password || this.state.confirm_password == "") {
            this.setState({
                passError: "Please confirm password"
            });
            return;
        }

        if (this.state.password != this.state.confirm_password) {
            this.setState({
                passError: "Passwords do not match"
            });
            return;
        }

        let bl = new ewars.Blocker(null, "Updating password...");
        ewars.tx("com.ewars.user.password.update", [window.user.id, this.state.password])
            .then(function (resp) {
                bl.destroy();
                this.setState({
                    showPassword: false,
                    password: "",
                    confirm_password: "",
                    passError: null
                });
                ewars.growl("Password Updated")
            }.bind(this))
    };

    _cancelPassword = () => {
        this.setState({
            showPassword: false,
            passError: null,
            password: "",
            confirm_password: ""
        })
    };

    onPassChange = (prop, value) => {
        this.setState({
            ...this.state,
            passError: null,
            [prop]: value
        })
    };

    _onModalAction = (action) => {
        if (action == "CANCEL") this._cancelPassword();
        if (action == "SUBMIT") this._submitPasswordChange();
    };

    _onRootChange = (prop, value) => {
        if (!this.state.dirty[prop]) {
            let origValue = this.state.user[prop];
            this.setState({
                user: {
                    ...this.state.user,
                    [prop]: value
                },
                dirty: {
                    ...this.state.dirty,
                    [prop]: origValue
                }
            })
        } else {
            let origValue = this.state.dirty[prop];
            if (origValue == value) {
                let dirty = ewars.copy(this.state.dirty);
                delete dirty[prop];
                this.setState({
                    user: {
                        ...this.state.user,
                        [prop]: origValue
                    },
                    dirty: dirty
                })
            } else {
                this.setState({
                    user: {
                        ...this.state.user,
                        [prop]: value
                    }
                })
            }

        }
    };

    _onCancelEdit = (prop) => {
        let origValue = this.state.dirty[prop];
        let dirty = ewars.copy(this.state.dirty);
        delete dirty[prop];
        this.setState({
            user: {
                ...this.state.user,
                [prop]: origValue
            }
        })
    };

    _onSaveEdit = (prop) => {
        let dirty = ewars.copy(this.state.dirty);

        let bl = new ewars.Blocker(null, "Updating settings...");
        ewars.tx("com.ewars.user.profile.update", [window.user.id], {prop: prop, value: this.state.user[prop]})
            .then(function (resp) {
                bl.destroy();
                delete dirty[prop];
                this.setState({
                    dirty
                });
                ewars.growl("Setting updated");
            }.bind(this))
    };

    render() {
        if (!this.isLoaded) return <Spinner/>;

        let view;

        if (this.state.view == "DEFAULT") view = <Profile user={this.state.user}
                                                          dirty={this.state.dirty}
                                                          onCancelEdit={this._onCancelEdit}
                                                          onSaveEdit={this._onSaveEdit}
                                                          onChange={this._onRootChange}/>;
        if (this.state.view == "ASSIGNMENTS") view = <Assignments/>;
        if (this.state.view == "NOTIFICATIONS") view =
            <Notifications user={this.state.user} onChange={this._onChange}/>;
        if (this.state.view == "SETTINGS") view = <Settings user={this.state.user} onChange={this._onChange}/>;

        const profile_image = {
            height: 200,
            width: "100%",
            background: "#000"
        };

        let hasAssigns = window.user.user_type == "USER" ? true : false;

        const modalButtons = [
            {label: "Submit", icon: "fa-save", action: "SUBMIT"},
            {label: "Cancel", icon: "fa-times", action: "CANCEL"}
        ];

        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col">
                            <div className="ide-layout">
                                <div className="ide-row" style={{maxHeight: 36}}>
                                    <div className="ide-col">
                                        <div xmlns="http://www.w3.org/1999/xhtml" className="iw-tabs"
                                             style={{display: "block"}}>
                                            <Tab
                                                label="Profile"
                                                icon="fa-user"
                                                active={this.state.view == "DEFAULT"}
                                                onClick={this._onChangeView}
                                                data={{view: "DEFAULT"}}/>
                                            {hasAssigns ?
                                                <Tab
                                                    label="Assignments"
                                                    icon="fa-clipboard"
                                                    active={this.state.view == "ASSIGNMENTS"}
                                                    onClick={this._onChangeView}
                                                    data={{view: "ASSIGNMENTS"}}/>
                                                : null}
                                            <Tab
                                                label="Notification Settings"
                                                icon="fa-bullhorn"
                                                active={this.state.view == "NOTIFICATIONS"}
                                                onClick={this._onChangeView}
                                                data={{view: "NOTIFICATIONS"}}/>
                                            <Tab
                                                label="Settings"
                                                icon="fa-cog"
                                                active={this.state.view == "SETTINGS"}
                                                onClick={this._onChangeView}
                                                data={{view: "SETTINGS"}}/>

                                            <div className="btn-group pull-right" style={{marginTop: 4}}>
                                                <ewars.d.Button
                                                    label="Change Password"
                                                    icon="fa-lock"
                                                    onClick={this._showPasswordChanger}/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="ide-row">
                                    <div className="ide-col">
                                        {view}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    title="Change Password"
                    icon="fa-lock"
                    buttons={modalButtons}
                    onAction={this._onModalAction}
                    visible={this.state.showPassword}>
                    <PasswordChanger
                        password={this.state.password}
                        confirm_password={this.state.confirm_password}
                        error={this.state.passError}
                        onChange={this.onPassChange}/>
                </Modal>

            </div>
        )
    }
}

export default Component;
