import Moment from "moment";

import {
    Tab,
    Button,
    Modal
} from "../../common";

import SeriesChart from "../../common/analysis/SeriesChart";
import PieChart from "../../common/analysis/PieChart";
import Gauge from "../../common/analysis/Gauge";

import Module from "./facets/Module.react";
import PlotBrowser from "./PlotBrowser";
import Store from "../utils/Store";

const CHARTS = {
    SERIES: SeriesChart,
    CATEGORY: PieChart,
    GUAGE: Gauge
}

import ChartSettings from "./facets/ChartSettings.react";

const MODAL_BUTTONS = [
    {label: "Close", icon: "fa-times", action: "CLOSE"}
];

class Configure extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="placeholder">Configure a chart to view.</div>
        )
    }
}

class Chart extends React.Component {
    chart = null;

    constructor(props) {
        super(props);

        this.state = {
            config: {}
        }

    }

    componentDidMount() {
        let ChartCmp = CHARTS[this.props.data.type];

        let config = ewars.copy(this.props.data);

        if (config.series) {
            config.series.forEach(function (item) {
                item.location = config.location;
                item.line_width = 2;
            }.bind(this))
        }

        if (this.chart) {
            this.chart.updateDefinition(config);
        } else {
            this.chart = ChartCmp(this.refs.chart, config, null, null, null, null);
        }

    }


    componentDidUpdate() {
        let config = ewars.copy(this.props.data);

        if (config.series) {
            config.series.forEach(function (item) {
                item.location = config.location;
                item.line_width = 2;
            }.bind(this))
        }

        this.chart.updateDefinition(config);
    }

    render() {
        return (
            <div className="chart" ref="chart" style={{padding: 16}}>
                <svg></svg>
            </div>
        )
    }
}

function isUsable(config) {
    let canUse = true;

    if (!config.type) canUse = false;

    if (config.type == "SERIES") {
        if (!config.interval) canUse = false;
        if (!config.series) canUse = false;
        if (config.series) {
            if (config.series.length <= 0) canUse = false;
        }
    }

    if (!config.start_date) canUse = false;
    if (!config.end_date) canUse = false;
    if (!config.location) canUse = false;

    if (config.type == "GAUGE") {
        if (!config.indicator) canUse = false;
        if (!config.reduction) canUse = false;
    }

    return canUse;
}

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "",
            chartType: "LINE",
            mode: "CHART",
            chart: {
                type: "SERIES",
                series: [],
                start_date: Moment.utc().format("YYYY-MM-DD"),
                end_date: Moment.utc().format("YYYY-MM-DD"),
                height: "400px"
            }
        }
    }

    _onModalAction = (action) => {
        if (action == "CLOSE") this.setState({showBrowser: false})
    };

    _onChange = (prop, value) => {
        this.setState({
            chart: {
                ...this.state.chart,
                [prop]: value
            }
        })
    };

    render() {

        let chart;

        if (isUsable(this.state.chart)) {

            chart = <Chart
                data={this.state.chart}/>
        } else {
            chart = <Configure/>
        }


        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col">
                            <div className="ide-layout">
                                <div className="ide-row">
                                    <ChartSettings
                                        data={this.state.chart}
                                        onChange={this._onChange}/>
                                    <div className="ide-col border-top">
                                        <div className="exp-chart">
                                            {chart}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    title="Plot Browser"
                    icon="fa-chart-line"
                    buttons={MODAL_BUTTONS}
                    visible={this.state.showBrowser}
                    onAction={this._onModalAction}>
                    <PlotBrowser onSelect={this._onSelect}/>
                </Modal>
            </div>
        )
    }
}

export default Component;
