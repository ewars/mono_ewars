import {
    DateField,
    SelectField,
    TextField,
    SwitchField
} from "../../../common/fields";

import {
    Button,
    Tab
} from "../../../common";

import Data from "./Data.react";
import Settings from "./Settings.react";

import Action from "../../utils/actions";
import Store from "../../utils/Store";


class ChartSettings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            view: "SETTINGS"
        };
    }


    _onChangeMode = (data) => {
        this.setState({
            view: data.mode
        })
    };

    _onFieldChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    _onSeriesChange = (series) => {
        this.props.onChange("series", series);
    };

    render() {
        let view;
        if (this.state.view == "SETTINGS") {
            view = <Settings
                onChange={this._onFieldChange}
                data={this.props.data}/>;
        }
        if (this.state.view == "DATA") {
            view = <Data
                data={this.props.data}
                onChange={this._onSeriesChange}/>;
        }

        return (
            <div className="ide-col border-right" style={{maxWidth: 400}}>
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div className="iw-tabs">
                                <Tab
                                    label="Settings"
                                    icon="fa-cog"
                                    active={this.state.view == "SETTINGS"}
                                    onClick={this._onChangeMode}
                                    data={{mode: "SETTINGS"}}/>
                                <Tab
                                    label="Data"
                                    icon="fa-tasks"
                                    active={this.state.view == "DATA"}
                                    onClick={this._onChangeMode}
                                    data={{mode: "DATA"}}/>
                            </div>
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            {view}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ChartSettings;
