import AppDispatcher from "./AppDispatcher";

var Actions = function (action, data) {
    AppDispatcher.handleViewAction({
        actionType: action,
        data: data
    })
};


export default Actions;
