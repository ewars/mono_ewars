import Component from "./components/Component.react";

import {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Modal,
    Toolbar,
    Form
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Button,
    Shade,
    Modal,
    Toolbar,
    Form
}

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
);


