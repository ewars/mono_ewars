export default {
    h_general: {
        type: "header",
        order: 0,
        label: {
            en: "General Settings"
        }
    },

    type: {
        type: "select",
        order: 2,
        label: {
            en: "Chart Type"
        },
        required: true,
        options: [
            ["SERIES", "Time Series"],
            ["CATEGORY", "Category"]
            //["GAUGE", "Gauge"]
            //["SCATTERPLOT", "Scatter Plot"],
            //["HEX_BIN", "Hex Bin"],
            //["TREEMAP", "Treemap"],
            //["BULLET", "Bullet Chart"],
            //["CALENDAR", "Calendar"],
            //["STREAM", "Stream"],
            //["SUNBURST", "Sunburst"],
            //["CO_OCCURENCE_MATRIX", "Co-Occurence Matrix"],
            //["RADIAL_PROGRESS", "Radial Progress"],
            //["CORONA_RADAR", "Corona Radar"],
            //["RADAR", "Radar"]
        ]
    },
    interval: {
        type: "select",
        order: 2,
        label: {
            en: "Sample Interval"
        },
        options: [
            ["DAY", "Daily"],
            ["WEEK", "Weekly"],
            ["MONTH", "Monthly"],
            ["YEAR", "Annually"]
        ],
        required: true,
        help: {
            en: "The interval at which to aggregate the data in the series"
        },
        conditions: {
            application: "all",
            rules: [
                ["type", "eq", "SERIES"]
            ]
        }
    },
    location: {
        type: "location",
        label: "Location"
    },
    start_date: {
        type: "date",
        order: 3,
        label: {
            en: "Start Date"
        },
        help: {
            en: "The start date of the series"
        }
    },
    end_date: {
        type: "date",
        label: {
            en: "End Date"
        },
        help: {
            en: "The end date of the series"
        }
    },
    reduction: {
        type: "select",
        label: "Reduction",
        help: {en: "Values for indicators will be aggregated using the specified function"},
        options: [
            ["SUM", "Sum"],
            ["AVG", "Average/Median"]
        ],
        conditions: {
            application: "any",
            rules: [
                ["type", "eq", "CATEGORY"],
                ["type", "eq", "BULLET"],
                ["type", "eq", "GAUGE"]
            ]
        }
    }
}
