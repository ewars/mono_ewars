import Feed from "../../../common/widgets/widget.activity";

class Activity extends React.Component {
    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <Feed noMax={true}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Activity;
