const ALERTS_LIST = {
    header: {
        type: "header",
        label: "General Settings"
    },
    status: {
        type: "select",
        label: {
            "en": "Alarm Status"
        },
        options: [
            ["ANY", "Any"],
            ["ACTIVE", "Inactive"],
            ["INACTIVE", "Inactive"]
        ]
    },
    alarm_id: {
        type: "select",
        label: {en: "Alarm"},
        optionsSource: {
            resource: "alarm",
            query: {},
            labelSource: "name",
            valSource: "uuid"
        }
    },
    restrict: {
        type: "switch",
        label: {en: "Restrict"},
        help: "ALERTS_LIST_WIDGET"
    }
};

export default ALERTS_LIST;