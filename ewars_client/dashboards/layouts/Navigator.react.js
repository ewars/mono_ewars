import user_types from "../../common/constants/user_types";

const USER_TYPES = [
    ["ACCOUNT_ADMIN", "Account Administrator"],
    ["REGIONAL_ADMIN", "Regional Administrator"],
    ["USER", "Reporting User"]
];

class Dashboards extends React.Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    _toggle = () => {
        this.setState({
            showChilds: !this.state.showChilds
        })
    };

    render() {

        let iconClass = "fa fa-caret-right";
        if (this.state.showChilds) iconClass = "fa fa-caret-down";

        let childs;
        childs = USER_TYPES.map(function (item) {
            return (
                <div className="block">
                    <div className="block-content">
                        <div className="ide-row">
                            <div className="ide-col" style={{maxWidth: 22}}>
                                <i className="fal fa-caret-right"></i>
                            </div>
                            <div className="ide-col">{item[1]}</div>
                        </div>
                    </div>
                </div>
            )
        });


        return (
            <div className="block">
                <div className="block-content" onClick={this._toggle}>
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 22}}>
                            <i className={iconClass}></i>
                        </div>
                        <div className="ide-col">Dashboards</div>
                    </div>
                </div>
                {this.state.showChilds ?
                    <div className="block-children">
                        {childs}
                    </div>
                    : null}
            </div>
        )
    }
}


class Layout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="block" onClick={() => this.props.onSelect(this.props.data)}>
                <div className="block-content">{ewars.I18N(this.props.data.name)}</div>
            </div>
        )
    }
}

class Explore extends React.Component {
    _isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            layouts: []
        }
    }

    _toggle = () => {
        if (!this._isLoaded && !this.state.show) {
            ewars.tx("com.ewars.query", ["layout", ["uuid", "name", "status"], {layout_type: {eq: "EXPLORE"}}, null, null, null, null, null])
                .then(function (resp) {
                    this._isLoaded = true;
                    this.setState({
                        show: true,
                        layouts: resp
                    })
                }.bind(this))
        } else {
            this.setState({
                show: !this.state.show
            })
        }
    };

    render() {
        let iconClass = "fal fa-caret-right";
        if (this.state.show) iconClass = "fal fa-caret-down";

        let layouts = this.state.layouts.map(function (layout) {
            return <Layout data={layout} onSelect={() => this.props.onSelect(layout)}/>
        }.bind(this));

        return (
            <div className="block">
                <div className="block-content" onClick={this._toggle}>
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 22}}>
                            <i className={iconClass}></i>
                        </div>
                        <div className="ide-col">Explore</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="block-children">
                        {layouts}
                    </div>
                    : null}
            </div>
        )
    }

}

class Charts extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 22}}>
                            <i className="fal fa-caret-right"></i>
                        </div>
                        <div className="ide-col">Preset Charts</div>
                    </div>
                </div>
            </div>
        )
    }
}

class Notebooks extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 22}}><i className="fal fa-caret-right"></i></div>
                        <div className="ide-col">Analysis Notebooks</div>
                    </div>
                </div>
            </div>
        )
    }
}

class Navigator extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="block-tree">
                    <Dashboards onSelect={(node) => this.props.onSelect(node)}/>
                    <Explore onSelect={(node) => this.props.onSelect(node)}/>
                    <Charts/>
                    <Notebooks/>
                </div>
            </div>
        )
    }
}

export default Navigator;
