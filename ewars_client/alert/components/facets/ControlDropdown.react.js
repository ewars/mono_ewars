import CONSTANTS from "../../../common/constants";

var STYLE = {
    position: "absolute",
    top: 10,
    right: 10,
    height: 25,
    width: 40,
    textAlign: "center",
    borderRadius: 3,
    padding: 5
};

var ControlDropDown = React.createClass({
    getInitialState: function () {
        return {
            showOptions: false
        }
    },
    componentDidMount: function () {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function () {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
    },

    handleBodyClick: function (evt) {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showOptions = false;
                this.forceUpdate();
            }
        }
    },

    handleClick: function (e) {
        e.stopPropagation();
    },

    _onClick: function () {
        this.setState({
            showOptions: this.state.showOptions ? false : true
        })
    },

    render: function () {
        return (
            <div className="btn" style={STYLE} ref="selector">
                <div className="handle" onClick={this._onClick}>
                    <i className={CONSTANTS.ICO_GEAR}></i>&nbsp;
                    <i className={CONSTANTS.ICO_CARET_DOWN}></i>
                </div>
                {this.state.showOptions ?
                    <div className="dropdown">
                        <div className="item"><i className={CONSTANTS.ICO_SEND}></i>&nbsp; Resend Notifications</div>
                    </div>
                    : null}
            </div>
        )
    }
});

export default ControlDropDown;
