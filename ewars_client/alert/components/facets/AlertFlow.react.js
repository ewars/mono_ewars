import Moment from "moment";
import markdown from "marked";

import {
    Button,
    Spinner
} from "../../../common";

import CONSTANTS from "../../../common/constants";

var STATES = {
    "VERIFICATION": {icon: "fa-check", color: "green", label: "Verified"},
    "UNVERIFIED": {icon: "fa-exclamation-circle", color: "red", label: "Unverified"}
};

var ACTIONS = {
    "VERIFIED": {label: "verified this alert on"},
    ASSESSED: {label: "assessed this alert on"},
    CHARACTERIZED: {label: "characterized this alert on"},
    OUTCOME: {label: "set the outcome for this alert on"},
    REOPEN: {label: "reopened this alert on"},
    DISCARDED: {label: "discarded this alert on"},
    AUTOCLOSED: {label: "discarded this alert on"},
    MONITOR: {label: "set this alerts state to Monitor on"},
    RESPOND: {label: "set this alerts outcome to Respond on"},
    CLOSED: {label: "caused this alert to close on"},
    RESPONSE: {label: "set this alerts outcome to Respond on"},
    REPORT_SUBMISSION: {label: "submitted an investigation report on"}
};

var EVENTS = {
    OPENED: {label: "Alert opened on"},
    CLOSED: {label: "Alert was closed on"},
    TRIGGERED: {label: "Alert was raised on"},
    AUTODISCARDED: {label: "Alert was auto-discarded by the system due to inaction on"},
    AUTOCLOSED: {label: "Alert was auto-discarded by the system due to inaction on"}
};

var RISK = [
    ["HIGH", "High Risk"],
    ["MODERATE", "Moderate Risk"],
    ["LOW", "Low Risk"],
    ["NONE", "No Risk"]
];

var FlowAction = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        var actionDate = Moment(this.props.data.action_date).format("MMMM Do, YYYY [UTC]");
        var iconClass = "fal " + this.props.data.icon;

        var state;
        if (this.props.data.state) {
            var stateIcon = "fal " + STATES[this.props.data.state].icon;
            state = (
                <span className="state">
                    <i className={stateIcon}></i>&nbsp;{STATES[this.props.data.state].label}
                </span>
            )
        }

        var actionLabel = ACTIONS[this.props.data.action_type].label;

        return (
            <div className="flow-item flow-action">
                <div className="flow-item-header">
                    <div className="flow-item-icon">
                        <i className={iconClass}></i>
                    </div>
                    <img src="/static/image/avatar.mini.png" width="16px" height="16px" alt=""
                         className="avatar"/>
                    <a href="#" className="author" rel="user"
                       data-user-id={this.props.data.user_id}>{this.props.data.user_name}</a> {actionLabel}
                    <a href="#" className="timestamp">&nbsp;{actionDate}</a>
                </div>
                {this.props.data.content ?
                    <div className="flow-item-body">
                        <div className="flow-item-body-content">{this.props.data.content}</div>
                    </div>
                    : null}
                {state}

            </div>
        )
    }
});

var FlowEvent = React.createClass({
    render: function () {
        var actionDate = Moment(this.props.data.action_date).format("MMMM Do, YYYY [UTC]");
        var iconClass = "fal " + this.props.data.icon;

        var state;
        if (this.props.data.state) {
            var stateIcon = "fal " + STATES[this.props.data.state].icon;
            state = (
                <span className="state">
                    <i className={stateIcon}></i>&nbsp;{STATES[this.props.data.state].label}
                </span>
            )
        }

        var eventLabel = EVENTS[this.props.data.action_type].label;

        return (
            <div className="flow-item flow-action">
                <div className="flow-item-header">
                    <div className="flow-item-icon">
                        <i className={iconClass}></i>
                    </div>
                    {eventLabel}
                    <a href="#" className="timestamp">&nbsp;{actionDate}</a>
                </div>
            </div>
        )
    }
});

var FlowComment = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        var actionDate = Moment(this.props.data.action_date).format("MMMM Do, YYYY HH:mm [(UTC)]");
        return (
            <div className="flow-item comment">
                <div className="flow-comment">
                    <div className="flow-comment-header">
                        <a href="#" className="author" rel="user"
                           data-user-id={this.props.data.user_id}>{this.props.data.user_name}</a> commented on
                        <a href="#" className="timestamp">&nbsp;{actionDate}</a>
                    </div>
                    <div className="flow-comment-body"
                         dangerouslySetInnerHTML={{__html: markdown(this.props.data.content)}}></div>
                </div>
            </div>
        )
    }
});

const AlertFlow = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        return {
            actions: [],
            content: ""
        }
    },

    componentWillMount: function () {
        ewars.subscribe("RELOAD_ACTIVITY", this._reload);
        this._reload();
    },

    _reload: function (bl) {
        ewars.tx("com.ewars.alert.activity", [this.props.data.uuid])
            .then((resp) => {
                 this.state.content = "";
                this.state.actions = resp;
                this._isLoaded = true;
                if (bl) bl.destroy();
                if (this.isMounted()) this.forceUpdate();
            })
    },


    _submitComment: function () {
        if (!this.state.content || this.state.content == "") {
            this.state.error = "Please provide a valid comment";
            return false
        }

        var blocker = new ewars.Blocker(null, "Saving comment...");

        ewars.tx("com.ewars.alert.comment", [this.props.data.uuid, {
                content: this.state.content,
                user_id: window.user.id
            }])
            .then(function (resp) {
                this.state.content = "";
                if (resp == true) {
                    this._reload(blocker);
                }
            }.bind(this))
    },

    _onContentChange: function (e) {
        this.setState({
            content: e.target.value
        })
    },

    render: function () {

        let items = this.state.actions.map(action => {
            if (action.node_type == "ACTION") return <FlowAction data={action} key={action.id}/>;
            if (action.node_type == "COMMENT") return <FlowComment data={action} key={action.id}/>;
            if (action.node_type == "EVENT") return <FlowEvent data={action} key={action.id}/>;
        });

        if (!this._isLoaded) items = <Spinner/>;

        return (
            <div className="flow">
                <div className="flow-items" ref="flowItems">
                    {items}
                </div>
                <div className="flow-actions">
                    <div className="flow-editor-wrapper">
                        <a href="">
                            <img className="flow-editor-avatar" src="" alt=""/>
                        </a>

                        <div className="editor">
                            <div className="editor-header">

                            </div>
                            <div className="editor-body">
                                <div className="flow-text-wrapper">
                                            <textarea name="content"
                                                      className="flow-text"
                                                      onChange={this._onContentChange}
                                                      value={this.state.content}
                                                      id=""
                                                      ref="contentInput"
                                                      cols="30"
                                                      rows="10"></textarea>
                                </div>

                                <div className="btn-group">
                                    <ewars.d.Button
                                        onClick={this._submitComment}
                                        icon="fa-comment"
                                        label="Comment"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});


export default AlertFlow;
