import CONSTANTS from "../../../common/constants";
import {
    Button,
    Modal,
    Shade,
    Layout, Row, Cell,
    Toolbar,
    ButtonGroup
} from "../../../common";

import { TextAreaField } from "../../../common/fields";

const RiskAssessment =  React.createClass({
    getInitialState: function () {
        return {
            data: {},
            showModal: false,
            current: null,
            showHelp: false
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.alert.action.get", [this.props.data.uuid], {type: "RISK_ASSESS"})
            .then(function (resp) {
                if (resp) {
                    this.state.current = resp;
                } else {
                    this.state.current = {
                        type: "RISK_ASSESS",
                        status: "DRAFT_OPEN",
                        user_id: window.user.id,
                        data: {
                            hazard_assessment: "",
                            exposure_assessment: "",
                            context_assessment: ""
                        },
                        alert_id: this.props.data.uuid
                    }
                }
                this._isLoaded = true;

                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _setOptions: function () {
        ewars.tx("com.ewars.alert.action.get", [this.props.data.uuid], {type: "RISK_ASSESS"})
            .then(function (resp) {
                if (resp) {
                    this.state.current = resp;
                }
                this._isLoaded = true;

                this.state.showModal = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onHelpToggle: function (state) {
        this.setState({
            showHelp: state
        })
    },

    _onChange: function (prop, value) {
        this.state.current.data[prop] = value;
        this.forceUpdate();
    },

    _cancel: function () {
        this.state.showModal = false;
        this.forceUpdate();
    },

    _save: function () {
        var data = ewars.copy(this.state.current);

        var blocker = new ewars.Blocker(null, "Saving...");
        ewars.tx("com.ewars.alert.action.update", [this.props.data.uuid, this.state.current.uuid || null, data])
            .then(function (resp) {
                this.state.current = resp;
                blocker.destroy();
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
                ewars.growl("Draft updated...");
                this.forceUpdate();
            }.bind(this))
    },

    _onShadeAction: function () {
        this.setState({
            current: null,
            showModal: false
        })
    },

    _submit: function () {
        if (this.state.current.data.hazard_assessment == "" || !this.state.current.data.hazard_assessment) {
            ewars.growl("Please provide a valid hazard assessment");
            return;
        }

        if (this.state.current.data.context_assessment == "" || !this.state.current.data.context_assessment) {
            ewars.growl("Please provide a valid context assessment");
            return;
        }

        if (this.state.current.data.exposure_assessment == "" || !this.state.current.data.exposure_assessment) {
            ewars.growl("Please provide a valid exposure assessment");
            return;
        }

        var blocker = new ewars.Blocker(null, "Submitting...");
        ewars.tx("com.ewars.alert.action.submit", [this.props.data.uuid, this.state.current.uuid || null, this.state.current])
            .then(function (resp) {
                ewars.emit("RELOAD_ALERT");
                ewars.emit("RELOAD_ACTIVITY");
                this.state.current = resp;
                this.state.showModal = false;
                this.forceUpdate();
                blocker.destroy();
            }.bind(this))
    },

    _swapGuidance: function (prop, value) {
        this.setState({
            showHelp: value
        })
    },

    render: function () {
        var buttonText = "Start risk assessment";
        var headerText = "Risk Assessment - ";

        if (this.props.data.stage == CONSTANTS.RISK_ASSESS) {
            if (this.props.data.stage_state == CONSTANTS.PENDING) {
                buttonText = "Start risk assessment";
                headerText += " Pending";
            }

            if (this.props.data.stage_state == CONSTANTS.ACTIVE) {
                buttonText = "Open risk assessment";
                headerText += " In Progress";
            }
        }

        if (this.props.data.stage == CONSTANTS.RISK_CHAR) {
            buttonText = "Review risk assessment";
            headerText += " Completed";
        }

        if (this.props.data.stage == CONSTANTS.OUTCOME) {
            if (this.props.data.stage_state == CONSTANTS.MONITOR) {
                buttonText = "Update risk assessment";
                headerText += " Monitoring";
            } else {
                buttonText = "Review risk assessment";
                headerText += " Completed";
            }
        }

        var readOnly = false;
        var buttons;
        if (this.state.showModal) {
            if (this.state.current.status == "SUBMITTED" && this.props.data.outcome != "MONITOR") readOnly = true;
            if (this.state.current.status == "DRAFT" && window.user.id != this.state.current.user_id) readOnly = true;

            if (readOnly) {
                buttons = [
                    {label: "Close", icon: CONSTANTS.ICO_CANCEL, onClick: this._cancel, colour: "red"}
                ]
            } else {
                buttons = [
                    {label: "Submit", icon: 'fa-share', onClick: this._submit, colour: "green"},
                    {label: "Save Draft", icon: CONSTANTS.ICO_SAVE, onClick: this._save, colour: "amber"},
                    {label: "Cancel", icon: CONSTANTS.ICO_CANCEL, onClick: this._cancel, colour: "red"}
                ]
            }
        }

        return (
            <div className="dialog">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <div className="dialog-title">{headerText}</div>
                            <p>Complete risk assessment to describe the hazard, exposure and context</p>
                        </td>
                        <td width="25%">
                            <ewars.d.Button
                                onClick={this._setOptions}
                                colour="green"
                                data={{view: "RISK_ASSESS"}}
                                label={buttonText}/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {this.state.showModal ?
                    <Shade
                        toolbar={false}
                        onAction={this._onShadeAction}
                        shown={this.state.showModal}>
                        <Layout>
                            <Toolbar label="Stage 2 | Risk Assessment">


                                <div className="btn-group pull-right">
                                    {buttons.map(button => {
                                        return (
                                            <ewars.d.Button
                                                icon={button.icon}
                                                color={button.colour}
                                                onClick={button.onClick}
                                                label={button.label}/>
                                        )
                                    })}
                                </div>

                                <div style={{float: "right", display: "inline-block"}}>
                                    <span style={{paddingRight: 8, lineHeight: "25px", verticalAlign: "top"}}>Guidance</span>
                                    <ButtonGroup
                                        name="guidance"
                                        value={this.state.showHelp}
                                        config={{options: [
                                            [false, "Off"],
                                            [true, "On"]
                                        ]}}
                                        onUpdate={this._swapGuidance}/>
                                </div>

                            </Toolbar>
                            <Row>
                                <Cell>
                                    <div className="ide-panel ide-panel-absolute ide-scroll">

                                        <div className="article" style={{padding: 14}}>
                                            <div className="alert-stage-heading">Hazard Assessment</div>
                                            {this.state.showHelp ?
                                                <div>
                                                    <p>Hazard assessment includes:</p>
                                                    <ul className="article-list">
                                                        <li> identifying the hazard(s) that could be causing the event
                                                        </li>
                                                        <li>reviewing key information about the potential hazard(s)
                                                            (i.e. characterizing
                                                            the
                                                            hazard)
                                                        </li>
                                                        <li>ranking potential hazards when more than one is considered a
                                                            possible cause
                                                            of the event
                                                            (equivalent to a differential diagnosis in clinical
                                                            medicine).
                                                        </li>
                                                    </ul>
                                                </div>
                                                : null}

                                            <TextAreaField
                                                name="hazard_assessment"
                                                onUpdate={this._onChange}
                                                readOnly={readOnly}
                                                value={this.state.current.data.hazard_assessment}
                                                config={{i18n: false}}/>


                                            <div className="alert-stage-heading">Exposure Assessment</div>
                                            {this.state.showHelp ?
                                                <div className="inline-help">
                                                    <p>The key output of the assessment is an estimate of the:</p>
                                                    <ul className="article-list">
                                                        <li> number of people or group known or likely to have been
                                                            exposed.
                                                        </li>
                                                        <li>number of exposed people or groups who are likely to be
                                                            susceptible (i.e
                                                            capable of
                                                            getting a
                                                            disease because they not immune)
                                                        </li>
                                                    </ul>
                                                </div>
                                                : null}

                                            <TextAreaField
                                                name="exposure_assessment"
                                                onUpdate={this._onChange}
                                                readOnly={readOnly}
                                                value={this.state.current.data.exposure_assessment}
                                                config={{i18n: false}}/>

                                            <div className="alert-stage-heading">Context Assessment</div>
                                            {this.state.showHelp ?
                                                <div>
                                                    <p>Context assessment addresses the following questions:</p>
                                                    <ul className="article-list">
                                                        <li> What are the factors associated with the environment,
                                                            health status,
                                                            behaviours,
                                                            social or
                                                            cultural practices, health infrastructure and legal and
                                                            policy frameworks
                                                            that increase
                                                            a
                                                            population’s vulnerability?
                                                        </li>
                                                    </ul>
                                                </div>
                                                : null}
                                            <TextAreaField
                                                name="context_assessment"
                                                onUpdate={this._onChange}
                                                readOnly={readOnly}
                                                value={this.state.current.data.context_assessment}
                                                config={{i18n: false}}/>

                                        </div>
                                    </div>
                                </Cell>
                            </Row>
                        </Layout>
                    </Shade>
                    : null}
            </div>
        )
    }
});


export default RiskAssessment;
