import CONSTANTS from "../../../common/constants";

import {
    Button,
    Modal,
    Shade,
    Layout, Row, Cell,
    Toolbar,
    ButtonGroup,
    Spinner,
    ListComponent
} from "../../../common";

import {
    TextAreaField
} from "../../../common/fields";

var OPTIONS = [
    {id: "DISCARDED", label: {en: "Discard"}},
    {id: "MONITOR", label: {en: "Monitor"}},
    {id: "ADVANCE", label: {en: "Start Risk Assessment"}}
];

const Verification = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        return {
            data: {},
            showModal: false,
            current: null,
            showHelp: false
        }
    },

    componentWillMount: function () {
        this._load();
    },

    _load: function () {
            ewars.tx("com.ewars.alert.action.get", [this.props.data.uuid], {type: "VERIFICATION"})
                .then(function (resp) {
                    if (resp) {
                        this.state.current = resp;
                    } else {
                        this.state.current = {
                            type: "VERIFICATION",
                            status: "DRAFT_OPEN",
                            user_id: window.user.id,
                            data: {content: "", outcome: null},
                            alert_id: this.props.data.uuid
                        }
                    }
                    this._isLoaded = true;

                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
    },

    componentWillReceiveProps: function (nextProps) {
        this._load();
    },

    _setOptions: function () {
        this._isLoaded = false;
        this.forceUpdate();
        this.state.showModal = true;
        this._load();
    },

    _onHelpToggle: function (state) {
        this.setState({
            showHelp: !this.state.showHelp
        })
    },

    _onChange: function (prop, value) {
        this.state.current.data[prop] = value;
        this.forceUpdate();
    },

    _cancel: function () {
        this.state.current = null;
        this._isLoaded = false;
        this.state.showModal = false;
        if (this.isMounted()) this.forceUpdate();
    },

    _save: function () {
        var data = ewars.copy(this.state.current);

        var blocker = new ewars.Blocker(null, "Saving...");
            ewars.tx("com.ewars.alert.action.update", [this.props.data.uuid, this.state.current.uuid || null, data])
                .then(function (resp) {
                    this.state.current = resp;
                    blocker.destroy();
                    ewars.emit("RELOAD_ALERT");
                    ewars.emit("RELOAD_ACTIVITY");
                    ewars.growl("Draft updated...");
                    this.forceUpdate();
                }.bind(this))
    },

    _submit: function () {
        if (this.state.current.data.content == "" || !this.state.current.data.content) {
            ewars.growl("Please provide valid Verification notes");
            return;
        }

        if (this.state.current.data.outcome == "" || !this.state.current.data.outcome) {
            ewars.growl("Please provide a valid verification outcome");
            return;
        }

        var blocker = new ewars.Blocker(null, "Submitting...");
            ewars.tx("com.ewars.alert.action.submit", [this.props.data.uuid, this.state.current.uuid || null, this.state.current])
                .then(function (resp) {
                    ewars.emit("RELOAD_ALERT");
                    ewars.emit("RELOAD_ACTIVITY");
                    this.state.current = resp;
                    this.state.showModal = false;
                    this.forceUpdate();
                    blocker.destroy();
                }.bind(this))
    },

    _action: function (data) {
        if (data.action == "CANCEL") {
            this._isLoaded = false;
            this.setState({
                current: null,
                showModal: false
            })
        }

        if (data.action == "SAVE") this._save();
        if (data.action == "SUBMIT") this._submit();
    },

    _onShadeAction: function () {
        this._isLoaded = false;
        this.setState({
            current: null,
            showModal: false
        })
    },

    _swapGuidance: function (prop, value) {
        this.setState({
            showHelp: value
        })
    },

    render: function () {
        if (!this._isLoaded && this.state.showModal) {
            return (
                <div className="dialog">
                    <Spinner/>
                </div>
            )
        }

        var buttonText = "Start Verification";
        var headerText = "Verification - ";

        if (this.props.data.stage == CONSTANTS.VERIFICATION) {
            if (this.props.data.stage_state == CONSTANTS.PENDING) {
                buttonText = "Start verification";
                headerText += "Pending"
            }
            if (this.props.data.stage_state == CONSTANTS.ACTIVE) {
                buttonText = "Open verification";
                headerText += "In Progress";
            }
            if (this.props.data.stage_state == CONSTANTS.COMPLETED) {
                buttonText = "Review verification";
                headerText += "Completed";
            }
            if (this.props.data.outcome == CONSTANTS.MONITOR) {
                buttonText = "Update verification";
                headerText += "Monitoring";
            }

        }

        if ([CONSTANTS.RISK_ASSESS, CONSTANTS.RISK_CHAR, CONSTANTS.OUTCOME].indexOf(this.props.data.stage) >= 0) {
            buttonText = "Review verification";
            headerText += "Completed";
        }

        var readOnly = false;
        var buttons;

        if (this.state.showModal) {
            if (this.state.current.status == "SUBMITTED") readOnly = true;
            if (this.state.current.status == "DRAFT" && window.user.id != this.state.current.user_id) readOnly = true;
            if ([CONSTANTS.OUTCOME, CONSTANTS.RISK_ASSESS, CONSTANTS.RISK_CHAR].indexOf(this.props.data.stage) >= 0) readOnly = true;
            if (this.props.data.outcome == "MONITOR" && this.props.data.stage == "VERIFICATION") readOnly = false;

            if (this.props.data.outcome == "DISCARDED") readOnly = true;

            if (readOnly) {
                buttons = [
                    {label: "Close", icon: 'fa-times', action: "CANCEL", color: "red"}
                ]
            } else {
                buttons = [
                    {label: "Submit", icon: 'fa-share', action: "SUBMIT", color: "green"},
                    {label: "Save Draft", icon: 'fa-save', action: "SAVE", color: "amber"},
                    {label: "Cancel", icon: 'fa-times', action: "CANCEL", color: "red"}
                ]
            }
        }

        return (
            <div className="dialog">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <div className="dialog-title">{headerText}</div>
                            <p>Verify the alert to determine if it needs to be risk assessed, monitored or discarded</p>
                        </td>
                        <td width="25%">
                            <ewars.d.Button
                                colour="green"
                                onClick={this._setOptions}
                                data={{view: "VERIFICATION"}}
                                label={buttonText}/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {this.state.showModal ?
                    <Shade
                        toolbar={false}
                        onAction={this._onShadeAction}
                        shown={this.state.showModal}>
                        <Layout>
                            <Toolbar label="Stage 1 | Verification">
                                <div className="btn-group pull-right">

                                    {buttons.map(button => {
                                        return (
                                            <ewars.d.Button
                                                label={button.label}
                                                icon={button.icon}
                                                data={button}
                                                onClick={this._action}
                                                color={button.color}/>
                                        )
                                    })}
                                </div>

                                <div style={{float: "right", display: "inline-block"}}>
                                    <span style={{paddingRight: 8, lineHeight: "25px", verticalAlign: "top"}}>Guidance</span>
                                    <ButtonGroup
                                        name="guidance"
                                        value={this.state.showHelp}
                                        config={{options: [
                                            [false, "Off"],
                                            [true, "On"]
                                        ]}}
                                        onUpdate={this._swapGuidance}/>
                                </div>
                            </Toolbar>
                            <Row>
                                <Cell>
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="article" style={{padding: 16}}>
                                            <div className="alert-stage-heading">Verification notes</div>
                                            {this.state.showHelp ?
                                                <div>
                                                    <ul className="article-list">
                                                        <li>Has the event been reported by an official source (e.g. local health-care
                                                            centre or
                                                            clinic,
                                                            public health authorities, animal health workers?
                                                        </li>
                                                        <li>Has the event been reported by multiple independent sources (e.g. residents,
                                                            news
                                                            media,
                                                            health-care workers, animal health status )?
                                                        </li>
                                                        <li>Does the event description include details about time, place and people
                                                            involved (e.g.
                                                            six
                                                            people
                                                            are sick and two died three days after a ending a local celebration on in
                                                            community
                                                            X)?
                                                        </li>
                                                        <li>Is the clinical presentation of the cases described (e.g. a cluster of seven
                                                            people
                                                            admitted to
                                                            hospital with atypical pneumonia, of whom two have died)?
                                                        </li>
                                                        <li>Has a similar event been reported previously (e.g. with a similar
                                                            presentation,
                                                            affecting a
                                                            similar population and geographical area, over the same me period)?
                                                        </li>
                                                    </ul>
                                                </div>
                                                : null}

                                            <TextAreaField
                                                name="content"
                                                readOnly={readOnly}
                                                onUpdate={this._onChange}
                                                value={this.state.current.data.content}
                                                config={{i18n: false, markdown: true}}/>

                                            <div className="alert-stage-heading">Outcome</div>

                                            <ListComponent
                                                items={OPTIONS}
                                                name="outcome"
                                                readOnly={readOnly}
                                                value={this.state.current.data.outcome}
                                                onUpdate={this._onChange}/>
                                        </div>
                                    </div>
                                </Cell>
                            </Row>
                        </Layout>
                    </Shade>
                    : null}
            </div>
        )
    }
});

export default Verification;
