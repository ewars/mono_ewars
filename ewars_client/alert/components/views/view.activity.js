import AlertFlow from '../facets/AlertFlow.react';

class Activity extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <AlertFlow data={this.props.data}/>
        )
    }
}

export default Activity;
