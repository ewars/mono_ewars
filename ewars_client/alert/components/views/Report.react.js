import Moment from "moment";

import CONSTANTS from "../../../common/constants";
import Lang from "../../../common/lang/Lang";
import { Form, Spinner } from "../../../common";
import FormUtils from "../../../common/utils/FormUtils";


var ReportComponent = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            entry: {},
            data: null,
            formVersion: {
                definition: {}
            }
        }

    },

    componentWillMount: function () {
        if (this.props.data.uuid) {
            ewars.tx("com.ewars.resource", ["report", this.props.data.uuid, null, ["user:created_by:id", "form:form_id:id:id,name,features", "version:form_version_id:uuid:uuid,definition"]])
                .then(function (resp) {
                    this.state.data = resp;
                    this._hasLoaded = true;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        } else {
            this._hasLoaded = true;
        }
    },


    _buildReportDetails: function () {

        var fields = {};

        if (this.state.data.form.ftype == "PRIMARY") {

            fields.location_id = {
                type: "location",
                label: "Location",
                required: true,
                location_type: this.state.data.site_type_id,
                form_id: this.state.data.form_id,
                help: "Select the location this report belongs to"
            }
        }

        var reportingInterval = this.state.data.form.time_interval;

        var allow_future = !this.state.data.form.block_future_dates;

        if (this.state.data.form.time_interval != "WEEK") {
            fields.data_date = {
                type: "date",
                label: "Report Date",
                required: true,
                allow_future: allow_future,
                date_type: reportingInterval,
                help: "Select the date that this reports data is relevant for"
            }
        } else if (this.state.data.form.time_interval == "WEEK") {
            fields.data_date = {
                type: "week",
                label: "Report Date",
                allow_future: allow_future,
                required: true,
                help: "Select the date that this reports data is relevant for"
            }
        }


        if (this.state.data.form.is_lab) {
            fields.lab_id = {
                type: "select",
                label: {
                    en: "Sample Destination Laboratory"
                },
                help: {
                    en: "Select the laboratory the sample was sent to"
                },
                required: true,
                optionsSource: {
                    resource: "Laboratory",
                    query: {
                        $filter: ["status eq 'ACTIVE'"],
                        $orderby: ['name ASC']
                    },
                    valSource: "uuid",
                    labelSource: "name"
                }
            };

            fields.sample_id = {
                type: "text",
                label: {
                    en: "Sample ID"
                },
                required: true
            }

        }

        return fields;
    },

    _buildReportMeta: function () {
        if (this.state.data.status == 'SUBMITTED') {
            return <div className="meta-item">Submitted by <a href="#" rel="user"
                                                              data-user-id={this.state.data.user.id}>{this.state.data.user.name}</a>&nbsp;
                on&nbsp;{Moment(this.state.data.submitted_date).format("dddd, MMMM Do YYYY")}</div>
        }
    },

    render: function () {
        if (!this.state.data && this.props.data.uuid && this._hasLoaded) {
            return (
                <div className="panel-fill">
                    <div className="placeholder">Triggering report is no longer available.</div>
                </div>
            )
        }

        if (!this._hasLoaded) {
            return (
                <div className="panel">
                    <Spinner/>
                </div>
            )
        }

        if (this.state.data.status == "DELETED") {
            return (
                <div className="panel">
                    <div className="warning"><i className="fal fa-exclamation-triangle"></i>&nbsp;The report which
                        originally triggered this alert has been deleted and is no longer available.
                    </div>
                </div>
            )
        }

        var readOnly = true;

        var submitDate;

        var submitBy;
        if (this.state.entry.status != "DRAFT") submitBy = (
            <a href="#" rel="user" data-user-id={this.state.data.created_by}>{this.state.data.user.name}</a>);

        var reportForm = this._buildReportDetails();

        var reportTitle = ewars.formatters.I18N_FORMATTER(this.state.data.form.name);

        var reportMeta = this._buildReportMeta();

        // Can the user edit the report details
        var detailsReadOnly = true;

        var form;
        if (this._hasLoaded) {
            form = <Form
                readOnly={readOnly}
                errors={this.state.reportErrors}
                data={this.state.data.data}
                updateAction={this._onEntryPropChange}
                definition={this.state.data.version.definition}/>
        }

        return (
            <div className="basic" style={{display: "block"}}>
                <div className="report">
                    <div className="report-header">
                        <div className="report-name">{reportTitle}</div>
                        <div className="meta">
                            {reportMeta}
                        </div>
                    </div>

                    <div className="widget">
                        <div className="body">
                            <Form
                                definition={reportForm}
                                errors={this.state.reportErrors}
                                updateAction={this._onRootEntryPropChange}
                                data={this.state.data}
                                readOnly={detailsReadOnly}/>

                        </div>
                    </div>

                    <div className="widget">
                        <div className="body">
                            {form}
                        </div>
                    </div>

                </div>
            </div>
        )
    }
});

export default ReportComponent;
