module.exports = {
    VERIFICATION: {
        title: {en: "Stage 1 | Verification"},
        content: "",
        guidance: "http://google.com"
    },
    RISK_ASSESS: {
        title: {en: "Stage 2 | Risk Assessment"},
        content: "",
        guidance: "http://google.com"
    },
    RISK_CHAR: {
        title: {en: "Stage 3 | Risk Characterization"},
        content: "",
        guidance: "http://google.com"
    },
    OUTCOME: {
        title: {en: "Stage 4 | Outcome"},
        content: "",
        guidance: "http://google.com"
    }
};