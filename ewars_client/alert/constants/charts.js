var CONSTANTS = require("../../../common/constants");

module.exports = {
    IND_TREND: {
        title: "Alert Indicator Trend: {indicator_name} - {location_name}",
        series: [
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: null, location: null}
        ]
    },
    ALARM_TREND: {
        title: "Alarm Trend: {alarm_name} - {location_name}",
        series: [
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: {itype: CONSTANTS.CUSTOM, context: CONSTANTS.INDICATOR, definition: {id: "ALARM_ALERTS_RAISED"}}, location: null},
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: {itype: CONSTANTS.CUSTOM, context: CONSTANTS.INDICATOR, definition: {id: "ALARM_ALERTS_UNVERIFIED"}}, location: null},
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: {itype: CONSTANTS.CUSTOM, context: CONSTANTS.INDICATOR, definition: {id: "ALARM_ALERTS_VERIFIED"}}, location: null},
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: {itype: CONSTANTS.CUSTOM, context: CONSTANTS.INDICATOR, definition: {id: "ALARM_ALERTS_RISK"}}, location: null},
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: {itype: CONSTANTS.CUSTOM, context: CONSTANTS.INDICATOR, definition: {id: "ALARM_ALERTS_INVESTIGATION"}}, location: null},
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: {itype: CONSTANTS.CUSTOM, context: CONSTANTS.INDICATOR, definition: {id: "ALARM_ALERTS_CLOSED"}}, location: null},
            {dataType: CONSTANTS.SERIES, timeInterval: null, indicator: {itype: CONSTANTS.CUSTOM, context: CONSTANTS.INDICATOR, definition: {id: "ALARM_ALERTS_OPEN"}}, location: null}
        ]
    },
    REPORTING_TREND: {
        title: "Reporting Trend: {location_name}",
        series: [
            {dataType: CONSTANTS.SERIES, timeInterval, null, indicator: null, location: null}
        ]
    },
    NEARBY_IND_TREND: {
        title: "Nearby Alert Indicator Trend: {indicator_name} - {location_name}",
        series: [
            {dataType: CONSTANTS.SERIES, timeInterval, null, indicator, null, location: null}
        ]
    }
};