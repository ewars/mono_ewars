const CONSTANTS = {
    VERIFICATION: "VERIFICATION",
    RISK_CHAR: "RISK_CHAR",
    RISK_ASSESS: "RISK_ASSESS",
    OUTCOME: "OUTCOME",
    TRIGGERED: "TRIGGERED",


    DASHBOARD: "DASHBOARD",
    INVESTIGATION: "INVESTIGATION",
    REPORT: "REPORT",
    REPORTS: "REPORTS",
    USERS: "USERS",
    ANALYSIS: "ANALYSIS",
    RELATED: "RELATED",
    GRAPHS: "GRAPHS",

    OPEN: "OPEN",
    CLOSED: "CLOSED",

    PENDING: "PENDING",
    ACTIVE: "ACTIVE",
    COMPLETED: "COMPLETED",

    RELOAD_ALERT: "RELOAD_ALERT",

    LOW: "LOW",
    MODERATE: "MODERATE",
    HIGH: "HIGH",
    SEVERE: "SEVERE",

    DRAFT: "DRAFT",
    SUBMITTED: "SUBMITTED"
}

export default CONSTANTS;
