import Component from "./components/Component.react";

import {
    Layout,
    Row,
    Cell,
    Toolbar,
    Panel,
    Button,
    ButtonGroup,
    Shade,
    Modal
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Toolbar,
    Panel,
    Button,
    ButtonGroup,
    Shade,
    Modal
};

ReactDOM.render(
    <Component/>,
    document.getElementById('app')
);


