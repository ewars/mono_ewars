import { Button, Modal } from "../../common";

var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

import Selector from "./Selector";

var USER_TYPES = [
    {
        icon: "fa-street-view",
        name: "Geographic Administrator",
        description: "Manage reports, users and tasks within a specific location.",
        code: "REGIONAL_ADMIN"
    },
    {
        icon: "fa-user",
        name: "Reporting User",
        description: "Submit and view reports.",
        code: "USER"
    }
];

const MODAL_BUTTONS = [
    {label: "Cancel", icon: "fa-times", action: "CANCEL"}
];

const MAPPER = {
    organization: 'org_id',
    account: 'account_id'
};

function isNull(value) {
    if (value == null) return true;
    if (value == undefined) return true;
    if (value == "") return true;
    return false;
}

var Completed = React.createClass({
    render: function () {
        return (
            <div className="widget">
                <div className="widget-header"><span>Registration Submitted</span></div>
                <div className="body">
                    <p>A verification email has been sent to the email that you have provided, please check your inbox
                        and click the link provided in the email to verify your ownership of the email address.</p>
                    <p>Once your email address has been verified an administrator will review your registration request
                        and approve or reject the registration, you will receive an email with the outcome.</p>

                    <p>Your account can not be accessed until approved by an administrator.</p>

                    <p>Thank you for submitting your registration request.</p>
                </div>
            </div>
        )
    }
});

class Error extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.data) return null;
        return (
            <p className="error"><i className="fal fa-exclamation-triangle"></i> {this.props.data}</p>
        )
    }
}

var UserDetails = React.createClass({
    getInitialState: function () {
        return {
            err: {},
            done: false
        }
    },

    _onNext: function () {
        if (this._validate()) {
            var blocker = new ewars.Blocker(null, "Processing...");
            ewars.tx("com.ewars.registration.action", ["CHECK_EMAIL", this.props.data.email])
                .then(function (resp) {
                    if (!resp.result) {
                        blocker.destroy();
                        this.props.onNext();
                    } else {
                        blocker.destroy();
                        this._showError("This email address is already in use.")
                    }

                }.bind(this))
        } else {
            this.forceUpdate();
        }
    },

    _validate: function () {
        if (!this.props.data.name || this.props.data.name == "") {
            this._showError("Please provide a valid name");
            return false;
        }

        if (!re.test(this.props.data.email)) {
            this._showError("Please provide a valid email");
            return false;
        }

        if (!this.props.data.email || this.props.data.email == "") {
            this._showError("Please provide a valid email");
            return false;
        }
        if (this.props.data.email != this.props.data.confirm_email) {
            this._showError("Emails do not match");
            return false;
        }

        if (!this.props.data.password || this.props.data.password == "") {
            this._showError("Please provide a valid password");
            return false;
        }

        if (this.props.data.password != this.props.data.confirm_password) {
            this._showError("Passwords do not match");
            return false;
        }

        if (this.props.data.password.length < 8) {
            this._showError("Password must be a minimum of 8 characters and contain at least 1 number");
            return false;
        }

        if (!/\d/.test(this.props.data.password)) {
            this._showError("Passwords must be a minimum of 8 characters and contain at least 1 number");
            return false;
        }

        return true;
    },

    _showError: function (message) {
        ewars.growl(message);
    },

    _onChange: function (e) {
        if (Object.keys(this.state.err).length > 0) {
            this.setState({
                err: {}
            })
        }
        this.props.onChange(e.target.name, e.target.value);
    },

    _selectAccount: function () {
        this.setState({
            show: true,
            resource: "account"
        })
    },

    _selectOrg: function () {
        this.setState({
            show: true,
            resource: "organization"
        })
    },

    _clearAccount: function () {
        this.setState({
            account: null
        });
        this.props.onChange("account_id", null);
    },

    _clearOrg: function () {
        this.setState({
            organization: null
        });
        this.props.onChange("org_id", null)
    },

    _onModalAction: function (action) {
        if (action == "CLOSE") this.setState({show: false, resource: null})
    },

    _onSelect: function (item) {
        let resource = this.state.resource;
        this.setState({
            resource: null,
            show: false,
            [resource]: item
        });
        this.props.onChange(MAPPER[resource], item.id || item.uuid);
    },

    _validate: function () {

        let err = {};
        console.log(this.props.data);

        if (isNull(this.props.data.name)) err.name = "Please provide a valid name";
        if (isNull(this.props.data.email)) err.email = "Please provide a value email";
        if (!re.test(this.props.data.email)) err.email = "Please provide a valid email";
        if (isNull(this.props.data.confirm_email)) err.confirm_email = "Please confirm the email address provided";
        if (this.props.data.confirm_email != this.props.data.email && isNull(err.email)) err.email = "Emails do not match";

        if (isNull(this.props.data.password)) err.password = "Please provide a valid password";
        if (isNull(this.props.data.confirm_password)) err.confirm_password = "Please confirm your password";
        if (this.props.data.password.length < 6) err.password = "Password must be at least 6 characters long";
        if (this.props.data.password != this.props.data.confirm_password) err.password = "Passwords do no match";

        if (isNull(this.props.data.org_id)) err.org_id = "Please select an organization";
        if (isNull(this.props.data.account.id)) err.account_id = "Please select an account";

        if (Object.keys(err).length > 0) {
            ewars.growl("There are errors in the registration form.");
            this.setState({
                err: err
            });
            return;
        }

        this._register();

    },

    _handleResult: function (res) {
        if (res.err) {
            this.setState({
                err: res.data
            })
        } else {
            this.setState({
                done: true
            })
        }
    },

    _register: function () {
        let bl = new ewars.Blocker(null, "Processing Registration....");


        var oReq = new XMLHttpRequest();
        oReq.open("POST", "/register", true);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        oReq.onreadystatechange = function () {
            let DONE = 4;
            let OK = 200;

            if (oReq.readyState == DONE) {
                if (oReq.status == OK) {
                    bl.destroy();
                    this._handleResult(JSON.parse(oReq.responseText))
                } else {
                }
            }
        }.bind(this);

        oReq.send(JSON.stringify(this.props.data))
    },

    render: function () {

        if (this.state.done) {
            return (
                <div className="widget">
                    <div className="body no-pad">
                        <Completed/>
                    </div>
                </div>
            )
        }

        return (
            <div>
                <div className="article"
                     style={{paddingRight: 150, paddingLeft: 30, paddingTop: 30, marginBottom: 40}}>
                    <div className="ide-layout">
                        <div className="ide-row">
                            <div className="ide-col" style={{maxWidth: 150}}></div>
                            <div className="ide-col">

                                <p>Please provide us with the details below to start your
                                    registration.</p>
                            </div>

                        </div>

                        <Error data={this.state.err.name}/>
                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Your Name *
                            </div>
                            <div className="ide-col">
                                <input type="text"
                                       name="name"
                                       onChange={this._onChange}
                                       value={this.props.data.name}
                                       placeholder="e.g. John Smith..."/>
                            </div>
                        </div>

                        <Error data={this.state.err.email}/>
                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Your Email *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="email"
                                    value={this.props.data.email}
                                    name="email"
                                    placeholder="e.g. j.smith@ewars.ws..."/>
                            </div>
                        </div>

                        <Error data={this.state.err.confirm_email}/>
                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Confirm Email *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="email"
                                    value={this.props.data.confirm_email}
                                    name="confirm_email"
                                    placeholder="e.g. j.smith@ewars.ws..."/>
                            </div>
                        </div>

                        <Error data={this.state.err.password}/>
                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Password *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="password"
                                    value={this.props.data.password}
                                    name="password"/>
                            </div>
                        </div>

                        <Error data={this.state.err.confirm_password}/>
                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Confirm Password *
                            </div>
                            <div className="ide-col">
                                <input
                                    onChange={this._onChange}
                                    type="password"
                                    value={this.props.data.confirm_password}
                                    name="confirm_password"/>
                            </div>
                        </div>

                        <Error data={this.state.err.org_id}/>
                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Organization *
                            </div>
                            <div className="ide-col">
                                {!this.state.organization ?
                                    <Button label="Select Organization" onClick={this._selectOrg}/>
                                    :
                                    <div className="reg-item">
                                        <div className="ide-row">
                                            <div
                                                className="ide-col result-name">{ewars.I18N(this.state.organization.name)}</div>
                                            <div className="ide-col other" style={{maxWidth: 30}}>
                                                <Button icon="fa-times" onClick={this._clearOrg}/>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>

                        <Error data={this.state.err.account_id}/>
                        <div className="ide-row" style={{marginBottom: 8}}>
                            <div className="ide-col"
                                 style={{
                                     maxWidth: 150,
                                     fontWeight: "bold",
                                     textAlign: "right",
                                     paddingRight: 10,
                                     paddingTop: 8
                                 }}>
                                Account *
                            </div>
                            <div className="ide-col">

                                {!this.state.account ?
                                    <Button label="Select Account" onClick={this._selectAccount}/>
                                    :
                                    <div className="reg-item">
                                        <div className="ide-row">
                                            <div
                                                className="ide-col result-name">{ewars.I18N(this.state.account.name)}</div>
                                            <div className="ide-col other" style={{maxWidth: 30}}>
                                                <Button icon="fa-times" onClick={this._clearAccount}/>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>

                    </div>
                </div>
                {
                    this.state.show ?
                        <ewars.d.Shade
                            title="Select Item"
                            icon="fa-search"
                            buttons={MODAL_BUTTONS}
                            onAction={this._onModalAction}
                            shown={this.state.show}>
                            <Selector
                                resource={this.state.resource}
                                onSelect={this._onSelect}/>
                        </ewars.d.Shade>
                        : null
                }
            </div>

        )
    }
});

export default UserDetails;
