import { Button } from "../../../common";

var Item = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "item";
        if (this.props.active) className += " active";

        var flagClass = "flag-icon flag-icon-" + this.props.data.flag_code;

        return (
            <div className={className} onClick={this._onClick}>
                <div className="wrapper">
                    <div className="icon">
                        <span className={flagClass}></span>
                    </div>
                    <div className="details">
                        <div className="title">{this.props.data.name}</div>
                        <div className="sub-title">{this.props.data.domain}</div>
                    </div>
                </div>
            </div>
        )
    }
});

var AccountSelection = React.createClass({
    getInitialState: function () {
        return {
            accounts: []
        }
    },

    componentWillMount: function () {
        var blocker = new ewars.Blocker(null, "Loading accounts...");
        ewars.tx("com.ewars.accounts", []).then(function (resp) {
            this.state.accounts = resp;
            blocker.destroy();
            this.forceUpdate();
        }.bind(this))
    },

    _onNext: function () {
        if (this._validate()) {
            this.props.onNext();
        }
    },

    _selectItem: function (node) {
        this.props.onChange("account_id", node.id);
    },

    _validate: function () {
        if (!this.props.data.account_id) {
            ewars.growl("Please select an account");
            return false;
        }

        return true;
    },

    _onPrev: function () {
        this.props.onPrev("ACCOUNT");
    },

    render: function () {

        var accs = _.map(this.state.accounts, function (account) {
            var active = false;
            if (this.props.data.account_id == account.id) active = true;
            return (
                <Item data={account} active={active} onClick={this._selectItem}/>
            );
        }, this);

        return (
            <div className="widget">
                <div className="widget-header"><span>Account Selection</span></div>
                <div className="body">
                    <p>Please select the account you are registering to</p>

                    <div className="select-list">
                        {accs}
                    </div>
                </div>
                <div className="widget-footer">

                    <div className="btn-group">
                        <Button
                            label="Back"
                            colour="amber"
                            icon="fa-caret-left"
                            onClick={this._onPrev}/>
                    </div>

                    <div className="btn-group pull-right">
                        <Button
                            label="Next"
                            colour="green"
                            icon="fa-caret-right"
                            onClick={this._onNext}/>
                    </div>

                </div>
            </div>
        )
    }
});

export default AccountSelection;
