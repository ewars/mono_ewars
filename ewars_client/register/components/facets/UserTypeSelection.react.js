import { Button } from "../../common";

var USER_TYPES = [
    {
        icon: "fa-user",
        name: "Reporting User",
        description: "Submit and view reports.",
        code: "USER"
    },
    //{
    //    icon: "fa-flask",
    //    name: "Laboratory User",
    //    description: "Manage and complete lab reports.",
    //    code: "LAB_USER"
    //},
    {
        icon: "fa-street-view",
        name: "Geographic Administrator",
        description: "Manage reports, users and tasks within a specific location.",
        code: "REGIONAL_ADMIN"
    }
    //{
    //    icon: "fa-hospital",
    //    name: "Organization Administrator",
    //    description: "Manage and oversee users, reports and data from users in your organization.",
    //    code: "ORG_ADMIN"
    //}
];

var UserType = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var icon = "fa " + this.props.data.icon;

        var className = "item";
        if (this.props.active) className += " active";

        return (
            <div className={className} onClick={this._onClick}>
                <div className="wrapper">
                    <div className="icon">
                        <i className={icon}></i>
                    </div>
                    <div className="details">
                        <div className="title">{this.props.data.name}</div>
                        <div className="sub-title">{this.props.data.description}</div>
                    </div>
                </div>
            </div>
        )
    }
});

var UserTypeSelection = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onSelect: function (item) {
        this.props.onChange("user_type", item.code);
    },

    _onNext: function () {
        if (this._validate()) {
            this.props.onNext();
        }
    },

    _validate: function () {
        if (!this.props.data.user_type) {
            ewars.growl("Please select a user type");
            return false;
        }

        return true;
    },

    _onPrev: function () {
        this.props.onPrev("USER_TYPE");
    },

    render: function () {

        var items = _.map(USER_TYPES, function (item) {
            var active = item.code == this.props.data.user_type;
            return (
                <UserType data={item} onClick={this._onSelect} active={active}/>
            )
        }, this);

        return (
            <div className="widget">
                <div className="widget-header"><span>User Type</span></div>
                <div className="body">
                    <p>Please select the type of user you want to be;</p>

                    <div className="select-list">
                        {items}
                    </div>
                </div>
                <div className="widget-footer">

                    <div className="btn-group">
                        <Button
                            label="Back"
                            colour="amber"
                            icon="fa-caret-left"
                            onClick={this._onPrev}/>
                    </div>
                    <div className="btn-group pull-right">
                        <Button
                            label="Next"
                            colour="green"
                            icon="fa-caret-right"
                            onClick={this._onNext}/>
                    </div>
                </div>

                <div className="clearer"></div>
            </div>
        )
    }
});

export default UserTypeSelection;
