import { Button } from "../../../common";

var ERROR_CODES = [
    "Please provide a valid email",
    "Email and Confirmation Email must match",
    "Please provide a valid password",
    "Password and Confirmation Password must match",
    "Please provide a stronger password",
    "A user with this email exists, please login or register with a different email",
    "An unknown error has occurred, please contact a system administrator",
    "The email provided is already registered to an account awaiting approval",
    "Please provide a valid name",
    "Please provide a valid email address",
    "Please provide a valid organization",
    "Please provide a valid country account",
    "Please provide a valid laboratory"
];

var Review = React.createClass({
    getInitialState: function () {
        return {}
    },

    _submit: function () {
        var blocker = new ewars.Blocker(null, "Submitting registration...");

        ewars.tx("com.ewars.register", [this.props.data])
            .then(function (resp) {
                if (resp.result) {
                    this._onSuccess();
                } else {
                    this._onError(resp.code);
                }
                blocker.destroy();
            }.bind(this))
    },

    _onSuccess: function () {
        this.props.onSuccess();
    },

    _onError: function (code) {
        ewars.growl(ERROR_CODES[code]);
    },

    _onPrev: function () {
        this.props.onPrev("REVIEW");
    },

    render: function () {
        return (
            <div className="widget">
                <div className="widget-header"><span>Review</span></div>
                <div className="body">
                    <p>Please review the account details below and click "Register" to request your account.</p>

                    <table width="100%" className="info-list">
                        <tr>
                            <th>Name</th>
                            <td>{this.props.data.name}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{this.props.data.email}</td>
                        </tr>
                    </table>
                </div>
                <div className="widget-footer">
                    <div className="btn-group">
                        <Button
                            label="Back"
                            colour="amber"
                            icon="fa-caret-left"
                            onClick={this._onPrev}/>
                    </div>

                    <div className="btn-group pull-right">
                        <Button
                            label="Submit Registration"
                            colour="green"
                            icon="fa-share-square"
                            onClick={this._submit}/>
                    </div>
                </div>
            </div>
        )
    }
});

export default Review;
