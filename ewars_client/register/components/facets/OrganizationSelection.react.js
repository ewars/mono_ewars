import { Button } from "../../../common";


var Item = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "item";
        if (this.props.active) className += " active";
        let name = ewars.I18N(this.props.data.name);

        return (
            <div className={className} onClick={this._onClick}>
                <div className="wrapper">
                    <div className="details">
                        <div className="title">{name}</div>
                    </div>
                </div>
            </div>
        )
    }
});

var OrganizationSelection = React.createClass({
    getInitialState: function () {
        return {}
    },

    componentWillMount: function () {
        var blocker = new ewars.Blocker(null, "Loading organizations...");
        ewars.tx("com.ewars.organizations", [])
            .then(function (resp) {
                this.state.organizations = resp;
                blocker.destroy();
                this.forceUpdate();
            }.bind(this))
    },

    _onSelect: function (node) {
        this.props.onChange("org_id", node.uuid);
    },

    _onNext: function () {
        if (this._validate()) {
            this.props.onNext();
        }
    },

    _validate: function () {
        if (!this.props.data.org_id) {
            ewars.growl("Please select an organization");
            return false;
        }
        return true;
    },

    _onPrev: function () {
        this.props.onPrev("ORGANIZATION");
    },

    render: function () {

        var orgs = _.map(this.state.organizations, function (org) {
            var active = false;
            if (this.props.data.org_id == org.uuid) active = true;
            return (
                <Item data={org} active={active} onClick={this._onSelect}/>
            );
        }, this);

        return (
            <div className="widget">
                <div className="widget-header"><span>Organization</span></div>
                <div className="body">
                    <p>Please select the organization that you belong to</p>

                    {this.props.data.user_type == "USER" ?
                        <p className="warning"><strong>Note:</strong> If you can not find your organization in the list
                            below, select "Other" and send a request to <a
                                href="mailto:support@ewars.ws">support@ewars.ws</a> to have your organization added.</p>
                        : null}

                    {this.props.data.user_type == "ORG_ADMIN" ?
                        <p className="warning"><strong>Note:</strong> If you can not find your organization in the list
                            below, please contact and administrator at <a
                                href="mailto:support@ewars.ws">support@ewars.ws</a> before continuing with registration.
                        </p>
                        : null}

                    <div className="select-list">
                        {orgs}
                    </div>
                </div>
                <div className="widget-footer">
                    <div className="btn-group">

                        <Button
                            label="Back"
                            colour="amber"
                            icon="fa-caret-left"
                            onClick={this._onPrev}/>
                    </div>

                    <div className="btn-group pull-right">
                        <Button
                            label="Next"
                            colour="green"
                            icon="fa-caret-right"
                            onClick={this._onNext}/>
                    </div>
                </div>
            </div>
        )
    }
});

export default OrganizationSelection;
