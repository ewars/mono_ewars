var Complete = React.createClass({
    render: function () {
        return (
            <div className="widget">
                <div className="widget-header"><span>Registration Submitted</span></div>
                <div className="body">
                    <p>You registration request has been submitted and will be reviewed and either approved or rejected by an Administrator.</p>
                    <p>Once your account is approved you will receive an email notifying you that you can begin using EWARS.</p>
                    <p>Thank you for registering.</p>
                </div>
            </div>
        )
    }
});