import {
    Spinner,
    Button
} from "../../common";

class Item extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="block hoverable" onClick={() => {
                this.props.onClick(this.props.data)
            }}>
                <div className="block-content">
                    {ewars.I18N(this.props.data.name)}
                </div>
            </div>
        )
    }
}

function isNull(value) {
    if (value == null) return true;
    if (value == undefined) return true;
    if (value == "") return true;
    return false;
}

class Account extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            search: "",
            data: null
        }
    }

    componentWillMount() {
        this._query(this.props.resource);
    }

    componentWillReceiveProps(props) {
        this._query(this.props.resource);
    }

    _query = (resource, param) => {
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "/registration", true);
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        oReq.onreadystatechange = function () {
            let DONE = 4;
            let OK = 200;

            if (oReq.readyState == DONE) {
                if (oReq.status == OK) {
                    this.setState({
                        data: JSON.parse(oReq.responseText)
                    })
                } else {
                }
            }
        }.bind(this);

        oReq.send(JSON.stringify({
            action: resource,
            param: isNull(param) ? null : param
        }))
    };

    _onClick = (item) => {
        this.props.onSelect(item);
    };

    _searchChange = (e) => {
        this.setState({
            search: e.target.value
        });

        if (this.state.search.length > 2) {
            this._query(this.props.resource, e.target.value);
        }
    };

    _clSearch = () => {
        this.setState({search: ""});
        this._query(this.props.resource, "");
    };

    render() {

        let items;

        if (!this.state.data) items = <Spinner/>;
        if (this.state.data) {
            items = this.state.data.map(function (item) {
                return <Item data={item} onClick={this._onClick}/>
            }.bind(this))
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row height="40">
                    <ewars.d.Cell style={{padding: 8}}>
                        <div className="search-wrapper">
                            <div className="ide-row">
                                <div className="ide-col">
                                    <input
                                        placeholder="Search..."
                                        onChange={this._searchChange}
                                        value={this.state.search}
                                        type="text"/>
                                </div>
                                {this.state.search != "" ?
                                    <div className="ide-col" style={{maxWidth: 30, padding: 2}}>
                                        <Button icon="fa-times" onClick={this._clSearch}/>
                                    </div>
                                    : null}
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                {items}
                            </div>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default Account;
