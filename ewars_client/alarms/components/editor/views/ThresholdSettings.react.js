const AGGREGATE_CONSTRAINT = {
    conditional_bool: true,
    conditions: {
        condition: "all",
        rules: [
            ['monitor_type', 'eq', 'AGGREGATE']
        ]
    }
}

const FORM = {
    tt_header: {
        type: "header",
        label: "General"
    },
    monitor_type: {
        type: "button_group",
        label: "Monitoring Type",
        options: [
            ['AGGREGATE', 'Aggregate-based'],
            ['RECORD', 'Record-based']
        ]
    },
    desc_aggregate: {
        type: "content",
        content: "Aggregate type alarms use an aggregation from multiple records for a given source to evaluate an alert. The record date for the form submission which triggered the evaluation is used as the period of data to aggregate. Only a single active alert can exist per location/date.",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ['monitor_type', 'eq', 'AGGREGATE']
            ]
        }
    },
    desc_record: {
        type: "content",
        content: "Record constrained alarms are sandboxed to indicator values coming from the record which triggered the evaluation and will be raised against the records location and date. Multiple alerts can be raised for a single location/date.",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ['monitor_type', 'eq', 'RECORD']
            ]
        }
    },
    t_header: {
        type: "header",
        label: "Monitored Location(s)",
        ...AGGREGATE_CONSTRAINT
    },
    loc_restrict: {
        type: "button_group",
        label: "Restrict Location(s)",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        ...AGGREGATE_CONSTRAINT
    },
    loc_spec: {
        type: "button_group",
        label: "Location Spec.",
        options: [
            ['SPECIFIC', "Specific location"],
            ["GROUPS", "Location Group(s)"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_restrict", "eq", true],
                ["monitor_type", 'eq', 'AGGREGATE']
            ]
        }
    },
    loc_id: {
        type: "location",
        label: "Location",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_restrict", "eq", true],
                ["loc_spec", "eq", "SPECIFIC"],
                ['monitor_type', 'eq', 'AGGREGATE']
            ]
        }
    },
    loc_groups: {
        type: "location_group",
        label: "Location Group(s)",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_restrict", "eq", true],
                ["loc_spec", "eq", "GROUPS"],
                ['monitor_type', 'eq', 'AGGREGATE']
            ]
        }
    },
    sti_restrict: {
        type: "button_group",
        label: "Restrict location type",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        ...AGGREGATE_CONSTRAINT
    },
    sti: {
        type: "select",
        label: "Location type",
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["sti_restrict", "eq", true],
                ['monitor_type', 'eq', 'AGGREGATE']
            ]

        }
    },
    sti_agg_rollup: {
        type: "button_group",
        label: "Evaluate at location type",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        help: "Roll up data to this location type",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ['sti_restrict', 'eq', true],
                ['monitor_type', 'eq', 'AGGREGATE']
            ]
        }
    },
    t_header_ds: {
        type: "header",
        label: "Monitored data"
    },
    ds_interval_type: {
        type: "button_group",
        label: "Data Source Interval Source",
        options: [
            ["TRIGGER_PERIOD", "Report Period"],
            ["RANGE", "Range"]
        ],
        ...AGGREGATE_CONSTRAINT
    },
    ds_interval: {
        type: "button_group",
        label: "Data Source Interval",
        options: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Year"]
        ],
        ...AGGREGATE_CONSTRAINT
    },
    ds_type: {
        type: "button_group",
        label: "Data Source",
        options: [
            ["INDICATOR", "Indicator"],
            ["COMPLEX", "Complex"]
        ]
    },
    ds_indicator: {
        type: "indicator",
        label: "Source Indicator",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["ds_type", "eq", "INDICATOR"]
            ]
        }
    },
    ds_formula: {
        type: "text",
        label: "Formula",
        nameOverride: 'ds_indicator.ds_formula',
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["ds_type", "eq", "COMPLEX"]
            ]
        }
    },
    ds_sources: {
        type: '_complex_sources',
        nameOverride: 'ds_indicator.ds_sources',
        label: {en: "Variables"},
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["ds_type", "eq", "COMPLEX"]
            ]
        }
    },
    ds_agg_lower: {
        type: "button_group",
        label: "Aggregate lower levels",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        ...AGGREGATE_CONSTRAINT
    },
    ds_aggregate: {
        type: "button_group",
        label: "Aggregation",
        options: [
            ["SUM", "Sum"],
            ["AVG", "Average/Median"]
        ]
    },
    ds_modifier: {
        type: "number",
        label: "Modifier"
    },
    crit_header: {
        type: "header",
        label: "Alert criteria"
    },
    crit_comparator: {
        type: "button_group",
        label: "Comparator",
        options: [
            ["GT", ">"],
            ["GTE", ">="],
            ["LT", "<"],
            ["LTE", "<="],
            ["EQ", "="],
            ["NEQ", "!="]
        ]
    },
    crit_source: {
        type: "button_group",
        label: "Criteria Data Source",
        options: [
            ["VALUE", "Static Value"],
            ["DATA", "Indicator"]
        ]
    },
    crit_sd_spec: {
        type: "select",
        label: "Start date specification",
        options: [
            ["INTERVALS_BACK", "Intervals Back from end date"],
            ["EVALUATION_DATE", "Report date"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "DATA"]
            ]
        }
    },
    crit_sd_intervals: {
        type: "number",
        label: "Start Date Intervals",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "DATA"],
                ["crit_sd_spec", "eq", "INTERVALS_BACK"]
            ]
        }
    },
    crit_ed_spec: {
        type: "select",
        label: "End date specification",
        options: [
            ["EVALUATION_DATE", "Report Date"],
            ["INTERVALS_BACK", "Intervals back from report date"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "DATA"]
            ]
        }
    },
    crit_ed_intervals: {
        type: "number",
        label: "End Date Intervals",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "DATA"],
                ["crit_ed_spec", "eq", "INTERVALS_BACK"]
            ]
        }
    },
    crit_indicator: {
        type: "indicator",
        label: "Indicator Source",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "DATA"]
            ]
        }
    },
    crit_formula: {
        type: "text",
        i18n: false,
        label: "Formula",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "COMPLEX"]
            ]
        }
    },
    crit_series: {
        type: "text",
        label: "Sources",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "COMPLEX"]
            ]
        }
    },
    crit_reduction: {
        type: "button_group",
        label: "Reduction",
        options: [
            ["SUM", "Sum"],
            ["AVG", "Average/Median"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "DATA"]
            ]
        }
    },
    crit_modifier: {
        type: "number",
        label: "Modifier",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "DATA"]
            ]
        },
        help: "The aggregate value will be multiplied by this value before comparison"
    },
    crit_value: {
        type: "number",
        label: "Value",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["crit_source", "eq", "VALUE"]
            ]
        }
    },
    crit_floor: {
        type: 'number',
        label: 'Floor',
        conditional_bool: true,
        conditions: {
            application: 'all',
            rules: [
                ['crit_source', 'eq', 'DATA']
            ]
        }
    }
};

class ThresholdSettings extends React.Component {
    constructor(props) {
        super(props);
    }

    _update = (data, prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        return (
            <ewars.d.Panel>
                <div className="ide-settings-content">
                    <div className="ide-settings-header">
                        <div className="ide-settings-icon"><i className="fal fa-eye"></i></div>
                        <div className="ide-settings-title">Monitoring</div>
                    </div>
                    <div className="ide-settings-intro">
                        <p>Configure how, where and when this alarm generates alerts.</p>
                    </div>
                    <div className="ide-settings-form">
                        <ewars.d.Form
                            definition={FORM}
                            data={this.props.data}
                            readOnly={false}
                            updateAction={this._update}/>
                    </div>
                </div>
            </ewars.d.Panel>
        )
    }
}

export default ThresholdSettings;

