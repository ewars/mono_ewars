import { SwitchField } from "../../../../common";

var ActionsSettings = React.createClass({
    getInitialState: function () {
        return {}
    },

    _update: function (prop, value) {
        var actions = JSON.parse(JSON.stringify(this.props.data.actions));
        actions[prop] = value;
        this.props.onChange("actions", actions);
    },

    render: function () {
        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                    <div className="ide-settings-title">Trigger Settings</div>
                </div>
                <div className="ide-settings-intro">
                    <p>What actions should be taken when this alarm is triggered</p>
                </div>
                <div className="ide-settings-form">

                    <div className="ide-section-basic">
                        <div className="header">Actions</div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Generate Alert</div>
                            <div className="ide-setting-control">
                                <SwitchField
                                    name="generate_alert"
                                    value={this.props.data.actions.generate_alert}
                                    onUpdate={this._update}/>
                            </div>
                        </div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Notify Account Admins (Email)</div>
                            <div className="ide-setting-control">
                                <SwitchField
                                    name="generate_email_admin"
                                    value={this.props.data.actions.generate_email_admin}
                                    onUpdate={this._update}/>
                            </div>
                        </div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Notify Account Admins (SMS)</div>
                            <div className="ide-setting-control">
                                <SwitchField
                                    name="generate_sms_admin"
                                    value={this.props.data.actions.generate_sms_admin}
                                    onUpdate={this._update}/>
                            </div>
                        </div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Notify Report Submitter (Email)</div>
                            <div className="ide-setting-control">
                                <SwitchField
                                    name="generate_email_submitter"
                                    value={this.props.data.actions.generate_email_submitter}
                                    onUpdate={this._update}/>
                            </div>
                        </div>

                        <div className="hsplit-box">
                            <div className="ide-setting-label">Notify Report Submitter (SMS)</div>
                            <div className="ide-setting-control">
                                <SwitchField
                                    name="generate_sms_submitter"
                                    value={this.props.data.actions.generate_sms_submitter}
                                    onUpdate={this._update}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default ActionsSettings;
