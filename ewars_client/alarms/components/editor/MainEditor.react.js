import {
    Filler,
    VertTab
} from "../../../common";

import GeneralSettings from "./views/GeneralSettings.react";
import ThresholdSettings from './views/ThresholdSettings.react';

import FormAssociations from "./views/FormAssociations.react";
import InvestigationSettings from "./views/InvestigationSettings.react";
import Recovery from "./views/Recovery.react";
import Inttegrations from "./views/Integrations.react";
import Administration from './views/Administration';

import defaults from "../../constants/defaults";

const CONSTANTS = {
    GENERAL: "GENERAL",
    THRESHOLD: "THRESHOLD",
    GUIDANCE: "GUIDANCE",
    INVESTIGATION: "INVESTIGATION",
    RECOVERY: "RECOVERY",
    INTEGRATIONS: "INTEGRATIONS"
};

var SettingsSection = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-settings-section";
        if (this.props.active) className += " ide-settings-section-active";

        var subs = [];
        if (this.props.subs) {
            this.props.subs.map(sub => {
                return (
                    <div className="section-item">{sub}</div>
                )
            });
        }

        var hasSubs = subs.length > 0;

        return (
            <div className={className}>
                <div className="section-header" onClick={this._onClick}>
                    <div className="section-title">{this.props.label}</div>
                </div>
                {hasSubs ?
                    <div className="section-items">
                        {subs}
                    </div>
                    : null}
            </div>
        )
    }
});


var MainEditor = React.createClass({
    getInitialState: function () {
        return {
            view: CONSTANTS.GENERAL,
            alarm: null,
            isDirty: false
        }
    },

    componentWillMount: function () {
        if (this.props.data.uuid) {
            var blocker = new ewars.Blocker(null, "Loading Alarm...");
            ewars.tx("com.ewars.resource", ["alarm", this.props.data.uuid, null, null])
                .then(function (resp) {
                    if (resp.crit_indicator) {
                        if (resp.crit_indicator.indexOf("uuid") >= 0) resp.crit_indicator = JSON.parse(resp.crit_indicator);
                    }

                    this.state.alarm = ewars.copy(resp);
                    this._init();
                    blocker.destroy();
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        } else {
            this.state.alarm = this.props.data;
        }
    },

    componentWillUnmount: function () {
        ewars.g.activeField = null;
    },

    _init: function () {
        if (!this.state.alarm.outcomes) this.state.alarm.outcomes = [];
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.data.uuid != this.props.data.uuid) {
            this.state.view = CONSTANTS.GENERAL;
            let blocker = new ewars.Blocker(null, "Loading alarms...");
            ewars.tx("com.ewars.resource", ["alarm", nextProps.data.uuid, null, null])
                .then(function (resp) {
                    if (resp.ds_indicator) {
                        if (resp.ds_indicator.indexOf("uuid") >= 0) resp.ds_indicator = JSON.parse(resp.ds_indicator);
                    }
                    if (resp.crit_indicator) {
                        if (resp.crit_indicator.indexOf("uuid") >= 0) resp.crit_indicator = JSON.parse(resp.crit_indicator);
                    }


                    this.state.alarm = ewars.copy(resp);
                    blocker.destroy();
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        } else {
            this.state.alarm = nextProps.data;
        }
    },

    _onPropsChange: function (prop, value) {
        console.log(prop, value, this.state.alarm);

        if (['ds_formula', 'ds_sources'].indexOf(prop) >= 0) {
            if (this.state.alarm.ds_indicator) {
                if (JSON.stringify(this.state.alarm.ds_indicator).indexOf('{') >= 0) {
                    // we have an object already
                    this.state.alarm.ds_indicator[prop] = value;
                } else {
                    this.state.alarm.ds_indicator = {};
                    this.state.alarm.ds_indicator[prop] = value;
                }
            } else {
                this.state.alarm.ds_indicator = {
                    [prop]: value
                };
            }

        } else {
            this.state.alarm[prop] = value;
        }
        console.log(this.state.alarm);
        this.forceUpdate();
    },

    _changeSection: function (view) {
        this.state.view = view;
        this.forceUpdate();
    },

    _save: function () {
        if (this.state.alarm.uuid) {
            var blocker = new ewars.Blocker(null, "Updating alarm...");
            ewars.tx("com.ewars.alarm.update", [this.state.alarm.uuid, this.state.alarm])
                .then(resp => {
                    blocker.destroy();
                    ewars.growl("Alarm updated");
                    this.setState({
                        alarm: resp
                    })
                })
        } else {
            var blocker = new ewars.Blocker(null, "Creating alarm...");
            ewars.tx("com.ewars.alarm.create", [this.state.alarm])
                .then(resp => {
                    blocker.destroy();
                    ewars.growl("Alarm created");
                    this.setState({
                        alarm: resp
                    })
                })
        }
    },

    render: function () {
        let view;

        switch (this.state.view) {
            case CONSTANTS.GENERAL:
                view = (
                    <GeneralSettings
                        data={this.state.alarm}
                        onChange={this._onPropsChange}/>
                );
                break;
            case CONSTANTS.THRESHOLD:
                view = (
                    <ThresholdSettings
                        data={this.state.alarm}
                        onChange={this._onPropsChange}/>
                );
                break;
            case CONSTANTS.INVESTIGATION:
                view = (
                    <InvestigationSettings
                        data={this.state.alarm}
                        onChange={this._onPropsChange}/>
                );
                break;
            case CONSTANTS.RECOVERY:
                view = (
                    <Recovery
                        data={this.state.alarm}
                        onChange={this._onPropsChange}/>
                )
                break;
            case CONSTANTS.INTEGRATIONS:
                view = (
                    <Integrations
                        data={this.state.alarm}/>
                )
                break;
            case 'ADMIN':
                view = (
                    <Administration
                        data={this.state.alarm}/>
                )
                break;
            default:
                break;
        }

        if (!this.state.alarm) return <Filler icon="fa-bell"/>;


        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar icon="fa-bell" label="Alarm Editor">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"
                            color="green"
                            label={_l("SAVE_CHANGES")}
                            onClick={this._save}/>
                        <ewars.d.Button
                            icon="fa-times"
                            color="amber"
                            onClick={this.props.onClose}
                            label={_l("CLOSE")}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="34px" style={{position: "relative", overflow: "hidden"}}>
                        <div className="ide-tabs">
                            <VertTab label="General" icon="fa-cog" active={this.state.view == "GENERAL"} onClick={() => this._changeSection("GENERAL")}/>
                            <VertTab label="Monitoring" icon="fa-chart-line" active={this.state.view == "THRESHOLD"} onClick={() => this._changeSection("THRESHOLD")}/>
                            <VertTab label="Auto-Discard" icon="fa-trash" active={this.state.view == "RECOVERY"} onClick={() => this._changeSection("RECOVERY")}/>
                            <VertTab label="Investigation" icon="fa-search" active={this.state.view == "INVESTIGATION"} onClick={() => this._changeSection("INVESTIGATION")}/>
                            <VertTab label="Administration" icon="fa-wrench" active={this.state.view == "ADMIN"} onClick={() => this._changeSection("ADMIN")}/>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
});

export default MainEditor;
