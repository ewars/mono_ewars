import { ContextMenu } from "../../../common";

var AlarmNode = React.createClass({
    getInitialState: function () {
        return {
            showOptions: false
        }
    },

    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    _showOptions: function (e) {
        e.stopPropagation();
        e.preventDefault();
        this.state.showOptions = true;
        this.forceUpdate();

        $('body').on("click", this._hideOptions);
    },

    _hideOptions: function () {
        this.state.showOptions = false;
        this.forceUpdate();
        $('body').off("click", this._hideOptions);
    },

    _delete: function () {
        ewars.prompt("fa-trash", "Delete Alarm?", "Are you sure you want to delete this alarm?", function () {
            var blocker = new ewars.Blocker(null, "Deleting Alarm....");
        }.bind(this))
    },

    render: function () {
        var alarmName = ewars.I18N(this.props.data.name);

        var iconStyle = {
            color: "#333",
            fontSize: 10,
            paddingTop: 3,
            textAlign: "center",
            width: "100%",
            height: "100%",
            lineHeight: 0
        };

        let className = "block-content";
        if (this.props.data.status) className += " status-green";
        if (!this.props.data.status) className += " status-red";

        return (
            <div className="block" onClick={this._onClick}>
                <div className={className}>
                    {alarmName}
                </div>
            </div>
        )
    }
});

var AlarmTreeNode = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            alarms: [],
            isCollapsed: true
        }
    },

    componentWillMount: function () {
        ewars.subscribe("RELOAD_ALARMS", this._onReload);
    },

    _onReload: function () {
        if (this._hasLoaded) {
            var query = {
                account_id: {eq: this.props.data.id}
            };

            ewars.tx("com.ewars.query", ["alarm", ['uuid', 'name', 'status'], query, {"name.en": "ASC"}, null, null, null])
                .then(function (resp) {
                    this.state.alarms = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    onOpen: function () {
        var query = {
            account_id: {eq: this.props.data.id}
        };

        ewars.tx("com.ewars.query", ["alarm", ['uuid', 'name', 'status'], query, {"name.en": "ASC"}, null, null, null])
            .then(function (resp) {
                this._hasLoaded = true;
                this.state.alarms = resp;
                this.state.isCollapsed = false;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onCaretClick: function () {
        if (this._hasLoaded) {
            this.state.isCollapsed = this.state.isCollapsed ? false : true;
            this.forceUpdate();
        } else {
            this.onOpen();
        }
    },

    render: function () {
        var accountName = ewars.formatters.I18N_FORMATTER(this.props.data.name);

        var alarms;
        if (this._hasLoaded && this.state.alarms) {
            alarms = _.map(this.state.alarms, function (alarm) {
                return <AlarmNode data={alarm} onClick={this.props.onEdit}/>
            }, this);
        }

        let iconClass = "fa fa-folder";
        if (!this.state.isCollapsed) iconClass = "fa fa-folder-open";

        return (
            <div className="block">
                <div className="block-content" onClick={this._onCaretClick}>
                    <div className="ide-layout">
                        <div className="ide-row">
                            <div className="ide-col" style={{maxWidth: 20}}>
                                <i className={iconClass}></i>
                            </div>
                            <div className="ide-col">{accountName}</div>
                        </div>
                    </div>
                </div>
                {!this.state.isCollapsed ?
                    <div className="block-children">
                        {alarms}
                    </div>
                    : null}
            </div>
        )
    }

});

var AlarmsTree = React.createClass({
    getInitialState: function () {
        return {
            accounts: []
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.query", ["account", ["id", "name"], {status: {eq: 'ACTIVE'}}, {"name": "ASC"}, null, null, null])
            .then(function (resp) {
                this.state.accounts = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    render: function () {
        var accounts = _.map(this.state.accounts, function (account) {
            return <AlarmTreeNode data={account} onEdit={this.props.onEdit}/>;
        }, this);

        return (
            <ewars.d.Panel>
                <div className="block-tree">
                    {accounts}
                </div>
            </ewars.d.Panel>
        )
    }
});

export default AlarmsTree;
