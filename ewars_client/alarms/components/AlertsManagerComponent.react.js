import { DataTable } from "../../common";

import MainEditor from "./editor/MainEditor.react";
import defaults from "../constants/defaults";

function boolFmt(value) {
    if (value) return "Active";
    if (!value) return "Inactive"
}

const SOURCE_OPTIONS = [
    ["INDICATOR", _l("INDICATOR")],
    ["COMPLEX", _l("COMPLEX")]
];

const CMP_SOURCE = [
    ["VALUE", "Static Value"],
    ["DATA", _l("DATA")]
];

const COLUMNS = [
    {
        name: "name",
        config: {
            type: "text",
            label: "Name"
        },
        fmt: ewars.I18N
    },
    {
        name: "status",
        config: {
            type: "select",
            options: [
                ["ACTIVE", "Active"],
                ["INACTIVE", "Inactive"]
            ],
            label: "Status"
        }
    },
    {
        name: "created",
        config: {
            type: "date",
            label: "Created"
        },
        fmt: ewars.DATE
    },
    {
        name: "last_modified",
        config: {
            type: "date",
            label: "Last Modified"
        },
        fmt: ewars.DATE
    },
    {
        name: "user.name",
        config: {
            type: "user",
            label: "Created By",
            filterKey: "created_by"
        }
    },
    {
        name: "ds_type",
        config: {
            type: "select",
            label: "Source Type",
            options: SOURCE_OPTIONS
        }
    },
    {
        name: "crit_source",
        config: {
            type: "select",
            label: "Comparator Source",
            options: CMP_SOURCE
        }
    },
    {
        name: "crit_comparator",
        config: {
            label: "Comparator"
        }
    },
    {
        name: "crit_value",
        config: {
            label: "Criteria Value"
        }
    }
];

const ACTIONS = [
    {label: "Edit", icon: "fa-pencil", action: "EDIT"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"}
];

const JOINS = [
    "user:created_by:id:name,email"
];

var AlertsManagerComponent = React.createClass({
    getInitialState: function () {
        return {
            view: "DEFAULT"
        }
    },

    _deleteAlarm: function (alarm) {
        ewars.prompt("fa-trash", "Delete Alarm?", "Are you sure you want to delete this alarm?", function () {
            ewars.emit("RELOAD_ALARMS");
        });
    },

    _cancelEdit: function () {
        this.setState({
            alarm: null,
            view: "DEFAULT"
        });
        ewars.emit("RELOAD_DT");
    },

    _onEditAlarm: function (alarm) {
        this.setState({
            alarm: alarm,
            view: "EDIT"
        })
    },

    _onNewAlarm: function () {
        this.setState({
            alarm: _.extend({}, defaults),
            view: "EDIT"
        })
    },

    _onCellAction: function (action, cell, data) {
        if (action == "EDIT") this._onEditAlarm(data);
        if (action == "CLICK") this._onEditAlarm(data);

        if (action == "DELETE") {
            this._deleteAlarm(data);
        }
    },

    _deleteAlarm: function (alarm) {
        ewars.prompt("fa-trash", "Delete Alarm?", "Are you sure you want to delete this alarm? Any alerts and alert data associated with this alarm will be lost.", function () {
            let bl = new ewars.Blocker(null, "Deleting alarm...");
            ewars.tx("com.ewars.alarm.delete", [alarm.uuid])
                .then((resp) => {
                    bl.destroy();
                    ewars.growl("Alarm deleted");
                    ewars.emit("RELOAD_DT");
                })
                .catch((err) => {
                    bl.destroy();
                    ewars.emit("RELOAD_DT");
                    ewars.growl("An error occurred");
                })

        })
    },

    render: function () {

        if (this.state.view == "DEFAULT") {
            return (
                <ewars.d.Layout>
                    <ewars.d.Toolbar title="Alarms">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                label="New"
                                icon="fa-plus"
                                onClick={this._onNewAlarm}/>
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <DataTable
                                onCellAction={this._onCellAction}
                                resource="alarm"
                                join={JOINS}
                                columns={COLUMNS}
                                actions={ACTIONS}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            )
        } else {
            return (
                <MainEditor
                    onClose={this._cancelEdit}
                    data={this.state.alarm}/>
            )
        }

    }
});

export default AlertsManagerComponent;
