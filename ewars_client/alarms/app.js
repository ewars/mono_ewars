import Component from "./components/AlertsManagerComponent.react";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Toolbar,
    ActionGroup,
    Panel,
    Form
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Modal,
    Button,
    Toolbar,
    ActionGroup,
    Panel,
    Form
};

ReactDOM.render(
    <Component/>,
    document.getElementById('application')
)


