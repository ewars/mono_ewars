class ContextMenuItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.props.onClick(this.props.data.action);
    };

    render() {
        let iconClass = `fal ${this.props.data.icon}`;

        return (
            <div onClick={this._onClick} className="context-menu-item">
                <div className="ide-row">
                    <div className="ide-col" style={{maxWidth: 25}}><i className={iconClass}></i></div>
                    <div className="ide-col">{this.props.data.label}</div>
                </div>
            </div>
        )
    }
}

class ContextMenu extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = (action) => {
        this.props.onClick(action);
    };

    render() {
        let style = {};
        if (!this.props.absolute) style.position = "relative";

        let actions = this.props.actions.map(function (action) {
            return <ContextMenuItem
                key={action.action}
                data={action}
                onClick={this._onClick}/>
        }.bind(this));

        return (
            <div className="context-menu" style={style}>
                {actions}
            </div>
        )
    }
}

export default ContextMenu;