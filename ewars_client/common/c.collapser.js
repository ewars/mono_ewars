export default class Collapser extends React.Component {
    static defaultProps = {
        open: false
    };

    render() {
        return (
            <div className="collapser" onClick={this.props.onClick}>
                <i className="fal fa-caret-right"></i>
            </div>
        )
    }
}