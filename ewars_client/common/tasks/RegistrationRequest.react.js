import {
    SelectField,
    TextAreaField,
    LocationField as LocationSelectField,
    DisplayField
} from "../fields";
import ButtonGroup from "../c.button_group";
import Modal from "../c.modal";
import Form from "../c.form";


const ROLE_OPTIONS = [
    ["USER", _l("USER")],
    ["ACCOUNT_ADMIN", _l("ACCOUNT_ADMIN")],
    ["REGIONAL_ADMIN", _l("REGIONAL_ADMIN")]
];

const ORG_OPTIONS = {
    optionsSource: {
        resource: "organization",
        valSource: "uuid",
        labelSource: "name",
        query: {},
        prefixed: [
            ["NEW", "New organization"]
        ]
    }
};

const STATUS_OPTIONS = [
    ["ACTIVE", _l("ACTIVE")],
    ["INACTIVE", _l("INACTIVE")]
];

var Details = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        var desc = this.props.data.user_name + " has requested an account:";

        let org_value = this.props.data.org_id;
        if (this.props.data.organization_name) {
            if (this.props.data.organization_name != "") org_value = "NEW";
        }

        return (

            <div className="ide-settings-content">

                <div className="ide-settings-form">

                    <div className="form light">
                        <div className="form-field header">
                            <div className="label"><label>User</label></div>
                            <div className="field-wrapper">
                                <div className="form-header style-title">User Details</div>
                            </div>
                        </div>
                    </div>

                    <div className="form-field">
                        <div className="label"><label htmlFor="">Name</label></div>
                        <div className="field-wrapper">
                            <input type="text" disabled={true} value={this.props.data.user_name}/>
                        </div>
                    </div>

                    <div className="form-field">
                        <div className="label"><label htmlFor="">Email</label></div>
                        <div className="field-wrapper">
                            <input type="text" disabled={true} value={this.props.data.email}/>
                        </div>
                    </div>

                    <div className="form-field">
                        <div className="label"><label htmlFor="">Organization</label></div>
                        <div className="field-wrapper">
                            <SelectField
                                value={org_value}
                                onUpdate={(prop, value) => {
                                    this.props.onChange("org_id", value);
                                }}
                                readOnly={false}
                                config={ORG_OPTIONS}/>
                        </div>
                    </div>

                    {this.props.data.organization_name ?
                        <div className="form-field">
                            <div className="label">
                                <label htmlFor="">New organization name</label>
                            </div>
                            <div className="field-wrapper">
                                <input
                                    onChange={(e) => {
                                        this.props.onChange("organization_name", e.target.value);
                                    }}
                                    value={this.props.data.organization_name}
                                    type="text"/>
                            </div>
                        </div>
                        : null}

                    <div className="form light">
                        <div className="form-field header">
                            <div className="label"><label>User</label></div>
                            <div className="field-wrapper">
                                <div className="form-header style-title">Access Details</div>
                            </div>
                        </div>
                    </div>

                    <div className="form-field">
                        <div className="label"><label htmlFor="">Role *</label></div>
                        <div className="field-wrapper">
                            <ButtonGroup
                                name="role"
                                config={{options: ROLE_OPTIONS}}
                                value={this.props.data.role}
                                onUpdate={this.props.onChange}/>
                        </div>
                    </div>

                    <div className="form-field">
                        <div className="label"><label htmlFor="">Status *</label></div>
                        <div className="field-wrapper">
                            <ButtonGroup
                                name="status"
                                config={{options: STATUS_OPTIONS}}
                                value={this.props.data.status}
                                onUpdate={this.props.onChange}/>
                        </div>
                    </div>

                    {this.props.data.role == "REGIONAL_ADMIN" ?
                        <div className="form-field">
                            <div className="label"><label htmlFor="">Location *</label></div>
                            <div className="field-wrapper">
                                <LocationSelectField
                                    name="location_id"
                                    value={this.props.data.location_id}
                                    onUpdate={this.props.onChange}/>
                            </div>
                        </div>
                        : null}


                </div>
            </div>
        )
    }
});

var Reject = React.createClass({
    getInitialState: function () {
        return {
            reason: null
        }
    },

    _change: function (e) {
        this.props.onChange(e.target.value);
    },

    render: function () {
        return (
            <div className="content">
                <p>You are rejecting this user registration, please provide a reason the request is being rejected and
                    EWARS will notify the requestor.</p>
                <label>Rejection Reason</label>
                <textarea value={this.props.rejection_reason} onChange={this._change}/>
            </div>
        )
    }
});

let DEFAULTS = {
    role: "USER",
    status: "ACTIVE",
    location_id: null
};

var RegistrationRequest = React.createClass({
    getInitialState: function () {
        return {
            view: "DEFAULT",
            rejection_reason: null,
            data: {},
            error: null
        }
    },

    _close: function () {
        this.props.onClose();
    },

    componentWillMount: function () {
        this.state.data = Object.assign(this.state.data, this.props.data.data, DEFAULTS);
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.data = Object.assign(this.state.data, nextProps.data.data, DEFAULTS);
    },

    _onFormChange: function (prop, value) {
        let org_name = this.state.data.organization_name;
        if (prop == "org_id" && value != 'NEW') {
            org_name = null;
        }
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                organization_name: org_name,
                [prop]: value
            }
        });
    },

    _approve: function () {
        //TODO: Validate editable fields

        if (!this.state.data.role || this.state.data.role == "") {
            this.setState({error: "Please specify a valid role"});
            return;
        }

        if (!this.state.data.status || this.state.data.status == "") {
            this.setState({error: "Please specify a valid status"});
            return;
        }

        if (this.state.data.role == "REGIONAL_ADMIN") {
            if (!this.state.data.location_id || this.state.data.location_id == "") {
                this.setState({error: "Please specify a location for this Geographic Administrator"});
                return;
            }
        }

        var blocker = new ewars.Blocker(null, "Processing...");

        ewars.tx("com.ewars.task.action", [this.props.data.id, "APPROVE", this.state.data])
            .then(function (resp) {
                blocker.destroy();
                this._close();
            }.bind(this))
    },

    _reject: function () {
        this.state.view = "REJECT";
        this.forceUpdate();
    },

    _onRejectChange: function (reason) {
        this.state.rejection_reason = reason;
        this.forceUpdate();
    },

    _resetView: function () {
        this.state.rejection_reason = null;
        this.state.view = 'DEFAULT';
        this.forceUpdate();
    },

    _submitRejection: function () {
        var blocker = new ewars.Blocker(null, "Processing...");
        ewars.tx("com.ewars.task.action", [this.props.data.id, "REJECT", {"reason": this.state.rejection_reason}])
            .then(function (resp) {
                blocker.destroy();
                this._close();
            }.bind(this))
    },

    render: function () {
        var buttons = [];
        if (this.state.view == "DEFAULT") {
            buttons = [
                {label: "Approve", onClick: this._approve, icon: 'fa-check', colour: "green"},
                {label: "Reject", onClick: this._reject, icon: "fa-ban", colour: "red"},
                {label: "Close", onClick: this._close, icon: 'fa-times'}
            ]
        }

        if (this.state.view == "REJECT") {
            buttons = [
                {label: "Back", icon: "fa-times", onClick: this._resetView},
                {label: "Submit", icon: "fa-check", onClick: this._submitRejection, colour: "green"}
            ]
        }

        var view;
        if (this.state.view == "DEFAULT") view = <Details data={this.state.data} onChange={this._onFormChange}/>;
        if (this.state.view == "REJECT") {
            view = <Reject
                data={this.props.data}
                rejection_reason={this.state.rejection_reason}
                onChange={this._onRejectChange}/>;
        }

        let error;
        if (this.state.error) {
            error = (
                <div className="widget">
                    <div className="body">
                        <div className="error"><i className="fal fa-exclamation-triangle"></i>&nbsp;{this.state.error}
                        </div>
                    </div>
                </div>
            )
        }

        let toolbar = (
            <ewars.d.Toolbar>
                <div className="btn-group pull-right">
                    <ewars.d.Button
                        label="Approve"
                        color="green"
                        icon="fa-check"
                        onClick={this._approve}/>
                    <ewars.d.Button
                        label="Reject"
                        color="red"
                        icon="fa-times"
                        onClick={this._reject}/>
                </div>
            </ewars.d.Toolbar>
        );

        if (this.state.view == "REJECT") {
            toolbar = (
                <ewars.d.Toolbar>
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            label="Reject"
                            color="green"
                            icon="fa-check"
                            onClick={this._submitRejection}/>
                        <ewars.d.Button
                            label="Cancel"
                            color="red"
                            icon="fa-times"
                            onClick={this._resetView}/>
                    </div>
                </ewars.d.Toolbar>
            )
        }

        return (
            <Modal
                buttons={buttons}
                title="Registration Request"
                icon="fa-user-plus"
                onAction={this._action}
                visible={true}>
                <div style={{padding: "8px"}}>
                    {error}
                    {view}
                </div>
            </Modal>
        )
    }
});

export default RegistrationRequest;
