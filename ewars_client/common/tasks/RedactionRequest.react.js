import Modal from "../c.modal";

var Details = React.createClass({
    getInitialState: function () {
        return {}
    },

    _openReport: function () {
        window.open("/reporting#?uuid=" + this.props.data.data.report_uuid);
    },

    render: function () {
        var desc = this.props.data.data.user_name + " has requested deletion of the following report:";

        var formName = ewars.I18N(this.props.data.data.form_name);
        var locationName = ewars.I18N(this.props.data.data.location_name);
        var reportDate = ewars.formatters.DATE_FORMATTER(this.props.data.data.report_date, this.props.data.data.form_interval);

        return (
            <div className="content">
                <p>{desc}</p>

                <table className="info-list">
                    <tr>
                        <th>Report Type</th>
                        <td>{formName}</td>
                    </tr>
                    <tr>
                        <th>Location</th>
                        <td>{locationName}</td>
                    </tr>
                    <tr>
                        <th>Report Date</th>
                        <td>{reportDate}</td>
                    </tr>
                    <tr>
                        <th>Reason</th>
                        <td>{this.props.data.data.reason}</td>
                    </tr>
                </table>

                <div className="clearer"></div>

                <div className="btn-group">
                    <ewars.d.Button
                        label="View Report"
                        onClick={this._openReport}/>
                </div>
            </div>
        )
    }
});

var Reject = React.createClass({
    getInitialState: function () {
        return {
            reason: null
        }
    },

    _change: function (e) {
        this.props.onChange(e.target.value);
    },

    render: function () {
        return (
            <div className="content">
                <p>You are rejecting this deletion request, please provide a reason the request is being rejected and
                    EWARS will notify the requestor.</p>
                <label>Rejection Reason</label>
                <textarea value={this.props.rejection_reason} onChange={this._change}/>
            </div>
        )
    }
});

var RedactionRequest = React.createClass({
    getInitialState: function () {
        return {
            view: "DEFAULT",
            rejection_reason: null
        }
    },

    _close: function () {
        this.props.onClose();
    },

    _approve: function () {
        var blocker = new ewars.Blocker(null, "Processing...");

        ewars.tx("com.ewars.task.action", [this.props.data.id, "APPROVE", null])
            .then(function (resp) {
                blocker.destroy();
                this._close();
            }.bind(this))
    },

    _reject: function () {
        this.state.view = "REJECT";
        this.forceUpdate();
    },

    _onRejectChange: function (reason) {
        this.state.rejection_reason = reason;
        this.forceUpdate();
    },

    _resetView: function () {
        this.state.rejection_reason = null;
        this.state.view = 'DEFAULT';
        this.forceUpdate();
    },

    _submitRejection: function () {
        var blocker = new ewars.Blocker(null, "Processing...");
        ewars.tx("com.ewars.task.action", [this.props.data.id, "REJECT", {"reason": this.state.rejection_reason}])
            .then(function (resp) {
                blocker.destroy();
                this._close();
            }.bind(this))
    },

    render: function () {
        var buttons = [];
        if (this.state.view == "DEFAULT") {
            buttons = [
                {label: "Approve", onClick: this._approve, icon: 'fa-check'},
                {label: "Reject", onClick: this._reject, icon: "fa-ban"},
                {label: "Close", onClick: this._close, icon: 'fa-times'}
            ]
        }

        if (this.state.view == "REJECT") {
            buttons = [
                {label: "Back", icon: "fa-times", onClick: this._resetView},
                {label: "Submit", icon: "fa-check", onClick: this._submitRejection}
            ]
        }

        var view;
        if (this.state.view == "DEFAULT") view = <Details data={this.props.data}/>;
        if (this.state.view == "REJECT") view =
            <Reject data={this.props.data} rejection_reason={this.state.rejection_reason}
                    onChange={this._onRejectChange}/>;


        return (
            <Modal
                buttons={buttons}
                title="Report Deletion Request"
                icon="fa-trash"
                visible={true}>
                <div className="ide-panel" style={{padding: 14}}>
                {view}

                </div>
            </Modal>
        )
    }
});

export default RedactionRequest;
