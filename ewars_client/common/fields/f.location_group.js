import SelectField from "./f.select";
import DisplayField from "./f.display";

const STYLE = {
    display: "inline-block",
    background: "#F2F2F2",
    borderRadius: 3,
    padding: 5,
    marginRight: 8
};

class Group extends React.Component {
    render() {
        return (
            <div style={STYLE}>
                <ewars.d.Row>
                    <ewars.d.Cell style={{fontSize: "12px"}}>{this.props.data}
                    </ewars.d.Cell>
                    <ewars.d.Cell width={25} onClick={() => {
                        this.props.onClose(this.props.data)
                    }}>
                        <i className="fal fa-times"></i>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class LocationGroupField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this._getGroups();
    }

    componentWillReceiveProps() {
        if (!this.state.data) {
            this._getGroups();
        }
    }

    _getGroups = () => {
        ewars.tx("com.ewars.location.groups")
            .then((data) => {
                this.setState({
                    data: data
                })
            })
    };

    _onAdd = (prop, value) => {
        let newValue = [];
        if (this.props.value) newValue = ewars.copy(this.props.value);
        if (newValue.indexOf(value) < 0) {
            newValue.push(value);
        }

        this.props.onUpdate(this.props.name, newValue);
    };

    _onRemove = (group) => {
        let index = this.props.value.indexOf(group);
        if (index >= 0) {
            let newValue = ewars.copy(this.props.value);
            newValue.splice(index, 1);
            this.props.onUpdate(this.props.name, newValue);
        }
    };

    render() {
        let fieldConfig = {
            options: this.state.data.map((item) => {
                return [item, item];
            })
        };

        let value = "";
        let arrValue = [];

        if (this.props.value) {
            arrValue = this.props.value;
            value = this.props.value.join(", ")
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row style={{paddingBottom: 8}}>
                    <ewars.d.Cell>
                        <div className="display-field" style={{minHeight: 26}}>
                            {arrValue.map((group) => {
                                return <Group data={group} onClose={this._onRemove}/>
                            })}

                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row style={{marginBottom: 8}}>
                    <ewars.d.Cell>
                        <SelectField
                            config={fieldConfig}
                            value=""
                            onUpdate={this._onAdd}
                            name="addition"/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default LocationGroupField;
