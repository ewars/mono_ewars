import Moment from "moment";

class Week extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onClick(this.props.week);
    };

    render() {


        let weekStart = Moment.utc(this.props.week).subtract(7, 'd');

        let label = ewars.DATE(this.props.week, "WEEK");
        let weekNo = this.props.week.isoWeek();

        let className = "cal-week";

        if (this.props.block_future) {
            if (this.props.week.isAfter(Moment.utc())) className += " inactive";
        }

        return (
            <tr className={className} onClick={this._onClick}>
                <td>
                    <div className="ide-row">
                        <div className="ide-col cal-week-no" style={{maxWidth: 25}}>{weekNo}</div>
                        <div className="ide-col cal-week-label">{label}</div>
                    </div>
                </td>
            </tr>
        )
    }
}

class WeekPicker extends React.Component {
    static defaultProps = {
        offsetAvailability: false
    };

    constructor(props) {
        super(props);

        this.state = {
            curYear: Moment.utc(this.props.value).year() || Moment.utc().year()
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            curYear: Moment.utc(nextProps.value).year() || Moment.utc().year()
        })
    }

    _prevMonth = () => {
        this.setState({
            curYear: this.state.curYear - 1
        })
    };

    _nextMonth = () => {
        this.setState({
            curYear: this.state.curYear + 1
        })
    };

    _onDateSelect = (newDate) => {
        this.props.onChange(newDate);
    };

    render() {

        let isoWeeksInYear = Moment.utc(this.state.curYear).isoWeeksInYear();

        // For each week in the year, generate a list item.
        var weeks2 = [];

        let comparator = Moment.utc();
        if (this.props.offsetAvailability) comparator.add(2, 'days');

        for (var i = 0; i <= isoWeeksInYear; i++) {
            if (this.props.block_future) {
                if (Moment.utc().isoWeekYear(this.state.curYear).isoWeek(i + 1).isoWeekday(7).isBefore(comparator)) {
                    weeks2.push(Moment.utc().isoWeekYear(this.state.curYear).isoWeek(i + 1).isoWeekday(7))
                }
            } else {
                let dt = Moment.utc()
                    .isoWeekYear(this.state.curYear)
                    .isoWeek(i + 1)
                    .isoWeekday(7);

                if (dt.isoWeekYear() == this.state.curYear) {
                    weeks2.push(Moment.utc()
                        .isoWeekYear(this.state.curYear)
                        .isoWeek(i + 1)
                        .isoWeekday(7))
                }
            }
        }

        weeks2.reverse();

        // Given the month of the date, get the isoweeks in the month

        let selected;
        let value;

        let weeks = weeks2.map(function (week) {
            return <Week
                onClick={this._onDateSelect}
                value={selected}
                block_future={this.props.block_future}
                week={week}/>
        }.bind(this));

        return (
            <div className="date-picker">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 30}}>
                        <div className="ide-col">
                            <div className="ide-row">
                                <div className="ide-col cal-left" style={{maxWidth: 25}} onClick={this._prevMonth}>
                                    <i className="fal fa-caret-left"></i>
                                </div>
                                <div className="ide-col cal-header">
                                    {this.state.curYear}
                                </div>
                                <div className="ide-col cal-right" style={{maxWidth: 25}} onClick={this._nextMonth}>
                                    <i className="fal fa-caret-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ide-row" style={{padding: 8}}>
                        <div className="ide-col">
                            <table>
                                <tbody>
                                {weeks}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default WeekPicker;
