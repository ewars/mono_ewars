var FieldNotImplemented = React.createClass({
    getInitialState: function () {
        return {};
    },

    render: function () {
        return (
            <input className="form-control" type="text" value="Field Type Not Implemented" disabled="disabled"/>
        )
    }
});

export default FieldNotImplemented;
