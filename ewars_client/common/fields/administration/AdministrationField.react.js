var AdministrationField = React.createClass({
    _map: null,
    _markers: [],
    _marker: null,

    getDefaultProps: function () {
        return {
            showTiles: true,
            height: 300,
            pointEditMode: false,
            defaultLat: 51.505,
            defaultLng: -0.09
        }
    },

    getInitialState: function () {
        return {};
    },

    componentDidMount: function () {
        if (!this._map) this.renderEditor();
    },

    componentDidUpdate: function () {
        if (!this._map) this.renderEditor();
        if (this._map) this._map.invalidateSize();

        if (this._multiPGroup) {
            this._map.removeLayer(this._multiPGroup);
            this._multiPGroup = null;
            this._map.removeControl(this._drawControl);
            this._drawControl = null;
        }

        if (this.props.value) {
            // Need to determin geometry type
            var value = JSON.parse(this.props.value);

            this._featureGroup = new L.featureGroup().addTo(this._map);
            _.each(value.coordinates[0][0], function (coordSet) {
                coordSet.reverse()
            });

            this._multiPGroup = new L.Polygon(value.coordinates[0][0]).addTo(this._featureGroup);

            this._drawControl = new L.Control.Draw({
                position: 'topright',
                edit: {
                    featureGroup: this._featureGroup
                }
            });
            this._map.addControl(this._drawControl);

        }
    },

    componentWillUnmount: function () {
        if (this._map) {
            this._map.off("click", this.onMapClick);
            this._map = null;
        }
    },

    onChange: function (name, value) {
        this.props.onChange(name, value);
    },

    render: function () {
        if (this.props.height) {
            var style = {
                height: this.props.height + "px"
            }
        }

        return (
            <div className="administration-field">
                <div className="map" style={style}></div>
                {!this.props.readOnly ?
                    <div className="edit-controls">
                        <p>Upload a new geometry for this administration, accpeted files are GeoJSON, TopoJSON and Shapefile (.shp)</p>
                        <input type="file"/>
                    </div>
                    : null}

            </div>
        )
    },

    renderEditor: function () {
        this._map = L.map(this.getDOMNode(), {
            minZoom: 0,
            maxZoom: 20,
            attributionControl: false,
            scrollWheelZoom: (this.props.pointEditMode ? false : true)
        });

        if (this.props.lat && this.props.lng) {
            this._map.setView([this.props.lat, this.props.lng], 1);
        } else {
            this._map.setView([this.props.defaultLat, this.props.defaultLng], 1);
        }

        var tiles = L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'});
        tiles.addTo(this._map);

        if (this.props.value) {
            this._featureGroup = new L.featureGroup().addTo(this._map);
            _.each(value.coordinates[0][0], function (coordSet) {
                coordSet.reverse()
            }, this);

            this._multiPGroup = new L.Polygon(value.coordinates[0][0]).addTo(this._featureGroup);

            this._drawControl = new L.Control.Draw({
                position: 'topright',
                edit: {
                    featureGroup: this._featureGroup
                }
            });
            this._map.addControl(this._drawControl);
        }

    }
});

export default AdministrationField;
