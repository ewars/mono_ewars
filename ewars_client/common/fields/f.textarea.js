const _languages = ["en", "fr", "ar"];

var LanguageNode = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.value);
    },

    render: function () {
        var className = "language";
        if (this.props.active) className += " active";

        return (
            <li className={className}>
                <div onClick={this._onClick}>{this.props.value}</div>
            </li>
        )
    }
});

var TextAreaField = React.createClass({
    _value: null,

    getInitialProps: function () {
        return {
            config: {
                i18n: false,
                markdown: false
            }
        }
    },

    getInitialState: function () {
        return {
            curLang: "en"
        };
    },

    componentWillMount: function () {
        // A little clean up, fix labels which are not dicts to dicts

    },

    componentWillReceiveProps: function (nextProps) {
    },

    onChange: function (event) {
        var path = this.props.config ? this.props.config.path : null;

        if (this.props.config.i18n) {
            this._value[this.state.curLang] = event.target.value;
            this.props.onUpdate(this.props.name, this._value, path);
        } else {
            this.props.onUpdate(this.props.name, event.target.value, path);
        }
    },

    _handleLanguageChange: function (lang) {
        this.setState({
            curLang: lang
        })
    },

    render: function () {
        this._value = this.props.value;
        var viewValue = this.props.value;

        if (this.props.config.i18n) {

            if (!this.props.value) {
                this._value = {en: "", fr: null};
            }

            if (typeof this.props.value == "string") {
                this._value = {
                    en: this.props.value,
                    fr: null
                }
            }

            if (!this._value[this.state.curLang]) this._value[this.state.curLang] = "";
            viewValue = this._value[this.state.curLang];
        }

        if (this.props.config.i18n) {
            var nodes = _languages.map(language => {
                return <LanguageNode
                    value={language}
                    key={language}
                    onClick={this._handleLanguageChange}
                    active={this.state.curLang == language}/>
            });

            nodes.unshift(
                <li className="icon">
                    <div className="icon"><i className="fal fa-language"></i></div>
                </li>
            )
        }

        if (!viewValue) viewValue = "";

        return (
            <div className="textarea-wrapper">
                {this.props.config.i18n ?
                    <div className="text-area-controls">
                        <ul className="language-list">
                            {nodes}
                        </ul>
                    </div>
                    : null}
                <textarea
                    onChange={this.onChange}
                    disabled={this.props.readOnly}
                    placeholder={this.props.placeholder}
                    className="form-control"
                    name={this.props.name}
                    value={viewValue}></textarea>
                {this.props.config.markdown ?
                <p className="small"><i className="fal fa-markdown"></i> &nbsp;This field supports markdown</p>
                    : null}
            </div>
        )
    }

});

export default TextAreaField;
