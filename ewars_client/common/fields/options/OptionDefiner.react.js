import Button from "../ButtonComponent.react";

var OptionRow = React.createClass({
    _delete: function () {
        this.props.onDelete(this.props.index);
    },

    _onChange: function (e) {
        this.props.onChange(this.props.index, e.target.name, e.target.value);
    },

    _onDelete: function () {
        this.props.onDelete(this.props.index);
    },

    render: function () {
        return (
            <tr>
                <td>
                    <input type="text" name="key" value={this.props.data[0]} onChange={this._onChange}/>
                </td>
                <td>
                    <input type="text" name="value" value={this.props.data[1]} onChange={this._onChange}/>
                </td>
                <td>
                    <ewars.d.Button
                        icon="fa-times"
                        onClick={this._onDelete}/>
                </td>
            </tr>
        )
    }
});

var OptionDefiner = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onChange: function (index, prop, value) {
        var data = JSON.parse(JSON.stringify(this.props.value));
        data[index][prop] = value;
        this.props.onUpdate(this.props.name, data);
    },

    _addOption: function () {
        var data = JSON.parse(JSON.stringify(this.props.value));
        data.push(["NONE", "None"]);
        this.props.onUpdate(this.props.name, data);
    },

    _deleteRow: function (index) {
        var data = JSON.parse(JSON.stringify(this.props.value));
        data.splice(index, 1);
        this.props.onUpdate(this.props.name, data);
    },

    render: function () {

        var items = _.map(this.props.value, function (outcome, index) {
            return <OptionRow
                index={index}
                onDelete={this._deleteRow}
                onChange={this._onChange}
                data={outcome}/>
        }, this);

        return (
            <div className="option-definer">
                <table>
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                        <th>Default</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items}
                    </tbody>
                </table>

                <div className="clearer"></div>
                <ewars.d.Button
                    label="Add option"
                    icon="fa-plus"
                    onClick={this._addOption}/>

            </div>
        )
    }
});

export default OptionDefiner;
