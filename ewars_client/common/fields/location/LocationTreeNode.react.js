var TreeNodeComponent = React.createClass({
    _hasLoaded: false,

    getDefaultProps: function () {
        return {
            allowCheck: false,
            checked: [],
            fillHeight: false,
            location_type: null,
            hideInactive: false,
            showType: false
        }
    },

    getInitialState: function () {
        return {
            children: [],
            showChildren: false,
            loading: false,
            isChecked: false
        };
    },

    componentWillMount: function () {
        // Check if this item is checked, the parent node can deselect all children form the
        // tree
        if (this.props.checked) {
            if (this.props.checked.indexOf(this.props.data.uuid) >= 0) {
                this.state.isChecked = true;
            } else {
                this.state.isChecked = false;
            }
        }
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.checked) {
            if (nextProps.checked.indexOf(nextProps.data.uuid) >= 0) {
                this.state.isChecked = true;
            } else {
                this.state.isChecked = false;
            }
        }
    },

    onCaretClick: function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (this._hasLoaded) {
            this.setState({
                showChildren: this.state.showChildren ? false : true
            });
            return;
        }

        this.setState({
            loading: true
        });

        var query = {
            parent_id: {eq: this.props.data.uuid}
        };
        if (this.props.hideInactive) {
            query.status = {eq: "ACTIVE"};
        } else {
            query.status = {neq: "DELETED"};
        }

        ewars.tx("com.ewars.query", ["location", ["uuid", "name", "site_type_id", "location_type", "geometry_type"], query, {"name.en": "ASC"}, null, null, null])
            .then(function (resp) {
                this._hasLoaded = true;

                if (resp.length > 0) {
                    this.state.children = resp;
                    this.state.showChildren = true;
                    this.state.loading = false;
                } else {
                    this.state.loading = false;
                }

                if (this.isMounted()) this.forceUpdate();
            }.bind(this))

    },

    _onLabelClick: function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (this.props.allowCheck) {
            return;
        }

        if (this.props.location_type) {
            if (this.props.data.site_type_id == this.props.location_type) {
                this.props.onNodeSelect(this.props.data);
            }
        } else {
            this.props.onNodeSelect(this.props.data);
        }
    },

    _hasChildren: function () {
        if (this.props.data.location_type == "ADMIN") return true;
        return false;
    },

    onCheck: function () {
        this.props.onCheck(this.props.data);
    },

    _processChildren: function () {
        var childs = [];

        _.each(this.state.children, function (child, i) {
            childs.push(
                <TreeNodeComponent
                    hideInactive={this.props.hideInactive}
                    checked={this.props.checked}
                    onCheck={this.props.onCheck}
                    allowCheck={this.props.allowCheck}
                    index={i}
                    onEditAdmin={this.props.onEditAdmin}
                    onEditSite={this.props.onEditSite}
                    onNodeSelect={this.props.onNodeSelect}
                    location_type={this.props.location_type}
                    data={child}/>
            )
        }, this);

        return childs;
    },

    render: function () {
        var hasChilds = this._hasChildren();

        var childs;
        if (hasChilds) {
            childs = this._processChildren();
        }

        var iconClass = "fal fa-circle";
        if (this.props.data.geometry_type == "ADMIN") iconClass = "fal fa-caret-right";
        if (this.props.data.geometry_type == "ADMIN" && this._hasLoaded && this.state.showChildren) iconClass = "fal fa-caret-down";
        if (this.props.data.geometry_type == "POINT") iconClass = "fal fa-map-pin";

        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.data.name);

        if (this.state.loading) iconClass = "fal fa-spin fa-spinner";

        var checkClass = "fal fa-square";
        if (this.state.isChecked) checkClass = "fal fa-check-square";

        var labelStyle = {color: "#333333"};
        if (this.props.location_type) {
            if (this.props.data.site_type_id == this.props.location_type) {
                labelStyle.color = "green";
            } else {
                labelStyle.color = "red";
            }
        }

        return (
            <li>
                <div className="node-control" onClick={this.onCaretClick}><i className={iconClass}></i></div>
                {this.props.allowCheck ?
                    <div className="node-check" onClick={this.onCheck}><i className={checkClass}></i>
                    </div>
                    : null}
                <div className="label" style={labelStyle} onClick={this._onLabelClick}>{nodeName}</div>
                {this.state.showChildren ?
                    <div className="children">
                        <ul>
                            {childs}
                        </ul>
                    </div>
                    : null}
            </li>
        )
    }
});

export defautl TreeNodeComponent;
