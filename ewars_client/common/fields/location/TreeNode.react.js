var TreeNodeComponent = React.createClass({
    _hasLoaded: false,

    getDefaultProps: function () {
        return {
            end_select_only: React.PropTypes.boolean,
            location_type: null,
            hideInactive: false
        }
    },

    getInitialState: function () {
        return {
            children: [],
            showChildren: false
        };
    },

    onCaretClick: function (e) {
        e.stopPropagation();
        e.preventDefault();
        if (this.props.data.child_count <= 0) return;

        if (this._hasLoaded) {
            this.setState({
                showChildren: this.state.showChildren ? false : true
            });
            return;
        }

        var self = this;
        var query = {};
        if (this.props.hideInactive) query.status = 'ACTIVE';
        ewars._connect().then(function (session) {
            session.call("com.ewars.locations.query", [query, self.props.data.uuid])
                .then(function (resp) {
                    this._hasLoaded = true;

                    if (resp.d.length > 0) {
                        this.setState({
                            children: resp.d,
                            showChildren: true
                        })
                    } else {
                        this.forceUpdate();
                    }

                }.bind(self))
        }.bind(this))
    },

    _onLabelClick: function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (this.props.location_type) {
            if (this.props.data.site_type_id == this.props.location_type) {
                this.props.onNodeSelect(this.props.data);
                return;
            }
        }

        if (this.props.data.child_count > 0) {
            if (!this.props.location_type)  {
                this.props.onNodeSelect(this.props.data);
            } else {
                this.onCaretClick(e);
            }
        }

    },

    _hasChildren: function () {
        if (this.props.data.child_count > 0) return true;
        return false;
    },

    _processChildren: function () {
        var childs = [];

        for (var i in this.state.children) {
            var child = this.state.children[i];

            childs.push(
                <TreeNodeComponent
                    end_select_only={this.props.end_select_only}
                    taxonomy_id={this.props.taxonomy_id}
                    index={i}
                    onNodeSelect={this.props.onNodeSelect}
                    data={child}/>
            )
        }

        return childs;
    },

    render: function () {
        var hasChilds = this._hasChildren();

        var childs;
        if (hasChilds) {
            childs = this._processChildren();
        }

        var iconClass = "fal fa-circle";
        if (this._hasChildren()) iconClass = "fal fa-caret-right";
        if (this._hasChildren() && this._hasLoaded && this.state.showChildren) iconClass = "fal fa-caret-down";

        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.data.name);

        var labelStyle = {color: "#333333"};
        if (this.props.location_type) {
            if (this.props.data.site_type_id == this.props.location_type) {
                labelStyle.color = "green";
            } else {
                labelStyle.color = "red";
            }
        }

        return (
            <li>
                <div className="node-control" onClick={this.onCaretClick}><i className={iconClass}></i></div>
                <div className="label" onClick={this._onLabelClick} style={labelStyle}>{nodeName}</div>
                {this.state.showChildren ?
                    <div className="children">
                        <ul>
                            {childs}
                        </ul>
                    </div>
                    : null}
            </li>
        )
    }
});

export default TreeNodeComponent;
