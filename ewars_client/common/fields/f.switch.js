var SwitchField = React.createClass({
    getInitialState: function () {
        return {};
    },

    onChange: function () {
        let value;
        if ([true, "true"].indexOf(this.props.value) >= 0) value = true;
        if ([false, "false", -1, 0].indexOf(this.props.value) >= 0) value = false;

        console.log(value);
        if (this.props.config && this.props.config.path) {
            this.props.onUpdate(this.props.name, !value, this.props.config.path);
        } else {
            this.props.onUpdate(this.props.name, !value);
        }
    },

    render: function () {
        var value = false;
        if (this.props.value) value = true;

        var name = ewars.utils.uniqueId("switch_");

        let className = "fal fa-toggle-off";
        if (value) className = "fal fa-toggle-on";

        let style = {};
        if (value) style.color = "green";

        return (
            <div className="ide-toggle" onClick={this.onChange}>
                <i style={style} className={className}></i>
            </div>
        )
    }
});

export default SwitchField;
