import Spinner from "../c.spinner";

var AssignmentItem = React.createClass({
    getInitialState: function () {
        return {}
    },

    onNodeClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var locationName = ewars.formatters.I18N_FORMATTER(this.props.data.location_name);

        return (
            <div className="item" onClick={this.onNodeClick}>
                {locationName}
            </div>
        )
    }
});

var Handle = React.createClass({
    render: function () {
        var name = ewars.I18N(this.props.current.name);

        return (
            <div className="handle" onClick={this.props.onClick}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>{name}</td>
                        <td width="20px" className="icon">
                            <i className="fal fa-caret-down"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});

var Item = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = 'item';
        if (this.props.value == this.props.data[0]) className += " active";

        return (
            <div onClick={this._onClick} className={className}>{this.props.data[1]}</div>
        )
    }
});

var AssignmentLocationField = React.createClass({
    _isLoaded: false,
    _initialLoad: false,

    getInitialState: function () {
        return {
            assignments: [],
            showTree: false,
            currentLocation: {
                name: {en: "No Location Selected"}
            }
        }
    },

    componentWillMount: function () {
        if (this.props.value) {
            this._loadSetLocation();
        } else {
            this._initialLoad = true;
        }
    },

    _loadSetLocation: function () {
        ewars.tx("com.ewars.resource", ["location", this.props.value, ["uuid", "name"], null])
            .then(function (resp) {
                this.state.currentLocation = resp;
                this._initialLoad = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _loadAssignments: function () {
        if (this.props.config.form_id) {
            ewars.tx("com.ewars.user.assignments", [])
                .then(function (resp) {
                    this.state.assignments = resp;
                    this._isLoaded = true;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.config.form_id != this.props.config.form_id) {
            if (nextProps.config.form_id) {
                ewars.tx("com.ewars.user.assignments", [])
                    .then(function (resp) {
                        this.state.assignments = resp;
                        if (this.isMounted()) this.forceUpdate();
                    }.bind(this));

                if (this.props.value != nextProps.value) {
                    ewars.tx("com.ewars.resource", ["location", nextProps.value, ["uuid", "name"], null])
                        .then(function (resp) {
                            this.state.currentLocation = resp;
                            if (this.isMounted()) this.forceUpdate();
                        }.bind(this))
                }
            }
        } else if (this.props.value != nextProps.value) {
            ewars.tx("com.ewars.resource", ["location", nextProps.value, ["uuid", "name"], null])
                .then(function (resp) {
                    this.state.currentLocation = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    onNodeSelect: function (node) {
        this.state.currentLocation = node;
        this.state.showTree = false;
        this.forceUpdate();
        this.props.onUpdate(this.props.name, node.location_uuid, this.props.config.path || "");
    },

    _processLocations: function () {
        var items = _.map(this.state.assignments, function (item) {
            if (item.form_id == this.props.config.form_id) {
                return <AssignmentItem data={item} onClick={this.onNodeSelect}/>
            }
        }, this);

        return items;
    },

    _toggle: function () {
        this.state.showTree = this.state.showTree ? false : true;
        this.forceUpdate();

        if (!this._isLoaded) {
            this._loadAssignments();
        }
    },

    componentDidMount: function () {
        window.__assign_hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function () {
        window.__assign_hack__.removeEventListener('click', this.handleBodyClick);
    },

    handleBodyClick: function (evt) {
        if (this.refs.selector) {
            const area = this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showTree = false;
                this.forceUpdate();
            }
        }
    },


    render: function () {
        if (this.props.value && !this._initialLoad) {
            return (
                <div className="ew-select">
                    <div className="handle">
                        <i className="fal fa-spin fa-circle-o-notch"></i>
                    </div>
                </div>
            )
        }


        var items = _.map(this.state.assignments, function (item) {
            if (item.form_id == this.props.config.form_id) {
                return <AssignmentItem data={item} onClick={this.onNodeSelect} key={item.assignment_id}/>;
            }
        }, this);

        var selectedName = "No Location Selected";
        if (!this.props.config.form_id) selectedName = "Please select a form first.";
        if (this.state.currentLocation) selectedName = ewars.formatters.I18N_FORMATTER(this.state.currentLocation.location_name);

        if (this.props.readOnly) {
            return (
                <input type="text" disable={true} value={selectedName}/>
            )
        }


        if (!this._isLoaded && this.state.showTree) {
            items = <Spinner/>;
        }

        return (
            <div ref="selector" className="ew-select">
                <Handle onClick={this._toggle}
                        current={this.state.currentLocation}
                        value={this.props.value}
                        options={this.state.assignments}/>
                {this.state.showTree ?
                    <div className="ew-select-data">
                        {items}
                    </div>
                    : null}
            </div>
        )
    }

});

export default AssignmentLocationField;
