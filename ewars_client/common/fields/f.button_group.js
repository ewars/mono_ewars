import Button from "../c.cmp.button";

class ButtonGroupField extends React.Component {
    static defaultProps = {
        context_help: null,
        value: null,
        config: {
            context_help: null
        }
    };

    _onClick = (data) => {
        this.props.onUpdate(this.props.config.nameOverride || this.props.name, data[0], this.props.path || this.props.name);
    };

    render() {
        let contextHelp;
        if (this.props.config.context_help) {
            if (this.props.value != null) {
                contextHelp = __(this.props.config.context_help[this.props.value])
            }
        }
        return (
            <div className="btn-group formed">
                {this.props.config.options.map((button, index) => {
                    let key = `BUTTON_${index}`;
                    return <ewars.d.Button
                        icon={button[2] || null}
                        label={button[1]}
                        key={key}
                        active={this.props.value == button[0]}
                        onClick={this._onClick}
                        data={button}/>
                })}
                <div className="clearer" style={{display: "block", paddingBottom: "5px"}}></div>
                {contextHelp ?
                    <p>{contextHelp}</p>
                    : null}
            </div>
        )
    }
}

export default ButtonGroupField;
