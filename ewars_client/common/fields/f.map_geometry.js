import chroma from "chroma-js";

const _colorScale = chroma.scale(['wheat', 'maroon']);

// need to extend leaflet with TopoJSON support
if (window.L) {
    L.TopoJSON = L.GeoJSON.extend({
        addData: function (jsonData) {
            if (jsonData.type === "Topology") {
                for (key in jsonData.objects) {
                    geojson = topojson.feature(jsonData, jsonData.objects[key]);
                    L.GeoJSON.prototype.addData.call(this, geojson);
                }
            }
            else {
                L.GeoJSON.prototype.addData.call(this, jsonData);
            }
        }
    });

    var _redIcon = L.icon({
        iconUrl: 'static/icons/map-markers/marker-red.png',

        iconSize: [21, 34], // size of the icon
        shadowSize: [26, 26], // size of the shadow
        iconAnchor: [10.5, 34], // point of the icon which will correspond to marker's location
        shadowAnchor: [10.5, 24],  // the same for the shadow
        popupAnchor: [-3, -9] // point from which the popup should open relative to the iconAnchor
    });
}

var MapGeometryField = React.createClass({
    _map: null,
    _markers: [],
    _marker: null,

    getDefaultProps: function () {
        return {
            showTiles: true,
            height: 300,
            pointEditMode: false,
            defaultLat: 51.505,
            defaultLng: -0.09
        }
    },

    getInitialState: function () {
        return {};
    },

    componentDidMount: function () {
        if (!this._map) this.renderEditor();
    },

    componentWillReceiveProps: function () {
        if (!this._map) this.renderEditor();

        if (this._multiPGroup) {
            this._map.removeLayer(this._multiPGroup);
            this._multiPGroup = null;
            this._map.removeControl(this._drawControl);
            this._drawControl = null;
        }

        if (this.props.value) {
            // Need to determin geometry type
            var value = JSON.parse(this.props.value);
            if (value.type == "MultiPolygon") {

                this._featureGroup = new L.featureGroup().addTo(this._map);
                _.each(value.coordinates[0][0], function (coordSet) {
                    coordSet.reverse()
                });

                this._multiPGroup = new L.Polygon(value.coordinates[0][0]).addTo(this._featureGroup);

                this._drawControl = new L.Control.Draw({
                    position: 'topright',
                    edit: {
                        featureGroup: this._featureGroup
                    }
                });
                this._map.addControl(this._drawControl);

            } else if (value.type == "Point") {
            } else {
                if (this._drawControl) this._map.removeControl(this._drawControl);
                this._drawControl = null;

                if (!this._marker) {
                    this._marker = L.marker(value.coordinates.reverse(), {draggable: true}).addTo(this._map);

                    this._map.on("dblclick", function (e) {
                        this._handleMapLocationChange(e.latlng.lat, e.latlng.lng);
                    }.bind(this));

                    this._marker.on('dragend', function (e) {
                        var loc = this._marker.getLatLng();
                        this._handleMapLocationChange(loc.lat, loc.lng);
                    }.bind(this));
                }

                //this._marker = L.marker(value.coordinates).addTo(this._map);
                this._marker.setLatLng(value.coordinates.reverse());
                this._map.panTo(new L.LatLng(value.coordinates[0], value.coordinates[1]));
            }
        }
    },

    componentWillUnmount: function () {
        if (this._map) {
            this._map.off("click", this.onMapClick);
            this._map = null;
        }
    },

    onChange: function (name, value) {
        this.props.onChange(name, value);
    },

    render: function () {
        if (this.props.height) {
            var style = {
                height: this.props.height + "px"
            }
        }

        return (
            <div className="administration-field">
                <div className="map" style={style} ref="geometryField"></div>
                {!this.props.readOnly ?
                    <div className="edit-controls">
                        <p>Upload a new geometry for this administration, accpeted files are GeoJSON, TopoJSON and Shapefile (.shp)</p>
                        <input type="file"/>
                    </div>
                    : null}

            </div>
        )
    },

    _createGeoJSON: function (lat, lng) {
        return JSON.stringify({"type": "Point", "coordinates": [lng, lat]});
    },

    _handleMapLocationChange: function (lat, lng) {
        this.props.onUpdate(this.props.name, this._createGeoJSON(lat, lng));
    },

    renderEditor: function () {
        this._map = L.map(this.refs.geometryField.getDOMNode(), {
            minZoom: 0,
            maxZoom: 20,
            attributionControl: false,
            doubleClickZoom: false,
            scrollWheelZoom: (this.props.pointEditMode ? false : true)
        });

        if (this.props.lat && this.props.lng) {
            this._map.setView([this.props.lat, this.props.lng], 1);
        } else {
            this._map.setView([this.props.defaultLat, this.props.defaultLng], 1);
        }

        L.mapbox.accessToken = 'pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg';
        var tiles = L.mapbox.tileLayer('jduren.lomc08mg').addTo(this._map);

        if (this.props.value) {
            // Need to determine geometry type
            var value = JSON.parse(this.props.value);

            if (value.type == "MultiPolygon") {

                this._featureGroup = new L.featureGroup().addTo(this._map);
                _.each(value.coordinates[0][0], function (coordSet) {
                    coordSet.reverse()
                });

                this._multiPGroup = new L.Polygon(value.coordinates[0][0]).addTo(this._featureGroup);

                this._drawControl = new L.Control.Draw({
                    position: 'topright',
                    edit: {
                        featureGroup: this._featureGroup
                    }
                });
                this._map.addControl(this._drawControl);

                //this._map.setView(this._multiPGroup.getBounds());
            } else {
                this._marker = L.marker(value.coordinates.reverse(), {draggable: true}).addTo(this._map);
                this._map.setView(new L.LatLng(value.coordinates[0], value.coordinates[1]), 8);

                this._map.on("dblclick", function (e) {
                    this._handleMapLocationChange(e.latlng.lat, e.latlng.lng);
                }.bind(this));

                this._marker.on('dragend', function (e) {
                    var loc = this._marker.getLatLng();
                    this._handleMapLocationChange(loc.lat, loc.lng);
                }.bind(this));
            }
        }

    }
});

export default MapGeometryField;
