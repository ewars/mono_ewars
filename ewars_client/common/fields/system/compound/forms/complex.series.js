export default {
    variable_name: {
        type: "slug",
        label: {
            en: "Variable Name"
        },
        replaceValue: "_"
    },
    indicatorUUID: {
        type: "indicator",
        required: true,
        label: {
            en: "Indicator Source"
        },
        help: {
            en: "The data to use"
        }
    }
};
