export default {
    type: {
        type: "select",
        label: {
            en: "Series Type"
        },
        required: true,
        options: [
            ["line", "Line"],
            ["bar", "Bar"],
            ["area", "Area"],
            ["column", "Column"],
            ["scatter", "Scatter"],
            ["spline", "Spline"],
            ["waterfall", "Waterfall"]
        ],
        help: {
            en: "The type of series chart to display"
        }
    },
    title: {
        type: "text",
        label: {
            en: "Series Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    locationUUID: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        }
    },
    indicatorUUID: {
        type: "indicator",
        required: true,
        label: {
            en: "Indicator Source"
        },
        help: {
            en: "The data to use"
        }
    },
    start_date: {
        type: "date",
        label: {
            en: "Start Date"
        },
        help: {
            en: "The start date of the series"
        }
    },
    end_date: {
        type: "date",
        label: {
            en: "End Date"
        },
        help: {
            en: "The end date of the series"
        }
    },
    timeInterval: {
        type: "select",
        label: {
            en: "Sample Interval"
        },
        options: [
            ["DAY", "Daily"],
            ["WEEK", "Weekly"],
            ["MONTH", "Monthly"],
            ["YEAR", "Annually"]
        ],
        required: true,
        help: {
            en: "The interval at which to aggregate the data in the series"
        }
    },
    colour: {
        type: "text",
        label: {
            en: "Series Colour"
        },
        help: {
            en: "The colour to use for the series, if unset, will default to indicator defined colour or random"
        }
    },
    modifier: {
        type: "number",
        label: {
            en: "Modifier"
        },
        help: {
            en: "A value to multiply each value in the series by, no modifier will be use if left blank"
        }
    }
};
