export default {
    aggType: {
        type: "select",
        label: {
            en: "Aggregation Type"
        },
        required: true,
        options: [
            ["SUM", "SUM"],
            ["AVG", "Average/Median"]
        ]
    }
};
