export default {
    indicatorUUID: {
        type: "indicator",
        required: true,
        label: {
            en: "Indicator Source"
        },
        help: {
            en: "The data to use"
        }
    },
    modifier: {
        type: "number",
        label: {
            en: "Modifier"
        },
        help: {
            en: "A value to multiply each value in the series by, no modifier will be use if left blank"
        }
    }
};
