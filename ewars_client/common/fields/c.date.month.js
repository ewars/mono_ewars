import CONSTANTS from "../constants";
import Moment from "moment";

class Month extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        let newDate = Moment.utc([this.props.year, this.props.month, 1]);
        newDate = newDate.clone().endOf("M");
        this.props.onClick(newDate);
    };

    render() {
        let name = CONSTANTS.MONTHS_SHORT[this.props.month];

        let className = "cal-month";

        let nominalDate = Moment.utc([this.props.year, this.props.month, 1]).endOf("M");

        if (this.props.value.isSame(nominalDate, "day")) className += " selected";

        return (
            <td onClick={this._onClick} className={className}>{name}</td>
        )
    }
}

const MONTHS = [
    [0, 1, 2, 3],
    [4, 5, 6, 7],
    [8, 9, 10, 11]
];

class MonthPicker extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            curDate: Moment.utc(this.props.value) || Moment.utc()
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            curDate: Moment.utc(nextProps.value) || Moment.utc()
        })
    }

    _prevMonth = () => {
        this.setState({
            curDate: this.state.curDate.clone().subtract(1, 'Y')
        })
    };

    _nextMonth = () => {
        this.setState({
            curDate: this.state.curDate.clone().add(1, "Y")
        })
    };

    _onDateSelect = (newDate) => {
        this.props.onChange(newDate);
    };

    render() {

        let months = [];
        let selected = Moment.utc(this.props.value);
        let year = this.state.curDate.year();

        for (var i = 0; i <= 2; i++) {
            let cells = [];
            for (var j = 0; j <= 3; j++) {
                cells.push(
                    <Month
                        onClick={this._onDateSelect}
                        year={year}
                        value={selected}
                        month={MONTHS[i][j]}/>
                )
            }
            months.push(<tr>{cells}</tr>)
        }

        return (
            <div className="date-picker">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 30}}>
                        <div className="ide-col">
                            <div className="ide-row">
                                <div className="ide-col cal-left" style={{maxWidth: 25}} onClick={this._prevMonth}>
                                    <i className="fal fa-caret-left"></i>
                                </div>
                                <div className="ide-col cal-header">
                                    {year}
                                </div>
                                <div className="ide-col cal-right" style={{maxWidth: 25}} onClick={this._nextMonth}>
                                    <i className="fal fa-caret-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ide-row" style={{padding: 8}}>
                        <div className="ide-col">
                            <table>
                                <tbody>
                                {months}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MonthPicker;
