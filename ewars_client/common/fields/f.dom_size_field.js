import { Layout, Row, Cell } from "../layout";
import ButtonGroup from "./f.button_group";

import NumericField from "./f.numeric";
const NumberField = NumericField;

const PX_REG = /([px])/g;
const EM_REG = /([em])/g;
const PERC_REG = /([%])/g;

const BUTTON_OPTIONS = [
    ["px", "px"],
    ["%", "%"],
    ["em", "em"]
];

const UNITS_CONFIG = {
    options: BUTTON_OPTIONS
};

class DOMSizeField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            units: "px"
        }
    }

    _onUnitsChange = (prop, units) => {
        let value = 0;
        if (PX_REG.test(this.props.value)) {
            value = this.props.value.replace("px", "");
            this.props.onUpdate(this.props.name, value + units);
            return;
        }

        if (EM_REG.test(this.props.value)) {
            value = this.props.value.replace("em", "");
            this.props.onUpdate(this.props.name, value + units);
            return;
        }

        if (PERC_REG.test(this.props.value)) {
            value = this.props.value.replace("%", "");
            this.props.onUpdate(this.props.name, value + units);

        }
    };

    _onValueChange = (prop, value) => {
        let units;
        if (PX_REG.test(this.props.value)) units = "px";
        if (EM_REG.test(this.props.value)) units = "em";
        if (PERC_REG.test(this.props.value)) units = "%";

        if (!units) units = "px";

        let update;
        if (value) update = value + units;

        this.props.onUpdate(this.props.name, update);
    };


    render() {
        let value = this.props.value;
        let units = "px";
        if (isNaN(value)) {
            if (value.indexOf("px") >= 0) {
                value = value.replace("px", "");
            } else if (value.indexOf("em") >= 0) {
                value = value.replace("em", "");
                units = "em";
            } else if (value.indexOf("%") >= 0) {
                value = value.replace("%", "");
                units = "%";
            }
        }

        return (
            <Layout>
                <Row>
                    <Cell>
                        <NumberField
                            name="value"
                            onUpdate={this._onValueChange}
                            value={value}/>
                    </Cell>
                    <Cell style={{paddingTop: 3, paddingLeft: 8}}>
                        <ButtonGroup
                            name="units"
                            value={units}
                            onUpdate={this._onUnitsChange}
                            config={UNITS_CONFIG}/>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}

export default DOMSizeField;
