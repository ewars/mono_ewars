import FormFieldTree from "./c.tree.form_fields";
import FormUtils from "../utils/FormUtils";

function _findFieldByPath(options, path) {
    var found;

    _.each(options, function (field) {
        if (field.path == path) found = field;
        if (!found && field.children) {
            var subFound = _findFieldByPath(field.children, path);
            if (subFound) found = subFound;
        }
    }, this);

    return found;
}

var ConditionRow = React.createClass({
    _data: [null, "eq", null],
    _node: null,

    getInitialProps: function () {
        return {
            data: [null, "eq", null]
        }
    },

    getInitialState: function () {
        return {
            data: [null, "eq", null]
        };
    },

    componentWillMount: function () {
        if (this.props.data) {
            this._node = _findFieldByPath(this.props.fieldOptions, this.props.data[0]);

            this.state.data = ewars.copy(this.props.data);
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.data = ewars.copy(nextProps.data);
    },

    _removeRule: function () {
        this.props.onRemove(this.props.index);
    },

    _onFieldSelect: function (e) {
        this.state.data[0] = e.target.value;
        this.props.onUpdate(this.props.index, this.state.data);
    },

    _onValueChange: function (prop, value) {
        this.state.data[2] = value;
        this.props.onUpdate(this.props.index, this.state.data);
    },

    _onCmpChange: function (e) {
        this.state.data[1] = e.target.value;
        this.props.onUpdate(this.props.index, this.state.data);
    },

    render: function () {
        var valueControl;

        if (this.state.data[0] && this.state.data[0] != "none") {
            // Find the field definition
            var fieldDef = _.find(this.props.fieldOptions, function (field) {
                if (field[0] == this.state.data[0]) return true;
            }, this);


            var FieldControl = this.props.map[fieldDef[2].type];

            valueControl = <FieldControl
                name="null"
                value={this.props.data[2]}
                onUpdate={this._onValueChange}
                config={fieldDef[2]}/>
        } else {
            valueControl = <span>Please select a field</span>;
        }

        var options = _.map(this.props.fieldOptions, function (optionSet) {
            var fieldLabel = optionSet[1];
            return <option value={optionSet[0]}>{fieldLabel}</option>
        }, this);
        options.unshift(<option value="">-- Select a Field --</option>);

        return (
            <div className="condition-row">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <select name="fieldName" value={this.props.data[0]} id="" data-index={this.props.index} onChange={this._onFieldSelect}>
                                {options}
                            </select>
                        </td>
                        <td>
                            <select value={this.props.data[1]} onChange={this._onCmpChange}>
                                <option value="eq">eq</option>
                                <option value="ne">ne</option>
                                <option value="gt">gt</option>
                                <option value="gte">gte</option>
                                <option value="lt">lt</option>
                                <option value="lte">lte</option>
                            </select>
                        </td>
                        <td>
                            {valueControl}
                        </td>
                        <td width="30px">
                            <div className="btn-group">
                                <button className="condition-action red" onClick={this._removeRule}>
                                    <i className="fal fa-times"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});

export default ConditionRow;
