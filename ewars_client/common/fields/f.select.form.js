var FormSelectionField = React.createClass({
    getInitialState: function () {
        return {}
    },

    componentWillMount: function () {
        var self = this;
        ewars._connect().then(function (session) {
            session.call("com.ewars.query", ["Form", {
                $select: ["id", "name"]
            }])
                .then(
                function (resp) {
                    self.setState({
                        forms: resp.d
                    })
                }
            )
        })
    },

    _onChange: function (e) {
        var selections = [];

        _.each(e.target.options, function (option) {
            if (option.selected) {
                selections.push(parseInt(option.value));
            }
        });

        if (this.props.config && this.props.config.path) {
            this.props.onUpdate(this.props.name, selections, this.props.config.path);
        } else {
            this.props.onUpdate(this.props.name, selections);
        }
    },

    render: function () {
        var options = [];
        for (var i in this.state.forms) {
            var form = this.state.forms[i];

            var selected = false;
            if (this.props.value) {
                if (this.props.value.indexOf(form.id) >= 0) selected = true;
            }
            var name = ewars.formatters.I18N_FORMATTER(form.name);
            options.push(
                <option value={form.id} selected={selected}>{name}</option>
            )
        }

        return (
            <select multiple={true} onChange={this._onChange}>
                {options}
            </select>
        )
    }

});

export default FormSelectionField;
