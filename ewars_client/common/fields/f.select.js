import Spinner from "../c.spinner";

window.__hack__ = document.getElementById("application");

var PRESET_TEMPLATES = {
    form: React.createClass({
        _onClick: function () {
            this.props.onClick(this.props.data);
        },

        render: function () {
            var formName, accountName;
            var item = this.props.data[2];
            var className = 'item';
            formName = ewars.I18N(item.name);
            if (item.account) accountName = ewars.I18N(item.account.name);
            if (this.props.value == this.props.data[0]) className += " active";

            return (
                <div onClick={this._onClick} className={className}>
                    <div className="ew-list-item-title">{formName}</div>
                    <span>{accountName}</span>
                </div>
            )
        }
    }),
    alarm: React.createClass({
        _onClick: function () {
            this.props.onClick(this.props.data);
        },

        render: function () {
            var alarmName, accountName;
            var item = this.props.data[2];
            var className = 'item';
            if (this.props.value == this.props.data[0]) className += " active";
            alarmName = ewars.I18N(item.name);
            accountName = ewars.I18N(item.account.name);

            return (
                <div onClick={this._onClick} className={className}>
                    <div className="ew list-item-title">{alarmName}</div>
                    <span>{accountName}</span>
                </div>
            )
        }
    }),
    assignment: React.createClass({
        _onClick: function () {
            this.props.onClick(this.props.data);
        },

        render: function () {
            let shortName = ewars.I18N(this.props.data[2][0]);
            let longName = this.props.data[2][1];
            longName = longName.split(",");
            let longNameDOM = longName.map(function (item) {
                return <span>{item}</span>
            });
            var className = 'item';
            if (this.props.value == this.props.data[0]) className += " active";

            return (
                <div onClick={this._onClick} className={className}>
                    <div className="ew list-item-title">{shortName}</div>
                    <div className="loc-long-name">
                        <div className="wrap">
                            {longNameDOM}
                        </div>
                    </div>
                </div>
            )
        }
    })
};

function _recurseOptions(items, label, val, level) {
    var options = [];
    var dashes = "";

    if (level) {
        let dashes = "";
        for (let i = 0; i <= level; i++) {
            dashes += " ";
        }
        dashes += "-";
    }

    items.forEach(option => {
        var itemLabel = dashes + " ";
        itemLabel += ewars.I18N(option[label]);

        options.push([option[val], itemLabel]);

        if (option.children.length > 0) {
            var subOptions = _recurseOptions(option.children, label, val, level + 1);
            options.push.apply(options, subOptions);
        }
    });

    return options;
}

function _processExternalOptions(options, hProp) {
    var optionsMap = {};
    options.forEach(option => {
        if (!optionsMap[option.id]) optionsMap[option.id] = option;
        optionsMap[option.id].children = [];
    });

    optionsMap.forEach(option => {
        if (option.parent_id) {
            optionsMap[option.parent_id].children.push(option);
        }
    });

    let cleaned = optionsMap.filter(item => {
        if (item.parent_id) return false;
        return true;
    });

    return cleaned;
}

var Handle = React.createClass({
    render: function () {
        var name = "No Selection";


        if (this.props.value) {
            let item = this.props.options.find(item => {
                return item[0] == this.props.value;
            });
            if (item) name = item[1];
        }


        if (this.props.prefixed) {
            let suspects = this.props.prefixed.filter(item => {
                return item[0] == this.props.value;
            })
            if (suspects.length > 0) {
                name = suspects[0][1];
            }
        }

        return (
            <div className="handle" onClick={this.props.onClick}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>{name}</td>
                        <td width="20px" className="icon">
                            <i className="fal fa-caret-down"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});

var MultiSelectWrapper = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var iconClass = "fal";
        if (this.props.value) {
            if (this.props.value.indexOf(this.props.data[0]) >= 0) {
                iconClass += " fa-check-square";
            } else {
                iconClass += " fa-square";
            }
        } else {
            iconClass += " fa-square"
        }
        return (
            <div className="ew-select-multi-item" onClick={this._onClick}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td width="25px">
                            <div className="ew-select-check">
                                <i className={iconClass}></i>
                            </div>
                        </td>
                        <td>
                            {this.props.children}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});

var Item = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = 'item';
        if (this.props.value == this.props.data[0]) className += " active";

        let onClick = this._onClick;
        if (this.props.multiple) onClick = null;

        return (
            <div className={className} onClick={onClick}>{this.props.data[1]}</div>
        )
    }
});

var SelectField = React.createClass({
    _isLoaded: false,
    _initialLoaded: false,

    PropTypes: {
        config: React.PropTypes.object
    },

    getDefaultProps: function () {
        return {
            path: null,
            name: null,
            config: {}
        }
    },

    getInitialProps: function () {
        return {
            name: null,
            config: {
                multiple: false,
                path: null,
                groups: false
            },
            emptyText: "No Selection"
        }
    },

    getInitialState: function () {
        return {
            options: [],
            rawOptions: [],
            showOptions: false,
            placeKey: ewars.utils.uuid()
        }
    },

    _init: function (props) {
        if (props.value && props.config.optionsSource) {
            if (props.config.multiple) {
                this._initialLoaded = true;
                if (this.isMounted()) this.forceUpdate();
                this._toggle(null);
                return;
            }
            var resource = props.config.optionsSource.resource.toLowerCase();
            var select = [props.config.optionsSource.labelSource, props.config.optionsSource.valSource];
            if (props.config.optionsSource.select) {
                select = props.config.optionsSource.select;
            }

            if (this.props.config.optionsSource.additionalProperties) {
                select = select.concat(this.props.config.optionsSource.additionalProperties);
            }

            let isPrefix;
            if (props.config.optionsSource.prefixed) {
                let suspects = props.config.optionsSource.prefixed.filter(item => {
                    return item[0] == props.value;
                });
                if (suspects.length > 0) isPrefix = true;
            }

            if (!isPrefix) {
                ewars.tx("com.ewars.resource", [resource, props.value, select, null])
                    .then(function (resp) {
                        if (resp) {
                            this.state.options = [[resp[props.config.optionsSource.valSource], ewars.I18N(resp[props.config.optionsSource.labelSource])]];
                        }

                        if (props.config) {
                            if (props.config.optionsSource) {
                                if (props.config.optionsSource.additional) {
                                    props.config.optionsSource.additional.forEach(option => {
                                        var optionValue = option[0];
                                        if (optionValue == null) optionValue = "null";
                                        this.state.options.unshift([optionValue, option[1]]);
                                    });
                                }
                            }
                        }

                        this._initialLoaded = true;
                        if (this.isMounted()) this.forceUpdate();
                    }.bind(this))
            } else {
                this._initialLoaded = true;
            }
        }
    },

    componentWillMount: function () {
        this._id = ewars.utils.uuid();
        if (this.props.config && this.props.config.optionsSource && this.props.value) {
            this._init(this.props);
        } else {
            this._initialLoaded = true;
            this.state.options = this.props.config.options;
        }
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.config && nextProps.config.optionsSource && nextProps.value) {
            if (this.props.value != nextProps.value) this._init(nextProps);
        } else {
            this._initialLoaded = true;
            this.state.options = nextProps.config.options;
        }
    },

    componentDidMount: function () {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function () {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
    },

    handleBodyClick: function (evt) {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showOptions = false;
                this.forceUpdate();
            }
        }
    },

    handleClick: function (e) {
        e.stopPropagation();
    },

    _selectNone: function (e) {
        e.preventDefault();

        let name = this.props.config.nameOverride || this.props.name;

        this.props.onUpdate(name, [], this.props.path, null);
    },

    _selectAll: function (e) {
        e.preventDefault();

        let name = this.props.config.nameOverride || this.props.name;

        let value;
        if (this.props.config.optionsSource) {
            value = this.state.rawOptions.map(function (item) {
                return item[this.props.config.optionsSource.valSource];
            }.bind(this));
        } else {
            value = this.props.config.options.map(function (item) {
                return item[0];
            }.bind(this))
        }

        this.props.onUpdate(name, value, this.props.path, null);
    },

    _onPrefixSelect: function (e) {
        if (this.props.readOnly) return;
        let val = e.target.getAttribute('data-value');

        let name = this.props.config.nameOverride || this.props.name;
        this.state.showOptions = false;

        this.props.onUpdate(name, val, this.props.path, null);
    },

    onChange: function (item) {
        if (this.props.readOnly) return;
        this.state.showOptions = false;

        let name = this.props.config.nameOverride || this.props.name;

        let path = this.props.path || null;

        var node = item;
        if (this.props.config.optionsSource) {
            node = this.state.rawOptions.find(result => {
                if (result[this.props.config.optionsSource.valSource] == item[0]) return true;
            });
        }

        console.log(item);
        if (!this.props.config.multiple) {
            if (this.props.config) this.props.onUpdate(name, item[0], path, node);
            if (!this.props.config) this.props.onUpdate(name, item[0], path, node);
        } else {
            let newVal = [];
            if (this.props.value && this.props.value.constructor === Array) {
                newVal = this.props.value;
            }

            if (newVal.indexOf(item[0]) < 0) {
                newVal.push(item[0]);
            } else {
                let tmp = newVal.filter(p => {
                    if (p == item[0]) return false;
                    return true;
                });
                newVal = tmp;
            }

            this.props.onUpdate(name, newVal, path, node);

        }
    },

    _processConfig: function () {
        var config = {};
        if (this.props.config) config = this.props.config;

        if (config.multiple == undefined || config.multiple == null) config.multiple = false;
        return config;
    },

    _getOptionDisplay: function () {
        var result;
        for (var i in this.state.options) {
            if (this.state.options[i][0] == this.props.value) {
                result = this.state.options[i][1];
            }
        }
        return result;
    },

    _getOptions: function () {
        if (this.props.options) return this.props.options;
        if (this.props.config.options) return this.props.config.options;
        if (this.state.options) return this.state.options;
        return [];
    },

    _toggle: function (e) {
        if (e) e.stopPropagation();
        this.state.showOptions = this.state.showOptions ? false : true;

        this.forceUpdate();

        if (this.props.config.optionsSource && this._isLoaded) {
            var options = [];

            if (this.props.config.optionsSource.hierarchical) {
                var optionsH = _processExternalOptions(this.state.rawOptions, this.props.config.optionsSource.hierarchyProp);
                options = _recurseOptions(optionsH, this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource, 0);
            } else {

                this.state.rawOptions.forEach((item) => {
                    options.push([
                        item[this.props.config.optionsSource.valSource],
                        ewars.I18N(item[this.props.config.optionsSource.labelSource]),
                        item
                    ])
                });

                if (this.props.config) {
                    if (this.props.config.optionsSource) {
                        if (this.props.config.optionsSource.additional) {
                            props.config.optionsSource.additional.forEach(option => {
                                var optionValue = option[0];
                                if (optionValue == null) optionValue = "null";
                                options.unshift([optionValue, option[1]]);
                            });
                        }
                    }
                }
            }

            this.state.options = options;
            if (this.isMounted()) this.forceUpdate();
        }

        if (this.props.config.optionsSource && !this._isLoaded) {
            // We need to load the options
            var resource = this.props.config.optionsSource.resource.toLowerCase();
            var select = [this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource];
            if (this.props.config.optionsSource.select) {
                select = this.props.config.optionsSource.select;
            }

            if (this.props.config.optionsSource.additionalProperties) {
                select = select.concat(this.props.config.optionsSource.additionalProperties);
            }

            var join = this.props.config.optionsSource.join || null;

            ewars.tx("com.ewars.query", [resource, select, this.props.config.optionsSource.query, null, null, null, join])
                .then(function (resp) {
                    var options = [];

                    if (this.props.config.optionsSource.hierarchical) {
                        var optionsH = _processExternalOptions(resp, this.props.config.optionsSource.hierarchyProp);
                        options = _recurseOptions(optionsH, this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource, 0);
                    } else {

                        resp.forEach(item => {
                            options.push([
                                item[this.props.config.optionsSource.valSource],
                                ewars.I18N(item[this.props.config.optionsSource.labelSource]),
                                item
                            ])
                        });

                        if (this.props.config) {
                            if (this.props.config.optionsSource) {
                                if (this.props.config.optionsSource.additional) {
                                    this.props.config.optionsSource.additional.forEach(option => {
                                        var optionValue = option[0];
                                        if (optionValue == null) optionValue = "null";
                                        options.unshift([optionValue, option[1]]);
                                    });
                                }
                            }
                        }

                        this.state.rawOptions = resp;
                    }

                    this.state.options = options;
                    this._isLoaded = true;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    render: function () {
        var dataClassName = "ew-select-data";

        if (!this._initialLoaded) {
            return (
                <div className="ew-select">
                    <div className="handle">
                        <i className="fal fa-spin fa-circle-o-notch"></i>
                    </div>
                </div>
            )
        }


        var multiple = this.props.config.multiple;
        if (multiple) {
            dataClassName += " multi-select";
        }

        if (this.props.config) {
            if (this.props.config.multiple != undefined || this.props.config.multiple != null) {
                multiple = this.props.config.multiple;
            }
        }

        var config = this._processConfig();

        if (this.props.readOnly && !multiple) {
            var value = this._getOptionDisplay();
            return (
                <input type="text" disabled={true} value={value}/>
            )
        }

        var rawOptions = this._getOptions();

        let ItemTemplate = this.props.ItemTemplate || this.props.config.ItemTemplate;
        let templateName = (this.props.template || this.props.config.template) || null;
        if (templateName) ItemTemplate = PRESET_TEMPLATES[templateName];
        if (!ItemTemplate) ItemTemplate = Item;

        let options = rawOptions.map(option => {
            let id = ewars.utils.uuid();

            let item;
            if (ItemTemplate) {
                item = (
                    <ItemTemplate
                        readOnly={this.props.readOnly}
                        multiple={multiple}
                        data={option}
                        onClick={this.onChange}
                        value={this.props.value}
                        key={id}/>
                )
            }

            if (multiple) {
                item = (
                    <MultiSelectWrapper
                        readOnly={this.props.readOnly}
                        data={option}
                        onClick={this.onChange}
                        value={this.props.value}>
                        {item}
                    </MultiSelectWrapper>
                )
            }

            return item;
        });


        let prefixed = [];
        if (this.props.config.optionsSource) {
            if (this.props.config.optionsSource.prefixed) {
                prefixed = this.props.config.optionsSource.prefixed;
                this.props.config.optionsSource.prefixed.forEach(item => {
                    options.unshift(
                        <div
                            key={item[0]}
                            className="item"
                            data-key={item[0]}
                            data-value={item[0]}
                            onClick={this._onPrefixSelect}>{item[1]}</div>
                    )

                    rawOptions.push(item);
                })
            }
        }

        if (!multiple) {
            options.unshift(<div
                key={this.state.placeKey}
                className="item"
                data-key={this.state.placeKey}
                data-value="null"
                onClick={this.onChange}>No
                Selection</div>);
        }

        if (this.props.config.optionsSource && !this._isLoaded) {
            options = <Spinner/>;
        }

        var onClick = multiple ? null : this.handleBodyClick;
        var show = multiple ? true : this.state.showOptions;
        let handleClass = "ew-select";
        if (show) handleClass += " ew-select-open";


        return (
            <div ref="selector" className={handleClass} onClick={onClick}>
                {!multiple ?
                    <Handle
                        onClick={this._toggle}
                        emptyText={this.props.emptyText}
                        value={this.props.value}
                        prefixed={prefixed}
                        options={rawOptions}/>
                    : null}
                {show ?
                    <div className={dataClassName}>
                        {options}
                    </div>
                    : null}
                {multiple && !this.props.readOnly ?
                    <div style={{padding: 8, borderTop: "1px solid #CCC"}}>
                        <a onClick={this._selectAll} href="#">Select All</a> | <a onClick={this._selectNone} href="#">Select
                        None</a>
                    </div>
                    : null}
            </div>
        )
    }
});

export default SelectField;
