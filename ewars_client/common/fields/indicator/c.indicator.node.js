var TreeNodeComponent = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            children: [],
            showChildren: false
        };
    },

    onCaretClick: function (e) {
        e.stopPropagation();
        e.preventDefault();
        this.toggleCaret();
    },

    toggleCaret: function () {
        if (this._hasLoaded) {
            this.setState({
                showChildren: this.state.showChildren ? false : true
            });
            return;
        }

        var self = this;
        ewars._connect().then(function (session) {
            session.call("com.ewars.indicators", [self.props.data.id])
                .then(function (resp) {
                    this._hasLoaded = true;

                    if (resp.d.length > 0) {
                        this.setState({
                            children: resp.d,
                            showChildren: true
                        })
                    } else {
                        this.forceUpdate();
                    }

                }.bind(self))
        })

    },

    _onLabelClick: function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (this.props.data.type == "node") {
            this.props.onNodeSelect(this.props.data);
        } else {
            this.toggleCaret();
        }
    },

    _hasChildren: function () {
        if (this.props.data.child_count > 0) return true;
        return false;
    },

    _hasIndicators: function () {
        if (this.props.data.indicator_count > 0) return true;
        return false;
    },

    _processChildren: function () {
        var childs = [];

        for (var i in this.state.children) {
            var child = this.state.children[i];

            childs.push(
                <TreeNodeComponent
                    index={i}
                    onNodeSelect={this.props.onNodeSelect}
                    data={child}/>
            )
        }

        return childs;
    },

    render: function () {

        var childs;
        if (this._hasChildren() || this._hasIndicators()) {
            childs = this._processChildren();
        }

        var iconClass = "fal fa-circle";
        if (this.props.data.type == "folder") iconClass = "fal fa-caret-right";
        if (this.props.data.type == "folder" && this._hasLoaded && this.state.showChildren) iconClass = "fal fa-caret-down";

        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.data.name);

        return (
            <li>
                <div className="node-control" onClick={this.onCaretClick}><i className={iconClass}></i></div>
                <div className="label" onClick={this._onLabelClick}>{nodeName}</div>
                {this.state.showChildren ?
                    <div className="children">
                        <ul>
                            {childs}
                        </ul>
                    </div>
                    : null}
            </li>
        )
    }
});

export default TreeNodeComponent;
