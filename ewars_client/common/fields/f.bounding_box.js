import L from 'leaflet';

import Button from "../Button";
import Shade from "../Shade";


class GeometryEditor extends React.Component {
    _map = null;

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        let osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
        this._map = L.map(this._el, {})

        var osm = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: 20, attribution: osmAttrib});
        this._map.addLayer(osm);

        console.log(this.props);

        if (this.props.value) {
            this._map.setView(
                [this.props.value.center[0], this.props.value.center[1]],
                this.props.value.zoom);
        } else {
            this._map.setView([51.505, -0.09], 13);
        }

        let info = L.control("topright");

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'meta-container'); // create a div with a class "info"
            return this._div;
        };

        info.addTo(this._map);

        this.info = info;

        this.info.update = function (props) {
            this._div.innerHTML = `
                    <div class="metabox-title">Position</div>
                    <div class="metabox-table">
                        <div class="metabox-table-row">Zoom: ${props.zoom}</div>
                        <div class="metabox-table-row">Latitude: ${props.lat}</div>
                        <div class="metabox-table-row">Longitude: ${props.lng}</div>
                    </div>
                `;
        };

        this._map.zoomControl.setPosition('bottomright');

        this._scale = new L.control.scale();
        this._scale.addTo(this._map);


        this._map.on("zoomend moveend", this._updateRender);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this._map.setView(
                [nextProps.value.center[0], nextProps.value.center[1]],
                nextProps.value.zoom);
        }
    }

    _updateRender = () => {
        this.info.update({
            zoom: this._map.getZoom(),
            lat: this._map.getCenter().lat,
            lng: this._map.getCenter().lng
        })
        ewars.emit("POS_UPDATED", [this._map.getZoom(), this._map.getCenter()]);
    };

    render() {
        return (
            <div className="map"
                 ref={(el) => {
                     this._el = el;
                 }}
                 style={{height: "100%"}}>

            </div>
        )
    }
}

class GeoEditor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            pos: null
        };

        this._pos = this.props.value || null;

        ewars.subscribe("POS_UPDATED", this._setPos);
    }

    componentWillUnmount() {
        ewars.unsubscribe("POS_UPDATED", this._setPos);
    }

    _setPos = (data) => {
        this._pos = data;
    };

    _show = () => {
        this.setState({
            show: true
        })
    };

    _save = () => {
        this.setState({
            show: false
        });

        this.props.onUpdate("center", {
            zoom: this._pos[0],
            center: [
                this._pos[1].lat,
                this._pos[1].lng
            ]
        });
    };

    _cancel = () => {
        this.setState({
            show: false
        })
    };

    _onChange = (e) => {
        let value= this.props.value || {
            zoom: null,
            center: []
        }

        switch (e.target.name) {
            case "zoom":
                value.zoom = e.target.value;
                break;
            case 'lat':
                value.center[0] = e.target.value;
                break;
            case 'lng':
                value.center[1] = e.target.value;
                break;
            default:
                break;
        }

        this.props.onUpdate("center", value);
    };

    render() {

        let editor;

        if (this.state.show) {
            editor = (
                <Shade
                    toolbar={false}
                    onAction={this._cancel}
                    shown={true}>
                    <ewars.d.Layout>
                        <ewars.d.Toolbar label="Bounds Editor">
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    label="Save Change(s)"
                                    onClick={this._save}
                                    icon="fa-save"/>
                                <ewars.d.Button
                                    label="Cancel"
                                    onClick={this._cancel}
                                    icon="fa-times"/>
                            </div>
                        </ewars.d.Toolbar>
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <GeometryEditor
                                    value={this.props.value}
                                    ref="geoEdit"/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </Shade>
            )
        }
        return (
            <div>
                <label htmlFor="">Zoom</label>
                <input
                    name="zoom"
                    type="number"
                    onChange={this._onChange}
                    value={this.props.value.zoom || null}/>
                <label htmlFor="">Latitude</label>
                <input
                    name="lat"
                    onChange={this._onChange}
                    value={this.props.value.center[0] || ""}
                    type="text"/>
                <label htmlFor="">Longitude</label>
                <input
                    name="lng"
                    onChange={this._onChange}
                    value={this.props.value.center[1] || ""}
                    type="text"/>
                <ewars.d.Button label="Edit" onClick={this._show}/>
                {editor}
            </div>
        )
    }
}

export default GeoEditor;