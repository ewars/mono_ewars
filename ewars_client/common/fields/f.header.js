var HeaderField = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        var className = "form-header";

        if (this.props.config.header_style) {
            className += " " + this.props.config.header_style;
        } else {
            className += " style-title";
        }

        return (
            <div className={className}>
                {this.props.config.icon ?
                    <div>
                        <i className={"fal " + this.props.config.icon}></i> {__(this.props.config.label)}
                    </div>
                    :
                    __(this.props.config.label)
                }
            </div>
        )
    }
});

export default HeaderField;
