import {Layout, Row, Cell} from "../layout";
import Button from "../c.button";
import Spinner from "../c.spinner";

const LOCATION_SELECT = ["uuid", "name", "parent_id", "@children", "site_type_id", "@lineage"];

var _backupLocations = null;

var LocationTreeNode = React.createClass({
    _isLoaded: false,

    getDefaultProps: function () {
        return {
            hideInactive: true,
            lineageRoot: null,
            selectionTypeId: null
        }
    },

    getInitialState: function () {
        return {
            showLocations: false
        }
    },

    _onSelect: function () {
        this.props.onSelect(this.props.data);
    },

    _onClick: function () {
        if (this.props.selectionTypeId && this.props.data.site_type_id == this.props.selectionTypeId) {
            this._onSelect();
            return;
        }

        if (this.props.data.children <= 0) return;


        this.state.showLocations = this.state.showLocations ? false : true;
        if (this.isMounted()) this.forceUpdate();

        if (!this._isLoaded && this.state.showLocations) {
            var filters = {
                parent_id: {eq: this.props.data.uuid}
            };

            if (this.props.hideInactive) filters.status = {eq: "ACTIVE"};
            if (!this.props.hideInactive) filters.status = {neq: "DELETED"};

            ewars.tx("com.ewars.query", ["location", LOCATION_SELECT, filters, {"name.en": "ASC"}, null, null, ["location_type:site_type_id:id"]])
                .then(function (resp) {
                    this.state.locations = resp;
                    this._isLoaded = true;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    render: function () {
        var className = "item locale-select";
        if (this.props.value == this.props.data.uuid) className += " active";

        var name = ewars.I18N(this.props.data.name);
        name += " (" + ewars.I18N(this.props.data.location_type.name) + ")";

        var children;
        if (this.state.showLocations && this._isLoaded) {
            children = this.state.locations.map(location => {
                return (
                    <LocationTreeNode
                        onSelect={this.props.onSelect}
                        selectionTypeId={this.props.selectionTypeId}
                        lineageRoot={this.props.lineageRoot}
                        hideInactive={this.props.hideInactive}
                        key={location.uuid}
                        data={location}/>
                )
            });
        } else {
            children = <Spinner/>;
        }

        var iconClass = "fal";
        if (this.state.showLocations) iconClass += " fa-caret-down";
        if (!this.state.showLocations) iconClass += " fa-caret-right";
        if (!this._isLoaded && this.state.showLocations) iconClass = "fal fa-spin fa-cog";

        if (this.props.data.children <= 0) iconClass = "fal fa-map-marker";
        if (this.props.selectionTypeId && this.props.data.site_type_id == this.props.selectionTypeId) iconClass = "fal fa-map-marker";

        var childStyle = {display: "none"};
        if (this.state.showLocations) childStyle.display = "block";

        var handleClass = "locale-tree-node";
        if (this.props.data.site_type_id == this.props.selectionTypeId) handleClass += " green";

        var showSelect = false;
        if (!this.props.selectionTypeId || this.props.selectionTypeId == this.props.data.site_type_id) showSelect = true;
        if (showSelect) handleClass += " has-button";

        let lineage;
        if (this.props.data['@lineage']) {
            lineage = (this.props.data['@lineage'] || []).join(' \\ ');
        }

        return (
            <div className={className}>
                <div className={handleClass} onClick={this._onClick}>
                    <div className="expander"><i className={iconClass}></i></div>
                    <div className="labeler">
                        {name}
                        {lineage ? <br/> : null}
                        {lineage ?
                            <i>{lineage}</i>
                        : null}

                    </div>
                    {showSelect ?
                        <div className="ew-select-btn">
                            <Button
                                icon="fa-caret-right"
                                onClick={this._onSelect}/>
                        </div>
                        : null}
                </div>
                <div className="locale-tree-node-children" style={childStyle}>
                    {children}
                </div>
            </div>
        )
    }
});

var Handle = React.createClass({
    render: function () {
        var name = "No Selection";

        if (this.props.value.uuid) {
            name = ewars.I18N(this.props.value.name);
            name += " (" + ewars.I18N(this.props.value.location_type.name) + ")";
        }
        return (
            <div className="handle" onClick={this.props.onClick}>
                <Layout>
                    <Row>
                        <Cell addClass="handleText">
                            {name}
                        </Cell>
                        <Cell width={31} addClass="handleIcon">
                            <i className="fal fa-caret-down"></i>
                        </Cell>
                    </Row>
                </Layout>
            </div>
        )
    }
});


function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

class SearchInput extends React.Component {
    static defaultProps = {
        value: ''
    };

    constructor(props) {
        super(props);

        this.sendForSearch = debounce(this.sendForSearch, 250);
    }

    componentDidMount() {
        this._el.focus();
    }

    sendForUpdate = (e) => {
        this.props.onChange(e.target.value);

        if (e.target.value.length > 2) {
            this.sendForSearch(e.target.value);
        }

        if (e.target.value == "") this.sendForSearch(null);
    };

    sendForSearch = (searchTerm) => {
        this.props.onSearch(searchTerm);
    };

    render() {
        return (
            <input
                type="text"
                ref={(el) => {this._el = el;}}
                onChange={this.sendForUpdate}
                value={this.props.value}/>
        )
    }
}

var LocationSelectField = React.createClass({
    _isLoaded: false,
    _initialLoad: false,
    _isSearch: false,

    propTypes: {
        _onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        emptyText: React.PropTypes.string
    },

    getDefaultProps: function () {
        return {
            config: {
                allowCheck: false,
                location_type: null,
                hideInactive: false,
                parentId: null,
                selectionTypeId: null,
                lineageRoot: null
            },
            emptyText: "No location selected"
        }
    },

    getInitialState: function () {
        return {
            showLocations: false,
            location: {
                uuid: null
            },
            search: ""
        };
    },

    _init: function (props) {
        if (props.value) {
            ewars.tx("com.ewars.resource", ["location", props.value, ["uuid", "name", "site_type_id"], ["location_type:site_type_id:id"]])
                .then(function (resp) {
                    if (resp) {
                        this.state.location = resp;
                    } else {
                        this.state.location = {
                            uuid: null,
                            name: "Unknown location"
                        }
                    }
                    this._initialLoad = true;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    componentWillMount: function () {
        if (this.props.value && this.props.value != this.state.location.uuid) {
            this._init(this.props);
        } else {
            this._initialLoad = true;
        }
    },

    componentWillReceiveProps: function (nextProps) {
        if (this.props.value != nextProps.value) {
            if (!nextProps.value) {
                this.state.location = {};
            } else {
                this._initialLoad = false;
                if (nextProps.value) this._init(nextProps);
            }
        } else {
            this._initialized = true;
        }
    },

    componentDidMount: function () {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function () {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
    },

    componentDidUpdate: function () {
        if (this.state.showLocations) {
        }
    },

    handleBodyClick: function (evt) {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showLocations = false;
                this.forceUpdate();
            }
        }
    },

    handleClick: function (e) {
        e.stopPropagation();
    },

    _onChange: function (location) {
        let name = this.props.config.nameOverride || this.props.name;
        let path = this.props.config.path || null;

        this.state.showLocations = false;

        if (this._isSearch) {
            this._isSearch = false;
            this.state.search = "";
            this.state.locations = JSON.parse(_backupLocations);
        }

        this.state.location = location;
        this.props.onUpdate(name, location.uuid, path);
    },

    _toggle: function (e) {
        e.stopPropagation();
        this.state.showLocations = this.state.showLocations ? false : true;

        this.forceUpdate();

        if (this.state.showLocations && !this._isLoaded) {
            // We need to load the options
            var resource = "location";
            var select = ["uuid", "name", "@children", "site_type_id"];
            var filters = {};
            if (this.props.config.hideInactive) filters.status = {eq: "ACTIVE"};
            if (!this.props.config.hideInactive) filters.status = {neq: "DELETED"};

            if (window.user.role != "SUPER_ADMIN") {
                if (window.user.role == "REGIONAL_ADMIN") {
                    filters.uuid = {eq: window.user.location_id}
                } else {
                    filters.uuid = {eq: window.user.clid}
                }
            } else {
                filters.parent_id = {eq: "NULL"}
            }

            ewars.tx("com.ewars.query", [resource, select, filters, null, null, null, ["location_type:site_type_id:id"]])
                .then(function (resp) {
                    this.state.locations = resp;
                    this._isLoaded = true;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    _resetLocations: function() {
        var resource = "location";
        var select = ["uuid", "name", "@children", "site_type_id"];
        var filters = {};
        if (this.props.config.hideInactive) filters.status = {eq: "ACTIVE"};
        if (!this.props.config.hideInactive) filters.status = {neq: "DELETED"};

        if (window.user.role != "SUPER_ADMIN") {
            if (window.user.role == "REGIONAL_ADMIN") {
                filters.uuid = {eq: window.user.location_id}
            } else {
                filters.uuid = {eq: window.user.clid}
            }
        } else {
            filters.parent_id = {eq: "NULL"}
        }

        ewars.tx("com.ewars.query", [resource, select, filters, null, null, null, ["location_type:site_type_id:id"]])
            .then((resp) => {
                this._isLoaded = true;
                this.setState({
                    locations: resp,
                    search: ''
                })
            })
    },

    _clearSearch: function () {
        this._resetLocations();
    },

    _onSearchChange: function (val) {
        this.setState({
            search: val
        })
    },

    _onSearch: function (val) {
        if (val == null || val == '') {
            this._resetLocations();
        } else {
            var query = {
                "name.en": {like: val}
            };
            if (this.props.config.parentId) {
                query.lineage = {has: this.props.config.parentId};
            } else {
                if (window.user.role != "SUPER_ADMIN") {
                    if (window.user.role == "ACCOUNT_ADMIN") {
                        query.lineage = {under: window.user.clid};
                    } else if (window.user.role == "REGIONAL_ADMIN") {
                        query.lienage = {under: window.user.location_id};
                    } else {
                        query.lineage = {under: window.user.clid};
                    }
                }
            }


            if (this.state.search.length >= 2) {
                ewars.tx("com.ewars.query", ["location", LOCATION_SELECT, query, null, null, null, ["location_type:site_type_id:id"]])
                    .then((resp) => {
                        this.setState({
                            locations: resp
                        });
                    })
            }
        }
    },

    render: function () {
        if (!this._initialLoad) {
            return (
                <div className="ew-select">
                    <div className="handle">
                        <i className="fal fa-spin fa-circle-o-notch"></i>
                    </div>
                </div>
            )
        }

        if (this.props.readOnly) {
            var name = ewars.I18N(this.state.location.name);
            name += " (" + ewars.I18N(this.state.location.name) + ")";
            return <input type="text" disabled={true} value={name}/>
        }

        var parent_id;
        if (this.props.config) {
            if (this.props.config.parent_id) parent_id = this.props.config.parent_id;
        }

        var rootNodes;
        if (!this._isLoaded && this.state.showLocations) rootNodes = <Spinner/>;

        if (this._isLoaded && this.state.showLocations) {
            rootNodes = this.state.locations.map(location => {
                return (
                    <LocationTreeNode
                        onSelect={this._onChange}
                        selectionTypeId={this.props.config.selectionTypeId || this.props.config.sti}
                        lineageRoot={this.props.config.lineageRoot}
                        hideInactive={this.props.config.hideInactive}
                        key={location.uuid}
                        data={location}/>
                )
            });
        }

        return (
            <div ref="selector" className="ew-select locale-selector" onClick={this.handleBodyClick}>
                <Handle
                    onClick={this._toggle}
                    readOnly={this.props.readOnly}
                    value={this.state.location}/>
                {this.state.showLocations ?
                    <div className="ew-select-data">
                        <div className="locale-search">
                            <div className="input">
                                <SearchInput
                                    value={this.state.search}
                                    onChange={this._onSearchChange}
                                    onSearch={this._onSearch}/>
                                {this.state.search.length > 0 ?
                                    <div className="search-button" onClick={this._clearSearch}>
                                        <i className="fal fa-times-circle"></i>
                                    </div>
                                    : null}
                            </div>
                        </div>
                        {rootNodes}
                    </div>
                    : null}
            </div>
        )
    }
});

export default LocationSelectField;
