export default [
    {
        _o: 0,
        l: "Data Source",
        t: "SELECT",
        n: "source",
        o: [
            ["form", "Forms"],
            ["location", "Locations"],
            ["alert", "Alerts"],
            ["alarm", "Alarms"]
        ]
    }
]