const FORM_SERIES_X = [
    {
        _o: 0,
        n: "type",
        l: "Type",
        t: "SELECT",
        o: [
            ["time", "Time"],
            ["location", "Location"]
        ]
    },
    {
        _o: 1,
        n: "x_b_title",
        l: "Show Title",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 1.1,
        n: "x_title",
        l: "X Axis Title",
        t: "TEXT",
        c: [
            "ALL",
            "x_b_title:=:true"
        ]
    }
]

export default FORM_SERIES_X;