const LOCATIONS = {
    status: {
        type: "select",
        label: "Status",
        options: [
            ["ACTIVE", "Active"],
            ["DISABLED", "Disabled"]
        ]
    },
    site_type_id: {
        type: "select",
        label: "Location Type",
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    }
};

export default LOCATIONS;
