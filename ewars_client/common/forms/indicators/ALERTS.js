const ALERTS = {
    alarm_id: {
        type: "select",
        label: "Alarm",
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            additionalOptions: [
                ["ANY", "Any"]
            ]
        }
    },
    dimension: {
        type: "select",
        label: "Dimension",
        options: [
            ["ALERTS_TRIGGERED", "Alerts triggered"],
            
            ["ALERTS_OPEN", "Open alerts"],
            ["ALERTS_CLOSED", "Closed alerts (incl. auto-discarded)"],
            ["ALERTS_CLOSED_NON_AUTO", "Closed alerts (not incl. auto-discarded"],
            ["ALERTS_AUTO_DISCARDED", "Auto-discarded alerts"],

            ["ALERTS_DISCARDED", "Alerts discarded"],
            ["ALERTS_MONITORED", "Alerts monitor"],
            ["ALERTS_RESPOND", "Alerts response"],

            ["ALERTS_IN_VERIFICATION", "Alerts in verification"],
            ["ALERTS_AWAIT_VERIFICATION", "Alerts awaiting verification"],
            ["ALERTS_VERIFIED", "Alerts verified"],
            ["ALERTS_DISCARDED_VERIFICATION", "Alerts discarded at verification"],
            ["ALERTS_MONITORED_VERIFICATION", "Alerts monitoring at verification"],

            ["ALERTS_IN_RISK_ASSESS", "Alerts in risk assessment"],
            ["ALERTS_AWAIT_RISK_ASSESS", "Alerts awaiting risk assessment"],
            ["ALERTS_RISK_ASSESSED", "Alerts risk assessed"],

            ["ALERTS_IN_RISK_CHAR", "Alerts in risk characterization"],
            ["ALERTS_AWAIT_RISK_CHAR", "Alerts awaiting risk characterization"],
            ["ALERTS_RISK_CHAR", "Alerts risk characterized"],

            ["ALERTS_IN_OUTCOME", "Alerts in outcome"],
            ["ALERTS_AWAIT_OUTCOME", "Alerts awaiting outcome"],
            ["ALERTS_OUTCOME", "Alerts with outcome"],
            ["ALERTS_DISCARD_OUTCOME", "Alerts discarded at outcome"],
            ["ALERTS_MONITORED_OUTCOME", "Alerts monitoring at outcome"],
            ["ALERTS_RESPOND_OUTCOME", "Alerts respond at outcome"],

            ["ALERTS_RISK_LOW", "Alerts (low risk)"],
            ["ALERTS_RISK_MODERATE", "Alerts (moderate risk)"],
            ["ALERTS_RISK_HIGH", "Alerts (high risk)"],
            ["ALERTS_RISK_SEVERE", "Alerts (very high risk)"]
        ]
    }
};

export default ALERTS;

