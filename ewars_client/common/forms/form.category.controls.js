const FORM_CATEGORY_CONTROLS = [
    {
        _o: 0,
        n: "tools",
        t: "BUTTONSET",
        l: "Show tools",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 1,
        n: "show_title",
        t: "BUTTONSET",
        l: "Show title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 2,
        n: "show_legend",
        t: "BUTTONSET",
        l:" Show legend",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 3,
        n: "legend_position",
        t: "SELECT",
        l: "Legend position",
        o: [
            ["TL", "Top-left"],
            ["TC", "Top-center"],
            ["TR", "Top-right"],
            ["MR", "Middle-right"],
            ["BR", "Bottom-right"],
            ["BC", "Bottom-center"],
            ["BL", "Bottom-left"],
            ["ML", "Middle-left"]
        ]
    },
    {
        _o: 4,
        n: "legend_label_source",
        t: "SELECT",
        l: "Legend label source",
        o: [
            ["DEFAULT", "Source title"],
            ["CUSTOM", "Custom"],
        ],
        c: [
            "ALL",
            "legend:eq:true"
        ]
    },
    {
        _o: 5,
        n: "legend_lbl_source_custom",
        t: "TEXT",
        l: "Custom legend label",
        c: [
            "ALL",
            "legend_label_source:eq:CUSTOM"
        ]
    }
]

export default FORM_CATEGORY_CONTROLS;