const FORM_ACTIVITY = [
    {
        _o: 0,
        t: "TEXT",
        l: "Title",
        n: "title"
    },
    {
        _o: 1,
        t: "TEXT",
        l: "Update Interval (minutes)",
        n: "int_update"
    }
]

export default FORM_ACTIVITY;