const FORM_DASHBOARD_STYLE = [
    {
        _o: 0,
        n: "icon",
        t: "TEXT",
        l: "Dashboard Icon"
    },
    {
        _o: 1,
        n: "headerColor",
        t: "COLOR",
        l: "Header background colour"
    },
    {
        _o: 2,
        n: "headerTextColor",
        t: "COLOR",
        l: "Header text colour"
    },
    {
        _o: 3,
        n: "widgetBackgroundColor",
        l: "Widget background colour",
        t: "COLOR"
    },
    {
        _o: 4,
        n: "widgetTextColor",
        l: "Widget text colour",
        t: "COLOR"
    },
    {
        _o: 5,
        n: "widgetSubText",
        l: "Widgest sub-text",
        t: "TEXT"
    },
    {
        _o: 6,
        n: "widgetFooterText",
        l: "Widget footer text",
        t: "TEXT"
    }
]

export default FORM_DASHBOARD_STYLE;