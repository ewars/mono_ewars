const FORM_LAYOUT_DASHBOARD_THEME = [
    {
        _o: 0,
        n: "forceHeight",
        l: "Fitted Height?",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 1,
        t: "COLOR",
        l: "Background colour",
        n: "color"
    }
]

export default FORM_LAYOUT_DASHBOARD_THEME;