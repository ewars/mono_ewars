import { html, css, LitElement } from "lit-element";


class ActionButton extends React.Component {
    static defaultProps = {
        height: "20px",
        tip: null
    };

    constructor(props) {
        super(props);

        this.state = {}
    }

    onClick = () => {
        this._hideHover();
        if (this.props.onClick) {
            this.props.onClick();
        } else {
            this.props.onAction(this.props.a);
        }
    };

    componentDidMount() {
        if (this.props.tip) {
            this.el.addEventListener('mouseover', this._showHover);
            this.el.addEventListener('mouseout', this._hideHover);
        }
    }

    componentWillUnmount() {
        if (this.props.tip) {
            this.el.removeEventListener('mouseover', this._showHover);
            this.el.removeEventListener('mouseout', this._hideHover);
        }
    }

    _showHover = () => {
        this._hover = document.createElement('div');
        if (this._hover) {
            this._hover.setAttribute('class', 'action-hover');
            this._hover.appendChild(document.createTextNode(this.props.tip));
            document.body.appendChild(this._hover);
        }
    };

    _hideHover = () => {
        if (this._hover) {
            document.body.removeChild(this._hover);
        }
    };
    render() {
        let style = {
            height: this.props.height,
            lineHeight: this.props.height
        };

        return (
            <div
                ref={(el) => {this.el = el;}}
                className="action-button"
                style={style}
                onClick={this.onClick}>
                <i className={"fal " + this.props.i}></i>
            </div>
        )
    }
}

class LitActionButton extends LitElement {
    static get properties() {
        return {
            i: {type: String},
            a: {type: String},
            tip: {type: String},
            height: {type: Number}
        }
    }

    constructor() {
        super();

        this.height = 20;
    }

    _onClick = (e) => {
        this.dispatchEvent(new CustomEvent("action-click", {detail: this.a}));
    };

    _hover = (e) => {
        if (this.tip) {
            this._hover = document.createElement("div");
            this._hover.setAttribute("class", "action-hover");
            this._hover.appendChild(document.createTextNode(this.tip));
            document.body.appendChild(this._hover);
        }
    };

    _hoverLeave = (e) => {
        if (this._hover) {
            document.body.removeChild(this._hover);
        }
    };

    connectedCallback() {
        super.connectedCallback();
        this.addEventListener("mouseover", this._hover);
        this.addEventListener("mouseout", this._hoverLeave);
    }

    disconnectedCallback() {
        this.removeEventListener("mouseover", this._hover);
        this.removeEventListener("mouseout", this._hoverLeave);
    }

    createRenderRoot() {
        return this;
    }

    render() {
        let iconClass = "fal " + this.i;
        return html`
            <div class="action-button" @click="${this._onClick}">
                <i class="${iconClass}"></i>
            </div>
        `;
    }
}
window.customElements.define("action-button", LitActionButton);

class ActionGroup extends React.Component {
    static defaultProps = {
        height: "20px"
    };

    render() {
        let className = "action-group";
        if (this.props.right) {
            className += " pull-right";
        }

        let style = {
            height: this.props.height,
            lineHeight: this.props.height
        };

        return (
            <span className={className} style={style}>
                {this.props.actions.map((action) => {
                    return (
                        <ActionButton
                            key={action[1]}
                            height={this.props.height}
                            onAction={(action) => {
                                this.props.onAction(action)
                            }}
                            tip={action[2] || null}
                            i={action[0]}
                            a={action[1]}/>
                    )
                })}
            </span>
        )
    }
}


class LitActionGroup extends LitElement {
    static get properties() {
        return {
            actions: {type: Array},
            height: {type: String},
            right: {type: Boolean}
        }
    }

    static get styles() {
        return css`
            :host {
                display: inline-block;
                line-height: 20px;
                height: 20px;
            }
        `
    }

    constructor() {
        super();
        this.height = "20px";
        this.actions = [];
        this.right = false;
    }

    _onClick = (e) => {
        console.log("HERE", e, e.detail);
        this.dispatchEvent(new CustomEvent("actionSelected", {detail: e.detail}));
    };

    createRenderRoot() {
        return this;
    }

    render() {
        let className = "action-group";
        if (this.right) className += " pull-right";
        return html`
            <style>
                :host {
                    height: ${this.height}px;
                }
            </style>
            ${this.actions.map(item => {
                return html`
                    <action-button
                    @action-click="${this._onClick}"
                    .tip="${item[2] || null}"
                    .i="${item[0]}"
                    .a="${item[1]}"
                    .height="${this.height}" />
                `;
            })}
        `;
    }
}

window.customElements.define("action-group", LitActionGroup);

export {
    LitActionGroup,
    ActionButton,
    ActionGroup
}
