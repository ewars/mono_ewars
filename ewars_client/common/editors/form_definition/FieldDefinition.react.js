import FieldMapper from "../../utils/FieldMapper";
// import FIELD_DEFINITION from "../../../common/constants/field_definition";
import { FIELD_FORM } from "../../constants/field_definition";
import SystemForm from "../../c.form.system";

const SORT_ORDER = (a, b) => {
    let aO = parseInt(a.order),
        bO = parseInt(b.order);
    if (aO > bO) return 1;
    if (aO < bO) return -1;
    return 0;
}

const _statusMap = [
    ["DISABLED", "Disabled"],
    ["ENABLED", "Enabled"]
];

const _statusDict = {
    "DISABLED": "Disabled",
    "ENABLED": "Enabled"
};

const ICONS = {
    text: "fa-font",
    textarea: "fa-align-left",
    date: "fa-calendar",
    display: "fa-square",
    number: "fa-hashtag",
    location: 'fa-map-marker',
    lat_long: "fa-location-arrow",
    header: "fa-heading",
    matrix: "fa-th",
    row: "fa-rectangle-wide",
    select: "fa-list-alt",
    calculated: "fa-calculator"
};


function _getArrayAsObject(sourceArray) {
    var fields = {};

    for (var index in sourceArray) {
        var field = sourceArray[index];
        fields[field.name] = field;
        fields[field.name].order = index;

        if (field.fields) {
            fields[field.name].fields = _getArrayAsObject(field.fields);
        }

        delete fields[field.name].name;
    }

    return fields;
}

function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields) {
    var fields = [];
    for (var token in sourceFields) {
        var field = JSON.parse(JSON.stringify(sourceFields[token]));
        field.name = token;

        if (field.fields) {
            var children = _getFieldsAsArray(field.fields);
            field.fields = children;
        }

        fields.push(field);
    }


    return fields.sort(SORT_ORDER);
}

var FieldDefinition = React.createClass({
    getInitialState: function () {
        return {
            showDetails: false
        };
    },

    propTypes: {
        data: React.PropTypes.object.isRequired
    },

    componentWillMount: function () {
        if (ewars.g.activeFields) {
            if (ewars.g.activeFields.indexOf(this.props.data.uuid) >= 0) {
                this.state.showDetails = true
            }
        }

        window.addEventListener("show-drops", this._showDrops);
        window.addEventListener("hide-drops", this._hideDrops);
    },

    componentWillUnmount() {
        window.removeEventListener("show-drop", this._showDrops);
        window.removeEventListener("hide-drops", this._hideDrops);
    },

    _showDrops: function (uuid) {
        if (uuid != this.props.data.uuid) {
            if (this._dropEl) {
                this._dropEl.style.display = "block";
            }
        }
    },

    _hideDrops: function () {
        if (this._dropEl) {
            this._dropEl.style.display = "none";
        }
    },

    formatLabel: function (labelValue) {
        if (!labelValue) return "Untitled Field";
        if (labelValue.length >= 70) {
            return labelValue.substring(0, 70) + "...";
        }
        return labelValue;
    },

    onPropChange: function (name, value) {
        this.props.onChange(this.props.data.uuid, name, value);
    },

    _hasChildren: function () {
        var hasChildren = false;

        if (this.props.data.type == "row" || this.props.data.type == "matrix") {
            if (this.props.data.fields) {
                return [true, this.props.data.fields];
            } else {
                return [true, this.props.data.fields];
            }
        } else {
            return [false, []]
        }

        return [false, []]
    },

    _processChildren: function () {
        var fields;

        if (!this.props.data.fields) return [];
        let subFields = Object.keys(this.props.data.fields).map(key => {
            let field = ewars.copy(this.props.data.fields[key]);
            field.name = key;
            return field;
        });

        let sortedFields = subFields.sort(SORT_ORDER);

        if (sortedFields.length > 0) {
            fields = sortedFields.map((item, index) => {
                let order = index;
                if (this.props.order) order = `${this.props.order}.${index + 1}`
                return (
                    <FieldDefinition
                        fieldOptions={this.props.fieldOptions}
                        data={item}
                        parentName={item.name}
                        onDelete={this.props.onDelete}
                        onChange={this.props.onChange}
                        moveFieldUp={this.props.moveFieldUp}
                        moveFieldDown={this.props.moveFieldDown}
                        onAddChild={this.props.onAddChild}
                        onDuplicate={this.props.onDuplicate}
                        index={item.uuid}
                        order={order}
                        key={item.uuid}/>
                )
            });
        } else {
            fields = (
                <p className="box-placeholder">There are currently no fields configured,
                    <a href="#" onClick={this.addChild}>Add one</a>
                </p>
            )
        }

        return fields;
    },

    addChild: function () {
        if (this.props.data.type == "row") {
            if (this.props.data.fields) {
                if (this.props.data.fields.length >= 6) {
                    ewars.notifications.notification("fa-warning", "Maximum Six", "A row can only contain a maximum of six items");
                    return;
                }
            }
        }
        this.props.onAddChild(this.props.data.uuid);
    },

    _action: function (action) {
        switch (action) {
            case "UP":
                this.props.moveFieldUp(this.props.data.uuid, -1);
                break;
            case "DOWN":
                this.props.moveFieldDown(this.props.data.uuid, 1);
                break;
            case "DUPE":
                this.props.onDuplicate(this.props.data.uuid);
                break;
            case "TOGGLE":
                ewars.g.activeFields = ewars.g.activeFields != null ? ewars.g.activeFields : [];
                if (ewars.g.activeFields.indexOf(this.props.data.uuid) < 0) {
                    ewars.g.activeFields.push(this.props.data.uuid);
                } else {
                    let idx = ewars.g.activeFields.indexOf(this.props.data.uuid);
                    ewars.g.activeFields.splice(idx, 1);
                }

                this.setState({
                    showDetails: this.state.showDetails ? false : true
                });
                break;
            case "DELETE":
                this.props.onDelete(this.props.data.uuid);
                break;
            case "ADD":
                this.addChild();
                break;
            default:
                break;
        }

    },

    _dragStart: function (e) {
        e.dataTransfer.setData("e", this.props.data.uuid);
        setTimeout(() => {
            window.dispatchEvent(new CustomEvent("show-drops"));
        }, 500)
    },

    _dragEnd: function () {
        window.dispatchEvent(new CustomEvent("hide-drops"));
    },

    _drop: function (e) {
        e.preventDefault();

        let data = e.dataTransfer.getData("e");
        let _data = JSON.parse(data);
        if (_data.new == true) {
            let newField = {
                type: _data.type,
                label: "New field",
                required: false,
                uuid: ewars.utils.uuid()
            }
            this.props.dropNew(newField, this.props.data.uuid);
        } else {
            this.props.bigMove(data, this.props.data.uuid);
        }
    },

    render: function () {
        var children = [];
        var hasChildren = this._hasChildren();
        if (hasChildren[0]) children = this._processChildren();

        var labelShort = this.formatLabel(ewars.I18N(this.props.data.label));

        var fieldTypeName = FieldMapper._types[this.props.data.type];

        // var order = parseInt(this.props.data.order) + 1;
        let order = this.props.order;

        let showClass = "fal fa-square-plus";
        if (this.state.showDetails) showClass = "fa fa-square-minus";

        let name = this.props.data.name || this.props.index;

        let expander = this.state.showDetails ? "fa-minus-square" : "fa-plus-square";
        const ACTIONS = [
            ["fa-angle-up", "UP"],
            ["fa-angle-down", "DOWN"],
            ["fa-copy", "DUPE"],
            ["fa-trash", "DELETE"],
            [expander, "TOGGLE"]
        ];

        if (hasChildren[0]) {
            ACTIONS.push(["fa-plus", "ADD"])
        }

        let formDefinition;
        // If we're showing the details for the field
        if (this.state.showDetails) {
            // Copy the field form definition
            formDefinition = ewars.copy(FIELD_FORM);

            // If the current field is a select field, find the
            if (this.props.data.type == 'select') {
                formDefinition.forEach(item => {
                    if (item.n == 'default') {
                        item.t = 'SELECT';
                        item.o = this.props.data.options;
                    }
                })
            }

            formDefinition.forEach(item => {
                if (item.t == "FIELDRULES") item.fo = this.props.fieldOptions;
                if (item.t == "CALC_FIELD_DEF") item.fo = this.props.fieldOptions;
            })
            // formDefinition[formDefinition.length - 1].fo = this.props.fieldOptionsons;
        }

        let style = {
            padding: 0,
            background: "rgba(0,0,0,0.1)",
            border: "1px solid transparent"
        };
        if (this.state.showDetails) style.border = "1px solid #F2F2F2";

        let isRoot = this.props.isRoot;

        let style_inner = {padding: 0};
        if (this.props.data.type == 'header') style_inner.background = "rgb(60, 146, 122)";

        let icon = <i className={"fal " + ICONS[this.props.data.type] || "fa-cog"}></i>;

        return (
            <div
                className="block"
                style={style}>
                <div
                    className="block-content"
                    style={style_inner}>
                    <ewars.d.Row height="35px">
                        <div style={{maxWidth: "30px", display: "block", padding: '8px 0px 8px 5px'}}>
                            {order}
                        </div>
                        {this.props.data.unique ?
                            <div style={{maxWidth: "20px", display: "block", padding: "8px 0px 8px 5px", textAlign: "center"}}>
                                <i className="fal fa-key" style={{color: "orange"}}></i>
                            </div>
                        : null}
                        <div style={{maxWidth: "20px", display: "block", padding: "8px 0px 8px 5px", textAlign: "center"}}>
                            {icon}
                        </div>
                        <ewars.d.Cell style={{padding: "8px", display: "block"}}>
                            <strong>{labelShort}</strong> - [{(name || "").replace("DUPE_", "")}]
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{display: "block", flexBasis: 0, flexGrow: 1}}>
                            <ewars.d.ActionGroup
                                height="30px"
                                right={true}
                                onAction={this._action}
                                actions={ACTIONS}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.showDetails ?
                    <div style={{border: "1px solid #262626"}}>
                        <SystemForm
                            definition={formDefinition}
                            enabled={true}
                            onChange={this.onPropChange}
                            data={this.props.data}/>
                    </div>
                    : null}
                {hasChildren[0] ?
                    <div style={{border: "1px solid #262626"}}>
                        <div className="block" style={{position: "relative"}}>
                            {children}
                        </div>
                    </div>
                    : null}
                {isRoot ?
                    <div
                        ref={(el) => {
                            this._dropEl = el;
                        }}
                        style={{
                            display: "none"
                        }}
                        onDragOver={(e) => {
                            e.preventDefault();
                        }}
                        className="field-drop-area"
                        onDrop={this._drop}>Drop field here</div>
                    : null}
            </div>
        );
    }
});

export default FieldDefinition;
