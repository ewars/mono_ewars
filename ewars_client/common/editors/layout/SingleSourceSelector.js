const FORM = [
    {
        _o: 0,
        t: "H",
        l: "General",
        i: "fa-cog"
    },
    {
        _o: 1,
        n: "code",
        l: "Code",
        t: "TEXT"
    },
    {
        _o: 2,
        t: "SELECT",
        l: "Reduction",
        n: "reduction",
        o: [
            ["SUM", "Sum"],
            ["AVERAGE", "Average"],
            ["MEDIAN", "Median"],
            ["MEAN", "Mean"]
        ]
    },
    {
        _o: 3,
        t: "H",
        i: "fa-database",
        l: "Data Source(s)"
    },
    {
        _o: 4,
        t: "SOURCES",
        l: "Data Sources",
        n: "sources"
    },
    {
        _o: 6,
        t:"H",
        l: "Location",
        i: "fa-map-marker"
    },
    {
        _o: 7,
        t: "LOCATION",
        l: "Location",
        n: "location"
    },
    {
        _o: 9,
        t: "H",
        i: "fa-calendar",
        l: "Source Time Period"
    },
    {
        _o: 10,
        t: "PERIOD",
        l: "Period",
        n: "period"
    },
];


class SingleSourceSelector extends React.Component {
    _onChange = (prop, value) => {
        let data = ewars.copy(this.props.data.source || {});
        data[prop] = value;

        ewars.z.dispatch("E", "WIDGET_UPDATE_PROP", [this.props.id, "source", data]);
    };

    render() {

        let form = ewars.copy(FORM);
        let data = this.props.data.source || {};
        if (data.sources) {
            if (data.sources.length > 1) {
                form.push({
                    _o: 5,
                    t: "TEXT",
                    l: "Formula",
                    n: "formula"
                })
            }
        }

        return (
            <ewars.d.Panel>
                <ewars.d.SystemForm
                    enabled={true}
                    vertical={true}
                    onChange={this._onChange}
                    data={this.props.data.source || {}}
                    definition={form}/>
            </ewars.d.Panel>

        )
    }
}

export default SingleSourceSelector;