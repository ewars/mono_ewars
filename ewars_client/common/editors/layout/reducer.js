const _flatten = (definition, parentId) => {
    let items = {};

    definition.forEach((item, i) => {
        items[item._] = item;

        if (item.i) {
            let subItems = _flatten(item.i, null);
            for (let t in subItems) {
                items[subItems[t]._] = subItems[t];
            }
        }
    })

    return items;

};

const _resizeCells = (state, rowId, row) => {
    let flattened = _flatten(state.definition, null);

    row.forEach((item) => {
        if (flattened[item._]) {
            flattened[item._].w = item.w;
        } else {
            flattened[item._] = item;
            let parent = item._.split(".");
            parent = parent.slice(0, parent.length - 1);
            parent = parent.join(".");
            flattened[parent].i.push(item);
        }
    });

    let roots = [];

    for (let i in flattened) {
        if (i.split(".").length == 1) {
            roots.push(flattened[i]);
        }
    }

    state.definition = roots;

    return state;
}

const _resizeRows = (state, cellId, cell) => {
    let flattened = _flatten(state.definition, null);

    cell.forEach((item) => {
        if (flattened[item._]) {
            flattened[item._].h = item.h;
        } else {
            flattened[item._] = item;
            let parent = item._.split(".");
            parent = parent.slice(0, parent.length - 1);
            parent = parent.join(".");
            flattened[parent].i.push(item);
        }
    });

    // let total = 0;
    // cell.forEach((item) => {
    //     total += flattened[item._].h;
    // })
    //
    // // if total does not add up to 100, re distribute difference
    // if (total < 100) {
    //     let redist = 100 - total;
    //     cell.forEach((item) => {
    //         flattened[item._].h += redist;
    //     })
    // }

    let roots = [];

    for (let i in flattened) {
        if (i.split(".").length == 1) {
            roots.push(flattened[i]);
        }
    }

    state.definition = roots;

    return state;
}

const _resizeRow = (state, rowId, newSize) => {
    let flattened = _flatten(state.definition, null);

    flattened[rowId].h = newSize;

    let roots = [];

    for (let i in flattened) {
        if (i.split(".").length == 1) {
            roots.push(flattened[i]);
        }
    }

    state.definition = roots;

    return state;
};

const _setConstraints = (state, data) => {
    for (let i in data) {
        state[i] = data[i];
    }

    return state;
};

const _resize = (state, dir, items) => {
    let flattened = _flatten(state.definition, null);

    items.forEach((item) => {
        if (flattened[item._]) {
            flattened[item._][dir] = item[dir];
        } else {
            flattened[item._] = item;
            let parent = item._.split(".");
            parent = parent.slice(0, parent.length - 1);
            parent = parent.join(".");
            flattened[parent].i.push(item);
        }
    });

    let roots = [];

    for (let i in flattened) {
        if (i.split(".").length == 1) {
            roots.push(flattened[i]);
        }
    }

    state.definition = roots;

    return state;
};

const _swapWidget = (state, targetId, data) => {
    let flattened = _flatten(state.definition, null);

    flattened[targetId].c = ewars.copy(data.c);

    let roots = [];
    for (let i in flattened) {
        if (i.length == 1) {
            roots.push(flattened[i]);
        }
    }

    state.definition = roots;
    return state;
}

const _addRoot = (state, data) => {
    let highest = 0;

    state.definition.forEach((item) => {
        if (parseInt(item._) > highest) highest += 1;
    });

    data._ = String(highest);


    state.definition.push(data);

    return state;
}

const _addPage = (state) => {
    let highest = 0;

    state.definition.forEach((item) => {
        if (parseInt(item._) > highest) {
            highest = parseInt(item._);
        }
    });

    let newId = 0;
    if (state.definition.length > 0) newId = highest + 1;


    state.definition.push({
        t: "P",
        i: [],
        _: newId,
        lid: null
    })

    return state;
};

const _addPageItem = (state, pageId, data) => {
    state.definition.forEach((item) => {
        if (item._ == pageId) {
            let highest = 0;
            item.i.forEach((si) => {
                let itemId = si._.split(".");
                if (parseInt(itemId[itemId.length - 1]) > highest) highest = parseInt(itemId[itemId.length - 1]);
            });

            let newId = `${item._}.0`;
            if (item.i.length > 0) newId = `${item._}.${highest + 1}`;

            item.i.push({
                t: data.t,
                i: [],
                h: 100,
                _: newId
            })
        }
    });

    return state;
};

const _setLayoutProp = (state, prop, value) => {
    state.data[prop] = value;

    return state;
};

const _widgetPropChange = (state, id, prop, value) => {
    state.definition.forEach((item) => {
        if (item._ == id) {
            item.c[prop] = value;
            state.widget = item;
        }
    })

    return state;
};

const _widgetAddSeries = (state, id, data) => {
    state.definition.forEach((item) => {
        if (item._ == id) {
            if (!item.c.series) {
                item.c.series = [data];
            } else {
                if (data.uuid) {
                    let found = false;
                    item.c.series.forEach((node, index) => {
                        if (node.uuid == data.uuid) {
                            found = true;
                            item.c.series[index] = data;
                        }
                    })
                    if (!found) item.c.series.push(data);
                } else {
                    item.c.series.push({
                        ...data,
                        uuid: ewars.utils.uuid()
                    });
                }
            }
            state.widget = item;
        }
    })

    return state;
}

const _clearSeries = (state, id) => {
    let flattened = _flatten(state.definition, null);

    flattened[id].c.series = [];
    state.widget.c.series = [];

    let roots = [];
    for (let i in flattened) {
        if (i.length == 1) {
            roots.push(flattened[i]);
        }
    }

    state.definition = roots;

    return state;
};

const _updateSetting = (state, prop, value) => {
    if (!state.data.settings) state.data.settings = {};
    state.data.settings[prop] = value;
    return state;
};

const _setSeries = (state, id, series) => {
    state.definition.forEach((item) => {
        if (item._ == id) {
            item.c.series = series;
            state.widget = item;
        }
    })

    return state;
};

const _resizer = (state, cell_id, new_size) => {
    let cell = state.definition.find((item) => {
        return item._ == cell_id;
    })
    let siblings = state.definition.filter((item) => {
        return item.p == cell.p;
    })


    let curSize = cell.w;
    if (cell_id.indexOf(".") >= 0) {
        if (new_size > curSize) {
            let split = cell_id.split(".");
            let nextId = split.slice(0, split.length - 1);
            nextId.push(parseInt(split[split.length - 1]) + 1);
            nextId = nextId.join(".");

            let diff = new_size - curSize;
            siblings.forEach((item) => {
                if (item._ == nextId) {
                    item.w -= diff;
                } else if (item._ == cell_id) {
                    item.w = new_size;
                }
            })
        } else {
            let split = cell_id.split(".");
            let nextId = split.slice(0, split.length - 1);
            nextId.push(parseInt(split[split.length - 1]) + 1);
            nextId = nextId.join(".");

            let diff = Math.abs(new_size - curSize);

            siblings.forEach((item) => {
                if (item._ == nextId) {
                    item.w += diff;
                } else if (item._ == cell_id) {
                    item.w = new_size;
                }
            })
        }
    }

    siblings.forEach((item) => {
        state.definition.forEach((node) => {
            if (node._ == item._) {
                node.h = item.h;
                node.w = item.w;
            }
        })
    })


    return state;
};

const _resizerV = (state, cell_id, new_size) => {

    let cell = state.definition.find((item) => {
        return item._ == cell_id;
    })
    let siblings = state.definition.filter((item) => {
        return item.p == cell.p;
    })


    let curSize = cell.h;
    if (cell_id.indexOf(".") >= 0) {
        if (new_size > curSize) {
            let split = cell_id.split(".");
            let nextId = split.slice(0, split.length - 1);
            nextId.push(parseInt(split[split.length - 1]) + 1);
            nextId = nextId.join(".");

            let diff = Math.abs(new_size - curSize);
            siblings.forEach((item) => {
                if (item._ == nextId) {
                    item.h -= diff;
                } else if (item._ == cell_id) {
                    item.h = new_size;
                }
            })
        } else {
            let split = cell_id.split(".");
            let nextId = split.slice(0, split.length - 1);
            nextId.push(parseInt(split[split.length - 1]) + 1);
            nextId = nextId.join(".");

            let diff = Math.abs(new_size - curSize);

            siblings.forEach((item) => {
                if (item._ == nextId) {
                    item.h += diff;
                } else if (item._ == cell_id) {
                    item.h = new_size;
                }
            })
        }
    }

    siblings.forEach((item) => {
        state.definition.forEach((node) => {
            if (node._ == item._) {
                node.h = item.h;
                node.w = item.w;
            }
        })
    });


    return state;
};

const _removeNode = (state, node_id) => {
    let killWidget = false;

    let parentId;
    let newDef = [];
    state.definition.forEach((item, index) => {
        if (item._ == node_id) {
            parentId = item.p;
        }

        if (item._.indexOf(node_id) < 0) {
            newDef.push(item);
        } else {
            if (state.widget) {
                if (item._ == state.widget._) {
                    killWidget = true;
                }
            }
        }
    });

    if (parentId) {
        let parent = newDef.find((item) => {
            return item._ == parentId;
        })
        let siblings = newDef.filter((item) => {
            return item.p == parentId;
        })

        if (parent.t == "H") {
            let totalWidth = 0;
            siblings.forEach((item) => {
                totalWidth += item.w;
            })
            let diff = (100 - totalWidth) / siblings.length;
            siblings.forEach((item) => {
                item.w += Math.abs(diff);
            })

        } else if (parent.t == "V") {
            let totalHeight = 0;
            siblings.forEach((item) => {
                totalHeight += item.h;
            })
            let diff = (100 - totalHeight) / siblings.length;
            siblings.forEach((item) => {
                item.h += Math.abs(diff);
            })
        }

        siblings = siblings.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        });

        siblings.forEach((item, index) => {
            let newId = `${parentId}.${index}`;
            newDef.forEach((node, subIndex) => {
                if (node._.indexOf(item._) == 0 && node._ != item._) {
                    node._ = node._.replace(item._, newId);
                }

                if (node.p) {
                    if (node.p.indexOf(item._) == 0) {
                        node.p = node.p.replace(item._, newId);
                    }
                }

                if (node._ == item._) {
                    newDef[subIndex] = {
                        ...item,
                        _: newId
                    }
                }
            })
        })


    }



    if (killWidget) state.widget = null;
    state.definition = newDef;

    return state;
};


/**
 * Add a child node to an item
 * @param state
 * @param parentId
 * @param node
 * @returns {*}
 * @private
 */
const _addChild = (state, parentId, node) => {
    let id = 0;

    node._ = `${parentId}.${id}`;
    node.p = parentId;
    node.h = 100;
    node.w = 100;

    state.definition.push(node);

    return state;

};

const _addSibling = (state, parentId, pos, sibId, node) => {
    return state;
}

const _replaceNode = (state, replaceId, data) => {

    let item, itemIndex;
    state.definition.forEach((node, index) => {
        if (node._ == replaceId) {
            item = node;
            itemIndex = index;
        }
    });

    let newItem = {
        ...data,
        _: item._,
        p: item.p,
        h: item.h,
        w: item.w
    };

    state.definition[itemIndex] = newItem;

    return state;

};

const _appendNode = (state, appendId, data) => {
    let parentId = appendId.split(".");
    parentId = parentId.slice(0, parentId.length - 1);
    parentId = parentId.join(".");

    let parent = state.definition.find((item) => {
        return item._ == parentId;
    });

    let siblings = state.definition.filter((item) => {
        return item.p == parentId;
    });

    let newHeight,
        newWidth;

    if (parent.t == "H") {
        newWidth = 10;
        newHeight = 100;
        let diff = newWidth / siblings.length;
        siblings.forEach((item) => {
            item.w -= diff;
        })
    } else {
        newWidth = 100;
        newHeight = 10;
        let diff = newHeight / siblings.length;
        siblings.forEach((item) => {
            item.h -= diff;
        })
    }

    let item = {};
    item.w = newWidth;
    item.h = newHeight;
    item.p = parentId;
    if (data.t == "H") {
        item.t = "H";
        item.i = [];
    } else if (data.t == "V") {
        item.t = "V";
        item.i = [];
    } else {
        item.t = "W";
        item.c = {
            type: data.t
        }
    }


    siblings = siblings.sort((a, b) => {
        if (a._ > b._) return 1;
        if (a._ < b._) return -1;
        return 0;
    });

    let sibIndex;
    siblings.forEach((node, index) => {
        if (node._ == appendId) sibIndex = index;
    })

    if (sibIndex == siblings.length - 1) {
        console.log("HERE")
        siblings.push(item);
    } else {

        let newSibs = [];
        siblings.forEach((node) => {
            newSibs.push(node);
            if (node._ == appendId) {
                newSibs.push(item);
            }
        })
        siblings = newSibs;
    }

    let sib_ids = [];
    siblings.forEach((node) => {
        sib_ids.push(node._);
    })

    let newDef = [];
    state.definition.forEach((node) => {
        if (sib_ids.indexOf(node._) < 0) {
            newDef.push(node);
        }
    });

    siblings.forEach((node, index) => {
        let newId = `${parentId}.${index}`;

        newDef.forEach((subNode) => {
            if (subNode._.indexOf(node._) == 0 && subNode._ != node._) {
                subNode._ = subNode._.replace(node._, newId);
            }

            if (subNode.p) {
                if (subNode.p.indexOf(node._) == 0) {
                    subNode.p = subNode.p.replace(node._, newId);
                }
            }
        })

        newDef.push({
            ...node,
            _: newId
        })

    });

    state.definition = newDef;

    return state;
};

const _prependNode = (state, sibId, data) => {
    let parentId = sibId.split(".");
    parentId = parentId.slice(0, parentId.length - 1);
    parentId = parentId.join(".");

    let parent = state.definition.find((item) => {
        return item._ == parentId;
    });

    let siblings = state.definition.filter((item) => {
        return item.p == parentId;
    });

    let newHeight,
        newWidth;

    if (parent.t == "H") {
        newWidth = 10;
        newHeight = 100;
        let diff = newWidth / siblings.length;
        siblings.forEach((item) => {
            item.w -= diff;
        })
    } else {
        newWidth = 100;
        newHeight = 10;
        let diff = newHeight / siblings.length;
        siblings.forEach((item) => {
            item.h -= diff;
        })
    }

    let item = {};
    item.w = newWidth;
    item.h = newHeight;
    item.p = parentId;
    if (data.t == "H") {
        item.t = "H";
        item.i = [];
    } else if (data.t == "V") {
        item.t = "V";
        item.i = [];
    } else {
        item.t = "W";
        item.c = {
            type: data.t
        }
    }


    siblings = siblings.sort((a, b) => {
        if (a._ > b._) return 1;
        if (a._ < b._) return -1;
        return 0;
    });

    if (sibId[sibId.length - 1] == "0") {
        siblings.unshift(item);
    } else {
        // find the index of the sibling
        let sibIndex;
        siblings.forEach((node, index) => {
            if (node._ == sibId) sibIndex = index;
        })
        siblings = [...siblings.slice(0, sibIndex - 1), item, ...siblings.slice(sibIndex, siblings.length - 1)];
    }

    let sib_ids = [];
    siblings.forEach((node) => {
        sib_ids.push(node._);
    })

    let newDef = [];
    state.definition.forEach((node) => {
        if (sib_ids.indexOf(node._) < 0) {
            newDef.push(node);
        }
    });

    siblings.forEach((node, index) => {
        let newId = `${parentId}.${index}`;

        newDef.forEach((subNode) => {
            if (subNode._.indexOf(node._) == 0 && subNode._ != node._) {
                subNode._ = subNode._.replace(node._, newId);
            }

            if (subNode.p) {
                if (subNode.p.indexOf(node._) == 0) {
                    subNode.p = subNode.p.replace(node._, newId);
                }
            }
        })

        newDef.push({
            ...node,
            _: newId
        })

    });

    state.definition = newDef;

    return state;
};

const _updateWidget = (state, wid, data) => {
    state.definition.forEach((node, index) => {
        if (node._ == wid) {
            node.c = data;
        }
    });

    state.widget = null;

    return state;
}

const _setData = (state, data) => {
    state.definition = data.definition;
    state.data = data;
    return state;
}

const reducer = (state, action, data) => {
    let newState = ewars.copy(state);

    switch (action) {
        case "SET_DATA":
            return _setData(newState, data);
        case "SET_WIDTH":
            newState.width = data;
            return newState;
        case "SET_CONSTRAINTS":
            return _setConstraints(newState, data);
        case "RESIZE_ROW":
            return _resizeRow(newState, data[0], data[1]);
        case "RESIZE_ROWS":
            return _resizeRows(newState, data[0], data[1]);
        case "CHANGE_VIEW":
            newState.view = data;
            return newState;
        case "EDIT_WIDGET":
            newState.widget = data;
            if (data != null) newState.view = "DEFAULT";
            return newState;
        case "UPDATE_WIDGET_CLOSE":
            return _updateWidget(newState, data[0], data[1]);
        case "RESIZE":
            return _resize(newState, data[0], data[1]);
        case "SWAP_WIDGET":
            return _swapWidget(newState, data[0], data[1]);
        case "ADD_ROOT":
            return _addRoot(newState, data);
        case "ADD_CHILD":
            return _addChild(newState, data[0], data[1]);
        case "ADD_PAGE":
            return _addPage(newState);
        case "PAGE_ADD_ITEM":
            return _addPageItem(newState, data[0], data[1]);
        case "SET_LAYOUT_PROP":
            return _setLayoutProp(newState, data[0], data[1]);
        case "WIDGET_PROP_CHANGE":
        case "WIDGET_UPDATE_PROP":
            return _widgetPropChange(newState, data[0], data[1], data[2]);
        case "ADD_SERIES":
            return _widgetAddSeries(newState, data[0], data[1]);
        case "CLEAR_SERIES":
            return _clearSeries(newState, data);
        case "UPDATE_SETTING":
            return _updateSetting(newState, data[0], data[1]);
        case "SET_SERIES":
            return _setSeries(newState, data[0], data[1]);
        case "RESIZER":
            return _resizer(newState, data[0], data[1]);
        case "RESIZER_V":
            return _resizerV(newState, data[0], data[1]);
        case "REMOVE_NODE":
            return _removeNode(newState, data);
        case "ADD_NODE":
            return _addNode(newState, data[0], data[1]);
        case "APPEND_NODE":
            return _appendNode(newState, data[0], data[1]);
        case "PREPEND_NODE":
            return _prependNode(newState, data[0], data[1]);
        case "REPLACE_NODE":
            return _replaceNode(newState, data[0], data[1]);
        default:
            return newState;
    }
};

export default reducer;