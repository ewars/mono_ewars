const NAMES = {
    V: "VBox",
    H: "HBox",
    W: "Widget",
    P: "Page"
};

const ACTIONS = [
    ["fa-trash", "DELETE"]
];

class TreeNode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false
        }

    }

    _toggle = () => {
        this.setState({
            shown: !this.state.shown
        })
    };

    _action = (action) => {
        if (action == "DELETE") {
            ewars.z.dispatch("E", "REMOVE_NODE", this.props.data._);
        }
    };

    render() {
        let iconClass = "fal fa-caret-right";
        if (this.state.shown) iconClass = "fal fa-caret-down";

        let children = this.props.definition.filter((item) => {
            return item.p == this.props.data._;
        });

        children = children.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        });

        console.log(this.props.data);

        return (
            <div className="block" style={{padding: 0}}>
                <div className="block-content">
                    <ewars.d.Row>
                        {this.props.data.t != "W" ?
                            <ewars.d.Cell style={{padding: '5px'}} width="20px" onClick={this._toggle}>
                                <i className={iconClass}></i>
                            </ewars.d.Cell>
                            : null}
                        <ewars.d.Cell style={{padding: '5px'}}>
                            {NAMES[this.props.data.t]}
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{display: 'block'}}>
                            <ewars.d.ActionGroup
                                actions={ACTIONS}
                                right={true}
                                height="20px"
                                onAction={this._action}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.shown ?
                    <div className="block-children">
                        {children.map((item) => {
                            return <TreeNode
                                definition={this.props.definition}
                                data={item}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }

}

class Tree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false
        }
    }

    render() {
        let roots = this.props.definition.filter((item) => {
            return !item.p;
        })

        roots = roots.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        });

        return (
            <div className="block-tree">
                {roots.map((item) => {
                    return <TreeNode
                        definition={this.props.definition}
                        data={item}/>;
                })}
            </div>
        )


    }
}

export default Tree;