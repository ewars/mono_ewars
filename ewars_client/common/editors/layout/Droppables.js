const TYPES = [
    {n: "SERIES", l: "Time Series"},
    {n: "CATEGORY", l: "Category"},
    {n: "RAW", l: "Raw"},
    {n: "MAP", l: "Map"}
]

import TYPE_DEFINITIONS from '../../constants/type_definitions';

const STYLE = {
    ICON_CIRCLE: {
        borderRadius: "60px",
        background: "#333",
        textAlign: "center",
        position: "absolute",
        top: "50%",
        left: "50%",
        width: "30px",
        height: "30px",
        lineHeight: "30px",
        marginLeft: "-15px",
        marginTop: "-15px",
        color: "#F2F2F2"

    }
}

class Droppable extends React.Component {
    static defaultProps = {
        vAllowed: false,
        hAllowed: false,
        wAllowed: false,
        centroid: false
    };

    constructor(props) {
        super(props);
    }

    _onDragStart = (e) => {
        let target = "dp-area";

        e.dataTransfer.setData("e", JSON.stringify(this.props.data));

        if (this.props.data.t == "H") {
            ewars.emit("SHOW_DROPS", "H");
        } else if (this.props.data.t == "V") {
            ewars.emit("SHOW_DROPS", "V");
        } else {
            ewars.emit("SHOW_DROPS", "W");
        }

    };

    _onDragEnd = (e) => {
        e.preventDefault();

        ewars.emit("HIDE_DROPS", null);
    };

    render() {
        let icon;
        if (this.props.data.i) icon = <i className={"fal " + this.props.data.i}></i>;

        let style = {
            display: "flex",
            flexDirection: "row"
        };
        if (this.props.data.bgd) style.backround = this.props.data.bgd;

        return (
            <div className="son-list-item"
                 style={style}
                 draggable={true}
                 onDragStart={this._onDragStart}
                 onDragEnd={this._onDragEnd}>
                <ewars.d.Cell width="35px" style={{textAlign: "center", marginRight: "5px"}}>
                        {icon}
                </ewars.d.Cell>
                <ewars.d.Cell>
                    {__(this.props.data.t)}
                </ewars.d.Cell>
            </div>
        )
    }
}

class Droppables extends React.Component {
    static defaultProps = {
        mode: "DASHBOARD"
    };

    constructor(props) {
        super(props)
    }

    render() {
        let items = [];
        for (let i in TYPE_DEFINITIONS) {
            let t = TYPE_DEFINITIONS[i];
            if (t.modes) {
                if (t.modes == "*" || t.modes.indexOf(this.props.mode) >= 0) {
                    items.push(t);
                }
            }
        }

        return (
            <ewars.d.Cell style={{overflowY: "auto", paddingTop: "8px"}}>
                {items.map((item) => {
                    return <Droppable data={item}/>
                })}
            </ewars.d.Cell>
        )
    }
}

export default Droppables;