const STYLES = {
    CONTROLS: {
        position: "absolute",
        top: 0,
        left: 0,
        height: "100%",
        width: "100%"
    },
    joint: {
        position: "absolute",
        background: "#CCCCCC",
        width: 10,
        height: 10
    },
    tlJoint: {
        top: 0,
        left: 0
    },
    trJoin: {
        top: 0,
        right: 0
    },
    blJoint: {
        bottom: 0,
        left: 0
    },
    brJoint: {
        bottom: 0,
        right: 0
    },
    rightControl: {
        position: "absolute",
        right: '0px',
        top: 0,
        height: "100%",
        background: "#F2F2F2",
        width: "4px",
        cursor: "col-resize"
    },
    bottomControl: {
        position: "absolute",
        bottom: "0px",
        left: 0,
        height: "4px",
        cursor: "row-resize",
        width: "100%",
        background: "#F2F2F2"
    },
    WIDGET_INNER: {
        position: "absolute",
        top: "5px",
        left: "5px",
        right: "5px",
        bottom: "5px",
        lineHeight: "20px",
        textAlign: "center"
    },
    DEFAULT_DROP: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        background: "rgba(0,0,0,0.1)",
        display: "none"
    },
    DROP_AREA: {
        position: "absolute",
        background: "#333333",
        borderRadius: "3px",
        width: "20px",
        height: "20px",
        top: "20px",
        left: "-10px",
        zIndex: 6
    },
    DROP_AREA_R: {
        position: "absolute",
        borderTopLeftRadius: "3px",
        background: "#333333",
        borderBottomLeftRadius: "3px",
        width: "20px",
        height: "20px",
        top: "20px",
        right: "0",
        zIndex: 6
    },
    DROP_AREA_L: {
        position: "absolute",
        background: "#333333",
        borderTopRightRadius: "3px",
        borderBottomRightRadius: "3px",
        width: "20px",
        height: "20px",
        top: "20px",
        left: "0",
        zIndex: 6
    },
    CENTER_DROP: {
        position: "absolute",
        top: 10,
        left: 10,
        right: 10,
        bottom: 10,
        background: "rgba(0,0,0,0.1)",
        textAlign: "center",
        zIndex: 999
    },
    CONTEXT_MENU: {
        position: "absolute",
        top: "5px",
        left: "50%",
        marginLeft: "-50px",
        width: "100px",
        height: "20px",
        display: "none"
    },
    DROPPER: {
        position: "absolute",
        bottom: "0px",
        left: "50%",
        marginLeft: "-10px",
        background: "#F2F2F2",
        borderTopRightRadius: "25px",
        borderTopLeftRadius: "25px",
        width: "20px",
        height: "20px",
        zIndex: 999,
        textAlign: "center",
        color: "#333333",
        lineHeight: "20px",
        display: "none",
        transformOrigin: "50% 100%",
        transition: "width 2s, height 2s"
    },
    DROPPER_T: {
        position: "absolute",
        top: "0px",
        left: "50%",
        marginLeft: "-10px",
        background: "#F2F2F2",
        borderBottomRightRadius: "25px",
        borderBottomLeftRadius: "25px",
        width: "20px",
        height: "20px",
        zIndex: 999,
        textAlign: "center",
        color: "#333333",
        lineHeight: "20px",
        display: "none",
        transformOrigin: "50% 0%"
    },
    DROPPER_R: {
        position: "absolute",
        top: "50%",
        marginTop: "-10px",
        right: "0",
        background: "#F2F2F2",
        borderTopLeftRadius: "25px",
        borderBottomLeftRadius: "25px",
        width: "20px",
        height: "20px",
        zIndex: 999,
        textAlign: "center",
        color: "#333333",
        lineHeight: "20px",
        display: "none",
        transformOrigin: "100% 50%"
    },
    DROPPER_L: {
        position: "absolute",
        top: "50%",
        marginTop: "-10px",
        left: 0,
        background: "#F2F2F2",
        borderTopRightRadius: "25px",
        borderBottomRightRadius: "25px",
        width: "20px",
        height: "20px",
        zIndex: 999,
        textAlign: "center",
        color: "#333333",
        lineHeight: "20px",
        display: "none",
        transformOrigin: "0% 50%"
    }
};

export default STYLES;