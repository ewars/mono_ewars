import DataSelector from "./DataSelector";

const ITEM_ACTIONS = [
    ["fa-pencil", "EDIT"],
    ["fa-caret-up", "UP"],
    ["fa-caret-down", "DOWN"],
    ["fa-copy", "DUPE"],
    ["fa-trash", "RM"]
];

const CTRL_STYLE = {
    background: "rgba(0,0,0,0.4)",
    position: "absolute",
    bottom: "-20px",
    borderBottomLeftRadius: "3px",
    borderBottomRightRadius: "3px",
    right: 0
}

class DataSource extends React.Component {
    _onAction = (action) => {
        this.props.action(action, this.props.data, this.props.index);
    };

    render() {
        let style = {}, showLeft;
        if (this.props.data.colour) {
            style.background = this.props.data.colour;
            showLeft = true;
        }

        return (
            <div className="block" style={{background: "transparent"}}>
                <div className="block-content" style={{padding: 0}}>
                    <ewars.d.Row style={{background: "rgba(0,0,0,0.5)"}}>
                        {showLeft ?
                            <ewars.d.Cell width="5px" style={style} borderRight={true}>
                            </ewars.d.Cell>
                            : null}
                        <ewars.d.Cell>
                            <ewars.d.Row height="20px" style={{background: "rgba(80,80,80,1)"}}>
                                <ewars.d.Cell></ewars.d.Cell>
                                <ewars.d.Cell style={{display: "block"}}>
                                    <ewars.d.ActionGroup
                                        right={true}
                                        onAction={this._onAction}
                                        vertical={true}
                                        actions={ITEM_ACTIONS}/>
                                </ewars.d.Cell>
                            </ewars.d.Row>
                            <ewars.d.Row>
                                <ewars.d.Cell style={{padding: "8px"}}>
                                    {this.props.data.title}
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
            </div>
        )
    }
}
;

const PANEL_ACTIONS = [
    ["fa-plus", "ADD"]
];

/**
 *
 * Section-based data selection
 *
 *
 * ext:
 *  d: dimensions - whether to allow selection of dimensions
 *  sp: Source property - The source property to edit on the node when this data changes
 *  single: default false
 *  mode: The mode to operate in
 *  sections:
 *      name: The name of the source property to edit
 *      limit: Limit the number of data sources allowed to be selected
 *      mode: The mode to operate in
 *      min: The min required data sources
 *      d: Allow dimension selection
 *
 */

export default class DataSources extends React.Component {
    static defaultProps = {
        mode: "DEFAULT",
        sourceProp: null,
        ext: {
            mode: "DEFAULT",
            d: false,
            sp: "",
            limit: false,
            single: false,
            sections: null
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            source: null
        }
    }


    _onAction = (action, data) => {
        switch (action) {
            case "CLOSE":
                this.setState({
                    shown: false,
                    source: null
                })
                break;
            case "SAVE":
                if (!data.uuid) data.uuid = ewars.utils.uuid();
                let sourcesSave = ewars.copy(this.props.data[this.props.ext.sp] || []);

                let changeIndex;
                sourcesSave.forEach((item, index) => {
                    if (item.uuid == data.uuid) {
                        changeIndex = index;
                    }
                })
                if (changeIndex != null) {
                    sourcesSave[changeIndex] = data;
                } else {
                    sourcesSave.push(data);
                }

                ewars.z.dispatch("E", "WIDGET_UPDATE_PROP", [this.props.id, this.props.ext.sp, sourcesSave]);
                this.setState({
                    shown: false,
                    source: null
                })
                break;
            case "UP":
                break;
            case "DOWN":
                break;
            case "EDIT":
                if (!data.uuid) data.uuid = ewars.utils.uuid();
                this.setState({
                    shown: true,
                    source: data
                })
                break;
            case "DELETE":
                let sources = ewars.copy(this.props.data.series);
                sources.splice(data, 1);
                ewars.z.dispatch("E", "WIDGET_UPDATE_PROP", [this.props.id, this.props.ext.sp, sources])
                break;
            case "ADD":
                this.setState({
                    shown: true,
                    source: {}
                })
                break;
            case "CLEAR":
                ewars.z.dispatch("E", "CLEAR_SERIES", this.props.id);
                break;
            default:
                break;
        }
    };

    render() {

        let view;

        if (this.props.ext.mode == "DEFAULT") {

            return (
                <ewars.d.Layout>
                    {this.props.mode == "DEFAULT" ?
                        <ewars.d.Toolbar>
                            <ewars.d.ActionGroup
                                actions={PANEL_ACTIONS}
                                onAction={this._onAction}
                                right={true}/>

                        </ewars.d.Toolbar>
                        : null}
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <ewars.d.Panel>
                                <div className="block-tree" style={{position: "relative"}}>
                                    {(this.props.data[this.props.ext.sp] || []).map((item, index) => {
                                        return (
                                            <DataSource
                                                index={index}
                                                action={this._onAction}
                                                data={item}/>
                                        )
                                    })}

                                </div>
                            </ewars.d.Panel>
                        </ewars.d.Cell>
                    </ewars.d.Row>

                    <ewars.d.Shade
                        toolbar={false}
                        onAction={this._onAction}
                        shown={this.state.shown}>
                        <DataSelector
                            dimensions={this.props.ext.d || false}
                            onSave={(data) => {
                                this._onAction("SAVE", data)
                            }}
                            onClose={() => {
                                this._onAction("CLOSE");
                            }}
                            data={this.state.source}/>
                    </ewars.d.Shade>

                </ewars.d.Layout>
            )
        }

        if (this.props.ext.mode == "SINGLE") {
            return (
                <ewars.d.Panel>
                    <DataSelector
                        hideSources={true}
                        data={this.state.source || {}}/>
                </ewars.d.Panel>
            )
        }
    }
}