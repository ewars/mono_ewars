import STYLES from "./styles";

class DropArea extends React.Component {
    static defaultProps = {
        data: {}
    };

    constructor(props) {
        super(props);
    }

    _onDragOver = (e) => {
        e.preventDefault();
        this._el.style.transform = "scale(2)";
    };

    _onDragLeave = (e) => {
        e.preventDefault();
        this._el.style.transform = "none";
    };

    _onDrop = (e) => {
        e.preventDefault();
        let data = JSON.parse(e.dataTransfer.getData("e"));
        if (data.t != "H") {
            // we're adding a new vertical column
            let items = ewars.copy(this.props.data.i || []);

            if (items.length > 0) {
                items.sort((a, b) => {
                    if (a._ > b._) return 1;
                    if (a._ < b._) return -1;
                    return 0;
                })
            }

            let lastId,
                newWidth = 100;

            if (items.length > 0) {
                lastId = items[items.length - 1]._;
                lastId = lastId.split(".");
                lastId[lastId.length - 1] = parseInt(lastId[lastId.length - 1]) + 1;
                lastId = lastId.join(".");
            } else {
                lastId = `${this.props.data._}.1`;
            }

            if (items.length > 0) {
                let subtract;
                if (items.length === 1) subtract = 10;
                if (items.length > 1) subtract = 10 / items.length;
                newWidth = 10;

                items.forEach((item) => {
                    item.w = item.w - subtract;
                })
            }

            let newItem = {
                _: lastId,
                t: data.t,
                w: newWidth,
            };

            if (data.t == "V") {
                newItem.i = [];
            } else {
                newItem.c = data.c;
            }

            items.push(newItem);

            if (this.props.pid) {
                ewars.z.dispatch("E", "RESIZE_P", [this.props.pid, "w", items]);
            } else {
                ewars.z.dispatch("E", "RESIZE", ["w", items]);
            }
        }
    };

    render() {
        let style = STYLES.DEFAULT_DROP;
        return (
            <div
                ref={(el) => {
                    this._el = el
                }}
                style={style}
                onDrop={this._onDrop}
                onDragLeave={this._onDragLeave}
                onDragOver={this._onDragOver}>
            </div>
        )
    }
}

export default DropArea;