import WidgetDrop from "./WidgetDrop";
import CenterDrop from "./CenterDrop";
import ContextMenu from "./ContextMenu";

import * as analysis from '../../visualisation';
import * as WIDGETS from '../../wdgts/';
import * as viz from '../../visualisation';

import STYLES from "./styles";

const ACTIONS = [
    ["fa-pencil", "EDIT"],
    ["fa-trash", "DELETE"]
];

class NullWidget extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let style = {
            width: "100%",
            height: "100%",
            background: "#000000"
        }

        return (
            <div style={style}>

            </div>
        )
    }
}

const _f = (val, fb) => {
    if (val == undefined) return fb;
    if (val == null) return fb;
    if (val == '') return fb;
    return val;
}

const VIZ_TYPES = ['DEFAULT', 'PIE', 'COLUMN', 'FUNNEL', 'PYRAMID', 'HEATMAP', 'DATAPOINT', 'MAP', 'TABLE', 'HTML'];
class Widget extends React.Component {

    static defaultProps = {
        vertical: false,
        height: "100%",
        width: "100%",
        resizable: true,
        showContext: false
    };

    constructor(props) {
        super(props);

        this.state = {
            context: false
        }
    }

    componentWillReceiveProps() {
        if (this._viz) {
            if (this._viz.redraw) this._viz.redraw();
        }
    }

    componentDidMount() {
        let renderClass;
        switch (this.props.data.type) {
            case 'DEFAULT':
                renderClass = viz.DefaultChart;
                break;
            case 'PIE':
                renderClass = viz.PieChart;
                break;
            case 'COLUMN':
                renderClass = viz.ColumnChart;
                break;
            case 'FUNNEL':
                renderClass = viz.FunnelChart;
                break;
            case 'PYRAMID':
                renderClass = viz.PyramidChart;
                break;
            case 'HEATMAP':
                renderClass = viz.HeatMap;
                break;
            case 'DATAPOINT':
                renderClass = viz.Default;
                break;
            case 'MAP':
                renderClass = viz.DefaultMap;
                break;
            case 'TABLE':
                renderClass = viz.TableDefault;
                break;
            case 'HTML':
                renderClass = viz.OtherHTML;
                break;
            default:
                break;
        }

        if (renderClass) {
            this._viz = new renderClass(
                this.props.data,
                null,
                this._elChart
            )
        }
    }



    _onAction = (action) => {
        if (action == "EDIT") {
            this.props.onEdit(this.props.data);
        }

        if (action == "DELETE") {
            this.props.onDelete(this.props.data);
        }

        if (action == "CLOSE") {
            ewars.emit("HIDE_CONTROLS");
        }
    };

    render() {
        let style = {
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column"
        };

        return (
            <div
                className="layout-widget widget-cell"
                ref={(el) => {
                    this._el = el
                }}
                onContextMenu={this._onContext}
                style={style}>
                <div className="tbar">
                    <div className="action-group-wrapper">
                    <ewars.d.ActionGroup
                        actions={ACTIONS}
                        right={true}
                        height="30px"
                        style={{float: 'right'}}
                        onAction={this._onAction}/>
                    </div>
                </div>
                <div
                    className="widget-content"
                    ref={(el) => { this._elChart = el;}}
                    onDoubleClick={this._onEdit}>

                </div>
            </div>
        )
    }
}

export default Widget;