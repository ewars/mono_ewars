const _updateDim = (state, uuid, data) => {

    ["x", "y"].forEach(axis => {
        state[axis].forEach(item => {
            if (item[0] == uuid) {
                item[3] = data;
            }
        })
    })

    return state;
};

const _removeItem = (state, uuid) => {
    let target, targetAxis;

    ["x", "y"].forEach(axis => {
        state[axis].forEach((item, index) => {
            if (item[0] == uuid) {
                target = index;
                targetAxis = axis;
            }
        })
    })

    if (target != null) state[targetAxis].splice(target, 1);

    if (state.filters[uuid]) {
        delete state.filters[uuid];
    }

    return state;
}

const _addItem = (state, axis, position, anchorId, item) => {

    item.unshift(ewars.utils.uuid());

    if (item[2] == "LOCATION") {
        if (item[3].sti) {
            state.filters[item[0]] = [
                [ewars.utils.uuid(), "D", "LOCATION_TYPE", ["EQ:" + item[3].sti]]
            ]
        }
    }

    if (item[3].type == "DATE") {
        state.filters[item[0]] = [
            [ewars.utils.uuid(), "D", item[2], {
                period: ["{NOW}-90D", "{NOW}"]
            }]
        ]

        item[3] = {
            interval: "DAY",
            type: "DATE"
        }
    }

    let items = state[axis];

    if (anchorId != null) {
        let anchorIndex;
        state[axis].forEach((node, index) => {
            if (node[0] == anchorId) {
                anchorIndex = index;
            }
        });


        if (position == "before") {
            state[axis].splice(anchorIndex, 0, item);
        } else if (position == "after") {
            state[axis].splice(anchorIndex + 1, 0, item)
        } else {
            state[axis].push(item);
        }
    } else {
        state[axis].push(item);
    }

    return state;
}

const _updateDimProp = (state, uuid, prop, value) => {
    ["x", "y"].forEach(axis => {
        state[axis].forEach((item) => {
            if (item[0] == uuid) {
                item[3][prop] = value;
            }
        })
    })

    return state;
}

const _addFilter = (state, item_id, filter) => {
    filter.unshift(ewars.utils.uuid());
    if (state.filters[item_id]) {
        state.filters[item_id].push(filter)
    } else {
        state.filters[item_id] = [filter]
    }

    return state;
}

const _updateFilter = (state, parentId, newData) => {
    state.filters[parentId].forEach(item => {
        if (item[0] == newData[0]) {
            item = newData;
        }
    })

    return state;
}

const _removeFilter = (state, parentId, filterId) => {
    let target;
    state.filters[parentId].forEach((item, index) => {
        if (item[0] == filterId) target = index;
    })

    if (target != null) {
        state.filters[parentId].splice(target, 1);
    }

    if (state.filters[parentId].length <= 0) delete state.filters[parentId];

    return state;
}

const reducer = (state, action, data) => {
    let newState = ewars.copy(state);

    this.setView = (view) => {
        newState.view = view;

        return newState;
    };

    this.setMainView = (view) => {
        newState.mainView = view;

        return newState;
    };

    switch (action) {
        case "SET_VIEW":
            newState.view = data;
            return newState;
        case "SET_MAIN_VIEW":
            newState.mainView = data;
            return newState;
        case "UPDATE_DIM":
            return _updateDim(newState, data[0], data[1]);
        case "UPDATE_DIM_PROP":
            return _updateDimProp(newState, data[0], data[1], data[2]);
        case "REMOVE_ITEM":
            return _removeItem(newState, data);
        case "ADD_ITEM":
            return _addItem(newState, data[0], data[1], data[2], data[3]);
        case "ADD_FILTER":
            data[2].unshift(ewars.utils.uuid());
            // Get rid of any remaining data items in there.
            data[2][3] = [];

            //TODO: Block adding the same dimension/measure a filter to the item

            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    if (group.f[data[1]]) {
                        group.f[data[1]].push(data[2]);
                    } else {
                        group.f[data[1]] = [data[2]]
                    }
                }
            });
            return newState;
        case "UPDATE_FILTER":
            // Full-on replace, we don't muck about searching,
            // just replace the full filter list completely for the
            // given node
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    group.f[data[1]].forEach((filt, index) => {
                        if (filt[0] == data[2][0]) group.f[data[1]][index] = data[2];
                    })
                }
            });
            return newState;
        case "REMOVE_FILTER":
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    let rIndex;
                    group.f[data[1]].forEach((item, index) => {
                        if (item[0] == data[2]) rIndex = rIndex;
                    })
                    group.f[data[1]].splice(rIndex, 1);
                }
            });
            return newState;
        case "SET_TYPE":
            newState.render_type = data;
            return newState;
        case "UPDATE_GROUPS":
            newState.groups = data;
            return newState;
        case "UPDATE_GROUP":
            newState.groups.forEach(group => {
                if (group._ == data._) {
                    group.d = data.d;
                    group.f = data.f;
                    group.c = data.c;
                }
            })
            return newState;
        case 'REMOVE_NODE_FROM_GROUP':
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    // got the group now find the node
                    let removalIndex;
                    group.d.forEach((item, index) => {
                        if (item[0] == data[1]) removalIndex = index;
                    })

                    group.d.splice(removalIndex, 1);
                }
            })
            return newState;
        case 'REMOVE_GROUP':
            let rIndex;
            newState.groups.forEach((group, index) => {
                if (group._ == data) rIndex = index;
            })
            newState.groups.splice(rIndex, 1);
            return newState;
        case 'DUPE_GROUP':
            let tGroup;
            newState.groups.forEach(group => {
                if (group._ == data) tGroup = ewars.copy(group);
            });
            tGroup._ = ewars.utils.uuid();
            newState.groups.push(tGroup);
            return newState;
        case 'ADD_TO_GROUP':
            // Measures and dimension contain two sets of ordering, they
            // are ordered independently of each other, so when an item is appended
            // we partition into two sets, one that contains D, and one that contains C, M
            // We then order by the .o property of the items and insert the new item accordingly
            data[1].unshift(ewars.utils.uuid());

            let group,
                measures,
                dimensions;

            let sortFunc = (a, b) => {
                if (a.o > b.o) return 1;
                if (a.o < b.o) return -1;
                return 0;
            };

            // Find the group that we're adding to
            group = newState.groups.filter(group => {
                return group._ == data[0];
            })[0];

            // Filter in groups and sort by current order
            measures = group.d.filter(item => {
                return ['M', 'C'].indexOf(item[1]) >= 0;
            }).sort(sortFunc);

            dimensions = group.d.filter(item => {
                return item[1] == 'D';
            }).sort(sortFunc);

            // Figure out where to append the new item
            if (data[1][1] == 'M') {
                measures.push(data[1]);
            } else if (data[1][1] == 'C') {
                measures.push(data[1]);
            } else {
                dimensions.push(data[1]);
            }

            let mCounter = 0;
            measures.forEach(item => {
                item.o = mCounter;
                mCounter++;
            })

            // reassign order numbers to items
            let dCounter = 0;
            dimensions.forEach(item => {
                item.o = dCounter;
                dCounter++;
            })

            // Merge the dimensions and measures back together again
            let results = measures.concat(dimensions);

            // Update the group in the state
            newState.groups.forEach(item => {
                if (item._ == data[0]) {
                    item.d = results;

                    if (data[1][2].indexOf('FIELD.') == 0) {
                        // this is a field in a form, add a filter
                        let formId = data[1][2].split('.')[1];
                        item.f[data[1][0]] = [[ewars.utils.uuid(), 'D', 'FORM', [`EQ:${formId}`]]];
                    }
                }
            });


            return newState;
        case 'CMP_SET_VAL':
            let gid = data[0],
                nid = data[1],
                prop = data[2][0],
                value = data[2][1];

            newState.groups.forEach(group => {
                if (group._ == gid) {
                    group.d.forEach(node => {
                        if (node[0] == nid) {
                            if (!node[3]) node[3] = {};
                            node[3][prop] = value;
                        }
                    })
                }
            })

            return newState;
        case 'UPDATE_AXIS':
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    if (!group.ax) group.ax = {};
                    group.ax[data[1]] = data[2];
                }
            });

            return newState;
        case 'ADD_CALC_TO_GROUP':
            let newCalc = [
                ewars.utils.uuid(),
                'C',
                "",
                {
                    d: []
                }
            ];

            newState.groups.forEach(group => {
                if (group._ == data) {
                    group.d.push(newCalc)
                }
            });
            return newState;
        case 'ADD_M_TO_CALC':
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    group.d.forEach(node => {
                        if (node[0] == data[1]) {
                            data[2].unshift(ewars.utils.uuid());
                            node[3].d.push(data[2]);
                        }
                    })

                    if (data[2][2].indexOf('FIELD.') >= 0) {
                        // This is a field, immediately add a form filter to the item
                        let formId = data[2][2].split('.')[1];
                        group.f[data[2][0]] = [
                            [ewars.utils.uuid(), 'D', 'FORM', [`EQ:${formId}`]]
                        ]
                    }

                }
            });

            return newState;
        case 'ADD_CONTROL':
            newState.groups.push({
                type: data,
                _: ewars.utils.uuid(),
                c: {}
            });
            return newState;
        case 'REMOVE_CONTROL':
            let rcIndex;
            newState.groups.forEach((item, index) => {
                if (item._ == data) rcIndex = index;
            })
            newState.groups.splice(rcIndex, 1);
            return newState;
        case 'UPDATE_CONTROL_PROP':
            return newState;
        case 'REMOVE_NODE_FROM_CALC':
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    group.d.forEach(node => {
                        if (node[0] == data[1]) {
                            let removalIndex;
                            node[3].d.forEach((subNode, index) => {
                                if (subNode[0] == data[2]) removalIndex = index;
                            })
                            node[3].d.splice(removalIndex, 1);
                        }
                    })
                }
            })
            return newState;
        case 'CALC_NODE_SET_VAL':
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    group.d.forEach(node => {
                        if (node[0] == data[1]) {
                            node[3].d.forEach(subNode => {
                                if (subNode[0] == data[2]) {
                                    subNode[3][data[3][0]] = data[3][1];
                                }
                            })
                        }
                    })
                }
            });
            return newState;
        case 'UPDATE_CALC_FORMULA':
            newState.groups.forEach(group => {
                if (group._ == data[0]) {
                    group.d.forEach(node => {
                        if (node[0] == data[1]) node[2] = data[2];
                    })
                }
            })
            return newState;
        case 'UPDATE_AXIS_PROP':
            newState.axis.forEach(item => {
                if (item._ == data._) {
                    item[data.prop] = data.value;
                }
            })
            return newState;
        case 'UPDATE_CONFIG':
            newState.config = data;
            return newState;
        case 'UPDATE_NODE_PROP':
            newState.groups.forEach(group => {
                if (group._ == data.gid) {
                    group.d.forEach(item => {
                        if (item[0] == data.nid) {
                            item[3][data.prop] = data.value;
                        }
                    })
                }
            });
            return newState;
        default:
            return newState;
    }
}

export default reducer;
