import {instance} from '../plot_instance';

class Wing extends React.Component {
    static defaultProps = {
        left: false,
        T: null,
        isCalcVariable: false,
        calc: false
    };

    constructor(props) {
        super(props);

        ewars.subscribe('SHOW_DROPS', this._showDrop);
        ewars.subscribe('HIDE_DROPS', this._hideDrops);
    }

    _showDrop = (T) => {
        // We don't allow dropping anything other than measures onto calcs
        if (this.props.isCalcVariable) {
            if (T != 'M') return;
        }

        if (this.props.T) {
            if (this.props.T == T) {
                if (this._el) this._el.style.display = 'block';
            }
        } else {
            if (this._el) this._el.style.display = 'block';
        }
    };

    _hideDrops = () => {
        if (this._el) {
            this._el.style.display = 'none';
            this._el.style.background = 'transparent';
        }
    };

    _dragOver = (e) => {
        e.preventDefault();
        if (this._el) this._el.style.background = 'red';
    };

    _dragLeave = (e) => {
        e.preventDefault();

        if (this._el) this._el.style.background = 'transparent';
    };

    _drop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData('n'));

        if (this._el) {
            this._el.style.display = 'none';
            this._el.style.background = 'transparent';
        }

        if (!this.props.calc) {
            instance.addSibling(this.props.nid, data);
        } else {
            instance.addCalcVariable(this.props.gid, data);
        }
    };

    render() {
        let className = 'wing';
        if (this.props.left) className += ' left';
        if (!this.props.left) className += ' right';

        return (
            <div
                className="wing"
                style={{
                    display: 'none',
                    float: 'left',
                    height: 'auto',
                    width: '1px',
                    position: 'relative'
                }}
                ref={(el) => {
                    this._el = el;
                }}>
                <div
                    style={{
                        position: 'absolute',
                        top: '-10px',
                        bottom: '0',
                        left: 0,
                        zIndex: 9,
                        width: '20px',
                        marginLeft: '-15px'
                    }}
                    onDragOver={this._dragOver}
                    onDragLeave={this._dragLeave}
                    onDrop={this._drop}
                    className="wing-inner">

                </div>
            </div>
        )
    }
}


export default Wing;