import {instance} from '../plot_instance';

class DataMainDrop extends React.Component {
    static defaultProps = {
        hasItems: false
    };

    constructor(props) {
        super(props);


        ewars.subscribe("HIDE_DROPS", this._hide);
        ewars.subscribe('SHOW_DROPS', this._show);
    }

    componentWillUnmount() {
        ewars.unsubscribe('HIDE_DROPS', this._hide);
        ewars.unsubscribe('SHOW_DROPS', this._show);
    }

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = e.dataTransfer.getData("n");

        instance.addControl(data);

        ewars.emit('HIDE_DROPS');
    };

    _hide = () => {
        if (this._el) this._el.style.display = 'none';
    };

    _show = (type) => {
        if (type == 'C') {
            if (this._el) this._el.style.display = 'block';
        }
    };

    render() {
        return (
            <div
                onDrop={this._onDrop}
                onDragOver={this._onDragOver}
                className="data-control-drop"
                ref={(el) => {
                    this._el = el
                }}>

            </div>
        )
    }
}

export default DataMainDrop;