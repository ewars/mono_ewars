import Wing from './wing';
import CardListItem from './data.card';

import {instance} from '../plot_instance';


const GROUP_ACTIONS = [
    ['fa-caret-right', 'TOGGLE'],
    ['fa-calculator', 'ADD_CALC'],
    ['fa-copy', 'DUPE'],
    ['fa-trash', 'DELETE']
];

/**
 * Used to sort dimensions, measures and complexes
 */
const sortFunc = (a, b) => {
    if (a.o > b.o) return 1;
    if (a.o < b.o) return -1;
    return 0;
}


class Group extends React.Component {
    static defaultProps = {
        allowDimensions: true,
        allowMeasures: true,
        allowColour: true,
        allowSize: true
    };

    constructor(props) {
        super(props);

        ewars.subscribe("SHOW_DROPS", this._showDrops);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    componentWillUnmount() {
        ewars.unsubscribe('SHOW_DROPS', this._showDrops);
        ewars.unsubscribe('HIDE_DROPS', this._hideDrops);
    }

    _hideDrops = () => {

    };

    _showDrops = () => {

    };

    _action = (action) => {
        switch (action) {
            case 'DELETE':
                instance.deleteGroup(this.props.nodeId);
                return;
            case 'DUPE':
                instance.duplicateGroup(this.props.nodeId);
                return;
            case 'ADD_CALC':
                if (instance.type == 'DATAPOINT') {
                    if (instance.nodes.length >= 1) {
                        ewars.growl('Datapoint can not have multiple measures');
                        return;
                    }
                }
                instance.addCalculation(this.props.nodeId);
                return;
            case 'TOGGLE':
                this._toggle();
                return;
            default:
                return;
        }
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("n"));
        if (instance.type == 'DATAPOINT') {
            if (instance.nodes.length >= 1) {
                ewars.growl('Datapoints limited to 1 Measure');
                return;
            }
            if (data.t == 'D') {
                ewars.growl('Can not add Dimension to datapoint');
                return;
            }
        }

        instance.addNodeToGroup(this.props.nodeId, data);

        ewars.emit('HIDE_DROPS');
    };

    _removeItem = (id) => {
        let data = ewars.copy(this.props.data);
        let removalIndex;
        data.d.forEach((item, index) => {
            if (item[0] == id) removalIndex = index;
        })
        data.d.splice(index, 1);
        ewars.z.dispatch('PE', 'UPDATE_GROUP', data);
    };

    _toggle = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'none' ? 'block' : 'none';
    };

    render() {


        let measures = instance.getMeasuresForGroup(this.props.nodeId);
        let dimensions = instance.getDimensionsForGroup(this.props.nodeId);

        let items = dimensions.concat(measures);

        let hasItems = items.length > 0 ? true : false;
        let onDrag, onDrop;
        if (!hasItems) {
            onDrag = this._onDragOver;
            onDrop = this._onDrop;
        }

        let nodes = [];
        if (hasItems) {
            let lastItem;
            items.forEach((item, index) => {
                nodes.push(
                    <Wing
                        left={true}
                        key={item._ + 'L'}
                        gid={item.g}
                        T={item.type}
                        nid={item._}/>
                )
                nodes.push(
                    <CardListItem
                        key={item._}
                        nodeId={item._}
                        onRemove={this._removeItem}/>
                )
                lastItem = item;
            })

            nodes.push(
                <Wing
                    right={true}
                    key={'RWING'}
                    nid={lastItem._}
                    gid={lastItem.g}/>
            )
        }

        let controls = [
            ['fa-caret-right', 'TOGGLE'],
            ['fa-calculator', 'ADD_CALC']
        ];
        if (instance.canHaveMultipleGroups()) {
            controls.push(
                ['fa-copy', 'DUPE'],
                ['fa-trash', 'DELETE']
            )
        }

        return (
            <div className="data-group">
                <div className="data-group-header">
                    <div className="label"><i className="fal fa-chart-line"></i>&nbsp;Group</div>
                    <ewars.d.ActionGroup
                        right={true}
                        height="30px"
                        onAction={this._action}
                        actions={controls}/>
                </div>
                <div
                    ref={(el) => {
                        this._el = el;
                    }}
                    className="data-group-body"
                    onDragOver={onDrag}
                    onDrop={onDrop}>
                    <ewars.d.Layout>
                        <ewars.d.Row
                            style={{
                                background: "rgba(0,0,0,0.1)",
                                overflowY: 'scroll',
                                overflowX: 'show',
                                position: 'relative'
                            }}>
                            {nodes}
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </div>
            </div>
        )
    }
}

export default Group;