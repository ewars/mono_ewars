import {CONTROLS} from '../constants';

import {instance} from '../plot_instance';

const CONTROL_ACTIONS = [
    ['fa-caret-right', 'TOGGLE'],
    ['fa-trash', 'DELETE']
];

class ControlGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case 'DELETE':
                instance.removeNode(this.props.nodeId);
                return;
            case 'TOGGLE':
                this._toggle();
                return;
            default:
                return;
        }
    };

    _toggle = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'none' ? 'block' : 'none';
    };

    render() {
        let node = instance.getNode(this.props.nodeId);

        let label = CONTROLS[node._t].n || "Unknown control type";

        let Cmp = CONTROLS[node._t].c || null;
        let view;
        if (Cmp) {
            if (['COL_MAP', 'VAL_MAP'].indexOf(node._t) >= 0) {
                if (node._t == 'COL_MAP') view = <Cmp nid={node._} mode='COLOUR'/>;
                if (node._t == 'VAL_MAP') view = <Cmp nid={node._} mode="VALUE"/>;
            } else {
                view = <Cmp nid={node._}/>
            }
        }


        return (
            <ewars.d.Cell width="33.3%" style={{minWidth: '33.3%', minHeight: '250px'}}>
                <div className="data-group" style={{height: '100%'}}>
                    <div className="data-group-header" style={{background: '#5f631d'}}>
                        <div className="label"><i className="fal fa-gamepad"></i>&nbsp;{label}</div>
                        <ewars.d.ActionGroup
                            actions={CONTROL_ACTIONS}
                            right={true}
                            height="30px"
                            onAction={this._action}/>
                    </div>
                    <div
                        ref={(el) => {
                            this._el = el;
                        }}
                        style={{height: 'auto', minHeight: '0'}}
                        className="control-group-body">
                        {view}
                    </div>
                </div>
            </ewars.d.Cell>
        )
    }
}

export default ControlGroup;