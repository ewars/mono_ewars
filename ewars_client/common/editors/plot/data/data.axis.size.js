import {CONTROLS} from '../constants';

import {instance} from '../plot_instance';

const CONTROL_ACTIONS = [
    ['fa-caret-right', 'TOGGLE'],
    ['fa-trash', 'DELETE']
];

class ControlField extends React.Component {
    render() {
        return (
            <div className="control-field">
                <ewars.d.Row>
                    <ewars.d.Cell width="30%">{this.props.label}</ewars.d.Cell>
                    <ewars.d.Cell>
                        {this.props.children}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

class SizeAxis extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case 'DELETE':
                instance.removeNode(this.props.nodeId);
                return;
            case 'TOGGLE':
                this._toggle();
                return;
            default:
                return;
        }
    };

    _toggle = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'none' ? 'block' : 'none';
    };

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nodeId, e.target.name, e.target.value);
    };

    render() {
        let axis = instance.getNode(this.props.nodeId);
        return (
            <ewars.d.Cell width="33.3%" style={{minWidth: '33.3%'}}>
                <div className="data-group">
                    <div className="data-group-header" style={{background: '#333333'}}>
                        <div className="label"><i className="fal fa-circle"></i>&nbsp;Size Axis
                        </div>
                        <ewars.d.ActionGroup
                            actions={CONTROL_ACTIONS}
                            right={true}
                            height="30px"
                            onAction={this._action}/>
                    </div>
                    <div
                        ref={(el) => {
                            this._el = el;
                        }}
                        style={{height: 'auto', minHeight: '0'}}
                        className="data-group-body">
                        <div className="control-editor">
                            <div className="control-editor">
                                <ControlField label="Label">
                                    <input type="text" value={axis.node.label} onChange={this._onChange}/>
                                </ControlField>
                            </div>
                        </div>
                    </div>
                </div>
            </ewars.d.Cell>
        )
    }
}

export default SizeAxis;