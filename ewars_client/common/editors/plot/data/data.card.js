import CardItem from '../card/card.item';

import {isX} from '../utils';

import {instance} from '../plot_instance';

/**
 * Wraps a card item in the group box
 */
class CardListItem extends React.Component {
    static defaultProps = {
        index: null,
        data: null,
        filters: null,
        nextMeas: false,
        ax: {},
        isCalcVariable: false,
        gid: null,
        nid: null,
        cid: null,
        axis: []
    };

    constructor(props) {
        super(props)
    }

    _onRemove = () => {
        this.props.onRemove(this.props.data[0])
    };

    _onAxisChange = (e) => {
        console.log(this.props);
        instance.updateNodeAxis(this.props.nodeId, e.target.value);
    };

    render() {
        let node = instance.getNode(this.props.nodeId);

        let axisOptions;
        if (['HEATMAP', 'TABLE'].indexOf(instance.type)  >= 0) {
            axisOptions = [
                <option value="x">X axis</option>,
                <option value="y">Y axis</option>,
                <option value="DAGG">Disaggregate</option>
            ]
        } else {

            axisOptions = instance.getAxis().map(item => {
                let label = item.node.label + ' (' + ([true, "true"].indexOf(item.x) >= 0 ? 'x' : 'y') + ')';

                return (
                    <option value={item._}>{label}</option>
                )
            });

            if (node.t == 'D') {
                axisOptions.push(
                    <option value="DAGG">Disaggregate</option>
                )
            }
        }

        let hasAxis = instance.hasAxis();
        let isCalcVariable = (node.p && ['D', 'M', 'C'].indexOf(node.t) >= 0 && node.g == null);

        return (
            <ewars.d.Cell width="250px">
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="group-card-wrapper">
                            <CardItem
                                nodeId={this.props.nodeId}/>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                {!isCalcVariable && hasAxis ?
                    <ewars.d.Row height="35px" style={{background: "#333333"}}>
                        <ewars.d.Cell>
                            <select
                                onChange={this._onAxisChange}
                                className="card-axis"
                                name="axis"
                                value={node.x}>
                                {axisOptions}
                            </select>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    : null}
            </ewars.d.Cell>
        )
    }
}

export default CardListItem;