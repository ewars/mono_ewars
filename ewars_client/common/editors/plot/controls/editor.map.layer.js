import ControlField from '../components/ui.control_field';
import LocationSelectField from '../../../components/fields/LocationSelectField.react';
import BoolButton from '../components/ui.boolbutton';

import {instance} from '../plot_instance';

class MapLayerEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    _onParentChange = (prop, value) => {
        instance.updateNodeProp(this.props.nid, 'parent', value);
    };

    render() {
        let node = instance.getNode(this.props.nid);

        let specType = node.node.spec || 'DEFAULT';

        return (
            <div className="control-editor">
                <ControlField label="Type">
                    <select
                        onChange={this._onChange}
                        name="spec"
                        value={node.node.spec || 'DEFAULT'}>
                        <option value="DEFAULT">Default</option>
                        <option value="OF_TYPE">Of Type</option>
                        <option value="OF_GROUP">Of Group</option>
                    </select>
                </ControlField>
                <ControlField label="Location">
                    <LocationSelectField
                        name="parent"
                        onUpdate={this._onParentChange}
                        value={node.node.parent || null}/>
                </ControlField>
                {specType == 'OF_GROUP' ?
                    <ControlField label="Group(s)">

                    </ControlField>
                    : null}
                {specType == 'OF_TYPE' ?
                    <ControlField label="Type">

                    </ControlField>
                    : null}
                <ControlField label="Fill">
                    <input
                        onChange={this._onChange}
                        name="fill"
                        value={node.node.fill || null}
                        type="color"/>
                </ControlField>
                <ControlField label="Stroke">
                    <input
                        name="stroke"
                        value={node.node.stroke || null}
                        onChange={this._onChange}
                        type="color"/>
                </ControlField>.
                <ControlField label="Stroke width">
                    <input
                        name="strokeWidth"
                        onChange={this._onChange}
                        value={node.node.strokeWidth || null}
                        type="number"/>
                </ControlField>
                <ControlField label="Opacity">
                    <input
                        name="opacity"
                        onChange={this._onChange}
                        value={node.node.opacity || 1}
                        type="number"/>
                </ControlField>
                <ControlField label="Show labels">
                    <BoolButton
                        name="showLabels"
                        value={node.node.showLabels}
                        onChange={this._onChange}/>
                </ControlField>
                {node.node.showLabels ?
                    <ControlField label="Label colour">
                        <input
                            name="labelColor"
                            onChange={this._onChange}
                            value={node.node.labelColor || null}
                            type="color"/>
                    </ControlField>
                    : null}
                {node.node.showLabels ?
                    <ControlField label="Label size (px)">
                        <input
                            onChange={this._onChange}
                            name="labelSize"
                            value={node.node.labelSize || 10}
                            type="number"/>
                    </ControlField>
                    : null}
                <ControlField label="Merge">
                    <BoolButton
                        name="merge"
                        onChange={this._onChange}
                        value={node.node.merge}/>
                </ControlField>
                <ControlField label="Ignore points">
                    <BoolButton
                        name="ignorePoints"
                        onChange={this._onChange}
                        value={node.node.ignorePoints}/>
                </ControlField>
                <ControlField label="Z-Index">
                    <input
                        value={node.node.zIndex}
                        onChange={this._onChange}
                        name="zIndex"
                        type="number"/>
                </ControlField>
                <ControlField label="Snap to">
                    <BoolButton
                        value={node.node.snapTo}
                        onChange={this._onChange}
                        name="snapTo"/>
                </ControlField>
            </div>
        )
    }
}

export default MapLayerEditor;