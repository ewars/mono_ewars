import ControlField from '../components/ui.control_field';

import {instance} from '../plot_instance';

const POSITIONS = [
    ['TL', 'Top-left'],
    ['TC', 'Top-Center'],
    ['TR', 'Top-Right'],
    ['ML', 'Mid-Left'],
    ['MR', 'Mid-right'],
    ['BL', 'Bottom-left'],
    ['BC', 'Bottom-center'],
    ['BR', 'Bottom-right']
]

class ScaleEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    render() {
        let node = instance.getNode(this.props.nid);
        console.log(node);


        return (
            <div className="control-editor">
                <ControlField label="Position">
                    <select
                        onChange={this._onChange}
                        value={node.node.position || null}
                        name="position">
                        {POSITIONS.map( item => {
                            return <option value={item[0]}>{item[1]}</option>
                        })}
                    </select>
                </ControlField>
                <ControlField label="Orientation">
                    <select
                        value={node.node.orientation || null}
                        onChange={this._onChange}
                        name="orientation">
                        <option value="V">Vertical</option>
                        <option value="H">Horizontal</option>
                    </select>
                </ControlField>
            </div>
        )
    }
}

export default ScaleEditor;