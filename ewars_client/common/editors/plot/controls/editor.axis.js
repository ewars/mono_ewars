const DEFAULTS = {
    type: 'AXIS',
    _: null,
    c: {
        name: 'x', // or y, or some custom name
        label: 'Axis Label', // Label to be shown on axis
        min: null, // min value for axis
        max: null, // max value for axis
        valFmt: null, // How to format values in the axis
        tickInterval: null, // interval at which to set ticks along the axis
        color: null, // The colour of the axis, used for differentiating axis when multiple are set up
        pos: 'TOP', // TOP|RIGHT|BOTTOM|LEFT
    }
}

const FIELDS = [
    ['label', 'text', 'Label'],
    ['type', 'select', 'Axis Type'],
    ['ceiling', 'number', 'Ceiling'],
    ['floor', 'number', 'Floor'],
    ['max', 'number', 'Max'],
    ['min', 'number', 'Min'],
    ['opposite', 'bool', 'Opposite'],
    ['axis', 'select', 'Axis'],
    ['visible', 'bool', 'Visible'],
    ['labelRotation', 'number', 'Label rotation'],
    ['format', 'text', 'Label format'],
    ['label.color', 'color', 'Label color'],
    ['tickColor', 'color', 'Tick color'],
    ['logarithmic', 'bool', 'Logarithmic']
]

class AxisEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="control-editor">
                {FIELDS.map(field => {
                    let control;
                    if (field[1] == 'select') {
                        control = (
                            <select>
                                <option value=""></option>
                            </select>
                        )
                    } else {
                        control = <input type={field[1]}/>
                    }
                    return (
                        <div className="control-field">
                            <ewars.d.Row>
                                <ewars.d.Cell width="30%">{field[2]}</ewars.d.Cell>
                                <ewars.d.Cell>
                                    {control}
                                </ewars.d.Cell>
                            </ewars.d.Row>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default AxisEditor;