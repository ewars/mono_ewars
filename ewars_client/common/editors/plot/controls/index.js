import AnnotationsEditor from './editor.annotation';
import AxisEditor from './editor.axis';
import BandEditor from './editor.band';
import LineEditor from './editor.line';
import LegendEditor from './editor.legend';
import NavigatorEditor from './editor.navigator';
import PolygonEditor from './editor.polygon';
import TextEditor from './editor.text';
import TrendEditor from './editor.trend';
import ScaleEditor from './editor.scale';
import MapLayerEditor from './editor.map.layer';
import MapPointsEditor from './editor.map.points';
import MapPolygonEditor from './editor.map.polygon';
import RangeEditor from './editor.val.map';
import StackEditor from './editor.stack';

export {
    AnnotationsEditor,
    AxisEditor,
    BandEditor,
    LineEditor,
    LegendEditor,
    NavigatorEditor,
    PolygonEditor,
    TextEditor,
    TrendEditor,
    ScaleEditor,
    MapPointsEditor,
    MapLayerEditor,
    MapPolygonEditor,
    RangeEditor,
    StackEditor
}