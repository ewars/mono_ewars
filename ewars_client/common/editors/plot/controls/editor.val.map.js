import {ControlField} from '../components'

import {instance} from '../plot_instance';

const DEFAULTS = {
    type: 'BAND',
    _: null,
    c: {
        orientation: 'v', // v = vertical, h = horizontal
        start: 0, // start position along axis
        end: 1, // end position along axis,
        fill: 'transparent', // fill colour for the band
        line: 'red', // line colour for the band
        name: 'Threshold' // Label to plot along the band
    }
};

const ACTIONS = [
    ['fa-plus', 'ADD']
];

class RangeItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="range-item">
                <ewars.d.Row>
                    <ewars.d.Cell>
                        [from]
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        [to]
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        [mapping]
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

const DEFAULT_COLOUR_RANGES = [
    [-1, '0,0,0'],
    ['INF', '255,255,255']
];


class ColourRangeItem extends React.Component {
    static defaultProps = {
        index: null,
        data: [],
        nid: null
    };

    constructor(props) {
        super(props)
    }

    _onChange = (e) => {

    };

    render() {
        let canDelete = true;
        if (this.props.data[0] == -1) canDelete = false;
        if (this.props.data[0] == 'INF') canDelete = false;

        let label, labelClass = 'colour-range-tick';
        if (this.props.data[0] == -1) label = 0;
        if (this.props.data[0] == 'INF') {
            label = '∞';
            labelClass = 'colour-range-tick-bottom';
        }

        let node = instance.getNode(this.props.nid);
        let nodes = [];

        if (this.props.index == 0) {
            let upperLimit;
            if (node.node.steps == 1) upperLimit = '∞';
            let showEditor = node.node.steps > 1;
            return (
                <div className="colour-range-item">
                    <ewars.d.Row>
                        <ewars.d.Cell width="1px" style={{position: 'relative'}}>
                            <div className="colour-range-tick">
                                <table width="100%">
                                    <tr>
                                        <td>0</td>
                                        <td>&nbsp;</td>
                                        <td width="100%" className="col-tick"></td>
                                    </tr>
                                    {upperLimit ?
                                        <tr>
                                            <td>{upperLimit}</td>
                                            <td>&nbsp;</td>
                                            <td width="100%" className="col-tick"></td>
                                        </tr>
                                        : null}
                                </table>
                            </div>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            <input type="color"/>
                        </ewars.d.Cell>
                        {showEditor ?
                            <ewars.d.Cell>
                                <input type="number"/>
                            </ewars.d.Cell>
                            : null}
                    </ewars.d.Row>
                </div>
            )
        } else {
            if (this.props.index == node.node.steps - 1) {
                // last one
                return (
                    <div className="colour-range-item">
                        <ewars.d.Row>
                            <ewars.d.Cell width="1px" style={{position: 'relative'}}>
                                <div className="colour-range-tick">
                                    <table width="100%">
                                        <tr>
                                            <td>∞</td>
                                            <td>&nbsp;</td>
                                            <td width="100%" className="col-tick"></td>
                                        </tr>
                                    </table>
                                </div>
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                <input type="color"/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                )
            } else {
                return (
                    <div className="colour-range-item">
                        <ewars.d.Row>
                            <ewars.d.Cell width="1px" style={{position: 'relative'}}>
                                <div className="colour-range-tick">
                                    <table width="100%">
                                        <tr>
                                            <td>[limit]</td>
                                            <td>&nbsp;</td>
                                            <td width="100%" className="col-tick"></td>
                                        </tr>
                                    </table>
                                </div>
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                <input type="color"/>
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                <input type="number"/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                )
            }
        }
        return null;

    }
}

class RangePreview extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="range-preview">
                <table>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        )
    }
}

class ColourMapper extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    _rangeChange = (index, value) => {
        let node = instance.getNode(this.props.nid);
        let rangeTree = node.node.rangeTree || {};
        rangeTree[index] = value;
        instance.updateNodeProp(this.props.nid, 'rangeTree', rangeTree);
    };

    _colourChange = (index, value) => {
        let node = instance.getNode(this.props.nid);
        let rangeColours = node.node.rangeColours || {};
        rangeColours[index] = value;
        instance.updateNodeProp(this.props.nid, 'rangeColours', rangeColours);
    };

    render() {
        let node = instance.getNode(this.props.nid);
        let ranges = node.node.ranges || ewars.copy(DEFAULT_COLOUR_RANGES);

        let type = node.node.rangeType || 'CUSTOM';
        let steps = (node.node.steps != null && node.node.steps != undefined) ? node.node.steps : 0;

        let rangeTree = node.node.rangeTree || {};
        let rangeColours = node.node.rangeColours || {};

        let nodes = [];
        if (steps > 0) {
            nodes.push(
                <div className="breaker">
                    <div className="label">0</div>
                </div>
            );
            if (steps == 1) {
                nodes.push(
                    <div className="colour-range-item">
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <input type="color"/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                )
            } else if (steps > 1) {
                for (let i = 1; i <= steps; i++) {
                    nodes.push(
                        <div className="colour-range-item">
                            <ewars.d.Row>
                                <ewars.d.Cell width="50px">
                                    <input
                                        value={rangeColours[i] || '#333333'}
                                        onChange={(e) => {
                                            this._colourChange(i, e.target.value);
                                        }}
                                        type="color"/>
                                </ewars.d.Cell>
                                {i != steps ?
                                    <ewars.d.Cell>
                                        <input
                                            name="range_val"
                                            onChange={(e) => {
                                                this._rangeChange(i, e.target.value);
                                            }}
                                            value={rangeTree[i] || ''}
                                            type="number"/>
                                    </ewars.d.Cell>
                                    : null}
                            </ewars.d.Row>
                        </div>
                    )

                    if (i != steps) {
                        let rangeValue = rangeTree[i] || '•';
                        nodes.push(
                            <div className="breaker">
                                <div className="label">{rangeValue}</div>
                            </div>
                        )
                    }
                }
            }
            nodes.push(
                <div className="breaker">
                    <div className="label">∞</div>
                </div>
            )
        }

        return (
            <div className="control-editor">
                <ControlField label="Type">
                    <select
                        name="rangeType"
                        onChange={this._onChange}
                        value={type}>
                        <option value="CUSTOM">Custom</option>
                        <option value="P1">Preset 1</option>
                        <option value="P2">Preset 2</option>
                    </select>
                </ControlField>
                <ControlField label="Steps">
                    <input
                        name="steps"
                        value={steps}
                        onChange={this._onChange}
                        type="number"/>
                </ControlField>

                {type == 'CUSTOM' ?
                    <div className="range-items">
                        {nodes}
                    </div>
                    : null}
            </div>
        )
    }
}


class ValueMapper extends React.Component {
    constructor(props) {
        super(props)
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    _rangeChange = (index, value) => {
        let node = instance.getNode(this.props.nid);
        let rangeTree = node.node.rangeTree || {};
        rangeTree[index] = value;
        instance.updateNodeProp(this.props.nid, 'rangeTree', rangeTree);
    };

    _resultChange = (index, value) => {
        let node = instance.getNode(this.props.nid);
        let rangeResult = node.node.rangeResult || {};
        rangeResult[index] = value;
        instance.updateNodeProp(this.props.nid, 'rangeResult', rangeResult);
    };

    render() {
        let node = instance.getNode(this.props.nid);

        let steps = (node.node.steps != null && node.node.steps != undefined) ? node.node.steps : 0;

        let rangeTree = node.node.rangeTree || {};
        let rangeResult = node.node.rangeResult || {};

        let nodes = [];
        if (steps > 0) {
            nodes.push(
                <div className="breaker">
                    <div className="label">0</div>
                </div>
            );
            if (steps == 1) {
                nodes.push(
                    <div className="colour-range-item">
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <input
                                    value={rangeResult[0] || ''}
                                    onChange={(e) => {
                                        this._resultChange(1, e.target.value);
                                    }}
                                    type="text"/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                )
            } else if (steps > 1) {
                for (let i = 1; i <= steps; i++) {
                    nodes.push(
                        <div className="colour-range-item">
                            <ewars.d.Row>
                                <ewars.d.Cell width="50px">
                                    <input
                                        value={rangeResult[i] || ''}
                                        onChange={(e) => {
                                            this._resultChange(i, e.target.value);
                                        }}
                                        type="text"/>
                                </ewars.d.Cell>
                                {i != steps ?
                                    <ewars.d.Cell>
                                        <input
                                            name="range_val"
                                            onChange={(e) => {
                                                this._rangeChange(i, e.target.value);
                                            }}
                                            type="number"/>
                                    </ewars.d.Cell>
                                    : null}
                            </ewars.d.Row>
                        </div>
                    )

                    if (i != steps) {
                        let rangeValue = rangeTree[i] || '•';
                        nodes.push(
                            <div className="breaker">
                                <div className="label">{rangeValue}</div>
                            </div>
                        )
                    }
                }
            }
            nodes.push(
                <div className="breaker">
                    <div className="label">∞</div>
                </div>
            )
        }

        return (
            <div className="control-editor">
                <ControlField label="Steps">
                    <input
                        name="steps"
                        value={steps}
                        onChange={this._onChange}
                        type="number"/>
                </ControlField>

                <div className="range-items">
                    {nodes}
                </div>
            </div>
        )
    }
}

class RangeEditor extends React.Component {
    static defaultProps = {
        mode: 'VALUE',
        nid: null
    };

    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nid, e.target.name, e.target.value);
    };

    _action = (action) => {
        switch (action) {
            case 'ADD':
                let node = instance.getNode(this.props.nid);
                let ranges = ewars.copy(node.node.ranges || []);
                ranges.push([0, 0, null]);
                instance.updateNodeProp(this.props.nid, 'ranges', ranges);
                break;
            default:
                break;
        }
    }

    render() {
        if (this.props.mode == 'COLOUR') return <ColourMapper nid={this.props.nid}/>;
        if (this.props.mode == 'VALUE') return <ValueMapper nid={this.props.nid}/>;
        return null;
    }
}

export default RangeEditor;