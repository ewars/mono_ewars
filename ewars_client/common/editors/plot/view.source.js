import {instance} from './plot_instance';

class SourceView extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let data = instance.getFinal();
        return (
            <ewars.d.Panel>
                <pre style={{color: '#333333', padding: '8px'}}>{JSON.stringify(data, null, '\t')}</pre>
            </ewars.d.Panel>
        )
    }
}

export default SourceView;