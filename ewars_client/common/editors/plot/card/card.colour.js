import {COLORS} from '../constants';

import {instance} from '../plot_instance';

class CardColor extends React.Component {
    static defaultProps = {
        gid: null,
        nid: null,
        data: {}
    };

    constructor(props) {
        super(props);

        ewars.subscribe("SHOW_DROPS", this._showDrops);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    componentDidMount() {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    };

    componentWillUnmount() {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
        ewars.unsubscribe("SHOW_DROPS", this._showDrops);
        ewars.unsubscribe("HIDE_DROPS", this._hideDrops);
    };

    _showDrops = (type) => {
        if (['M', 'D'].indexOf(type) >= 0) {
            if (this._ael) this._ael.style.border = "1px solid #F2F2F2";
        }
    };

    _hideDrops = () => {
        if (this._ael) this._ael.style.border = "1px solid #505152";
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData('n'));
        ewars.emit('HIDE_DROPS');

        instance.addColourNode(this.props.nid, data);
    };

    _onToggle = () => {
        if (this._el) {
            if (this._shown) {
                this._shown = false;
                this._el.style.display = "none";
            } else {
                this._shown = true;

                this._el.style.display = "block";
            }
        }
    };

    handleBodyClick = (evt) => {
        if (this._el) {
            const area = this._el;

            if (!area.contains(evt.target)) {
                this._shown = false;
                this._el.style.display = "none";
            }
        }
    };

    _set = (style) => {
        this._shown = false;
        if (this._el) this._el.style.display = "none";
    };

    _showCol = () => {
        if (this._colEl) {
            this._colEl.focus();
            this._colEl.click();
        }
    };

    _setColor = (color) => {
        // ewars.z.dispatch('')
        this._el.style.display = 'none';
        instance.updateNodeProp(this.props.nid, 'color', color);
    };

    _setOpacity = (e) => {
        this._el.style.display = 'none';
        instance.updateNodeProp(this.props.nid, 'opacity', e.target.value);
    };

    render() {
        let colors = [];
        let node = instance.getNode(this.props.nid);

        COLORS.forEach((col, cIndex) => {
            let rows = col.map((item, index) => {
                let style  = {background: 'rgb(' + item + ')'};
                if (node.node.color == item) style.border = '1px solid #F2F2F2';
                return (
                    <ewars.d.Row
                        key={"COLOR_" + item}
                        height="12px"
                        onClick={() => {
                            this._setColor(item);
                        }}
                        style={style}/>
                )
            })

            colors.push(
                <ewars.d.Cell key={'COLOR_COL_' + cIndex}>{rows}</ewars.d.Cell>
            )
        });

        return (
            <div
                ref={el => this._ael = el}
                onDragOver={this._onDragOver}
                onDrop={this._onDrop}
                className="plot-settings">
                <div className="inner" onClick={this._onToggle}>
                    <div className="icon"><i className="fal fa-adjust"></i></div>
                    <div className="text">Color</div>
                </div>
                <div
                    ref={el => this._el = el}
                    className="axis-drop-options">
                    <div className="axis-drop-inner" style={{background: '#333333'}}>
                        <label htmlFor="color">Color</label>
                        <div className="color-grid">
                            <ewars.d.Row height="100px" style={{height: "100px"}}>
                                {colors}
                            </ewars.d.Row>
                        </div>
                        <div className="custom-color" onClick={this._showCol}>More colors...</div>
                        <input type="color" ref={el => this._colEl = el} style={{visibility: "hidden"}}/>
                        <div className="line"></div>
                        <div className="padder">
                            <label htmlFor="">Opacity</label>
                            <input
                                min={0}
                                max={1}
                                value={node.node.opacity || 1}
                                step={0.1}
                                type="range"
                                list="opacity"/>
                            <datalist id="opacity" onChange={this._setOpacity}>
                                <option value="0" label="0%"/>
                                <option value="0.1"/>
                                <option value="0.2"/>
                                <option value="0.3"/>
                                <option value="0.4"/>
                                <option value="0.5" label="50%"/>
                                <option value="0.6"/>
                                <option value="0.7"/>
                                <option value="0.8"/>
                                <option value="0.9"/>
                                <option value="1" label="100%"/>
                            </datalist>


                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default CardColor;