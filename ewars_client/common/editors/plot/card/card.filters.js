import Measure from '../Measure';
import Dimension from '../Dimension';

import * as NUGGETS from '../nuggets/';

import {instance} from '../plot_instance';


const STYLE = {
    WRAPPER: {
        display: "block",
        background: "rgba(0,0,0,0.3)",
        borderRadius: '3px',
        color: "#F2F2F2",
        height: "inherit",
        paddingLeft: "8px",
        paddingRight: "8px",
        paddingTop: "8px",
        paddingBottom: "8px",
        cursor: "pointer",
        position: "relative"
    }
}

class LockedFilter extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log(ewars.g);
        console.log(this.props.fid);
        return (
            <div style={STYLE.WRAPPER}>
                <i className="fal fa-lock"></i>&nbsp;{formName}
            </div>
        )
    }
}

class CardFilters extends React.Component {
    static defaultProps = {
        nid: null
    };

    constructor(props) {
        super(props);
    }

    render() {
        let filters = instance.getFiltersForNode(this.props.nid);
        let node = instance.getNode(this.props.nid);
        //
        // let extra_childs = [];
        // if (node.fid) {
        //     extra_childs.push(
        //         <LockedFilter fid={node.fid}/>
        //     )
        // }

        let items = [];

        filters.forEach(node => {
            items.push(
                <NUGGETS.NuggetFilter data={node}/>
            )
        });


        return (
            <div
                className="filter-drop"
                style={{display: "block", minHeight: "100%"}}>
                {filters.map((item) => {
                    if (item.t == "D") {
                        return <Dimension
                            nid={item._}
                            key={item._}/>
                    } else {
                        return <Measure
                            nid={item._}
                            key={item._}/>
                    }
                })}
            </div>
        )
    }
}

export default CardFilters;
