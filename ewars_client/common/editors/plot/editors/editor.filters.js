import SelectField from '../../../../common/cmp/fields/SelectField';
import PeriodSelector from '../../../cmp/fields/PeriodSelector';

import {instance} from '../plot_instance';

const MODAL = {
    position: "absolute",
    top: "30px",
    left: "50%",
    width: "800px",
    marginLeft: "-400px",
    maxHeight: "400px",
    boxShadow: "1px 0px 3px #000000"
};

const WRAPPER = {
    position: "fixed",
    display: "block",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    background: "rgba(0,0,0,0.7)",
    zIndex: 99
}

const TYPES = {
    FORM: {
        data: function () {
            return new Promise((resolve, reject) => {
                if (ewars.g.forms) {
                    resolve(ewars.g.forms);
                } else {
                    ewars.tx("com.ewars.query", ["form", ["id", "name"], {status: {eq: "ACTIVE"}}, null, null, null, null])
                        .then((resp) => {
                            ewars.g.forms = resp;
                            resolve(ewars.g.forms);
                        })
                }
            })
        },
        ops: ["EQ", "NEQ", "IN", "NIN"],
        t: "SELECT",
        map: ["id", "name"]
    },
    FIELD: {
        data: function (def) {
            return new Promise((resolve, reject) => {
                let id = node.n.replace('FIELD.', '');
                if (ewars.g.fields[id]) {
                    resolve(ewars.g.fields[id]);
                } else {
                    ewars.tx("com.ewars.form.field", [id])
                        .then((resp) => {
                            ewars.g.fields[id] = resp;
                            resolve(resp);
                        })
                }
            })
        },
        ops: ["EQ", "NEQ", "GT", "GTE", "LT", "LTE"],
        t: "FIELD_DEFINED"
    },
    LOCATION_TYPE: {
        data: function (def) {
            return new Promise((resolve, reject) => {
                ewars.tx("com.ewars.query", ["location_type", ["id", "name"], {status: {eq: "ACTIVE"}}, null, null, null, null])
                    .then((resp) => {
                        resolve(resp);
                    })
            })
        },
        ops: ["EQ", "NEQ", "IN", "NIN"],
        t: "SELECT",
        map: ["id", "name"]
    },
    RECORD_DATE: {
        data: function () {
            return new Promise((resolve, reject) => {
                resolve({});
            })
        },
        ops: [],
        t: "PERIOD"
    }
}

const FILTER_ROW = {
    display: "block",
    borderRadius: "16px",
    background: "rgba(0,0,0,0.1)",
    margin: "8px",
    color: "#F2F2F2"
}

const SELECT_STYLE = {
    borderTopLeftRadius: "16px",
    borderBottomLeftRadius: "16px"
}

const FILT_ACTIONS = [
    ["fa-trash", "REMOVE"]
]

ewars.g.fields = {};

class FilterRow extends React.Component {
    static defaultProps = {
        type: null,
        data: null,
        ext: null
    };

    constructor(props) {
        super(props)
    }

    _onChange = (e) => {
        let newData = ewars.copy(this.props.data.split(":"));

        if (!newData) newData = [];
        if (e.target.name == "val") newData[1] = e.target.value;
        if (e.target.name == "cmp") newData[0] = e.target.value;

        this.props.onChange(this.props.index, newData.join(":"));

    };

    _action = (action) => {
        if (action == 'REMOVE') {
            this.props.onRemove(this.props.data);
        }
    };

    render() {
        let view;

        if (this.props.type.t == "SELECT") {
            let multiple = false;
            if (["NIN", "IN"].indexOf(this.props.data.split(":")[0]) >= 0) {
                multiple = true;
            }
            view = (
                <select
                    value={this.props.data.split(":")[1]}
                    onChange={this._onChange}
                    multiple={multiple}
                    name="val">
                    <option value={null}>No Selection</option>
                    {(this.props.opts || this.props.ext).map((item) => {
                        return <option value={item[this.props.type.map[0]]}>{__(item[this.props.type.map[1]])}</option>
                    })}
                </select>
            )
        } else if (this.props.type.t == "NUMERIC") {
            view = (
                <input
                    type="number"
                    value={this.props.data.split(":")[1] || ""}
                    name="val"
                    onChange={this._onChange}/>
            )
        } else if (this.props.type.t == "FIELD_DEFINED") {
            if (this.props.ext[3] == "select") {
                view = (
                    <select
                        value={this.props.data.split(":")[1]}
                        onChange={this._onChange}
                        name="val">
                        <option value={null}>No Selection</option>
                        {this.props.ext[4].options.map((item) => {
                            return <option value={item[0]}>{__(item[1])}</option>
                        })}
                    </select>
                )
            } else if (this.props.ext[3] == "number") {
                view = (
                    <input
                        type="number"
                        value={this.props.data.split(":")[1] || ""}
                        name="val"
                        onChange={this._onChange}/>
                )
            }
        }


        return (
            <div style={FILTER_ROW}>
                <ewars.d.Row>
                    <ewars.d.Cell width="100px">
                        <select
                            value={this.props.data.split(":")[0]}
                            onChange={this._onChange}
                            name="cmp"
                            style={SELECT_STYLE}>
                            <option value={null}>No Selection</option>
                            {this.props.type.ops.map((item) => {
                                return <option value={item}>{item}</option>
                            })}
                        </select>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                    <ewars.d.Cell width="30px" style={{display: "block"}}>
                        <ewars.d.ActionGroup
                            right={true}
                            actions={FILT_ACTIONS}
                            onAction={this._action}/>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </div>
        )
    }
}

function isObject(val) {
   return val === Object(val);
}

class ModalDialog extends React.Component {
    static defaultProps = {
        filters: [],
        gid: null,
        nid: null,
        data: []
    };

    constructor(props) {
        super(props);

        let node = instance.getNode(props.nid);
        let conf = node.f || [];

        let type;

        if (node.isField()) type = TYPES.FIELD;

        if (TYPES[node.n]) type = TYPES[node.n];

        this.state = {
            data: null,
            filters: conf || []
        };

        if (type) {
            type.data(node)
                .then((result) => {
                    this.setState({data: result})
                })
        }
    }

    _addFilter = () => {
        let filters = this.state.filters;
        filters.push("");
        this.setState({filters: filters})
    };

    _onChange = (index, newData) => {
        let data = this.state.filters;
        data[index] = newData;
        this.setState({
            filters: data
        })
    };

    _save = () => {
        //TODO: Update to use instance
        this.props.onSave(this.state.filters);
    };

    _onPeriodChange = (prop, value) => {
        let filters = [value.join(',')]
        this.setState({
            filters: filters
        })
    };

    _removeFilterRow = (row) => {
        let items = ewars.copy(this.state.filters),
            rIndex;

        items.forEach((item, index) => {
            if (item == row) rIndex = index;
        })

        items.splice(rIndex, 1);
        this.setState({
            filters: items
        })
    };

    render() {
        let view,
            showAdd = true;

        let node = instance.getNode(this.props.nid);

        if (!this.state.data) {
            view = <div className="placeholder">Loading...</div>
        } else {
            let type;
            if (node.isField()) {
                type = TYPES.FIELD;
            } else {
                type = TYPES[node.n];
            }

            if (type.t == "PERIOD") {
                view = (
                    <PeriodSelector
                        onUpdate={this._onPeriodChange}
                        name="period"
                        value={this.state.filters[0].split(',') || ['{NOW}', '{NOW}']}/>
                );
                showAdd = false;
            } else {
                view = this.state.filters.map((item, index) => {
                    return <FilterRow
                        type={type}
                        index={index}
                        nid={node._}
                        onChange={this._onChange}
                        onRemove={this._removeFilterRow}
                        key={"FILT" + index}
                        ext={this.state.data}
                        data={item}/>
                })
            }

        }

        return (
            <div style={WRAPPER}>
                <div style={MODAL}>
                    <ewars.d.Layout>
                        <ewars.d.Toolbar label="Filter Editor" icon="fa-filter">
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    onClick={this._save}
                                    icon="fa-save"/>
                                <ewars.d.Button
                                    onClick={this.props.onClose}
                                    icon="fa-times"/>
                            </div>
                            {showAdd ?
                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        icon="fa-plus"
                                        onClick={this._addFilter}/>
                                </div>
                                : null}
                        </ewars.d.Toolbar>
                        <ewars.d.Row>
                            <ewars.d.Cell style={{background: "#333333", minHeight: "200px"}}>
                                {view}
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </div>
            </div>
        )
    }
}

export default ModalDialog;
