import PeriodSelector from '../../../cmp/fields/PeriodSelector';
import {instance} from '../plot_instance';

const STYLES = {
    MODAL: {
        position: "absolute",
        top: "30px",
        left: "50%",
        width: "800px",
        marginLeft: "-400px",
        maxHeight: "400px",
        boxShadow: "1px 0px 3px #000000"
    },
    WRAPPER: {
        position: "fixed",
        display: "block",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        background: "rgba(0,0,0,0.7)",
        zIndex: 99
    }
};


const MODAL_ACTIONS = [
    ['fa-save', 'SAVE'],
    ['fa-times', 'CLOSE']
];

class DateFilter extends React.Component {
    constructor(props) {
        super(props);

        let node = instance.getNode(this.props.nid);

        let filt = node.f;
        if (!node.f || node.f.length <= 0) {
            filt = ['{NOW}-30D,{NOW}']
        }

        this.state = {
            filters: filt
        }
    }

    _onPeriodChange = (prop, value) => {
        this.setState({
            filters: [value.join(',')]
        })
    };

    _action = (action) => {
        switch(action) {
            case 'CLOSE':
                this.props.onClose();
                return;
            case 'SAVE':
                this.props.onSave(this.state.filters);
                return;
            default:
                return;
        }
    };

    render() {
        return (
            <div style={STYLES.WRAPPER}>
                <div style={STYLES.MODAL}>
                    <ewars.d.Layout>
                        <ewars.d.Toolbar label="Date Filters" icon="fa-filter">
                            <ewars.d.ActionGroup
                                right={true}
                                onAction={this._action}
                                actions={MODAL_ACTIONS}/>

                        </ewars.d.Toolbar>
                        <ewars.d.Row height="300px" style={{background: '#333333'}}>
                            <ewars.d.Cell>
                                <PeriodSelector
                                    onUpdate={this._onPeriodChange}
                                    name="period"
                                    mode="DEFAULT"
                                    onChange={this._onPeriodChange}
                                    value={this.state.filters[0].split(',')}/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </div>
            </div>
        )
    }

}

export default DateFilter;