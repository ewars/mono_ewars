import CTX_DIM_LOCATION from './ctx.dimension.location';
import CTX_DIM_RECORD_DATE from './ctx.dimension.date'

export {
    CTX_DIM_LOCATION,
    CTX_DIM_RECORD_DATE
}