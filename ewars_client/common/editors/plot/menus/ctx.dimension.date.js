export default [
    ["FILTER", "Filter...", "fa-filter", null],
    "-",
    ["I_DAY", "Daily Interval", null, null],
    ["I_WEEK", "Weekly Interval", null, null],
    ["I_MONTH", "Monthly Interval", null, null],
    ["I_YEAR", "Annual Interval", null, null],
    "-",
    ["STYLE", "Style...", "fa-paint-brush", null],
    "-",
    ["REMOVE", "Remove", "fa-trash", null]
]