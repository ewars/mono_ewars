// Icons for different types of nuggets
export default {
    filter: 'fal fa-filter',
    size: 'fal fa-circle',
    colour: 'fal fa-paint-brush',
    detail: 'fal fa-tag'
};