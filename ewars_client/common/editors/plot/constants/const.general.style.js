import POSITIONS from './const.positions';

export default {
    t_general: {
        l: 'General Settings',
        t: 'H'
    },
    title: {
        l: 'title',
        t: 'bool',
        d: false,
        __: {
            titleText: {l: 'text', t: 'text', d: 'Chart'},
            titleColor: {l: 'colour', t: 'color', d: '#333333'},
            titleSize: {l: 'size (px)', t: 'number', d: 12},
            titleWeight: {
                l: 'weight',
                t: 'select',
                d: 'normal',
                o: [
                    ['normal', 'Normal'],
                    ['bold', 'Bold']
                ]
            },
            titlePos: {
                l: 'Position',
                t: 'select',
                o: POSITIONS
            }
        }
    },
    subTitle: {
        l: 'Sub-Title',
        t: 'bool',
        d: false,
        __: {
            sTitleText: {l: 'text', t: 'text', d: 'Chart'},
            sTitleColor: {l: 'colour', t: 'color', d: '#333333'},
            sTitleSize: {l: 'size (px)', t: 'number', d: 12},
            sTitleWeight: {
                l: 'weight',
                t: 'select',
                d: 'normal',
                o: [
                    ['normal', 'Normal'],
                    ['bold', 'Bold']
                ]
            },
            sTitlePos: {
                l: 'Position',
                t: 'select',
                o: POSITIONS
            }
        }
    },
    t_container_style: {
        l: "Container Styling",
        t: "H"
    },
    border: {
        l: "border",
        t: 'bool',
        d: false,
        // This property can break out into other properties
        __: {
            borderColor: {l: 'color', t: 'color', d: 'transparent'},
            borderWidth: {l: 'width', t: 'number', d: 0},
            borderRadius: {l: 'radius', t: 'number', d: 0}
        }
    },
    margin: {
        l: "margin",
        t: 'text',
        __: {
            marginTop: {l: 'top', t: 'number', d: 0},
            marginRight: {l: 'right', t: 'number', d: 0},
            marginBottom: {l: 'bottom', t: 'number', d: 0},
            marginLeft: {l: 'left', t: 'number', d: 0}
        }
    },
    padding: {
        l: "padding",
        t: 'text',
        __: {
            paddingTop: {l: 'top', t: 'number', d: 0},
            paddingRight: {l: 'right', t: 'number', d: 0},
            paddingBottom: {l: 'bottom', t: 'number', d: 0},
            paddingLeft: {l: 'left', t: 'number', d: 0}
        }
    },
    boxShadow: {
        l: "box shadow",
        t: 'bool',
        __: {
            bsXOffset: {l: 'x', t: 'number', d: 0},
            bsYOffset: {l: 'y', t: 'number', d: 0},
            bsBlur: {l: 'blur', t: 'number', d: 0},
            bdColor: {l: 'color', t: 'number', d: 0}
        }
    },
    backgroundColor: {
        l: "background",
        t: 'color'
    }
}