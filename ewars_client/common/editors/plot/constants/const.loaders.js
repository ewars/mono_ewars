// Loads up data and caches it for different types

let CACHE = {};
let LOCATION_CACHE = {};

export default {
    LOCATION_TYPE: () => {
        return new Promise((resolve, reject) => {
            if (CACHE.LOCATION_TYPE) {
                resolve(CACHE.LOCATION_TYPE);
            } else {
            ewars.tx('com.ewars.query', ['location_type', null, null, null, null, null])
                .then(resp => {
                    CACHE.LOCATION_TYPE = resp;
                    resolve(resp);
                })

            }
        })
    },
    LOCATION: () => {

    },
    FORM_STATUS: () => {
        return new Promise((resolve, reject) => {
            resolve([
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ])
        })
    },
    ALERT_STATE: () => {
        return new Promise((resolve, reject) => {
            resolve([
                ['OPEN', 'Open'],
                ['CLOSED', 'Closed']
            ])
        })
    },
    ALARM_STATE: () => {
        return new Promise((resolve, reject) => {
            resolve([
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ])
        })
    }
}