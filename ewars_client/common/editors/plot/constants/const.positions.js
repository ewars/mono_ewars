export default [
    ['TL', 'Top-left'],
    ['TC', 'Top-center'],
    ['TR', 'Top-right'],
    ['ML', 'Mid-left'],
    ['MR', 'Mid-right'],
    ['BL', 'Bottom-left'],
    ['BC', 'Bottom-center'],
    ['BR', 'Bottom-right']
]