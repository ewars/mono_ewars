// PRESET_NAMES
export default {
    FIELD: "Field",

    LOCATIONS: "Number of Locations",
    LOCATION: "Location",
    LOCATION_TYPES: "Number of Location Types",
    LOCATION_TYPE: "Location Type",
    LOCATION_STATUS: "Location Status",
    LOCATION_GROUP: "Location Group(s)",
    LOCATION_GROUPS: "Number of Location Groups",
    LOCATION_GEOM: "Location Geom.",

    FORM: "Form",
    FORMS: "Number of Forms",
    FORM_STATUS: "Form Status",

    RECORDS: "Number of Records",
    RECORD_DATE: "Record Date",
    RECORD_SUBMISSION_DATE: "Record Subm. Date",
    RECORD_STATUS: "Record Status",

    ALERTS: "Number of Alert",
    ALERT_STATE: "Alert State",
    ALERT_STAGE: "Alert Stage",
    ALERT_DATE: 'Alert Date',

    ALARM: "Alarm",
    ALARMS: "Number of Alarms",
    ALARM_STATUS: "Alarm Status",
    ALARM_STATE: "Alarm State",

    RECORDS_LATE: "Number of Late Records",
    RECORDS_TIMELY: "Number of On-time Records",
    COMPLETENESS: "Record Completeness",
    TIMELINESS: "Record Timeliness",

    USER: "User",
    USER_TYPE: "User Type",

    DEVICE: "Device",
    DEVICE_TYPE: "Device Type",
    DEVICE_TYPES: "Number of Device Types",

    ORGANIZATION: "Organization",
    ORGANIZATIONS: "Number of Organizations"

}
