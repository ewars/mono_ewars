import ALLOWED_FILTERS from './const.allowed';
import PRESET_NAMES from './const.names';
import PRESETS from './const.presets';
import STYLE from './const.style';
import DP_STYLE from './const.datapoint.style'
import HEATMAP_STYLE from './const.heatmap.style';
import MAP_STYLE from './const.map.style';
import LINE_STYLES from './const.line.styles';
import PIE_STYLE from './const.pie.style';
import REDUCTIONS from './const.reductions';
import INTERVALS from './const.intervals';
import COLORS from './const.colors';
import CONTROLS from './const.controls';
import DASH_STYLES from './const.style.dashes';
import MARKER_STYLES from './const.style.markers';
import ICON_MAP from './const.icons';
import X_COLOUR_MAP from './const.x.colours';
import LOADERS from './const.loaders';
import TABLE_STYLE from './const.table.style';
import NUM_FORMATS from './const.num_formats';

export {
    NUM_FORMATS,
    ALLOWED_FILTERS,
    PRESET_NAMES,
    PRESETS,
    STYLE,
    DP_STYLE,
    HEATMAP_STYLE,
    MAP_STYLE,
    LINE_STYLES,
    REDUCTIONS,
    INTERVALS,
    COLORS,
    CONTROLS,
    TABLE_STYLE,
    DASH_STYLES,
    MARKER_STYLES,
    PIE_STYLE,
    ICON_MAP,
    X_COLOUR_MAP,
    LOADERS
}

