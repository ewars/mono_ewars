export default [
    ['Solid', 'Default (Solid)'],
    ['ShortDash', ' - '],
    ['ShortDot', ' • '],
    ['ShortDashDot', ' - • '],
    ['ShortDashDotDot', ' - • • '],
    ['Dot', ' • '],
    ['Dash', ' - '],
    ['LongDash', ' -- '],
    ['DashDot', ' -• '],
    ['LongDashDot', ' -- • '],
    ['LongDashDotDot', ' -- • • ']
]