export default {
    name: 'Location Group',
    icon: 'fa-map-marker',
    f: ['LOCATION_GROUP', 'LOCATION', 'LOCATION_TYPE', 'LOCATION_STATUS'],
    t: 'location_group'
}