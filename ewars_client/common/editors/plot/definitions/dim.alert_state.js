export default {
    name: 'Alert State',
    icon: 'fa-exclamation-triangle',
    f: ['ALERT_STATE'],
    t: 'select',
    o: [
        'OPEN,Open',
        'CLOSED,Closed'
    ]
}