export default {
    name: 'Source Type',
    icon: 'fa-mobile',
    f: ['SOURCE'],
    m: ['RECORDS', 'FIELD'],
    t: 'SELECT',
    o: [
        'MOBILE,Mobile',
        'SYSTEM,Web',
        'DESKTOP,Desktop',
        'API,API',
        'SMS,SMS',
        'EMAIL,Email'
    ]
}