export default {
    name: 'Alert Risk',
    icon: 'fa-exclamation-triangle',
    f: ['ALERT_RISK'],
    m: ['ALERTS'],
    t: 'select',
    o: [
        'LOW,Low',
        'MODERATE,Moderate',
        'HIGH,High',
        'SEVERE,Severe'
    ]
}