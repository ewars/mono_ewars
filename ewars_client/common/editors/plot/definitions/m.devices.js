export default {
    name: 'Number of Devices',
    icon: 'fa-hashtag',
    t: 'NUMERIC',
    aggs: [
        'COUNT,Count'
    ]
}