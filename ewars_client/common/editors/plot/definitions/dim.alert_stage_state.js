export default {
    name: 'Alert Stage Stage',
    icon: 'fa-exclamation-triangle',
    f: ['ALERT_STAGE_STATE'],
    t: 'select',
    o: [
        'PENDING,Pending',
        'COMPLETED,Completed'
    ]
}