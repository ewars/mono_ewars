export default {
    name: 'Record Submission Date',
    icon: 'fa-calendar',
    f: ['RECORD_SUBMISSION_DATE'],
    m: ['RECORDS', 'FIELD'],
    t: 'DATE'
}