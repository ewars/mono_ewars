import {PRESET_NAMES} from '../constants/';

import {instance} from '../plot_instance';

// Map colours to different types
const COLOUR_MAP = {
    'D': 'steelblue',
    'M': 'rgba(0,0,0,0.8)', // dark grey
    'C': 'rgb(58, 32, 1)' // umber/burnt orange
};

// Icons for different types of nuggets
const ICON_MAP = {
    filter: 'fal fa-filter',
    size: 'fal fa-circle',
    colour: 'fal fa-paint-brush',
    detail: 'fal fa-tag'
};

class Base extends React.Component {
    static defaultProps = {
        filter: false,
        colour: false,
        size: false,
        type: 'D', // dimension, measure or calculcation [D,M,C]
        data: {}
    };

    _label = 'Unknown';
    _style = {background: 'rgba(0,0,0,0.9)'};

    constructor(props) {
        super(props);
        // Overridden in the extended component

        console.log(props.nid);
        let node = instance.getNode(props.nid);
        console.log(node);

        // Set up standard properties for all nuggets
        if (node._t == 'F') this._icon = ICON_MAP.filter;
        if (node._t == 'C') this._icon = ICON_MAP.colour;
        if (node._t == 'S') this._icon = ICON_MAP.size;
        if (node._t == 'D') this._icon = ICON_MAP.detail;

        if (node.t == 'D') this._style.background = COLOUR_MAP.D;
        if (node.t == 'M') this._style.background = COLOUR_MAP.M;
        if (node.t == 'C') this._style.background = COLOUR_MAP.C;

        this._label = node.getShortLabel();
    }

    render() {
        return (
            <div className="placeholder">Not implemented</div>
        )
    }
}

export default Base;
