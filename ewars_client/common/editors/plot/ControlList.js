import {instance} from './plot_instance';

const TYPES = {
    DEFAULT: ['ANNOTATION', 'AXIS', 'BAND', 'BELL', 'LEGEND', 'LINE', 'SCALE', 'TREND', 'STACK'],
    TABLE: ['LEGEND', 'SCALE', 'VAL_MAP', 'COL_MAP'],
    DATAPOINT: ['SCALE', 'VAL_MAP', 'COL_MAP'],
    PIE: ['LEGEND'],
    MAP: ['LEGEND', 'SCALE', 'MAP_LAYER', 'MAP_POINT', 'COL_MAP'],
    HEATMAP: ['LEGEND', 'SCALE', 'VAL_MAP', 'COL_MAP'],
    OTHER: ['LEGEND', 'TEXT']
}


const CONTROLS = [
    ["ANNOTATION", "Annotation"],
    ["AXIS", "Axis"],
    ["BAND", "Plot band"],
    ['BELL', 'Bell curve'],
    ["LEGEND", "Legend"],
    ["LINE", "Plot line"],
    ['LOG', 'Logarithm line'],
    ['MAP_LAYER', 'Static Map layer'],
    ['MAP_POINT', 'Static Map point'],
    ['MAP_POLY', 'Static map poly'],
    ['MAP_NAV', 'Map navigation controls'],
    ['MAP_ZOOM', 'Map zoom controls'],
    ["NAVIGATOR", "Navigator"],
    ['POLYGON', 'Polygon'],
    ["SCALE", "Scale"],
    ["TEXT", "Text"],
    ["TREND", "Trend line"],
    ['VAL_MAP', 'Value mapping'],
    ['COL_MAP', 'Colour mapping'],
    ['STACK', 'Stacking group']
]

class Control extends React.Component {
    _onDragStart = (e) => {

        ewars.emit("SHOW_DROPS", "C");
        e.dataTransfer.setData("n", this.props.data[0])
    };

    _onDragEnd = (e) => {
        ewars.emit('HIDE_DROPS');
    };

    render() {
        return (
            <div className="block"
                 onDragEnd={this._onDragEnd}
                 draggable={true}
                 onDragStart={this._onDragStart}>
                <div className="block-content">{this.props.data[1]}</div>
            </div>
        )
    }
}

class ControlList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let controls = [];
        if (TYPES[instance.type]) {
            CONTROLS.forEach(item => {
                if (TYPES[instance.type].indexOf(item[0]) >= 0) controls.push(item);
            })
        } else {
            CONTROLS.forEach(item => {
                if (TYPES.OTHER.indexOf(item[0]) >= 0) controls.push(item);
            })
        }

        return (
            <ewars.d.Panel>
                <div className="block-tree" style={{position: "relative"}}>
                    {controls.map((item) => {
                        return <Control
                            key={item}
                            data={item}/>
                    })}
                </div>
            </ewars.d.Panel>
        )
    }
}

export default ControlList;