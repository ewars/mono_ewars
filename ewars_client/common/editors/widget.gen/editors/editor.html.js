import CodeMirror from "codemirror";

import {instance} from '../widget_instance';


class HTMLEditor extends React.Component {
    static defaultProps = {
        value: ""
    };

    constructor(props) {
        super(props);

        this.state = {
            edit: false,
            html: ""
        }
    }

    componentDidMount() {
        let height = this._el.getBoundingClientRect().height;
        let initialValue = instance.getProp('html', '');
        if (initialValue == undefined) initialValue = '';
        this._editor = CodeMirror(this._el, {
            value: initialValue,
            lineNumbers: true,
            mode: "html"
        })
        this._editor.setSize("100%", height);
        this._editor.on('change', this._onChange)
        if (this._pEl) this._pEl.innerHTML = initialValue;
    };

    _onChange = (value) => {
        if (this._pEl) this._pEl.innerHTML = value.getValue();
        instance.silentUpdateProp('html', value.getValue());
    };

    render() {
        return (
            <ewars.d.Layout>
                <ewars.d.Row height="50%" borderBottom={true}>
                    <ewars.d.Cell>
                        <div
                            ref={(el) => {
                                this._el = el
                            }}
                            style={{height: "100%", width: "100%"}}>


                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Toolbar label="Preview">

                </ewars.d.Toolbar>
                <ewars.d.Row height="50%">
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div
                                ref={(el) => {
                                    this._pEl = el;
                                }}>

                            </div>
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default HTMLEditor;