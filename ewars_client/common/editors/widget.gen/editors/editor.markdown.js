import CodeMirror from "codemirror";

import {instance} from '../widget_instance';


class MarkdownEditor extends React.Component {
    static defaultProps = {
        value: ""
    };

    constructor(props) {
        super(props);

        this.state = {
            edit: false,
            html: ""
        }
    }

    componentDidMount() {
        let height = this._el.getBoundingClientRect().height;
        let initialValue = instance.getProp('md', '');
        if (initialValue == undefined) initialValue = '';
        this._editor = CodeMirror(this._el, {
            value: initialValue,
            lineNumbers: true,
            mode: "markdown"
        })
        this._editor.setSize("100%", height);
        this._editor.on('change', this._onChange)
        if (this._pEl) this._pEl.innerHTML = initialValue;
    };

    _onChange = (value) => {
        instance.silentUpdateProp('md', value.getValue());
    };

    render() {
        return (
            <div
                ref={(el) => {
                    this._el = el
                }}
                style={{height: "100%", width: "100%"}}>


            </div>
        )
    }
}

export default MarkdownEditor;