import ControlField from '../components/ui.control_field';

class EditorAssignments extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="control-editor">
                <ControlField label="Allow request">
                    <select name="" id="">
                        <option value={false}>No</option>
                        <option value={true}>Yes</option>
                    </select>
                </ControlField>

            </div>
        )
    }
}

export default EditorAssignments;