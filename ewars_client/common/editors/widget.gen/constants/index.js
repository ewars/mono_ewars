import ALERT_SETTINGS from './settings.alerts';
import ACTIVITY_SETTINGS from './settings.activity';
import OVERDUE_SETTINGS from './settings.overdue';
import UPCOMING_SETTINGS from './settings.upcoming';
import PERFORMANCE_SETTINGS from './settings.performance';
import IMAGE_SETTINGS from './settings.image';
import TASKS_SETTINGS from './settings.tasks';
import NOTIFICATIONS_SETTINGS from './settings.notifications';
import ASSIGNMENTS_SETTINGS from './settings.assignments';
import DOCUMENTS_SETTINGS from './settings.documents';
import WIKIS_SETTINGS from './settings.wikis';
import LINKS_SETTINGS from './settings.links';

export {
    ALERT_SETTINGS,
    ACTIVITY_SETTINGS,
    OVERDUE_SETTINGS,
    UPCOMING_SETTINGS,
    PERFORMANCE_SETTINGS,
    IMAGE_SETTINGS,
    TASKS_SETTINGS,
    NOTIFICATIONS_SETTINGS,
    ASSIGNMENTS_SETTINGS,
    DOCUMENTS_SETTINGS,
    WIKIS_SETTINGS,
    LINKS_SETTINGS
}