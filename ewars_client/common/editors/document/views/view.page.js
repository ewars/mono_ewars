const STYLE = {
    PAGE: {
        margin: '18px',
        border: '1px solid rgba(0,0,0,0.1)',
        minHeight: '300px'
    }
}

class ViewPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="theme-page" style={STYLE.PAGE}>


            </div>
        )
    }
}

export default ViewPage;