import ViewData from './view.data';
import ViewDroppables from './view.droppables';
import ViewEditor from './view.editor';
import ViewSettings from './view.settings';
import ViewPreview from './view.preview';
import ViewPermissions from './view.permissions';

export {
    ViewData,
    ViewDroppables,
    ViewEditor,
    ViewSettings,
    ViewPreview,
    ViewPermissions
}