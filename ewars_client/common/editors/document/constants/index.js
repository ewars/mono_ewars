import TABS_MAIN from './const.tabs.main';
import TABS_SIDE from './const.tabs.side';

export {
    TABS_MAIN,
    TABS_SIDE
}