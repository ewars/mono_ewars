import {
    SelectField,
    TextField,
    NumericField as NumberField,
    DateField
} from "../../fields";

import FieldSelector from "../form_definition/FieldSelector";
import FormUtils from "../../utils/FormUtils";

const TYPE_MAP = {
    number: NumberField,
    select: SelectField,
    text: TextField,
    date: DateField

};

const options = [
    ["EQ", "Is equal to"],
    ["NEQ", "Is not equal to"],
    ["GT", "Is greater than"],
    ["LT", "Is less than"],
    ["GTE", "Is greater-than or equal to"],
    ["LTE", "Is less-than or equal to"]
];

class NoField extends React.Component {
    render() {
        return (
            <div><p>Please select a field</p></div>
        )
    }
}

class SetItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (prop, value) => {
        window.dispatchEvent(new CustomEvent("etl-set-update-rule", {
            detail: {
                _event: "etl-set-update-rule",
                id: this.props.indicator,
                path: this.props.path,
                prop,
                value
            }
        }));
    };

    _onRemove = () => {
        window.dispatchEvent(new CustomEvent("etl-set-remove-rule", {
            detail: {
                _event: "etl-set-remove-rule",
                id: this.props.indicator,
                path: this.props.path
            }
        }));
    };


    render() {
        let fieldPath = this.props.data.split(":")[0];


        let Cmp, field;
        if (fieldPath != "null" && fieldPath) {
            field = FormUtils.field(this.props.definition, fieldPath);
            if (TYPE_MAP[field.type]) {
                Cmp = TYPE_MAP[field.type];
            } else {
                Cmp = NoField;
            }
        }

        if (!field && fieldPath.length == 3) {
            if (fieldPath[0] != "null") {
                return (
                    <div className="field">
                        <div className="ide-row">
                            <div className="ide-col"><p className="error">Unrecognized field {fieldPath}</p></div>
                            <div className="ide-col icon" style={{maxWidth: 22}} onClick={this._onRemove}>
                                <i className="fal fa-trash"></i>
                            </div>
                        </div>
                    </div>
                )
            }
        }


        let endOptions = ewars.copy(options);
        let value = this.props.data.split(':')[2] || '';
        if (field) {
            if (field.multiple) {
                endOptions.push(['CNT', 'Contains']);
                endOptions.push(['NCNT', 'Does not contain']);

                // create an array from the value
                if (value != '' && value != null && value != 'null') {
                    value = value.split(',');
                } else {
                    value = [];
                }
            }
        }

        endOptions.push(['NULL', 'Null']);


        return (
            <div className="field">
                <div className="ide-row">
                    <div className="ide-col" style={{padding: 0}}>
                        <div className="ide-row">
                            <div className="ide-col">
                                <FieldSelector
                                    name="field"
                                    onUpdate={this._onChange}
                                    definition={this.props.definition}
                                    value={this.props.data.split(":")[0]}/>

                            </div>
                            <div className="ide-col">
                                <SelectField
                                    name="operator"
                                    onUpdate={this._onChange}
                                    value={this.props.data.split(":")[1]}
                                    config={{options: endOptions}}/>
                            </div>
                            <div className="ide-col">
                                {field ?
                                <Cmp
                                    config={field}
                                    name="value"
                                    onUpdate={this._onChange}
                                    value={value}/>
                                    : null}

                            </div>
                        </div>
                    </div>
                    <div className="ide-col icon" style={{maxWidth: 22}} onClick={this._onRemove}>
                        <i className="fal fa-trash"/>
                    </div>

                </div>
            </div>
        )
    }
}

export default SetItem;
