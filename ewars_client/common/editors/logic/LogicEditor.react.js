import {
    SelectField,
    NumericField as NumberField,
    DateField,
    TextField,
    TextAreaField
} from "../../fields";

import Spinner from "../../c.spinner";
import Button from "../../c.button";

import Field from "./Field";
import SetItem from "./SetItem";
import Set from "./Set";
import Complex from "./Complex";
import FormNode from "./FormNode";
import Indicator from "./Indicator";

import FormUtils from "../../utils/FormUtils";

import utils from "./utils";

function getOptionsData(definition) {

}

class SetAdder extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDragStart = (e) => {
        ewars.emit("SHOW_DROPS");
        e.dataTransfer.setData("d", "{set}");
    };

    _dragEnd = () => {
        ewars.emit("HIDE_DROPS");
    };


    render() {
        return (
            <div className="block" draggable={true} onDragStart={this._onDragStart} onDragEnd={this._dragEnd}>
                <div className="block-content status-green">Complex Set</div>
            </div>
        )
    }
}

let ITEM_COUNT = 0;

const events = [
    // Set events
    "etl-add-field",
    "etl-add-set",
    "etl-remove_set",
    "etl-change-rule",

    // Root-level events
    "ind-rule-change",
    "ind-swap",
    "remove-ind",
    "dupe-ind",
    "ind-move-up",
    "ind-move-down",

    "ind-remove-field",
    "ind-update-field",
    "ind-remove-complex",

    "etl-set-update-rule",
    "etl-set-remove-rule",
    "etl-set-change-provision"
]

class LogicEditor extends React.Component {
    _isLoaded = true;
    _itemsLoaded = 0;
    _totalItems = 0;

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        events.forEach(event => {
            window.addEventListener(event, this._onAction);
        });
    }

    componentWillUnmount() {
        events.forEach(event => {
            window.removeEventListener(event, this._onAction);
        });
    }

    componentWillUnmount() {
        this._isLoaded = false;
        this.state.indicators = null;
    }

    _onAction = (e) => {
        let data = e.detail;
        let _event = data._event || null;
        let etl = ewars.copy(this.props.form.version.etl || {});

        this.props.onChange(utils.handleEvent(_event, etl, data));
    };

    _add = () => {
        this.props.onChange(utils.addRootIndicator(this.props.form.version.etl || {}));
    };


    render() {
        let definition = ewars.copy(this.props.form.version.definition);

        let indications = [];
        if (this._isLoaded) {
            let asArray = utils.logicToArray(this.props.form.version.etl);

            indications = asArray.map((item, index) => {
                return (
                    <Indicator
                        key={item.key}
                        indicator={item.i}
                        index={index}
                        indicators={this.state.indicators}
                        definition={this.props.form.version.definition}
                        etl={this.props.form.version.etl}
                        data={item}/>
                )
            });

        } else {
            indications = <Spinner/>;
        }

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col border-right" style={{maxWidth: 300}}>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                <FormNode
                                    data={definition}
                                    path={null}
                                    root={true}/>
                                <SetAdder/>
                            </div>
                        </div>
                    </div>
                    <div className="ide-col">
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 35}}>
                                <div className="ide-col">
                                    <div className="ide-tbar">
                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                onClick={this._add}
                                                icon="fa-plus"
                                                label="Add Indicator"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="logical">
                                            {indications}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default LogicEditor;
