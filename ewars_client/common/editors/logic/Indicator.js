import {
    SelectField,
    NumericField as NumberField,
    DateField,
    TextField,
    TextAreaField,
    IndicatorField as IndicatorSelector
} from "../../fields";

import Complex from "./Complex";
import Field from "./Field";

const REDUCTION_OPTIONS = {
    options: [
        ["SUM", _l("SUM")],
        ["AVG", _l("MEDIAN")],
        ["MIN", _l("MIN_VALUE")],
        ["MAX", _l("MAX_VALUE")]
    ]
};

class Indicator extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            indicator: {name: "Loading..."}
        }
    }

    componentWillMount() {
        ewars.subscribe("SHOW_DROPS", () => this._action("SHOW_DROPS"), this.props.indicator);
        ewars.subscribe("HIDE_DROPS", () => this._action("HIDE_DROPS"), this.props.indicator);

        if (this.props.indicator.indexOf("NEW") < 0) {
            ewars.tx("com.ewars.resource", ["indicator", this.props.indicator, ['uuid', 'name'], null])
                .then(function (resp) {
                    this.setState({
                        indicator: resp
                    })
                }.bind(this))
        } else {
            this.state.indicator.name = "No Indicator Selected";
        }
    }

    componentWillUnmount() {
        ewars.unsubscribe("SHOW_DROPS", this.props.indicator);
        ewars.unsubscribe("HIDE_DROPS", this.props.indicator);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.indicator != nextProps.indicator) {
            if (nextProps.indicator.indexOf("NEW") < 0) {
                ewars.tx("com.ewars.resource", ["indicator", nextProps.indicator, ['uuid', 'name'], null])
                    .then(function (resp) {
                        this.setState({indicator: resp})
                    }.bind(this))
            } else {
                this.state.indicator = {
                    name: "No indicator selected"
                }
            }
        }
    }

    _action = (action) => {
        if (this.state.show && this._dropper) {
            if (action == "SHOW_DROPS") this._dropper.style.display = "block";
            if (action == "HIDE_DROPS") this._dropper.style.display = "none";
        }
    };

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    _onDrop = (e) => {
        e.preventDefault();
        let fieldName = e.dataTransfer.getData("d");
        let logic = ewars.copy(this.props.data);

        if (fieldName != "{set}") {
            logic.n.push(fieldName);
            this._emit("ind-rule-change", {
                id: this.props.indicator,
                data: logic
            });
        } else {
            logic.n.push([
                "ALL"
            ]);
            this._emit("ind-rule-change", {
                id: this.props.indicator,
                data: logic
            });
        }

        ewars.emit("HIDE_DROPS");
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onReduceChange = (prop, value) => {
        let data = this.props.data || {};
        data.r = value;
        this._emit("ind-rule-change", {
            id: this.props.indicator,
            data: data
        });
    };

    _onIndChange = (prop, value) => {
        this._emit("ind-swap", {
            id: this.props.indicator,
            new: value
        });
    };

    _onRemove = (index) => {
        this._emit("remove-ind", {
            id: this.props.indicator
        });
    };

    _dupe = (index) => {
        this._emit("dupe-ind", {
            id: this.props.indicator
        });
    };

    _onRemoveItem = (index) => {
        let logic = ewars.copy(this.props.data);
        logic.n.splice(index, 1);
        this._emit("ind-rule-change", {
            id: this.props.indicator,
            data: logic
        });
    };

    _onComplexChange = (index, data) => {
        let logic = ewars.copy(this.props.data);
        logic.n[index] = data;
        this._emit("ind-rule-change", {
            id: this.props.indicator,
            data: logic
        });
    };

    _moveUp = () => {
        this._emit("ind-move-up", {
            id: this.props.indicator
        });
    };

    _moveDown = () => {
        this._emit("ind-move-down", {
            id: this.props.indicator
        });
    };

    _emit = (event, data) => {
        window.dispatchEvent(new CustomEvent(event, {
            detail: {
                ...data,
                _event: event
            }
        }));
    };

    render() {

        let items;
        if (this.state.show) {
            items = this.props.data.n.map(function (item, index) {
                if (typeof item == "string") {
                    return <Field data={item}
                                  indicator={this.props.indicator}
                                  onRemove={this._onRemoveItem}
                                  index={index}
                                  definition={this.props.definition}/>;
                } else {
                    return <Complex data={item}
                                    onRemove={this._onRemove}
                                    index={index}
                                    indicator={this.props.indicator}
                                    definition={this.props.definition}
                                    onChange={this._onComplexChange}/>;
                }
            }.bind(this));
        }

        let showReduction = false;
        if (this.props.data.n.length > 1) showReduction = true;

        let dropIcon = "fal fa-plus-square";
        if (this.state.show) dropIcon = "fal fa-minus-square";

        let indId = this.props.indicator != "NEW" ? this.props.indicator : null;

        let indName = ewars.I18N(this.state.indicator.name);

        let hStyle = {};
        if (this.props.indicator.indexOf("NEW") >= 0) hStyle = {background: "darkred"};

        return (
            <div className="indicator">
                <div className="header" style={hStyle}>
                    <div className="ide-row">
                        <div className="ide-col border-right">{indName}</div>

                        {this.props.index != 0 ?
                            <div className="ide-col border-right action" style={{maxWidth: 30}} onClick={this._moveUp}>
                                <i className="fal fa-chevron-up"></i>
                            </div>
                            : null}
                            <div className="ide-col border-right action"
                                style={{maxWidth: 30}}
                                onClick={() => this._emit("ind-move-down", {id: this.props.indicator})}>
                            <i className="fal fa-chevron-down"></i>
                        </div>
                        <div className="ide-col border-right action" style={{maxWidth: "30px"}} onClick={this._dupe}>
                            <i className="fal fa-copy"></i>
                        </div>

                        <div className="ide-col border-right action" style={{maxWidth: 30}} onClick={this._toggle}>
                            <i className={dropIcon}></i>
                        </div>
                        <div className="ide-col" style={{maxWidth: 30}}
                             onClick={this._onRemove}><i className="fal fa-trash"></i>
                        </div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="content">
                        <div className="dropper" ref={(el) => this._dropper = el} style={{display: "none"}} onDrop={this._onDrop}
                             onDragOver={this._onDragOver}>
                            <div className="inner">
                                <div className="text">DROP HERE</div>
                            </div>
                        </div>
                        <div className="nugget-up"></div>
                        <div className="hd">
                            <div className="ide-row">
                                {showReduction ?
                                    <div className="ide-col" style={{maxWidth: 200, marginRight: 10}}>
                                        <SelectField
                                            name="reduction"
                                            onUpdate={this._onReduceChange}
                                            value={this.props.data.r}
                                            config={REDUCTION_OPTIONS}/>
                                    </div>
                                    : null}
                                <div className="ide-col">
                                    <IndicatorSelector
                                        onUpdate={this._onIndChange}
                                        name="indicator"
                                        value={indId}/>
                                </div>
                            </div>
                            <div className="control">
                            </div>
                        </div>
                        {items}
                        <div className="nugget-end"></div>
                    </div>
                    : null}
            </div>
        )
    }

}

export default Indicator;
