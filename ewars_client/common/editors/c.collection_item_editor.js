class CollectionItemEditor extends React.Component {
    static defaultProps = {
        form: null
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {data: {}}
        }

    }

    componentWillUnmount() {

    }

    _onChange = (data, prop, value) => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                data: {
                    ...this.state.data.data,
                    [prop]: value
                }
            }
        })
    };

    render() {
        console.log(this.props);
        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                label="Submit"/>
                            <ewars.d.Button
                                label="Amend"/>
                            <ewars.d.Button
                                label="Retract"/>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>

                            <div className="report-header">
                                <div className="report-title"></div>
                            </div>

                            <div className="report-info">
                                <div className="small">Fields with an * are required</div>
                            </div>

                            <div style={{paddingLeft: "25px", paddingRight: "25px"}}>

                                <ewars.d.Form
                                    definition={this.props.form.definition}
                                    data={this.state.data.data}
                                    readOnly={false}
                                    errors={this.state.errors}
                                    updateAction={this._onChange}/>

                            </div>

                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default CollectionItemEditor;

