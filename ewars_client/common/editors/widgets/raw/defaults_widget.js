export default {
    spec: 2,
    type: "RAW",
    title: {en: "New Widget"},
    additionalClass: null,
    source_type: "SLICE",
    series: [],
    format: "0",
    reduction: "SUM",
    interval: "DAY",
    loc_spec: "SPECIFIC"
};
