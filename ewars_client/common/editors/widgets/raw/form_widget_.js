const modeConditions = {
    application: "all",
    rules: [
        ["mode", "eq", "DASHBOARD"]
    ]
};

let FORM = [
    {
        l: "General",
        t: "section",
        c: [
            {
                t: "language_string",
                l: "Title",
                p: "title"
            },
            {
                t: "select",
                l: "Aggregation Interval",
                p: "interval",
                o: [
                    ["DAY", "Daily"],
                    ["WEEK", "Weekly"],
                    ["MONTH", "Monthly"],
                    ["YEAR", "Annually"]
                ]
            }
        ]
    },
    {
        l: "Location",
        t: "section",
        c: [
            {
                t: "location",
                l: "Location",
                p: "location"
            }
        ]
    },
    {
        l: "Data Source",
        t: "section",
        c: [
            {
                t: "indicator",
                l: "Indicator",
                p: "indicator"
            },
            {
                t: "date_range",
                l: "Date Range",
                p: "range"
            },
            {
                t: "select",
                l: "Reduction",
                p: "reduction",
                o: [
                    ["SUM", "Sum"]
                ]
            }
        ]
    },
    {
        l: "Style",
        t: "section",
        c: [
            {
                t: "select",
                l: "Formatting",
                o: [
                    ["0", "0"]
                ]
            }
        ]
    }
];

export default FORM;
