import Button from "../../../c.button";
import DEFAULT_SERIES from "./defaults_series";
import FORM_SERIES from "./form_series";
import Form from "../../../c.form";

if (!ewars.copy) ewars.copy = function (item) {
    return JSON.parse(JSON.stringify(item));
};

var Column = React.createClass({
    getInitialState: function () {
        return {
            isExpanded: false,
            errors: []
        }
    },

    _moveUp: function () {
        this.props.onUp(this.props.data, this.props.index);
    },

    _moveDown: function () {
        this.props.onDown(this.props.data, this.props.index);
    },

    _edit: function () {
        this.setState({
            isExpanded: this.state.isExpanded ? false : true
        })
    },

    _delete: function () {
        this.props.delete(this.props.data, this.props.index);
    },

    _update: function (data, prop, value) {
        this.props.onChange(this.props.index, prop, value);
    },

    _dupe: function () {
        this.props.dupe(this.props.data, this.props.index);
    },

    render: function () {
        var colTitle = ewars.I18N(this.props.data.title);

        var editIcon = "fal fa-pencil";
        if (this.state.isExpanded) editIcon = "fal fa-times";

        if (window.user.role != "SUPER_ADMIN") {
            FORM_SERIES.generator_locations_parent.parentId = window.user.clid;
            FORM_SERIES.location.parentId = window.user.clid;
        }

        if (ewars.TEMPLATE_MODE == "TEMPLATE") {
            FORM_SERIES.loc_spec = {
                type: "button_group",
                label: "Location source",
                options: [
                    ["SPECIFIC", "Specific Location"],
                    ["GENERATOR", "Generator"],
                    ["REPORT_LOCATION", "Report Location"],
                    ["GROUP", "Location Group(s)"]
                ]
            }
        }

        return (
            <div className="data-item-wrapper">
                <div className="series-list-item">
                    <div className="name">{colTitle}</div>
                    <div className="control-wrapper" style={{maxWidth: "200px"}}>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-caret-up"
                                onClick={this._moveUp}/>
                            <ewars.d.Button
                                icon="fa-caret-down"
                                onClick={this._moveDown}/>
                        </div>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon={editIcon}
                                onClick={this._edit}/>
                            <ewars.d.Button
                                icon="fa-copy"
                                onClick={this._dupe}/>
                            <ewars.d.Button
                                icon="fa-trash"
                                colour="red"
                                onClick={this._delete}/>
                        </div>
                    </div>
                </div>
                {this.state.isExpanded ?
                    <div className="data-item-settings">
                        <div className="ide-settings-content">
                            <div className="ide-settings-form">
                                <div className="chart-edit">
                                    <Form
                                        definition={FORM_SERIES}
                                        horizontal={true}
                                        data={this.props.data}
                                        updateAction={this._update}
                                        readOnly={false}
                                        errors={this.state.errors}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
});

var DataEditor = React.createClass({
    getInitialState: function () {
        return {}
    },

    _addSeries: function () {
        var cols = ewars.copy(this.props.data.series || []);
        cols.push({...{}, ...DEFAULT_SERIES});
        this.props.onChange("series", cols);
    },

    _onChange: function (index, prop, value) {
        var cols = ewars.copy(this.props.data.series || []);
        cols[index][prop] = value;
        this.props.onChange("series", cols);
    },

    _onUp: function (col, index) {
        if (index <= 0) return;

        var targetIndex = index - 1;
        var cols = ewars.copy(this.props.data.series || []);
        var tmp = cols[targetIndex];
        cols[targetIndex] = col;
        cols[index] = tmp;
        this.props.onChange("series", cols);
    },

    _onDown: function (col, index) {
        if (index >= this.props.data.series.length - 1) return;

        var targetIndex = index + 1;
        var cols = ewars.copy(this.props.data.series || []);
        var tmp = cols[targetIndex];
        cols[targetIndex] = col;
        cols[index] = tmp;
        this.props.onChange("series", cols);
    },

    _onDelete: function (data, index) {
        var series = ewars.copy(this.props.data.series || []);
        series.splice(index, 1);
        this.props.onChange("series", series);
    },

    _duplicate: function (data, index) {
        let series = ewars.copy(this.props.data.series || []);
        series.push(ewars.copy(this.props.data.series[index]));
        this.props.onChange("series", series);
    },

    render: function () {
        let cols;

        if (this.props.data.series) {
            cols = this.props.data.series.map((col, index) => {
                return <Column
                    onChange={this._onChange}
                    data={col}
                    dupe={this._duplicate}
                    onUp={this._onUp}
                    onDown={this._onDown}
                    delete={this._onDelete}
                    index={index}/>;
            });
        }

        return (
            <div className="data-manager" id="dm">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div className="ide-tbar">
                                <div className="ide-tbar-text">Data</div>

                                <div className="btn-group pull-right">
                                    <ewars.d.Button
                                        icon="fa-plus"
                                        onClick={this._addSeries}
                                        label="Add Series"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="series-list">
                                    {cols}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default DataEditor;
