const modeConditions = {
    application: "all",
    rules: [
        ["mode", "eq", "DASHBOARD"]
    ]
};

export default {
    t_header: {
        type: "header",
        label: "General"
    },
    dataType: {
        type: "hidden"
    },

    title: {
        type: "language_string",
        label: {
            en: "Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    loc_spec_header: {
        type: "header",
        label: {
            en: "Locations Source"
        }
    },
    loc_source: {
        type: "button_group",
        label: "Location(s)",
        options: [
            ["SPECIFIC", "Specific"],
            ["TYPE", "Of Type"],
            ["GROUP", "Location Group(s)"]
        ]
    },
    group_ids: {
        type: "location_group",
        label: "Location Group(s)",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_source", "eq", "GROUP"]
            ]
        }
    },
    location: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        },
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ["loc_source", "eq", "SPECIFIC"],
                ["loc_source", "eq", "TYPE"]
            ]
        }
    },
    site_type_id: {
        type: "select",
        label: "Location Type",
        required: true,
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_source", "eq", "TYPE"]
            ]
        }
    },
    location_status: {
        type: "select",
        label: "Location Status",
        required: true,
        options: [
            ["ACTIVE", "Active"],
            ["DISABLED", "Inactive"],
            ["ANY", "Any"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_source", "eq", "TYPE"]
            ]
        }
    },
    d_header: {
        type: "header",
        label: {
            en: "Data Source"
        }
    },
    source_type: {
        type: "button_group",
        label: "Query Type",
        required: true,
        options: [
            ["SLICE", "Indicator"],
            ["SLICE_COMPLEX", "Complex"]
        ]
    },
    formula: {
        type: "text",
        label: {
            en: "Formula"
        },
        required: true,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {en: "Variables"},
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    indicator: {
        type: "indicator",
        label: "Indicator",
        required: true,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    interval: {
        type: "button_group",
        label: "Formula Aggregation Interval",
        options: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Year"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    reduction: {
        type: "button_group",
        label: {
            en: "Reduction"
        },
        required: true,
        options: [
            ["SUM", "Sum"],
            ["AVG", "Avg"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    period: {
        type: "date_range",
        label: "Period"
    },
    t_thresholds: {
        type: "header",
        label: "Thresholds"
    },
    thresholds: {
        type: "colour_thresholds",
        label: "Thresholds"
    },
    style_header: {
        type: "header",
        label: {
            en: "Styling"
        }
    },
    legend: {
        type: "button_group",
        label: "Legend",
        options: [
            ["SHOW", "Show"],
            ["HIDE", "Hide"]
        ]
    },
    chloro_opacity: {
        type: "number",
        decimal_allowed: true,
        label: "Opacity"
    },
    base_colour: {
        type: "color",
        label: "Base Geometry Colour"
    },
    bgd_colour: {
        type: "color",
        label: "Background Colour"
    },
    s_stroke: {
        type: "color",
        label: "Stroke Colour"
    },
    s_stroke_width: {
        type: "number",
        label: "Stroke Width"
    },

    width: {
        type: "dom_size",
        label: "Width"
    },
    height: {
        type: "dom_size",
        label: "Height"
    },
    t_labels: {
        type: "header",
        label: "Label Styling"
    },
    b_labels: {
        type: "button_group",
        label: "Show labels",
        options: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    lbl_style: {
        type: "select",
        label: "Labelling style",
        options: [
            ["DEFAULT", "Default"],
            ["NUMBERED", "Numbered"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["b_labels", "eq", true]
            ]
        }
    },
    label_font_size: {
        type: "text",
        label: "Label Font Size (px)",
        help: "Labels are scaled based on the overall map scale, use em scaling to rescale labels",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["b_labels", "eq", true]
            ]
        }
    },
    label_colour: {
        type: "color",
        label: "Label Colour",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["b_labels", "eq", true]
            ]
        }
    },
    label_threshold: {
        type: "number",
        label: "Label Threshold (gte)",
        help: "Threshold above which labels will be displayed",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["b_labels", "eq", true]
            ]
        }
    },
    t_widget: {
        type: "header",
        label: "Widget Settings",
        conditional_bool: true,
        conditions: modeConditions
    },
    headerColor: {
        type: "text",
        label: "Header Colour",
        conditional_bool: true,
        conditions: modeConditions
    },
    headerTextColor: {
        type: "text",
        label: "Header Text Colour",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetBackgroundColor: {
        type: "text",
        label: "Widget Background Colour",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetTextColor: {
        type: "text",
        label: "Widget Text Colour",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetSubText: {
        type: "text",
        label: "Widget Sub-Text",
        conditional_bool: true,
        conditions: modeConditions
    },
    widgetFooterText: {
        type: "text",
        label: "Widget Footer Text",
        conditional_bool: true,
        conditions: modeConditions
    }
}