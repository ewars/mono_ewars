export default {
    title: {
        en: "New Category"
    },
    colour: null,
    start_date: new Date(),
    end_date: new Date(),
    indicator: null,
    location: null,
    reduction: "SUM",
    source_type: "SLICE",
    interval: "NONE",
    loc_spec: "SPECIFIC",
    override: "INHERIT"

};
