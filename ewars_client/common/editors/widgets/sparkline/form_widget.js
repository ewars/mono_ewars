const modeConditions = {
    application: "all",
    rules: [
        ["mode", "eq", "DASHBOARD"]
    ]
};

export default {
    dataType: {
        type: "hidden"
    },

    title: {
        type: "language_string",
        label: {
            en: "Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    loc_spec_header: {
        type: "header",
        label: {
            en: "Location Specification"
        }
    },
    loc_spec: {
        type: "select",
        label: {
            en: "Location Spec"
        },
        options: [
            ["SPECIFIC", "Specific Location"],
            ["REPORT_LOCATION", "Report Location"]
        ]
    },
    location: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        },
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "SPECIFIC"]
            ]
        }
    },
    data_source_header: {
        type: "header",
        label: {en: "Data Source"}
    },
    source_type: {
        type: "select",
        label: {
            en: "Source Type"
        },
        options: [
            ["SLICE", "Indicator"],
            ["SLICE_COMPLEX", "Complex"]
        ]
    },
    formula: {
        type: "text",
        label: {
            en: "Formula"
        },
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {en: "Variables"},
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE_COMPLEX"]
            ]
        }
    },
    indicator: {
        type: "indicator",
        label: {
            en: "Indicator"
        },
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    reduction: {
        type: "select",
        label: {
            en: "Reduction"
        },
        options: [
            ["NONE", "None"],
            ["SUM", "Sum"]
        ],
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SLICE"]
            ]
        }
    },
    start_date_header: {
        type: "header",
        label: "Data Query Period"
    },
    interval: {
        type: "select",
        label: "Time Interval",
        options: [
            ["DAY", "Day"],
            ["WEEK", "Week"],
            ["MONTH", "Month"],
            ["YEAR", "Year"]
        ]
    },
    start_date_spec: {
        type: "select",
        label: {
            en: "Start Date Specification"
        },
        options: [
            ["MANUAL", "Manual"],
            ["END_SUBTRACTION", "Interval(s) back from End Date"],
            ["REPORT_DATE", "Report Date"],
            ["CURRENT", "Current"]
        ]
    },
    start_date: {
        type: "date",
        label: {
            en: "Start Date"
        },
        help: {
            en: "The start date of the series"
        },
        conditions: {
            application: "all",
            rules: [
                ["start_date_spec", "eq", "MANUAL"]
            ]
        }
    },
    start_intervals: {
        type: "number",
        label: {
            en: "Intervals to Subtract from End Date"
        },
        help: {
            en: "The amount of the chart interval back that will be subtracted from the Start Date"
        },
        conditions: {
            application: "all",
            rules: [
                ["start_date_spec", "eq", "END_SUBTRACTION"]
            ]
        }
    },
    end_date_spec: {
        type: "select",
        label: {
            en: "End Date Specification"
        },
        options: [
            ["MANUAL", "Manual"],
            ["REPORT_DATE", "Report Date"],
            ["REPORT_DATE_SUBTRACTION", "Interval Back from Report Date"],
            ["CURRENT", "Current"]
        ]
    },
    end_intervals: {
        type: "number",
        label: {
            en: "Intervals to Subtract from Report Date"
        },
        conditions: {
            application: "all",
            rules: [
                ["end_date_spec", "eq", "REPORT_DATE_SUBTRACTION"]
            ]
        }
    },
    end_date: {
        type: "date",
        label: {
            en: "End Date"
        },
        help: {
            en: "The end date of the series"
        },
        conditions: {
            application: "all",
            rules: [
                ["end_date_spec", "eq", "MANUAL"]
            ]
        }
    },
    style_header: {
        type: "header",
        label: {
            en: "Styling"
        }
    },
    format: {
        type: "select",
        label: {
            en: "Number Formatting"
        },
        options: [
            ["0", "0"],
            ["0.0", "0.0"],
            ["0.00", "0.00"],
            ["0,0.0", "0,0.0"],
            ["0,0.00", "0,0.00"],
            ["0,0", "0,0"],
            ["0%", "0%"],
            ["0.0%", "0.0%"],
            ["0.00%", "0.00%"]

        ]
    },
    t_widget: {
        type: "header",
        label: "Widget settings",
        conditions: modeConditions
    },
    widgetBaseColor: {
        type: "text",
        label: "Base colour",
        conditions: modeConditions
    },
    widgetHighlightColor: {
        type: "text",
        label: "Highlight colour",
        conditions: modeConditions
    },
    widgetIcon: {
        type: "text",
        label: "Icon",
        conditions: modeConditions
    },
    widgetSubText: {
        type: "text",
        label: "Widget sub-text",
        conditions: modeConditions
    },
    widgetFooterText: {
        type: "text",
        label: "Widget footer text",
        conditions: modeConditions
    }
};
