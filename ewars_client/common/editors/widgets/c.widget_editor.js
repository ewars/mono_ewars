import KeyMirror from "keymirror";
import { Layout, Row, Cell } from "../../layout";
import Toolbar from "../../c.toolbar";

import Button from "../../c.button";
import DefinitionUtils from "./DefinitionUtils";

// Editors
import TableWidgetEditor from "./table/Editor.react";
import RawWidgetEditor from "./raw/Editor.react";
import SeriesWidgetEditor from "./series/Editor.react";
import CategoryWidgetEditor from "./category/Editor.react";
import OtherEditor from "./other/Editor.react";
import MapEditor from "./map/Editor.react";

import GaugeEditor from "./gauge/Editor.react";
import MappingEditor from "./mapping/v.editor";


// Defaults
import DEFAULTS_TABLE from "./table/defaults_widget";
import DEFAULTS_RAW from "./raw/defaults_widget";
import DEFAULTS_SERIES from "./series/defaults_widget";
import DEFAULTS_PIE from "./category/defaults_widget";
import {default as DEFAULTS_MAP} from "./map/defaults_widget";

import DEFAULTS_GAUGE from "./gauge/defaults_widget";
import DEFAULTS_MAPPING from './mapping/defaults_widget';

const TYPES = {
    SERIES: "SERIES",
    TABLE: "TABLE",
    MAP: "MAP",
    RAW: "RAW",
    OTHER: "OTHER",
    CATEGORY: "CATEGORY",
    PIE: "CATEGORY",
    GAUGE: "GAUGE",
    MAPPING: "MAPPING"
};

var EDITORS = {
    TABLE: TableWidgetEditor,
    RAW: RawWidgetEditor,
    SERIES: SeriesWidgetEditor,
    CATEGORY: CategoryWidgetEditor,
    MAP: MapEditor,
    OTHER: OtherEditor,
    GAUGE: GaugeEditor,
    PIE: CategoryWidgetEditor,
    MAPPING: MappingEditor
};

var DEFAULTS = {
    TABLE: DEFAULTS_TABLE,
    RAW: DEFAULTS_RAW,
    SERIES: DEFAULTS_SERIES,
    CATEGORY: DEFAULTS_PIE,
    PIE: DEFAULTS_PIE,
    MAP: DEFAULTS_MAP,
    GAUGE: DEFAULTS_GAUGE,
    OTHER: {
        type: "OTHER"
    },
    MAPPING: DEFAULTS_MAPPING
};

const OTHER_OPTIONS = [
    'ACTIVITY',
    "TEXT",
    "ALERTS",
    "METRICS_OVERALL",
    "DOCUMENTS",
    "ASSIGNMENTS",
    "METRICS",
    "OVERDUE",
    "UPCOMING",
    "IMAGE",
    "NOTIFICATIONS",
    "DELETIONS",
    "AMENDMENTS",
    "TASKS",
    "PERIOD",
    "ALERTS",
    'SUMMARY_STATS',
    'DELETIONS',
    'TASKS'
];

let TYPE_BUTTONS = [
    ["SERIES", "fa-chart-bar"],
    ["CATEGORY", "fa-chart-pie"],
    ["MAP", "fa-map"],
    ["TABLE", "fa-table"],
    ["RAW", "fa-tag"]
];


var WidgetEditor = React.createClass({
    _editor: null,

    getDefaultProps: function () {
        return {
            visible: false,
            showPreview: true,
            mode: "DEFAULT"
        }
    },

    getInitialState: function () {
        return {
            widget: null
        };
    },

    componentWillMount: function () {

        var def = ewars.copy(this.props.data || {type: "SERIES"});

        if (def.type) {
            this.state.widget = Object.assign({}, DEFAULTS[def.type], def);
            if (this.state.widget.type == "PIE") this.state.widget.type = "CATEGORY";
            if (this.state.widget.type == "TABLE") {
                (this.state.widget.columns || []).forEach((col) => {
                    if (!col.uuid) col.uuid = ewars.utils.uuid();
                })
            }
        }
    },

    componentWillReceiveProps: function (nextProps) {
        var def = ewars.copy(nextProps.data || {});

        if (def.type != this.state.widget.type) {
            this.state.widget = Object.assign({}, DEFAULTS[def.type], def);

            if (this.state.widget.type == "TABLE") {
                (this.state.widget.columns || []).forEach((col) => {
                    if (!col.uuid) col.uuid = ewars.utils.uuid();
                })
            }
        }
    },

    _onSave: function () {
        this.props.onSave(this.props.rowIndex, this.props.colIndex, this.state.widget);
    },

    _getEditor: function () {

    },

    _onChange: function (prop, value) {
        this.setState({
            widget: {
                ...this.state.widget,
                [prop]: value
            }
        });
    },

    _onJSONSave: function (data) {
        this.setState({
            widget: data
        })
    },

    _changeType: function (data) {
        let newItem = ewars.copy(DEFAULTS[data.type]);
        newItem.type = data.type;
        this.setState({
            widget: newItem
        })
    },

    componentDidMount: function () {
    },

    render: function () {
        // Temp fix for improper naming
        let type = this.props.data.type;
        if (type == "PIE") type = "CATEGORY";

        var divClass = "chart-editor";
        if (!this.props.visible) {
            divClass += " is-hidden";
        }

        var editor;
        if (this.props.data) {
            let Editor;
            if (EDITORS[this.state.widget.type]) {
                Editor = EDITORS[this.state.widget.type];
            } else {
                Editor = EDITORS.OTHER;
            }

            if (!Editor) {
                this._editor = null;
            } else {
                this._editor = <Editor
                    ref="editor"
                    onJSONSave={this._onJSONSave}
                    mode={this.props.mode}
                    definition={this.props.definition || null}
                    onChange={this._onChange}
                    data={this.state.widget}/>;
            }
        }

        let isOther = false;

        let button_types = ewars.copy(TYPE_BUTTONS);
        if (this.props.mode == "DASHBOARD") {
            button_types.push(["OTHER", "fa-ellipsis-h"]);
        }

        button_types.push(["MAPPING", 'fa-map-marker']);

        let overall_type = this.state.widget.type;
        if (OTHER_OPTIONS.indexOf(overall_type) >= 0) overall_type = 'OTHER';

        return (
            <Layout>
                <Toolbar label="Widget Editor">
                    <div className="btn-group">
                        {button_types.map(btnType => {
                            return (
                                <ewars.d.Button
                                    key={"BTN_" + btnType[0]}
                                    icon={btnType[1]}
                                    active={overall_type == btnType[0]}
                                    onClick={this._changeType}
                                    data={{type: btnType[0]}}/>
                            )
                        })}
                    </div>

                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            label="Save Change(s)"
                            onClick={this._onSave}
                            icon="fa-save"/>
                        <ewars.d.Button
                            label="Cancel"
                            onClick={this.props.onCancel}
                            icon="fa-times"/>
                    </div>

                </Toolbar>
                <Row>
                    <Cell style={{display: "flex"}}>
                        {this._editor}
                    </Cell>
                </Row>
            </Layout>
        )
    }
});

export default WidgetEditor;
