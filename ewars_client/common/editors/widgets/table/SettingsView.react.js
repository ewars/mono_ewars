import Form from "../../../c.form";
import DEFAULTS from "./defaults_widget";
import formWidget from "./form_widget";

var SettingsView = React.createClass({
    getInitialState: function () {
        return {
            errors: []
        }
    },

    _update: function (data, prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {

        if (ewars.TEMPLATE_MODE == "TEMPLATE") {
            formWidget.loc_spec.options = [
                ["SPECIFIC", "Specific Location"],
                ["REPORT_LOCATION", "Report Location"],
                ["GENERATOR", "Generator"],
                ["GROUP", "Location Group(s)"]
            ]
        }

        formWidget.t_sort_column.options = (() => {
            let items = [
                ["LOC_ASC", "Location Name Asc."],
                ["LOC_DESC", "Location Name Desc."]
            ];

            (this.props.data.columns || []).forEach((item) => {
                items.push([`${item.uuid}.ASC`, __(item.title) + " Asc."]);
                items.push([`${item.uuid}.DESC`, __(item.title) + " Desc."]);
            })

            return items;
        })()

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll ide-panel-padded">
                <div className="ide-settings-content">
                    <div className="ide-settings-form">
                        <br />
                        <div className="chart-edit">
                            <Form
                                definition={formWidget}
                                data={this.props.data}
                                updateAction={this._update}
                                readOnly={false}
                                errors={this.state.errors}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default SettingsView;
