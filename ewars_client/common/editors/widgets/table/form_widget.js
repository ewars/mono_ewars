export default {
    dataType: {
        type: "hidden"
    },

    title: {
        type: "language_string",
        label: {
            en: "Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    loc_spec_header: {
        type: "header",
        label: {
            en: "Location Specification"
        }
    },
    loc_spec: {
        type: "button_group",
        label: {
            en: "Location Spec"
        },
        options: [
            ["SPECIFIC", "Specific Location"],
            ["GENERATOR", "Generator"],
            ["GROUP", "Location Group(s)"]
        ]
    },
    group_ids: {
        type: "location_group",
        label: "Location Group(s)",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GROUP"]
            ]
        }
    },
    b_parent_col: {
        type: "button_group",
        label: "Show parent column?",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_parent_source: {
        type: "button_group",
        label: "Parent Location",
        options: [
            ["INHERIT", "Inherit"],
            ["CUSTOM", "Custom"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_locations_parent: {
        type: "location",
        label: {
            en: "Generator parent location"
        },
        hideInactive: true,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"],
                ["generator_parent_source", "eq", "CUSTOM"]
            ]
        }
    },
    generator_locations_type: {
        type: "select",
        label: {
            en: "Generator location type"
        },
        optionsSource: {
            resource: "location_type",
            labelSource: "name",
            valSource: "id",
            query: {}
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_location_status: {
        type: "select",
        label: {
            en: "Location Status"
        },
        options: [
            ["ANY", "Any"],
            ["ACTIVE", "Active"],
            ["INACTIVE", "Inactive"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    location: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "SPECIFIC"]
            ]
        }
    },
    style_header: {
        type: "header",
        label: {
            en: "Style"
        }
    },
    show_column_header: {
        type: "button_group",
        label: {
            en: "Column Headers"
        },
        options: [
            [true, "Show"],
            [false, "Hide"]
        ]
    },
    additionalClass: {
        type: "text",
        label: {
            en: "Additional ClassName"
        },
        help: {
            en: "Specify a CSS class name to set on the main element in order to apply custom styling"
        }
    },
    t_advanced: {
        type: "header",
        label: "Advanced"
    },
    hide_null_rows: {
        type: "button_group",
        label: "Hide rows with 0 or no data",
        options: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    t_sort_column: {
        type: "select",
        label: "Sorting Column",
        options: []
    },
    t_limit: {
        type: "number",
        label: "Limit rows"
    }
};
