import Button from "../../../c.button";
import { Layout, Row, Cell } from "../../../layout";
import Toolbar from "../../../c.toolbar";
import DEFAULTS_COLUMN from "./defaults_column";
import FORM_COLUMN from "./form_column";
import Form from "../../../c.form";

if (!ewars.copy) ewars.copy = function (item) {
    return JSON.parse(JSON.stringify(item));
};

class Column extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isExpanded: false,
            errors: []
        }
    }

    _onAction = (action) => {
        switch(action) {
            case "UP":
                this.props.onUp(this.props.data, this.props.index);
                break;
            case "DOWN":
                this.props.onDown(this.props.data, this.props.index);
                break;
            case "DUPE":
                this.props.onDupe(this.props.data, this.props.index);
                break;
            case "DELETE":
                this.props.onDelete(this.props.data, this.props.index);
                break;
        }
    };

    _edit = () =>{
        this.setState({
            isExpanded: !this.state.isExpanded
        })
    };

    _update = (data, prop, value) => {
        this.props.onChange(this.props.index, prop, value);
    };


    render() {
        let colTitle = ewars.I18N(this.props.data.title);

        let editIcon = "fal fa-pencil";
        if (this.state.isExpanded) editIcon = "fal fa-times";


        return (
            <div className="data-item-wrapper">
                <div className="series-list-item">
                    <div className="name">{colTitle}</div>
                    <div className="control-wrapper" style={{maxWidth: "200px"}}>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-caret-up"
                                onClick={() => {this._onAction("UP")}}/>
                            <ewars.d.Button
                                icon="fa-caret-down"
                                onClick={() => {this._onAction("DOWN")}}/>
                        </div>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon={editIcon}
                                onClick={this._edit}/>
                            <ewars.d.Button
                                icon="fa-copy"
                                onClick={() => {this._onAction("DUPE")}}/>
                            <ewars.d.Button
                                icon="fa-trash"
                                colour="red"
                                onClick={() => {this._onAction("DELETE")}}/>
                        </div>
                    </div>
                </div>
                {this.state.isExpanded ?
                    <div className="data-item-settings">
                        <div className="ide-settings-content">
                            <div className="ide-settings-form">
                                <Form
                                    definition={FORM_COLUMN}
                                    data={this.props.data}
                                    updateAction={this._update}
                                    readOnly={false}
                                    errors={this.state.errors}/>
                            </div>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}


var DataEditor = React.createClass({
    getInitialState: function () {
        return {}
    },

    _addColumn: function () {
        var cols = ewars.copy(this.props.data.columns || []);
        cols.push({...{}, ...DEFAULTS_COLUMN});
        this.props.onChange("columns", cols);
    },

    _onChange: function (index, prop, value) {
        var cols = ewars.copy(this.props.data.columns);
        cols[index][prop] = value;
        this.props.onChange("columns", cols);
    },

    _onUp: function (col, index) {
        if (index <= 0) return;

        var targetIndex = index - 1;
        var cols = ewars.copy(this.props.data.columns || []);
        var tmp = cols[targetIndex];
        cols[targetIndex] = col;
        cols[index] = tmp;
        this.props.onChange("columns", cols);
    },

    _onDown: function (col, index) {
        if (index >= this.props.data.columns.length - 1) return;

        var targetIndex = index + 1;
        var cols = ewars.copy(this.props.data.columns || []);
        var tmp = cols[targetIndex];
        cols[targetIndex] = col;
        cols[index] = tmp;
        this.props.onChange("columns", cols);
    },

    _onDelete: function (data, index) {
        var series = ewars.copy(this.props.data.columns || []);
        series.splice(index, 1);
        this.props.onChange("columns", series);
    },

    _onDupe: function (data, index) {
        let series = ewars.copy(this.props.data.columns || []);
        // replace the uuid
        data.uuid = ewars.utils.uuid();
        series.push(ewars.copy(data));
        this.props.onChange("columns", series);
    },

    render: function () {

        let cols = this.props.data.columns.map((col, index) => {
            let key = "COLUMN_" + index;
            return <Column
                key={key}
                onChange={this._onChange}
                data={col}
                onUp={this._onUp}
                onDown={this._onDown}
                onDelete={this._onDelete}
                onDupe={this._onDupe}
                index={index}/>;
        });

        return (
            <div className="data-manager" id="dm">
                <Layout>
                    <Toolbar label="Table Columns">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-plus"
                                onClick={this._addColumn}
                                label="Add Column"/>
                        </div>
                    </Toolbar>
                    <Row>
                        <Cell>
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="series-list">
                                    {cols}
                                </div>
                            </div>
                        </Cell>
                    </Row>
                </Layout>
            </div>
        )
    }
});

export default DataEditor;
