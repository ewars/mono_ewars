export default {
    spec: 2,
    type: "TABLE",
    loc_spec: "SPECIFIC",
    title: {en: "New Table"},
    additionalClass: null,
    columns: []
};
