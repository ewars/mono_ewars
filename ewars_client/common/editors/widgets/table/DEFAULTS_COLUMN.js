export default {
    source_type: "SLICE",
    title: {en: "New Column"},
    indicator: null,
    formula: null,
    series: null,
    reduction: null,
    start_date: null,
    end_date: null
};
