import Moment from "moment";

const DEFAULTS = {
    type: "MAPPING",
    uuid: null,
    layers: [],
    annotations: []
};

export default DEFAULTS;
