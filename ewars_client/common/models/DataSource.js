var Moment = require("moment");

var DateUtils = require("../../documents/utils/DateUtils");

var COLOURS = [
    "orange", "red", "purple", "pink", "yellow", "green", "magenta", "grey", "black", "aque", "blue", "chocolate", "cyan"
];

/**
 * DataSource instance constructor
 * @param config
 * @constructor
 */
function DataSource(config, isPublic) {
    this.id = _.uniqueId("DS_");
    this._config = config;
    this._isPublic = isPublic || false;
    this._templateId = null;
    this._isLoaded = false;
    this._multiple = false;
    this.uuid = config.uuid || null;

    this.data = [];

    this.series = config.series || null;

    this.indicator = config.indicator;
    this.indicatorDefinition = null;

    this.location = config.location;
    this.locationDefinition = null;
    this.report_location = null;

    this.title = config.title || null;
    this.type = config.type;
    this.style = config.style || null;
    this.colour = config.colour || null;
    this.line_width = config.line_width || 0.25;
    this.marker_symbol = config.marker_symbol || undefined;
    this.marker_radius = config.marker_radius || 2;
    this.formula = config.formula || null;
    this.sources = config.series || null;
    this.loc_spec = config.loc_spec || null;

    this.load_geometry = config.geometry || false;

    // Map Reduce
    this.reduction = config.reduction || null;

    // Timeframe
    this.interval = config.interval || "NONE";
    this.start_date = config.start_date || null;
    this.end_date = config.end_date || null;
    this.filter = config.filter || null;

    // Check whether the result is multiple series.
    if (this.loc_spec == "GENERATOR") {
        this._multiple = true;
    }


    // ADvanced options
    this.hide_no_data = config.hide_no_data || false;
}

/**
 * Gets the data in the source reduced to a single value
 * @param reduction
 */
DataSource.prototype.getReduced = function (reduction) {
    var value = 0;

    _.each(this.data, function (item) {
        value = value + parseFloat(item[1]);
    }, this);

    return value;
};

DataSource.prototype.getTitle = function () {
    if (this.title) return ewars.formatters.I18N_FORMATTER(this.title);

    var title = [];
    if (this.locationDefinition) {
        title.push(ewars.formatters.I18N_FORMATTER(this.locationDefinition.title));
    }
    if (this.indicatorDefinition) {
        title.push(ewars.formatters.I18N_FORMATTER(this.indicatorDefinition.title));
    }
    if (this.formula) {
        title.push(this.formula);
    }

    return title.join(" - ");
};

/**
 * Retrieves the data source defined as a highcharts series
 */
DataSource.prototype._getHighcharts = function (dataOverride, seriesDef) {
    var data = this.data,
        indicator = this.indicatorDefinition,
        location = this.locationDefinition;
    if (dataOverride) {
        data = dataOverride.data;
        indicator = dataOverride.indicator;
        location = dataOverride.location;
    }

    var graph = {
        color: this.colour
    };

    if (this.type == "SERIES") {
        graph.data = _.sortBy(data, function (node) {
            return node[0];
        }, this);

        graph.data = _.map(graph.data, function (node) {
            var value = 0;
            if (!_.isNaN(parseFloat(node[1]))) value = parseFloat(node[1]);
            return [Moment(node[0]).valueOf(), node[1]];
        });

        graph.marker = {
            enabled: true,
            radius: this.marker_radius,
            symbol: this.marker_symbol
        };

        graph.lineWidth = parseFloat(this.line_width);

        var title = [];
        if (this.title) title.push(ewars.I18N(this.title));
        if (!this.title) {
            if (!this.title && indicator) title.push(ewars.formatters.I18N_FORMATTER(indicator.name));
            if (location && this.loc_spec == "GENERATOR") title.push(ewars.formatters.I18N_FORMATTER(location.name));
            if (!this.title && location) title.push(ewars.formatters.I18N_FORMATTER(location.name));
        }
        graph.name = title.join(" - ");


        graph.color = this.colour || null;

        graph.type = this.style;
        if (this.style == "area") {
            graph.fillOpacity = 0.6;
        }

        if (this.style == "bar") {
            graph.type = "column";
            graph.fillOpacity = 0.6;
        }


    }

    if (this.type == "COMPLEX") {
        graph.data = _.sortBy(data, function (node) {
            return node[0];
        }, this);

        graph.data = _.map(graph.data, function (node) {
            return [Moment.utc(node[0], "YYYY-MM-DD").valueOf(), parseFloat(node[1])];
        });

        graph.marker = {
            enabled: true,
            radius: this.marker_radius,
            symbol: this.marker_symbol
        };

        graph.lineWidth = parseFloat(this.line_width);

        var title = [];
        if (this.title) title.push(ewars.formatters.I18N_FORMATTER(this.title));
        if (!this.title && indicator) title.push(ewars.formatters.I18N_FORMATTER(indicator.name));
        if (!this.title && location) title.push(ewars.formatters.I18N_FORMATTER(location.name));
        graph.name = title.join(" - ");


        graph.color = this.colour || null;

        graph.type = this.style;
        if (this.style == "area") {
            graph.fillOpacity = 0.6;
        }

        if (this.style == "bar") {
            graph.type = "column";
            graph.fillOpacity = 0.6;
        }

    }

    return graph;
};

/**
 * Returns the data as an array of arrays which can easily be
 * converted into an excel sheet by the export API
 */
DataSource.prototype.getAsSheet = function () {
    var data = this.data;

    if (["SLICE", "SLICE_COMPLEX"].indexOf(this.type) >= 0) {
        if (!this._multiple) {
            data = [[this.end_date, this.getReduced()]];
        } else {

        }

    }

    if (["RAW", "RAW_COMPLEX"].indexOf(this.type) >= 0) {
        if (!this._multiple) {
            data = [[this.end_date, this.getReduced()]];
        } else {

        }
    }
    return [this.getTitle(), data];
};

DataSource.prototype.getAsSheetsMultiple = function () {
    // This source is responsible for multiple series,
    // so will return multiple sheets
    var sheets = [];

    _.each(this.data, function(data_item) {
        var title = [];
        if (this.title) title.push(ewars.formatters.I18N_FORMATTER(this.title));
        if (data_item.location) title.push(ewars.formatters.I18N_FORMATTER(data_item.location.name));
        if (data_item.indicator) title.push(ewars.formatters.I18N_FORMATTER(data_item.indicator.name));
        title = title.join(" - ");
        sheets.push([title, data_item.data]);
    }, this);

    return sheets;
};

DataSource.prototype.reduce = function (items) {
    var value = 0;

    _.each(items, function (item) {
        if (!_.isNaN(parseFloat(item[1]))) {
            value = value + parseFloat(item[1])
        }
    });

    return value;
};

/**
 * Check if the DataSource is valid for querying
 * @returns {boolean}
 */
DataSource.prototype.isValid = function () {
    var isValid = true;
    if (this.type == "SERIES") {
        if (!this.location) isValid = false;
        if (!this.indicator) isValid = false;
        if (!this.start_date) isValid = false;
        if (!this.end_date) isValid = false;
        if (!this.interval) isValid = false;
    }

    if (this.type == "COMPLEX") {
        if (!this.formula) isValid = false;
        if (!this.interval) isValid = false;
    }

    return isValid;
};

/**
 * Get the formatted analysis query for the item
 * @private
 */
DataSource.prototype._getQuery = function () {

    var query = {};

    if (this.type == "SERIES") {
        query = {
            dataType: this.type,
            target_indicator: this.indicator,
            target_interval: this.interval,
            target_location: this.location,
            options: {
                start_date: this.start_date,
                end_date: this.end_date,
                geometry: this.load_geometry
            },
            template_id: this._templateId || null
        }
    } else if (this.type == "COMPLEX") {
        query = {
            dataType: this.type,
            target_interval: this.interval,
            series: this.sources,
            formula: this.formula,
            options: {
                start_date: this.start_date,
                end_date: this.end_date,
                geometry: this.load_geometry
            },
            template_id: this._templateId || null
        }
    } else if (this.type == "SLICE") {
        query = {
            dataType: "SERIES",
            target_interval: this.interval,
            target_location: this.location,
            target_indicator: this.indicator,
            reduction: this.reduction,
            options: {
                start_date: this.start_date,
                end_date: this.end_date,
                geometry: this.load_geometry
            },
            template_id: this._templateId || null
        }
    } else if (this.type == "RAW") {

        query = {
            dataType: "SERIES",
            target_interval: this.interval,
            target_location: this.location,
            target_indicator: this.indicator,
            reduction: this.reduction,
            options: {
                start_date: this.start_date,
                end_date: this.end_date,
                geometry: this.load_geometry
            },
            template_id: this._templateId || null
        }
    } else if (this.type == "RAW_COMPLEX") {
        query = {
            dataType: "COMPLEX",
            target_interval: this.interval,
            series: this.sources,
            formula: this.formula,
            reduction: this.reduction,
            options: {
                start_date: start_date,
                end_date: end_date,
                geometry: this.load_geometry
            },
            template_id: this.templateId || null
        }
    } else if (this.type = "SLICE_COMPLEX") {
        query = {
            dataType: "COMPLEX",
            context: "SLICE",
            target_interval: this.interval,
            series: this.sources,
            formula: this.formula,
            reduction: this.reduction,
            options: {
                start_date: this.start_date,
                end_date: this.end_date,
                geometry: this.load_geometry
            },
            template_id: this._templateId || null
        }
    }

    if (this.loc_spec == "GENERATOR") {
        query.dataType = "GENERATOR";
        query.generator = {
            location_type: this.generator_location_type,
            location_parent: this.generator_location_parent,
            location_status: this.generator_location_status
        }
    }

    if (query.dataType == "COMPLEX") {
        query.series = _.map(query.series, function (source) {
            if (source.location_spec == "REPORT_LOCATION") {
                source.location = this.report_location;
            }
            return source;
        }, this)
    }
    return JSON.stringify(query);
};

/**
 * Load the data source
 * @returns {Promise}
 */
DataSource.prototype.load = function (callback, errCallback) {
    var uri = "http://" + ewars.domain + "/arc/analysis";
    if (this._isPublic) uri = "http://" + ewars.domain + "/arc/analysis";

    $.ajax({
        url: uri,
        dataType: "JSON",
        contentType: "application/json",
        context: this,
        type: "POST",
        data: JSON.stringify(this._config),
        success: function (resp) {
            if (_.isObject(this._config.location)) {
                this._multiple = true;
                this.data = resp;
                if (resp.length > 0) this.indicatorDefinition = resp[0].indicator;
            } else {
                this.data = resp.data;
                this.indicatorDefinition = resp.indicator;
                this.locationDefinition = resp.location;
            }

            this._isLoaded = true;
            callback(this);
        },
        error: function() {
            if (errCallback) errCallback(this.type, this);
        }
    })

};

DataSource.prototype.render = function (chart, seriesDef) {
    if (this._multiple) {
        _.each(this.data, function (series) {
            chart.addSeries(this._getHighcharts(series, seriesDef));
        }, this)
    } else {
        chart.addSeries(this._getHighcharts());
    }
};

DataSource.prototype.isMultiple = function () {
    return this._multiple;
};

module.exports = DataSource;