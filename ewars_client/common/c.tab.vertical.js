'use strict';

var VertTab = React.createClass({
    getDefaultProps: function () {
        return {
            active: false
        }
    },

    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-tab";
        if (this.props.active) className += " ide-tab-down";

        var iconClass = "fal " + this.props.icon;

        return (
            <div className={className} onClick={this._onClick}>
                <i className={iconClass}></i>&nbsp;{this.props.label}
            </div>
        )
    }
});

export default VertTab;
