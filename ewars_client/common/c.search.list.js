var _dataCache;

var SearchListComponent = React.createClass({
    getInitialState: function () {
        return {
            activeIndex: null,
            data: []
        }
    },

    componentWillMount: function () {
        var self = this;
        ewars._connect().then(function (session) {
            session.call("com.ewars.query", [self.props.resource, {
                $orderby: [
                    self.props.labelProperty + " DESC"
                ]
            }])
                .then(
                function (resp) {
                    self.setState({
                        data: resp.d
                    })
                }
            )
        })
    },

    _handleSelection: function (e) {
        this.setState({
            activeIndex: e.target.dataset.index
        });

        this.props.onItemSelect(this.state.data[e.target.dataset.index]);
    },

    _processSelections: function () {
        var nodes = [];

        if (!this.state.data || this.state.data.length <= 0) {
            return (
                <li>
                    <p className="placeholder">There are currently no values to display</p>
                </li>
            )
        }

        for (var i in this.state.data) {
            var isActive = "";
            if (i == this.state.activeIndex) isActive = "active";
            nodes.push(
                <li className={isActive} data-index={i} onClick={this._handleSelection}>
                    {this.state.data[i][this.props.labelProperty]}
                </li>
            )
        }
        return nodes;
    },

    _handleSearchChange: function (e) {
        var val = e.target.value;

        // if dataCache is not initialized, then move the data to it and update the data
        if (!_dataCache) _dataCache = JSON.parse(JSON.stringify(this.state.data));
        var searchResults = [];

        for (var i in _dataCache) {
            if (_dataCache[i][this.props.labelProperty].indexOf(val) >= 0) {
                searchResults.push(_dataCache[i]);
            }
        }

        this.setState({
            search: val,
            data: searchResults
        })
    },

    _handleSearchCancel: function () {
        this.setState({
            data: _dataCache,
            search: null
        })
    },

    render: function () {

        var nodes = this._processSelections();

        return (
            <div className="search-list">
                <div className="search">
                    <input type="text" value={this.state.search} onChange={this._handleSearchChange}/>
                    {this.state.search ?
                        <button className="search-cancel" onClick={this._handleSearchCancel}><i
                            className="fal fa-times-circle"></i></button>
                        : null}

                </div>
                <div className="items">
                    <ul>
                        {nodes}
                    </ul>
                </div>

            </div>
        )
    }
});

export default SearchListComponent;
