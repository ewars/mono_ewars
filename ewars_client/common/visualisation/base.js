Highcharts.setOptions({
    animation: false,
    chart: {
        animation: false
    }
})


const _getData = (data, filters, axis) => {
    return new Promise((resolve, reject) => {
        ewars.tx("com.ewars.plot", [data, filters, axis])
            .then((resp) => {
                resolve(resp);
            })
            .catch(err => {
                reject(err);
            })
    })
}

const TYPE_MAP = {
    'DATE': 'datetime',
    'NUM': null,
    'PERC': null,
    'BOOl': 'category'
};

const isNum = (val) => {
    return !isNaN(parseFloat(val)) && isFinite(val);

}

const _f = (val, alternate) => {
    if (val == undefined || val == '' || val == null) return alternate;
    if (isNum(val)) return parseInt(val);
    return val;
}

class Base {
    constructor(props, data, el) {
        this._config = props;
        this._data = data;
        this._chart = null;
        this._el = el;

        this._data = [];

        // TODO: Drop loading screen into the el

        let options = {
            chart: {
                renderTo: this._el,
                backgroundColor: _f(props.backgroundColor, undefined),
                borderColor: _f(props.borderColor, undefined),
                borderWidth: _f(props.borderWidth, 0),
                borderRadius: _f(props.borderRadius, 0)
            },
            title: {text: undefined},
            subtitle: {text: undefined},
            legend: {enabled: false},
            credits: {enabled: false},
            yAxis: [],
            xAxis: [],
            series: [],
            plotOptions: {
                column: {
                    groupPadding: 0.1,
                    pointPadding: 0,
                    borderWidth: 1
                }
            }
        };


        if (props.title) {
            options.title = {
                text: props.titleText || undefined,
                align: props.titleAlign || 'center',
                verticalAlign: _f(props.titleVAlign, undefined),
                style: {
                    color: props.titleColor || '#333333',
                    fontSize: props.titleSize || '12px',
                    fontWeight: props.titleWeight || 'normal'
                },
                x: _f(props.titleX, 0),
                y: _f(props.titleY, undefined)
            }
        }

        if (props.subTitle) {
            options.subtitle = {
                text: _f(props.sTitleText, undefined),
                align: _f(props.sTitleAlign, 'center'),
                verticalAlign: _f(props.sTitleVAlign, 'top'),
                style: {
                    color: _f(props.sTitleColor, '#333333'),
                    fontSize: _f(props.sTitleSize, '12px'),
                    fontWeight: _f(props.sTitleWeight, 'normal')
                },
                x: _f(props.sTitleX, undefined),
                y: _f(props.sTitleY, undefined)
            }
        }

        if (props.zoomable) {
            options.chart.zoomType = 'xy'
            options.chart.plotBackgroundColor = _f(props.plotBackgroundColor, 'transparent');
        }

        options.boost = {
            useGPUTranslations: true
        };

        let iconEl = document.createElement('i');
        iconEl.setAttribute('class', 'fal fa-cog fa-spin');
        iconEl.style.position = 'absolute';
        iconEl.style.top = '50%';
        iconEl.style.left = '50%';
        iconEl.style.fontSize = '20px';
        iconEl.style.color = '#000000';
        this._icon = iconEl;
        this._el.appendChild(iconEl);


        this.options = options;

    };

    _complete = () => {

        let controls = this._config.n.filter(node => {
            return node.t == 'X';
        })

        controls.forEach(node => {
            if (node._t == 'LEGEND') this._renderLegend(node);
        })

        this._icon.style.display = 'none';
        this._chart = Highcharts.chart(this.options);
    };


    _renderLegend = (legend) => {
        this.options.legend = {
            enabled: true,
            layout: _f(legend.layout, 'vertical'),
            backgroundColor: _f(legend.bgdColor, undefined),
            itemStyle: {
                fontSize: _f(legend.fontSize, 12),
                fontWeight: _f(legend.weight, 'normal'),
                color: _f(legend.color, '#333333')
            },
            symbolHeight: _f(legend.symbolHeight, undefined)
        }

        if (legend.bBorder) {
            this.options.legend.borderWidth = _f(legend.borderWidth, 1);
            this.options.legend.borderColor = _f(legend.borderColor, '#333333');
        }

        this.options.legend.title = {text: _f(legend.title, undefined)};


        switch (legend.position) {
            case 'TL':
                this.options.legend.verticalAlign = 'top';
                this.options.legend.align = 'left';
                this.options.legend.floating = true;
                break;
            case 'TC':
                this.options.legend.verticalAlign = 'top';
                this.options.legend.align = 'center';
                break;
            case 'TR':
                this.options.legend.verticalAlign = 'top';
                this.options.legend.align = 'right';
                this.options.legend.floating = true;
                break;
            case 'ML':
                this.options.legend.verticalAlign = 'middle';
                this.options.legend.align = 'left';
                break;
            case 'MR':
                this.options.legend.verticalAlign = 'middle';
                this.options.legend.align = 'right';
                break;
            case 'BL':
                this.options.legend.verticalAlign = 'bottom';
                this.options.legend.align = 'left';
                this.options.legend.floating = true;
                break;
            case 'BC':
                this.options.legend.verticalAlign = 'bottom';
                this.options.legend.align = 'center';
                break;
            case 'BR':
                this.options.legend.verticalAlign = 'bottom';
                this.options.legend.align = 'right';
                this.options.legend.floating = true;
                break;
            default:
                this.options.legend.verticalAlign = 'middle';
                this.options.legend.align = 'right';
                break;
        }

    };

    redraw = () => {
        if (this._chart) {
            if (this._chart.reflow) this._chart.reflow();
        }
    };

    getData = () => {

    };

    render = () => {


    };
}

export default Base;
