import Source from '../source';
import Moment from 'moment';

import Base from '../base';
import CONTROLS from '../features';
import aQueue from '../../api';

Highcharts.setOptions({
    animation: false,
    chart: {
        animation: false
    }
})


const _getData = (data, filters, axis) => {
    return new Promise((resolve, reject) => {
        ewars.tx("com.ewars.plot", [data, filters, axis])
            .then((resp) => {
                resolve(resp);
            })
            .catch(err => {
                reject(err);
            })
    })
}

const _f = (val, fb) => {
    if (val == undefined || val == null) return fb;
    if (val == '') return fb;
    return val;
}

class PieChart extends Base {
    constructor(props, data, el) {
        super(props, data, el);
        this.render();
    }

    render = () => {
        this.series = [];
        this._totalGroups = this._config.g.length;
        this._loadedGroups = 0;

        let itemDict = {};

        // Create two sets of groups, where the data is grouped together along the same axis
        let definition = this._config.n.filter(node => {
            return (
                node.t != 'A'
                && node.t != 'X'
            )
        });

        this.options.plotOptions = {
            pie: {
                showInLegend: true
            }
        }

        aQueue.get(definition, [])
            .then(resp => {

                let definition = this._config.n.filter(node => {
                    return (
                        node.t != 'A'
                        && node.t != 'X'
                    )
                });

                /**
                 * {
                     *  keys: [[index, dim.id],...]
                     *  m: <measureId>
                     *  mi: <measureIndex>
                     * }
                 */
                let groupings = [];
                let dimAggs = {},
                    hasDims = false;

                let measures = this._config.n.filter(node => {
                    return ['M', 'C'].indexOf(node.t) >= 0 && !node.p && !node._t;
                });

                let dimensions = this._config.n.filter(node => {
                    return node.t == 'D' && !node.p;
                })

                measures.forEach(meas => {
                    let result = {
                        m: meas,
                        mi: resp.i[meas._],
                        dims: []
                    };
                    dimensions.forEach(dim => {
                        hasDims = true;
                        result.dims.push([
                            resp.i[dim._],
                            dim._
                        ])
                    });
                    groupings.push(result);
                });

                if (hasDims) {
                    let valueSets = [];
                    for (let i in dimAggs) {
                        valueSets.push([i, resp.i[i], resp.m[i].values]);
                    }

                    // iterate over the values sets to group values into series
                    let sIndex = 0;
                    valueSets.forEach(group => {
                        group[2].forEach(val => {
                            if (valueSets.length > 1) {
                                // apply each sub-value to create a group
                                valueSets.slice(sIndex, valueSets.length).forEach(val => {

                                })
                            } else {
                                measures.forEach(meas => {
                                    // TODO: Get axis for dim and resultset
                                    // TODO: Get metadata for labels, etc..
                                    // TODO: Styling

                                    let measIndex = resp.i[meas[0]];
                                    let graph = {
                                        data: [],
                                        borderColor: null,
                                        name: meas.label || meas.n,
                                        dataLabels: {
                                            enabled: _f(this._config.showLabels, false)
                                        }
                                    };
                                    let subSet = resp.d.filter(row => {
                                        return row[group[1]] == val;
                                    })

                                    subSet = subSet.map(row => {
                                        return [row[resp.i[dimensions[0][0]]], parseFloat(row[measIndex])]
                                    })
                                    graph.data = subSet.sort((a, b) => {
                                        if (a[0] > b[0]) return 1;
                                        if (a[0] < b[0]) return -1;
                                        return 0;
                                    })
                                    graph.meta = resp.m[group[0]].dict[val];

                                    this.options.series.push(graph);
                                });
                            }
                        })

                    });
                } else {
                    let graph = {
                        data: [],
                        type: 'pie',
                        borderColor: null,
                        colorByPoint: true,
                        dataLabels: {
                            enabled: _f(this._config.showLabels, false)
                        }
                    };

                    if (this._config.pBorder) {
                        graph.borderWidth = _f(this._config.borderWidth, 1);
                        graph.borderColor = _f(this._config.borderColor, 'white');
                    }

                    if (this._config.donut) {
                        this.options.plotOptions.pie.innerSize = '50%';
                    }

                    measures.forEach(meas => {
                        let measIndex = resp.i[meas._];
                        graph.data.push({
                            name: meas.label || meas.n,
                            y: parseFloat(resp.d[0][measIndex])
                        })
                    })
                    this.options.series.push(graph);
                }


                this._loadedGroups++;
                if (this._loadedGroups >= this._totalGroups) this._complete();
            })
            .catch(err => {
                // Catch any errors on the client-side
                console.log(err);
            })

    };




    _recvGroupData = (group, data) => {

    };

    update = (data) => {
        this._config = data;
        this.render();
    };

    getData = () => {

    };

    destroy = () => {

    };

    exportImage = () => {

    };
}

export default PieChart;
