import Source from '../source';
import Moment from 'moment';
import aQueue from '../../api';

import CONTROLS from '../features';

import Base from '../base';

Highcharts.setOptions({
    animation: false,
    chart: {
        animation: false
    }
})


const _getData = (data, axis) => {
    return new Promise((resolve, reject) => {
        ewars.tx("com.ewars.plot", [data, axis])
            .then((resp) => {
                resolve(resp);
            })
            .catch(err => {
                reject(err);
            })
    })
}

const TYPE_MAP = {
    'DATE': 'datetime',
    'NUM': null,
    'PERC': null,
    'BOOl': 'category'
};

let isX = (val) => {
    return (val == 'true' || val == true);
};

const isNodeADate = (node) => {
    if (node._dt == 'DATE') return true;
    if (node._t == 'date') return true;
    if (node.n) {
        if (node.n.indexOf('DATE') >= 0) return truel
    }

    return false;
};

const _fb = (val, fb) => {
    if (val == undefined) return fb;
    if (val == null) return fb;
    if (val == '') return fb;
    return val;
}

class DefaultChart extends Base {
    constructor(props, data, el) {
        super(props, data, el);
        this.render();
    }

    _createSeries = () => {

    };


    render = () => {
        console.log(this._config);
        if (!this._config.n || this._config.n.length <= 0) {
            return;
        }
        let groups = this._config.g;
        let controls = this._config.n.filter(node => {
            return node.t == 'X';
        });

        let showInNavigator = false;
        let addedControls = [];
        controls.forEach(control => {
            if (CONTROLS[control._t]) {
                if (control._t == 'NAVIGATOR') showInNavigator = true;
                let newControl = new CONTROLS[control._t](this.options, control)
                this.options = newControl.getOptions();
                addedControls.push(newControl);
            }
        });

        this.options.plotOptions = {
            line: {
                size: '100%'
            }
        }

        this.series = [];
        this._totalGroups = groups.length;
        this._loadedGroups = 0;

        let axisDict = {};
        let axis = this._config.n.filter(node => {
            return node.t == 'A';
        })
        axis.forEach(item => {
            axisDict[item._] = item;
        });

        // Configure axis for the chart
        let xAxis = [], yAxis = [];
        axis.forEach(item => {
            let newAxis = {
                title: {text: item.label},
                opposite: item.opposite ? true : false,
                type: item._t || null,
                id: item._,
                min: item.min || undefined,
                max: item.max || undefined,
                ceiling: item.ceil || undefined,
                floor: item.floor || undefined

            };

            if (newAxis.type == 'numeric') newAxis.allowDecimals = false;

            // Get controls which attach to this axis
            let axControls = this._config.n.filter(n => {
                return n.t == 'X' && n.axis == newAxis.id;
            });

            axControls.forEach(control => {
                if (control._t == 'LINE') {
                    if (!newAxis.plotLines) newAxis.plotLines = [];
                    let value = control.from;
                    if (newAxis.type == 'date') value = new Date(control.from).getTime();
                    newAxis.plotLines.push({
                        value: value,
                        width: _fb(control.strokeWidth, 1),
                        color: _fb(control.strokeColour, '#333333'),
                        dashStyle: _fb(control.dashStyle, 'Solid'),
                        label: {
                            text: _fb(control.label, '')
                        },
                        zIndex: 1
                    })
                }

                if (control._t == 'BAND') {
                    if (!newAxis.plotBands) newAxis.plotBands = [];
                    let from = control.from;
                    let to = control.to;
                    if (newAxis.type == 'date') {
                        from = new Date(from).getTime();
                        to = new Date(to).getTime();
                    }

                    newAxis.plotBands.push({
                        from: from,
                        to: to,
                        color: _fb(control.color, '#333333'),
                        label: {
                            text: _fb(control.label, undefined)
                        },
                        borderColor: _fb(control.borderColor, '#333333'),
                        borderWidth: _fb(control.borderWidth, 1),
                        zIndex: 1
                    })
                }
            })

            let isDate = isNodeADate(item);

            newAxis.labels = {formatter: function () {
                return this.value;
            }};
            if (isDate) {
                newAxis.labels.formatter = function () {
                    return Moment.utc(this.value).format(_fb(item.dateFormat, 'YYYY-MM-DD'))
                }
            }

            newAxis.labels.rotation = _fb(item.labelRotation, 0);

            // find the dimensions being plotted along this axis
            let axisDims = this._config.n.filter(node => {
                return node.x == item._ && node.t == 'DIM';
            });

            if (isX(item.x)) xAxis.push(newAxis);
            if (!isX(item.x)) yAxis.push(newAxis);
        });

        let filters = this._config.n.filter(node => {
                return node._t == 'F';
            }),
            details = this._config.n.filter(node => {
                return node._t == 'D';
            }),
            colourers = this._config.n.filter(node => {
                return node._t == 'C';
            }),
            calc_vars = this._config.n.filter(node => {
                return node._t == 'CX';
            });

        let alternates = [calc_vars, colourers, details, filters];


        this.options.xAxis = xAxis;
        this.options.yAxis = yAxis;

        // Handle setting up stacking groups
        let stacks = this._config.n.filter(n => {
            return n.t == 'X' && n._t == 'STACK';
        })

        let stackGroups;
        if (stacks.length > 0) {
            stackGroups = [];
            stacks.forEach(stack => {
                let g = [ewars.utils.uuid().split('-')[0], _fb(stack.mode, 'normal')];
                stack.targets.forEach(s => {
                    g.push(s);
                })
                stackGroups.push(g);
            })
        }

        groups.forEach(group => {
            // Create two sets of groups, where the data is grouped together along the same axis
            let axis = this._config.n.filter(node => {
                return node.t == 'A';
            });

            let nodesInGroup = this._config.n.filter(node => {
                return (node.g == group._ && ['M', 'C', 'D'].indexOf(node.t) >= 0);
            });
            let rootNodeIds = nodesInGroup.map(node => {
                return node._;
            });

            alternates.forEach(series => {
                series.forEach(node => {
                    if (rootNodeIds.indexOf(node.p) >= 0) {
                        nodesInGroup.push(node);
                        rootNodeIds.push(node._);
                    }
                })
            });

            aQueue.get(nodesInGroup, axis)
                .then(resp => {
                    // Need to break up the data
                    // get dimensions that are on the same axis as the measure
                    let measures = nodesInGroup.filter(node => {
                        return ['M', 'C'].indexOf(node.t) >= 0 && !node.p;
                    });

                    let dimensions = nodesInGroup.filter(node => {
                        return node.t == 'D';
                    });

                    //TODO: Get detail and colour variables as well for each datapoint


                    /**
                     * {
                     *  keys: [[index, dim.id],...]
                     *  m: <measureId>
                     *  mi: <measureIndex>
                     * }
                     */
                    let groupings = [];
                    let dimAggs = {},
                        hasDims = false;

                    // There can only be one of these per group
                    let plotDim = dimensions.filter(dim => {
                        return dim.x != 'AGG';
                    })[0];
                    let dimIndex = resp.i[plotDim._];

                    measures.forEach(meas => {
                        let result = {
                            m: meas,
                            mi: resp.i[meas._],

                            dims: [],
                            props: []
                        };

                        dimensions.forEach(node => {
                            if (node.x == 'DAGG') {
                                result.dims.push({
                                    i: resp.i[dim._],
                                    dim: node
                                })
                            }

                            if (['D', 'C', 'S'].indexOf(node._t) >= 0) {
                                result.props.push({
                                    i: resp.i[dim._],
                                    dim: node
                                })
                            }
                        });
                        groupings.push(result);
                    });


                    groupings.forEach(sub_group => {
                        if (sub_group.dims.length <= 0) {
                            // no disaggregation, so we're just creating a single series
                            let measIndex = sub_group.mi;


                            let xId, yId, axisPlot = 'y';
                            let mPrimAxis = axisDict[sub_group.m.x];
                            if (isX(mPrimAxis)) {
                                // plotting along the x a
                                axisPlot = 'x';
                                xId = sub_group.m.x;
                                axis.forEach(node => {
                                    if (!isX(node.x)) {
                                        yId = node._;
                                    }
                                })
                            } else {
                                yId = sub_group.m.x;
                                // need to find x
                                axis.forEach(node => {
                                    if (isX(node.x)) {
                                        xId = node._;
                                    }
                                })
                            }

                            console.log(stackGroups);
                            let stackId, stackType;
                            if (stackGroups) {
                                stackGroups.forEach(g => {
                                    if (g.indexOf(sub_group.m._) >= 0) {
                                        stackId = g[0];
                                        stackType = g[1];
                                    }
                                })
                            }

                            let graph = {
                                data: [],
                                xAxis: xId,
                                yAxis: yId,
                                borderColor: null,
                                name: _fb(sub_group.m.label, sub_group.m.n),
                                stack: _fb(stackId, undefined),
                                stacking: _fb(stackType, undefined),
                                type: sub_group.m.render || 'line',
                                dashStyle: sub_group.m.dashStyle || undefined,
                                color: sub_group.m.color ? `rgb(${sub_group.m.color}` : undefined
                            };
                            let subSet = resp.d.map(row => {
                                return [row[dimIndex], parseFloat(row[measIndex])]
                            })
                            graph.data = subSet.sort((a, b) => {
                                if (a[0] > b[0]) return 1;
                                if (a[0] < b[0]) return -1;
                                return 0;
                            })

                            this.options.series.push(graph);
                        }
                    })


                    this._loadedGroups++;
                    if (this._loadedGroups >= this._totalGroups) this._complete();
                })
                .catch(err => {
                    // Catch any errors on the client-side
                    console.log(err);
                })
        })

    };


    _recvGroupData = (group, data) => {

    };

    update = (data) => {
        this._config = data;
        this.render();
    };

    getData = () => {

    };

    destroy = () => {

    };

    exportImage = () => {

    };
}

export default DefaultChart;
