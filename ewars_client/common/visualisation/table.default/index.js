import aQueue from '../../api';
import Base from '../base';


const _getData = (data, axis) => {
    return new Promise((resolve, reject) => {
        ewars.tx("com.ewars.plot", [data, axis])
            .then((resp) => {
                resolve(resp);
            })
            .catch(err => {
                reject(err);
            })
    })
}

const TYPE_MAP = {
    'DATE': 'datetime',
    'NUM': null,
    'PERC': null,
    'BOOl': 'category'
};

let isX = (val) => {
    return (val == 'true' || val == true);
};

const isNodeADate = (node) => {
    if (node._dt == 'DATE') return true;
    if (node._t == 'date') return true;
    if (node.n) {
        if (node.n.indexOf('DATE') >= 0) return truel
    }

    return false;
};

class TableDefault extends Base {
    constructor(props, data, el) {
        super(props, data, el);
        this.render();
    }

    _createSeries = () => {

    };

    render = () => {
        if (!this._config.n || this._config.n.length <= 0) {
            return;
        }
        let groups = this._config.g;
        let controls = this._config.n.filter(node => {
            return node.t == 'X';
        });

        this.series = [];
        this._totalGroups = groups.length;
        this._loadedGroups = 0;

        let axisDict = {};
        let axis = this._config.n.filter(node => {
            return node.t == 'A';
        })
        axis.forEach(item => {
            axisDict[item._] = item;
        });

        // Configure axis for the chart
        let xAxis = [], yAxis = [];
        axis.forEach(item => {
            let newAxis = {
                title: {text: item.label},
                opposite: item.opposite ? true : false,
                type: item._t || null,
                id: item._,
                min: item.min || undefined,
                max: item.max || undefined,
                ceiling: item.ceil || undefined,
                floor: item.floor || undefined

            }

            let isDate = isNodeADate(item);

            if (isDate) {
                newAxis.labels = {
                    formatter: function () {
                        return Moment.utc(this.value).format('YYYY-MM-DD')
                    },
                    rotation: -90
                }
            }

            // find the dimensions being plotted along this axis
            let axisDims = this._config.n.filter(node => {
                return node.x == item._ && node.t == 'DIM';
            });

            if (isX(item.x)) xAxis.push(newAxis);
            if (!isX(item.x)) yAxis.push(newAxis);
        })

        this.options.xAxis = xAxis;
        this.options.yAxis = yAxis;

        groups.forEach(group => {
            // Create two sets of groups, where the data is grouped together along the same axis
            let axis = this._config.n.filter(node => {
                return node.t == 'A';
            });

            let nodesInGroup = this._config.n.filter(node => {
                return ((node.g == group._ && ['M', 'C', 'D'].indexOf(node.t) >= 0) || node._t == 'CX');
            });
            let rootNodeIds = nodesInGroup.map(node => {
                return node._;
            });

            this._config.n.forEach(node => {
                if (rootNodeIds.indexOf(node.p) >= 0) {
                    nodesInGroup.push(node);
                }
            })

            aQueue.get(nodesInGroup, axis)
                .then(resp => {
                    this._data = resp;
                    // Need to break up the data
                    // get dimensions that are on the same axis as the measure
                    let measures = nodesInGroup.filter(node => {
                        return ['M', 'C'].indexOf(node.t) >= 0 && !node.p;
                    });

                    let dimensions = nodesInGroup.filter(node => {
                        return node.t == 'D';
                    });

                    //TODO: Get detail and colour variables as well for each datapoint

                    /**
                     * {
                     *  keys: [[index, dim.id],...]
                     *  m: <measureId>
                     *  mi: <measureIndex>
                     * }
                     */
                    let groupings = [];
                    let dimAggs = {},
                        hasDims = false;

                    // There can only be one of these per group
                    let plotDim = dimensions.filter(dim => {
                        return dim.x != 'AGG';
                    })[0];
                    let dimIndex = resp.i[plotDim._];

                    measures.forEach(meas => {
                        let result = {
                            m: meas,
                            mi: resp.i[meas._],

                            dims: [],
                            props: []
                        };

                        dimensions.forEach(node => {
                            if (node.x == 'DAGG') {
                                result.dims.push({
                                    i: resp.i[dim._],
                                    dim: node
                                })
                            }

                            if (['D', 'C', 'S'].indexOf(node._t) >= 0) {
                                result.props.push({
                                    i: resp.i[dim._],
                                    dim: node
                                })
                            }
                        });
                        groupings.push(result);
                    });


                    groupings.forEach(sub_group => {
                        if (sub_group.dims.length <= 0) {
                            // no disaggregation, so we're just creating a single series
                            let measIndex = sub_group.mi;

                            let xId, yId, axisPlot = 'y';
                            axis.forEach(node => {
                                if (node._ == sub_group.m.x) {
                                    if (isX(node.x)) {
                                        axisPlot = 'x';
                                        xId = node._;
                                    } else {
                                        yId = node._;
                                    }
                                }
                            });
                            // Need to find alternate axis
                            axis.forEach(node => {
                                if (axisPlot == 'y') {
                                    if (!isX(node.x)) yId = node._;
                                } else {
                                    if (isX(node.x)) xId = node._;
                                }
                            })

                            let graph = {
                                data: [],
                                xAxis: xId,
                                yAxis: yId,
                                borderColor: null,
                                type: sub_group.m.render || 'line',
                                dashStyle: sub_group.m.dashStyle || undefined,
                                color: sub_group.m.color ? `rgb(${sub_group.m.color}` : undefined
                            }
                            let subSet = resp.d.map(row => {
                                return [row[dimIndex], parseFloat(row[measIndex])]
                            })
                            graph.data = subSet.sort((a, b) => {
                                if (a[0] > b[0]) return 1;
                                if (a[0] < b[0]) return -1;
                                return 0;
                            })

                            this.options.series.push(graph);
                        }
                    })


                    this._loadedGroups++;
                    if (this._loadedGroups >= this._totalGroups) this._complete();
                })
                .catch(err => {
                    // Catch any errors on the client-side
                    console.log(err);
                })
        })

    };

    _createCell = (data) => {
        let cell = document.createElement('td');
        if (data) cell.appendChild(document.createTextNode(data));
        return cell;
    };

    _createHeaderCell = (data) => {
        let cell = document.createElement('th');
        if (data) cell.appendChild(document.createTextNode(data));
        return cell;
    };

    redraw = () => {
        // this._complete();
    }

    _complete = () => {
        let wrapper = document.createElement('div');
        wrapper.style.overflow = 'scroll';
        wrapper.style.maxWidth = '100%';
        wrapper.style.width = '100%';
        wrapper.style.height = '100%';
        wrapper.style.maxHeight = '100%';
        wrapper.style.position = 'absolute';
        wrapper.style.top = 0;
        wrapper.style.left = 0;
        this._wrapper = wrapper;

        let table = document.createElement('table');
        table.style.tableLayout = 'auto';
        table.style.borderCollapse = 'collapse';
        table.setAttribute('class', 'nb-table');
        table.setAttribute('width', '100%');
        table.style.width = '100%';

        let thead = document.createElement('thead');
        let headers = [];
        let yHeaders = [];

        let yAxis = this._config.n.filter(node => {
            return node.x == 'y';
        });
        yAxis.forEach(node => {
            headers.push(node.n);
        });

        let xAxis = this._config.n.filter(node => {
            return node.x == 'x';
        });
        xAxis.forEach(node => {
            headers.push(node.n);
        });

        let disaggs = this._config.n.filter(node => {
            return node.x == 'DAGG';
        });

        let xHeaders = [];
        let rows = [];

        if (disaggs.length <= 0) {
            // we're not breaking down the axis by anything meaningful
            let xLooks = [];

            xAxis.forEach(node => {
                if (node.t == 'M') {
                    xHeaders.push(node);
                    xLooks.push([
                        'M',
                        node._,
                        this._data.i[node._]
                    ])
                } else if (node.t == 'C') {
                    xHeaders.push(node);
                    xLooks.push([
                        'C',
                        node._,
                        this._data.i[node._]
                    ])
                } else if (node.t == 'D') {
                    xHeaders.push.apply(xHeaders, this._data.m[node._].values);
                }
            });

            let lookup = {};
            this._config.n.forEach(node => {
                lookup[node._] = node;
            })

            yAxis.forEach(node => {
                if (node.t == 'M') {
                    let newRow = [];
                    newRow.push(node.n);

                } else if (node.t == 'D') {
                    let values = this._data.m[node._].values;
                    let dimIndex = this._data.i[node._];
                    values.forEach(item => {
                        let row = [];

                        row.push(this._createCell(this._data.m[node._].dict[item].name || null));

                        let subRows = this._data.d.filter(r => {
                            return r[dimIndex] == item;
                        })[0] || null;

                        xLooks.forEach(x => {
                            if (!subRows) {
                                row.push(this._createCell());
                            } else {
                                let node = lookup[x[1]];
                                let value = subRows[x[2]];
                                if (node.format) value = ewars.NUM(value, node.format || '0');
                                row.push(this._createCell(value));
                            }
                        })

                        rows.push(row);

                    })
                }
            })
        }

        let headerRow = document.createElement('tr');
        let key = document.createElement('th');
        key.appendChild(document.createTextNode(yAxis[0].label || yAxis[0].n))
        headerRow.appendChild(key);

        xHeaders.forEach((node, index) => {
            let headerCell;
            if (node.t == 'M' || node.t == 'C') headerCell = this._createHeaderCell(node.label || node.n);
            if (node.t == 'D') headerCell = this._createHeaderCell(node.valueName || node.n);
            if (index >= xHeaders.length) headerCell.style.width = '100%';
            headerRow.appendChild(headerCell);
        });
        thead.appendChild(headerRow);
        table.appendChild(thead);

        let tbody = document.createElement('tbody');

        rows.forEach(item => {
            let newRow = document.createElement('tr');
            item.forEach(node => {
                newRow.appendChild(node);
            })
            tbody.appendChild(newRow);
        })

        table.appendChild(tbody);

        thead.appendChild(headerRow);

        let tfoot = document.createElement('tfoot');
        table.appendChild(tfoot);
        wrapper.appendChild(table);

        this._el.appendChild(wrapper);

        this._icon.style.display = 'none';

    };


    _recvGroupData = (group, data) => {

    };

    update = (data) => {
        this._config = data;
        this.render();
    };

    getData = () => {

    };

    destroy = () => {

    };

    exportImage = () => {

    };
}

export default TableDefault;