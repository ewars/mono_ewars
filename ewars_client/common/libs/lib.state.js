import BaseState from '../../common/libs/lib.state';

let state;
class IndicatorState extends BaseState {
    constructor(props) {
        super(props);

        state = this;

        this.view = 'DEFAULT';
        this.editing = null;
        this.indicators = [];
        this.groups = [];
        this.search = '';
    }

    getNodes = (pid) => {

    };

    searchNodes = (searchTerm) => {

    };

    updateSearch = (value) => {
        this.search = value;

        this.emit('SEARCH_CHANGE');
    };

    clearSearch = () => {
        this.search = '';

        this.emit('SEARCH_CLEAR');
    };

    editIndicator = (data) => {
        this.view = 'EDIT_INDICATOR';
        this.editing = data;
    };

    editIndicatorGroup = (data) => {
        this.view = 'EDIT_GROUP';
        this.editing = data;
    };

    cancelEditing = () => {
        this.view = 'DEFAULT';
        this.editing = null;
    };

    saveIndicator = () => {

    };

    saveGroup = () => {

    };

    deleteGroup = (id) => {

    };

    /**
     * Delete an indicator
     */
    deleteIndicator = (id) => {

    };

    /**
     * Update the property of the item being edited
     * @param prop
     * @param value
     */
    updateEditingProps = (prop, value) => {
        this.editing[prop] = value;

        this.emit("ON_EDIT_CHANGE");
    };

}

export {state, IndicatorState};
export default IndicatorState;