class BaseState {
    constructor() {
        this._listeners = {};
        this._subStates = {};
    }

    on = (event, callback) => {
        if (this._listeners[event]) {
            this._listeners[event].push(callback);
        } else {
            this._listeners[event] = [callback];
        }
    };

    off = (event, callback) => {
        if (this._listeners[event]) {
            let rIndex;
            this._listeners[event].forEach((cb, index) => {
                if (cb == callback) rIndex = index;
            })
            this._listeners[event].splice(rIndex, 1);
        }
    };

    _emit = (event) => {
        if (this._listeners[event]) {
            this._listeners[event].forEach(cb => {
                cb();
            })
        }
    };

    /**
     * Retrieve state in s storable format, JSON serializable
     */
    getStoreableState = () => {

    };

    registerSubState = (id, state) => {
        this.subStates[id] = state;
    };

    getSubState = (id) => {
        if (this.subStates[id]) return this.subStates[id];
        return null;
    };

    removeSubState = (id) => {
        if (this.subStates[id]) delete this.subStates[id];
        return null;
    }
}

export default BaseState;