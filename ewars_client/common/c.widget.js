class Widget extends React.Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        padded: true,
        label: "NO_LABEL",
        icon: null
    };

    render() {

        let bodyClass = "body";
        if (!this.props.padded) bodyClass += " no-pad";

        return (
            <div className="widget">
                <div className="widget-header"><span>{this.props.label}</span></div>
                <div className="body">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Widget;
