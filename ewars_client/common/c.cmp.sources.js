import DataSourceTree from "./c.cmp.data_source_tree";

const ACTIONS = [
    ["fa-minus", "REMOVE"]
];

class DataSourceSelect extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            expanded: false
        }

    }

    _toggle = () => {
        this.setState({
            expanded: !this.state.expanded
        })
    };

    _onClick = (data) => {
        this.setState({
            expanded: false
        })

        this.props.onSelect(data);
    };

    render() {
        let dispValue = "Add Source";

        return (
            <div ref="selector" className="ew-select" style={{marginBottom: "8px"}}>
                <div className="handle" onClick={this._toggle} style={{maxHeight: "26px", overflow: "hidden"}}>
                    <ewars.d.Row>
                        <ewars.d.Cell style={{overflowY: "hidden", lineHeight: "12px"}}>{dispValue}</ewars.d.Cell>
                        <ewars.d.Cell borderLeft={true} width="30px"
                                      style={{textAlign: "center", background: "rgba(0,0,0,0.1)"}}>
                            <i className="fal fa-caret-down"></i>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.expanded ?
                    <div className="ew-select-data" style={{padding: 0}}>
                        <DataSourceTree
                            draggable={false}
                            onClick={this._onClick}/>
                    </div>
                    : null}
            </div>
        )
    }
}

const ROW_ACTIONS = [
    ["fa-pencil", "EDIT"],
    ["fa-trash", "DELETE"]
]


class Source extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }

    }

    componentWillMount() {
        if (this.props.data[0] == "I") {
            ewars.tx("com.ewars.resource", ["indicator", this.props.data[1], null, null])
                .then((resp) => {
                    this.setState({
                        data: resp
                    })
                })
        }

        if (this.props.data[0] == "F") {

        }
    }

    render() {
        let label;
        let iconClass = "fal fa-cog fa-spin";
        if (this.state.data) {
            iconClass = "fal fa-code-branch";
            if (this.props.data[0] == "F") {
                iconClass = "fal fa-list";
            }
            if (this.props.data[0] == "I") label = __(this.state.data.name);
            if (this.props.data[0] == "F") label = this.props.data[1];
        }


        return (
            <div className="iw-list-edit-item">
                <ewars.d.Layout>
                    <ewars.d.Row height="30px">
                        <ewars.d.Cell style={{background: "rgba(0,0,0,0.1)", display: "block"}}>
                            <ewars.d.ActionGroup
                                right={true}
                                onAction={this._action}
                                actions={ROW_ACTIONS}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                    <ewars.d.Row>
                        <ewars.d.Cell style={{padding: "8px"}} width="20px">
                            <i className={iconClass}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: "8px"}}>
                            {label}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
}

class Sources extends React.Component {
    constructor(props) {
        super(props);
    }

    _onSelect = (data) => {
        let sources = this.props.value || [];
        sources.push(data);
        this.props.onChange(this.props.name, sources);
    };

    render() {
        return (
            <div>
                <DataSourceSelect
                    onSelect={this._onSelect}/>
                <div className="iw-edit-list">
                    <div className="iw-edit-list-items" onClick={this._clearSelected}>
                        {(this.props.value || []).map((item) => {
                            return <Source data={item}/>
                        })}
                    </div>
                    <div className="iw-edit-list-controls">
                        <ewars.d.ActionGroup
                            actions={ACTIONS}
                            height="30px"
                            onAction={this._onAction}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Sources;
