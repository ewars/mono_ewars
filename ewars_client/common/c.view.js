class View extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="view">
                {this.props.children}
            </div>
        )
    }
}

export default View;