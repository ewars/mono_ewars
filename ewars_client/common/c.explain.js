import Button from "./ButtonComponent.react";

var Explain = React.createClass({
    getDefaultProps: function () {
        return {
            content: {
                en: "No guidance provided"
            }
        }
    },

    getInitialState: function () {
        return {
            showExplain: false,
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
        }
    },

    componentWillUnmount: function () {
    },

    onClick: function () {
        this.refs.explanation.getDOMNode().style.display = "block";

        var btnRect = this.refs.expBtn.getDOMNode().getBoundingClientRect();
        var windowBounds = document.body.getBoundingClientRect();
        var elemRect = this.refs.explanation.getDOMNode().getBoundingClientRect();

        // Figure out if it should be displayed top or bottom
        var position = "top";
        if (btnRect.top < elemRect.height) position = "bottom";

        var elem = this.refs.explanation.getDOMNode();
        if (position == "top") {
            elem.style.top = -(elemRect.height + 10) + "px";
            elem.style.left = -(elemRect.width / 2) + "px";
        }


        this.setState({
            showExplain: this.state.showExplain ? false : true
        })
    },

    render: function () {
        var style = {display: "none"};
        if (this.state.showExplain) style.display = "block";

        var explain = ewars.formatters.I18N_FORMATTER(this.props.content);
        var title = ewars.formatters.I18N_FORMATTER(this.props.title);

        var iconClass = "fa-question";
        if (this.state.showExplain) iconClass = "fa-times";

        return (
            <div className="explain">
                <div className="explanation" style={style} ref="explanation">
                    <div className="explain-title">{title}</div>
                    <div className="content">{explain}</div>
                </div>
                <ewars.d.Button
                    ref="expBtn"
                    onClick={this.onClick}
                    icon={iconClass}/>
            </div>
        )
    }
});

export default Explain;
