import DataTable from "./datatable/c.datatable";

var COLUMNS = [
    {
        name: "data_date",
        label: "Report Date",
        width: 80,
        fmt: function (value) {
            return ewars.DATE(value, "DAY")
        }
    },
    {
        name: "location_name",
        label: "Location",
        width: 250,
        fmt: function (value) {
            return ewars.I18N(value)
        }
    },
    {
        name: "submitted_date",
        label: "Submitted Date",
        width: 100,
        fmt: function (value) {
            return ewars.DATE(value, "DAY")
        }
    },
    {
        name: "creator_name",
        filterKey: "created_by",
        label: "Submitted",
        width: 150
    },
    {
        name: "created",
        label: "Created",
        width: 100,
        fmt: ewars.DATE
    },
    {
        name: "form_id",
        label: "Form ID",
        width: 100
    },
    {
        name: "form_version_id",
        label: "Form Version ID",
        width: 300
    },
    {
        name: "source",
        label: "Source",
        width: 100
    },
    {
        name: "status",
        label: "Status",
        width: 100
    },
    {
        name: "uuid",
        label: "UUID",
        width: 300
    }
];

class ReportBrowser extends React.Component {
    static defaultProps = {
        form_id: null,
        filter: null,
        editor: false,
        actions: [],
        id: null,
        onAction: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {};

        this._reload(props.form_id);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.form_id != this.props.form_id) {
            this._reload(nextProps.form_id);
        }
    }

    _reload = (form_id) => {
        ewars.tx("com.ewars.form.definition", [form_id])
            .then((resp) => {
                this.setState({
                    definition: resp
                })
            })
    };

    render() {
        if (!this.state.definition) {
            return (
                <div className="placeholder">Loading definition</div>
            )
        }

        let grid;
        if (this.state.definition.grid) {
            grid = ewars.copy(this.state.definition.grid);

            grid.columns.forEach((col) => {
                switch (col.name) {
                    case "submitted_date":
                        col.fmt = (val) => {
                            return ewars.DATE(val, "YYYY-MM-DD HH:mm")
                        };
                        break;
                    case "created":
                        col.fmt = (val) => {
                            ewars.DATE(val, "YYYY-MM-DD HH:mm");
                        };
                        break;
                    case "data_date":
                        col.fmt = (val) => {
                            return ewars.DATE(val, this.state.definition.interval);
                        };
                        break;
                    default:
                        break;
                }
            })
        }


        return (
            <div>
                <DataTable
                    header_actions={[]}
                    grid={grid}
                    columns={COLUMNS}
                    isReports={true}
                    id={this.props.id}
                    filter={this.props.filter}
                    initialOrder={{"submitted_date": "DESC"}}
                    formId={this.props.form_id}/>
            </div>
        )
    }
}

export default ReportBrowser;
