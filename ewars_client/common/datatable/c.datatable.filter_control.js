import Button from "../c.button";
import {
    SelectField,
    NumericField as NumberField,
    DateField,
    TextField,
    LocationField,
    LocationGroupField
} from "../fields/";

const CONDITION_OPTIONS = [
    ["EQ", "Is equal to"],
    ["NEQ", "Is not equal to"],
    ["NULL", "Is empty"],
    ["NOT_NULL", "Is not empty"],
    ["STARTS_WITH", "Begins with"],
    ["ENDS_WITH", "Ends with"],
    ["GT", "Is greater than"],
    ["GTE", "Is greater than or equal to"],
    ["LT", "Is less than"],
    ["LTE", "Is less than or equal to"],
    ["IN", "Is between"],
    ["NIN", "Is not between"],
    ["WITHIN", "Is within"],
    ["NWITHIN", "Is not within"],
    ["UNDER", "Under"],
    ["AT+UNDER", "At + under"],
    ["CONTAINS", "Contains"]
];

var FIELDS = {
    filter_condition: {
        type: "select",
        options: []
    },
    number_field: {}
};

const TYPE_MAPS = {
    "user": ["EQ", "NEQ"],
    "number": ["NULL", "NOT_NULL", "EQ", "NEQ", "GT", "GTE", "LT", "LTE", "IN", "NIN"],
    "text": ["EQ", "NEQ", "NULL", "NOT_NULL", "CONTAINS", "STARTS_WITH", "ENDS_WITH"],
    "textarea": ["EQ", "NEQ", "NULL", "NOT_NULL", "CONTAINS", "STARTS_WITH", "ENDS_WITH"],
    "date": ["NULL", "NOT_NULL", "EQ", "NEQ", "GT", "GTE", "LT", "LTE", "IN", "NIN"],
    "location": ["EQ", "UNDER"],
    "select": ["NULL", "NOT_NULL", "EQ", "NEQ"],
    "LOCGROUPS": ["EQ", "NEQ", "IN", "NIN"]
};

class User extends React.Component {
    constructor(props) {
        super(props)
    }

    _select = () => {
        this.props.onSelect(this.props.data);
    };

    render() {

        let className = "fal fa-square";
        if (this.props.value) {
            if (this.props.value.indexOf(this.props.data.id) >= 0) className = "fal fa-check-square"
        }

        return (
            <div className="block">
                <div className="block-content" style={{padding: 0}} onClick={this._select}>
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 18, padding: 8}}>
                            <i className={className}></i>
                        </div>
                        <div className="ide-col" style={{padding: 8}}>
                            {this.props.data.name}
                            <div style={{height: 8}}></div>
                            [{this.props.data.email}]
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class UserFilter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: "",
            data: []
        }
    }

    query = (term) => {
        ewars.tx("com.ewars.query", ["sso", ['id', 'name', 'email'], {name: {like: term}}, null, null, null, null])
            .then(function (resp) {
                this.setState({
                    data: resp
                })
            }.bind(this))
    };

    _onSearchChange = (e) => {
        if (e.target.value.length > 2) {
            this.setState({search: e.target.value});
            this.query(this.state.search)
        } else {
            this.setState({search: e.target.value, data: []})
        }
    };

    _onSelect = (user) => {
        let data = this.props.value;
        if (!data) data = [];

        if (data.indexOf(user.id) < 0) {
            data.push(user.id);
            this.props.onUpdate(this.props.name, data);
        } else {
            let place = data.indexOf(user.id);
            data.splice(place, 1);
            this.props.onUpdate(this.props.name, data);
        }
    };

    render() {
        return (
            <div>
                <div className="ide-row">
                    <div className="ide-col" style={{paddingBottom: 8}}>
                        <input type="text" value={this.state.search} onChange={this._onSearchChange}/>
                    </div>
                </div>
                {this.state.data.length > 0 ?
                    <div className="ide-row" style={{height: 200, border: "1px solid #CCC"}}>
                        <div className="ide-col">
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="block-tree" style={{padding: 0, margin: 0, textAlign: "left"}}>
                                    {this.state.data.map(function (user) {
                                        return <User data={user} onSelect={this._onSelect} value={this.props.value}/>;
                                    }.bind(this))}
                                </div>
                            </div>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

class DataTableFilterControl extends React.Component {
    static defaultProps = {
        filter: null,
        id: null,
        cell: null,
        top: 0,
        left: 0
    };

    constructor(props) {
        super(props);

        this.state = {
            filter: {},
            condition: "EQ",
            value: null,
            upper: null
        }

        let comparator = "EQ";
        let value;
        let cellName = this.props.cell.config.filterKey || this.props.cell.name;
        if (this.props.filter[cellName]) {
            // There is filtering applied
            let keys = Object.keys(this.props.filter[cellName]);
            if (keys.indexOf("type") >= 0) keys.splice(keys.indexOf("type"), 1);
            comparator = keys[0].toUpperCase();
            value = this.props.filter[cellName][keys[0]] || ""
        }

        if (!value && this.props.cell.config.type == 'date') value = new Date();

        this.state.condition = comparator;
        this.state.value = value;
    }

    componentWilReceiveProps(props) {
        let comparator = this.state.condition;
        let value = this.state.value;
        let cellName = props.cell.config.filterKey || props.cell.name;
        if (props.filter[cellName]) {
            // There is filtering applied
            let keys = Object.keys(props.filter[cellName]);
            if (keys.indexOf("type") >= 0) keys.splice(keys.indexOf("type"), 1);
            comparator = keys[0].toUpperCase();
            value = props.filter[cellName][keys[0]] || ""
        }

        if (!value && props.cell.config.type == 'date') value = new Date();

        this.state.condition = comparator;
        this.state.value = value;
    }

    _filterChange = (prop, value) => {
        this.setState({
            ...this.state,
            [prop]: value
        })
    };

    _clear = () => {
        this.setState({
            ...this.state,
            condition: null,
            value: null,
            upper: null
        });
        this.props.onSave(null);
    };

    _onSave = () => {
        if (!this.state.condition) {
            this.props.onSave(null);
            return;
        }

        let filter = {};
        switch (this.state.condition) {
            case "EQ":
                filter.eq = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case "NEQ":
                filter.neq = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case "GT":
                filter.gt = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case "GTE":
                filter.gte = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case "LT":
                filter.lt = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case "LTE":
                filter.lte = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case "UNDER":
                filter.under = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case "AT+UNDER":
                filter.within = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            case 'CONTAINS':
                filter.contains = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
            default:
                filter.eq = this.state.value;
                filter.type = this.props.cell.config.type || null;
                break;
        }


        this.props.onSave(filter);
    };

    render() {
        let Control;
        if (this.props.cell.config.type == "date") Control = DateField;
        if (this.props.cell.config.type == "select") Control = SelectField;
        if (this.props.cell.config.type == "number") Control = NumberField;
        if (this.props.cell.config.type == "location") Control = LocationField;
        if (this.props.cell.config.type == "text") Control = TextField;
        if (this.props.cell.config.type == "textarea") Control = TextField;

        if (this.props.cell.config.type == "user") Control = UserFilter;
        if (this.props.cell.config.type == "LOCGROUPS") Control = LocationGroupField;


        let SecondControl;
        if (["NIN", "IN", "WITHIN", "NWITHIN"].indexOf(this.state.condition) >= 0) {
            SecondControl = Control;
        }

        let options;
        if (TYPE_MAPS[this.props.cell.config.type]) {
            options = [];
            CONDITION_OPTIONS.forEach(function (item) {
                if (TYPE_MAPS[this.props.cell.config.type].indexOf(item[0]) >= 0) {
                    options.push(item);
                }
            }.bind(this));
            FIELDS.filter_condition.options = options;
        }

        let controlConfig = ewars.copy(this.props.cell.config);
        if (this.props.cell.config.type == "location") {
            if (window.user.user_type != "SUPER_ADMIN") {
                controlConfig.parentId = window.user.clid;
                if (window.user.user_type == "REGIONAL_ADMIN") {
                    controlConfig.parentId = window.user.location_id;
                }
            }
        }

        let hasFilter = false;
        let cellName = this.props.cell.config.filterKey || this.props.cell.name;

        return (
            <div className="dt-filter-controls">
                <div className="control-action">
                    <ewars.d.Button
                        icon="fa-times"
                        label="Remove filter"
                        style={{
                            display: "block"
                        }}
                        onClick={this._clear}/>
                </div>
                <div className="control-set">
                    <label htmlFor="">Filter by condition:</label>
                    <SelectField
                        value={this.state.condition}
                        name="condition"
                        onUpdate={this._filterChange}
                        config={FIELDS.filter_condition}/>
                </div>
                <div className="control-set">
                    <label htmlFor="">Filter by Value</label>
                    <Control
                        onUpdate={this._filterChange}
                        name="value"
                        value={this.state.value}
                        config={controlConfig}/>
                    {SecondControl ?
                        <SecondControl
                            onUpdate={this._filterChange}
                            name="upper"
                            value={this.state.upper}
                            config={this.props.cell.config}/>
                        : null}
                    <div className="selectables">

                    </div>
                </div>
                <div className="control-set">

                    <div className="btn-group">
                        <ewars.d.Button
                            onClick={this._onSave}
                            label="Save"
                            colour="green"/>
                        <ewars.d.Button
                            onClick={this.props.onCancel}
                            label="Cancel"/>
                    </div>
                </div>
            </div>
        )
    }
}

export default DataTableFilterControl;
