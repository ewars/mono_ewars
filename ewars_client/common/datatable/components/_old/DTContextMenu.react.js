class DTContextMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shown: false
        }
    }

    hide = () => {
        this.setState({shown: false})
    };

    show = () => {
        this.setState({shown: true})
    };

    render() {
        return (
            <div className="dt-context-menu">

            </div>
        )
    }
}

export default DTContextMenu;