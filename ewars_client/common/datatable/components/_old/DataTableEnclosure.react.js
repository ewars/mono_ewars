
class DataTableEnclosure extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        ewars.subscribe("TABLE_SCROLL", this.setScroll);
    }

    static defaultProps = {
        type: "master"
    };

    setScroll = (direction, distance) => {

    };

    render() {
        return (
            <div className={WRAPPERS[this.props.type]}>
                <div className="dtHolder">
                    <div className="dtHider">
                        <div className="dtSpreader">
                            <table className="dtCore">
                                <colgroup>
                                    <col></col>
                                </colgroup>
                                <thead>

                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DataTableEnclosure;