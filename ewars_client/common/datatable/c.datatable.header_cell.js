import DataTableFilterControl from "./c.datatable.filter_control";

class DataTableHeaderCell extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showControl: false,
            controls: false
        };
    }

    componentWillMount() {
        window.__hack__.addEventListener("click", this._clear);
        window.__hack__.addEventListener("contextmenu", this._clear);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._clear);
        window.__hack__.removeEventListener("contextmenu", this._clear);
    }

    _clear = (evt) => {
        if (this.refs.item) {
            if (!this.refs.item.contains(evt.target)) {
                this.setState({
                    controls: false
                })
            }
        }
    };

    static defaultProps = {
        sortable: false,
        filterable: false
    };

    _filter = () => {
        this.setState({
            controls: !this.state.controls
        })
    };

    _sort = (e) => {
        e.preventDefault();
        let colName;
        if (this.props.data.config) {
            if (this.props.data.config.name) colName = this.props.data.config.name;
        }
        if (this.props.data.name) colName = this.props.data.name;

        if (this.props.data.config.sortKey) colName = this.props.data.config.sortKey;
        ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "UPDATE_SORT", {
            field: colName,
            fieldType: this.props.data.type || this.props.data.config.type
        })
    };

    _cancelFilter = () => {
        this.setState({
            controls: false
        })
    };

    _applyFilter = (value) => {
        this.setState({
            controls: false
        }, () => {
            ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "UPDATE_FILTER", {
                key: this.props.data.config.filterKey || this.props.data.name,
                config: value
            });
        });

    };

    render() {
        let label = ewars.I18N(this.props.data.config.label);
        let fieldType = this.props.data.config.type;

        let hasControls = true;
        if (["matrix", "display", "row"].indexOf(this.props.data.config.type) >= 0) hasControls = false;

        let colSpan = this.props.data.cols || 1,
            rowSpan = this.props.data.rows || 1;

        let width = (this.props.data.width || 90) + "px";

        let sortIcon;

        let colName;
        if (this.props.data.config) {
            if (this.props.data.config.name) colName = this.props.data.config.name
        }
        if (this.props.data.name) colName = this.props.data.name;

        sortIcon = <i className="fal fa-sort"></i>;
        if (this.props.sort[colName]) {
            if (this.props.sort[colName].indexOf("DESC") >= 0) sortIcon = <i className="fal fa-caret-down"></i>;
            if (this.props.sort[colName].indexOf("ASC") >= 0) sortIcon = <i className="fal fa-caret-up"></i>;
        }

        let actionControlClass = "ide-col dt-filter-btn";
        if (this.props.filter[this.props.data.config.filterKey || this.props.data.name]) actionControlClass += " active";

        let filterIconClass = "fal fa-filter";
        if (this.props.filter[this.props.data.config.filterKey || this.props.data.name]) filterIconClass += " green";

        let hasSort = true;
        let style = {position: "relative"};
        if (['row', 'matrix'].indexOf(this.props.data.config.type) >= 0) {
            hasSort = false;
            style.background = "rgba(0,0,0,0.1)"
        }

        return (
            <th ref="cell" rowSpan={rowSpan} colSpan={colSpan} className="dt-header-cell"
                style={style}>
                <div className="ide-row" style={{position: "relative", height: "100%", minHeight: "25px"}}>
                    <div className="ide-col" style={{padding: "5px 10px"}}>
                        {label}
                    </div>
                    {hasSort ?
                        <div className="ide-col" style={{maxWidth: 20, padding: "5px", cursor: "pointer"}}
                             onClick={this._sort}>
                            {sortIcon}
                        </div>
                        : null}
                    {hasControls ?
                        <div className="ide-col " style={{maxWidth: 20, padding: 5, cursor: "pointer"}}
                             onClick={this._filter}>
                            <i className={filterIconClass}></i>
                        </div>
                        : null}

                </div>
                {this.state.controls ?
                    <div style={{position: "absolute", top: 22, left: 0, zIndex: 1}} ref="item">
                        <DataTableFilterControl
                            id={this.props.id}
                            filter={this.props.filter}
                            onCancel={this._cancelFilter}
                            onSave={this._applyFilter}
                            cell={this.props.data}/>
                    </div>
                    : null}
            </th>
        )
    }

}

export default DataTableHeaderCell;
