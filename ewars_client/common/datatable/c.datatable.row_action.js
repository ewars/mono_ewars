class RowAction extends React.Component {
    constructor(props) {
        super(props);
    }

    onAction = (e) => {
        this.props.onAction(this.props.data);
    };

    render() {
        let label = ewars.I18N(this.props.data.label);
        let icon;
        if (this.props.data.icon) icon = `fal ${this.props.data.icon}`;
        return (
            <div className="dtAction" onClick={this.onAction}>
                <div className="ide-row">
                    {icon ?
                        <div className="ide-col border-right" style={{padding: '2px 8px', maxWidth: 25}}>
                            <i className={icon}></i>
                        </div>
                        : null}
                    <div className="ide-col" style={{padding: '2px 8px'}}>{label}</div>
                </div>
            </div>
        )
    }
}

export default RowAction;