import {
    DateField,
    MatrixField,
    SelectField,
    TextAreaField,
    TextField as TextInputField,
    DisplayField,
    ConditionsField,
    RowField,
    TopoJSONField,
    LocationInputField,
    LocationField as LocationSelectField,
    LanguageStringField,
    NumericField as NumberField,
    SlugField,
    SwitchField,
    IndicatorField as IndicatorSelector,
    AdministrationField,
    PointField,
    GeometryField as MapGeometryField,
    SelectFieldOptionsField,
    HeaderField,
    AssignmentField as AssignmentLocationField,
    IndicatorDefinition
} from "../fields";

const FieldMapper = {
    _typeMap: {
        select: SelectField,
        text: TextInputField,
        textarea: TextAreaField,
        date: DateField,
        email: TextInputField,
        matrix: MatrixField,
        number: NumberField,
        point: PointField,
        display: DisplayField,
        conditions: ConditionsField,
        row: RowField,
        topojson: TopoJSONField,
        location: LocationSelectField,
        language_string: LanguageStringField,
        slug: SlugField,
        switch: SwitchField,
        indicator: IndicatorSelector,
        administration: AdministrationField,
        geometry: MapGeometryField,
        select_options: SelectFieldOptionsField,
        header: HeaderField,
        assignment_location: AssignmentLocationField,
        indicator_definition: IndicatorDefinition
    },

    _optionsList: [
        ['date', 'Date'],
        ['display', 'Display'],
        ["header", "Header"],
        ["number", "Number"],
        ['select', 'Select Field'],
        ['text', 'Text Field'],
        ['textarea', 'Text Area'],
        ['matrix', 'Matrix'],
        ['row', 'Row'],
        ['location', 'Location Field'],
        ["point", "Location Point (lat/lng)"],
        ["switch", "Switch Field"]
    ],

    _types: {
        "color": "Color",
        "checkbox": "Checkbox",
        "date": "Date",
        "display": "Display",
        "header": "Header",
        "number": "Number",
        "select": "Select Field",
        "text": "Text",
        "textarea": "Text Area",
        "time": "Time",
        "matrix": "Matrix",
        "row": "Row",
        "topojson": "Topo JSON",
        "location": "Location Field",
        "point": "Location Point (lat/lng)",
        "language_string": "Language String Field",
        "slug": "Slug Field",
        "switch": "Switch Field"
    }

};

export default FieldMapper;
