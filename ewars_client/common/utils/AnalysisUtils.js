var PROTOTYPES = {
    SLICE: {
        type: "SLICE",
        uuid: null,
        indicator: null,
        location: null,
        start_date: null,
        end_date: null,
        reduction: null
    },
    SLICE_COMPLEX: {
        type: "SLICE_COMPLEX",
        uuid: null,
        location: null,
        start_date: null,
        end_date: null,
        formula: null,
        series: []
    },
    SERIES_COMPLEX: {
        type: "SERIES_COMPLEX",
        uuid: null,
        location: null,
        interval: null,
        start_date: null,
        end_date: null,
        formula: null,
        series: []
    },
    SERIES: {
        type: "SERIES",
        uuid: null,
        location: null,
        interval: null,
        indicator: null,
        start_date: null,
        end_date: null
    },
    MATRIX: {
        type: "TABLE",
        uuid: null,
        location: null,
        columns: []
    },
    THRESHOLD: {
        type: "THRESHOLD",
        uuid: null,
        indicator: null,
        location: null,
        start_date: null,
        end_date: null,
        interval: null
    }
};

var SOURCES_COMPLEX = {
    SLICE_COMPLEX: {
        uuid: null,
        indicator: null,
        variable_name: null,
        reduction: null
    },
    SERIES_COMPLEX: {
        uuid: null,
        indicator: null,
        variable_name: null
    }
};

var TYPE_MAPS = {
    INDICATOR: "SLICE",
    SLICE: "SLICE",
    SLICE_COMPLEX: "SLICE_COMPLEX",
    COMPLEX: "COMPLEX",
    SERIES: "SERIES",
    TABLE: "TABLE",
    SERIES_COMPLEX: "SERIES_COMPLEX",
    THRESHOLD: "THRESHOLD"
};

function _parseType(definition) {
    if (definition.type == "TABLE") return "MATRIX";
    if (definition.source_type) {
        return TYPE_MAPS[definition.source_type];
    }

    if (definition.dataType == "SERIES") return "SERIES";
    if (["line", "bar", "area"].indexOf(definition.type) >= 0 && definition.dataType == "COMPLEX") {
        return "SERIES_COMPLEX";
    }

    if (definition.dataType) {
        return TYPE_MAPS[definition.dataType];
    }

    if (definition.type == "RAW") {
        // Need to figure out if it's a SLICE_COIMPLEX
        if (definition.series[0].series && definition.series[0].series.length > 0) {
            return "SLICE_COMPLEX";
        } else {
            return "SLICE"
        }
    }
}

function _or() {
    var result = null;

    _.each(arguments, function (arg) {
        if (arg && arg != undefined) result = arg;
    });

    return result;
}


/**
 * Normalizes legacy definitions to the new query type
 * @param source {obj} The definition for the query
 * @param additional {obj} Additional parameters passed to the builder
 */
function buildQuery(source, additional) {
    var type = _parseType(source);

    var query = _.extend({}, PROTOTYPES[type]);

    query.uuid = additional.uuid || ewars.utils.uuid();

    if (source.type == "RAW") {
        source = _.extend(source, source.series[0]);
    }


    if ("location" in query) {
        if (source.loc_spec == "SPECIFIC") {
            query.location = _or(source.location, source.locationUUID, source.location_id);
        } else if (source.loc_spec == "REPORT_LOCATION") {
            query.location = additional.location;
        } else if (source.loc_spec == "GENERATOR") {
            query.location = {
                site_type_id: source.generator_locations_type,
                parent_id: source.generator_locations_parent,
                status: _or(source.generator_locations_status, source.generator_location_status)
            }
        } else {
            query.location = _or(source.location, source.locationUUID, source.location_id, additional.location);
        }
    }

    // Fix interval
    if ("interval" in query) query.interval = _or(source.target_interval, source.interval, source.timeInterval, source.time_interval, additional.interval);

    // Fix indicator
    if ("indicator" in query) query.indicator = _or(source.indicatorUUID, source.target_indicator, source.indicator, source.indicator_id, additional.indicator);

    // Fix dates
    if ("start_date" in query) {
        var optStart = source.options ? source.options.start_date : null;
        query.start_date = _or(source.start_date, optStart, additional.start_date);
    }

    if ("end_date" in query) {
        var optEnd = source.options ? source.options.end_date : null;
        query.end_date = _or(source.end_date, optEnd, additional.end_date);
    }

    // Fix formula
    if ("formula" in query) query.formula = _or(source.formula, additional.formula);

    // Fix reduction
    if ("reduction" in query) query.reduction = _or(additional.reduction, source.reduction) || "NONE";

    if ("template_id" in additional) query.template_id = additional.template_id;

    if ("series" in query) query.series = _or(source.series, source.sources, additional.series);

    if ("columns" in query) query.columns = _or(source.columns, additional.columns);

    if (query.type == "THRESHOLD") query.options = source.options;

    return query;
}

module.exports = {
    buildQuery: buildQuery
};