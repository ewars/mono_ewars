function _convertFormToOptions(definition, path, lLabel) {
    var options = [];

    for (let key in definition) {
        let field = definition[key];
        let fieldName = ewars.formatters.I18N_FORMATTER(field.label);
        let realName = fieldName;
        if (lLabel) realName = `${lLabel.join(" \\ ")} \\ ${fieldName}`;
        let fieldPath = `${key}`;
        if (path) fieldPath = `${path}.${fieldPath}`;
        options.push([key, realName, {...field, path: fieldPath}]);

        if (field.fields) {
            let subLabel = [fieldName];
            if (lLabel) {
                subLabel = ewars.copy(lLabel);
                subLabel.push(fieldName);
            }

            let subPath;
            if (path) {
                subPath = path + "." + key;
            } else {
                subPath = key;
            }
            let subFields = _convertFormToOptions(field.fields, fieldPath, subLabel);
            options.push.apply(options, subFields);
        }
    }

    return options;
}

/**
 * Converts a form definition to a tree
 * @param definition
 * @param place
 * @param placeName
 * @returns {Array}
 * @private
 */
function _convertFormToTree(definition, place, placeName) {
    var items = [];

    var asArray = _.map(definition, function (field, fieldName) {
        var newField = _.extend({}, field, {name: fieldName});
        return newField;
    }, this);

    var asSorted = _.sortBy(asArray, function (item) {
        return item.order;
    }, true);

    _.each(asSorted, function (field) {
        if (field.fields) {
            var path;
            if (!place) path = field.name;
            if (place) path = place + "/" + field.name;

            var pathName;
            if (!placeName) pathName = ewars.formatters.I18N_FORMATTER(field.label);
            if (placeName) pathName = placeName + " / " + ewars.formatters.I18N_FORMATTER(field.label);

            var children = _convertFormToTree(field.fields, path, pathName);

            items.push({
                label: ewars.formatters.I18N_FORMATTER(field.label),
                pathLabel: pathName,
                name: field.name,
                path: path,
                field: field,
                nodeType: field.type,
                children: children
            })
        } else {
            var path;
            if (!place) path = field.name;
            if (place) path = place + "/" + field.name;

            var pathName;
            if (!placeName) pathName = ewars.formatters.I18N_FORMATTER(field.label);
            if (placeName) pathName = placeName + " / " + ewars.formatters.I18N_FORMATTER(field.label);

            items.push({
                label: ewars.formatters.I18N_FORMATTER(field.label),
                name: field.name,
                pathLabel: pathName,
                path: path,
                field: field,
                nodeType: field.type
            })
        }
    }, this);

    return items;
}

function _findField(definition, fieldName) {
    var current = definition;

    if (definition[fieldName]) {
        current = definition[fieldName];
    }
}

/**
 * Retrieve a field from within the definition
 * @param definition
 * @param path
 * @returns {*}
 * @private
 */
function _getField(definition, specPath) {
    let item = definition;

    let path = specPath.split(".");
    if (path.indexOf("data.") >= 0) path.shift();

    path.forEach(function (pathItem) {
        if (item[pathItem]) {
            if (item[pathItem].fields) {
                item = item[pathItem].fields;
            } else {
                item = item[pathItem];
            }
        }
    }.bind(this));

    return item;
}


function _getFieldProperty(definition, field, prop) {
    if (field.indexOf(".") >= 0) {
        let splat = field.split(".");

        let defn = definition;
        let splatLen = splat.length;
        for (var i in splat) {
            if (defn[i]) {
                if (defn[i].fields) {
                    defn = defn[i].fields;
                } else {
                    defn = defn[i];
                }
            } else {

            }
        }
        return defn[prop];
    } else {
        return definition[field][prop] || null;
    }
}

const _getRuleResult = (rule, data) => {
    var result;

    var sourceValue = ewars.getKeyPath(rule[0], data);
    if (sourceValue == null || sourceValue == undefined) return false;

    switch (rule[1]) {
        case "eq":
            if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                if (sourceValue.indexOf(rule[2]) >= 0) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = sourceValue == rule[2];
            }
            break;
        case "ne":
        case "neq":
            if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                if (sourceValue.indexOf(rule[2]) < 0) {
                    result = true;
                } else {
                    result = false;
                }
            } else {
                result = sourceValue != rule[2];
            }
            break;
        case "gt":
            result = sourceValue > rule[2];
            break;
        case "lt":
            result = sourceValue < rule[2];
            break;
        case 'lte':
            result = sourceValue <= rule[2];
            break;
        case 'gte':
            result = sourceValue >= rule[2];
            break;
        default:
            result = false;
            break
    }

    return result;
};

/**
 * Check if a field is visible if it isnt' we shouldn'y
 * try to vlaidate it
 * @param data The data in the system
 * @param fieldDef The definition for the field
 * @param fieldKey
 * @private
 */
function _isVisible(data, field, fieldKey) {
    if (!field.conditional_bool) return true;

    if (field.conditional_bool == true) {
        let result = false;

        if (!field.conditions) return true;
        if (!field.conditions.rules) return true;

        if (["ANY", "any"].indexOf(field.conditions.application) >= 0) {
            // Only one of the rules has to pass

            for (let conIdx in field.conditions.rules) {
                let rule = field.conditions.rules[conIdx];

                var tmpResult = _getRuleResult(rule, data);
                if (result != true && tmpResult == true) result = true;
            }

        } else {
            let ruleCount = field.conditions.rules.length;
            let rulePassCount = 0;

            for (let ruleIdx in field.conditions.rules) {
                let rule = field.conditions.rules[ruleIdx];
                let ruleResult = _getRuleResult(rule, data);
                if (ruleResult) rulePassCount++;
            }

            if (ruleCount == rulePassCount) result = true;

        }
        return result;
    } else {
        return true;
    }

}

/**
 * Instantiate an object with the defaults from a form definition
 * @param definition
 * @param flattened
 * @returns {{}}
 */
function setFormDefaults(definition, flattened) {
    let data = {};

    return data;
}

function isSet(value) {
    if (value == null) return false;
    if (value == undefined) return false;
    if (value == "") return false;
    if (value == "null") return false;
    if (value == "NULL") return false;
    if (value == "None") return false;

    return true
}

const NON_VALIDATE = ['matrix', 'row', 'header', 'display'];

function _validate(definition, data, flattened) {
    let errors = {};
    let hasErrors = false;

    for (var i in definition) {
        let field = definition[i];

        let value;
        if (flattened) {
            value = data[i] || null;
        } else {
            value = ewars.getKeyPath(i, data);
        }

        errors[i] = [];

        let dotCount = (i.match(/\./g) || []).length;

        let isVisible = true;
        if (dotCount > 0 && NON_VALIDATE.indexOf(field.type) < 0) {
            // This is a non-root field, we need to evaluate whether it's parents are
            // visible before evaluating whether the actual field is
            let key;
            let fieldTree = [];
            i.split(".").forEach(item => {
                if (!key) {
                    key = item;
                } else {
                    key += "." + item;
                }

                fieldTree.push(key);
            })
            fieldTree.pop();

            fieldTree.forEach(item => {
                let _vis = _isVisible(data, definition[item], item);
                if (!_vis) isVisible = false;
            })
        } else {
            isVisible = _isVisible(data, field, i);
        }

        if (isVisible && NON_VALIDATE.indexOf(field.type) < 0) {
            if (field.required && NON_VALIDATE.indexOf(field.type) < 0) {
                if (!isSet(value)) {
                    hasErrors = true;
                    errors[i].push(ewars.I18N(field.label) + " is a required field");
                } else {
                    let typeErrors = [];
                    switch (field.type) {
                        case "number":
                            typeErrors = _validateNumber(field, value);
                            break;
                        case "text":
                            typeErrors = _validateText(field, value);
                            break;
                        case "select":
                            typeErrors = _validateSelect(field, value);
                            break;
                        case "date":
                            typeErrors = _validateDate(field, value);
                            break;
                        case "location":
                            typeErrors = _validateLocation(field, value);
                            break;
                        default:
                            break;
                    }

                    if (typeErrors.length > 0) hasErrors = true;
                    errors[i].push.apply(errors[i], typeErrors);
                }
            }
        }
    }

    for (var f in errors) {
        if (errors[f].length <= 0) delete errors[f];
    }

    return {hasErrors, errors};
}

function _validateNumber(def, value) {
    let errors = [];

    return errors;
}

function _validateText(def, value) {
    return [];
}

function _validateSelect(def, value) {
    return [];
}

function _validateDate(def, value) {
    return [];
}

function _validateLocation(def, value) {
    return [];
}


function _getFieldsAsArray(sourceFields, parent) {
    let result = {};

    for (var i in sourceFields) {
        let field = sourceFields[i];

        let key;
        if (parent) {
            key = `${parent}.${i}`;
        } else {
            key = `${i}`;
        }

        if (field.type == "matrix") {
            let results = _getFieldsAsArray(field.fields, key);
            result[key] = field;
            for (var f in results) {
                result[f] = results[f];
            }
        } else if (field.type == "row") {
            let results = _getFieldsAsArray(field.fields, key);
            result[key] = field;
            for (var f in results) {
                result[f] = results[f];
            }
        } else {
            result[key] = field;
        }
    }

    return result;
}

export default {
    validate: _validate,
    flattenDefinition: function (definition) {

    },
    asOptions: function (definition) {
        return _convertFormToOptions(definition, null);
    },
    getField: function (definition, fieldPath) {
        return _findField(definition, fieldPath);
    },

    nest: function (data) {
        let unnested = {};

        for (var i in data) {
            let path = i.split(".");
            let last = path[path.length - 1];

            let term = unnested;

            path.forEach(item => {
                if (term[item]) {
                    term = term[item];
                } else {
                    if (item != last) {
                        term[item] = {};
                        term = term[item]
                    } else {
                        term[item] = data[i]
                    }
                }
            })

        }

        return unnested;
    },
    flatten: function (definition) {
        return _getFieldsAsArray(definition, null);
    },
    field: _getField,
    fieldProperty: _getFieldProperty,
    asTree: function (form) {
        let formItems;
        if (form.version) formItems = _convertFormToTree(form.version.definition, null, "Form");
        if (!form.version) formItems = _convertFormToTree(form, null, "Form");

        if (form.location_aware) {
            formItems.unshift({
                label: ewars.formatters.I18N_FORMATTER({
                    en: "Report Location"
                }),
                field: {
                    type: "location",
                    required: true
                },
                nodeType: "location",
                type: "location",
                name: "location_id",
                path: "form/location_id",
                pathLabel: "Form / Location"
            })
        }

        formItems.unshift({
            label: ewars.formatters.I18N_FORMATTER({
                en: "Report Date"
            }),
            type: "date",
            field: {
                type: "date",
                required: true
            },
            nodeType: "date",
            name: "data_date",
            path: "form/data_date",
            pathLabel: "Form / Report Date"
        });

        return [{
            nodeType: "form",
            label: "Form",
            name: "form",
            field: {
                type: "form"
            },
            type: "form",
            children: formItems
        }];
    }
};
