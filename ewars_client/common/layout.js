class Cell extends React.Component {
    static defaultProps = {
        width: null,
        borderTop: false,
        borderRight: false,
        borderBottom: false,
        borderLeft: false,
        addClass: null,
        padding: null,
        onClick: null,
        style: {}
    };

    constructor(props) {
        super(props);
    }

    _onClick = () => {
        if (this.props.onClick) this.props.onClick();
    };

    render() {
        let style = {};

        style.maxWidth = this.props.width || null;
        style.padding = this.props.padding || null;

        let className = "ide-col";
        className += this.props.borderRight ? " border-right" : "";
        className += this.props.borderBottom ? " border-bottom" : "";
        className += this.props.borderLeft ? " border-left" : "";
        className += this.props.borderTop ? " border-top" : "";
        className += this.props.addClass ? ` ${this.props.addClass}` : "";

        if (this.props.onClick) style.cursor = "pointer";

        Object.assign(style, this.props.style || {});

        return (
            <div className={className} style={style} onClick={this._onClick}>
                {this.props.children}
            </div>
        )
    }
}

class Row extends React.Component {
    static defaultProps = {
        height: null,
        borderTop: false,
        borderBottom: false,
        borderRight: false,
        borderLeft: false,
        style: null,
        addClass: null,
        onClick: null
    };

    constructor(props) {
        super(props);
    }

    render() {
        let className = "ide-row";
        if (this.props.addClass) className += " " + this.props.addClass;
        let style = {};

        style.maxHeight = this.props.height || null;

        if (this.props.style) Object.assign(style, this.props.style);

        let onClick = this.props.onClick || null;

        return (
            <div className={className} style={style} onClick={onClick}>
                {this.props.children}
            </div>
        )
    }
}

class Layout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let style = {};

        if (this.props.style) Object.assign(style, this.props.style);
        return (
            <div className="ide-layout" style={style}>
                {this.props.children}
            </div>
        )
    }
}

class Panel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let style = {};
        if (this.props.style) {
            for (let i in this.props.style) {
                style[i] = this.props.style[i];
            }
        }
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll" style={style}>
                {this.props.children}
            </div>
        )
    }
}

export {
    Layout,
    Row,
    Cell,
    Panel
};
