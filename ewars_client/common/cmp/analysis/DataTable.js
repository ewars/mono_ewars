/* eslint-disable no-console,func-names,react/no-multi-comp */
import PRESET_NAMES from '../../editors/plot/const.names';

const STYLES = {
    ROW: {
        display: "flex",
        flexDirection: "row"
    },
    COL: {
        display: "flex",
        flex: 1,
        flexDirection: "column",
        minWidth: "20px"
    },
    COL_VAL: {
        display: "block",
        wordWrap: "nowrap",
        whiteSpace: "nowrap",
        padding: "8px",
        color: "black"
    },
    LEAD: {
        borderBottom: "1px solid rgba(0,0,0,0.1)"
    },
    WRAPPER: {
        overflowX: "scroll",
        overflowY: "scroll",
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: "100%",
        height: "100%"
    }
}

const MODES = {
    VERT: 1,
    HORIZ: 0
}

function cartesian() {
    var r = [], args = Array.from(arguments);
    args.reduceRight(function (cont, factor, i) {
        return function (arr) {
            for (var j = 0, l = factor.length; j < l; j++) {
                var a = arr.slice(); // clone arr
                a[i] = factor[j];
                cont(a);
            }
        };
    }, Array.prototype.push.bind(r))(new Array(args.length));
    return r;
}


class Data {
    constructor(data, xD, xM, yD, yM) {
        this.data = data.d;
        this.xDimensions = xD || [];
        this.xMeasures = xM || [];
        this.yDimensions = yD || [];
        this.yMeasures = yM || [];

        this._headers = [];

        this._matrix = [];

        this.xDimensions.forEach(node => {
            let row = [];
            // Collect values
            data.d.forEach(d => {
                let dimIndex = d.i.idx[node[0]];
                d.d.forEach(val => {
                    row.push(val[dimIndex]);
                })
            })
            this._matrix.push(row);
        });


        let xCodes = this.xMeasures(node => {return node[0]});
        this._matrix.push(xCodes);

        this.xMeasures.forEach(node => {

        })


        if (this.xMeasures.length <= 0) {

        } else if (this.xDimensions.length == 1) {

        } else {

        }

        if (this.yDimensions.length <= 0) {

        } else if (this.yDimensions.length == 1) {

        } else {

        }

        if (this.yMeasures.length <= 1) {

        } else if (this.yMeasures.length == 1) {

        } else {

        }

    }

    _old_constructor(data, xD, xM, yD, yM) {
        this.data = data.d;
        this.xDimensions = xD || [];
        this.xMeasures = xM || [];
        this.yDimensions = yD || [];
        this.yMeasures = yM || [];

        this.headers = this.data.slice(0, 1)[0];
        this.data = data.d.slice(1, data.d.length);

        this._data = {};

        this._lp = {};
        this.headers.forEach((item, index) => {
            this._lp[item.replace(/_/g, "-").substr(1)] = index;
        })

        // Map out distinct values
        this._dimValues = {};
        this._yCart = [];
        this._xCart = [];
        this._yValues = this.yMeasures.map(node => {
            return node[0];
        });
        this._xValues = this.xMeasures.map(node => {
            return node[0];
        });

        let dimIndices = [];

        [this.xDimensions, this.yDimensions].forEach(node => {
            node.forEach(dim => {
                let values = [];
                let lp = this._lp[dim[0]];
                dimIndices.push(lp);
                this.data.forEach(item => {
                    values.push(item[lp]);
                })
                this._dimValues[dim[0]] = values.filter((elem, index, self) => {
                    return index == self.indexOf(elem);
                })
            })
        });

        // Create cartesians for y and x

        if (this.yDimensions.length > 0) {
            let cart = null;
            yD.forEach(dim => {
                let values = this._dimValues[dim[0]];
                if (cart) {
                    cart = cartesian(cart, values);
                } else {
                    cart = values.map(item => {
                        return [item];
                    });
                }
            })
            this._yCart = cart;
        }

        if (this.xDimensions.length > 0) {
            let cart = null;
            xD.forEach(dim => {
                let values = this._dimValues[dim[0]];
                if (cart) {
                    cart = cartesian(cart, values)
                } else {
                    cart = values.map(item => {
                        return [item];
                    });

                }
            })
            this._xCart = cart;
        }

        let measIndices = [];

        this.xMeasures.forEach(node => {
            measIndices.push(this._lp[node[0]]);
        })
        this.yMeasures.forEach(node => {
            measIndices.push(this._lp[node[0]]);
        })

        this._hash = {};

        // hash is xDim1.xDim2.XDim3|yDim1,yDim2,yDim3: {meas1: <>, meas2: <>}
        this.data.forEach(node => {
            let xDims = [];
            let yDims = [];
            let hash = "";
            let value = {};

            this.xDimensions.forEach(dim => {
                xDims.push(node[this._lp[dim[0]]]);
            })
            this.yDimensions.forEach(dim => {
                yDims.push(node[this._lp[dim[0]]]);
            })

            hash = [xDims.join("."), "|", yDims.join(".")].join("");

            measIndices.forEach(meas => {
                value[this.headers[meas].replace(/_/g, "-").substr(1)] = node[meas]
            })

            this._hash[hash] = value;
        })

    }

    getHeaders = () => {
        let cols = [];
        if (this._xValues.length <= 0) {
            // we;re plotting the y values
            this._xCart.forEach(item => {
                let col = {};
                col.col = item;
                col.measures = this._yValues;
                col.mode = MODES.VERT;
                cols.push(col);
            })
        } else {

        }

        return cols;
    };

    getRowHeaders = () => {
        let rows = [];

        if (this._yCart.length <= 0) {
            // we're just plotting values
            this._yValues.forEach(item => {
                rows.push({
                    row: item,
                    mode: null
                })
            })
        } else {
            this._yCart.forEach(item => {
                rows.push({
                    row: item,
                    measures: this._yValues,
                    mode: null
                })
            })
        }


        return rows;
    };

    getLabel = (target) => {

    };

    getRow = (x, y) => {

    };

    /**
     * Get metadata about a value, for instance getting the name
     * of a location by its uuid, or a form label by its canonical path
     * @param dimension
     */
    getMetadata = (value) => {

    };

    /**
     * Get a measure value for a combination of xDimension, yDimension
     * Uses a computed hash to look up the value in the data table
     * @param xDim
     * @param yDim
     * @param measure
     */
    getData = (xDim, yDim, measure) => {
        let xCodes = xDim || [];
        let yCodes = yDim || [];
        let hash = [xCodes.join('.'), "|", yCodes.join(".")].join("");
        return this._hash[hash][measure] || 0;
    };


}

class DataTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.ds) return null;

        let data = this.props.ds.getData();
        if (!data) return null;


        let lookup = {};

        data.d[0].forEach((item, index) => {
            lookup[item.replace(/_/g, "-").substr(1)] = index;
        });

        let xMeasures = this.props.x.filter((item) => {
            return item[1] == "M";
        })


        let xDimensions = this.props.x.filter(item => {
            return item[1] == "D";
        })

        let yMeasures = this.props.y.filter(item => {
            if (item[1] == "M") return item;
        })

        let yDimensions = this.props.y.filter(item => {
            if (item[1] == "D") return item;
        })

        let dt = new Data(data, xDimensions, xMeasures, yDimensions, yMeasures);

        let rHeaders = dt.getHeaders();
        let rowHeaders = dt.getRowHeaders();
        let columns = [];

        rHeaders.forEach((head, index) => {
            let col = [];

            col.push(
                <div style={STYLES.ROW}>
                    <div style={STYLES.COL_VAL}>{head.col}</div>
                </div>
            )

            if (head.mode == MODES.VERT) {
                head.measures.forEach(meas => {
                    let value = dt.getData(head.col, null, meas);
                    col.push(
                        <div style={STYLES.ROW}>
                            <div style={STYLES.COL_VAL}>{value}</div>
                        </div>
                    )
                })
            }

            col.push(
                <div style={STYLES.ROW}>

                </div>
            )

            columns.push(
                <div style={{...STYLES.COL, flex: "none"}}>
                    {col}
                </div>
            )
        })

        let rows = [];
        rowHeaders.forEach(rH => {
            rows.push(
                <div style={STYLES.ROW}>
                    <div style={STYLES.COL_VAL}>{rH.row}</div>
                </div>
            )
        })

        return (
            <div style={STYLES.WRAPPER}>
                <div>
                    <div style={STYLES.ROW}>
                        <div style={{...STYLES.COL, flex: 1}}>
                            <div style={{...STYLES.ROW, padding: "8px"}}>&nbsp;</div>
                            {rows}
                        </div>
                        <div style={{...STYLES.COL, flex: 3, overflowY: "scroll", overflowX: "scroll"}}>
                            <div style={STYLES.ROW}>
                                {columns}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DataTable;