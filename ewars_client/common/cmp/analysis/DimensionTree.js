class FormNode extends React.Component {
    static defaultProps = {
        draggable: true,
        onClick: false,
        root: false
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false,
            data: null
        }

    }

    componentWillMount() {
        if (this.props.root) {
            ewars.tx("com.ewars.query", ["form", ["id", "name"], {"status": {eq: "ACTIVE"}}, null, null, null, null])
                .then((resp) => {
                    this.setState({
                        data: resp,
                        show: true
                    })
                })
        }
    }

    _toggle = () => {
        if (this.state.data) {
            this.setState({
                show: !this.state.show
            })
        } else {
            // this.refs.icon.className = "fal fa-cog fa-spin";
            ewars.tx("com.ewars.form.dimensions", [this.props.data.id])
                .then((resp) => {
                    let sorted = resp.sort((a, b) => {
                        if (a[0] > b[0]) return 1;
                        if (a[0] < b[0]) return -1;
                        return 0;
                    })
                    this.setState({
                        data: resp,
                        show: true
                    })
                })
        }
    };

    render() {
        if (!this.state.data && this.props.root) return <div></div>;

        if (this.props.root) {
            let items;
            items = this.state.data.map(item => {
                return <FormNode data={item}/>;
            })

            return (
                <div className="section">
                    {items}
                </div>
            )
        }


        let label, icon = this.state.show ? "fal fa-caret-down" : "fal fa-caret-right";
        if (this.props.root) label = "Form Dimensions";
        if (!this.props.root) label = __(this.props.data.name);

        let items;
        if (this.state.show) {
            items = this.state.data.map(item => {
                return <Node type="FIELD" data={item}/>
            })
        }
        return (
            <div className="node">
                <div className="node-handle" onClick={this._toggle}>
                    <ewars.d.Row>
                        <ewars.d.Cell width="20px" style={{padding: "6px 5px", textAlign: "center"}}>
                            <i className={icon}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell style={{padding: "6px 5px"}}>{label}</ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.show ?
                    <div className="node-children">
                        {items}
                    </div>
                    : null}
            </div>
        )
    }
}

class Node extends React.Component {
    static defaultProps = {
        type: null
    };

    constructor(props) {
        super(props);

        this.state = {
            selected: false
        }
    }

    _onDragStart = (e) => {
        let code, config = {};
        switch (this.props.type) {
            case "FIELD":
                code = "FIELD." + this.props.data[0] + "." + this.props.data[2];
                break;
            default:
                code = this.props.type;
                config.type = this.props.data.type || null;
                break;
        }


        ewars.emit("SHOW_DROPS", "D");
        e.dataTransfer.setData("n", JSON.stringify({
            t: 'D',
            n: code,
            c: config
        }))
    };

    _onDragEnd = () => {
        ewars.emit('HIDE_DROPS');
    };

    _toggle = (e) => {
        if (e.shiftKey) {
            this.setState({
                selected: !this.state.selected
            })
        }
    };

    render() {
        let label, icon;

        switch (this.props.type) {
            case "LOCATION":
                icon = "fal fa-map-marker";
                label = __(this.props.data.name);
                break;
            case 'LOCATION_TYPE':
                icon = 'fal fa-map-marker';
                label = this.props.data.name;
                break;
            case 'LOCATION_GROUP':
                icon = 'fal fa-map-marker';
                label = this.props.data.name;
                break;
            case 'LOCATION_STATUS':
                icon = 'fal fa-map-marker';
                label = this.props.data.name;
                break;
            case 'LOCATION_GEOM':
                icon = 'fal fa-map-marker';
                label = this.props.data.name;
                break;
            case "RECORD_DATE":
                icon = "fal fa-calendar";
                label = __(this.props.data.name);
                break;
            case "FIELD":
                icon = "fal fa-list";
                label = __(this.props.data[4]);

                if (this.props.data[3] == "date") icon = "fal fa-calendar";
                if (this.props.data[3] == "text") icon = "fal fa-font";
                if (this.props.data[3] == "select") icon = "fal fa-list-alt";
                if (this.props.data[3] == "location") icon = "fal fa-map-marker";
                break;
            case "USER_TYPE":
                icon = "fal fa-users";
                label = __(this.props.data.name);
                break;
            case "SOURCE":
                icon = "fal fa-laptop";
                label = "Source Type";
                break;
            case "STATUS":
                icon = "fal fa-toggle-on";
                label = "Status";
                break;
            case "ORGANIZATION":
                icon = "fal fa-building";
                label = "Organization";
                break;
            case "USER":
                icon = "fal fa-user";
                label = "User";
                break;
            case 'FORM_STATUS':
                icon = 'fal fa-list-all';
                label = 'Form Status';
                break;
            default:
                icon = "fal fa-list-alt";
                if (this.props.data.type == "DATE") icon = "fal fa-calendar";
                label = __(this.props.data.name);
                break;
        }

        let className = "node";
        if (this.state.selected) className == " selected";
        return (
            <div className={className} onClick={this._toggle}>
                <div className="node-handle">
                    <div className="node-control" draggable={true} onDragStart={this._onDragStart} onDragEnd={this._onDragEnd}>
                        <ewars.d.Row>
                            <ewars.d.Cell width="20px">
                                <i className={icon}></i>
                            </ewars.d.Cell>
                            <ewars.d.Cell>{label}</ewars.d.Cell>
                        </ewars.d.Row>
                    </div>
                </div>
            </div>
        )
    }
}

class LocationTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            show: false
        }
    }

    _toggle = () => {
        if (this.state.data) {
            this.setState({
                show: !this.state.show
            })
        } else {
            ewars.tx("com.ewars.query", ["location_type", null, {status: {eq: "ACTIVE"}}, null, null, null, null, null])
                .then(resp => {
                    this.setState({
                        show: true,
                        data: resp
                    })
                })
        }
    };

    _onDragStart = (e) => {
        let data = ["D", "LOC", "Location", {}];
        e.dataTransfer.setData("n", JSON.stringify(data));
    };

    render() {
        let icon = "fal fa-caret-right";
        if (this.state.show) icon = "fal fa-caret-down";

        return (
            <div className="node">
                <div className="node-handle">
                    <ewars.d.Row>
                        <ewars.d.Cell width="20px" onClick={this._toggle}
                                      style={{padding: "6px 5px", textAlign: "center"}}>
                            <i className={icon}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            <div className="node-control" draggable={true} onDragStart={this._onDragStart}>
                                <ewars.d.Row>
                                    <ewars.d.Cell>
                                        Location
                                    </ewars.d.Cell>
                                    <ewars.d.Cell width="20px">
                                        <div className="node-ctx">
                                            <i className="fal fa-caret-down node-drop"></i>
                                        </div>
                                    </ewars.d.Cell>
                                </ewars.d.Row>
                            </div>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.show ?
                    <div className="node-children">
                        {this.state.data.map(item => {
                            return <Node key={"LT" + item.id} data={item} type="LOCATION"/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class DimensionTree extends React.Component {
    static defaultPrpos = {
        draggable: true,
        onClick: null
    };

    constructor(props) {
        super(props)
    }

    _startDrag = (e) => {

    };

    _onVarDrag = (e) => {
        e.dataTransfer.setData("n", JSON.stringify(["V", null, null, {}]));
    };

    _onClick = (data) => {
        this.props.onClick(data);
    };

    _onVarClick = () => {
        this.props.onClick(["V", null, null, {}]);
    };

    render() {
        return (
            <ewars.d.Panel>
                <div className="plot-tree" style={{padding: "5px"}}>
                    <FormNode root={true}/>
                    <Node type="FORM" data={{name: "Form"}}/>
                    <Node type="FORM_STATUS" data={{name: "FORM_STATUS"}}/>
                    <Node type="LOCATION" data={{name: "Location"}}/>
                    <Node type="LOCATION_STATUS" data={{name: "Location Status"}}/>
                    <Node type="LOCATION_TYPE" data={{name: "Location Type"}}/>
                    <Node type="LOCATION_GROUP" data={{name: "Location Group"}}/>
                    <Node type="LOCATION_PCODE" data={{name: 'Location Code'}}/>
                    <Node type="LOCATION_GEOM" data={{name: 'Loc. Geom. Type'}}/>
                    <Node type="RECORD_DATE" data={{name: "Record Date", type: "DATE"}}/>
                    <Node type="DATE_SUB" data={{name: "Record Submission Date", type: "DATE"}}/>
                    <Node type="USER_TYPE" data={{name: "User Type"}}/>
                    <Node type="USER" data={{name: "User"}}/>
                    <Node type="ORGANIZATION" data={{name: "Organization"}}/>
                    <Node type="SOURCE" data={{name: "Source"}}/>
                    <Node type="ALARM" data={{name: "Alarm"}}/>
                    <Node type="ALERT_STATE" data={{name: "Alert State"}}/>
                    <Node type="ALERT_STAGE" data={{name: "Alert Stage"}}/>
                    <Node type="ALERT_STAGE_STATE" data={{name: "Alert Stage State"}}/>
                    <Node type="ALERT_OUTCOME" data={{name: "Alert Outcome"}}/>
                    <Node type="ALERT_DATE" data={{name: "Alert Date", type: "DATE"}}/>

                </div>
            </ewars.d.Panel>
        )
    }
}

export default DimensionTree;