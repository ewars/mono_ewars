import * as d3 from "d3";

import Axis from "../pieces/Axis";

class Series {
    constructor(el, config, additions) {
        this._el = el;
        this._conf = config;

        this._xAxis = [];
        this._yAxis = [];
        this._data = {};

        this._height = 0;
        this._width = 0;

        this._setup();
    }

    _setup = () => {
        let bounds = this._el.getBoundingClientRect();
        this._width = bounds.width;
        this._height = bounds.height;

        this._svg = d3.select(this._el).append("svg");

        this._root = this._svg.append("g");


    }


}