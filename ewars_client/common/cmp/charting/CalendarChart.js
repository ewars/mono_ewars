import * as d3 from "d3";

const CELL_SIZE = 17;

const pathMonth = (t0) => {
    let t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
        d0 = t0.getDay(),
        w0 = d3.timeWeek.count(d3.timeYear(t0), t0),
        d1 = t1.getDay(),
        w1 = d3.timeWeek.count(d3.timeYear(t1), t1);

    return "M" + (w0 + 1) * CELL_SIZE + "," + d0 * CELL_SIZE
        + "H" + w0 * CELL_SIZE + "V" + 7 * CELL_SIZE
        + "H" + w1 * CELL_SIZE + "V" + (d1 + 1) * CELL_SIZE
        + "H" + (w1 + 1) * CELL_SIZE + "V" + 0
        + "H" + (w0 + 1) * CELL_SIZE + "Z";
};


const DATA = [
    ["2017-01-01", 12],
    ["2017-01-02", 12],
    ["2017-01-03", 19],
    ["2017-01-04", 222],
    ["2017-01-05", 12],
    ["2017-01-06", 1],
    ["2017-01-07", 0],
    ["2018-01-01", 1]
]


class CalendarChart {
    constructor(el, definition, reportDate, locationId) {
        this.definition = definition;
        this._el = el;
        this.reportDate = reportDate;
        this.locationId = locationId;

        this._cellSize = 17;

        this._setup();
    }

    _setup = () => {
        this._el.style.overflow = "hidden";
        this._el.style.display = "block";
        let bounds = this._el.getBoundingClientRect();
        this._width = bounds.width;
        this._height = bounds.height;

        let color = d3.scaleQuantize()
            .domain([0, 1000])
            .range(["#a50026", "#d73027", "#f46d43", "#fdae61", "#fee08b", "#ffffbf", "#d9ef8b", "#a6d96a", "#66bd63", "#1a9850", "#006837"])

        let rowHeight = this._height / 3;
        this._svg = d3.select(this._el)
            .selectAll("svg")
            .data(d3.range(2017, 2020))
            .enter()
            .append("svg")
            .attr("width", this._width)
            .attr("height", rowHeight)
            .append("g")
            .attr(
                "transform",
                (d, i) => {
                    console.log(d, i);
                    return `translate(${((this._width - CELL_SIZE * 53) / 2)},${rowHeight - CELL_SIZE * 7 - 1})`
                }
            );

        this._svg.append("text")
            .attr("transform", `translate(-6,${this._cellSize * 3.5})rotate(-90)`)
            .attr("font-family", "sans-serif")
            .attr("font-size", 10)
            .attr("text-anchor", "middle")
            .text(function (d) {
                return d;
            });

        let rect = this._svg.append("g")
            .attr("fill", "none")
            .attr("stroke", "#CCCCCC")
            .selectAll("rect")
            .data(function (d) {
                return d3.timeDays(new Date(d, 0, 1), new Date(d + 1, 0, 1));
            })
            .enter()
            .append("rect")
            .attr("width", CELL_SIZE)
            .attr("height", CELL_SIZE)
            .attr("x", function (d) {
                return d3.timeWeek.count(d3.timeYear(d), d) * CELL_SIZE;
            })
            .attr("y", function (d) {
                return d.getDay() * CELL_SIZE;
            })
            .datum(d3.timeFormat("%Y-%m-%d"));

        this._svg.append("g")
            .attr("fill", "none")
            .attr("stroke", "#000000")
            .selectAll("path")
            .data(function (d) {
                return d3.timeMonths(new Date(d, 0, 1), new Date(d + 1, 0, 1));
            })
            .enter()
            .append("path")
            .attr("d", pathMonth);

        rect.data(DATA)
            .attr("fill", function (d) {
                console.log(d);
                return color(d[1]);
            })
            .append("title")
            .text(function (d) {
                return d[0] + ": " + d[1];
            })


    }
}

export default CalendarChart;