import * as d3 from "d3";
import RangeUtils from "../../utils/RangeUtils";

class RawValue {
    constructor(el, definition, reportDate, locationId) {
        console.log(definition);
        this._el = el;
        this._config = definition;
        this.reportDate = reportDate;
        this.locationId = locationId;

        this._query();
    }

    _getSeries = (src) => {
        if (src.sources.length <= 1) {
            let item = src.sources[0];
            switch (item[0]) {
                case "I":
                    if (item[3].dimension) {
                        return {
                            indicator: {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            }
                        }
                    } else {
                        return {
                            indicator: item[1]
                        };
                    }
                case "F":
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    }
                default:
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    };
            }
        } else {
            return {
                series: src.sources.map((item) => {
                    switch (item[0]) {
                        case "I":
                            if (item[3].dimension) {
                                return {
                                    ...item[3],
                                    variable_name: item[2],
                                    uuid: item[1]
                                }
                            } else {
                                return {
                                    ...item[3],
                                    uuid: item[1],
                                    variable_name: item[2]
                                }
                            }
                        case "F":
                            return {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            };
                        default:
                            return {
                                ...item[3],
                                uuid: item[1]
                            };
                    }
                })
            }
        }
    };

    _query = () => {
        let end_date = RangeUtils.process(this._config.source.period[1], this._reportDate),
            start_date = RangeUtils.process(this._config.source.period[0], this._reportDate, end_date);

        let query = {
            start_date: start_date,
            end_date: end_date,
            type: this._config.source.sources.length > 1 ? "SLICE_COMPLEX" : "SLICE",
            interval: "DAY",
            formula: this._config.source.formula || null,
            location: this._config.source.location,
            reduction: this._config.source.reduction || "SUM",
            meta: false,
            ...this._getSeries(this._config.source)
        };


        ewars._queue.push("/arc/analysis", query)
            .then(resp => {
                if (resp) {
                    this._render(resp);
                }
            })
            .catch((err) => {
                ewars.emit("WIDGET_LOADED");
            })

    };

    _render = (resp) => {
        let bounds = this._el.getBoundingClientRect();
        this.width = bounds.width;
        this.height = bounds.height;

        this._svg = d3.select(this._el)
            .append("svg")
            .attr('width', this.width)
            .attr("height", this.height);

        console.log(resp);


        let main = this._svg.append("g")
            .attr("transform", `translate(${this.width / 2},${this.height / 2})`);
        console.log(resp.data, this._config.style);

        console.log(ewars.NUM(resp.data, this._config.style.format || "0,0"))

        main.append("text")
            .attr("fill", "#000000")
            .attr("stroke", "none")
            .style("text-align", "center")
            .style("text-anchor", "middle")
            .style("font-size", this._config.style.fontSize || "22px")
            .text(ewars.NUM(resp.data, this._config.style.format || "0,0"));

        if (this._config.style.show_title) {
            this._svg.append("text")
                .attr("fill", "#000000")
                .attr('stroke', 'none')
                .attr("transform", `translate(${this.width / 2},${20})`)
                .style("text-anchor", "middle")
                .style("text-align", "center")
                .style("font-size", "14px")
                .text(this._config.title)
        }

        this._svg.append('text')
            .attr("fill", "#000000")
            .attr('stroke', 'none')
            .attr("transform", `translate(${this.width / 2},${35})`)
            .style("text-anchor", "middle")
            .style("text-align", "center")
            .style("font-size", "12px")
            .text("Sub title")


    }


}

export default RawValue;