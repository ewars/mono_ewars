class Image extends React.Component {
    _onClick = () => {
        if (this.props.data.style.click_url) {
            if (this.props.data.style.click_type == "BLANK") {
                window.open(this.props.data.click_url);
            } else {
                document.location = this.props.data.click_url;
            }
        }
    };

    render() {
        return (
            <div style={{width: "100%", height: "100%"}}>
                <image src={this.props.data.url} onClick={this._onClick}/>
            </div>
        )
    }
}

export default Image;