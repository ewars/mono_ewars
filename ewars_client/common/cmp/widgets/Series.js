import Moment from "moment";

import RangeUtils from "../../utils/RangeUtils";

const LEGENDS = {
    top: {
        enabled: true,
        align: "center",
        verticalAlign: "top",
        layout: "horizontal",
        x: 0,
        y: 0
    },
    right: {
        enabled: true,
        align: "right",
        verticalAlign: "top",
        layout: "vertical",
        x: 0,
        y: 0
    },
    bottom: {
        enabled: true,
        align: "center",
        verticalAlign: "bottom",
        layout: "horizontal",
        x: 0,
        y: 0
    },
    left: {
        enabled: true,
        align: "left",
        verticalAlign: "top",
        layout: "vertical",
        x: 0,
        y: 0
    }
};

const SORT_MAP = {
    CAT_NAME: "name",
    CAT_NAME_DESC: "name",
    VALUE_ASC: "value",
    VALUE_DESC: "value"
};


class Series {
    constructor(el, config, additions) {
        this._id = ewars.utils.uuid();
        this._reportDate = additions.report_date || null;
        this._locationId = additions.location_id || null;
        this._chart = null;
        this._config = config;
        this._el = el;
        this._token = additions.token || null;

        this._totalItems = 0;
        this._loadedItems = 0;

        this._chartConfig = this._configure(this._config);


        this.render();
    }

    update = (cfg, additions) => {
        if (additions.report_date) this._reportDate = additions.report_date;
        if (additions.location_uuid) this._locationId = additions.location_uuid;
        this._config = cfg;
        this._chartConfig = this._configure(cfg);
        this._reload()
    };

    _configureLegend = (cfg) => {
        if (!cfg.legend) return {enabled: false};
        let legend = {
            ...LEGENDS[cfg.legend_position]
        };
        legend.marginLeft = legend.align == "left" ? 120 : 0;
        legend.marginTop = legend.align == "top" ? 120 : 0;
        legend.marginRight = legend.align == "right" ? 120 : 0;
        legend.marginBottom = legend.align == "bottom" ? 120 : 0;

        return legend;
    };

    _configure = (cfg) => {
        return {
            chart: {
                renderTo: this._el,
                animation: false,
                backgroundColor: "rgba(255,255,255,0)",
                zoomType: cfg.zoom || null
            },
            credits: {enabled: false},
            title: {
                text: cfg.show_title ? __(cfg.title) : null,
                align: "center"
            },
            subtitle: {
                text: cfg.show_subtitle ? __(cfg.subtitle) : null,
                align: "center"
            },
            exporting: {
                enabled: false
            },
            noData: "",
            navigator: {
                enabled: cfg.navigator ? true : false
            },
            legend: this._configureLegend(cfg),
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: false
                    },
                    enableMouseTracking: true,
                    stacking: cfg.stack ? "normal" : null,
                    states: {
                        hover: {
                            lineWidthPlus: 0,
                            marker: {radius: null}
                        }
                    }
                },
                column: {
                    stacking: cfg.stack ? "normal" : null
                }
            },
            tooltip: {
                formatter: function () {
                    let interval = cfg.interval || "DAY";
                    let plotDate = this.key;
                    if (!config.decouple) {
                        plotDate = ewars.DATE(this.key, interval);
                    }
                    let seriesName = ewars.I18N(this.series.name);
                    let locationName = "";
                    if (this.series.options._eData.location)
                        locationName = ewars.I18N(this.series.options._eData.location.name);

                    let val = this.y;
                    val = ewars.NUM(val, cfg.y_axis_format || "0");

                    return `<span style="font-size: 10px;">${plotDate} - ${locationName} </span><br/><span style="color: ${this.color}">●</span><span style="font-size: 10px">${seriesName}</span><b> ${val}</b><br/>`;
                }
            },
            yAxis: {
                tickLength: 10,
                tickWidth: 1,
                tickInterval: cfg.y_tick_interval || null,
                max: cfg.max_y_value || undefined,
                min: cfg.min_y_value || 0,
                labels: {
                    formatter: function () {
                        return ewars.NUM(this.value, cfg.y_axis_format || "0")
                    }
                },
                title: {
                    text: __(cfg.y_axis_label || "NUMBER")
                },
                allowDecimals: cfg.y_axis_allow_decimals || undefined,
                alternateBandColor: "#F2F2F2"
            },
            xAxis: {
                type: "datetime",
                //     startOfWeek: 0,
                //     minTickInterval: Moment.duration(7, "day").asMilliseconds(),
                labels: {
                    autoRotation: [-45],
                    rotation: cfg.x_label_rotation || null,
                    formatter: function () {
                        return Moment.utc(this.value).clone().subtract("1", "d").format("YYYY-MM-DD")
                    }
                },
                plotBands: null,
                plotLines: null
            }
        };

    };

    _formatDecoupled = (dt, format) => {
        switch (format) {
            case "WEEK":
                return Moment.utc(dt).format("[W]W");
            case "MONTH":
                return Moment.utc(dt).format("MMM");
            case "YEAR":
                return Moment.utc(dt).format("YYYY");
            default:
                return Moment.utc(dt).format("YYYY-MM-DD")
        }
    };

    _configLocation = (src) => {
        switch (src.loc_spec) {
            case "REPORT_LOCATION":
                return this._locationId;
            case "GENERATOR":
                return {
                    parent_id: src.generator_locations_parent,
                    site_type_id: src.generator_location_type,
                    status: src.generator_location_status || "ACTIVE"
                };
            case "GROUP":
                return {
                    agg: src.group_output,
                    groups: src.group_ids
                };
            default:
                return src.location;
        }
    };

    _shouldAdd = (src, val) => {
        if (src.hide_no_data && val <= 0) return false;
        return true;
    };


    _getSeries = (src) => {
        if (src.sources.length <= 1) {
            let item = src.sources[0];
            switch (item[0]) {
                case "I":
                    if (item[3].dimension) {
                        return {
                            indicator: {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            }
                        }
                    } else {
                        return {
                            indicator: {
                                uuid: item[1]
                            }
                        };
                    }
                case "F":
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    }
                default:
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    };
            }
        } else {
            return {
                series: src.sources.map((item) => {
                    switch (item[0]) {
                        case "I":
                            if (item[3].dimension) {
                                return {
                                    ...item[3],
                                    variable_name: item[2],
                                    uuid: item[1]
                                }
                            } else {
                                return {
                                    ...item[3],
                                    uuid: item[1],
                                    variable_name: item[2]
                                }
                            }
                        case "F":
                            return {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            };
                        default:
                            return {
                                ...item[3],
                                uuid: item[1]
                            };
                    }
                })
            }
        }
    };

    render = () => {

        this._chart = new Highcharts.Chart(this._chartConfig);

        this._chart.showLoading();

        if ((this._config.sources || this._config.series) == null) return;

        let rootEnd = RangeUtils.process(this._config.period[1], this._reportDate);
        let rootStart = RangeUtils.process(this._config.period[0], this._reportDate, rootEnd);

        (this._config.sources || this._config.series).forEach((src, index) => {
            src.index = index;
            let endDate,
                startDate;
            if (src.period) {
                endDate = RangeUtils.process(src.period[1], this._reportDate);
                startDate = RangeUtils.process(src.period[0], this._reportDate, endDate);
            } else {
                endDate = rootEnd;
                startDate = rootStart;
            }

            let query = {
                uuid: ewars.utils.uuid(),
                type: src.sources.length > 1 ? "SERIES_COMPLEX" : "SERIES",
                indicator: src.indicator,
                interval: this._config.interval,
                start_date: startDate,
                end_date: endDate,
                formula: src.formula || null,
                ...this._getSeries(src),
                location: this._configLocation(src)
            };


            ewars._queue.push("/arc/analysis", query, this._token)
                .then((resp) => {
                    this._renderResult(src, resp);
                })

        })
    };

    _renderResult = (src, result) => {
        let series = {
            marker: {
                enabled: true,
                radius: src.marker_radius || 3,
                symbol: src.marker_symbol || "circle"
            },
            name: src.name || src.title,
            lineWidth: parseFloat(src.line_width || 1),
            _eData: {location: result.location || null},
            dashStyle: src.line_style ? src.line_style : "Solid",
            color: src.color || undefined,
            type: src.style || "line",
            fillOpactiy: 0.6
        };

        series.data = result.data.sort((a, b) => {
            if (a[0] > b[0]) return 1;
            if (a[0] < b[0]) return -1;
            return 0;
        })

        series.data = series.data.map((item) => {
            let value = parseFloat(item[1]) || null;
            return [Moment.utc(item[0]).valueOf(), value];
        })

        this._chart.hideLoading();
        this._chart.addSeries(series, false);
        this._chart.redraw();
    };
}

export default Series;