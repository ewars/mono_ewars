import RangeUtils from "../../utils/RangeUtils";

const LEGENDS = {
    top: {
        enabled: true,
        align: "center",
        verticalAlign: "top",
        layout: "horizontal",
        x: 0,
        y: 0
    },
    right: {
        enabled: true,
        align: "right",
        verticalAlign: "top",
        layout: "vertical",
        x: 0,
        y: 0
    },
    bottom: {
        enabled: true,
        align: "center",
        verticalAlign: "bottom",
        layout: "horizontal",
        x: 0,
        y: 0
    },
    left: {
        enabled: true,
        align: "left",
        verticalAlign: "top",
        layout: "vertical",
        x: 0,
        y: 0
    }
};

const SORT_MAP = {
    CAT_NAME: "name",
    CAT_NAME_DESC: "name",
    VALUE_ASC: "value",
    VALUE_DESC: "value"
};


class Category {
    constructor(el, config, additions) {
        this._id = ewars.utils.uuid();
        this._reportDate = additions.report_date || null;
        this._locationId = additions.location_id || null;
        this._chart = null;
        this._config = config;
        this._el = el;
        this._token = additions.token || null;

        this._totalItems = 0;
        this._loadedItems = 0;

        this._chartConfig = this._configure(this._config);


        this.render();
    }

    update = (cfg, additions) => {
        if (additions.report_date) this._reportDate = additions.report_date;
        if (additions.location_uuid) this._locationId = additions.location_uuid;
        this._config = cfg;
        this._chartConfig = this._configure(cfg);
        this._reload()
    };

    _configureLegend = (cfg) => {
        if (!cfg.show_legend) return {enabled: false};
        let legend = {
            ...LEGENDS[cfg.legend_position]
        };
        legend.marginLeft = legend.align == "left" ? 120 : 0;
        legend.marginTop = legend.align == "top" ? 120 : 0;
        legend.marginRight = legend.align == "right" ? 120 : 0;
        legend.marginBottom = legend.align == "bottom" ? 120 : 0;

        return legend;
    };

    _configure = (cfg) => {
        return {
            chart: {
                renderTo: this._el,
                animation: false,
                type: "pie",
                backgroundColor: "rgba(255,255,255,0)"
            },
            credits: {enabled: false},
            title: {
                text: cfg.show_title ? __(cfg.title) : null,
                align: "center"
            },
            exporting: {
                enabled: false
            },
            noData: "",
            navigator: {
                enabled: cfg.navigator ? true : false
            },
            legend: this._configureLegend(cfg),
            plotOptions: {
                pie: {
                    allowPointerSelect: true,
                    cursor: "pointer",
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {y}',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || "black"
                        }
                    },
                    showInLegend: true,
                    startAngle: cfg.style == "ARC" ? -90 : null,
                    endAngle: cfg.style == "ARC" ? 90 : null,
                    center: cfg.style == "ARC" ? ["50%", "75%"] : ["50%", "50%"]
                }
            },
            series: [],
            tooltip: {
                formatter: function () {
                    return '<span style="' + this.color + '">●</span><span style="font-size: 10px">' + this.key + ': </span> <b>' + this.y + '</b><br />';
                }
            },
            yAxis: {
                min: 0,
                labels: {
                    formatter: () => {
                        return ewars.NUM(this.value, cfg.y_axis_format || "0")
                    }
                },
                title: {
                    text: __(cfg.y_axis_label || "NUMBER")
                },
                allowDecimals: cfg.y_axis_allow_decimals
            }
        };

    };

    _configLocation = (src) => {
        switch (src.loc_spec) {
            case "REPORT_LOCATION":
                return this._locationId;
            case "GENERATOR":
                return {
                    parent_id: src.generator_locations_parent,
                    site_type_id: src.generator_location_type,
                    status: src.generator_location_status || "ACTIVE"
                };
            case "GROUP":
                return {
                    agg: src.group_output,
                    groups: src.group_ids
                };
            default:
                return src.location;
        }
    };

    _shouldAdd = (src, val) => {
        if (src.hide_no_data && val <= 0) return false;
        return true;
    };



    _getSeries = (src) => {
        if (src.sources.length <= 1) {
            let item = src.sources[0];
            switch (item[0]) {
                case "I":
                    if (item[3].dimension) {
                        return {
                            indicator: {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            }
                        }
                    } else {
                        return {
                            indicator: {
                                uuid: item[1]
                            }
                        };
                    }
                case "F":
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    }
                default:
                    return {
                        indicator: {
                            uuid: item[1]
                        }
                    };
            }
        } else {
            return {
                series: src.sources.map((item) => {
                    switch (item[0]) {
                        case "I":
                            if (item[3].dimension) {
                                return {
                                    ...item[3],
                                    variable_name: item[2],
                                    uuid: item[1]
                                }
                            } else {
                                return {
                                    ...item[3],
                                    uuid: item[1],
                                    variable_name: item[2]
                                }
                            }
                        case "F":
                            return {
                                ...item[3],
                                uuid: item[1],
                                variable_name: item[2]
                            };
                        default:
                            return {
                                ...item[3],
                                uuid: item[1]
                            };
                    }
                })
            }
        }
    };

    render = () => {

        this._chart = new Highcharts.Chart(this._chartConfig);
        let series = {
            colorByPoint: true,
            data: [],
            lineWidth: 1,
            innerSize: ['ARC', 'DONUT'].indexOf(this._config.style) >= 0 ? "50%" : null,
            type: "pie"
        };
        this._series = this._chart.addSeries(series);

        this._chart.showLoading();

        if ((this._config.sources || this._config.series) == null) return;

        (this._config.sources || this._config.series).forEach((src, index) => {
            let endDate = RangeUtils.process(src.period[1], this._reportDate),
                startDate = RangeUtils.process(src.period[0], this._reportDate, endDate);

            let query = {
                uuid: ewars.utils.uuid(),
                type: src.sources.length > 1 ? "SLICE_COMPLEX" : "SLICE",
                indicator: src.indicator,
                interval: "DAY",
                start_date: startDate,
                end_date: endDate,
                formula: src.formula || null,
                ...this._getSeries(src),
                location: this._configLocation(src),
                reduction: src.reduction || "SUM"
            };


            ewars._queue.push("/arc/analysis", query, this._token)
                .then((resp) => {
                    this._renderResult(src, resp);
                })

        })
    };

    _renderResult = (src, result) => {
        let colourCount = 0,
            slices = [],
            categories = [];

        if (result instanceof Array) {
            result.forEach((subSeries) => {
                if (this._shouldAdd(src, subSeries.data)) {
                    categories.push(__(subSeries.location.name));
                    slices.push({
                        name: __(subSeries.location.name),
                        color: src.colour || null,
                        y: subSeries.data,
                        value: subSeries.data

                    })
                }
            })
        } else {
            if (this._shouldAdd(src, result.data)) {
                categories.push(__(src.title));
                slices = [{
                    name: src.title ? __(src.title) : "Untitled",
                    color: src.colour || null,
                    y: result.data,
                    value: result.data
                }]
            }
        }

        // this._chart.xAxis[0].setCategories(categories);

        this._chart.hideLoading();
        slices.forEach((item) => {
            this._series.addPoint(item, false, false, false);
        })
        this._chart.redraw();
    };
}

export default Category;