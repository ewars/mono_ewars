import SelectField from "./c.cmp.select";
import TextField from "./c.cmp.text";
import { Layout, Row, Cell } from "./layout";
import { ActionGroup } from "./c.action_button";
import Toolbar from "./c.toolbar";
import Button from "./c.button";

const ACTIONS = [
    ["fa-plus", "ADD"]
];

const APPLICATION = {
    n: "application",
    o: [
        ["ALL", "All of the following are true"],
        ["ANY", "Any of the following are true"]
    ]
};

const ROW_ACTIONS = [
    ["fa-trash", "DELETE"]
]

class Rule extends React.Component {
    _onChange = (prop, value) => {
        let rule = ewars.copy(this.props.data);
        if (prop == 't_value') rule[2] = value;
        if (prop == 'comparator') rule[1] = value;
        if (prop == "field") rule[0] = value;

        this.props.onChange(this.props.index, rule);
    };

    _onAction = (action) => {
        this.props.onRemove(this.props.index);
    };

    render() {
        let val, cmp;
        if (this.props.data[0] != null) {
            let field;
            this.props.fields.forEach((item) => {
                if (item[0] == this.props.data[0]) field = item;
            });

            switch (field[2].type) {
                case "text":
                case "number":
                    cmp = (
                        <TextField
                            value={this.props.data[2]}
                            onChange={this._onChange}
                            data={{
                                n: "t_value"
                            }}/>
                    )
                    break;
                case "select":
                    cmp = (
                        <SelectField
                            value={this.props.data[2]}
                            onChange={this._onChange}
                            data={{
                                n: "t_value",
                                o: field[2].options
                            }}/>
                    )
                    break;
                default:
                    // default case is text
                    break;

            }
        }

        return (
            <div className="block">
                <div className="block-content">
                    <Row>
                        <Cell>
                            <SelectField
                                value={this.props.data[0]}
                                onChange={this._onChange}
                                data={{
                                    n: "field",
                                    o: this.props.fields
                                }}/>

                        </Cell>
                        <Cell>
                            <SelectField
                                value={this.props.data[1]}
                                onChange={this._onChange}
                                data={{
                                    n: "comparator",
                                    o: [
                                        ["eq", "Equal to"],
                                        ["neq", "Not equal to"],
                                        ["gt", "Greater than"],
                                        ["gte", "Greater than / equal to"],
                                        ["lt", "Less than"],
                                        ["lte", "Less than / equal to"]
                                    ]
                                }}/>

                        </Cell>
                        <Cell>
                            {cmp}
                        </Cell>
                        <Cell width="30px" style={{display: "block", width: "25px"}}>
                            <ActionGroup
                                right={true}
                                actions={ROW_ACTIONS}
                                height="28px"
                                onAction={this._onAction}/>
                        </Cell>
                    </Row>
                </div>
            </div>
        )
    }
}

const DEFAULT = {
    application: "ALL",
    rules: []
};

export default class RulesEditor extends React.Component {
    static defaultProps = {
        value: {
            application: "ALL",
            rules: []
        }
    };

    constructor(props) {
        super(props);
    }

    _action = (action, args) => {
        if (action == "ADD") {
            let cp = ewars.copy(this.props.value || DEFAULT);
            cp.rules.push([null, "eq", null]);

            this.props.onChange(this.props.data.n, cp);
        }

        if (action == "DELETE") {
            let cp = ewars.copy(this.props.value || DEFAULT);
            cp.rules.splice(args, 1);
            this.props.onChange(this.props.data.n, cp);
        }

        if (action == "MERGE") {

        }
    };

    _onTypeChange = (prop, value) => {
        let v = ewars.copy(this.props.value || DEFAULT);
        v.application = value;

        this.props.onChange(this.props.data.n, v);
    };

    _onRuleChange = (index, value) => {
        let v = ewars.copy(this.props.value || DEFAULT);
        v.rules[index] = value;
        this.props.onChange(this.props.data.n, v);
    };

    _removeRule = (index) => {
        let v = ewars.copy(this.props.value || DEFAULT);
        v.rules.splice(index, 1);
        this.props.onChange(this.props.data.n, v);
    };

    render() {
        let application = "ALL";
        if (this.props.value) {
            application = this.props.value.application || "ALL";
        }

        let rules = [];
        if (this.props.value) {
            if (this.props.value.rules) {
                rules = this.props.value.rules;
            }
        }

        let items = rules.map((item, index) => {
            return (
                <Rule
                    index={index}
                    onChange={this._onRuleChange}
                    onRemove={this._removeRule}
                    fields={this.props.data.fo}
                    data={item}/>
            )
        });

        let showRules = items.length > 0 ? true : false;

        return (
            <Layout style={{border: "1px solid #262626"}}>
                <Toolbar label="Conditions Editor">
                    <Row style={{minHeight: "35px"}}>
                        <Cell>
                            <div style={{marginTop: "-3px"}}>
                                <SelectField
                                    data={APPLICATION}
                                    value={application}
                                    onChange={this._onTypeChange}/>
                            </div>

                        </Cell>
                        <Cell style={{display: "block"}}>
                            <div className="btn-group pull-right">
                                <Button
                                    icon="fa-plus"
                                    onClick={() => this._action("ADD")}/>
                            </div>
                        </Cell>
                    </Row>
                </Toolbar>
                {showRules ?
                <Row>
                    <Cell>
                        <div className="block-tree"
                             style={{
                                 position: "relative",
                                 height: "auto",
                                 overflowY: "inherit"
                             }}>
                            {items}
                        </div>

                    </Cell>
                </Row>
                : null}
            </Layout>
        )
    }
}
