class SettingsField extends React.Component {
    static defaultProps = {
        vertical: false,
        label: null,
        required: false
    }

    render() {
        return (
            <div className="hsplit-box">
                <div className="ide-setting-label">{this.props.label}</div>
                <div className="ide-setting-control">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default SettingsField;
