const QUEUE_LIMIT = 2;
const CHECK_FREQUENCY = 3000;
const XHR_DONE = 4;
const XHR_OK = 200;

class AnalysisQueue {
    constructor() {
        aQueue = this;
        this.queued = [];
        this.timeout = null;
        this.current = [];

        this.running = false;
        this._run();
    };

    get = (items, axis, options) => {
        let resolver, rejector;
        let promise = new Promise((resolve, reject) => {
            resolver = resolve;
            rejector = reject;
        })

        this.queued.push([resolver, rejector, items, axis, options]);

        if (!this.timeout) {
            this.timeout = window.setInterval(() => {
                if (this.load < QUEUE_LIMIT) {
                    this._runItem()
                }
            }, CHECK_FREQUENCY);
        }
        this._runItem();

        return promise;
    };

    _runItem = () => {
        if (this.load >= QUEUE_LIMIT) return;
        let current;
        if (!this.queued.length) {
            window.clearInterval(this.timeout);
            this.timeout = 0;
            return;
        }

        current = this.queued.shift();
        this.load++;

        if (current) {
            let resolve = current[0];
            let reject = current[1];
            let nodes = current[2];
            let axis = current[3];
            let options = current[4];

            let xhr;
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else {
                xhr = new ActiveXObject("Microsot.XMLHTTP");
            }

            xhr.open('POST', `http://${window.location.host}/plotter`, true);
            xhr.setRequestHeader('Content-Type', 'application/json');

            xhr.onreadystatechange = () => {
                if (xhr.readyState === XHR_DONE) {
                    if (xhr.status === XHR_OK) {
                        this.load--;
                        resolve(JSON.parse(xhr.responseText));
                    } else {
                        this.load--;
                        reject(xhr.status);
                    }
                }
            };

            xhr.send(JSON.stringify([nodes, axis, options]));


        }

    };

    clear = () => {
        this.queued = [];
    };

    _run = () => {

    };
};

let aQueue = new AnalysisQueue();

export default aQueue;


