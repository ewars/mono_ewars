import Spinner from "../c.spinner";

class Block extends React.Component {
    constructor(props) {
        super(props)
    }

    _onClick = () => {
        window.open(`/reporting#?location_id=${this.props.data.location_id}&form_id=${this.props.data.form_id}&data_date=${this.props.data.data_date}`);
    };

    render() {
        return (
            <li className="iw-list-item" onClick={this._onClick}>
                <div className="title">{ewars.I18N(this.props.data.form_name)}
                    - {ewars.DATE(this.props.data.data_date, this.props.data.interval)}</div>
                <div className="content">{this.props.data.lname}</div>
            </li>
        )
    }
}

class Upcoming extends React.Component {
    _isLoaded = false;
    static defaultProps = {
        fitted: null
    };

    constructor(props) {
        super(props)
    }


    componentWillMount() {
        ewars.tx("com.ewars.user.upcoming")
            .then(resp => {
                this._isLoaded = true;
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        if (window.user.role != "USER") {
            return (
                <div className="placeholder">Upcoming reports are not available for your role.</div>
            )
        }
        if (!this._isLoaded) return <Spinner/>;

        let style = {
            maxHeight: "250px",
            overflowY: "auto"
        };

        if (this.props.fitted) {
            style = {
                display: "block",
            }
        }

        return (
            <ul className="iw-list" style={style}>
                {this.state.data.map(item => {
                    return (
                        <Block data={item}/>
                    )
                })}
            </ul>
        )
    }
}

export default Upcoming;
