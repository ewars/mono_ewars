import Spinner from "../c.spinner";

var LANG_INTERVAL = {
    DAY: "Daily",
    WEEK: "Weekly",
    MONTH: "Monthly",
    YEAR: "Annual"
};

var ReportTemplateListItem = React.createClass({
    _onClick: function () {
        document.location = "/documents#uuid=" + this.props.data.uuid;
    },

    render: function () {
        return (
            <div className="iw-list-item" onClick={this._onClick}>
                <div className="title">{__(this.props.data.template_name)}</div>
            </div>
        )
    }
});

var DocumentWidget = React.createClass({
    _isLoaded: false,

    getDefaultProps: function () {
        return {
            data: {}
        }
    },

    getInitialState: function () {
        return {
            templates: []
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.user.documents", [])
            .then(function (resp) {
                this.state.templates = resp;
                this._isLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
    },

    render: function () {

        var content;
        if (!this._isLoaded) content = <Spinner/>;

        if (this._isLoaded && this.state.templates.length > 0) {
            content = this.state.templates.map(template => {
                return <ReportTemplateListItem key={template.template.uuid} data={template.template}/>
            });
        }

        if (this._isLoaded && this.state.templates.length <= 0) {
            content = <p className="placeholder">There are currently no documents available.</p>;
        }

        return (
            <div className="iw-list">
                {content}
            </div>
        )
    }
});

export default DocumentWidget;
