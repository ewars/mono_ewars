import marked from "marked";

class Text extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let content = marked(this.props.data.content);

        return (
            <div className="article" dangerouslySetInnerHTML={{__html: content}} style={{paddingLeft: 14, paddingRight: 14}}>
            </div>
        )
    }
}

export default Text;
