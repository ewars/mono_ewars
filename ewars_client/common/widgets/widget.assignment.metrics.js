import Moment from "moment";
import Numeral from "numeral";

var AssignmentMetricsWidget = React.createClass({
    getInitialState: function () {
        return {
            metrics: {}
        };
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.user.metrics.assignments", [window.user.id])
            .then(function (resp) {
                this.state.metrics = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    render: function () {

        var completeness;
        if (this.state.metrics.completeness < 1) {
            var amount = this.state.metrics.completeness * 100 + "%";
            completeness = <span className="red">{amount}</span>
        } else {
            completeness = <span>100%</span>
        }

        var today = Moment().format("MMM Do, YYYY");
        var start_date = Moment(this.state.metrics.start_date).format("MMM Do, YYYY");

        var dateRange = start_date + " - " + today;

        var totalReports = Numeral(this.state.metrics.reports).format("0,0");

        return (
            <div className="widget">
                <div className="widget-header"><span>My Metrics</span></div>
                <div className="body no-pad">
                    <table className="metrics-block">
                        <tr>
                            <td>
                                <div className="metric-title">Number of Reports</div>
                                <div className="metric-value">{totalReports}</div>
                                <div className="metric-period">{dateRange}</div>
                            </td>
                            <td>
                                <div className="metric-title">Completeness</div>
                                <div className="metric-value">{completeness}</div>
                                <div className="metric-period">{dateRange}</div>
                            </td>
                            <td>
                                <div className="metric-title">Timeliness</div>
                                <div className="metric-value">N/A</div>
                                <div className="metric-period">{dateRange}</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        )
    }
});

export default AssignmentMetricsWidget;
