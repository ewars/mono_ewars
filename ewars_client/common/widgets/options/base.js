const ALERT_CONDITIONS = {
    application: "any",
    rules: [
        ["type", "eq", "ALERTS_MAP"],
        ["type", "eq", "ALERTS_LIST"]
    ]
};

const WORKLOAD_CONDITIONS = {
    application: "any",
    rules: [
        ["type", "eq", "OVERDUE"],
        ["type", "eq", "UPCOMING"]
    ]
};

const BASE_OPTIONS = {
    type: {
        type: "hidden"
    },
    title: {
        type: "text",
        label: "Widget Title"
    },

    // Metrics Widget Layout
    metric_layout: {
        type: "select",
        label: "Layout",
        options: [
            ["BAR", "Bar"],
            ["MINI_BAR", "Mini-bar"],
            ["TABLE", "Table"]
        ],
        conditions: {
            application: "all",
            rules: [
                ["type", "eq", "METRICS"]
            ]
        }
    },
    metrics_itms: {
        type: "select",
        multiple: true,
        label: "Selected Metrics",
        options: [
            ["SUBMISSIONS", "Form Submissions"],
            ["ASSIGNMENTS", "Assignments"],
            ["REPORTING_LOCATIONS", "Reporting Locations"],
            ["LOCATIONS", "Locations Total"],
            ["USERS", "Users"],
            ["ALERTS_OPEN", "Alerts Open"],
            ["ALERTS_TOTAL", "Alerts Triggered"],
            ["ALERTS_CLOSED", "Alerts Closed"],
            ["FORMS", "Forms"],
            ["DEVICES", "Devices"],
            ["PARTNERS", "Partners/Organizations"],
            ["TASKS_OPEN", "Tasks Open"],
            ["TASKS_UNACTIONED", "Tasks Unactioned"],
            ["TASKS_TOTAL", "Tasks Total"],
            ["COMPLETENESS", "Completeness"],
            ["TIMELINESS", "Timeliness"],
            ["DOCUMENTS", "Documents Total"]
        ]
    },

    // Alerts List and Alerts Map Options
    alarm_id: {
        type: "select",
        label: "Specific Alarm (Optional)",
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSouce: "name",
            query: {}
        },
        conditions: ALERT_CONDITIONS
    },
    alert_state: {
        type: "select",
        label: "Alert State (Optional)",
        options: [
            ["OPEN", "Active"],
            ["CLOSED", "Closed"]
        ],
        conditions: ALERT_CONDITIONS
    },
    alert_stage: {
        type: "select",
        label: "Alert Stage",
        options: [
            ["VERIFICATION", "Verification"],
            ["RISK_ASSESS", "Risk Assessment"],
            ["RISK_CHAR", "Risk Characterization"],
            ["OUTCOME", "Outcome"]
        ],
        conditions: ALERT_CONDITIONS
    },
    alert_stage_state: {
        type: "select",
        label: "Alert Stage State",
        options: [
            ["PENDING", "Pending"],
            ["ACTIVE", "Active"],
            ["COMPLETED", "Completed"]
        ],
        conditions: ALERT_CONDITIONS
    },
    alert_risk: {
        type: "select",
        label: "Alert Risk",
        options: [
            ["LOW", "Low Risk"],
            ["MODERATE", "Moderate Risk"],
            ["HIGH", "High Risk"],
            ["SEVERE", "Very High Risk"]
        ],
        conditions: ALERT_CONDITIONS
    },

    // Overdue Reports
    overdue_form_id: {
        type: "select",
        label: "Specific Form (Optional)",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"}
            }
        },
        conditions: WORKLOAD_CONDITIONS
    }
};

export default BASE_OPTIONS;