import Moment from "moment";

class Outbreak extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let start_date = Moment.utc(this.props.data.start_date).local().format("LL");
        return (
            <div className="out-item">
                <strong>{this.props.data.title}</strong>{start_date}
            </div>
        ) }
}

class OutbreaksWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            outbreaks: [],
            loaded: false,
            error: false
        }
    }

    componentDidMount() {
        ewars.tx('com.sonoma.outbreaks.active', [])
            .then(res => {
                this.setState({outbreaks: res, loaded: true});
            }).catch(err => {
                this.setState({
                    error: true
                });
            });
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div className="widget-loader">
                    <i className="fal fa-cog fa-spin"></i>
                </div>
            )
        }

        if (this.state.error && this.state.loaded) {
            return (
                <div className="widget-error">
                    <i className="fal fa-warning-triangle"></i>
                </div>
            )
        }

        return (
            <div className="out-widget">
                <div className="space-1"></div>
                <div className="out-header">
                    <div className="inner">
                        <div className="counter">{this.state.outbreaks.length}</div>
                        <div className="label">Active outbreaks</div>
                    </div>
                </div>
                <div className="space-2"></div>
                <div className="out-list">
                    {this.state.outbreaks.map(item => {
                        return <Outbreak data={item}/>
                    })}
                </div>
            </div>
        )
    }
}

export default OutbreaksWidget;
