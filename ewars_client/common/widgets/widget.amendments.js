const query = {
    amended_by: {eq: window.user.id},
    status: {eq: "PENDING"}
};

const joins = [
    "user:amended_by:id:id,name,email",
    "form:form_id:id:id,name"
];


class Amendments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentWillMount() {
        ewars.tx("com.ewars.query", ["amendment", null, query, null, null, null, joins])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        let data;

        if (this.state.data.length <= 0) data = <p className="placeholder">{_l("NO_AMENDMENTS")}</p>;

        return data;
    }
}

export default Amendments;