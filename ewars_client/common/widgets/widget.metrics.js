import numeral from "numeral";
import Spinner from "../c.spinner";

const ALL = [
    "SUBMISSIONS",
    "ASSIGNMENTS",
    "REPORTING_LOCATIONS",
    "USERS",
    "LOCATIONS",
    "PARTNERS",
    "ALERTS_OPEN",
    "ALERTS_TOTAL",
    "ALERTS_CLOSED",
    "FORMS",
    "DEVICES",
    "PARTNERS",
    "TASKS_OPEN",
    "TASKS_UNACTIONED",
    "TASKS_TOTAL",
    "COMPLETENESS",
    "TIMELINESS",
    "DOCUMENTS",
    "ALARMS"
];

Object.defineProperty(Array.prototype, 'chunk_inefficient', {
    value: function (chunkSize) {
        var array = this;
        return [].concat.apply([],
            array.map(function (elem, i) {
                return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
            })
        );
    }
});

class Bar extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);

        this.state = {
            data: {}
        }
    }

    componentWillMount() {
        let metrics = this.props.data.metrics_items;
        if (this.props.data.metrics_items.length <= 0) metrics = ALL;
        if (metrics) {
            metrics.forEach(metric => {
                ewars.tx("com.ewars.metric", [metric])
                    .then(resp => {
                        if (this._isMounted) {
                            this.setState({
                                data: {
                                    ...this.state.data,
                                    [metric]: resp
                                }
                            })
                        }
                    })
            })
        }
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        let metrics = this.props.data.metrics_items;
        if (this.props.data.metrics_items.length <= 0) metrics = ALL;

        let chunks;
        if (metrics.length > 6) {
            // We need to chunk this up as thats too many in a row
            chunks = metrics.chunk_inefficient(6);
        }

        let items;
        if (chunks) {
            items = [];

            chunks.forEach(chunk => {
                let rowItems = chunk.map(item => {
                    let val;
                    if (!this.state.data[item] != null) val = <i className="fal fa-spin fa-cog"></i>;
                    if (this.state.data[item]) val = numeral(this.state.data[item]).format("0,0");
                    return (
                        <div className="ide-col">
                            <div className="metric-item">
                                <div className="amount">{val}</div>
                                <div className="label">{_l(item)}</div>
                            </div>
                        </div>
                    )
                });

                items.push(
                    <div className="ide-row">
                        <div className="ide-col">&nbsp;</div>
                        {rowItems}
                        <div className="ide-col">&nbsp;</div>
                    </div>
                )
            })

        } else {
            let rowSingleItems = metrics.map(item => {
                let val;
                if (!this.state.data[item] != null) val = <i className="fal fa-spin fa-cog"></i>;
                if (this.state.data[item]) val = numeral(this.state.data[item]).format("0,0");
                return (
                    <div className="ide-col">
                        <div className="metric-item">
                            <div className="amount">{numeral(this.state.data[item]).format("0,0")}</div>
                            <div className="label">{_l(item)}</div>
                        </div>
                    </div>
                )
            });

            items = (
                <div className="ide-row">
                    <div className="ide-col">&nbsp;</div>
                    {rowSingleItems}
                    <div className="ide-col">&nbsp;</div>
                </div>
            )

        }

        return (
            <div className="ide-layout">
                {items}
            </div>
        )
    }
}

class MiniBar extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">&nbsp;</div>

                    <div className="ide-col">&nbsp;</div>
                </div>
            </div>
        )
    }
}

class TableMetrics extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {}
        }
    }

    componentWillMount() {
        let metrics = this.props.data.metrics_items;
        if (this.props.data.metrics_items.length <= 0) metrics = ALL;

        metrics.forEach(metric => {
            ewars.tx("com.ewars.metric", [metric])
                .then(resp => {
                    this.setState({
                        data: {
                            ...this.state.data,
                            [metric]: resp
                        }
                    })
                })
        })
    }

    render() {

        let metrics = this.props.data.metrics_items;
        if (this.props.data.metrics_items.length <= 0) metrics = ALL;

        let rows = metrics.map(item => {
            return (
                <tr>
                    <th>{_l(item)}</th>
                    <td>{numeral(this.state.data[item]).format("0,0")}</td>
                </tr>
            )
        });

        return (
            <table className="metric-table">
                <tbody>
                    {rows}
                </tbody>
            </table>
        )
    }
}

class Metrics extends React.Component {
    static defaultProps = {
        data: {
            metric_layout: "BAR",
            title: "Metrics",
            metrics_items: []
        }
    };

    constructor(props) {
        super(props);
    }


    render() {
        let Cmp;
        switch (this.props.data.metric_layout) {
            case "BAR":
                Cmp = Bar;
                break;
            case "MINI_BAR":
                Cmp = MiniBar;
                break;
            case "TABLE":
                Cmp = TableMetrics;
                break;
            default:
                Cmp = Bar;
                break;
        }

        return (
            <Cmp data={this.props.data}/>
        )
    }
}

export default Metrics;

