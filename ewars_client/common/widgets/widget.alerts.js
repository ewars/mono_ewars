import mapboxgl from "mapbox-gl";
import Tab from "../c.tab";

import Spinner from "../c.spinner";

const CONSTANTS = {
    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    OPEN: "OPEN",
    CLOSED: "CLOSED"
};

/**
var markerTemplate = _.template([
    '<div class="map-popup-wrapper">',
    '<table width="100%">',
    '<tr><th>Name:</th><td><%= alarm_name %></td></tr>',
    '<tr><th>Location:</th><td><%= location_name %></td></tr>',
    '<tr><th>Date:</th><td><%= trigger_period %></td></tr>',
    '</table>',
    '<a href="/alert#?uuid=<%= alert_uuid %>">More info</a>',
    '</div>'
].join(""));
 **/

const markerTemplate = (data) => {
    return `
    <div class="map-popup-wrapper">
    <table width="100%">
    <tr><th>Name:</th><td>${data.alarm_name}</td></tr>
    <tr><th>Location:</th><td>${data.location_name}</td></tr>
    <tr><th>Date:</th><td>${data.trigger_period}</td></tr>
    </table>
    <a href="/alert#?uuid=${data.alert_uuid}">More info</a>
    </div>
    `;
};

const TRIGGER_END_SORT = (a, b) => {
    if (a.alert.trigger_end > b.alert.trigger_end) return 1;
    if (a.alert.trigger_end < b.alert.trigger_end) return -1;
    return 0;
}


var AlertDate = React.createClass({
    _onClick: function () {
        document.location = "/alert#?uuid=" + this.props.data.alert.uuid;
    },

    render: function () {

        var alertPeriod = ewars.DATE(this.props.data.alert.trigger_end, this.props.data.alert.interval_type);

        return (
            <div className="alert-list-item" onClick={this._onClick}>
                <div className="period"><i className="fal fa-bell"></i>&nbsp;{alertPeriod}</div>
            </div>
        )
    }
});

var AlertGroup = React.createClass({
    _onClick: function () {

    },

    render: function () {

        var locationName = ewars.I18N(this.props.data[0].location.name);

        let alerts = this.props.data.sort(TRIGGER_END_SORT).reverse();
        alerts = alerts.map(alert => {
            return <AlertDate data={alert}/>
        });

        return (
            <div className="alert-location-group">
                <h3>{locationName}</h3>
                <div className="alerts-list">
                    {alerts}
                </div>
            </div>
        );
        return null;
    }
});

var AlarmGroup = React.createClass({
    render: function () {
        var alarmName = ewars.I18N(this.props.data[0].alert.alarm_name);

        let alerts = this.props.data.sort(TRIGGER_END_SORT);

        let grouped = {};
        alerts.forEach(alert => {
            if (grouped[alert.alert.location_id]) {
                grouped[alert.alert.location_id].push(alert);
            } else {
                grouped[alert.alert.location_id] = [alert];
            }
        });

        let end_alerts = [];

        for (let i in grouped) {
            end_alerts.push(<AlertGroup data={grouped[i]}/>);
        }

        return (
            <div className="alert-list-group">
                <h3>{alarmName}</h3>
                <div className="alerts-list">
                    {end_alerts}
                </div>
            </div>
        )
    }
});

var AlertMap = React.createClass({
    _map: null,
    _markers: null,
    _isLoaded: false,

    getInitialState: function () {
        return {
            data: [],
            view: CONSTANTS.OPEN
        }
    },

    componentWillMount: function () {
    },

    query: function (filter) {
        ewars.tx("com.ewars.alerts.query.legacy", [filter, 100, 0])
            .then(resp => {
                this._isLoaded = true;
                this.setState({
                    data: resp.results,
                    confinement: resp.confinement
                });
                this._run(resp.results, resp.confinement);
            })
    },

    componentDidMount: function () {
        this._renderMap();
    },

    _run: function (data, confinement) {
        if (confinement && !this._hasNaved) {
            var centerLngLat = new mapboxgl.LngLat(parseFloat(confinement.center.coordinates[0]), parseFloat(confinement.center.coordinates[1]));
            this._map.setZoom(confinement.default_zoom);
            this._map.setCenter(centerLngLat);
            this._hasNaved = true;
        }

        var markData = {
            type: "FeatureCollection",
            features: []
        };

        data.forEach(alert => {
            var alertPeriod = ewars.formatters.DATE_FORMATTER(alert.alert.trigger_end, alert.alert.interval_type);
            markData.features.push({
                type: "Feature",
                geometry: JSON.parse(alert.location.point || alert.location.centroid),
                properties: {
                    description: markerTemplate({
                        alarm_name: ewars.I18N(alert.alert.alarm_name),
                        location_name: ewars.I18N(alert.alert.location_name),
                        alert_uuid: alert.alert.uuid,
                        trigger_period: alertPeriod
                    })
                }
            })
        });

        this._map.getSource("data").setData(markData);

    },

    _renderMap: function () {
        this._popup = new mapboxgl.Popup();

        if (!this._map) {
            mapboxgl.accessToken = 'pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg';
            this._map = new mapboxgl.Map({
                container: this.refs.map,
                minZoom: 0,
                style: 'mapbox://styles/jduren/cim4smtop0114ccm3nf2wz0ya',
                maxZoom: 20,
                width: "100%",
                height: "490px",
                center: [-15.240329, -0.000000],
                zoom: 1.31,
                attributionControl: false,
                doubleClickZoom: false,
                scrollWheelZoom: false
            });


            this._map.on("style.load", function () {
                this._map.addSource("data", {
                    type: "geojson",
                    data: {
                        type: "FeatureCollection",
                        features: []
                    }
                });

                this._map.addLayer({
                    id: "markers",
                    interactive: true,
                    type: "circle",
                    source: "data",
                    paint: {
                        "circle-radius": 10,
                        "circle-color": "rgb(241, 139, 35)"
                    }
                });

                this._map.on("click", function (e) {
                    var features = this._map.queryRenderedFeatures(e.point, {
                        layer: "markers",
                        radius: 10,
                        includeGeometry: true
                    });

                    if (features.length) {
                        var feature = features[0];

                        new mapboxgl.Popup()
                            .setLngLat(feature.geometry.coordinates)
                            .setHTML(feature.properties.description)
                            .addTo(this._map);
                    }

                }.bind(this));


                this._map.on("mousemove", function (e) {
                    var features = this._map.queryRenderedFeatures(e.point, {
                        layers: ["markers"],
                        radius: 10
                    });


                    this._map.getCanvas().style.cursor = (features.length) ? "pointer" : "";
                }.bind(this));

                this.query("OPEN");

            }.bind(this));
        }
    },

    _swapFilter: function (data) {
        this.state.view = data.view;
        this._isLoaded = false;
        this.forceUpdate();
        this.query(data.view);
    },

    componentWillUnmount: function () {
        this._map.stop();
        this._map.remove();
        this._map = null;
    },

    render: function () {

        let groups = {};
        this.state.data.forEach(item => {
            if (groups[item.alert.alarm_id]) {
                groups[item.alert.alarm_id].push(item);
            } else {
                groups[item.alert.alarm_id] = [item];
            }
        });

        let alerts = [];
        for (let i in groups) {
            alerts.push(<AlarmGroup data={groups[i]}/>);
        }

        if (alerts.length <= 0) {
            alerts = <div className="placeholder">There are currently no alerts available</div>;
        }

        if (!this._isLoaded) alerts = <Spinner/>;

        return (
            <div className="ide-layout" style={{minHeight: 490, maxHeight: 490}}>
                <div className="ide-row map-wrapper">
                    <div className="ide-col" style={{maxHeight: 490, height: 490}}>
                        <div className="map" id="map" ref="map">

                        </div>
                    </div>
                    <div className="ide-col" style={{maxWidth: 490, background: "transparent"}}>
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 35}}>
                                <div className="ide-col">
                                    <div xmlns="http://www.w3.org/1999/xhtml" className="iw-tabs"
                                         style={{display: "block"}}>
                                        <Tab
                                            label="Active"
                                            active={this.state.view == "OPEN"}
                                            onClick={this._swapFilter}
                                            data={{view: "OPEN", count: 0}}/>
                                        <Tab
                                            label="Inactive"
                                            active={this.state.view == "CLOSED"}
                                            onClick={this._swapFilter}
                                            data={{view: "CLOSED", count: 0}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <div className="ide-panel ide-panel-absolute ide-scroll"
                                         style={{borderLeft: "1px solid #CCC"}}>

                                        {alerts}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default AlertMap;
