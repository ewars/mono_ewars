class Widget extends React.Component {
    static defaultProps = {
        noHeader: false
    };

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="widget">
                {!this.props.noHeader ?
                    <div className="widget-header"><span>{this.props.title}</span></div>
                    : null}
                <div className="body no-pad">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Widget;