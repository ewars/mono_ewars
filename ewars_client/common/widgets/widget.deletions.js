const query = {
    retracted_by: {eq: window.user.id},
    status: {eq: "PENDING"}
};

const joins = [
    "user:retracted_by:id:id,name,email"
];

class Deletions extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentWillMount() {
        ewars.tx("com.ewars.query", ["retraction", null, query, null, null, null, joins])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {

        let data;

        if (this.state.data.length <= 0) data = <p className="placeholder">{_l("NO_RETRACTIONS")}</p>;

        return data;
    }
}

export default Deletions;