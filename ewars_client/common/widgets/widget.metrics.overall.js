import Spinner from "../c.spinner";

var OverallMetricsWidget = React.createClass({
    getInitialState: function () {
        return {
            data: {
                alerts: null,
                responses: null,
                overdue: null
            }
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.metrics.overall", []).then(function (resp) {
            this.state.data = resp;
            if (this.isMounted()) this.forceUpdate();
        }.bind(this))
    },

    render: function () {

        let loader = <Spinner/>;

        return (
            <div className="ide-layout" style={{minHeight: 120}}>
                <div className="ide-row" style={{minHeight: 120}}>
                    <div className="ide-col">
                        <div className="indicator-item" style={{backgroundColor: "rgb(254, 191, 50)"}}>
                            <div className="title">Surveillance</div>
                            <div
                                className="value">{this.state.data.overdue != null ? this.state.data.overdue : loader}</div>
                            <div className="sub-title"># of active report types</div>
                        </div>
                    </div>
                    <div className="ide-col">
                        <div className="indicator-item" style={{backgroundColor: "rgb(241, 139, 35)"}}>
                            <div className="title">Alerts</div>
                            <div
                                className="value">{this.state.data.alerts != null ? this.state.data.alerts : loader}</div>
                            <div className="sub-title"># of active alerts</div>
                        </div>
                    </div>
                    <div className="ide-col">
                        <div className="indicator-item" style={{backgroundColor: "#ec4f4f"}}>
                            <div className="title">Responses</div>
                            <div
                                className="value">{this.state.data.responses != null ? this.state.data.responses : loader}</div>
                            <div className="sub-title"># of active responses</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default OverallMetricsWidget;
