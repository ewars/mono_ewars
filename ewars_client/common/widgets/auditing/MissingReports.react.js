var Moment = require("moment");

var Button = require("../../../common/components/ButtonComponent.react");

var ReportItem = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        var reportDueDate = Moment(this.props.data[0]).format("LL");
        var locationName = ewars.formatters.I18N_FORMATTER(this.props.data[2]);

        return (
            <div className="missing-report">
                <div className="report_date">Due On: {reportDueDate}</div>
                <div className="location_name">in: {locationName}</div>
                <div className="responsible"></div>
            </div>
        )
    }
});

var ReportSet = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        var formName = ewars.formatters.I18N_FORMATTER(this.props.data.form_name);

        var reports = _.map(this.props.data.missing, function (missed) {
            return <ReportItem data={missed}/>
        }, this);

        return (
            <div className="report-set">
                <h3>{formName}</h3>
                <div className="missing-reports-list">
                    {reports}
                </div>
            </div>
        )
    }
});

var MissingReports = React.createClass({
    getInitialState: function () {
        return {
            data: []
        }
    },

    componentWillMount: function () {
        this._init(this.props);
    },

    _init: function (props) {
        ewars.tx("com.ewars.account.reporting.missing", [props.timeFilter, props.formId])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    copmonentWillReceiveProps: function (nextProps) {
        this._init(nextProps);
    },

    _swapFilter: function (data) {
        this.state.view = data.view;
        if (this.isMounted()) this.forceUpdate();
    },

    _render: function () {

    },

    render: function () {

        var reportSets = _.map(this.state.data, function (item) {
            return <ReportSet data={item}/>;
        }, this);

        return (
            <div className="widget">
                <div className="body">
                    {reportSets}
                </div>
            </div>
        )
    }
});

module.exports = MissingReports;