var SubNavComponent = React.createClass({
    _handleClick: function (e) {
        e.preventDefault();
        this.props.onClick(this.props.view);
    },

    render: function () {
        var icon = "fal " + this.props.icon;

        var isActive = false;
        var className;
        if (this.props.active) className = "active";

        return (
            <li>
                <a href="#" className={className} onClick={this._handleClick}>
                    <div className="nav-icon"><i className={icon}></i></div>
                &nbsp;{this.props.label}
                </a>
            </li>
        )
    }
});

module.exports = SubNavComponent;
