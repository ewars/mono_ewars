import Form from "./Form.react";
import FieldDefinition from "./FieldDefinition.react";

function _findParentNode(parentName, childObj) {
    var testObj = childObj.parentNode;
    var count = 1;
    while (testObj.getAttribute('name') != parentName) {
        alert("My name is " + testObj.getAttribute("name") + ". Lets try moving up one level");
        testObj = testObj.parentNode;
        count++;
    }
    alert("Fainlly");
}

/**
 * Comparator used for sorting the fields in the definition by
 * their order property
 * @param a
 * @param b
 * @returns {number}
 * @private
 */
function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields) {

    var fields = [];
    for (var token in sourceFields) {
        var field = sourceFields[token];
        field.name = token;

        if (field.fields) {
            var children = _getFieldsAsArray(field.fields);
            field.children = children;
        }

        fields.push(field);
    }

    return fields.sort(_comparator);
}

function _getArrayAsObject(sourceArray) {
    var fields = {};

    for (var index in sourceArray) {
        var field = sourceArray[index];
        fields[field.name] = field;
        fields[field.name].order = index;

        if (field.children) {
            fields[field.name].fields = _getArrayAsObject(field.children);
        }

        delete fields[field.name].name;
    }

    return fields;
}

var FormDefinitionComponent = React.createClass({

    _data: null,
    _over: null,
    _dragged: null,
    _placeholder: null,
    _nodePlacement: null,

    _tmpNumber: 0,

    getInitialState: function () {
        return {
            dirty: false,
            fields: {},
            data: [],
            definition: null,
            activeDetails: null,
            view: "edit"
        }
    },

    propTypes: {
        onChange: React.PropTypes.func.isRequired,
        onDelete: React.PropTypes.func.isRequired,
        definition: React.PropTypes.object.isRequired
    },

    componentWillMount: function () {
    },

    /**
     * Handles when a field definitions options panel is opened
     * @param idx
     * @private
     */
    _handlePropsExpose: function (idx) {
        this.setState({
            activeDetails: idx
        })
    },

    /**
     * Calls the onFieldChange event set by the parent when something in the definition changes.
     * Note: We always fire back the entire definition, it keeps it simpler for handling updates
     * in the parent.
     *
     * @param fields {obj} The definition object
     * @private
     */
    _updateOrdersFromData: function (fields) {
        var newData = _getArrayAsObject(this._data);

        this.props.onChange('definition', newData);
    },

    /**
     * Handles adding a new field to the definition
     * @private
     */
    _addField: function () {
        if (!this._data) this._data = [];
        this._data.push({
            name: "untitled" + this._tmpNumber,
            label: "Untitled Field " + this._tmpNumber,
            type: "text",
            order: null,
            required: false
        });

        this._tmpNumber++;

        this._updateOrdersFromData(this._data);
    },

    /**
     * Handle when a property of a fields options changes within the definition
     * @param fieldName {string} The name of the field
     * @param index {int} the index of the field  in the _data array
     * @param name {string} The name of the option for the field being updated
     * @param value {*} The new value of the option
     * @private
     */
    _handleOptionsChange: function (fieldName, index, option, value) {
        // Fix booleans
        if (value == "true") value = true;
        if (value == "false") value = false;

        this._data[index][option] = value;
        this._updateOrdersFromData(this._data);
    },

    /**
     * Handle when a field is deleted from the definition
     * @param fieldName
     * @private
     */
    _handleFieldDestroy: function (fieldName) {
        this.props.onFieldDelete(fieldName);
    },

    _showPreview: function () {
        this.setState({
            view: "preview"
        })
    },

    _hidePreview: function () {
        this.setState({
            view: "edit"
        })
    },

    _onFieldMove: function (originalIndex, newIndex) {
        if (newIndex < 0) return;

        if (newIndex >= this._data.length) {
            var k = newIndex - this._data.length;
            while ((k--) + 1) {
                this._data.push(undefined);
            }
        }

        this._data.splice(newIndex, 0, this._data.splice(originalIndex, 1)[0]);

        this._updateOrdersFromData(this._data);

    },

    _onAddChild: function (fieldIndex) {
        if (!this._data[fieldIndex].children) this._data[fieldIndex].children = [];
        this._data[fieldIndex].children.push({
            type: "text",
            name: "untitled_field_" + this._tmpNumber,
            label: "Untitled Field " + this._tmpNumber
        });

        this._tmpNumber++;

        this._updateOrdersFromData(this._data);
    },

    /**
     * Works reursively to resolve a hierarchical structure for the form definition in order
     * to display the content in a meaningful way.
     * @returns {*}
     * @private
     * @param sourceFields {Array} Fields to process
     */
    _processFields: function (sourceFields) {
        var fields;

        if (sourceFields.length > 0) {
            fields = [];
            for (var idx in sourceFields) {
                var field = sourceFields[idx];
                var exposed = false;
                if (this.state.activeDetails == idx) exposed = true;

                var children = {};
                if (field.children) {
                    children = this._processFields(_getFieldsAsArray(field.children));
                }

                var canAddChilds = false;
                if (field.type == "row") canAddChilds = true;
                if (field.type == "matrix") canAddChilds = true;

                fields.push(
                    <FieldDefinition
                        definition={field}
                        form={this.props.definition}
                        name={field.name}
                        order={field.order}
                        key={field.name}
                        index={idx}
                        canAddChilds={canAddChilds}
                        exposed={exposed}
                        onChange={this._handleOptionsChange}
                        onShowProps={this._handlePropsExpose}
                        onMove={this._onFieldMove}
                        onAddChild={this._onAddChild}
                        onDelete={this._handleFieldDestroy}>
                        {children}
                    </FieldDefinition>
                )

            }
        } else {
            fields = (
                <p className="box-placeholder">There are currently no fields configured,
                    <a href="#" onClick={this._addField}>Add one</a>
                </p>
            )
        }

        return fields;
    },

    render: function () {

        this._placeholder = document.createElement("div");
        this._placeholder.className = "placeholder";

        this._data = _getFieldsAsArray(this.props.definition);
        var fields = this._processFields(this._data);

        var view;
        if (this.state.view == "edit") {
            view = (
                <div className="widget">
                    <div className="widget-header">
                        <span>Form Edit</span>

                        <div className="btn-group pull-right">
                            <button className="btn btn-default btn-sm" onClick={this._addField}>
                                <i className="fal fa-plus"></i>
                            </button>
                            <button className="btn btn-default btn-sm" onClick={this._showPreview}>
                                <i className="fal fa-eye"></i>
                            </button>
                        </div>
                    </div>
                    <div className="body no-pad">
                        <div className="form-definition-editor">
                            {fields}
                        </div>
                    </div>
                </div>
            )
        }

        if (this.state.view == "preview") {
            view = (
                <div className="form-definition">
                    <div className="toolbar">
                        <span>Form Preview</span>

                        <div className="btn-group pull-right">
                            <button className="btn btn-default btn-sm" onClick={this._hidePreview}>
                                <i className="fal fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <Form
                        readOnly={false}
                        definition={this.props.definition}
                        onCancel={this._hidePreview}/>
                </div>
            )
        }

        return view;
    }
});

export default FormDefinitionComponent;
