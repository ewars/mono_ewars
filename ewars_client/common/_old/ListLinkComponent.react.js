var ListLinkComponent = React.createClass({
    propTypes: {
        onClick: React.PropTypes.func.isRequired,
        data: React.PropTypes.object
    },

    getInitialState: function () {
        return {};
    },

    _handleClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {

        var iconString = "fa";
        if (this.props.icon) {
            iconString += " " + this.props.icon;
        }

        return (
            <li>
                <a href="#" className="list-link" onClick={this._handleClick}>
            {this.props.icon ?
                <span>
                    <i className={iconString}></i>
                &nbsp;
                </span>
                : null }
                    {this.props.label}
                </a>
                {this.props.children}
            </li>
        )
    }

});

module.exports = ListLinkComponent;
