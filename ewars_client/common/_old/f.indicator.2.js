const SelectField = require("./SelectField");

const CONSTANTS = require("../../constants");

import USERS from "../../forms/indicators/USERS";
import OLD_ALERTS from "../../forms/indicators/OLD_ALERTS";
import ALERTS from "../../forms/indicators/ALERTS";
import ASSIGNMENTS from "../../forms/indicators/ASSIGNMENTS";
import DEVICES from "../../forms/indicators/DEVICES";
import FORM_SUBMISSIONS from "../../forms/indicators/FORM_SUBMISSIONS";
import LOCATIONS from "../../forms/indicators/LOCATIONS";
import TASKS from "../../forms/indicators/TASKS";
import FORMS from "../../forms/indicators/FORMS";

const isObject = (val) => {
    if (val === null) return false;
    if (val === undefined) return false;
    if (JSON.stringify(val).indexOf("{") == 0) return true;
    return false;
}


const SYSTEM_INDS = {
    USERS: USERS,
    ALERTS: ALERTS,
    OLD_ALERTS: OLD_ALERTS,
    ASSIGNMENTS: ASSIGNMENTS,
    DEVICES: DEVICES,
    FORM_SUBMISSIONS: FORM_SUBMISSIONS,
    LOCATIONS: LOCATIONS,
    TASKS: TASKS,
    FORMS: FORMS
};

var SYSTEM_TYPES = {
    ALARM: null,
    FORM: null,
    ORGANIZATION: null,
    ALERT: null,
    TASK: null
};

const resourceId = {
    "form": "id",
    alarm: "uuid",
    organization: "uuid"
};

let LABELS = {
    form: "FORM",
    organization: "ORGANIZATION",
    metric: "METRIC",
    submitter_type: "SUBMITTER_TYPE",
    form_id: "FORM",
    organization_id: "ORGANIZATION",
    source: "SOURCE"
};

const queryDef = {
    form: {
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"}
            },
            orderby: {"name.en": "ASC"}
        }
    },
    alarm: {
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            orderby: {"name.en": "ASC"}
        }
    },
    organization: {
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            orderby: {"name.en": "ASC"}
        }
    }

};

const itemStyle = {
    marginTop: 8
};

var ALARM_CONFIG = {
    optionsSource: {
        resource: "alarm",
        valSource: "uuid",
        labelSource: "name",
        query: (function () {
            var q = {
                status: {eq: true}
            };

            if (window.user) {
                if ([CONSTANTS.SUPER_ADMIN, CONSTANTS.GLOBAL_ADMIN, CONSTANTS.INSTANCE_ADMIN].indexOf(window.user.role) < 0) {
                    q.account_id = window.user.account_id;
                }
            }

            return q;
        })()
    }
};

const COMPARATORS = [
    ["EQ", "Equal to"],
    ["NEQ", "Not equal to"]
];


var TreeNodeComponent = React.createClass({
    _hasLoaded: false,

    getInitialState: function () {
        return {
            children: [],
            showChildren: false
        };
    },

    onCaretClick: function (e) {
        e.stopPropagation();
        e.preventDefault();
        this.toggleCaret();
    },

    toggleCaret: function () {
        if (this._hasLoaded) {
            this.setState({
                showChildren: this.state.showChildren ? false : true
            });
            return;
        }

        var self = this;
        ewars._connect().then(function (session) {
            session.call("com.ewars.indicators", [self.props.data.id])
                .then(function (resp) {
                    this._hasLoaded = true;

                    if (resp.d.length > 0) {
                        this.setState({
                            children: resp.d,
                            showChildren: true
                        })
                    } else {
                        this.forceUpdate();
                    }

                }.bind(self))
        })

    },

    _onLabelClick: function (e) {
        e.stopPropagation();
        e.preventDefault();

        if (this.props.data.type == "node") {
            this.props.onNodeSelect(this.props.data);
        } else {
            this.toggleCaret();
        }
    },

    _hasChildren: function () {
        if (this.props.data.child_count > 0) return true;
        return false;
    },

    _hasIndicators: function () {
        if (this.props.data.indicator_count > 0) return true;
        return false;
    },

    _processChildren: function () {
        var childs = [];

        for (var i in this.state.children) {
            var child = this.state.children[i];

            childs.push(
                <TreeNodeComponent
                    index={i}
                    onNodeSelect={this.props.onNodeSelect}
                    data={child}/>
            )
        }

        return childs;
    },

    render: function () {

        var childs;
        if (this._hasChildren() || this._hasIndicators()) {
            childs = this._processChildren();
        }

        var iconClass = "fal fa-circle";
        if (this.props.data.type == "folder") iconClass = "fal fa-caret-right";
        if (this.props.data.type == "folder" && this._hasLoaded && this.state.showChildren) iconClass = "fal fa-caret-down";

        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.data.name);

        return (
            <li>
                <div className="node-control" onClick={this.onCaretClick}><i className={iconClass}></i></div>
                <div className="label" onClick={this._onLabelClick}>{nodeName}</div>
                {this.state.showChildren ?
                    <div className="children">
                        <ul>
                            {childs}
                        </ul>
                    </div>
                    : null}
            </li>
        )
    }
});

class FilterControl extends React.Component {
    constructor(props) {
        super(props)
    }

    _onChange = (prop, value) => {
        let settings = ewars.copy(this.props.settings);
        if (value) {
            settings[prop] = value;
        } else {
            if (settings[prop]) delete settings[prop];
        }
        this.props.onChange(settings);
    };

    render() {
        let label = LABELS[this.props.data[0]];
        let valOpts = {};

        if (this.props.data[1]) {
            if (this.props.data[1].resource) {
                valOpts = queryDef[this.props.data[1].resource]
            } else {
                let options = this.props.data[1].map(item => {
                    return [item, _l(item)]
                });
                valOpts.options = options;
            }
        }

        let value = this.props.settings[this.props.data[0]] || null;

        return (
            <div style={itemStyle}>
                <div className="ide-row">
                    <div className="ide-col" style={{padding: "5px 5px 5px 0"}}>{label}</div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <SelectField
                            name={this.props.data[0]}
                            onUpdate={this._onChange}
                            value={value}
                            config={valOpts}/>
                    </div>
                </div>
            </div>
        )
    }
}

class IndicatorControl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: []
        }
    }

    _filterChange = (data, prop, value) => {
        let settings = ewars.copy(this.props.data);
        if (!isObject(settings)) {
            settings = {};
        }
        settings[prop] = value;
        this.props.onChange(settings);
    };

    render() {

        let FORM_DEF = SYSTEM_INDS[this.props.itype];

        if (!FORM_DEF) {
            return <div className="ind-options">
                <div className="placeholder">Loading...</div>
            </div>
        }

        let IndForm = this.props.Form || Form;

        return (
            <IndForm
                definition={FORM_DEF}
                data={this.props.data}
                readOnly={false}
                updateAction={this._filterChange}/>
        )
    }
}

var Handle = React.createClass({
    render: function () {
        var name = "No Selection";

        if (this.props.value) {
            name = ewars.I18N(this.props.value.name);
        }
        return (
            <div className="handle" onClick={this.props.onClick}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>{name}</td>
                        <td width="20px" className="icon">
                            <i className="fal fa-caret-down"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});

class IndicatorNode extends React.Component {
    _isLoaded = false;

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            show: false
        }
    }

    _onClick = () => {
        if (!this._isLoaded && this.props.data.context == "FOLDER") {
            ewars.tx("com.ewars.indicators", [this.props.data.id])
                .then(function (resp) {
                    this._isLoaded = true;
                    this.setState({
                        data: resp,
                        show: !this.state.show
                    })
                }.bind(this));
            return;
        }

        this.setState({
            show: !this.state.show
        })
    };

    render() {
        var className = "item locale-select";
        if (this.props.value == this.props.data.uuid) className += " active";

        var name = ewars.I18N(this.props.data.name);

        var children;
        if (this.state.show && this._isLoaded) {
            children = this.state.data.map(function (item) {
                return <IndicatorNode
                    onSelect={this.props.onSelect}
                    hideInactive={true}
                    key={item.uuid || item.id}
                    data={item}/>
            }.bind(this))
        } else {
            children = (
                <i className="fal fa-cog fa-spin"></i>
            )
        }

        let iconClass;
        if (this.props.data.context == "FOLDER") iconClass = "fal fa-folder";
        if (this.props.data.context == "FOLDER" && this.state.show) iconClass = "fal fa-folder-open";

        var handleClass = "locale-tree-node";

        var showSelect = false;
        if (this.props.data.context == "INDICATOR") showSelect = true;
        if (showSelect) handleClass += " has-button";

        let handler = this._onClick;
        if (this.props.data.context == "INDICATOR") handler = () => {
            this.props.onSelect(this.props.data)
        };

        return (
            <div className={className}>
                <div className={handleClass} onClick={handler}>
                    {iconClass ?
                        <div className="expander"><i className={iconClass}></i></div>
                        : null}
                    <div className="labeler no-pad">{name}</div>
                </div>
                {this.state.show ?
                    <div className="locale-tree-node-children" style={{display: "block"}}>
                        {children}
                    </div>
                    : null}
            </div>
        )
    }
}

class IndicatorField extends React.Component {
    static defaultProps = {
        config: {
            showSystem: true
        },
        name: null
    }

    constructor(props) {
        super(props);

        this.state = {
            showTree: false,
            data: [],
            search: "",
            nodeName: "NO_SELECTION",
            nodeUUID: null,
            indicator: null
        }

        if (this.props.value) {

        }
    }

    _onSearchChange = (e) => {
        if (e.target.value == "") {
            ewars.tx("com.ewars.indicators", [null])
                .then(function (resp) {
                    this._isLoaded = true;
                    this.setState({
                        search: "",
                        data: resp
                    })
                }.bind(this));
        } else {
            if (e.target.value.length > 3) {
                this._search(e.target.value);
            } else {
                this.setState({
                    ...this.state,
                    search: e.target.value
                })
            }
        }
    };

    _clearSearch = () => {
        ewars.tx("com.ewars.indicators", [null])
            .then(function (resp) {
                this._isLoaded = true;
                this.setState({
                    search: "",
                    data: resp
                })
            }.bind(this));
    };

    _search = (term) => {
        let filter = {
            "name.en": {LIKE: term}
        };
        if (!this.props.showSystem) filter.itype = {eq: "DEFAULT"};
        ewars.tx("com.ewars.query", ["indicator", ["uuid", "name", "itype", "definition", "status"], filter, null, null, null, null])
            .then(resp => {
                this.setState({
                    search: term,
                    data: resp.map(item => {
                        return {
                            ...item,
                            context: "INDICATOR"
                        }
                    })
                })
            })
    };

    _toggle = () => {

        if (!this._isLoaded) {
            ewars.tx("com.ewars.indicators", [null])
                .then(function (resp) {
                    this._isLoaded = true;
                    this.setState({
                        data: resp,
                        showTree: !this.state.showTree
                    })
                }.bind(this));
            return;
        }

        this.setState({
            showTree: !this.state.showTree
        })
    };

    componentWillMount() {

        if (isObject(this.props.value) && this.props.value.uuid) {
            this._init(this.props.value.uuid);
        } else {
            this._init(this.props.value);
        }
    };

    componentWillReceiveProps(nextProps) {

        if (this.state.node) {
            if (isObject(nextProps.value)) {
                // This is a dict based indicator
                if (nextProps.value.uuid != this.state.node.uuid) this._init(nextProps.value.uuid);
            } else {
                if (this.state.node.uuid != nextProps.value) this._init(nextProps.value);
            }
        } else {
            if (isObject(nextProps.value)) this._init(nextProps.value.uuid);
            if (!isObject(nextProps.value)) this._init(nextProps.value);
        }
    };

    _init = (value) => {

        if (value && value.context) {
            // This is a legacy indicator
            this.setState({
                nodeName: null,
                nodeUUID: null,
                node: null
            });
            return;
        }
        if (value && !isObject(value)) {
            ewars.tx("com.ewars.resource", ["indicator", value, null, null])
                .then((resp) => {
                    this.setState({
                        nodeName: ewars.I18N(resp.name),
                        nodeUUID: resp.uuid,
                        node: resp
                    })
                })
        } else if (value && isObject(value)) {
            if (value.itype == "CUSTOM") {
                this.state.nodeName = value.name;
                this.state.nodeUUID = null;
                this.state.node = value;
            } else {
                ewars.tx("com.ewars.resource", ["indicator", value.uuid, null, null])
                    .then(function (resp) {
                        if (resp != null) {
                            this.state.nodeName = ewars.I18N(resp.name);
                            this.state.nodeUUID = resp.uuid;
                            this.state.node = resp
                        }
                    }.bind(this))
            }
        }
    };

    _onNodeSelection = (node) => {

        if (node.context != "INDICATOR") return;

        this.setState({
            nodeName: ewars.formatters.I18N_FORMATTER(node.name),
            nodeUUID: node.uuid || null,
            node: node,
            showTree: false
        });

        let name = this.props.config.nameOverride || this.props.name;

        var path = this.props.config ? this.props.config.path : null;
        if (node.itype == "DEFAULT") {
            this.props.onChange(name, node.uuid, path, node);
        } else {
            this.props.onChange(name, {uuid: node.uuid}, path, node);
        }
    };

    _systemIndUpdate = (data) => {

        let name = this.props.config.nameOverride || this.props.name;
        this.props.onChange(name, data, this.props.config ? this.props.config.path : null, {itype: "SYSTEM"});
    };

    render() {
        let isConfigurable = false,
            rootNodes;
        if (this.state.node) {
            isConfigurable = this.state.node.itype != "DEFAULT";
        }


        if (this._isLoaded && this.state.showTree) {
            rootNodes = this.state.data.map((item) => {
                return <IndicatorNode data={item} onSelect={this._onNodeSelection}/>
            })
        }

        return (
            <div className="depth">
                <div ref="selector" className="ew-select locale-selector">
                    <Handle
                        onClick={this._toggle}
                        readOnly={this.props.readOnly}
                        value={this.state.node}/>
                    {this.state.showTree ?
                        <div className="ew-select-data">
                            <div className="locale-search">
                                <div className="input">
                                    <input ref="localeInput" type="text" placeholder="Search indicators..."
                                           value={this.state.search}
                                           onChange={this._onSearchChange}/>
                                    {this.state.search.length > 0 ?
                                        <div className="search-button" onClick={this._clearSearch}><i
                                            className="fal fa-times-circle"></i></div>
                                        : null}
                                </div>
                            </div>
                            {rootNodes}
                        </div>
                        : null}


                </div>
                {isConfigurable ?
                    <div className="add-on">
                        <IndicatorControl
                            data={this.props.value}
                            readOnly={false}
                            Form={this.props.Form}
                            itype={this.state.node.itype || null}
                            onChange={this._systemIndUpdate}
                            definition={this.state.node.definition}/>
                    </div>
                    : null}
            </div>
        )
    }
}

export default IndicatorField;

