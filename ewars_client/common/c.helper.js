import Spinner from "./Spinner.react";

var Helper = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        return {
            showInfo: false,
            data: null
        }
    },

    _toggle: function () {
        if (this.props.token.indexOf("/") >= 0) {
            window.open(this.props.token);
            return;
        }

        if (this.state.data) {
            this._show();
        } else {
            ewars.tx("com.ewars.guidance.action", ["GET", this.props.token, {lang: "en"}])
                .then(function (resp) {
                    this.state.data = resp;
                    this.forceUpdate();
                    this._show();
                }.bind(this))
        }
    },

    _show: function () {
        ewars.prompt("fa-question-circle", this.state.data.title, this.state.data.content, function () {
        })
    },

    render: function () {

        return (
            <div ref="helper" className="helper" onClick={this._toggle}>
                <div className="handle">
                    <i className="fal fa-question-circle"></i>
                </div>
            </div>
        )
    }
});

export default Helper;
