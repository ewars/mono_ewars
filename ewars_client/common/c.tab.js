const ICON_STYLE = {
    maxWidth: "20px",
    flexBasis: "0px",
    flexGrow: 1,
    padding: "8px 5px",
    textAlign: "center",
    borderTopLeftRadius: "3px"
}

const LABEL_STYLE = {
    wordBreak: "keep-all",
    flex: "0 0 auto",
    padding: "8px"
}

class Tab extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = (e) => {
        e.preventDefault();
        this.props.onClick(this.props.data);
    };

    render() {
        let className = "panelsbar-btn";
        if (this.props.active) className += " panelsbar-btn-active";

        let icon, label;
        if (this.props.icon) icon = <i className={"fal " + this.props.icon}></i>;
        if (this.props.label) label = __(this.props.label);

        return (
            <div className={className}
                onClick={this._onClick}
                xmlns="https://www.w3.org/19999/xhtml"
                style={{padding: "0px"}}>
                <div className="ide-row">
                    {icon ?
                        <div className="ide-col" style={ICON_STYLE}>{icon}</div>
                    : null}
                    <div className="ide-col" style={LABEL_STYLE}>{label}</div>
                </div>
            </div>
        )
    }
}

export default Tab;
