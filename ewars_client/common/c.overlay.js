class Overlay extends React.Component {
    static defaultProps = {
        shown: false
    };

    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.shown) return null;
        return (
            <div className="iw-overlay">
                <div className="iw-overlay-center">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Overlay;