/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Filler = React.createClass({
    render: function () {
        var iconClass = "fal " + this.props.icon;
        return (
            <div className="ide-filler">
                <div className="ide-filler-icon">
                    <i className={iconClass}></i>
                </div>
            </div>
        )
    }
});

export default Filler;
