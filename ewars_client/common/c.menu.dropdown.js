var MenuItem = React.createClass({
    _onClick: function () {
        this.props.data.handler();
        this.props.onClick();
    },

    render: function () {
        var icon;
        if (this.props.data.icon) {
            icon = "fal " + this.props.data.icon;
        }

        return (
            <div className="ide-dd-item" onClick={this._onClick}>
                {icon ?
                    <div className="ide-dd-item-icon"><i className={icon}></i></div>
                    : null}
                <div className="ide-dd-item-label">{this.props.data.label}</div>
            </div>
        )
    }
});

var DropDownMenu = React.createClass({
    getInitialProps: function () {
        return {
            isExpanded: false
        }
    },

    getInitialState: function () {
        return {
            isExpanded: false
        }
    },

    hide: function () {
        this.setState({isExpanded: false});
        document.removeEventListener("click", this.hide);
    },

    _onHandleClick: function () {
        this.state.isExpanded = this.state.isExpanded ? false : true;
        if (this.state.isExpanded) {
            document.addEventListener("click", this.hide);
        } else {
            document.removeEventListener("click", this.hide);
        }
        this.forceUpdate();
    },

    _onClick: function () {
        this.state.isExpanded = false;
        document.removeEventListener("click", this.hide);
        this.forceUpdate();
    },

    render: function () {
        var iconName = "fal " + this.props.icon;

        var items;
        items = _.map(this.props.menu, function (menuItem) {
            return <MenuItem
                data={menuItem}
                onClick={this._onClick}/>
        }, this);

        return (
            <div className="ide-dropdown">
                <div className="handle" onClick={this._onHandleClick}>
                    <div className="ide-dd-icon"><i className="fal fa-plus"></i></div>
                    {this.props.label ?
                        <div className="ide-dd-label">{this.props.label}</div>
                        : null}
                    <div className="ide-dd-caret"><i className="fal fa-caret-down"></i></div>
                </div>
                {this.state.isExpanded ?
                    <div className="ide-dd-menu">
                        {items}
                    </div>
                    : null}
            </div>
        )
    }
});

export default DropDownMenu;
