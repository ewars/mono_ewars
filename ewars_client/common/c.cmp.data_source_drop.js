import FORM_SOURCE_VARIABLE from "./forms/form.source.variable";

const ACTIONS = [
    ["fa-pencil", "EDIT"],
    ["fa-trash", "REMOVE"]
]

const NONEDIT_ACTIONS = [
    ["fa-trash", "REMOVE"]
]

const VAR_BLOCK = {
    border: "1px solid #CCCCCC",
    padding: "8px"

}

class Variable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            id: null,
            name: "Loading...",
            var: null,
            shown: false,
            isValid: true
        };

        this._init(this.props.data);
    }

    componentWillReceiveProps(nextProps) {
        this.state.var = nextProps.data[2];
    }

    _init = (data) => {
        // validate config for the source

        if (data[0] == "I") {
            ewars.tx("com.ewars.resource", ["indicator", data[1], null, null])
                .then((resp) => {
                    this.setState({
                        data: resp,
                        name: __(resp.name),
                        var: data[2]
                    })
                })
        }

        if (data[0] == "F") {
            ewars.tx("com.ewars.form.field", [data[1]])
                .then((resp) => {
                    this.setState({
                        name: __(resp[2]),
                        var: data[2]
                    })
                })
        }

        if (data[0] == "D") {
            // Data source column
        }

        if (data[0] == "V") {
            this.state.name = data[2] || "VAR_";
        }

    };

    _loadIndicator = () => {

    };

    _onAction = (action) => {
        if (action == "REMOVE") {
            this.props.remove(this.props.index)
        }

        if (action == "EDIT") {
            this.setState({
                shown: !this.state.shown
            })
        }
    };

    _onChange = (prop, value) => {
        this.props.onChange(this.props.index, prop, value);
    };

    render() {
        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            {this.state.var}: {this.state.name}
                        </ewars.d.Cell>
                        <ewars.d.Cell width="60px" style={{display: "block"}}>
                            <ewars.d.ActionGroup
                                actions={ACTIONS}
                                onAction={this._onAction}
                                right={true}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.shown ?
                    <div style={VAR_BLOCK}>
                        <ewars.d.SystemForm
                            definition={FORM_SOURCE_VARIABLE}
                            data={{
                                ...this.props.data[3],
                                var: this.state.var
                            }}
                            enabled={true}
                            vertical={true}
                            onChange={this._onChange}/>

                    </div>
                : null}
            </div>
        )
    }
}

const TYPES = [
    ["INDICATOR", "<uuid>", null, {}],
    ["FIELD", "<form_id>.<field></field>", null, {}],
    ["DS", "<ds_id>.<column|key>", null, {}],
    ["VAR", "<value>", null, {}]
]

class DataSourceDrop extends React.Component {
    static defaultProps = {
        name: null,
        value: []
    };

    constructor(props) {
        super(props);

        this.state = {
            data: []
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    };

    _onDrop = (e) => {
        e.preventDefault();
        let node = e.dataTransfer.getData("n");

        if (node.indexOf("[") == 0) {
            node = JSON.parse(node);
        }


        let data = ewars.copy(this.props.value || []);
        if (node[0] == "F") {
            data.push(["F", node[1], node[2], {}]);
        }

        if (node[0] == "I") {
            data.push(["I", node[1], node[2], {}]);
        }

        if (node[0] == "V") {
            data.push(["V", null, "NEW_VAR", {}])
        }


        this._el.style.display = "none";

        this.props.onChange(this.props.name, data);

    };

    _onDragOver = (e) => {
        e.preventDefault();
        this._el.style.display = "block";
    };

    _onChange = (index, prop, value) => {
        let data = ewars.copy(this.props.value);
        console.log(data)
        if (prop == "var") {
            data[index][2] = value;
        } else {
            data[index][3][prop] = value;
        }

        this.props.onChange(this.props.name, data);
    };

    _remove = (index) => {
        let data = ewars.copy(this.props.value);
        data.splice(index, 1);

        this.props.onChange(this.props.name, data);
    };

    render() {
        let value = this.props.value;
        if (_isNullOrEmpty(value)) {
            return (
                <div ref={(el) => {
                    this._el = el
                }} style={{...DROPZONE, ...DROPZONE_BASE}} onDrop={this._onDrop} onDragOver={this._onDragOver}>
                    <div className="placeholder">Drop data source to start</div>
                </div>
            )
        }

        return (
            <div className="block-tree" style={{position: "relative", border: "1px solid #CCCCCC", minHeight: "150px"}}
                 onDragOver={this._onDragOver}>
                {value.map((item, index) => {
                    return <Variable
                        data={item}
                        index={index}
                        remove={this._remove}
                        onChange={this._onChange}/>
                })}
                <div ref={(el) => {
                    this._el = el
                }} style={DROPZONE} onDrop={this._onDrop} onDragOver={this._onDragOver}>
                    <div className="placeholder">Drop data source to start</div>
                </div>
            </div>
        )
    }
}

const DROPZONE_BASE = {
    position: "relative",
    height: "150px",
    width: "100%",
    display: "block"
}

const DROPZONE = {
    display: "none",
    height: "100%",
    width: "100%",
    background: "rgba(0,0,0,0.9)",
    position: "absolute",
    top: 0,
    lefT: 0
}

export default DataSourceDrop;
