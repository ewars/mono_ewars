import Spinner from "../Spinner.react";

var ListItem = React.createClass({
    _selectItem: function () {
        this.props.onSelect(this.props.index, this.props.data);
    },

    render: function () {
        var className = "item-list-item";
        if (this.props.selected == this.props.index) className += " active";

        var label = ewars.I18N(this.props.data[this.props.config.displayValue]);

        return (
            <div className={className} onClick={this._selectItem}>{label}</div>
        )
    }
});

var LinkedListsComponent = React.createClass({
    _isLoaded: false,

    getInitialState: function () {
        return {
            data: [],
            selected: [],
            selectedItem: null,
            selectedIndex: null,
            sourceSelectedItem: null,
            sourceSelectedIndex: null,
            hidden: []
        }
    },

    componentWillMount: function () {
        if (this.props.config.resource) {
            ewars.tx("com.ewars.query", [this.props.config.resource, this.props.config.select, this.props.config.query, null, null, null, null])
                .then(function (resp) {
                    this.state.data = resp;
                    this._isLoaded = true;
                    _.each(resp, function (item, index) {
                        if (item.features.ALERT) {
                            if (this.props.value.indexOf(item.id) >= 0) {
                                this.state.selected.push(item);
                                this.state.hidden.push(index);
                            }
                        }
                    }, this);
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        } else {
            this.state.data = ewars.copy(this.props.config.options);
            this._isLoaded = true;
            this.state.data.forEach(function (item, index) {
                if (item.features.ALERT) {
                    if (this.props.value.indexOf(item[0]) >= 0) {
                        this.state.selected.push(item);
                        this.state.hidden.push(index);
                    }
                }
            }.bind(this));
            if (this.isMounted()) this.forceUpdate();
        }
    },

    _init: function () {
        this.state.selected = [];
        _.each(this.state.data, function (item) {
            if (this.props.value.indexOf(item.id) >= 0) {
                this.state.selected.push(item);
                this.state.hidden.push(item.id);
            }
        }, this);
    },

    _moveRight: function () {
        if (this.state.sourceSelectedIndex == null) {
            ewars.growl("Please select a form first");
        } else {
            this.state.selected.push(this.state.sourceSelectedData);
            this.state.hidden.push(this.state.sourceSelectedIndex);
            this.state.sourceSelectedData = null;
            this.state.sourceSelectedIndex = null;
            var values = _.map(this.state.selected, function (item) {
                return item.id;
            });
            this.props.onUpdate(this.props.name, values);
        }
    },

    _remove: function () {
        if (this.state.selectedIndex == null) {
            ewars.growl("Please select a form");
        } else {
            this.state.selected.splice(this.state.selected.indexOf(this.state.selectedData), 1);
            this.state.hidden.splice(this.state.hidden.indexOf(this.state.selectedIndex), 1);
            this.state.selectedIndex = null;
            this.state.selectedData = null;
            var values = _.map(this.state.selected, function (item) {
                return item.id
            });
            this.props.onUpdate(this.props.name, values);
        }
    },

    _selectSource: function (index, data) {
        this.setState({
            sourceSelectedIndex: index,
            sourceSelectedData: data
        });
    },

    _selectItem: function (index, data) {
        this.setState({
            selectedItem: data,
            selectedIndex: index
        })
    },

    render: function () {

        var selectables = _.map(this.state.data, function (item, index) {
            if (this.state.hidden.indexOf(index) < 0 && item.features.ALERT) {
                return <ListItem
                    data={item}
                    selected={this.state.sourceSelectedIndex}
                    config={this.props.config}
                    onSelect={this._selectSource}
                    index={index}/>
            }
        }, this);

        var selected = _.map(this.state.selected, function (item, index) {
            return <ListItem
                target={true}
                data={item}
                selected={this.state.selectedIndex}
                config={this.props.config}
                onSelect={this._selectItem}
                index={index}/>
        }, this);

        if (!this._isLoaded) {
            selectables = <Spinner/>;
            selected = <Spinner/>;
        }

        if (this._isLoaded && selectables.length <= 0) {
            selectables = <div className="placeholder">No investigative forms available for this account</div>
        }

        return (
            <table className="linked-lists">
                <tbody>
                <tr>
                    <td width="50%">
                        <label htmlFor="">{this.props.config.sourceLabel || "Source"}</label>
                        <div className="item-list">
                            {selectables}
                        </div>
                    </td>
                    <td>
                        <div className="div" style={{height: 23}}></div>
                        <ewars.d.Button
                            icon="fa-caret-right"
                            onClick={this._moveRight}/>
                        <div className="clearer"></div>
                        <div className="div" style={{height: 10}}></div>
                        <ewars.d.Button
                            icon="fa-caret-left"
                            onClick={this._remove}/>
                    </td>
                    <td width="50%">
                        <label htmlFor="">{this.props.config.targetLabel || "Selected"}</label>
                        <div className="item-list">
                            {selected}
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        )
    }
});

export default LinkedListComponent;
