var FormField = React.createClass({
    getInitialState: function () {
        return {
            content: "",
            helpShown: false,
            linkagesShown: false,
            errors: [],
            hasErrors: false,
            error: false
        };
    },

    _showHelp: function (e) {
        e.preventDefault();
        let title = ewars.I18N(this.props.field.label);
        ewars.prompt("fa-question-circle", `Help: ${title}`, ewars.I18N(this.props.field.help));
    },

    _helpClick: function () {
        if (this.props.field.help) {
            ewars.prompt("fa-question-circle", ewars.I18N(this.props.field.label), ewars.I18N(this.props.field.help), function () {

            })
        }
    },

    render: function () {
        var hasHelp = this.props.field.help ? true : false;
        var hasLinkages = this.props.field.linkages ? true : false;

        var fieldClass = "form-field";
        if (this.props.field.type == "row") fieldClass += " form-row";
        if (this.props.field.type == "switch") fieldClass += " switch";
        if (this.props.field.type == "header") fieldClass += " header";

        var labelClass = "label";
        if (this.props.field.type == "row") labelClass += " row-label";
        if (this.props.field.type == "matrix") labelClass += " matrix-label";

        var features = {
            help: true,
            error: true,
            label: true,
            fieldControls: true
        };

        if (this.props.readOnly || ["row", "header", "matrix"].indexOf(this.props.field.type) >= 0) {
            features.fieldControls = false;
        }

        let label = false;
        if (this.props.field.label !== false) {
            label = ewars.I18N(this.props.field.label);
            if (this.props.field.required) label += " *";
            if (this.props.field.help) labelClass += " has-help";
        }

        var children = this.props.children;
        if (React.Children.count(this.props.children) == 1) {
            children = [this.props.children];
        }

        let showError = true;
        if (["matrix", "row", "header"].indexOf(this.props.field.type) >= 0) showError = false;

        let errors;
        if (this.props.errors) {
            if (this.props.errors[this.props.field.path]) {
                errors = this.props.errors[this.props.field.path];
            }
        }

        let wrapper_style = {};
        if (label == false) wrapper_style.paddingLeft = 15;

        if (this.props.field.type == 'content') {
            return (
                <div className={fieldClass}>
                    <div className={labelClass}></div>
                    <div className="field-wrapper" style={wrapper_style}>
                        <div className="ide-row">
                            <div className="ide-col">
                                <p>{this.props.field.content}</p>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className={fieldClass}>
                {label ?
                    <div className={labelClass}>
                        <label htmlFor="" onClick={this._helpClick}>{label}</label>
                    </div>
                    : null}

                <div className="field-wrapper" style={wrapper_style}>
                    <div className="ide-row">
                        <div className="ide-col">
                            {children}
                        </div>

                    </div>
                </div>

                {features.fieldControls ?
                    <div className="field-controls">
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td>
                                    {errors ?
                                        <div className="field-error">
                                            <div className="icon"><i className="fal fa-exclamation-circle"></i></div>
                                            <div className="error-message">{errors}</div>
                                        </div>
                                        :
                                        <span>&nbsp;</span>
                                    }
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    : null}
            </div>
        )
    }

});

export default FormField;
