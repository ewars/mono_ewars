import TextField from "./c.cmp.text";
import SelectField from "./c.cmp.select";
import { Layout, Row, Cell } from "./layout";
import Toolbar from "./c.toolbar";
import Shade from "./c.shade";
import { ActionGroup, LitActionGroup } from "./c.action_button";
import Panel from "./c.panel";

const languages = [
    ["en", "English (en)"],
    ["fr", "French (fr)"]
];

const ACTIONS = [
    ["fa-plus", "ADD", "Add Translation"],
    ["fa-times", "CLOSE", "Close"]
];

const ROW_ACTIONS = [
    ["fa-trash", "REMOVE"]
];

class LangRow extends React.Component {
    _action = (action) => {

    };

    _onKeyChange = (prop, value) => {
        this.props.onAction("KEY_CHANGE", [this.props.code, value]);
    };

    _onValueChange = (prop, value) => {
        this.props.onAction("VALUE_CHANGE", [this.props.code, value]);
    };

    render() {
        return (
            <tr>
                <td>
                    <SelectField
                        data={{o: languages}}
                        value={this.props.code}
                        onChange={this._onKeyChange}/>
                </td>
                <td>
                    <TextField
                        value={this.props.data}
                        data={{ml: this.props.ml}}
                        onChange={this._onValueChange}/>
                </td>
                <td>
                    <ActionGroup
                        actions={ROW_ACTIONS}
                        onAction={this._action}
                        right={true}/>
                </td>
            </tr>
        )
    }
}

export default class LanguageEditor extends React.Component {
    static defaultProps = {
        value: {en: ""},
        data: {
            i18: false,
            ml: false // multiline
        }
    };

    constructor(props) {
        super(props);
    }

    _action = (action, args) => {
        let data = this.props.value;
        if (data) {
            if (JSON.stringify(data).indexOf("{") < 0) {
                data = {en: this.props.value}
            }
        } else {
            data = {en: ""}
        }

        let cp = ewars.copy(data || {});

        switch (action) {
            case "ADD":
                cp["NW"] = "New String";
                this.props.onChange(this.props.data.n, cp);
                break;
            case "KEY_CHANGE":
                let [oldCode, newCode] = args;
                let tmp = cp[oldCode];
                delete cp[oldCode];
                this.props.onChange(this.props.data.n, cp);
                break;
            case "VALUE_CHANGE":
                let [code, newValue] = args;
                cp[code] = newValue;
                this.props.onChange(this.props.data.n, cp);
                break;
            case "REMOVE":
                delete cp[args];
                this.props.onChange(this.props.data.n, cp);
                break;
            case "CLOSE":
                window.dispatchEvent(new CustomEvent("close-shade"));
                break;
            default:
                break;
        }

    };

    _onAction = (e) => {
        console.log("FIRED", e);
    };

    render() {
        let data = this.props.value;
        if (data) {
            if (JSON.stringify(data).indexOf("{") < 0) {
                data = {en: this.props.value}
            }
        } else {
            data = {en: this.props.value || ""};
        }

        let rows = [];
        for (let i in data) {
            rows.push(
                <LangRow
                    data={this.props.value}
                    key={i}
                    code={i}
                    ml={this.props.data.ml}
                    onAction={this._action}/>
            )
        }

        return (
            <Layout>
                <Toolbar label="Language Editor">
                    <ActionGroup
                        right={true}
                        onAction={this._action}
                        actions={ACTIONS}/>
                </Toolbar>
                <Row>
                    <Cell style={{padding: "8px"}}>
                        <Panel>
                            <div style={{padding: "8px"}}>
                            <table width="100%" className="admin-table">
                                <thead>
                                <tr>
                                    <th width="100px">Code</th>
                                    <th>Translation</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                {rows}
                                </tbody>
                            </table>
                            </div>
                        </Panel>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}
