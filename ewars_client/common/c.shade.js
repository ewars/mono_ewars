import { Layout, Row, Cell } from "./layout";
import Toolbar from "./c.toolbar";
import Button from "./c.cmp.button";

class Shade extends React.Component {
    static defaultProps = {
        shown: false,
        title: "",
        actions: [],
        onAction: null,
        toolbar: true
    };

    constructor(props) {
        super(props);
    }

    onClose = () => {
        this.props.onAction("CLOSE", null);
    };

    _onAction = (data) => {
        this.props.onAction(data.action);
    };

    componentDidMount() {
        if (this.props.shown) {
        } else {

        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.shown && !this.props.shown) {
        }
    }

    render() {
        if (!this.props.shown) return (
            <div className="shade hidden"></div>
        );

        return (
            <div className="shade shown">
                <Layout>
                    <Row>
                        <Cell width="10%" onClick={this.onClose} addClass="shade-shoulder">
                            <div className="shade-close">
                                <i className="fal fa-times"></i>
                            </div>
                        </Cell>
                        <Cell>
                            <div className="shade-content" ref="content">
                                <Layout>
                                    {this.props.toolbar ?
                                        <Toolbar label={this.props.title} icon={this.props.icon}>
                                            <div className="btn-group pull-right">
                                                {this.props.actions.map(item => {
                                                    return (
                                                        <ewars.d.Button
                                                            label={ewars.I18N(item.label)}
                                                            icon={item.icon || null}
                                                            onClick={this._onAction}
                                                            color={item.color || null}
                                                            data={item}/>

                                                    )
                                                })}
                                            </div>
                                        </Toolbar>
                                        : null}

                                    <Row>
                                        <Cell>
                                            {this.props.children}
                                        </Cell>
                                    </Row>

                                </Layout>
                            </div>
                        </Cell>
                    </Row>
                </Layout>
            </div>
        )
    }
}

export default Shade;

