import Panel from "./c.panel";

class SettingsForm extends React.Component {
    static defaultProps = {
        title: null,
        icon: "fa-wrench",
        description: null
    }

    render() {
        let description = this.props.description ?
            <div className="ide-settings-intro">{this.props.description}</div>
            : null;

        return (
            <Panel>
                <div className="ide-settings-content">
                    <div className="ide-settings-header">
                        <div className="ide-settings-icon"><i className={"fal " + this.props.icon}></i></div>
                        <div className="ide-settings-title">{this.props.title}</div>
                    </div>
                    {description}

                    <div className="ide-settings-form">
                        {this.props.children}
                    </div>
                </div>
            </Panel>
        )
    }
}

export default SettingsForm;
