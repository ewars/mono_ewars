class Control extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick = () => {
        this.props.onClick(this.props.action);
    };

    render() {
        return (
            <div className="iw-control" onClick={this.onClick}>
                <i className={"fal " + this.props.icon}></i>
            </div>
        )
    }
}

export default Control;