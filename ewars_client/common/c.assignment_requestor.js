import Moment from "moment";

import {
    DateField,
    LocationField as LocationSelector
} from "./fields";

import Button from "./c.button";
import Filler from "./c.filler";
import Empty from "./c.empty";


class Completed extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="article" style={{padding: 14}}>
                    <h1>Assignment Request Submitted</h1>

                    <p style={{padding: '8px', background: '#bcf2b7'}}>Your assignment request has been submitted. An administrator will review your request and you
                        will receive feedback soon.</p>

                    <p>Select another form opposite to request another assignment.</p>
                </div>
            </div>
        )
    }
}

class Assignable extends React.Component {
    constructor(props) {
        super(props)
    }

    _onChange = (prop, value) => {
        this.props.onChange(prop, value)
    };

    _submit = () => {
        this.props.onSubmit();
    };

    render() {
        let DATE_TYPE = "DAY";
        if (this.props.data.features.INTERVAL_REPORTING) DATE_TYPE = this.props.data.features.INTERVAL_REPORTING.interval;

        let hasLocation = this.props.data.features.LOCATION_REPORTING != null;

        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="ide-tbar-text">Assignment Details</div>
                            <div className="btn-group pull-right">
                                <ewars.d.Button
                                    label="Request Assignment"
                                    icon="fa-share-square"
                                    onClick={this._submit}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="article" style={{padding: "14px 14px 0 14px"}}>
                                <h1>{ewars.I18N(this.props.data.name)}</h1>
                                <p>{ewars.I18N(this.props.data.description)}</p>

                                {this.props.error ?
                                    <p className="error" style={{padding: 5}}><i
                                        className="fal fa-exclamation-triangle"></i> {ewars.I18N(this.props.error)}</p>
                                    : null}

                                {hasLocation ?
                                    <fieldset>
                                        <legend>Assignment Details</legend>
                                        <p>Assignments for this form require approval by an administrator before they
                                            can be
                                            used.</p>
                                        {this.props.data.features.LOCATION_REPORTING ?
                                            <div className="hsplit-box">
                                                <div className="ide-setting-label">Assignment Location *</div>
                                                <div className="ide-setting-control">
                                                    <LocationSelector
                                                        config={{
                                                            lineageRoot: window.user.clid,
                                                            selectionTypeId: this.props.data.features.LOCATION_REPORTING.site_type_id
                                                        }}
                                                        name="location_id"
                                                        onUpdate={this._onChange}
                                                        value={this.props.req.location_id}/>
                                                </div>
                                            </div>
                                            : null}

                                    </fieldset>
                                    :
                                    <ewars.d.Button
                                        icon="fa-share-square"
                                        onClick={this._submit}
                                        label="Request Assignment"/>
                                }

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

class Form extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <div className="block" onClick={this._onClick}>
                <div className="block-content">{ewars.I18N(this.props.data.name)}</div>
            </div>
        )
    }
}

var query = {
    status: {eq: "ACTIVE"}
};

class AssignmentRequest extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            form: null,
            error: null,
            forms: [],
            showCompleted: false,
            req: {
                form_id: null,
                location_ids: null
            }
        }
    }

    componentWillMount() {
        ewars.tx("com.ewars.query", ["form", ["id", "name", "features", "description"], query, null, null, null, null])
            .then(resp => {
                this.setState({forms: resp})
            })
    };

    _onSelect = (data) => {
        this.setState({
            error: null,
            showCompleted: false,
            req: {
                ...this.state.req,
                form_id: data.id
            },
            form: data
        })
    };

    _onReqChange = (prop, value) => {
        let realValue = value;
        if (["start_date", "end_date"].indexOf(prop) >= 0) {
            realValue = Moment(value).format("YYYY-MM-DD");
        }
        this.setState({
            error: null,
            req: {
                ...this.state.req,
                [prop]: realValue
            }
        })
    };

    _submit = () => {
        if (this.state.form.features.LOCATION_REPORTING) {
            if (["", null, undefined].indexOf(this.state.req.location_id) >= 0) {

                this.setState({
                    error: "Please select a valid location"
                });
                return;
            }
        }

        let bl = new ewars.Blocker(null, "Submitting assignment request...");
        ewars.tx("com.ewars.user.assignment.request", [this.state.req])
            .then(function (resp) {
                bl.destroy();

                if (resp == true) {
                    this.setState({
                        form: null,
                        showCompleted: true,
                        req: {
                            form_id: null,
                            location_id: null
                        }
                    })
                } else {
                    this.setState({
                        error: resp.message
                    })
                }
            }.bind(this))
    };

    render() {
        let form = (
            <Empty
                text="Select a form from the left to request an assignment"
                icon="fa-clipboard"/>
        );

        if (this.state.showCompleted) {
            form = <Completed/>;
        } else {

            if (this.state.form) {
                form = (
                    <Assignable
                        error={this.state.error}
                        onChange={this._onReqChange}
                        req={this.state.req}
                        onSubmit={this._submit}
                        data={this.state.form}/>
                )
            }
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="300px">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                {this.state.forms.map(form => {
                                    return <Form data={form} onClick={this._onSelect}/>;
                                })}
                            </div>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>

                        {form}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default AssignmentRequest;
