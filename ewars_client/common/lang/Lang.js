/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var LANG = {
    NONE_INTERVAL: {en: "None"},
    DAY_INTERVAL: {en: "Daily"},
    WEEK_INTERVAL: {en: "Weekly"},
    MONTH_INTERVAL: {en: "Monthly"},
    YEAR_INTERVAL: {en: "Yearly"},
    NEVER: {en: "Never Submitted"},
    SUPER_ADMIN: {en: "Super Administrator"},
    INSTANCE_ADMIN: {en: "Instance Administrator"},
    ACCOUNT_ADMIN: {en: "Account Admin"},
    USER: {en: "User"},
    LAB_USER: {en: "Laboratory User"},
    BOT: {en: "Bot"}
};

module.exports = function (CODE) {
    return ewars.formatters.I18N_FORMATTER(LANG[CODE]);
};

