class Vbox extends React.Component {
    static defaultProps = {
        width: null,
        borderTop: false,
        borderRight: false,
        borderBottom: false,
        borderLeft: false,
        addClass: null,
        padding: null,
        onClick: null,
        style: {}
    };

    constructor(props) {
        super(props);
    }

    _onClick = () => {
        if (this.props.onClick) this.props.onClick();
    };

    render() {
        let style = {};

        style.maxWidth = this.props.width || null;
        style.padding = this.props.padding || null;

        let className = "vbox";
        className += this.props.borderRight ? " border-right" : "";
        className += this.props.borderBottom ? " border-bottom" : "";
        className += this.props.borderLeft ? " border-left" : "";
        className += this.props.borderTop ? " border-top" : "";
        className += this.props.addClass ? ` ${this.props.addClass}` : "";

        if (this.props.onClick) style.cursor = "pointer";

        Object.assign(style, this.props.style || {});

        return (
            <div className={className} style={style} onClick={this._onClick}>
                {this.props.children}
            </div>
        )
    }
}

export default Vbox;