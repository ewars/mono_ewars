var CODES = {
    DUPLICATE_REPORTING_PERIOD: "A reporting period already exists with the same criteria for this location."
};

module.exports = function (code) {
    return ewars.I18N(CODES[code]);
};