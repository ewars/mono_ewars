var NotImplemented = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        return (
            <div className="not-implemented">
                <div className="not-impl-notice">
                    <h3><i className="fal fa-heart"></i> Application Not Implemented</h3>
                    <p>This application is currently under active development, please check back later</p>
                </div>
            </div>
        )
    }
});

export default NotImplemented;
