class Tab extends React.Component {
    render() {
        let className = "ux-tab";
        if (this.props.active) className += " active";

        return (
            <div
                onClick={() => {
                    this.props.onClick(this.props.id)
                }}
                className={className}>{this.props.label}</div>
        )
    }
}

const TABS = [
    ["Details", "DETAILS"],
    // ["Shared", "SHARED"],
    ["Assignments", "ASSIGNMENTS"],
    ["Reporting Periods", "REPORTING_PERIODS"],
    // ["Alerts", "ALERTS"]
    ["Users", "USERS"],
    ["Structure", "STRUCTURE"]
];

const data = [
    ["total_reports", "0,0", "Reports Submitted"],
    ["rep", "0.0%", "Reporting Representativeness"],
    ["assignments", "0,0", "Assignments (Active)"],
    ["regional_admins", "0", "Regional Admins"],
    ["open_alerts", "0,0", "Alerts (Open)"],
    ["triggered_alerts", "0,0", "Alerts (Triggered)"],
    ["childs_active", "0,0", "Child Locations (Active)"],
    ["childs_archived", "0,0", "Child Locations (Archived)"]
];

const ROLES = {
    USER: "Reporting User",
    REGIONAL_ADMIN: "Geographic Administrator"
};

class User extends React.Component {
    _showUser = () => {
        window.open("http://" + ewars.domain + '/user/' + this.props.data.id);
    };

    render() {
        let str = this.props.data.name + " " + this.props.data.email;

        if (this.props.data.role == "REGIONAL_ADMIN") {

            str += " (" +  ROLES[this.props.data.role] + " @ " + ewars.I18N(this.props.data.location_name) + ")";
        } else {
            str += " (" +  ROLES[this.props.data.role] + ")";
        }



        return (
            <div className="block hoverable" onClick={this._showUser}>
                <div className="block-content">{str}</div>
            </div>
        )
    }
}

class Users extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        };

        ewars.tx('com.ewars.location.profile.users', [this.props.uuid])
            .then((resp) => {
                this.setState({data: resp})
            })
    }

    render() {
        if (!this.state.data) return <div className="placeholder">Loading users</div>;
        if (this.state.data.length <= 0) return <div className="placeholder">No users available</div>;

        return (
            <div className="block-tree" style={{position: "relative"}}>
                {this.state.data.map((user) => {
                    return <User data={user} key={"USER_" + user.id}/>
                })}

            </div>
        )
    }
}

const RP_TYPES = {
    PROPAGATED: "Inherited",
    DEFAULT: "Assigned"

};

class ReportingPeriod extends React.Component {
    render() {
        console.log(this.props.data);
        let rpName = RP_TYPES[this.props.data.type];

        let endDate = "Current";
        if (this.props.data.end_date) {
            endDate = ewars.DATE(this.props.data.end_date, "DAY");
        }

        return (
            <div className="block">
                <div className="block-content">
                    {ewars.I18N(this.props.data.name)} - {rpName} - {ewars.DATE(this.props.data.start_date, "DAY")}&nbsp;
                    to {endDate}
                </div>
            </div>
        )
    }
}

class ReportingPeriods extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        };

        ewars.tx("com.ewars.location.profile.periods", [this.props.uuid])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        if (!this.state.data) {
            return <div className="placeholder">Loading reporting periods</div>;
        }

        if (this.state.data.length <= 0) {
            return <div className="placeholder">No reporting periods set for this location</div>
        }

        return (
            <div className="block-tree" style={{position: "relative"}}>
                {this.state.data.map((item) => {
                    return <ReportingPeriod data={item} key={"RP_" + item.id}/>
                })}

            </div>
        )
    }
}

class Assignment extends React.Component {
    render() {
        let dt = this.props.data;
        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Layout>
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                {ewars.I18N(dt.form_name)} - {ewars.I18N(dt.location_name)} ({ewars.I18N(dt.sti_name)})
                            </ewars.d.Cell>
                        </ewars.d.Row>
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                {dt.user_name} ({dt.user_email})
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </div>
            </div>
        )
    }
}

class Assignments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        };

        ewars.tx("com.ewars.location.profile.assignments", [this.props.uuid])
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="placeholder">Loading assignments</div>
            )
        }

        if (this.state.data.length <= 0) {
            return (
                <div className="placeholder">No assignments</div>
            )
        }

        return (
            <div className="block-tree" style={{position: "relative"}}>
                {this.state.data.map((item) => {
                    return <Assignment data={item} key={item.uuid}/>
                })}
            </div>
        )
    }
}

class Details extends React.Component {
    render() {
        // Calculate representativeness
        this.props.data.rep = "0";
        if (this.props.data.total_reports > 0) {
            this.props.data.rep = this.props.data.total_reports / this.props.data.all_reports
        }

        return (
            <div className="profile-content">

                <table>
                    <tbody>
                    <tr>
                        <th className="spread" colSpan="2">General</th>
                    </tr>
                    <tr>
                        <th>Location Name</th>
                        <td>{this.props.data.name}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>
                            <div className="description"
                                 dangerouslySetInnerHTML={{__html: this.props.data.description}}></div>
                        </td>
                    </tr>
                    <tr>
                        <th>PCode</th>
                        <td>{this.props.data.pcode}</td>
                    </tr>
                    <tr>
                        <th>Location Path</th>
                        <td>{this.props.data.full_name.reverse().join(", ")}</td>
                    </tr>
                    <tr>
                        <th>Location Type</th>
                        <td>{this.props.data.sti_name}</td>
                    </tr>
                    <tr>
                        <th>UUID</th>
                        <td>{this.props.data.uuid}</td>
                    </tr>
                    <tr>
                        <th colSpan="2" className="spread">Metrics</th>
                    </tr>
                    </tbody>
                </table>

                <div className="flex-list">

                    {data.map((item) => {
                        return (
                            <div className="flex-node">
                                <div className="flex-node-inner">
                                    <div className="text">{ewars.NUM(this.props.data[item[0]], item[1])}</div>
                                </div>
                                <div className="label">{item[2]}</div>
                            </div>
                        )
                    })}

                </div>


            </div>
        )
    }
}

export default class LocationProfile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            view: "DETAILS"
        };

        ewars.tx("com.ewars.location.profile", [this.props.uuid])
            .then((res) => {
                this.setState({
                    data: res
                })
            })
    }

    _swapTab = (tab) => {
        this.setState({
            view: tab
        })
    };

    render() {
        if (!this.state.data) {
            return (
                <div className="placeholder">Loading location...</div>
            )
        }

        let view;

        if (this.state.view == "DETAILS") view = <Details data={this.state.data}/>;
        if (this.state.view == "ASSIGNMENTS") view = <Assignments uuid={this.props.uuid}/>;
        if (this.state.view == "REPORTING_PERIODS") view = <ReportingPeriods uuid={this.props.uuid}/>;
        if (this.state.view == "USERS") view = <Users uuid={this.props.uuid}/>;

        return (
            <ewars.d.Panel>
                <div className="user-header">
                    <div className="user-avatar">
                        <i className="fal fa-map-marker"></i>
                    </div>
                    <div className="user-details">
                        <div className="user-name">{ewars.I18N(this.state.data.name)}</div>
                        <div className="user-role"></div>
                        <div className="user-role">{ewars.I18N(this.state.data.sti_name)}</div>
                        <div className="user-meta">
                        </div>
                        <div className="user-connect">
                        </div>
                    </div>

                    <div className="user-tabs">
                        <div className="ux-tabs">

                            {TABS.map((tab) => {
                                    return <Tab
                                        active={this.state.view === tab[1]}
                                        label={tab[0]}
                                        id={tab[1]}
                                        onClick={this._swapTab}/>;
                                }
                            )}
                        </div>
                    </div>

                    <div className="profile-wrapper">
                        {view}
                    </div>
                </div>
            </ewars.d.Panel>
        )
    }
}