var MenuItem = React.createClass({
    onClick: function (e) {
        e.preventDefault();
        this.props.onClick(this.props.data);
    },

    render: function () {
        var iconClass = "fal " + this.props.data.icon;
        var label = ewars.I18N(this.props.data.label);

        return (
            <div className="item" onClick={this.onClick}><i className={iconClass}></i>&nbsp;{label}</div>
        )
    }
});

var ContextMenu = React.createClass({
    getDefaultProps: function () {
        return {
            visible: false,
            absolute: true
        }
    },

    getInitialState: function () {
        return {};
    },

    _onMenuItem: function (item) {
        this.hide();
        item.onClick();
    },

    hide: function () {
        this.props.visible = false;
    },

    render: function () {
        let style = {};
        if (!this.props.absolute) style.position = "relative";

        if (!this.props.visible) return null;

        var items = _.map(this.props.items, function(item) {
            return <MenuItem data={item} onClick={this._onMenuItem}/>;
        }, this);

        return (
            <div className="context-menu" style={style}>
                {items}
            </div>
        )

    }
});

export default ContextMenu;
