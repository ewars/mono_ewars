var LinkSelect = React.createClass({
    getInitialState: function () {
        return {
            showOptions: false
        }
    },

    _click: function () {
        this.state.showOptions = this.state.showOptions ? false : true;
        this.forceUpdate();
    },

    render: function () {
        var displayValue = "All";
        return (
            <div className="ide-link-select">
                <a href="#" onClick={this._click}>{displayValue} <i className="fal fa-caret-down"></i></a>
                {this.state.showOptions ?
                    <div className="options">
                    </div>
                    : null}
            </div>
        )
    }
});

export default LinkSelect;
