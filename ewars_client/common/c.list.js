var ListElement = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {

        var view;
        if (this.props.Template) {
            var Tmpl = this.props.Template;
            view = <Tmpl data={this.props.data}/>
        } else {
            var label = ewars.I18N(this.props.data.label);
            view = <div className="ew-list-item-title">{label}</div>
        }

        var className = "ew-list-item";
        if (this.props.value == this.props.data.id && this.props.value != null) className += " active";

        return (
            <div className={className} onClick={this._onClick}>
                {view}
            </div>
        )
    }
});

class ListComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    onChange = (item) => {
        if (!this.props.readOnly) {
            this.props.onUpdate(this.props.name, item.id, null, item);
        }
    };

    render() {
        let clickEvent = this.onChange;
        if (this.props.onClick) clickEvent = this.props.onClick;

        let items = this.props.items.map(item => {
            return (
                <ListElement
                    data={item}
                    value={this.props.value}
                    onClick={clickEvent}
                    Template={this.props.Template}/>
            )
        });

        if (items.length <= 0) {
            items = <p className="placeholder">No items available</p>;
        }

        return (
            <div className="ew-select-list">
                {items}
            </div>
        )
    }

}


export default ListComponent;
