var CONSTANTS = require("../../../../constants");

var AccountItem = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "user-type";
        if (this.props.selected) className += " selected";

        return (
            <div className={className} onClick={this._onClick}>
                <div className="name">{this.props.data.name}</div>
                <div className="description">{this.props.data.domain}</div>
            </div>
        )
    }
});


var Account = React.createClass({
    getInitialState: function () {
        return {
            data: [],
            errors: []
        }
    },

    componentWillMount: function () {
        var query = {};

        if (window.user.role == CONSTANTS.SUPER_ADMIN) null;
        if (window.user.role == CONSTANTS.INSTANCE_ADMIN) null;
        if (window.user.role == CONSTANTS.ACCOUNT_ADMIN) query.id = {eq: window.user.aid};

        query.instance_id = {eq: window.instance.uuid};

        ewars.tx("com.ewars.query", ["account", ["id", "name", "location_id"], query, {name: "ASC"}, null, null, null])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onClick: function (account) {
        this.state.errors = [];
        this.props.onChange("account", account);
    },

    canAdvance: function () {
        var isComplete = true;

        if (!this.props.data.account || this.props.data.account == "") {
            isComplete = false;
            this.state.errors.push("Please provide a valid account");
        }

        return isComplete;
    },

    render: function () {

        var accounts = _.map(this.state.data, function (item) {
            if (item.id != 1) {
                var isSelected = false;
                if (this.props.data.account) {
                    isSelected = (item.id == this.props.data.account.id);
                }
                if (window.user.group_id == 5) {
                    if (window.user.account_id == item.id) {
                        return <AccountItem
                            data={item}
                            selected={isSelected}
                            onClick={this._onClick}/>;
                    }
                } else {
                    return <AccountItem
                        data={item}
                        selected={isSelected}
                        onClick={this._onClick}/>;
                }
            }
        }, this);

        var errors = _.map(this.state.errors, function (err) {
            return <li>{err}</li>
        });

        return (
            <div className="wizard-page">
                <h3>Account</h3>

                <p>Select the account this user belongs to</p>

                {this.state.errors.length > 0 ?
                    <ul className="errors">
                        {errors}
                    </ul>
                    : null}

                <div className="padded">
                    <div className="user-type-list">
                        {accounts}
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Account;