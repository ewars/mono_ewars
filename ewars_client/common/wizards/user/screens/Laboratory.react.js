var Lab = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "user-type";
        if (this.props.selected) className += " selected";


        return (
            <div className={className} onClick={this._onClick}>
                <div className="name">{this.props.data.name}</div>
                <div className="description"></div>
            </div>
        )
    }
});

var Laboratory = React.createClass({
    getInitialState: function () {
        return {
            data: []
        }
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.laboratories", [])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _onLabSelect: function (lab) {
        this.props.onChange("laboratory", lab);
    },

    render: function () {
        var labs = _.map(this.state.data, function (lab) {
            var isSelected = false;
            if (this.props.data.laboratory) isSelected = (this.props.data.laboratory.uuid == lab.uuid);
            return <Lab
                selected={isSelected}
                data={lab}
                onClick={this._onLabSelect}/>;
        }, this);

        return (
            <div className="wizard-page">
                <h3>Laboratory</h3>

                <p>Select the laboratory that this user belongs to</p>

                <div className="padded">
                    <div className="user-type-list">
                        {labs}
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Laboratory;