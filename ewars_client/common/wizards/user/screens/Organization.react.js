var Org = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "user-type";
        if (this.props.selected) className += " selected";
        var name = ewars.I18N(this.props.data.name);

        return (
            <div className={className} onClick={this._onClick}>
                <div className="name">{name}</div>
                <div className="description">{this.props.data.acronym}</div>
            </div>
        )
    }
});

var Organization = React.createClass({
    getInitialState: function () {
        return {
            data: [],
            errors: []
        }
    },

    onClick: function (org) {
        this.state.errors = [];
        this.props.onChange("organization", org);
    },

    canAdvance: function () {
        var isComplete = true;

        if (!this.props.data.organization || this.props.data.organization == "") {
            isComplete = false;
            this.state.errors.push("Please select a valid organization");
        }

        return isComplete;
    },

    componentWillMount: function () {
        ewars.tx("com.ewars.query", ["organization", ["name", "uuid"], {}, {"name.en": "ASC"}, null, null, null])
            .then(function (resp) {
                this.state.data = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    render: function () {

        var orgs = _.map(this.state.data, function (org) {
            var isSelected = false;
            if (this.props.data.organization) isSelected = (this.props.data.organization.uuid == org.uuid);
            return <Org
                selected={isSelected}
                data={org}
                onClick={this.onClick}/>
        }, this);

        var errors = _.map(this.state.errors, function (err) {
            return <li>{err}</li>
        });

        return (
            <div className="wizard-page">
                <h3>Organization</h3>

                <p>Select the organization that this user belongs to</p>

                {this.state.errors.length > 0 ?
                    <ul className="errors">
                        {errors}
                    </ul>
                    : null}

                <div className="padded">
                    <div className="user-type-list">
                        {orgs}
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Organization;