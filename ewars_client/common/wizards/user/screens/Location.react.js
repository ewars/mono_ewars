var LocationTreeView = require("../../../../../locations/components/LocationTreeView.react");

var LocationSelection = React.createClass({
    _onSelect: function (location) {
        this.props.onChange('location', location);
    },

    canAdvance: function () {
        if (this.props.data.location != null) return true;
        return false;
    },

    render: function () {
        var parentId;

        if (window.user.group_id == "ACCOUNT_ADMIN") parentId = window.user.clid;
        if (window.user.role != "ACCOUNT_ADMIN") parentId = this.props.data.clid;

        var location;

        if (this.props.data.location) {
            var locName = ewars.formatters.I18N_FORMATTER(this.props.data.location.name);
            location = <p><strong>Selected Location: </strong> {locName}</p>
        }

        return (
            <div className="wizard-page">
                <h3>Location Selection</h3>

                <p>Select the location that this users access should be constrained to:</p>

                {location}
                <div className="padded location">
                    <LocationTreeView
                        style="DARK"
                        parent_uuid={parentId}
                        name="location_uuid"
                        onEdit={this._onSelect}
                        restrict_selection={false}
                        hideInactive={true}
                        value={this.props.data.location.uuid}
                        onUpdate={this._onChange}/>

                </div>
            </div>
        )
    }
});

module.exports = LocationSelection;