const btnClass = {
    red: "btn-red",
    green: "btn-green",
    amber: "btn-amber"
};

var ButtonComponent = React.createClass({
    _id: null,

    propTypes: {
        label: React.PropTypes.string,
        icon: React.PropTypes.string,
        type: React.PropTypes.string
    },

    getDefaultProps: function () {
        return {
            enabled: true,
            processing: false,
            active: false
        }
    },

    getInitialState: function () {
        return {
            enabled: true,
            processing: false,
            processingMessage: "Processing..."
        }
    },

    /**
     * Public method to set button to processing mode
     * @param message
     */
    setProcessing: function (message) {
        this.state.processing = true;
        if (message != null && message != undefined) this.state.processingMessage = message;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method to set button to not processing mode
     */
    unsetProcessing: function () {
        this.state.processing = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method used to disable the button
     */
    disable: function () {
        this.state.enabled = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Pubilc method used to enable button
     */
    enable: function () {
        this.state.enabled = true;
        if (this.isMounted()) this.forceUpdate();
    },

    shouldComponentUpdate: function () {
        return true;
    },

    _onClick: function (e) {
        if (!this.props.enabled) return;
        if (!this.state.enabled) return;
        if (this.state.processing) return;
        if (this.props.processing) return;

        e.preventDefault();
        e.stopPropagation();
        this.props.onClick((this.props.data ? this.props.data : null));
    },

    render: function () {
        var colour;
        if (this.props.colour) colour = btnClass[this.props.colour];

        var className = "bttn";
        if (colour) className += " " + colour;
        var iconClass;
        if (this.props.icon) iconClass = "fal " + this.props.icon;

        if (this.props.active) className += " active";

        var title;
        if (this.props.title) title = this.props.title;

        if (!this.state.enabled || !this.props.enabled) className += " disabled";

        var label;
        if (this.props.label) label = ewars.formatters.I18N_FORMATTER(this.props.label);

        if (this.state.processing || this.props.processing) {
            iconClass = "fal fa-circle-o-notch fa-spin";
            label = this.state.processingMessage || "Processing...";
        }

        var view;
        if (iconClass && label) {
            view = (
                <div rel="tooltip" title={title} className={className} onClick={this._onClick}>
                    <i className={iconClass}></i>&nbsp;
                    {label}
                </div>
            )
        } else if (iconClass && !label) {
            view = (
                <div rel="tooltip" title={title} className={className} onClick={this._onClick}>
                    <i className={iconClass} style={{paddingRight: "0px"}}></i>
                </div>
            )
        } else {
            view = (
                <div rel="tooltip" title={title} className={className} onClick={this._onClick}>{label}</div>
            )
        }

        return view;
    }
});

export default ButtonComponent;
