import DateUtils from "../../documents/utils/DateUtils";
import RangeUtils from "../utils/RangeUtils";
import turf from 'turf';

function _isSet(check, fallback) {
    if (["", null, undefined].indexOf(check) >= 0) {
        return fallback;
    }

    return check;
}


class Map2 {
    constructor(el, definition, reportDate, location_uuid, isPublic, idx) {
        this.definition = definition;
        this._el = el;
        this._reportDate = reportDate;
        this._locationUUID = location_uuid;
        this._public = isPublic;
        this._idx = idx;

        this._setupLoading();
        this.query(definition);
    }

    _setupLoading = () => {
        this._el.className += " chart-loading";
        this._el.innerText = "Loading...";
    };

    _tearDownLoading = () => {
        this._el.className = "chart";
        this._el.innerText = "";
    };

    getColour = (value, ot, oth) => {
        console.log(value);
        let colour;

        let anchors = this.definition.scales || this.definition.thresholds;
        console.log(anchors)
        anchors.forEach(scale => {
            if (scale[1] === "INF") {
                if (parseFloat(value.data) >= parseFloat(scale[0])) colour = scale;
            } else {
                if (parseFloat(value.data) >= parseFloat(scale[0]) && parseFloat(value.data) <= parseFloat(scale[1])) {
                    colour = scale;
                }
            }
        });

        if (!colour) return "rgba(0,0,0,0)";
        return colour[2];
    };

    getData = () => {
        return ["MAP", [this.definition, this._data]];
    };

    query = (config) => {
        let start_date,
            end_date;
        if (config.period) {
            end_date = RangeUtils.process(config.period[1], this._reportDate);
            start_date = RangeUtils.process(config.period[0], this._reportDate, end_date);
        } else {
            let dateRange = DateUtils.processDateSpec(config, this._reportDate);
            start_date = dateRange[0];
            end_date = dateRange[1];
        }

        let loadGeom = true;
        // if (window.dt && window.dt.geom_dict) loadGeom = false;

        let query = {
            type: config.source_type || "SLICE",
            reduction: "SUM",
            interval: "DAY",
            indicator: config.indicator,
            start_date: start_date,
            end_date: end_date,
            location: null,
            tid: this._tid,
            geometry: loadGeom,
            series: config.series || null,
            formula: config.formula || null,
            centroid: true
        };

        if (config.loc_source || config.loc_spec) {
            let loc_source = config.loc_source || config.loc_spec;

            if (loc_source == "SPECIFIC") {
                query.location = config.location;
            } else if (["TYPE", "GENERATOR"].indexOf(loc_source) >= 0) {
                query.location = {
                    parent_id: config.location,
                    site_type_id: config.site_type_id,
                    status: config.location_status || "ACTIVE"
                }
            } else if (loc_source == "GROUP") {
                query.location = {
                    groups: config.group_ids,
                    agg: "INDIVIDUAL"
                }
            }
        } else {
            query.location = {
                parent_id: config.location,
                site_type_id: config.site_type_id,
                status: config.location_status || "ACTIVE"
            }
        }

        this._emitLoaded = false;

        ewars._queue.push("/arc/analysis", query)
            .then(resp => {
                this._data = resp;
                this.render();
            })
            .catch(err => {
                if (!this._emitLoaded) {
                    this._emitLoaded = true;
                    ewars.emit('WIDGET_LOADED')
                }
            })

    };

    updateDefinition = (config) => {
        this.definition = config;

        if (this.svg) {
            this.svg.remove();
            this.svg = null;
        }
        this.query(config);
    };

    buildLegend = (config) => {
        if (this._legendId) {
            let original = document.getElementById(this._legendId);
            original.parentNode.removeChild(original);
        }
        var container = document.createElement("div");
        container.setAttribute("class", "map-legend");
        this._legendId = ewars.utils.uuid();
        container.setAttribute("id", this._legendId);

        let scales = config.thresholds || config.scales;
        scales.forEach(item => {
            let scaleEl = document.createElement("div");
            let dot = document.createElement("span");
            dot.setAttribute("class", "legend-dot");
            let dotInner = document.createElement('div');
            dotInner.setAttribute('class', 'legend-dot-inner');
            dot.appendChild(dotInner);
            dotInner.style.background = item[2];

            let labelText;
            if (["", NaN, null, undefined].indexOf(item[1]) >= 0) {
                labelText = document.createTextNode(` ${item[0] || 0}+`);
            } else {
                labelText = document.createTextNode(` ${item[0] || 0} to ${item[1]}`);
            }

            scaleEl.appendChild(dot);
            scaleEl.appendChild(labelText);

            container.appendChild(scaleEl);
        });

        this._el.appendChild(container);
    };

    _buildKey = (locations, pointLocations) => {
        var container = document.createElement("div");
        container.setAttribute("class", "map-key");
        this._keyId = ewars.utils.uuid();
        container.setAttribute("id", this._keyId);

        locations.forEach((item, index) => {
            let row = document.createElement("div");
            row.setAttribute("class", "key-row");
            let rowKey = document.createElement("div");
            rowKey.setAttribute("class", "key-row-id");
            let keyText = document.createTextNode(index + 1);
            rowKey.appendChild(keyText);
            row.appendChild(rowKey);

            let rowLabel = document.createElement("div");
            rowLabel.setAttribute("class", "key-row-label");
            let labelText = document.createTextNode(__(item.location.name));
            rowLabel.appendChild(labelText);
            row.appendChild(rowLabel);

            container.appendChild(row);
        })

        if (pointLocations) {
            pointLocations.forEach((item, index) => {
                let row = document.createElement('div');
                row.setAttribute('class', 'key-row');
                let rowKey = document.createElement('div');
                rowKey.setAttribute('class', 'key-row-id');
                let keyText = document.createTextNode('P' + index + 1);
                rowKey.appendChild(keyText);
                row.appendChild(rowKey);

                let rowLabel = document.createElement('div');
                rowLabel.setAttribute('class', 'key-row-label');
                let labelText = document.createTextNode(__(item.location.name));
                rowLabel.appendChild(labelText);
                row.appendChild(rowLabel);

                container.appendChild(row);
            })
        }

        this._el.appendChild(container);

    };

    render = () => {
        this._tearDownLoading();
        let self = this;

        let height = this._el.parentNode.clientHeight;
        if (this.definition.height && this.definition.height != "") {
            height = this.definition.height;
            if (height.indexOf) {
                if (height.indexOf("px") >= 0) height = height.replace("px", "");
                if (height.indexOf("px") >= 0) height = height.replace("%", "");
                if (height == "") height = this._el.parentNode.clientHeight;
            }
        }

        if (height[height.length - 1] == "%") height = this._el.parentNode.clientHeight;

        let width = this._el.parentNode.clientWidth;
        if (this.definition.width && this.definition.width != "") {
            width = this.definition.width;
            if (width.indexOf) {
                if (width.indexOf("px") >= 0) width = width.replace("px", "");
                if (width.indexOf("px") >= 0) width = width.replace("%", "");
                if (width == "") width = this._el.parentNode.clientWidth;
            }
        }

        if (width[width.length - 1] == '%') width = this._el.parentNode.clientWidth;
        if (width == 0) width = this._el.parentNode.offsetWidth;

        width = parseInt(width);
        height = parseInt(height);


        this._el.style.position = 'relative';

        // append root SVG
        let svg = d3.select(this._el).append("svg")
            .attr("width", width)
            .attr("height", height);

        // Create base projection
        let projection = d3.geoMercator()
            .scale(1)
            .translate([0, 0]);

        // Create geo path
        let path = d3.geoPath()
            .projection(projection);

        this.svg = svg;

        let bgd = svg.append("g");
        bgd.append("rect")
            .attr("class", "background")
            .style("fill", _isSet(this.definition.bgd_colour, "transparent"))
            .attr("width", width)
            .attr("height", height);

        let base = svg.append("g");

        if ((this.definition.loc_source || "TYPE") == "SPECIFIC") {
            console.log("HERE");
            let baseG = g.append("g");
            let labelsG = g.append("g");

            let data = this._data;

            baseG.append("path")
                .attr("d", path(data.d.location.geometry))
                .style("fill", this.getColour({data: data.d.data}));

            let fontSize = "0.1em";
            if (ewars.isSet(this.definition.label_font_size)) {
                fontSize = this.definition.label_font_size;
                if (fontSize.indexOf("em") < 0) fontSize += "em";
            }
            let labelColour = "#000000";
            if (ewars.isSet(this.definition.label_colour)) {
                labelColour = this.definition.label_colour;
            }
            labelsG.append("text")
                .style("text-anchor", "middle")
                .style("fill", labelColour)
                .style("font-size", "12px")
                .attr("x", function (d) {
                    return path.centroid(data.d.location.geometry)[0];
                })
                .attr("y", function (d) {
                    return path.centroid(data.d.location.geometry)[1];
                })
                .text(function (d) {
                    return ewars.I18N(data.d.location.name);
                });

            let bounds = path.bounds(this._data.d.location.geometry),
                dx = bounds[1][0] - bounds[0][0],
                dy = bounds[1][1] - bounds[0][1],
                x = (bounds[0][0] + bounds[1][0]) / 2,
                y = (bounds[0][1] + bounds[1][1]) / 2,
                scale = .9 / Math.max(dx / width, dy / height),
                translate = [width / 2 - scale * x, height / 2 - scale * y];

            g.attr("transform", "translate(" + translate + ")scale(" + scale + ")");
        }

        if (["GROUP", "TYPE", "GENERATOR"].indexOf(this.definition.loc_source) >= 0 || this.definition.loc_spec == "GENERATOR") {
            let validLocs = [];
            let validPointLocs = [];


            let geoms = this._data.d.filter(item => {
                return item.location.geometry_type == 'ADMIN';
            });

            let points = this._data.d.filter(item => {
                return item.location.geometry_type == 'POINT';
            });

            // Filter out bad geometries
            geoms.forEach(item => {
                let geo = item.location.geometry || {};
                if (geo.features == null || geo.features == undefined) {

                } else {
                    validLocs.push(item);
                }
            });

            // Filter out bad point location values
            points.forEach(item => {
                let geo = item.location.geometry || {};
                if (geo.type != null || geo.type != undefined) {
                    validPointLocs.push(item);
                }
            })


            let root = this._data.g;
            let b, multiG = false;
            if (this._data.g.features) {
                if (this._data.g.features[0].geometry.type == 'MultiPolygon' && this._data.g.features[0].geometry.coordinates.length > 1) {
                    multiG = true;
                    root = {
                        type: 'FeatureCollection',
                        features: this._data.g.features[0].geometry.coordinates.map(item => {
                            return {
                                type: 'Feature',
                                geometry: {
                                    type: 'Polygon',
                                    coordinates: item
                                }
                            }
                        })
                    };
                    let _c = turf.bbox(root);
                    let poly = turf.bboxPolygon(_c);

                    let realCoordinates = [];
                    poly.geometry.coordinates[0].forEach(item => {
                        realCoordinates.unshift(item)
                    })

                    b = path.bounds({
                        type: 'FeatureCollection',
                        features: [
                            {
                                type: 'Feature',
                                geometry: {
                                    type: 'Polygon',
                                    coordinates: [
                                        realCoordinates
                                    ]
                                }
                            }
                        ]
                    });
                } else {
                    b = path.bounds(root);
                }
            }
            // b = path.bounds(root);

            let s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height),
                t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];


            projection.scale(s)
                .translate(t);

            base.append("path")
                .datum(root)
                .style("fill", _isSet(this.definition.base_colour, "#FFFFFF"))
                .attr("d", path);

            // Set up chloropleths
            let chloros = svg.append("g");

            chloros.selectAll("path")
                .data(validLocs)
                .enter()
                .append("path")
                .attr("d", function (datum) {
                    return path(datum.location.geometry)
                })
                .attr("id", function (d) {
                    return d.location.uuid;
                })
                .attr("class", "mesh")
                .style("fill-opacity", _isSet(this.definition.chloro_opacity, 0.8))
                .style("fill", this.getColour)
                .style("stroke", this.definition.s_stroke || "rgba(0,0,0,1)")
                .style("stroke-width", this.definition.s_stroke_width || 1);

            let pointChlors = svg.append('g');
            pointChlors.selectAll('circle')
                .data(validPointLocs)
                .enter()
                .append('circle')
                .attr('r', 5)
                .attr('transform', (d) => {
                    let geo = d.location.geometry || '{}';
                    return "translate(" + projection([geo.coordinates[0], geo.coordinates[1]]) + ")";
                })
                .style('fill', this.getColour);

            if (this.definition.b_labels) {
                let labelItems = validLocs;
                if (this.definition.label_threshold != null && this.definition.label_threshold != undefined) {
                    labelItems = validLocs.filter((item) => {
                        return item.data >= parseFloat(this.definition.label_threshold);
                    })
                }

                if (this.definition.lbl_style == "DEFAULT") {
                    // Set up labels
                    let labels = svg.append("g");
                    labels.selectAll("text")
                        .data(labelItems)
                        .enter()
                        .append("text")
                        .style("text-anchor", "middle")
                        .style("fill", this.definition.label_colour || "#000")
                        .style("font-size", this.definition.label_font_size || 12)
                        .style("text-align", "center")
                        .attr("x", function (d) {
                            return path.centroid(d.location.geometry || "{}")[0];
                        })
                        .attr("y", function (d) {
                            return path.centroid(d.location.geometry || "{}")[1];
                        })
                        .text(function (d) {
                            return __(d.location.name);
                        });

                    let pLabels = svg.append('g');
                    pLabels.selectAll('text')
                        .data(validPointLocs)
                        .enter()
                        .append('text')
                        .style('text-anchor', function (d) {
                            let geo = d.location.geometry || '{}';
                            return geo.coordinates[0] > -1 ? "start" : "end";
                        })
                        .style('fill', this.definition.label_colour || '#000')
                        .attr('transform', function (d) {
                            let geo = d.location.geometry || '{}';
                            return "translate(" + projection(geo.coordinates) + ")";
                        })
                        .attr('dy', '.35em')
                        .style('font-size', '11px')
                        .text(function (d) {
                            return __(d.location.name);
                        })

                    pLabels.selectAll('text')
                        .attr('x', function (d) {
                            let geo = d.location.geometry || '{}';
                            return geo.coordinates[0] > -1 ? 6 : -6;
                        })
                        .style('text-anchor', function (d) {
                            let geo = d.location.geometry || '{}';
                            return geo.coordinates[0] > -1 ? "start" : "end";
                        })
                }

                if (this.definition.lbl_style == "NUMBERED") {
                    let labels = svg.append("g");
                    labels.selectAll("text")
                        .data(labelItems)
                        .enter()
                        .append("text")
                        .style("text-anchor", "middle")
                        .style("fill", this.definition.label_colour || "#000")
                        .style("font-size", this.definition.label_font_size || 10)
                        .style("text-align", "center")
                        .attr("x", function (d) {
                            return path.centroid(d.location.geometry || "{}")[0];
                        })
                        .attr("y", function (d) {
                            return path.centroid(d.location.geometry || "{}")[1];
                        })
                        .text(function (d, i) {
                            return i + 1;
                        });

                    let pLabels = svg.append('g');
                    pLabels.selectAll('text')
                        .data(validPointLocs)
                        .enter()
                        .append('text')
                        .style('text-anchor', function (d) {
                            let geo = d.location.geometry || '{}';
                            return geo.coordinates[0] > -1 ? "start" : "end";
                        })
                        .style('fill', this.definition.label_colour || '#000')
                        .attr('transform', function (d) {
                            let geo = d.location.geometry || '{}';
                            return "translate(" + projection(geo.coordinates) + ")";
                        })
                        .attr('dy', '.35em')
                        .text(function (d, i) {
                            return 'P' + (i++);
                        })

                    pLabels.selectAll('text')
                        .attr('x', function (d) {
                            let geo = d.location.geometry || '{}';
                            return geo.coordinates[0] > -1 ? 6 : -6;
                        })
                        .style('text-anchor', function (d) {
                            let geo = d.location.geometry || '{}';
                            return geo.coordinates[0] > -1 ? "start" : "end";
                        })

                    this._buildKey(validLocs, validPointLocs);
                }
            }

        }


        if ((this.definition.legend || "HIDE") == "SHOW") {
            this.buildLegend(this.definition);
        }

        if (!this._emitLoaded) {
            this._emitLoaded = true;
            ewars.emit('WIDGET_LOADED');
        }

    }
}

export default Map2;
