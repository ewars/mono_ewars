var Moment = require("moment");
const d3 = require("d3");

var DateUtils = require("../../documents/utils/DateUtils");
var DataSource = require("../models/DataSource");
var AnalysisUtils = require("../utils/AnalysisUtils");
var defaults = {};

var src_data = [
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000},
    {date: "2016-01-01", value: 1000}
];

function transition() {
    d3.selectAll("path")
        .data(function () {
            var d = layers1;
            layers1 = layers0;
            return layers0 = d;
        })
        .transition()
        .duration(2500)
        .attr("d", area);
}

// Inspired by Lee Byron's test data generator.
function bumpLayer(n) {

    function bump(a) {
        var x = 1 / (.1 + Math.random()),
            y = 2 * Math.random() - .5,
            z = 10 / (.1 + Math.random());
        for (var i = 0; i < n; i++) {
            var w = (i / n - y) * z;
            a[i] += x * Math.exp(-w * w);
        }
    }

    var a = [], i;
    for (i = 0; i < n; ++i) a[i] = 0;
    for (i = 0; i < 5; ++i) bump(a);
    return a.map(function (d, i) {
        return {x: i, y: Math.max(0, d)};
    });
}


module.exports = function (el, definition, reportDate, location_uuid, template_id, isPublic) {
    var instance = {
        _config: definition,
        _reportDate: reportDate,
        _el: el,
        _isPublic: isPublic,

        getData: function () {
            return this._value;
        },

        render() {
            var n = 20, // number of layers
                m = 200, // number of samples per layer
                stack = d3.stack().offset(d3.stackOffsetWiggle),
                layers0 = stack(d3.range(n).map(function () {
                    return bumpLayer(m);
                })),
                layers1 = stack(d3.range(n).map(function () {
                    return bumpLayer(m);
                }));

            var width = 960,
                height = 500;

            var x = d3.scaleLinear()
                .domain([0, m - 1])
                .range([0, width]);

            var y = d3.scaleLinear()
                .domain([0, d3.max(layers0.concat(layers1), function (layer) {
                    return d3.max(layer, function (d) {
                        return d.y0 + d.y;
                    });
                })])
                .range([height, 0]);

            var color = d3.scaleLinear()
                .range(["#aad", "#556"]);

            var area = d3.area()
                .x(function (d) {
                    return x(d.x);
                })
                .y0(function (d) {
                    return y(d.y0);
                })
                .y1(function (d) {
                    return y(d.y0 + d.y);
                });

            var svg = d3.select(this._el).append("svg")
                .attr("width", width)
                .attr("height", height);

            svg.selectAll("path")
                .data(layers0)
                .enter().append("path")
                .attr("d", area)
                .style("fill", function () {
                    return color(Math.random());
                });
        },

        destroy: function () {
            //this._chart.destroy();
            //this._chart = null;
        },

        updateDefinition(newDef) {
            this.init(newDef);
        },

        init: function () {
            this.render();
            return;
            this._el.removeClass("ewarschart");

            this._el.html('<i class="fal fa-spinner fa-spin"></i>');

            // Parse definition
            var date_range = DateUtils.processDateSpec(this._config, this._reportDate);
            var options = {start_date: date_range[0], end_date: date_range[1]};
            options.location = location_uuid;
            options.template_id = template_id;
            var query = AnalysisUtils.buildQuery(this._config, options);

            // Create data source
            var ds = new DataSource(query, isPublic);

            // Load data
            ds.load(function (source) {
                    this._el.replaceWith("<span>" + ewars.NUM(source.data, this._config.format || "0.0,00") + "</span>");
                    ewars.emit("WIDGET_LOADED");
                }.bind(this),
                function (errType, data) {
                    this._el.replaceWith('<i class="fal fa-exclamation-triangle red"></i>');
                    ewars.emit("WIDGET_LOADED");
                }.bind(this))

        }

    };

    instance.init();

    return instance;
};