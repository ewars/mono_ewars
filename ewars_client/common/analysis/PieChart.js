import DateUtils from "../../documents/utils/DateUtils";
import AnalysisUtils from "../utils/AnalysisUtils";

import RangeUtils from "../utils/RangeUtils";

import DataSource from "../models/DataSource";


// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
    Object.keys = (function () {
        'use strict';
        var hasOwnProperty = Object.prototype.hasOwnProperty,
            hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
            dontEnums = [
                'toString',
                'toLocaleString',
                'valueOf',
                'hasOwnProperty',
                'isPrototypeOf',
                'propertyIsEnumerable',
                'constructor'
            ],
            dontEnumsLength = dontEnums.length;

        return function (obj) {
            if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
                throw new TypeError('Object.keys called on non-object');
            }

            var result = [], prop, i;

            for (prop in obj) {
                if (hasOwnProperty.call(obj, prop)) {
                    result.push(prop);
                }
            }

            if (hasDontEnumBug) {
                for (i = 0; i < dontEnumsLength; i++) {
                    if (hasOwnProperty.call(obj, dontEnums[i])) {
                        result.push(dontEnums[i]);
                    }
                }
            }
            return result;
        };
    }());
}

Highcharts.setOptions({
    plotOptions: {
        series: {
            animation: false
        }
    }
});

var defaults = {};

var COLOURS = [
    "orange", "red", "purple", "pink", "yellow", "green", "magenta", "grey", "black", "aque", "blue", "chocolate", "cyan"
];

var LEGENDS = {
    top: {
        enabled: true,
        align: "center",
        verticalAlign: "top",
        layout: "horizontal",
        x: 0,
        y: 0
    },
    right: {
        enabled: true,
        align: "right",
        verticalAlign: "top",
        layout: "vertical",
        x: 0,
        y: 0
    },
    bottom: {
        enabled: true,
        align: "center",
        verticalAlign: "bottom",
        layout: "horizontal",
        x: 0,
        y: 0
    },
    left: {
        enabled: true,
        align: "left",
        verticalAlign: "top",
        layout: "vertical",
        x: 0,
        y: 0
    }
};

var SORT_PROP = {
    "CAT_NAME": "name",
    "CAT_NAME_DESC": "name",
    "VALUE_ASC": "value",
    "VALUE_DESC": "value"
};


const PieChart = function (el, definition, reportDate, location_uuid, isPublic, idx) {
    var instance = {
        _report_date: reportDate,
        _location_uuid: location_uuid,
        _id: null,
        _el: null,
        _chart: null,
        _validSeries: [],
        _definition: {},
        _data: {},
        _series: [],
        _hide_no_data: false,
        _slices: {},

        _populate: function () {
            if (this._series.length <= 0) {
                this._chart.redraw();
                return;
            }

            while (this._chart.series.length > 0)
                this._chart.series[0].remove(true);

            var colourCount = 0;

            var slices = [];
            var categories = [];

            // Iterate over series and get the data slices
            this._series.forEach(dataSource => {
                var sliceDef = dataSource.series;
                if (Array.isArray(dataSource.result)) {
                    dataSource.result.forEach(subSeries => {

                        var shouldAdd = true;
                        if (this._hide_no_data && subSeries.data <= 0) shouldAdd = false;

                        if (shouldAdd) {
                            categories.push(ewars.I18N(subSeries.location.name));
                            slices.push({
                                name: ewars.I18N(subSeries.location.name),
                                color: sliceDef.colour || null,
                                y: subSeries.data,
                                value: subSeries.data
                            })
                        }
                    });
                } else {
                    var shouldAdd = true;
                    var value = dataSource.result.data;
                    if (this._hide_no_data && value <= 0) shouldAdd = false;
                    var slice = dataSource.series;

                    if (shouldAdd) {
                        categories.push(ewars.I18N(slice.title));
                        slices.push({
                            name: slice.title ? ewars.I18N(slice.title) : "",
                            color: slice.colour || null,
                            y: value,
                            value: value
                        })
                    }
                }
            });

            if (this._definition.slice_ordering != "NONE" && this._definition.slice_ordering) {
                slices = slices.sort((a, b) => {
                    let aO = a[SORT_PROP[this._definition.slice_ordering]],
                        bO = b[SORT_PROP[this._definition.slice_ordering]];
                    if (aO > bO) return 1;
                    if (aO < bO) return -1;
                    return 0;
                });

                if (this._definition.slice_ordering.indexOf("DESC") >= 0) {
                    slices.reverse()
                }
            }

            categories = [];
            slices.forEach(slice => {
                categories.push(slice.name);
            });

            this._chart.xAxis[0].setCategories(categories);

            var series = {
                name: this._definition.title,
                colorByPoint: true,
                data: slices,
                lineWidth: 1
            };

            if (this._definition.style == "ARC") {
                series.innerSize = "50%";
            }

            if (this._definition.style == "DONUT") {
                series.innerSize = "50%";
            }

            if (this._definition.style == "BAR") {
                series.type = "bar";
            }

            if (this._definition.style == "TREEMAP") {
                series.type = "treemap";
                series.layoutAlgorithm = "squarified";

                this._chart.colorAxis = {
                    minColor: "#FFFFFF",
                    maxColor: "red"
                }
            }

            this._chart.hideLoading();
            this._chart.addSeries(series);

            this._chart.redraw();
        },

        _receiveData: function (id, data) {
            this._data[id] = data;

            var isLoaded = true;

            for (let key in this._series) {
                let val = this._series[key];
                if (!val._isLoaded) isLoaded = false;
            }

            if (isLoaded) this._populate();
        },

        getData: function () {
            return ["CATEGORY", [definition, this._series]];
        },

        destroy: function () {
            this._chart.destroy();
            this._chart = null;
        },

        updateDefinition: function (newDef) {
            this.init(newDef);
        },

        init: function (newDef) {
            this._id = ewars.utils.uuid();
            this._el = el;

            if (this._chart) {
                this._chart.destroy();
                this._chart = null;
            }

            var config = newDef || definition;
            this._definition = config;
            this._hide_no_data = config.hide_no_data || false;

            var title = false;
            if (config.show_title) {
                if (config.title) {
                    title = ewars.I18N(config.title);
                }
            }

            var plotOptions = {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {y}',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true
                },
                column: {
                    borderWidth: 1,
                    stacking: "normal"
                },
                bar: {
                    borderWidth: 1,
                    stacking: "normal"
                }
            };

            if (config.style == "ARC") {
                plotOptions.pie.startAngle = -90;
                plotOptions.pie.endAngle = 90;
                plotOptions.pie.center = ['50%', '75%'];
            }

            let legend = {enabled: false};
            if (config.show_legend) {
                legend.enabled = true;
                if (config.legend_position) {
                    legend = LEGENDS[config.legend_position];
                }
            }

            if (config.style == "BAR") legend.enabled = false;

            var marginTop = null,
                marginRight = null,
                marginBottom = null,
                marginLeft = null;

            if (legend.enabled) {
                legend.navigation = {enabled: false};
                if (legend.align == "left") marginLeft = 120;
                if (legend.align == "top") marginTop = 120;
                if (legend.align == "right") marginRight = 120;
                if (legend.align == "bottom") marginBottom = 120;
            }

            let height;
            if (config.height && config.height != "") {
                height = config.height;
                if (height.indexOf("px") >= 0) height = height.replace("px", "");
                if (height.indexOf("px") >= 0) height = height.replace("%", "");
                if (height == "") height = null;
            } else {
                height = this._el.parentNode.clientHeight;
            }

            let width;
            if (config.width && config.width != "") {
                width = config.width;
                if (width.indexOf("px") >= 0) width = width.replace("px", "");
                if (width.indexOf("px") >= 0) width = width.replace("%", "");
                if (width == "") width = null;
            } else {
                width = this._el.parentNode.clientWidth;
            }
            console.log(legend);

            // Map in options
            var options = {
                chart: {
                    renderTo: el,
                    animation: false,
                    type: "pie",
                    backgroundColor: 'rgba(255,255,255,0)',
                    marginTop: marginTop,
                    marginRight: marginRight,
                    marginBottom: marginBottom,
                    marginLeft: marginLeft,
                    height: height,
                    width: width
                },
                credits: {enabled: false},
                title: {
                    text: title,
                    align: 'center'
                },
                noData: "",
                exporting: {
                    enabled: false
                },
                navigator: {
                    enabled: config.navigator
                },
                legend: {
                    ...legend,
                    navigation: {
                        enabled: false
                    }
                },
                plotOptions: plotOptions,
                series: [],
                tooltip: {
                    formatter: function () {
                        var seriesName = ewars.I18N(this.series.name);

                        return '<span style="color: ' + this.color + '">●</span><span style="font-size: 10px">' + this.key + ': </span> <b>' + this.y + '</b><br />';
                    }
                },
                yAxis: {
                    min: 0,
                    labels: {
                        formatter: function () {
                            return ewars.NUM(this.value, config.y_axis_format || "0")
                        }
                    },
                    title: {
                        text: ewars.I18N(config.y_axis_label) || "Number"
                    },
                    allowDecimals: config.y_axis_allow_decimals || false
                }
            };


            if (!this._chart) {
                this._chart = new Highcharts.Chart(options);
            }

            var options = {};
            if (config.period) {
                options.end_date = RangeUtils.process(config.period[1], this._report_date);
                options.start_date = RangeUtils.process(config.period[0], this._report_date, options.end_date);
            } else {
                let dateRange = DateUtils.processDateSpec(config, this._report_date);
                options.start_date = dateRange[0];
                options.end_date = dateRange[1];
            }
            options.location = location_uuid;

            if (idx) {
                if (ewars.g[idx]) {
                    this._series = ewars.g[idx];
                    this._populate();
                    return;
                } else {
                    ewars.g[idx] = [];
                }
            }

            this._chart.showLoading();
            this._seriesLoaded = 0;
            this._series = [];
            (config.slices || config.series).forEach((series, index) => {
                series.index = index;
                let startDate = options.start_date;
                let endDate = options.end_date;

                if (series.override == "OVERRIDE") {
                    endDate = RangeUtils.process(series.period[1], this._report_date);
                    startDate = RangeUtils.process(series.period[0], this._report_date, endDate);
                }

                let query = {};

                if (series.source_type == "SLICE") {
                    query = {
                        uuid: ewars.utils.uuid(),
                        type: "SLICE",
                        indicator: series.indicator,
                        interval: config.interval,
                        start_date: startDate,
                        end_date: endDate,
                        location: series.location || options.location,
                        reduction: series.reduction
                    }
                }

                if (series.source_type == "SLICE_COMPLEX") {
                    query = {
                        uuid: ewars.utils.uuid(),
                        type: "SLICE_COMPLEX",
                        formula: series.formula,
                        interval: config.interval,
                        start_date: startDate,
                        end_date: endDate,
                        series: series.series,
                        location: series.location || options.location,
                        reduction: series.reduction
                    }
                }

                if (series.loc_spec == "GENERATOR") {
                    query.location = {
                        parent_id: series.generator_locations_parent,
                        site_type_id: series.generator_locations_type,
                        status: series.generator_location_status || "ACTIVE"
                    }
                } else if (series.loc_spec == "REPORT_LOCATION") {
                    query.location = location_uuid;
                } else if (series.loc_spec == "GROUP") {
                    query.location = {
                        agg: series.group_output,
                        groups: series.group_ids
                    }
                }


                this._slices[query.uuid] = series;

                let token;
                if (window.dt) token = window.dt.token || null;
                ewars._queue.push("/arc/analysis", query, token)
                    .then(resp => {
                        if (resp) {
                            if (idx) ewars.g[idx].push({
                                result: resp,
                                series: series
                            });
                            this._series.push({
                                result: resp,
                                series: series,
                                uuid: query.uuid
                            });
                            this._seriesLoaded++;
                            if (this._seriesLoaded == config.slices.length) {
                                this._populate();
                                if (!this._emitLoaded) {
                                    this._emitLoaded = true;
                                    ewars.emit("WIDGET_LOADED");
                                }
                            }
                        } else {
                            this._seriesLoaded++;
                            if (this._seriesLoaded == config.slices.length) {
                                if (!this._emitLoaded) {
                                    this._emitLoaded = true;
                                    ewars.emit("WIDGET_LOADED");
                                }
                            }
                        }
                    })
                    .catch((err) => {
                        this._seriesLoaded++;
                        if (this._seriesLoaded == config.slices.length) {
                            if (!this._emitLoaded) {
                                this._emitLoaded = true;
                                ewars.emit("WIDGET_LOADED");
                            }
                        }
                    })

            });

        }

    };

    instance.init();

    return instance;
};

export default PieChart;
