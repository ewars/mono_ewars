import Moment from "moment";
import DateUtils from "../../documents/utils/DateUtils";
import AnalysisUtils from "../utils/AnalysisUtils";

import RangeUtils from "../utils/RangeUtils";
import DefinitionUtils from "../editors/widgets/DefinitionUtils";

import DataSource from "../models/DataSource";

var defaults = {};

import CONSTANTS from "../constants";

if (Highcharts) {
    Highcharts.setOptions({
        global: {
            useUTC: true
        }
    });
}

let DEFAULTS = {
    chart: {
        renderTo: null,
        animation: false,
        height: 300,
        width: null,
        backgroundColor: 'rgba(255,255,255,0)'
    },
    credits: {enabled: false},
    title: {
        text: null,
        align: 'center'
    },
    noData: "",
    exporting: {
        enabled: false
    },
    navigator: {
        enabled: false
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            stacking: null,
            states: {
                hover: {
                    lineWidthPlus: 0,
                    marker: {
                        radius: null,
                        states: {
                            hover: {
                                enabled: false
                            }
                        }
                    }
                }
            }
        },
        column: {
            stacking: null,
            seriesPadding: 0.05,
            groupPadding: 0.05,
            borderWidth: 0
        },
        bar: {
            seriesPadding: 0.05,
            groupPadding: 0.05,
            borderWidth: 0
        }
    },
    tooltip: {},
    yAxis: {
        labels: {
            formatter: function () {
                return ewars.NUM(this.value, config.y_axis_format || "0");
            }
        },
        max: null,
        title: {
            text: "Number"
        },
        min: 0,
        alternateBandColor: "#F2F2F2",
        allowDecimals: false,
        plotBands: null,
        plotLines: null
    },
    xAxis: {
        type: "datetime",

        labels: {
            autoRotation: [-45],
            rotation: -90,
            formatter: function () {
                return Moment.utc(this.value).clone().subtract(1, 'd').format("YYYY-MM-DD");
            }
        },
        plotBands: null,
        plotLines: null
    }
};

let X_AXIS = {
    type: "datetime",
    startOfWeek: 0,
    minTickInterval: Moment.duration(7, 'day').asMilliseconds(),
    labels: {
        autoRotation: [-45],
        rotation: -90,
        formatter: function () {
            return Moment.utc(this.value).clone().subtract(1, 'd').format("YYYY-MM-DD");
        }
    },
    plotBands: null,
    plotLines: null
};

function formatDecoupled(dt, format) {
    if (format == "WEEK") {
        return Moment.utc(dt).format("[W]W")
    } else if (format == "MONTH") {
        return Moment.utc(dt).format("MMM")
    } else if (format == "YEAR") {
        return Moment.utc(dt).format("YYYY")
    } else {
        return ewars.DATE(dt, "DAY")
    }
}

const SeriesChart = function (el, definition, reportDate, location_uuid, isPublic, idx) {

    var instance = {
        _report_date: reportDate,
        _location_uuid: location_uuid,
        _id: null,
        _el: null,
        _chart: null,
        _validSeries: [],
        _definition: {},
        _data: {},
        _seriesDef: {},
        _categories: [],
        _seriesLoaded: 0,


        _setDataSeries: function (index, data) {
            this._data[index] = data;
        },

        getData: function () {
            return ["SERIES", [definition, this._series]];
        },

        _render: function (data, indicator, location, series, config, targetIndex) {
            this._series.push({
                data: data,
                indicator: indicator,
                location: location,
                series: series
            });
            // If this series has a fn attribute, it means that the function
            // should be run against the series before its used
            if (series.fn) {
                data = series.fn(data);
            }

            let graph = {
                data: [],
                marker: {
                    enabled: true,
                    radius: series.marker_radius || 3,
                    symbol: series.marker_symbol || "circle"
                },
                lineWidth: parseFloat(series.line_width) || 1,
                _eData: {location: location || null},
                dashStyle: series.line_style ? series.line_style : "Solid",
                color: series.colour || undefined,
                type: series.style,
                fillOpacity: 0.6,
                zIndex: series.index,
                index: series.index,
                xAxis: targetIndex
            };

            graph.data = data.sort((a, b) => {
                if (a[0] > b[0]) return 1;
                if (a[0] < b[0]) return -1;
                return 0;
            });

            graph.data = graph.data.map(node => {
                var value = null;
                if (!isNaN(parseFloat(node[1]))) value = parseFloat(node[1]);
                if (config.decoupled) {
                    return [formatDecoupled(node[0], config.interval), value]
                } else {
                    return [Moment.utc(node[0], "YYYY-MM-DD").valueOf(), value];
                }
            }, this);

            let title = "";
            if (series.title) title += ewars.I18N(series.title);
            if (location) title += " - " + ewars.I18N(location.name);
            graph.name = title;
            this._chart.addSeries(graph);

            // if (this._seriesLoaded == definition.series.length) ewars.emit("WIDGET_LOADED");

        },

        destroy: function () {
            this._chart.destroy();
            this._chart = null;
        },

        setup: function (definition) {
            if (this._chart) {
                this._chart.destroy();
                this._chart = null;
            }
            this._definition = definition;
            var config = this._definition;

            var title = false;
            if (config.show_title) {
                if (config.title) {
                    title = ewars.formatters.I18N_FORMATTER(config.title);
                }
            }

            let chartOptions = ewars.copy(DEFAULTS);

            chartOptions.chart.renderTo = this._el;

            if (config.interval == "WEEK" && !config.decouple) {
                //chartOptions.xAxis.minTickInterval = 7 * 24 * 3600 * 1000;
                chartOptions.xAxis.startOfWeek = 0;
                chartOptions.xAxis.labels.formatter = function () {
                    return Moment.utc(this.value).format("[W]WW GGGG");
                }
            } else {
                chartOptions.xAxis.labels.formatter = function() {
                    return Moment.utc(this.value).subtract(1, 'd').format("YYYY-MM-DD")
                }
            }


            if (config.hide_x_labels) chartOptions.xAxis.labels.formatter = function () {
                return ""
            };
            if (config.x_label_rotation) chartOptions.xAxis.labels.rotation = config.x_label_rotation;

            let height;
            if (config.height && config.height != "") {
                height = config.height;
                if (height.indexOf("px") >= 0) height = height.replace("px", "");
                if (height.indexOf("%") >= 0) height = height.replace("%", "");
                if (height == "") height = null;
            }

            let width;
            if (config.width && config.width != "") {
                width = config.width;
                if (width.indexOf("px") >= 0) width = width.replace("px", "");
                if (width.indexOf("%") >= 0) width = null;
                if (width == "") width = null;
            } else {
                width = this._el.parentNode.offsetWidth;
            }

            if (width == null) width = this._el.parentNode.offsetWidth;

            chartOptions.chart.zoomType = config.zoom || null;
            chartOptions.chart.height = height || "300";
            chartOptions.chart.height = chartOptions.chart.height.replace("px", "");
            chartOptions.chart.width = width;

            chartOptions.title.text = title || null;
            chartOptions.exporting.enabled = config.tools == true;
            chartOptions.navigator.enabled = config.navigator == true;
            chartOptions.legend.enabled = config.show_legend == true;
            chartOptions.plotOptions.series.stacking = config.stack ? "normal" : null;
            chartOptions.plotOptions.column.stacking = config.stack ? "normal" : null;
            chartOptions.tooltip.formatter = function () {
                let interval = config.interval || "DAY";
                let plotDate = this.key;
                if (!config.decouple) {
                    plotDate = ewars.DATE(this.key, interval);
                }
                let seriesName = ewars.I18N(this.series.name);
                let locationName = "";
                if (this.series.options._eData.location)
                    locationName = ewars.I18N(this.series.options._eData.location.name);

                let val = this.y;
                val = ewars.NUM(val, config.y_axis_format || "0");

                return `<span style="font-size: 10px;">${plotDate} - ${locationName} </span><br/><span style="color: ${this.color}">●</span><span style="font-size: 10px">${seriesName}</span><b> ${val}</b><br/>`;
            };

            chartOptions.yAxis.title.text = config.y_axis_label || "Number";
            chartOptions.yAxis.title.enabled = config.hide_y_title ? true : null;
            if (config.hide_y_title) chartOptions.yAxis.title.text = null;
            chartOptions.yAxis.tickInterval = config.y_tick_interval || null;
            chartOptions.yAxis.max = config.max_y_value || null;
            chartOptions.yAxis.allowDecimals = config.y_axis_allow_decimals || false;
            chartOptions.yAxis.plotBands = config.yPlotBands || null;
            chartOptions.yAxis.plotLines = config.yPlotLines || null;
            chartOptions.yAxis.labels.formatter = function () {
                return ewars.NUM(this.value, config.y_axis_format || "0");
            };


            if (!this._chart) {
                this._chart = new Highcharts.Chart(chartOptions);
            }

            this._validSeries = [];
            this._data = {};

            let options = {};
            if (config.period) {
                options.end_date = RangeUtils.process(config.period[1], this._report_date);
                options.start_date = RangeUtils.process(config.period[0], this._report_date, options.end_date);
            } else {
                let dateRange = DateUtils.processDateSpec(config, this._report_date);
                options.start_date = dateRange[0];
                options.end_date = dateRange[1];
            }
            options.location = location_uuid;
            options.interval = config.interval;

            this._series = [];
            this._seriesLoaded = 0;

            if (idx) {
                if (ewars.g[idx]) {
                    ewars.g[idx].forEach(item => {
                        this._render(item[0], item[1], item[2], item[3], item[4]);
                    });
                    return;
                } else {
                    if (idx) if (!ewars.g[idx]) ewars.g[idx] = []; // Store cached data
                }
            }

            let seriesIndex = 0;
            let totalIndexes = 0;
            config.series.forEach((series, index) => {
                let targetAxis = 0;

                if (series.override == "OVERRIDE") {
                    options.end_date = RangeUtils.process(series.period[1], this._report_date);
                    options.start_date = RangeUtils.process(series.period[0], this._report_date, options.end_date);

                    // Add a secondary series for the chart
                    let newSeries = ewars.copy(DEFAULTS.xAxis);
                    if (config.interval == "WEEK" && !config.decouple) {
                        //newSeries.minTickInterval = 7 * 24 * 3600 * 1000;
                        newSeries.startOfWeek = 0;
                        newSeries.labels.formatter = function () {
                            return Moment.utc(this.value).format("[W]WW YYYY");
                        }
                    }

                    if (config.hide_x_labels) newSeries.labels.formatter = function () {
                        return ""
                    };
                    if (config.x_label_rotation) newSeries.labels.rotation = config.x_label_rotation;
                    this._chart.addAxis(newSeries, true, true);
                    totalIndexes += 1;
                    targetAxis = totalIndexes;
                }

                let query = {
                    type: series.source_type || series.type,
                    indicator: series.indicator,
                    interval: config.interval,
                    start_date: options.start_date,
                    formula: series.formula,
                    series: series.series,
                    end_date: options.end_date,
                    location: series.location || options.location,
                    fill: true
                };

                if (series.loc_spec) {
                    if (series.loc_spec == "GENERATOR") {
                        query.location = {
                            status: series.generator_location_status,
                            parent_id: series.generator_locations_parent,
                            site_type_id: series.generator_locations_type
                        }
                    }

                    if (series.loc_spec == "GROUP") {
                        query.location = {
                            groups: series.group_ids,
                            agg: series.group_output
                        }
                    }
                }

                series.index = index;

                this._seriesDef[query.uuid] = series;

                ewars._queue.push("/arc/analysis", query)
                    .then(resp => {
                        if (resp) {
                            if (resp.location == undefined || resp.location == null) {
                                resp.data.forEach((sub) => {
                                    this._render(sub.data, sub.indicator, sub.location, series, config, targetAxis);
                                })
                                this._seriesLoaded++;
                                if (this._seriesLoaded == config.series.length) ewars.emit("WIDGET_LOADED");
                            } else {
                                if (idx) ewars.g[idx].push([resp.data, resp.indicator, resp.location, series, config]);
                                this._render(resp.data, resp.indicator, resp.location, series, config, targetAxis);
                                this._seriesLoaded++;
                                if (this._seriesLoaded == config.series.length) ewars.emit("WIDGET_LOADED");
                            }

                        } else {
                            this._seriesLoaded++;
                            if (this._seriesLoaded == config.series.length) ewars.emit("WIDGET_LOADED");
                        }
                    })
                    .catch(err => {
                        console.log("SERIES ERROR");
                    })

            });
        },

        updateDefinition: function (definition) {
            this.setup(definition);
        },

        init: function () {
            this._id = ewars.utils.uuid();
            this._el = el;

            this.setup(definition);
        },

        _getData: function () {
            return ["SERIES", [this._definition, this._data]];

        }
    };

    instance.init();

    return instance;
};

export default SeriesChart;
