import Moment from "moment";
import DateUtils from "../../documents/utils/DateUtils";
import DataSource from "../models/DataSource";
import AnalysisUtils from "../utils/AnalysisUtils";
var defaults = {};

Highcharts.setOptions({
    global: {
        useUTC: true
    }
});

const Gauge = function (el, definition, reportDate, location_uuid, template_id, isPublic) {
    var instance = {
        _config: definition,
        _reportDate: reportDate,
        _el: el,
        _isPublic: isPublic,

        getData: function () {
            return this._value;
        },

        render() {
            if (this._chart) {
                this._chart.destroy();
                this._chart = null;
            }

            let ranges = definition.ranges.split(",");
            let colours = definition.colours.split(",");

            let threshes = [];
            ranges.forEach((item, index) => {
                threshes.push([item, colours[index]])
            });

            var gaugeOptions = {

                chart: {
                    type: 'solidgauge',
                    renderTo: this._el
                },

                title: null,

                pane: {
                    center: ['50%', '85%'],
                    size: '100%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: '#EEE',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },

                tooltip: {
                    enabled: false
                },

                // the value axis
                yAxis: {
                    stops: threshes,
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickAmount: 2,
                    title: {
                        y: -70,
                        text: "Value"
                    },
                    labels: {
                        y: 16
                    },
                    min: 0
                },

                credits: {
                    enabled: false
                },

                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                },

                series: [{
                    name: ewars.I18N(definition.title),
                    data: [this._result.data],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                        ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                        '<span style="font-size:12px;color:silver"></span></div>'
                    },
                    tooltip: {
                        valueSuffix: ' km/h'
                    }
                }]
            };

            this._chart = new Highcharts.Chart(gaugeOptions);
        },

        destroy: function () {
            this._chart.destroy();
            this._chart = null;
        },

        updateDefinition(newDef) {
            this.init(newDef);
        },

        init: function () {
            // Parse definition
            var date_range = DateUtils.processDateSpec(this._config, this._reportDate);
            var options = {start_date: date_range[0], end_date: date_range[1]};
            options.location = location_uuid;
            options.template_id = template_id;

            let query = {
                uuid: ewars.utils.uuid(),
                location: definition.location,
                indicator: definition.indicator,
                interval: definition.interval,
                type: "SLICE",
                start_date: options.start_date,
                end_date: options.end_date,
                reduction: definition.reduction
            };

            var uri = "http://" + ewars.domain + "/arc/analysis";

            $.ajax({
                url: uri,
                dataType: "JSON",
                contentType: "application/json",
                context: this,
                type: "POST",
                data: JSON.stringify(query),
                success: function (resp) {
                    this._result = resp;
                    ewars.emit("WIDGET_LOADED");
                    this.render();
                },
                error: function () {
                    ewars.emit("WIDGET_LOADED");
                }
            })

        }

    };

    instance.init();

    return instance;
};

export default Gauge;
