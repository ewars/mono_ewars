import {Layout, Row, Cell} from "./Layout";

const STYLE = {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14
};

const ICON_STYLE = {
    fontSize: 90,
    textAlign: "center",
    marginBottom: 45
};

class Empty extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Layout>
                <Row>
                </Row>
                <Row>
                    <Cell style={STYLE}>
                        {this.props.icon ?
                            <div className="empty-icon" style={ICON_STYLE}>
                                <i className={"fal " + this.props.icon}></i>
                            </div>
                        : null}
                        <p>{this.props.text}</p>
                    </Cell>
                </Row>
                <Row></Row>
            </Layout>
        )
    }
}

export default Empty;