import Control from "./c.control";

class Dashboard extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let counter = 0;
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll dashboard">
                <div className="grid">
                    {this.props.data.definition.map((row, index) => {
                        counter++;
                        let key = `${this.props.data.uuid}_${counter}`;
                        return <Row
                            data={row}
                            uuid={this.props.data.uuid}
                            key={key}
                            index={index}/>
                    })}
                </div>
            </div>
        )
    }
}

class EditableList extends React.Component {
    static defaultProps = {
        orderable: false,
        onAdd: null,
        onRemove: null,
        onOrder: null,
        items: [],
        onChange: null,
        Template: null
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedIndex: null
        }
    }

    _handleAction = (action) => {
        this.props.onAction(
            action,
            this.state.selectedIndex,
            this.props.items[this.state.selectedIndex]
        );
    };

    _onItemClick = (index) => {
        this.setState({
            selectedIndex: index
        })
    };

    _clearSelected = (evt) => {
    };

    render() {
        let selected;
        if (this.state.selectedIndex != null) {
            selected = this.props.items[this.state.selectedIndex];
        }

        return (
            <div className="iw-edit-list">
                <div className="iw-edit-list-items" onClick={this._clearSelected}>
                    {this.props.items.map((item, index) => {
                        return <this.props.Template
                            key={"EL_ITEM_" + index}
                            active={index == this.state.selectedIndex}
                            data={item}
                            onChange={this.props.onChange || null}
                            onAction={this.props.onAction}
                            index={index}
                            onClick={this._onItemClick}/>
                    })}
                </div>
                <div className="iw-edit-list-controls">
                    {this.props.actions.map(action => {
                        let show = true;
                        if (action.show) {
                            show = false;
                        }
                        return (
                            <Control
                                key={"CONTROL_" + action.action}
                                icon={action.icon}
                                action={action.action}
                                onClick={this._handleAction}/>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default EditableList;
