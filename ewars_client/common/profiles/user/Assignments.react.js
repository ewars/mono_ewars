var Assignments = React.createClass({
    getInitialState: function () {
        return {
            assignments: []
        }
    },

    render: function() {
        if (this.props.data.role != "USER") {
            return (
                <div className="ide-panel ide-panel-absolute ide-scoll">
                    <div className="placeholder">This user does not have any assignments</div>
                </div>
            )
        }

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">

            </div>
        )
    }
});

export default Assignments;
