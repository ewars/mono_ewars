import CONSTANTS from "../../constants";
import USER_TYPES from "../../constants/user_types";
import Assignments from "./user/Assignments.react";

const TABS = [
    {id: "ASSIGNMENTS", label: "Assignments", icon: CONSTANTS.ICO_DASHBOARD}
];

const STYLES = {
    NAME: {fontSize: 20, fontWeight: "bold", marginBottom: 16},
    META: {fontSize: 12, display: "block", marginBottom: 6}
};

var PanelBarButton = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var className = "panelsbar-btn";
        if (this.props.data.id == this.props.activeView) className += " panelsbar-btn-active";

        return (
            <div xmlns="http://www.w3.org/1999/xhtml"
                 onClick={this._onClick}
                 className={className}><i
                className={this.props.data.icon}></i>&nbsp;{this.props.data.label}
            </div>
        )
    }
});

var UserProfile = React.createClass({
    getInitialState: function () {
        return {
            data: null,
            view: "ASSIGNMENTS"
        }
    },

    _swapView: function (data) {
        this.setState({
            view: data.id
        })
    },

    render: function () {
        var tabs = _.map(TABS, function (tab) {
            return <PanelBarButton
                activeView={this.state.view}
                onClick={this._swapView}
                data={tab}/>

        }, this);

        var orgName = ewars.I18N(this.props.data.organization_name);

        var location, occupation;
        if (this.props.data.profile) {
            if (this.props.data.profile.location) location = this.props.data.profile.location;
            if (this.props.data.profile.occupation) occupation = this.props.data.profile.occupation;
        }
        if (this.props.data.location_name) location = ewars.I18N(this.props.data.location_name);

        var view;
        if (this.state.view == "ASSIGNMENTS") view = <Assignments data={this.props.data}/>;

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="basic" style={{background: "#FFF", padding: 16, paddingBottom: 16}}>
                            <div className="ide-layout light">
                                <div className="ide-row">
                                    <div className="ide-col" style={{maxWidth: 150}}>
                                        <img width={150} src="/static/image/avatar-placeholder.jpg"/>
                                    </div>
                                    <div className="ide-col" style={{background: "#FFF"}}>
                                        <div className="basic" style={{padding: "0 8px 8px 16px", background: "#FFF"}}>
                                            <h1 style={STYLES.NAME}>{this.props.data.name}</h1>

                                            <table>
                                                <tr>
                                                    <td>
                                                        {this.props.data.organization_name ?
                                                            <div style={STYLES.META}>
                                                                <i className="fal fa-building"></i> {ewars.I18N(this.props.data.organization_name)}
                                                            </div>
                                                            : null}
                                                        {this.props.data.lab_name ?
                                                            <div style={STYLES.META}>
                                                                <i className="fal fa-flask"></i> {ewars.I18N(this.props.data.lab_name)}
                                                            </div>
                                                            : null}
                                                        <div style={STYLES.META}>
                                                            <i className="fal fa-envelope"></i> <a
                                                            href={"mailto:"+this.props.data.email}>{this.props.data.email}</a>
                                                        </div>
                                                        <div style={STYLES.META}>
                                                            <i className="fal fa-phone"></i> {this.props.data.phone || "None provided"}
                                                        </div>
                                                    </td>
                                                    <td width={20}>&nbsp;</td>
                                                    <td>
                                                        <div style={STYLES.META}>
                                                            <i className="fal fa-user"></i> {ewars.I18N(USER_TYPES[this.props.data.role].name)}
                                                        </div>
                                                        <div style={STYLES.META}>
                                                            <i className="fal fa-briefcase"></i> {occupation || "None provided"}
                                                        </div>
                                                        <div style={STYLES.META}>
                                                            <i className="fal fa-map-marker"></i> {ewars.I18N(this.props.data.location_name)}
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        )
    }
});

export default UserProfile;
