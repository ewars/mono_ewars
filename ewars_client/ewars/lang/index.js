import CONTENT from "./en/content";
import DATES from "./en/dates";
import FIELD from "./en/field";
import STRINGS from "./en/strings";

const LANG = {
    ...CONTENT,
    ...DATES,
    ...FIELD,
    ...STRINGS
};

var _l = (langString) => {
    if (LANG[langString]) {
        return LANG[langString];
    } else {
        return langString;
    }
};

export default _l;

