import utils from "../utils/general";

var _notifications = [];

function _fadeOut(el) {
    el.style.opacity = 1;

    (function fade() {
        if ((el.style.opacity -= .1) < 0) {
            el.parentNode.removeChild(el);
        } else {
            requestAnimationFrame(fade);
        }
    })();
}

const Notification = {
    _buildBase: function (id, iconString, titleString, contentString, colour) {
        if (!colour) colour = 'blue';


        var wrapper = document.createElement("div");
        wrapper.setAttribute("class", "ew-not-modal modal " + colour);
        wrapper.setAttribute("id", id);


        // Set up header
        var header = document.createElement("div");
        header.setAttribute("class", "header");

        var iconWrapper = document.createElement("div");
        iconWrapper.setAttribute("class", "icon");
        var icon = document.createElement("I");
        icon.setAttribute("class", "fal " + iconString);
        iconWrapper.appendChild(icon);

        var title = document.createElement("div");
        title.setAttribute("class", "title");

        var titleText = document.createTextNode(titleString);
        title.appendChild(titleText);
        header.appendChild(iconWrapper);
        header.appendChild(title);

        wrapper.appendChild(header);

        // Set up Body
        var body = document.createElement("div");
        body.setAttribute("class", "body");
        var content = document.createTextNode(contentString);
        body.appendChild(content);

        wrapper.appendChild(body);

        return wrapper;
    },
    /**
     * Provides a way to show notifications to the user form anywhere in the application
     * @param icon
     * @param title
     * @param content
     */
    notification: function (iconString, titleString, contentString, timeout, colour) {
        if (!timeout) timeout = 5000;

        var id = utils.uniqueId("notification");

        var wrapper = this._buildBase(id, iconString, titleString, contentString, colour);

        var notStream = document.getElementById("notification-stream");

        notStream.appendChild(wrapper);

        setTimeout(function () {
            var item = document.getElementById(id);
            _fadeOut(item);
        }, timeout);
    },

    error: function (message) {
        let id = utils.uniqueId("error");
        let wrapper = document.createElement("div");
        wrapper.setAttribute("id", id);
        wrapper.setAttribute("class", "error-wrapper");

        let inner = document.createElement("div");
        inner.setAttribute("class", "error-inner");

        let icon = document.createElement("i");
        icon.setAttribute("class", 'fal fa-exclamation-triangle');
        let text = document.createTextNode(" " + message);

        inner.appendChild(icon);
        inner.appendChild(text);
        wrapper.appendChild(inner);

        let notStream = document.getElementById("notification-stream");
        notStream.appendChild(wrapper);

        setTimeout(() => {
            let item = document.getElementById(id);
            _fadeOut(item);
        }, 5000)
    },
    /**
     * Create a prompt which requires user feedback before an action is undertaken
     * @param icon
     * @param title
     * @param content
     * @param ok
     * @param cancel
     */
    prompt: function (iconString, titleString, contentString, okCallback, cancelCallback, okText, cancelText) {
        if (!okText) okText = "OK";
        if (!cancelText) cancelText = "Cancel";

        var id = utils.uniqueId("notification");

        var wrapper = this._buildBase(id, iconString, titleString, contentString);

        // Buttons
        var buttonsWrapper = document.createElement("div");
        buttonsWrapper.setAttribute("class", "footer");

        var okBtn = document.createElement('button');
        okBtn.setAttribute("class", "btn-flat");
        okBtn.appendChild(document.createTextNode(okText));
        okBtn.addEventListener("click", function () {
            var item = document.getElementById(id);
            _fadeOut(item);
            okCallback();
        });

        var cancelBtn = document.createElement("button");
        cancelBtn.setAttribute("class", "btn-flat");
        cancelBtn.appendChild(document.createTextNode(cancelText));
        cancelBtn.addEventListener("click", function () {
            var item = document.getElementById(id);
            _fadeOut(item);
            if (cancelCallback) cancelCallback();
        });

        var btnGroup = document.createElement("div");
        btnGroup.setAttribute("class", "btn-group pull-right");
        btnGroup.appendChild(okBtn);
        btnGroup.appendChild(cancelBtn);
        buttonsWrapper.appendChild(btnGroup);

        // BUG: Need to fix layout with pull-right
        var clearer = document.createElement("div");
        clearer.setAttribute("class", "clearer");
        buttonsWrapper.appendChild(clearer);

        wrapper.appendChild(buttonsWrapper);
        // END Buttons

        var notStream = document.getElementById("notification-stream");

        notStream.appendChild(wrapper);
    },
    /**
     * Show a progress indicator and fire a callback when finished
     * @param icon
     * @param title
     * @param processHandler
     * @param finishedCallback
     */
    progress: function (icon, title, processHandler, finishedCallback) {

    },

    killPrompts: function () {

    }
};

export default Notification;
