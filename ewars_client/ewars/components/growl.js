function _fadeOut(el) {
    el.style.opacity = 1;

    (function fade() {
        if ((el.style.opacity -= .1) < 0) {
            el.parentNode.removeChild(el);
        } else {
            requestAnimationFrame(fade);
        }
    })();
}

const growl = function (context) {
    var _id = _.uniqueId("GROWL_");
    var _wrapper = document.createElement("div");
    _wrapper.setAttribute("class", "growl-wrapper");
    _wrapper.setAttribute("id", _id);

    var _content = document.createElement("div");
    _content.setAttribute("class", "growl-content");

    var _text = document.createTextNode(content);

    _content.appendChild(_text);
    _wrapper.appendChild(_content);

    document.getElementsByTagName('body')[0].appendChild(_wrapper);

    setTimeout(function () {
        var el = document.getElementById(_id);
        _fadeOut(el);
    }, 3000);
};

export default growl;
