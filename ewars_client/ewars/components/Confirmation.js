export default class Confirmation {
    static buttons = [
        ["CONFIRM", "Confirm"],
        ["CANCEL", "Cancel"]
    ];

    constructor(config) {
        this._config = config;
        this._build();
    }

    _build = () => {
        this._id = "PROMPT_" + ewars.utils.uuid();

        this._base = document.createElement("div");
        this._base.setAttribute("class", "prompt-wrapper");
        this._base.setAttribute("id", this._id);

        var _main = document.createElement("div");
        _main.setAttribute("class", "prompt-container");

        var _header = document.createElement("div");
        _header.setAttribute("class", "prompt-header");

        // Set up icon
        var _icon = document.createElement("div");
        _icon.setAttribute("class", "prompt-icon");
        var _iconItem = document.createElement("i");
        _iconItem.setAttribute("class", "fa " + this._config.icon);
        _icon.appendChild(_iconItem);

        // Set up prompt title
        var _title = document.createElement("div");
        _title.setAttribute("class", "prompt-title");

        var _titleText = document.createTextNode(this._config.title);
        _title.appendChild(_titleText);
        _header.appendChild(_icon);
        _header.appendChild(_title);

        _main.appendChild(_header);
        // Header setup complete

        var _body = document.createElement("div");
        _body.setAttribute("class", "prompt-body");
        var _content = document.createTextNode(this._config.body);
        _body.appendChild(_content);

        _main.appendChild(_body);
        // Main body set up

        // Buttons
        var buttonsWrapper = document.createElement("div");
        buttonsWrapper.setAttribute("class", "footer");

        var btnGroup = document.createElement("div");
        btnGroup.setAttribute("class", "btn-group pull-right");
        buttonsWrapper.appendChild(btnGroup);

        let buttons = this.buttons;
        if (this._config.buttons) {
            buttons = this._config.buttons;
        }

        buttons.forEach((button) => {
            let btn = document.createElement("button");
            btn.setAttribute("class", "btn-flat");
            btn.appendChild(document.createTextNode(button[1]));
            btn.addEventListener("click", () => {
                this.onAction(button[0]);
            });
            btnGroup.appendChild(btn);
        });

        // BUG: Need to fix layout with pull-right
        var clearer = document.createElement("div");
        clearer.setAttribute("class", "clearer");
        buttonsWrapper.appendChild(clearer);

        _main.appendChild(buttonsWrapper);
        this._base.appendChild(_main);

        document.getElementsByTagName('body')[0].appendChild(this._base);

    };

    show = () => {

    };

    listen = (listener) => {
        this._listener = listener;
    };

    destroy = () => {
        this._base.parentNode.removeChild(this._base);
    };

    onAction = (action) => {
        this._listener(action);
    }
}