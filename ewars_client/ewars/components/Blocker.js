function Blocker(target, message) {
    this._message = message;
    this._el = null;
    this._id = ewars.utils.uuid();

    let elTarget = target || document.body;
    let outer = document.createElement("div");
    outer.setAttribute("class", "gui-blocker");
    outer.setAttribute("id", this._id);

    let inner = document.createElement("div");
    inner.setAttribute("class", "gui-blocker-msg");

    let iconWrapper = document.createElement("div");
    iconWrapper.setAttribute("class", "gui-blocker-icon");

    let icon = document.createElement("i");
    icon.setAttribute("class", "fal fa-circle-o-notch fa-spin");

    iconWrapper.appendChild(icon);
    inner.appendChild(iconWrapper);

    let msg = document.createElement("div");
    msg.setAttribute("class", "gui-message");
    msg.setAttribute("id", "inner-message");
    msg.appendChild(document.createTextNode(message));

    inner.appendChild(msg);

    outer.appendChild(inner);

    elTarget.appendChild(outer);

    this._el = document.getElementById(this._id);

}
Blocker.prototype.destroy = function () {
    if (this._el.parentNode) {
        this._el.parentNode.removeChild(this._el);
    } else {
        let el = document.getElementById(this._id);
        if (el)
            el.parentNode.removeChild(el);
    }
    delete this;
};

Blocker.prototype.hide = function () {
    this._el.style.display = "none";
};

Blocker.prototype.show = function () {
    this._el.style.display = "block";
};

Blocker.prototype.setMessage = function (message) {
    document.getElementById("inner-message").innerText = message;
};

export default Blocker;
