import Moment from "moment";
import Numeral from "numeral";

function formatPercentage(n, format) {
    var space = '',
        output,
        value = n * 100;

    // check for space before %
    if (format.indexOf(' %') > -1) {
        space = ' ';
        format = format.replace(' %', '');
    } else {
        format = format.replace('%', '');
    }

    output = formatNumber(value, format);

    if (output.indexOf(')') > -1) {
        output = output.split('');
        output.splice(-1, 0, space + '%');
        output = output.join('');
    } else {
        output = output + space + '%';
    }

    return output;
}

function toFixed (value, maxDecimals, optionals) {
    var splitValue = value.toString().split('.'),
        minDecimals = maxDecimals - (optionals || 0),
        boundedPrecision,
        optionalsRegExp,
        power,
        output;

    // Use the smallest precision value possible to avoid errors from floating point representation
    if (splitValue.length === 2) {
        boundedPrecision = Math.min(Math.max(splitValue[1].length, minDecimals), maxDecimals);
    } else {
        boundedPrecision = minDecimals;
    }

    power = Math.pow(10, boundedPrecision);

    //roundingFunction = (roundingFunction !== undefined ? roundingFunction : Math.round);
    // Multiply up by precision, round accurately, then divide and use native toFixed():
    output = (Math.round(value * power) / power).toFixed(boundedPrecision);

    if (optionals > maxDecimals - boundedPrecision) {
        optionalsRegExp = new RegExp('\\.?0{1,' + (optionals - (maxDecimals - boundedPrecision)) + '}$');
        output = output.replace(optionalsRegExp, '');
    }

    return output;
}

function formatNumber(value, format) {
    var negP = false,
        signed = false,
        optDec = false,
        abbr = '',
        abbrK = false, // force abbreviation to thousands
        abbrM = false, // force abbreviation to millions
        abbrB = false, // force abbreviation to billions
        abbrT = false, // force abbreviation to trillions
        abbrForce = false, // force abbreviation
        bytes = '',
        ord = '',
        abs = Math.abs(value),
        w,
        precision,
        thousands,
        d = '',
        neg = false;

    // check if number is zero and a custom zero format has been set
    // see if we should use parentheses for negative number or if we should prefix with a sign
    // if both are present we default to parentheses
    if (format.indexOf('(') > -1) {
        negP = true;
        format = format.slice(1, -1);
    } else if (format.indexOf('+') > -1) {
        signed = true;
        format = format.replace(/\+/g, '');
    }

    // see if abbreviation is wanted
    if (format.indexOf('a') > -1) {
        // check if abbreviation is specified
        abbrK = format.indexOf('aK') >= 0;
        abbrM = format.indexOf('aM') >= 0;
        abbrB = format.indexOf('aB') >= 0;
        abbrT = format.indexOf('aT') >= 0;
        abbrForce = abbrK || abbrM || abbrB || abbrT;

        // check for space before abbreviation
        if (format.indexOf(' a') > -1) {
            abbr = ' ';
        }

        format = format.replace(new RegExp(abbr + 'a[KMBT]?'), '');

        if (abs >= Math.pow(10, 12) && !abbrForce || abbrT) {
            // trillion
            value = value / Math.pow(10, 12);
        } else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9) && !abbrForce || abbrB) {
            // billion
            value = value / Math.pow(10, 9);
        } else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6) && !abbrForce || abbrM) {
            // million
            value = value / Math.pow(10, 6);
        } else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3) && !abbrForce || abbrK) {
            // thousand
            value = value / Math.pow(10, 3);
        }
    }

    if (format.indexOf('[.]') > -1) {
        optDec = true;
        format = format.replace('[.]', '.');
    }

    w = value.toString().split('.')[0];
    precision = format.split('.')[1];
    thousands = format.indexOf(',');

    if (precision) {
        if (precision.indexOf('[') > -1) {
            precision = precision.replace(']', '');
            precision = precision.split('[');
            d = toFixed(value, (precision[0].length + precision[1].length), precision[1].length);
        } else {
            d = toFixed(value, precision.length);
        }

        w = d.split('.')[0];

        if (d.indexOf('.') > -1) {
            d = "." + d.split('.')[1];
        } else {
            d = '';
        }

        if (optDec && Number(d.slice(1)) === 0) {
            d = '';
        }
    } else {
        w = toFixed(value, null);
    }

    // format number
    if (w.indexOf('-') > -1) {
        w = w.slice(1);
        neg = true;
    }

    if (thousands > -1) {
        w = w.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + ",");
    }

    if (format.indexOf('.') === 0) {
        w = '';
    }

    return ((negP && neg) ? '(' : '') + ((!negP && neg) ? '-' : '') + ((!neg && signed) ? '+' : '') + w + d + ((ord) ? ord : '') + ((abbr) ? abbr : '') + ((bytes) ? bytes : '') + ((negP && neg) ? ')' : '');
}

const FORMATTERS = {
    AGE_FORMATTER: function (value) {
        if (!value) return "Unknown";
        if (!value.units) return "Unknown";
        var result = value.value + " " + value.units + "(s)";
        return result;
    },
    DATE_FORMATTER: function (value, format) {
        if (!value) return "Unknown";

        var defFormat = 'YYYY-MM-DD';

        if (format == "WEEK") {
            var startDate = Moment.utc(value).startOf('isoweek').format("DD");
            var endDate = Moment.utc(value).endOf("isoweek").format("DD");
            var stringDate = Moment.utc(value).format("[W]W GGGG | [START - END] MMMM");
            stringDate = stringDate.replace("START", startDate).replace("END", endDate);
            return stringDate;
        }

        if (!format || typeof format === 'object' || typeof format === 'Array') return Moment.utc(value).format(defFormat);
        return Moment.utc(value).format(defFormat);
    },
    I18N_FORMATTER: function (data) {
        if (typeof(data) == "string") {
            if (data.indexOf("{") == 0) {
                data = JSON.parse(data);
            } else {
                return data;
            }
        }

        if (!data) return "Unknown";
        if (data.en) {
            return data.en;
        } else {
            return "";
        }
    },
    NUM_FORMATTER: function (num) {
        return Numeral(num).format("0,0.00");
    },
    NUM: function (value, format) {
        if (value === undefined || value === null || value === "") return "";
        if (format.indexOf("%") > -1) {
            return formatPercentage(value, format);
        } else {
            return formatNumber(value, format);
        }

    }
};

export default FORMATTERS;
