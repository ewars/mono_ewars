const ERROR_CODES = {
    NO_AUTH: "Incorrect Email/Password",
    USER_DELETED: "Incorrect Email/Password",
    EMAIL_PENDING_VERIFICATION: "Account is pending email verification, please check your inbox.",
    USER_EXISTS_NO_ACCESS_ACCOUNTS: "You do not have access to any accounts at this time.",
    USER_EXISTS_NO_ACCESS_ACCOUNT: "You do not have access to this account at this time.",
    NO_ACCOUNT: "You do not have access to any accounts at this time",
    USER_ACCESS_REVOKED: "Your access to the account was revoked",
    USER_PENDING_APPROVAL: "You account access is currently pending approval",
    USER_NO_ACCESS_ACCOUNTS: "You currently do not have access to any accounts"
};

var DEFAULT = 'Global EWARS is designed with the needs of frontline users in mind. We use technology and innovation to make disease control in emergencies easier and more effective.';

const style = {
    error: {
        position: "absolute",
        top: -50
    },
    arrowDown: {

    }
};

var Component = React.createClass({
    _isMobile: false,

    getInitialState: function () {
        return {
            email: "",
            password: "",
            error: null,
            multiple: false
        }
    },

    componentWillMount: function () {
        var check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))check = true
        })(navigator.userAgent || navigator.vendor || window.opera);
        if (check) {
            // This is a mobile browser
            this._isMobile = true;
        }

        document.body.className = "app-public";
    },

    componentDidMount: function () {
        this.refs.emailInput.focus();
    },

    _onIndicatorChange: function (event) {
        var curState = this.state;
        curState[event.target.name] = event.target.value;
        this.setState(curState);
    },

    _handleEnter: function (event) {
        if (event.keyCode == 13) {
            event.stopPropagation();
            var blocker = new ewars.Blocker(null, "Logging in...");
            this._doLogin(this.state.email, this.state.password, blocker);
        }
    },

    _onLogin: function () {
        var blocker = new ewars.Blocker(null, "Logging in...");
        if (!this.state.password || this.state.password == "") {
            blocker.destroy();
            this.setState({
                error: "Please provide a valid email/password"
            });
            return;
        }

        if (!this.state.email || this.state.email == "") {
            blocker.destroy();
            this.setState({
                error: "Please provide a valid email/password"
            });
            return;
        }

        this._doLogin(this.state.email, this.state.password, blocker);
    },

    _doLogin: function (email, password, blocker) {
        let self = this;
        let data = JSON.stringify({
            email: email,
            password: password
        });

        let r = new XMLHttpRequest();
        r.open("POST", "/login", true);
        r.onreadystatechange = () => {
            blocker.destroy();
            if (r.readyState != 4 && r.status != 200) {
                this.setState({
                    error: ERROR_CODES[3]
                })
            } else {
                if (r.readyState == 4 && r.status == 200) {
                    let resp = JSON.parse(r.responseText);

                    if (resp.code) {
                        switch(resp.code) {
                            case "MULTI_ACCOUNT":
                                this.setState({
                                    multiple: true,
                                    accounts: resp.accounts
                                });
                                break;
                            default:
                                this.setState({error: ERROR_CODES[resp.code]});
                        }
                    } else {
                        if (resp.cortex) {
                            document.location = "/cortex";
                        } else {
                            document.location = "/";
                        }
                    }
                }
            }
        };
        r.send(data);


    },

    _clearError: function () {
        this.setState({
            error: null
        })
    },

    _selectAccount: function(aid) {
        let data = JSON.stringify({
            email: this.state.email,
            password: this.state.password,
            account_id: aid
        });

        let bl = new ewars.Blocker(null, "Authenticating...");
        let r = new XMLHttpRequest();
        r.open("POST", "/login");
        r.onreadystatechange = () => {
            bl.destroy();
            if (r.readyState != 4 && r.status != 200) {
                this.setState({
                    error: ERROR_CODES[3]
                })
            } else {
                if (r.readyState == 4 && r.status == 200) {
                    let resp = JSON.parse(r.responseText);

                    if (resp.err) {
                        this.setState({error: ERROR_CODES[resp.code]});
                    } else {
                        document.location = "/";
                    }
                }
            }
        };
        r.send(data);
    },


    render: function () {
        if (this.state.multiple) {
            return (
                <div className="registration-wizard">
                    <h1 className="register">Accounts</h1>
                    <p className="register">Please select the account you would like to access.</p>

                    <div className="widget">
                        <div className="body no-pad">
                            <div className="block-tree" style={{position: "relative"}}>
                                {this.state.accounts.map((acc) => {
                                    return (
                                        <div className="block hoverable" onClick={() => {this._selectAccount(acc[0])}}>
                                            <div className="block-content">
                                                {ewars.I18N(acc[1])} {acc[2]}
                                            </div>
                                        </div>
                                    )
                                })}

                            </div>
                        </div>
                    </div>

                </div>
            )
        }

        if (this._isMobile) {
            return (
                <div className="login-wrapper">
                    <div className="mob">
                        <h3>Mobile Unsupported</h3>

                        <p>Unfortunately, at this time, mobile browsers are not supported by the EWARS platform. A
                            mobile application is under active development and will be released for various platforms
                            (Android, iOS, Windows Phone) in the coming months.</p>
                        <p>In the meantime, please access the EWARS application through either a desktop browser, or the
                            desktop client.</p>
                        <p>If you have any questions or concerns, please contact <a href="mailto:support@ewars.ws">support@ewars.ws</a>
                        </p>
                    </div>
                </div>
            )
        }

        var accName = "Global";
        if (window.account) accName = window.account.name;

        let year = new Date().getFullYear();

        return (
            <div className="login-wrapper">
                <div className="login-container">
                    <div className="login-controls">
                        <div className="login-control">

                            <h3>Existing User? Sign in</h3>

                            {this.state.error ?
                                <div className="error" style={style.error}>
                                    <i className="fal fa-warning"></i>&nbsp;
                                    {this.state.error}
                                </div>
                                : null}

                            <label htmlFor="Email">Email</label>
                            <input type="text" ref="emailInput" name="email" onFocus={this._clearError}
                                   className="form-control" onChange={this._onIndicatorChange} value={this.state.email}/>
                            <br />

                            <label htmlFor="password">Password</label>
                            <input type="password" name="password" onFocus={this._clearError}
                                   className="form-control"
                                   onChange={this._onIndicatorChange}
                                   value={this.state.password} onKeyDown={this._handleEnter}/>
                            <br />

                            <div className="button-wrapper">
                                <div className="button"
                                     label="Sign In"
                                     ref=""
                                     onClick={this._onLogin}>Login
                                </div>
                            </div>
                            <a href="/recovery" className="forgot-pass-link">Forgot Password</a>
                            <a href="/register" className="login-btn">Create an Account</a>
                        </div>

                    </div>
                    <div className="instance-details">
                        <div className="details">

                            <div className="access-details">
                                {window.account.account_flag && window.account.account_flag != "" ?
                                <div className="logo">
                                    <img width="100%" src={window.account.account_flag} alt=""/>
                                </div>
                                    : null}
                                <div className="names">
                                    <div className="acc-name">{accName}</div>
                                    <div className="instance-name">{window.config.APP_NAME || "EWARS"}</div>
                                </div>
                                <div className="clearer"></div>
                            </div>

                            {window.account.name ?
                                <p>{window.account.description}</p>
                            :
                                <p>{DEFAULT}</p>
                            }

                            <div className="clearer"></div>

                            {window.account.account_logo && window.account.account_logo != "" ?
                            <img src={window.account.account_logo} alt=""/>
                                : null}

                        </div>
                        <div className="copyright">All Content Copyright WHO {year}. All Rights Reserved. Powered by <a href="http://ewars-project.org" target="_blank">ewars</a></div>

                    </div>
                </div>
            </div>
        )

    }
});

module.exports = Component;
