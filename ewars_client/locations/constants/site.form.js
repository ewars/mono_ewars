export default {
    name: {
        order: 0,
        type: "language_string",
        label: "Location Name",
        required: true
    },
    site_type_id: {
        order: 1,
        type: "select",
        label: "Location Type",
        required: true,
        optionsSource: {
            resource: "location_type",
            query: {},
            valSource: "id",
            labelSource: "name"
        }
    },
    status: {
        order: 2,
        type: "select",
        label: "Site Status",
        required: true,
        options: [
            ["ACTIVE", "Active"],
            ["DISABLED", "Disabled"],
            ["PENDING", "Pending"]
        ]
    },
    pcode: {
        order: 3,
        type: "text",
        label: "PCODE",
        required: true
    },
    parent_id: {
        order: 4,
        type: "location",
        label: "Parent Location"
    },
    geometry_type: {
        order: 5,
        type: "select",
        label: "Geometry Type",
        required: true,
        options: [
            ["ADMIN", "Administrative"],
            ["POINT", "Lat/Lng Point"]
        ]
    },
    lat: {
        order: 6,
        type: "number",
        label: "Latitude",
        required: true,
        decimal_allowed: true,
        allow_negative: true,
        conditions: {
            application: "all",
            rules: [
                ["geometry_type", "eq", "POINT"]
            ]
        }
    },
    lng: {
        order: 7,
        type: "number",
        label: "Longitude",
        required: true,
        decimal_allowed: true,
        allow_negative: true,
        conditions: {
            application: "all",
            rules: [
                ["geometry_type", "eq", "POINT"]
            ]
        }
    },
    description: {
        order: 8,
        type: "wysiwyg",
        label: "Description",
        required: true
    },
    data: {
        order: 9,
        type: "metadata",
        label: "Additional Metadata",
        required: false
    }
};
