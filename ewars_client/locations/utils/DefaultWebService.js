var Actions = require("../actions/Actions");

export default {
    updateLocation: function (data, handler) {
        ewars.tx("com.ewars.location.update", [data.uuid, data])
            .then(function (resp) {
                handler(resp);
            }.bind(this))
    },
    createLocation: function (data, handler) {
        ewars.tx("com.ewars.location.create", [data])
            .then(function (resp) {
                handler(resp);
            }.bind(this))
    },
    deleteLocation: function (uuid) {
        ewars.tx("com.ewars.location", ["DELETE_TYPE", uuid, null])
            .then(function (resp) {
                ewars.growl("Location deleted successfully");
            }.bind(this))
    },
    updateType: function (id, data, callback) {
        ewars.tx("com.ewars.location", ["UPDATE_TYPE", id, data])
            .then(function (resp) {
                callback(resp);
            }.bind(this))
    },
    createType: function (data, handler) {
        ewars.tx("com.ewars.location", ["CREATE_TYPE", null, data])
            .then(function (resp) {
                handler(resp);
            }.bind(this))
    },
    deleteType: function (id) {
        ewars.tx("com.ewars.location", ["DELETE_TYPE", id, null])
            .then(function (resp) {
                ewars.growl('Location type deleted')
            })
    }
};
