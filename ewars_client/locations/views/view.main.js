import Component from '../components/Component.react';
import DataView from '../components/DataView';
import ViewLocations from './view.locations';
import ViewLocationTypes from './view.types';

const VIEWS = [
    ['fa-map-marker', 'Locations', 'DEFAULT'],
    ['fa-map', 'Map', 'Map'],
    ['fa-download', 'Export', 'EXPORT'],
    ['fa-upload', 'Import', 'IMPORT'],
    ['fa-cube', 'Extensions', 'EXT'],
    ['fa-map-marker', 'Old', 'OLD']
];

class ViewMain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'DEFAULT'
        }
    }

    _setView = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        let view;

        if (this.state.view == 'DEFAULT') view = <ViewLocations/>;
        if (this.state.view == 'EXPORT') view = <DataView/>;
        if (this.state.view == 'OLD') view = <Component/>;
        if (this.state.view == 'TYPES') view = <ViewLocationTypes/>;

        return (
            <ewars.d.Layout>
                {/*<ewars.d.Toolbar>*/}
                    {/*<div className="btn-group btn-tabs">*/}
                        {/*{VIEWS.map(item => {*/}
                            {/*return (*/}
                                {/*<ewars.d.Button*/}
                                    {/*icon={item[0]}*/}
                                    {/*label={item[1]}*/}
                                    {/*highlight={this.state.view == item[2]}*/}
                                    {/*onClick={() => {*/}
                                        {/*this._setView(item[2])*/}
                                    {/*}}/>*/}
                            {/*)*/}
                        {/*})}*/}
                    {/*</div>*/}
                {/*</ewars.d.Toolbar>*/}
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewMain;