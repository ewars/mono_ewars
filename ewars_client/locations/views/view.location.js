import ControlField from '../../common/controls/ui.control_field';

const ACTIONS = [
    ['fa-save', 'SAVE']
]

class ViewLocation extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        let cmp, view;

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label={__(this.props.data.name)}>
                    <ewars.d.ActionGroup
                        right={true}
                        actions={ACTIONS}
                        onAction={this._action}/>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <div className="admin-control-editor">

                            <ControlField label="Name">
                                <input type="text" value={__(this.props.data.name)}/>
                            </ControlField>

                            <ControlField label="UUID">
                                <input type="text" value={this.props.data.uuid}/>
                            </ControlField>

                            <ControlField label="Status">
                                <select value={this.props.data.status}>
                                    <option value="ACTIVE">Active</option>
                                    <option value="INACTIVE">Inactive</option>
                                </select>
                            </ControlField>

                            <ControlField label="PCode">
                                <input type="text" value={this.props.data.pcode}/>
                            </ControlField>

                            <ControlField label="Location type">
                                <select name="location_type" id="">

                                </select>
                            </ControlField>

                            <ControlField label="Location group(s)">

                            </ControlField>

                            <ControlField label="Geometry type">
                                <select value={this.props.data.geometry_type}>
                                    <option value="ADMIN">Geometry</option>
                                    <option value="POINT">Point</option>
                                </select>
                            </ControlField>

                            {this.props.data.geometry_type == 'ADMIN' ?
                                <ControlField label="Default center (Lat/Lng)">

                                </ControlField>
                                : null}

                            {this.props.data.geometry_type == 'ADMIN' ?
                                <ControlField label="Default zoom">
                                    <input type="number"/>
                                </ControlField>
                                : null}

                            {this.props.data.geometry_type == 'ADMIN' ?
                                <ControlField label="GeoJSON">
                                    <textarea name="geojson" value={this.props.data.geojson || ''} id="" cols="30"
                                              rows="10"></textarea>
                                </ControlField>
                                : null}

                            <ControlField label="Reporting periods">

                            </ControlField>

                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewLocation;