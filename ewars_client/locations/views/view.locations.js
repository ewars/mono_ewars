import LocationTreeView from '../components/LocationTreeView.react';
import ViewLocation from './view.location';
import ViewLocationTypes from './view.types';
import ViewGroups from './view.groups';

import Editor  from '../components/editor/Editor.react';

import ViewGroup from './view.group';
import ViewType from './view.type';

const ACTIONS = [
    ['fa-plus', 'ADD']
]

class ViewLocations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            node: null,
            nodeType: null,
            view: 'LOCATIONS'
        }
    }

    _action = (action, data) => {
        switch (action) {
            case 'EDIT':
                var nodeType = 'LOCATION';
                if (this.state.view == 'GROUPS') nodeType = 'GROUP';
                if (this.state.view == 'TYPES') nodeType = 'TYPE';
                this.setState({
                    node: data,
                    nodeType: nodeType
                })
                break;
            case 'ADD':
                var nodeType = 'LOCATION';
                if (this.state.view == 'GROUPS') nodeType = 'GROUP';
                if (this.state.view == 'TYPES') nodeType = 'TYPE';
                this.setState({
                    node: {},
                    nodeType: nodeType
                })
                break;
            default:
                break;
        }
    };

    render() {
        let view;

        if (this.state.node) {
            if (this.state.nodeType == 'GROUP') view = <ViewGroup data={this.state.node}/>;
            if (this.state.nodeType == 'LOCATION') view = <Editor data={this.state.node}/>;
            if (this.state.nodeType == 'TYPE') view = <ViewType data={this.state.node}/>;
        }

        let sView;
        if (this.state.view == 'LOCATIONS') sView = <LocationTreeView onAction={this._action}/>;
        if (this.state.view == 'TYPES') sView = <ViewLocationTypes onAction={this._action}/>;
        if (this.state.view == 'GROUPS') sView = <ViewGroups onAction={this._action}/>;

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Locations">
                    <div className="btn-group pull-right">
                        <ewars.d.ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell width="30%" borderRight={true}>
                        {/*<ewars.d.Toolbar>*/}
                            {/*<ewars.d.Row>*/}
                                {/*<ewars.d.Cell>*/}
                                    {/*<div className="btn-group btn-tabs">*/}
                                        {/*<ewars.d.Button*/}
                                            {/*highlight={this.state.view == 'LOCATIONS'}*/}
                                            {/*icon="fa-map-marker"*/}
                                            {/*onClick={() => {*/}
                                                {/*this.setState({view: 'LOCATIONS'})*/}
                                            {/*}}/>*/}
                                        {/*<ewars.d.Button*/}
                                            {/*highlight={this.state.view == 'TYPES'}*/}
                                            {/*icon="fa-tag"*/}
                                            {/*onClick={() => {*/}
                                                {/*this.setState({view: 'TYPES'});*/}
                                            {/*}}/>*/}
                                        {/*<ewars.d.Button*/}
                                            {/*highlight={this.state.view == 'GROUPS'}*/}
                                            {/*icon="fa-cubes"*/}
                                            {/*onClick={() => {*/}
                                                {/*this.setState({view: 'GROUPS'});*/}
                                            {/*}}/>*/}
                                    {/*</div>*/}
                                {/*</ewars.d.Cell>*/}
                                {/*<ewars.d.Cell style={{display: 'block'}}>*/}
                                    {/*<ewars.d.ActionGroup*/}
                                        {/*actions={ACTIONS}*/}
                                        {/*right={true}*/}
                                        {/*onAction={this._action}/>*/}

                                {/*</ewars.d.Cell>*/}
                            {/*</ewars.d.Row>*/}
                        {/*</ewars.d.Toolbar>*/}
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                {sView}
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

export default ViewLocations;