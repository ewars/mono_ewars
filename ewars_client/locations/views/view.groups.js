class GroupType extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div
                onClick={() => {
                    this.props.onClick(this.props.data);
                }}
                className="block">
                <div className="block-content">{this.props.data}</div>
            </div>
        )
    }
}

class ViewGroups extends React.Component {
    constructor(props) {
        super(props);

        this.state = {data: []};

        ewars.tx('com.ewars.location.groups', [])
            .then(resp => {
                this.setState({
                    data: resp
                })
            })
    }

    _onClick = (group) => {
        this.props.onAction('EDIT', group)
    };

    render() {
        return (
            <ewars.d.Panel>
                <div className="block-tree" style={{position: 'relative'}}>
                    {this.state.data.map(item => {
                        return (
                            <GroupType
                                onClick={this._onClick}
                                data={item}/>
                        )
                    })}
                </div>
            </ewars.d.Panel>
        )
    }
}

export default ViewGroups;