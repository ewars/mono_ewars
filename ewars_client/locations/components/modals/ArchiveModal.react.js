class ArchiveModal extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDataChange = (prop, value) => {
        this.props.onChange(prop, value);
    };

    render() {
        let locationName = ewars.I18N(this.props.item.name);

        return (
            <div>
                <p>Are you sure you want to archive this location, the location, it's children and any associated data (alerts, form submissions, etc...) will be removed from the system and archived.</p>
                <p>Archives can be retrieved by contacting your system administrator. If you will never need to use the location again, please delete the location instead.</p>

            </div>
        )
    }
}

export default ArchiveModal;
