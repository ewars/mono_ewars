import { Layout, Row, Cell } from "../../../common/layout";
import Toolbar from "../../../common/c.toolbar";

import { LocationField } from "../../../common/fields";

const ACTION_TYPES = {
    "DELETE": ["status-red", "fa-times"],
    "MERGE": ["status-amber", "fa-code-branch"],
    "MIGRATE": ["status-green", "fa-arrow-circle-right"]
};

const DEFAULT_ACTION = ["", null];

class MergeTreeNode extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let actionDetails = ACTION_TYPES[this.props.actionType] || DEFAULT_ACTION;

        if (this.props.data.actionType) actionDetails = ACTION_TYPES[this.props.data.actionType] || DEFAULT_ACTION;

        if (this.props.source) {
            if (!actionDetails[1]) actionDetails = ACTION_TYPES["MIGRATE"];
        }

        let icon = null;
        if (actionDetails[1]) icon = <Cell width={20}><i className={"fa " + actionDetails[1]}></i></Cell>;
        let className = "block-content " + actionDetails[0];

        let name = ewars.I18N(this.props.data.name || {});
        if (this.props.sourceTree) {
            name = ewars.I18N(this.props.sourceTree.name) + " => " + ewars.I18N(this.props.data.name);
        }

        return (
            <div className="block">
                <div className={className}>
                    <Layout>

                        <Row>
                            {icon}
                            <Cell>{name}</Cell>
                        </Row>
                    </Layout>
                </div>
                {this.props.data.children ?
                    <div className="block-children">
                        {this.props.data.children.map(item => {
                            return <MergeTreeNode
                                key={item.uuid}
                                source={this.props.source}
                                data={item}/>
                        })}

                    </div>
                    : null}
            </div>
        )
    }
}

class MergeTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentWillMount() {
        if (this.props.data.uuid) {
            ewars.tx("com.ewars.location.merge.preview", [this.props.data.uuid, this.props.source.uuid])
                .then(resp => {
                    this.setState({data: resp})
                })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.uuid) {
            ewars.tx("com.ewars.location.merge.preview", [nextProps.data.uuid, this.props.source.uuid])
                .then(resp => {
                    this.setState({
                        data: resp
                    })
                })
        }
    }

    render() {
        if (!this.state.data) {
            return (<p>Loading...</p>)
        }

        if (this.state.data.error) {
            return (
                <div className="placeholder">
                    <center><i className="fal fa-exclamation-triangle"></i></center>
                    <br />
                    {ewars.I18N(this.state.data.error_code)}
                </div>
            )
        }

        return (
            <div className="block-tree">
                {this.state.data.map(loc => {
                    return <MergeTreeNode
                        key={loc.uuid}
                        sourceTree={this.props.sourceTree}
                        actionType="MERGE"
                        data={loc}/>
                })}
            </div>
        )
    }
}

class SourceTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentWillMount() {
        if (this.props.data.uuid) {
            ewars.tx("com.ewars.location.tree", [this.props.data.uuid])
                .then(resp => {
                    this.setState({
                        data: resp
                    })
                })
        }
    }

    render() {
        if (!this.state.data) return (
            <p>Loading...</p>
        );
        let actionType = "MERGE";
        let childActionType = "";

        if (this.props.data.children) {
            actionType = "MERGE";
        }

        return (

            <div className="block-tree">
                {this.state.data.map(loc => {
                    return <MergeTreeNode
                        key={loc.uuid}
                        source={true}
                        actionType={actionType}
                        data={loc}/>
                })}
            </div>
        )
    }
}

class MergeModal extends React.Component {
    static defaultProps = {
        mergeTarget: null
    };

    constructor(props) {
        super(props)
    }

    _onLocationChange = (prop, value, data) => {
        this.props.onChange("mergeTarget", value, data);
    };

    render() {

        return (
            <Layout padding={10}>
                <Row height={90}>
                    <Cell style={{padding: 10}}>

                        <label htmlFor="">Merge {ewars.I18N(this.props.data.name)} into:</label>
                        <LocationField
                            hideInactive={true}
                            value={this.props.mergeTarget}
                            onUpdate={this._onLocationChange}/>

                    </Cell>
                </Row>
                <Row borderTop={true}>
                    <Cell borderRight={true}>
                        <Layout>
                            <Toolbar label="Location to merge">

                            </Toolbar>
                            <Row>
                                <Cell>
                                    <SourceTree
                                        data={this.props.data}/>
                                </Cell>
                            </Row>
                        </Layout>
                    </Cell>
                    <Cell borderRight={true}>
                        <Layout>
                            <Toolbar label="Result preview">

                            </Toolbar>
                            <Row>
                                <Cell>
                                    <MergeTree
                                        source={this.props.data}
                                        sourceTree={this.props.data}
                                        data={{uuid: this.props.mergeTarget}}/>
                                </Cell>
                            </Row>
                        </Layout>
                    </Cell>
                </Row>
            </Layout>
        )
    }
}

export default MergeModal;
