import mapboxgl from "mapbox-gl";
window.mapboxgl = mapboxgl;
import MapboxDraw from "mapbox-gl-draw/dist/mapbox-gl-draw";

import turf from "turf";

class GeometryEditor extends React.Component {
    _map = null;

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        mapboxgl.accessToken = 'pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg';
        this._map = new mapboxgl.Map({
            container: this.refs.map,
            minZoom: 0,
            width: 980,
            height: '100%',
            style: 'mapbox://styles/jduren/cj95cps6g154x2rmnu6xphlya',
            attributionControl: false,
            doubleClickZoom: false,
            scrollWheelZoom: false
        });

        //this._map.addControl(new mapboxgl.Navigation({position: "top-leddft"}));

        this._map.on("load", function () {
            this._render();
        }.bind(this));
    }

    componentWillReceiveProps(nextProps) {
        this.Draw.remove(this._drawing);
        let data = nextProps.data;

        data.features.forEach(function (item) {
            if (!item.properties) item.properties = {};
        }.bind(this));

        this._drawing = this.Draw.add(data);
    }

    _renderGeo = () => {
    };

    _render = () => {
        let data = this.props.data;

        if (data.features) {
            data.features.forEach(function (item) {
                if (!item.properties) item.properties = {};
            }.bind(this));
        } else {
            data.features = [];
        }

        let extent = turf.bbox(data);

        this.Draw = new MapboxDraw({
            drawing: true,
            displayControlsDefault: false,
            controls: {
                polygon: true,
                trash: true
            }
        });
        this._map.addControl(this.Draw);

        this._drawing = this.Draw.add(data);

        if (data.features.length > 0) {
            this._map.fitBounds(extent, {padding: 20});
        }
    };

    getData = () => {
        let data = this.Draw.getAll();

        let indexToDelete;
        data.features.forEach(function(geom, index) {
            if (geom.geometry.coordinates[0].length == 2 && geom.geometry.type == "Polygon") {
                if (geom.geometry.coordinates[0][0][0][0] == geom.geometry.coordinates[0][0][1][0]) {
                    indexToDelete = index;
                }
            }
        }.bind(this));

        if (indexToDelete) {
            delete data.features.splice(indexToDelete, 1);
        }

        return data;
    };

    render() {
        return <div className="map" ref="map" style={{height: '100%'}}>

        </div>
    }
}

export default GeometryEditor;
