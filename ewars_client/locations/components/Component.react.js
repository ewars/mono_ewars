import Validator from "../../common/utils/Validator";

import LocationTreeView from "./LocationTreeView.react";

import Editor from "./editor/Editor.react";
import TypeEditor from "./editor/TypeEditor.react";
import LocationTypes from "./editor/panels/LocationTypes.react";
import LocationGroups from "./editor/panels/LocationGroups.react";
import DataView from "./DataView";
import MergeModal from "./modals/MergeModal";

import DeleteModal from "./modals/DeleteModal.react";
import TransferModal from "./modals/TransferModal.react";
import ArchiveModal from "./modals/ArchiveModal.react";

import {
    Button,
    VertTab,
    Shade,
    ContextMenu,
    Filler,
    Modal as ModalComponent,
    Form
} from "../../common";

var GCONSTANTS = require("../../common/constants");

var CONSTANTS = {
    INSPECTIONS: "INSPECTIONS",
    LOCATIONS: "LOCATIONS",
    REPORTS: "REPORTS",
    TYPES: "TYPES",
    EDIT_TYPE: "EDIT_TYPE",
    EDIT: "EDIT",
    REPORT: "REPORT",
    GROUPS: "GROUPS"
};

const CONTEXT_ACTIONS = [
    {label: "Edit", icon: "fa-pencil", action: "EDIT"},
    {label: "Add Child", icon: "fa-plus", action: "NEW_CHILD"},
    {label: "Merge Into", icon: "fa-trash", action: "MERGE"},
    {label: "Enable Children", icon: "fa-check", action: "ENABLE_CHILDREN"},
    {label: "Disable Children", icon: "fa-ban", action: "DISABLE_CHILDREN"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"}
];

const MERGE_ACTIONS = [
    {label: "Apply Change(s)", icon: "fa-check", action: "COMMIT", color: "green"},
    {label: "Cancel", icon: "fa-times", action: "CANCEL", color: "red"}
];

const modals = {
    DELETE: {
        Cmp: DeleteModal,
        title: "Delete Location?",
        icon: "fa-trash",
        buttons: [
            {label: "I understand the consequences, delete this location", colour: "red", action: "DELETE"},
            {label: "Cancel", action: "CANCEL"}
        ]
    },
    MERGE: {Cmp: null, title: "Merge Location"},
    TRANSFER_DATA: {
        Cmp: TransferModal,
        tile: "Move Data",
        icon: "fa-exchange",
        buttons: [
            {label: "Commit Transfer", colour: "green", action: "TRANSFER"},
            {label: "Cancel", colour: "red", action: "CANCEL"}
        ]
    },
    ARCHIVE: {
        Cmp: ArchiveModal,
        title: "Archive Location",
        icon: "fa-archive",
        buttons: [
            {label: "Commit to Archive", colour: "green", action: "ARCHIVE"},
            {label: "Cancel", colour: "red", action: "CANCEL"}
        ]
    }
};

class MainView extends React.Component {
    constructor(props) {
        super(props);

        this.state ={
            view: "TREE"
        }
    }

    render() {

        let view;

        if (this.state.view == "DATA") {
            view = <DataView/>;
        } else {
            view = <Component/>;
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell width="34px" style={{position: "relative", overflow: "hidden"}} borderRight={true}>
                        <div className="ide-tabs">
                            <div className={"ide-tab" + (this.state.view == "TREE" ? " ide-tab-down" : "")}
                                onClick={() => this.setState({view: "TREE"})}>
                                <i className="fal fa-tree"></i>&nbsp;Tree</div>
                            <div className={"ide-tab" + (this.state.view == "DATA" ? " ide-tab-down" : "")}
                                onClick={() => this.setState({view: "DATA"})}>
                                <i className="fal fa-table"></i>&nbsp;Table</div>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}

var Component = React.createClass({
    getInitialState: function () {
        return {
            view: "TREE",
            errors: [],
            editing: null,
            footerView: null,
            shoulderView: CONSTANTS.LOCATIONS,
            modal: null,
            modalData: {},
            item: null,
            mergeTarget: null
        }
    },

    _onEdit: function (admin) {
        this.setState({
            editing: admin,
            view: "EDIT"
        })
    },

    _create: function () {
        this.setState({
            editing: {
                name: {en: ""},
                status: "",
                description: "",
                geometry: null,
                parent_id: null,
                lineage: null,
                point: JSON.stringify({type: "Point", coordinates: [0, 0]}),
                pcode: "",
                site_type_id: null,
                geometry_type: "POINT"
            },
            view: "EDIT"
        })
    },

    _editClose: function () {
        this.state.editing = null;
        this.state.view = null;
        this.forceUpdate();
    },

    _showDelete: function (data) {
        this.setState({
            item: data,
            modal: modals["DELETE"],
            modalData: {location_name: ""}
        })
    },

    _deleteLocation: function (data) {
        let blocker = new ewars.Blocker(null, "Deleting location...");
        ewars.tx("com.ewars.location.delete", [this.state.item.uuid])
            .then(resp => {
                ewars.emit("LOCATIONS_CHANGE", [this.state.item.parent_id]);
                blocker.destroy();
                this.setState({
                    view: "default",
                    editing: null,
                    item: null,
                    modal: null,
                    modalData: null
                })
            })
    },

    _onLocationAction: function (action, data) {
        if (action == "EDIT") this._onEdit(data);
        if (action == "DELETE") this._showDelete(data);
        if (action == "VIEW_PROFILE") window.open("/location?uuid=" + data.uuid);

        if (action == "TRANSFER_DATA") {
            this.setState({
                modal: modals["TRANSFER_DATA"],
                modalData: {location_id: null},
                item: data
            })
        }

        if (action == "ARCHIVE") {
            this.setState({
                modal: modals["ARCHIVE"],
                item: data
            })
        }

        if (action == "MERGE") {
            this.setState({
                item: data,
                isMerger: true
            })
        }

        if (action == "NEW_CHILD") {
            this.setState({
                editing: {
                    uuid: null,
                    name: {en: ""},
                    status: "INACTIVE",
                    description: "",
                    geometry: null,
                    parent_id: data.uuid,
                    lineage: null,
                    point: JSON.stringify({type: "Point", coordinates: [0, 0]}),
                    pcode: "",
                    site_type_id: null,
                    geometry_type: "POINT"
                },
                view: "EDIT"
            })
        }

        if (action == "DISABLE_CHILDREN") {
            let blocker = new ewars.Blocker(null, "Disabling child locations...");
            ewars.tx("com.ewars.location.children.disable", [data.uuid])
                .then(function (resp) {
                    blocker.destroy();
                    ewars.growl("Children disabled");
                    ewars.emit("LOCATIONS_CHANGE", [data.uuid]);
                }.bind(this));
        }

        if (action == "ENABLE_CHILDREN") {
            let blocker = new ewars.Blocker(null, "Enabling child locations...");
            ewars.tx("com.ewars.location.children.enable", [data.uuid])
                .then(function (resp) {
                    blocker.destroy();
                    ewars.growl("Children enabled");
                    ewars.emit("LOCATIONS_CHANGE", [data.uuid]);
                }.bind(this))
        }
    },


    _onModalAction: function (action) {
        if (action == "CANCEL") {
            this.setState({
                modalData: null,
                modal: null,
                item: null
            })
        }
        if (action == "DELETE") {
            if (this.state.modalData.location_name == ewars.I18N(this.state.item.name)) {
                this._deleteLocation(this.state.item);
                return;
            }
            ewars.error("Incorrect Name", "You entered the name incorrectly, please try again");
        }
    },

    _onModalDataChange: function (prop, value) {
        this.setState({
            ...this.state,
            modalData: {
                ...this.state.modalData,
                [prop]: value
            }
        })
    },

    _onMergeAction: function (action) {
        if (action == "CANCEL") this.setState({item: null, isMerger: false, mergeTarget: null});
        if (action == "CLOSE") this.setState({item: null, isMerger: false, mergeTarget: null});

        if (action == "COMMIT") {
            let blocker = new ewars.Blocker(null, "Performing merge...");
            ewars.tx("com.ewars.location.merge", [this.state.mergeTarget, this.state.item.uuid])
                .then(resp => {
                    if (resp.error) {
                        blocker.destroy();
                        ewars.growl(resp.error_code);
                    } else {
                        ewars.emit("LOCATIONS_CHANGE", [this.state.item.parent_id, this.state.mergeTarget]);
                        this.setState({
                            mergeTarget: null,
                            item: null,
                            isMerger: false
                        });
                        blocker.destroy();
                    }
                })
        }
    },

    _onMergeSettingsChange: function (prop, value, data) {
        this.setState({
            [prop]: value,
            mergeTargetData: data || null
        })
    },

    render: function () {
        var subView = <Filler icon="fa-globe"/>, formDefinition;

        if (this.state.view == CONSTANTS.EDIT) subView =
            <Editor onClose={this._editClose} data={this.state.editing} onDelete={this._deleteLocation}/>;
        if (this.state.view == CONSTANTS.EDIT_TYPE) subView =
            <TypeEditor onClose={this._editClose} data={this.state.editing}/>;


        // let ctxMenu = <ContextMenu actions={CONTEXT_ACTIONS} onClick={this._onContext}/>;
        let shoulderView = <LocationTreeView
            actions={CONTEXT_ACTIONS}
            onAction={this._onLocationAction}/>;

        var create_menu = [
            {icon: "fa-plus", label: "New Location", handler: this._create},
        ];

        var canEdit = false;
        if ([GCONSTANTS.SUPER_ADMIN, GCONSTANTS.GLOBAL_ADMIN, GCONSTANTS.ACCOUNT_ADMIN].indexOf(window.user.user_type) >= 0) canEdit = !canEdit;

        let modalTitle, modalIcon, ModalCmp, modalButtons;
        if (this.state.modal) {
            modalTitle = this.state.modal.title;
            ModalCmp = this.state.modal.Cmp;
            modalIcon = this.state.modal.icon;
            modalButtons = this.state.modal.buttons;
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Toolbar label="Locations" icon="fa-map">
                            <div className="btn-group pull-right" style={{marginRight: "8px"}}>
                                <ewars.d.Button
                                    icon="fa-plus"
                                    label="Create new"
                                    onClick={this._create}/>
                            </div>
                        </ewars.d.Toolbar>

                        <ewars.d.Row>
                            <ewars.d.Cell width="300px" borderRight={true}>
                                {shoulderView}
                            </ewars.d.Cell>
                            <ewars.d.Cell>
                                {subView}
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Cell>
                </ewars.d.Row>

                <ModalComponent
                    visible={this.state.modal != null}
                    title={modalTitle}
                    buttons={modalButtons}
                    onAction={this._onModalAction}
                    icon={modalIcon}>
                    {this.state.modal ?
                        <ModalCmp
                            onChange={this._onModalDataChange}
                            item={this.state.item}
                            data={this.state.modalData}/>
                        : null}
                </ModalComponent>

                <Shade
                    title="Merge Location"
                    shown={this.state.isMerger}
                    actions={MERGE_ACTIONS}
                    onAction={this._onMergeAction}
                    icon="fa-code-branch">
                    <MergeModal
                        onChange={this._onMergeSettingsChange}
                        mergeTarget={this.state.mergeTarget}
                        data={this.state.item}/>
                </Shade>
            </ewars.d.Layout>
        )
    }
});

export default MainView;
