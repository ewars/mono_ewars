import CONSTANTS from "../../common/constants";

import ContextMenu from "../../common/c.context_menu";

const SELECTS = ["uuid", "name", "location_type", "site_type_id", "geometry_type", "status", "@children", "parent_id", "geometry", "point"];
const SEARCH_SELECTS = ["@lineage", "uuid", "name", "location_type", "site_type_id", "geometry_type", "status", "@children", "parent_id", "geometry", "point"];
const JOINS = ["location_type:site_type_id:id"];


class TreeNodeComponent extends React.Component {
    isLoaded = false;

    constructor(props) {
        super(props);

        this.state = ewars.g["LOC_" + this.props.data.uuid] || {
                children: null,
                showChildren: false,
                isChecked: false
            }
    }

    componentWillMount() {
        if (this.props.checked) this.state.isChecked = this.props.checked.indexOf(this.props.data.uuid) >= 0;
        ewars.subscribe("LOCATIONS_CHANGE", this._changeReload);
        window.__hack__.addEventListener("click", this._onBodyClick);
    }

    componentWillUnmount() {
        ewars.g["LOC_" + this.props.data.uuid] = ewars.copy(this.state);
        window.__hack__.removeEventListener("click", this._onBodyClick);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.checked) this.state.isChecked = nextProps.checked.indexOf(this.props.data.uuid) >= 0;
    }

    _onBodyClick = (evt) => {
        if (!this.refs.item.contains(evt.target)) {
            this.setState({
                showContext: false
            })
        }
    };

    _load = (location_id, show) => {
        this.refs.iconHandle.setAttribute("class", "fa fa-spin fa-gear");
        var query = {
            parent_id: {eq: this.props.data.uuid},
            status: this.props.hideInactive ? {eq: "ACTIVE"} : {neq: "DELETED"}
        };

        ewars.tx("com.ewars.query", ["location", SELECTS, query, {"name.en": "ASC"}, null, null, JOINS])
            .then(function (resp) {
                this._hasLoaded = true;
                this._isLoaded = true;
                this.setState({
                    children: resp,
                    showChildren: show ? show : this.state.showChildren
                })
            }.bind(this))
    };

    _changeReload = (change_locs) => {
        if (change_locs.indexOf(this.props.data.uuid) >= 0) {
            var query = {
                parent_id: {eq: this.props.data.uuid},
                status: this.props.hideInactive ? {eq: "ACTIVE"} : {neq: "DELETED"}
            };

            ewars.tx("com.ewars.query", ["location", SELECTS, query, {"name.en": "ASC"}, null, null, JOINS])
                .then(function (resp) {
                    this.setState({
                        children: resp
                    })
                }.bind(this))
        }
    };


    _reload = (location_id, show) => {
        if (!this._isLoaded && this.props.data.children > 0) {
            this._load(location_id, show);
        }
    };

    _onCaretClick = () => {
        // Children are already loaded
        if (this.state.children) {
            this.setState({
                showChildren: !this.state.showChildren
            });
            return;
        }

        // No Children loaded, load them
        this._reload(this.props.data.uuid, true);
    };

    _onLabelClick = () => {
        if (this.props.restrict_selection &&
            this.props.data.site_type_id == this.props.restrict_selection)
            return;

        this.props.onAction("EDIT", this.props.data);
    };

    _hasChildren = () => {
        return true;
    };

    _onCheck = () => {
        this.props.onAction("CHECK", this.props.data);
    };

    _onContext = (e) => {
        e.preventDefault();
        this.setState({
            showContext: true
        })
    };

    _onContextAction = (action) => {
        this.setState({
            showContext: false
        });

        this.props.onAction(action, this.props.data);
    };

    render() {
        let childs;
        if (this.state.children) {
            childs = this.state.children.map(function (child, i) {
                return <TreeNodeComponent
                    actions={this.props.actions}
                    key={child.uuid}
                    hideInactive={this.props.hideInactive}
                    checked={this.props.checked}
                    allowCheck={this.props.allowCheck}
                    onAction={this.props.onAction}
                    index={i}
                    selectTypeId={this.props.selectTypeId}
                    data={child}/>
            }.bind(this))
        }

        let icon = "fa-folder";
        if (this.state.showChildren) icon = "fa-folder-open";
        if (this.props.data.children <= 0) icon = "fa-map-marker";
        let iconClass = `fal ${icon}`;

        let name = `${ewars.I18N(this.props.data.name)} (${ewars.I18N(this.props.data.location_type.name)})`;

        let checkClass = "fal fa-square";
        if (this.state.isChecked) checkClass = "fal fa-check-square";

        let blockClass = "block-content";
        if (this.props.data.status == CONSTANTS.ACTIVE) blockClass += " status-green";
        if (this.props.data.status == CONSTANTS.DISABLED) blockClass += " status-red";

        let lineage;
        if (this.props.data['@lineage']) {
            lineage = (this.props.data['@lineage'] || []).join(" \\ ");
        }

        return (
            <div className="block">
                <div className={blockClass} style={{padding: 0}} ref="item" onContextMenu={this._onContext}>
                    <div className="ide-row">
                        <div className="ide-col" onClick={this._onCaretClick}
                             style={{maxWidth: 20, padding: 8}}>
                            <div className="node-control"><i ref="iconHandle" className={iconClass}></i></div>
                        </div>
                        {this.props.allowCheck ?
                            <div className="ide-col" onClick={this._onCheck} style={{maxWidth: 20, padding: 8}}>
                                <div className="node-check"><i className={checkClass}></i></div>
                            </div>
                            : null}
                        <div className="ide-col tree-node-item" onClick={this._onLabelClick} style={{padding: 8}}>
                            {name}
                            {lineage ?
                                <i>{lineage}</i>
                            : null}
                        </div>
                    </div>
                    {this.props.actions && this.state.showContext ?
                        <ContextMenu
                            absolute={true}
                            onClick={this._onContextAction}
                            actions={this.props.actions}/>
                        : null}
                </div>
                {this.state.showChildren ?
                    <div className="block-children">
                        {childs}
                    </div>
                    : null}
            </div>
        )
    }
}


function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

class SearchInput extends React.Component {
    static defaultProps = {
        value: ''
    };

    constructor(props) {
        super(props);

        this.sendForSearch = debounce(this.sendForSearch, 300);
    }

    sendForUpdate = (e) => {
        this.props.onChange(e.target.value);

        if (e.target.value.length > 2) {
            this.sendForSearch(e.target.value);
        }

        if (e.target.value == "") this.sendForSearch(null);
    };

    sendForSearch = (searchTerm) => {
        this.props.onSearch(searchTerm);
    };

    render() {
        return (
            <input
                type="text"
                onChange={this.sendForUpdate}
                value={this.props.value}/>
        )
    }
}


var LocationTreeView = React.createClass({
    getInitialProps: function () {
        return {
            allowCheck: false,
            parentId: null,
            hideInactive: false,
            onAction: null,
            actions: null,
            selectTypeId: null
        }
    },

    getInitialState: function () {
        return {
            steps: [],
            rootElements: [],
            checked: [],
            nodesChecked: [],
            search: ""
        };
    },

    componentWillMount: function () {
        this._init();

        ewars.subscribe("LOCATIONS_CHANGE", this._init);
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.allowCheck) {
            this.state.checked = this.props.checked;
            // coalesce changes in selections into nodesChecked
            this.state.checkedNodes = [];
            this.state.checkedNodes.forEach(function (item) {
                var index = this.state.checked.indexOf(item.uuid);
                if (index >= 0) this.state.checkedNodes.push(item);
            }.bind(this));
        }
    },

    _init: function () {
        var query = {};

        if (!this.props.parentId) query.parent_id = {eq: "NULL"};
        if (this.props.parentId) query.uuid = {eq: this.props.parentId};


        if (this.props.hideInactive) {
            query.status = {eq: "ACTIVE"}
        } else {
            query.status = {neq: "DELETED"};
        }

        ewars.tx("com.ewars.query", ["location", SELECTS, query, {"name.en": "ASC"}, null, null, JOINS])
            .then(function (resp) {
                this.state.rootElements = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))
    },

    _handleSearchChange: function (e) {
        var val = e.target.value;

        this.setState({
            search: val,
            rootElements: []
        });

        var args = [val, null, null];

        if (this.props.hideInactive) args[2] = {eq: "ACTIVE"};


        if (val.length >= 3) {
            var query = {
                "name.en": {like: val}
            };

            if (this.props.hideInactive) {
                query.status = {eq: "ACTIVE"}
            } else {
                query.status = {neq: "DELETED"};
            }

            ewars.tx("com.ewars.locations.search", [val])
                .then(function (resp) {
                    this.setState({
                        rootElements: resp
                    })
                }.bind(this))
        } else {
            this._init();
        }
    },

    _onCheck: function (node) {
        var isChecked = true;
        var checked = this.props.checked || [];

        if (checked.indexOf(node.uuid) < 0) {
            checked.push(node.uuid)
        } else {
            checked = _.without(checked, node.uuid);
            isChecked = false;
        }

        // Store location data
        var checkedNodes = this.state.checkedNodes || [];

        if (isChecked) {
            checkedNodes.push(node);
            var existing = [];
            checkedNodes = _.reject(checkedNodes, function (node) {
                if (existing.indexOf(node.uuid) < 0) {
                    existing.push(node.uuid);
                    return false;
                } else {
                    return true;
                }
            })
        } else {
            checkedNodes = _.reject(checkedNodes, function (item) {
                return item.uuid == node.uuid;
            }, this)
        }

        this.setState({
            checked: checked,
            checkedNodes: checkedNodes
        });

        this.props.onSelectChange(checked, checkedNodes);
    },

    _clearSearch: function () {
        this.setState({
            search: ""
        });
        this._init();
    },

    _onSearchChange: function(val) {
        this.setState({
            search: val
        })
    },

    _search: function(val) {
        if (val == null || val == '') {
            this._init();
        } else {

            var args = [val, null, null];

            if (this.props.hideInactive) args[2] = {eq: "ACTIVE"};


            if (val.length >= 2) {
                var query = {
                    "name.en": {like: val}
                };

                if (this.props.hideInactive) {
                    query.status = {eq: "ACTIVE"}
                } else {
                    query.status = {neq: "DELETED"};
                }

                ewars.tx("com.ewars.locations.search", [val])
                    .then((resp) => {
                        this.setState({
                            rootElements: resp
                        })
                    })
            } else {
                this._init();
            }
        }
    },

    render: function () {
        let rootNodes = this.state.rootElements.map(function (element) {
            let id = element.uuid;
            if (this.state.search != "") id = "SRCH_" + id;
            return <TreeNodeComponent
                key={id}
                onAction={this.props.onAction}
                hideInactive={this.props.hideInactive}
                checked={this.props.checked}
                allowCheck={this.props.allowCheck}
                data={element}
                actions={this.props.actions}
                selectTypeId={this.props.selectTypeId}/>
        }.bind(this));

        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 45}}>
                    <div className="ide-col">
                        <div className="search">
                            <div className="search-inner">
                                <div className="ide-row">
                                    <div className="ide-col search-left" style={{maxWidth: 25}}>
                                        <i className="fal fa-search"></i>
                                    </div>
                                    <div className="ide-col search-mid">
                                        <SearchInput
                                            value={this.state.search}
                                            onChange={this._onSearchChange}
                                            onSearch={this._search}/>
                                    </div>
                                    {this.state.search != "" ?
                                        <div className="ide-col search-right" style={{maxWidth: 25}}
                                             onClick={this._clearSearch}>
                                            <i className="fal fa-times"></i>
                                        </div>
                                        : null}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ide-row">
                    <div className="ide-col">
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                {rootNodes}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default LocationTreeView;
