import GeneralSettings from "./panels/GeneralSettings.react";
import Guidance from "./panels/Guidance.react";
import Mapping from "./panels/Mapping.react";
import Reporting from "./panels/Reporting.react";
import { Spinner, VertTab } from "../../../common";

var SettingsSection = React.createClass({
    _onClick: function () {
        this.props.onClick(this.props.view);
    },

    render: function () {
        var className = "ide-settings-section";
        if (this.props.active) className += " ide-settings-section-active";
        var iconClass = "fa fa-caret-right";
        if (this.props.active) iconClass = "fa fa-caret-down";

        var subs = [];
        if (this.props.subs) {
            subs = _.map(this.props.subs, function (sub) {
                return (
                    <div className="section-item">{sub}</div>
                )
            }, this);
        }

        var hasSubs = subs.length > 0;

        return (
            <div className={className}>
                <div className="section-header" onClick={this._onClick}>
                    <div className="section-title">{this.props.label}</div>
                </div>
                {hasSubs ?
                    <div className="section-items">
                        {subs}
                    </div>
                    : null}
            </div>
        )
    }
});

const CONSTANTS = {
    GENERAL: "GENERAL",
    MAPPING: "MAPPING",
    GUIDANCE: "GUIDANCE",
    REPORTING: "REPORTING"
};

var Editor = React.createClass({
    _isLoading: true,

    getInitialState: function () {
        return {
            data: {},
            view: CONSTANTS.GENERAL,
            errors: {}
        }
    },

    componentWillMount: function () {
        this.state.data = this.props.data;
        if (this.props.data.uuid) {
            ewars.tx("com.ewars.resource", ["location", this.props.data.uuid, ["uuid", "parent_id", "name", "status", "pcode", "site_type_id", "geojson", "point", "description", "lineage", "geometry_type", "default_center", "default_zoom", "groups"], null])
                .then(function (resp) {
                    this.state.data = resp;
                    this._isLoading = false;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        } else {
            this.state.data = JSON.parse(JSON.stringify(this.props.data));
            this._isLoading = false;
        }
    },

    componentDidMount: function () {
        document.addEventListener("keydown", function (e) {
            if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                e.preventDefault();
                // Process event...
                this._save();
            }
        }.bind(this), false);
    },

    _save: function () {
        if (this.state.data.uuid) {
            let blocker = new ewars.Blocker(null, "Updating location...");
            ewars.tx("com.ewars.location.update", [this.state.data.uuid, this.state.data])
                .then(function (resp) {
                    if (resp.err) {
                        this.setState({
                            errors: resp.errs
                        });
                        blocker.destroy();
                        ewars.growl("Error creating location, please see form for more information.")
                    } else {
                        // If the location has been moved we need to update multiple lopcation
                        if (this.state.data.parent_id != resp.parent_id) {
                            ewars.emit("LOCATIONS_CHANGE", [this.state.data.parent_id, resp.parent_id, this.state.data.uuid]);
                        } else {
                            ewars.emit('LOCATIONS_CHANGE', [this.state.data.parent_id, this.state.data.uuid])
                        }
                        this.setState({
                            data: resp
                        });
                        blocker.destroy();
                        ewars.growl("Location updated");
                    }
                }.bind(this));
        } else {
            let blocker = new ewars.Blocker(null, "Creating location...");
            ewars.tx("com.ewars.location.create", [this.state.data])
                .then(function (resp) {
                    if (resp.err) {
                        this.setState({
                            errors: resp.errs
                        });
                        blocker.destroy();
                        ewars.growl("Error creating location, please see form for more information.")
                    } else {
                        this.setState({
                            data: resp
                        });
                        ewars.emit("LOCATIONS_CHANGE", [this.state.data.parent_id]);
                        blocker.destroy();
                        ewars.growl("Location Updated");
                    }
                }.bind(this))
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.data = nextProps.data;
        this.state.errors = {};
        this.state.view = CONSTANTS.GENERAL;
        this._isLoading = true;

        if (nextProps.data.uuid) {
            ewars.tx("com.ewars.resource", ["location", nextProps.data.uuid, ["uuid", "parent_id", "geojson", "name", "status", "pcode", "site_type_id", "geometry", "point", "description", "lineage", "geometry_type", "default_center", "default_zoom", "groups"], null])
                .then(function (resp) {
                    this._isLoading = false;
                    this.setState({
                        data: resp
                    })
                }.bind(this))
        } else {
            this._isLoading = false;
            this.forceUpdate();
        }
    },

    _changeSection: function (view) {
        this.state.view = view;
        this.forceUpdate();
    },

    _onChange: function (prop, value) {
        this.setState({
            errors: {},
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    },

    _onDelete: function () {
        ewars.prompt("fa-trash", "Delete Location", "Are you sure you want to delete this location? All child locations and any data associated with this location will be lost including assignments, alerts, etc...", function () {
            this.props.onDelete(this.state.data);
        }.bind(this));
    },

    render: function () {

        let view;

        if (this.state.view == CONSTANTS.GENERAL) {
            view = <GeneralSettings
                errors={this.state.errors}
                onChange={this._onChange}
                data={this.state.data}/>;
        }
        if (this.state.view == CONSTANTS.GUIDANCE) {
            view = <Guidance
                errors={this.state.errors}
                onChange={this._onChange}
                data={this.state.data}/>;
        }
        if (this.state.view == CONSTANTS.MAPPING) {
            view = <Mapping
                errors={this.state.errors}
                onChange={this._onChange}
                data={this.state.data}/>;
        }
        if (this.state.view == CONSTANTS.REPORTING) {
            view = <Reporting
                onChange={this._onReportinChange}
                data={this.state.data}/>;
        }

        if (this._isLoading) {
            return (
                <div className="ide-settings-panel">
                    <ewars.d.Layout>
                        <ewars.d.Row>
                            <ewars.d.Cell>
                                <Spinner/>
                            </ewars.d.Cell>
                        </ewars.d.Row>
                    </ewars.d.Layout>
                </div>
            )
        }

        let saveText = _l("SAVE_CHANGES");

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Location Editor">
                    <div className="btn-group pull-right" style={{marginRight: "8px"}}>
                        {this.state.view != "REPORTING" ?
                        <ewars.d.Button
                            icon="fa-save"
                            label={saveText}
                            ref="saveBtn"
                            onClick={this._save}/>
                        : null}
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="34px" style={{position: "relative", overflow: "hidden"}}>
                        <div className="ide-tabs" style={{zIndex: "99"}}>
                            <VertTab label="General Settings" icon="fa-cog" active={this.state.view == "GENERAL"} onClick={() => this._changeSection("GENERAL")}/>
                            <VertTab label="Mapping" icon="fa-map" active={this.state.view == "MAPPING"} onClick={() => this._changeSection("MAPPING")}/>
                            <VertTab label="Reporting" icon="fa-clipboard" active={this.state.view == "REPORTING"} onClick={() => this._changeSection("REPORTING")}/>
                        </div>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
});

export default Editor;
