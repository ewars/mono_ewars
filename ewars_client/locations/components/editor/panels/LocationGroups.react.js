import Button from "../../../../common/c.button";

var LocationGroups = React.createClass({
    getInitialState: function () {
        return {}
    },

    render: function () {
        return (
            <div className="ide-layout">
                <div className="ide-row" style={{maxHeight: 35}}>
                    <div className="ide-col">
                        <div className="ide-tbar">
                            <div className="btn-group">
                                <Button
                                    label="New"
                                    icon="fa-plus"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default LocationGroups;
