import CONSTANTS from "../../../../common/constants";

import {
    SelectField,
    LanguageStringField as LanguageStringInput,
    LatLngField as LatLng,
    TextField,
    TextAreaField
} from "../../../../common/fields";
import Button from "../../../../common/c.button";
import ModalComponent from "../../../../common/c.modal";

import GeometryEditor from "../../GeometryEditor";

const BUTTONS = [
    {label: "Save", icon: "fa-save", action: "SAVE"},
    {label: "Cancel", icon: "fa-times", action: "CANCEL"}
];


var Mapping = React.createClass({
    _map: null,

    getInitialState: function () {
        return {
            showPreview: false
        }
    },

    _onUpdate: function (prop, value) {
        if (["center_lat", "center_lng"].indexOf(prop) >= 0) {
            var gjson = this.props.data.default_center ? JSON.parse(this.props.data.default_center) : {
                type: "Point",
                coordinates: []
            };
            if (prop == "center_lat") gjson.coordinates[1] = value;
            if (prop == "center_lng") gjson.coordinates[0] = value;
            this.props.onChange("default_center", JSON.stringify(gjson));
        } else if (prop == 'point') {
            console.log(prop, value);
            this.props.onChange('geojson', JSON.parse(value));
        } else {
            console.log(prop, value);
            this.props.onChange(prop, value);
        }
    },

    _onGeoChange: function (e) {
        this.props.onChange(e.target.name, e.target.value);
    },

    togglePreview: function () {
        this.setState({
            showPreview: !this.state.showPreview
        })
    },

    _onModalAction: function (action) {
        if (action == 'CLOSE') this.setState({showPreview: false});
        if (action == "CANCEL") this.setState({showPreview: false});

        if (action == "SAVE") {
            let data = this.refs.editor.getData();
            this.setState({showPreview: false});
            this.props.onChange("geojson", JSON.stringify(data))
        }
    },

    render: function () {
        var readOnly = false;

        var center_lng, center_lat;
        // if (this.props.data.default_center) {
        //     var default_center = JSON.parse(this.props.data.default_center);
        //     center_lng = default_center.coordinates[0];
        //     center_lat = default_center.coordinates[1];
        // }

        let previewButtons = [
            {label: "Close", onClick: this.togglePreview}
        ];

        let geom, point;

        if (this.props.data.geojson == undefined || this.props.data.geojson == null) {
            geom = '{"type": "FeatureCollection", "features": []}';
        } else {
            if (typeof this.props.data.geojson == 'string') {
                if (this.props.data.geojson.indexOf('{') >= 0) {
                    geom = JSON.parse(this.props.data.geojson);
                }
            } else {
                geom = this.props.data.geojson || {};
            }
        }

        return (
            <div className="ide-settings-content">
                <div className="ide-settings-header">
                    <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                    <div className="ide-settings-title">Mapping</div>
                </div>
                <div className="ide-settings-intro">
                    <p>Configure settings for how this location should display in maps througjhout EWARS.</p>
                </div>
                <div className="ide-settings-form">

                    <div className="ide-section-basic">
                        <div className="header">General</div>
                        <div className="hsplit-box">
                            <div className="ide-setting-label">Mapping Display Type*</div>
                            <div className="ide-setting-control">
                                <SelectField
                                    name="geometry_type"
                                    readOnly={readOnly}
                                    label="Default Geometry"
                                    onUpdate={this._onUpdate}
                                    value={this.props.data.geometry_type}
                                    config={{options: [["ADMIN", "Geometry"], ["POINT", "Point"]]}}/>
                                {this.props.errors.geometry_type ?
                                    <div className="ide-setting-error"><i
                                        className="fal fa-exclamation-triangle"></i>&nbsp;Please enter a valid value
                                    </div>
                                    : null}
                            </div>
                        </div>
                    </div>

                    {this.props.data.geometry_type == "POINT" ?
                        <div className="ide-section-basic">
                            <div className="header">Lat/Lng</div>
                            <div className="hsplit-box">
                                <div className="ide-setting-label">Latitude/Longitude</div>
                                <div className="ide-setting-control">
                                    <LatLng
                                        parent_id={this.props.data.parent_id}
                                        readOnly={readOnly}
                                        onUpdate={this._onUpdate}
                                        name="point"
                                        value={this.props.data.geojson}/>
                                </div>
                            </div>
                        </div>
                        : null}

                    {this.props.data.geometry_type == "ADMIN" ?
                        <div className="ide-section-basic">
                            <div className="header">Geometry</div>
                            <div className="hsplit-box">
                                <div className="ide-setting-label">Administrative Geometry</div>
                                <div className="ide-setting-control">
                                    <ewars.d.Button
                                        onClick={this.togglePreview}
                                        icon="fa-pencil"
                                        label="Edit Geometry"/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Default Center (Lng/Lat)</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="center_lng"
                                        value={center_lng}
                                        onUpdate={this._onUpdate}/>
                                    <TextField
                                        name="center_lat"
                                        value={center_lat}
                                        onUpdate={this._onUpdate}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Default Zoom</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="default_zoom"
                                        value={this.props.data.default_zoom}
                                        onUpdate={this._onUpdate}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">GeoJSON</div>
                                <div className="ide-setting-control">
                                    <textarea name="geojson" value={JSON.stringify(geom || {})} onChange={this._onGeoChange}></textarea>

                                </div>
                            </div>

                        </div>
                        : null}

                </div>
                {this.state.showPreview ?
                    <ewars.d.Shade
                        title="Geometry Editor"
                        icon="fa-map-marker"
                        shown={this.state.showPreview}
                        actions={BUTTONS}
                        onAction={this._onModalAction}>
                        {this.state.showPreview ?
                            <GeometryEditor
                                parent_id={this.props.data.parent_id || null}
                                ref="editor"
                                data={geom}/>
                            : null}
                    </ewars.d.Shade>
                    : null}
            </div>
        )
    }
});


export default Mapping;
