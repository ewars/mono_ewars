import CONSTANTS from "../../../../common/constants";

import {
    TextField,
    SelectField,
    LocationField,
    LanguageStringField as LanguageStringInput,
    DisplayField
} from "../../../../common/fields";

import {
    ErrorStrip,
    SettingsForm,
    SettingsSection,
    SettingsField
} from "../../../../common";

var fieldConfigs = {
    site_type_id: {
        optionsSource: {
            resource: "location_type",
            query: {},
            valSource: "id",
            labelSource: "name"
        }
    },
    status: {
        options: [
            ["ACTIVE", "Active"],
            ["DISABLED", "Disabled"]
        ]
    }
};

const SEARCH_LIST = [
    "WHO",
    "EWARS",
    "Test",
    "WHODUNNIT",
    "Cabbage",
    "TESTED"
];

const BACKSPACE = 8;
const COMMAN = 188;
const TAB = 9;
const ENTER = 13;

var noop = function () {
};

var retTrue = function () {
    return true
};


class Tag extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="tag-item">
                <div className="ide-row">
                    <div className="ide-col tag-delete" style={{maxWidth: 22}}
                         onClick={() => this.props.onRemove(this.props.data)}>
                        <i className="fal fa-times"></i>
                    </div>
                    <div className="ide-col tag-label">{this.props.data}</div>
                </div>
            </div>
        )
    }
}

class TagResult extends React.Component {
    constructor(props) {
        super(props);
    }

    _add = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        let style = {};
        if (this.props.active) style.background = "#CCC";
        return (
            <div className="block" style={style} onClick={this._add}>
                <div className="block-content">{this.props.data}</div>
            </div>
        )
    }
}


class LocationGroupTags extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            input: "",
            tags: ewars.copy(this.props.value || []),
            availableTags: [],
            results: [],
            highlightedIndex: -1
        };
    }

    componentWillMount() {
        this._getTags();
    }

    _getTags = () => {
        ewars.tx("com.ewars.location.groups")
            .then((resp) => {
                this.setState({
                    availableTags: resp
                })
            })
    };

    onChange = (e) => {
        this.setState({
            input: e.target.value,
            highlightedIndex: -1
        })
    };

    _onAdd = (tag) => {
        if (this.state.tags.indexOf(tag) < 0) {
            this.state.tags.push.apply(this.state.tags, [tag]);
            this.setState({
                tags: this.state.tags,
                input: "",
                results: [],
                highlightedIndex: -1
            });
            this.refs.tagInput.focus();
            this.props.onUpdate(this.props.name, this.state.tags);
        }
    };

    _onRemove = (tag) => {
        this.state.tags.splice(this.state.tags.indexOf(tag), 1);
        this.setState({
            tags: this.state.tags
        });
        this.props.onUpdate(this.props.name, this.state.tags);
    };

    _onKeyPress = (e) => {
        if (e.key == "ArrowDown") {
            this.setState({
                highlightedIndex: this.state.highlightedIndex + 1
            })
        }

        if (e.key == "ArrowUp") {
            this.setState({
                highlightedIndex: this.state.highlightedIndex < 0 ? -1 : this.state.highlightedIndex - 1
            })
        }

        if (e.key == "Enter" && this.state.highlightedIndex > -1) {
            this._onAdd(this.state.results[this.state.highlightedIndex])
        }
    };

    render() {
        let tags = this.state.tags.map(function (val) {
            return <Tag data={val} onRemove={this._onRemove}/>
        }.bind(this));

        let results;
        let exactMatch = false;
        if (this.state.input.length > 1) {
            results = [];
            this.state.availableTags.forEach((item) => {
                if (item.toUpperCase().indexOf(this.state.input.toUpperCase()) >= 0) {
                    results.push(
                        <TagResult data={item} onClick={this._onAdd}/>
                    );
                }
            })
        }

        return (
            <div className="tag-editor">
                <div className="ide-layout">
                    <div className="ide-row">
                        <div className="ide-col">
                            <input type="text" value={this.state.input} onChange={this.onChange} ref="tagInput"
                                   onKeyUp={this._onKeyPress}/>
                            {results ?
                                <div
                                    style={{
                                        position: "absolute",
                                        top: 31,
                                        left: 0,
                                        width: "100%",
                                        zIndex: 999,
                                        background: "#F2F2F2"
                                    }}>
                                    <div className="tag-results" style={{display: "block"}}>
                                        {results}
                                        {!exactMatch ?
                                            <div className="block" onClick={() => this._onAdd(this.state.input)}>
                                                <div className="block-content">Create Group: {this.state.input}</div>
                                            </div>
                                            : null}
                                    </div>
                                </div>
                                : null}
                        </div>
                    </div>
                    {this.state.tags.length > 0 ?
                        <div className="ide-row">
                            <div className="ide-col">
                                <div className="tags">
                                    {tags}
                                </div>
                            </div>
                        </div>
                        : null}
                </div>
            </div>
        )
    }
}

var GeneralSettings = React.createClass({
    getDefaultProps: function () {
        return {
            errors: {}
        }
    },

    getInitialState: function () {
        return {}
    },

    _onUpdate: function (prop, value) {
        this.props.onChange(prop, value);
    },

    render: function () {
        var readOnly = false;

        let canChangeParent = true;
        if (this.props.data.uuid && !this.props.data.parent_id) canChangeParent = false;


        return (
            <div className="ide-panel ide-panel-abolute ide-scroll">
                <div className="ide-settings-content">
                    <div className="ide-settings-header">
                        <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                        <div className="ide-settings-title">General Settings</div>
                    </div>
                    <div className="ide-settings-intro">
                        <p>These settings are specific to the location and dictate how the location works within the
                            system.</p>
                    </div>
                    <div className="ide-settings-form">

                        <div className="ide-section-basic">
                            <div className="header">Location Info.</div>
                            <div className="hsplit-box">
                                <div className="ide-setting-label">Location Name*</div>
                                <div className="ide-setting-control">
                                    <LanguageStringInput
                                        name="name"
                                        readOnly={readOnly}
                                        value={this.props.data.name}
                                        onUpdate={this._onUpdate}/>
                                    {this.props.errors.name ?
                                        <div className="ide-setting-error"><i
                                            className="fal fa-exclamation-triangle"></i>&nbsp;{this.props.errors.name}
                                        </div>
                                        : null}
                                </div>
                            </div>
                            <div className="hsplit-box">
                                <div className="ide-setting-label">UUID</div>
                                <div className="ide-setting-control">
                                    <DisplayField
                                        config={{defaultValue: this.props.data.uuid}}
                                        value={this.props.data.uuid}
                                        name="uuid"/>
                                </div>
                            </div>
                            <div className="hsplit-box">
                                <div className="ide-setting-label">Status*</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="status"
                                        readOnly={readOnly}
                                        value={this.props.data.status}
                                        config={fieldConfigs.status}
                                        onUpdate={this._onUpdate}/>
                                    {this.props.errors.status ?
                                        <div className="ide-setting-error"><i
                                            className="fal fa-exclamation-triangle"></i>&nbsp;{this.props.errors.status}
                                        </div>
                                        : null}
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">PCODE*</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="pcode"
                                        readOnly={readOnly}
                                        value={this.props.data.pcode}
                                        onUpdate={this._onUpdate}
                                        config={{required: true}}/>
                                    {this.props.errors.pcode ?
                                        <div className="ide-setting-error"><i
                                            className="fal fa-exclamation-triangle"></i>&nbsp;{this.props.errors.pcode}
                                        </div>
                                        : null}
                                </div>
                            </div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Type</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Location Type*</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="site_type_id"
                                        readOnly={readOnly}
                                        value={this.props.data.site_type_id}
                                        onUpdate={this._onUpdate}
                                        config={fieldConfigs.site_type_id}/>
                                    {this.props.errors.site_type_id ?
                                        <div className="ide-setting-error"><i
                                            className="fal fa-exclamation-triangle"></i>&nbsp;{this.props.errors.site_type_id}
                                        </div>
                                        : null}

                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Location Group(s)</div>
                                <div className="ide-setting-control">
                                    <LocationGroupTags
                                        name="groups"
                                        value={this.props.data.groups}
                                        onUpdate={this._onUpdate}/>
                                </div>
                            </div>
                        </div>

                        {canChangeParent ?
                                <SettingsSection title="Lineage">
                                    <SettingsField label="Parent Location*">
                                        <LocationField
                                            name="parent_id"
                                            readOnly={readOnly}
                                            value={this.props.data.parent_id}
                                            onUpdate={this._onUpdate}/>
                                        {this.props.errors.parent_id ?
                                            <div className="ide-setting-error"><i
                                                className="fal fa-exclamation-triangle"></i>&nbsp;{this.props.errors.parent_id}
                                            </div>
                                            : null}
                                    </SettingsField>
                                </SettingsSection>
                            : null}

                    </div>
                </div>
            </div>
        )
    }
});


export default GeneralSettings;
