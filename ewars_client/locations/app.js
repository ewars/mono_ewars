// import Main from "./components/Main";
// import ViewMain from './views/view.main';
import MainView from "./components/Component.react"
import {
    Shade,
    Button,
    Layout, Row, Cell,
    Panel,
    Modal,
    Toolbar
} from "../common";

window.ewars.d = {
    Shade,
    Button,
    Layout,
    Row,
    Cell,
    Modal,
    Panel,
    Toolbar
};

ewars.tx("com.ewars.conf", ["*"])
    .then(res => {

        ReactDOM.render(
            <MainView/>,
            document.getElementById('application')
        );
    })
    .then(err => {
        console.log(err);
    });


