const EN = {
    INTRO: 'You can adjust application settings around "notifications" here to customize when and how users are notified of changes within the system.',
    EMAIL_SERVER_TYPE: "Email server type",
    EMAIL_HOST: "Email server host",
    EMAIL_PASS: "Email server password",
    EMAIL_PORT: "Email server port",
    SEND_EMAIL: "From email address",
    REPLY_TO_EMAIL: "Reply-To email address",

    HUB_CONN_MESSAGE: `You are currently disconnected from the Hub, you can connect below in order to access more data and functionality from other accounts and the central WHO repository.`,

    INTEGRATIONS: "Integrations"
};

export default EN;