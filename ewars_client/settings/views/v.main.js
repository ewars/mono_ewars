import { Form } from "../../common";

import Dashboard from "../controls/c.dashboard";
import DashboardsSettings from "./v.settings.dashboards";
import OrganizationsManager from "./v.organizations";
import Locations from "./v.settings.locations";
import DeviceManager from "./v.devices";

import SettingsForm from "../controls/c.form.settings";
import _GENERAL from "../forms/_GENERAL";
import _ALARMS from "../forms/_ALARMS";
import _THEME from "../forms/_THEME";
import Invitations from "./v.invitations";

const SECTIONS = [
    ["GENERAL", "General", 'fa-cogs'],
    ["ALARMS", "Alarms", 'fa-bell'],
    ["DASHBOARDS", "Dashboards", 'fa-tachometer'],
    ["DEVICES", "Devices", "fa-mobile"],
    ["LOCATIONS", "Locations", 'fa-map-marker'],
    ["ORGANIZATIONS", "Organizations", "fa-building"],
    ["INVITES", "Invitations", "fa-envelope"],
    ["THEME", "Theme", "fa-paint-brush"]
];

const FORMS = {
    "GENERAL": _GENERAL,
    "ALARMS": _ALARMS,
    THEME: _THEME
};

const CMPS = {
    ORGANIZATIONS: OrganizationsManager,
    LOCATIONS: Locations,
    DEVICES: DeviceManager,
    INVITES: Invitations
};

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DASHBOARD",
            account: {},
            conf: null
        }

    }

    componentWillMount() {
        ewars.tx("com.ewars.resource", ["account", window.user.aid, null, null])
            .then(resp => {
                this.setState({
                    account: resp
                });
            })

        ewars.tx("com.ewars.conf", ["*"])
            .then((resp) => {
                this.setState({
                    conf: resp
                })
            })
    };

    _change = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        if (!this.state.conf) {
            return (
                <ewars.d.Layout>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <div className="placeholder">Loading...</div>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            )
        }

        let view;
        if (this.state.view == "DASHBOARDS") {
            view = <DashboardsSettings
                account={this.state.account}/>;
        }

        if (FORMS[this.state.view]) {
            view = <SettingsForm
                data={FORMS[this.state.view]}
                type={this.state.view}/>
        }


        if (this.state.view == "DASHBOARD") view = <Dashboard onView={this._change}/>;

        if (CMPS[this.state.view]) {
            let Cmp = CMPS[this.state.view];
            view = <Cmp/>;
        }

        return (
            <ewars.d.Layout>
                <ewars.d.Row>
                    <ewars.d.Cell borderRight={true} width="250px">
                        <ewars.d.Panel>
                            <div className="block-tree">
                                {SECTIONS.map(section => {
                                    return (
                                        <div className="block hoverable"
                                             onClick={() => {
                                                 this._change(section[0])
                                             }}>
                                            <div className="block-content">
                                                <ewars.d.Row>
                                                    <ewars.d.Cell width="20px">
                                                        <i className={"fal " + section[2]}></i>
                                                    </ewars.d.Cell>
                                                    <ewars.d.Cell>
                                                        {section[1]}
                                                    </ewars.d.Cell>
                                                </ewars.d.Row>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>

                        </ewars.d.Panel>
                    </ewars.d.Cell>
                    <ewars.d.Cell>
                        {view}
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}


export default Component;

