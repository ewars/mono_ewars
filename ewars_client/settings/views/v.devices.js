import {
    Modal,
    DataTable
} from "../../common";
import DeviceEditor from "../controls/c.editor.device";

const COLUMNS = [
    {
        name: "device_type",
        config: {
            label: "Device Type",
            type: "select",
            options: [
                ["ANDROID", "Android"],
                ["IOS", "iOS"],
                ["DESKTOP", "Desktop"],
                ["NODE", "Node"]
            ]

        }
    },
    {
        name: "device_id",
        config: {
            label: "Device ID",
            type: "text"
        }
    },
    {
        name: "device_os_version",
        config: {
            label: "Device OS Version",
            type: "text"
        }
    },
    {
        name: "device_number",
        config: {
            label: "Number",
            type: "text"
        }
    },
    {
        name: "user.name",
        config: {
            label: "User",
            type: "user",
            filterKey: "user_id"
        }
    },
    {
        name: "user.email",
        config: {
            label: "User Email"
        }
    },
    {
        name: "last_seen",
        config: {
            label: "Last Seen",
            type: "date"
        },
        fmt: ewars.DATE
    },
    {
        name: "status",
        config: {
            label: "Status",
            type: "select",
            options: [
                ["ACTIVE", "Active"],
                ["INACTIVE", "Inactive"]
            ]
        }
    },
    {
        name: "app_version",
        config: {
            label: "App Version",
            type: "text"
        }
    },
    {
        name: "app_version_name",
        config: {
            label: "App Version Name",
            type: "text"
        }
    },
    {
        name: "android_version",
        config: {
            label: "Android Version",
            type: "text"
        }
    },
    {
        name: "device_name",
        config: {
            label: "Device Type Name",
            type: "text"
        }
    }
];

const joins = [
    "user:user_id:id:id,name,email"
];


const ACTIONS = [
    {label: "View", icon: "fa-eye", action: "VIEW"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"}
];

var DeviceManager = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onCellAction: function (action, cell, data) {
        if (action == "VIEW") this.setState({
            editing: data,
            showEditor: true
        });

        if (action == "CLICK") this.setState({
            editing: data,
            showEditor: true
        });

        if (action == "DELETE") this._delete(data);
    },

    _add: function () {
        this.setState({
            editing: {},
            showEditor: true
        })
    },

    _onCancelEdit: function () {
        this.state.showEditor = false;
        this.state.editing = null;
        this.forceUpdate();
    },

    _updateUserProp: function (prop, value) {
        if (prop.indexOf("profile") >= 0) {
            var path = prop.split(".");
            if (!this.state.editing.profile) this.state.editing.profile = {};
            this.state.editing.profile[path[1]] = value;
        } else {
            this.state.editing[prop] = value;
        }
        this.forceUpdate();
    },

    _delete: function (data) {
        ewars.prompt("fa-trash", "Remove Device?", "Are you sure you want to remove this device", function () {
            let bl = new ewars.Blocker(null, "Removing device...");
            ewars.tx("com.ewars.device", ["DELETE", data.id, null])
                .then(resp => {
                    bl.destroy();
                    this.setState({
                        editing: null,
                        showEditor: false
                    });
                    ewars.emit("RELOAD_DT");
                    ewars.growl("Device removed");
                })
        }.bind(this))
    },

    _onAction: function (action) {
        if (action === "CLOSE") {
            this.setState({
                showEditor: false,
                editing: null
            })
        }
    },

    render: function () {

        var modalButtons = [
            {label: "Save Change(s)", onClick: this._onUserSave, icon: "fa-disk"},
            {label: "Cancel", onClick: this._onCancelEdit, icon: 'fa-cancel'}
        ];


        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Devices" icon="fa-mobile">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-plus"
                            onClick={this._add}
                            label="ADD"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <DataTable
                            join={joins}
                            onCellAction={this._onCellAction}
                            actions={ACTIONS}
                            columns={COLUMNS}
                            id="DEVICES"
                            paused={this.state.showEditor}
                            resource="device"/>

                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    title="Device Editor"
                    onAction={this._onAction}
                    shown={this.state.showEditor}
                    toolbar={false}>
                    <DeviceEditor
                        data={this.state.editing}
                        onUpdate={this._updateUserProp}/>

                </ewars.d.Shade>
            </ewars.d.Layout>
        )

    }
});

export default DeviceManager;
