import { EditableList } from "../../common";
import LocationTypeEditor from "../controls/c.editor.location_types";

const STYLES = {
    panel: {
        background: "#2d3032"
    },
    article: {
        paddingRight: 150,
        paddingLeft: 30,
        paddingTop: 30
    },
    label: {
        maxWidth: 150,
        fontWeight: "bold",
        textAlign: "right",
        paddingRight: 10,
        paddingTop: 8
    },
    row: {
        marginBottom: 8
    },
    header: {
        maxWidth: 150
    }
};

const ITEM_STYLE = {
    padding: 8
};

const TYPE_ACTIONS = [
    {icon: "fa-plus", action: "ADD"},
    {icon: "fa-trash", action: "REMOVE"},
    {icon: "fa-pencil", action: "EDIT"}
];

class LocationType extends React.Component {
    constructor(props) {
        super(props)
    }

    onClick = () => {
        this.props.onClick(this.props.index);
    };

    render() {
        let className = "iw-list-edit-item";
        if (this.props.active) className += " active";
        return (
            <div className={className} style={ITEM_STYLE} onClick={this.onClick}>
                {ewars.I18N(this.props.data.name)}
            </div>
        )
    }
}

class Locations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            editor: null

        }
    }

    componentWillMount() {
        this._reload();
    }

    _onAction = (action, index, item) => {
        if (action == "ADD") {
            this.setState({
                editor: {
                    status: "ACTIVE"
                }
            })
        }

        if (action == "EDIT") {
            this.setState({
                editor: item
            })
        }

        if (action == "CLOSE") {
            this.setState({
                editor: null
            });
            this._reload();
        }

        if (action == "REMOVE") {
            this._delete(item);
        }
    };

    _delete = (item) => {
        ewars.prompt("fa-trash", "Delete Location Type?", "Are you sure you want to delete this location type? Any locations of this type will be automatically reverted to 'Uncategorized'", () => {
            let bl = new ewars.Blocker(null, "Deleting location type");
            ewars.tx("com.ewars.locations.type.delete", [item.id])
                .then((resp) => {
                    bl.destroy();
                    ewars.growl("Location type deleted");
                    this._reload();
                })
        })
    };

    _reload = () => {
        ewars.tx("com.ewars.locations.types")
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    };

    _close = () => {
        this.setState({
            editor: null
        });
        this._reload();
    };

    render() {
        let types = [];
        this.state.data.forEach((item) => {
            if (item.id != 1) types.push(item);
        });

        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Locations">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"
                            color="green"
                            label="Save Change(s)"/>
                    </div>
                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            <div style={STYLES.panel} className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="system-form" style={STYLES.article}>

                                    <ewars.d.Row style={STYLES.row}>
                                        <ewars.d.Cell style={STYLES.label}>Location Types</ewars.d.Cell>
                                        <ewars.d.Cell>
                                            <EditableList
                                                onAction={this._onAction}
                                                Template={LocationType}
                                                actions={TYPE_ACTIONS}
                                                items={types}/>
                                            <div className="adm-help">Types of locations available within the system.
                                            </div>
                                        </ewars.d.Cell>
                                    </ewars.d.Row>


                                </div>
                            </div>
                        </ewars.d.Panel>

                    </ewars.d.Cell>
                </ewars.d.Row>
                <ewars.d.Shade
                    toolbar={false}
                    onAction={this._onAction}
                    shown={this.state.editor}>
                    {this.state.editor ?
                        <LocationTypeEditor
                            onSave={this._close}
                            data={this.state.editor}/>
                        : null}

                </ewars.d.Shade>
            </ewars.d.Layout>
        )
    }
}

export default Locations;
