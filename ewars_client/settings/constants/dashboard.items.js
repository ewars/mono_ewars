const DASHBOARD_ITEMS = [
    {
        t: "General",
        i: "fa-cogs",
        v: "GENERAL",
        p: `
            <p>Administer general settings for the account.</p>
        `
    },
    {
        t: "Dashboards",
        i: "fa-tachometer",
        v: "DASHBOARDS",
        p: `
            <p>Configure individual role types dashboards.</p>
        `
    },
    {
        t: "Alarms",
        i: "fa-bell",
        v: "ALARMS",
        p: `
            <p>Configure and administer settings for alarms and alerts within EWARS.</p>
        `
    },
    {
        t: "Locations",
        i: "fa-map-marker",
        v: "LOCATIONS",
        p: `
            <p>Set and configure settings for how locations operate within the account.</p>
        `
    },
    // {
    //     t: "Hub",
    //     i: "fa-share",
    //     v: "HUB",
    //     p: `
    //         <p>Administer and configure hub integrations.</p>
    //     `
    // },
    {
        t: "Organizations",
        i: "fa-building",
        v: "ORGANIZATIONS",
        p: `
            <p>Customize the organizations that users for this account can belong to.</p>
        `
    },
    // {
    //     t: "Distribution Lists",
    //     v: "DISTRIBUTIONS",
    //     i: "fa-share",
    //     p: `
    //         <p>Configure custom sets of users and non-EWARS emails to send information to</p>
    //     `
    // },
    // {
    //     t: "Teams",
    //     v: "TEAMS",
    //     i: "fa-group",
    //     p: `
    //         <p>Create custom teams consisting of users grouped by location, assignment, role and more</p>
    //     `
    // },
    // {
    //     t: "Rules",
    //     i: "fa-code-branch",
    //     v: "RULES",
    //     p: `
    //         <p>Set up custom rules for how different aspects of EWARS operates.</p>
    //     `
    // },
    {
        t: "Devices",
        i: "fa-mobile",
        v: "DEVICES",
        p: `
            <p>Manage and view devices connexted to your account.</p>
        `
    }
];

export default DASHBOARD_ITEMS;