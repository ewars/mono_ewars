import Component from "./views/v.main";

import {
    Layout, Row, Cell,
    Button,
    Shade,
    Modal,
    Form,
    Toolbar,
    Panel,
    ButtonGroup,
    ActionGroup
} from "../common";

ewars.d = {
    Layout, Row, Cell,
    Button,
    Shade,
    Modal,
    Form,
    Toolbar,
    Panel,
    ButtonGroup,
    ActionGroup
}

import EN from "./lang/en";
__.register(EN);

ReactDOM.render(
    <Component/>,
    document.getElementById("application")
);
