import TIMEZONES from "../../common/constants/timezones";

let timezoneOptions = [];

for (var i in TIMEZONES) {
    TIMEZONES[i].forEach(location => {
        timezoneOptions.push([
            location + " " + i,
            location + " [" + i + "]"
        ])
    })
}

const SSL_OPTIONS = [
    ["ENABLED", "Enabled"],
    ["DISABLED", "Disabled"]
];

export default [
    "General Settings",
    {
        l: "Account name",
        t: "text",
        n: "name"
    },
    {
        l: "Domain name",
        t: "text",
        n: "domain"
    },
    {
        l: "Description",
        t: "textarea",
        n: "description"
    },
    {
        l: "ISO2",
        t: "text",
        n: "iso2"
    },
    {
        l: "ISO3",
        t: "text",
        n: "iso3"
    },
    "Authentication & Security",
    "Change settings pertaining to how users access the EWARS systems",
    {
        l: "Two-factor authentication",
        t: "button_group",
        c: {
            options: [
                ["ENABLED", "Enabled"],
                ["DISABLED", "Disabled"]
            ]
        },
        n: "two_factor"
    },
    {
        l: "Enforce SSL",
        t: "button_group",
        c: {
            options: SSL_OPTIONS
        },
        n: "ssl"
    },
    "System Defaults",
    "Set sane defaults for some user-overridable settings",
    {
        l: "Default language",
        t: "select",
        c: {
            options: [
                ["en", "English"],
                ["fr", "French"]
            ]
        },
        n: "default_language"
    },
    {
        l: "Timezone",
        t: "select",
        c: {
            options: timezoneOptions
        },
        n: "timezone"
    },
    {
        l: "Date format",
        t: "select",
        c: {
            options: [
                ["DEFAULT", "Default (ISO 8601: 200-02-28)"],
                ["ISO8601", "ISO 8601: 2000-02-28"],
                ["US", "2/28/2000"],
                ["EU", "Europe: 28-02-2000"]
            ]
        },
        n: "fmt_date"
    },
    {
        l: "Time format",
        t: "select",
        c: {
            options: [
                ["DEFAULT", "Default (12 Hour, 2:34 PM)"],
                ["12", "12 Hour, 2:34 PM"],
                ["24", "24 Hour, 14:34"]
            ]
        },
        n: "fmt_time"
    },
    {
        l: "Translation",
        t: "select",
        c: {
            options: [
                ["en_US", "English (US)"],
                ["fr_FR", "French (France)"],
                ["en_A*", "ENGLISH (US, ALL CAPS)"],
                ["en_R*", "English (Raw Strings)"]
            ]
        },
        n: "translation"
    },
    "System/Account Information",
    {
        l: "Octet version",
        t: "display",
        n: "NFO_OCTET"
    },
    {
        l: "Hub access",
        t: "display",
        n: "NFO_HUB"
    },
    {
        l: "Temp domain",
        t: "display",
        n: "NFO_TEMP_DOMAIN"
    },
    {
        l: "ID",
        t: "display",
        n: "NFO_ID"
    },
    {
        l: "Storage",
        t: "display",
        n: "NFO_STORAGE"
    },
    {
        l: "Emails Sent",
        t: "display",
        n: "NFO_EMAILS_SENT"
    }
];
