import {
    TextField,
    SelectField
} from "../../common/fields";

var fields = {
    device_type: {
        options: [
            ["ANDROID", "Android"],
            ["IOS", "iOS"],
            ["WINDOWS_MOB", "Windows Mobile"],
            ["SYMBIAN", "Symbian"],
            ["BLACKBERRY", "Blackberry OS"],
            ["OSX", "Apple OSX"],
            ["EWARS_NUC", 'EWARS Remote Node'],
            ["WINDOWS", "Windows Desktop"],
            ["LINUX", "Linux Desktop"],
            ["CLI", "CLI"]
        ]
    },
    status: {
        options: [
            ['INACTIVE', "Inactive"],
            ["ACTIVE", "Active"],
            ["BLOCKED", "Blocked"]
        ]
    },
    gate_id: {
        optionsSource: {
            resource: "gateway",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"}
            }
        }
    }
};

var DeviceEditor = React.createClass({
    getInitialState: function () {
        return {
            data: {},
            emailChanged: false,
            account: null
        }
    },

    componentWillMount: function () {
        this.state.data = JSON.parse(JSON.stringify(this.props.data));

        ewars.tx("com.ewars.resource", ["account", this.props.data.account_id, ["location_id"], null])
            .then(function (resp) {
                this.state.account = resp;
                this.forceUpdate();
            }.bind(this))
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.data = JSON.parse(JSON.stringify(nextProps.data));

        ewars.tx("com.ewars.resource", ["account", this.props.data.account_id, ["location_id"], null])
            .then(function (resp) {
                this.state.account = resp;
                this.forceUpdate();
            }.bind(this))
    },

    _updateField: function (prop, value) {
        if (prop == "email") this.state.emailChanged = true;
        this.props.onUpdate(prop, value);
    },

    render: function () {

        var lastSeen = ewars.formatters.DATE_FORMATTER(this.props.data.last_seen);

        return (
            <div className="ide-settings-panel">
                <div className="ide-settings-content">
                    <div className="ide-settings-header">
                        <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                        <div className="ide-settings-title">Device Settings</div>
                    </div>
                    <div className="ide-settings-intro">
                        <p>Edit and save change(s) to a device connected to the system.</p>
                    </div>
                    <div className="ide-settings-form">

                        <div className="ide-section-basic">
                            <div className="header">Platform Details</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Operating System*</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="device_type"
                                        readOnly={true}
                                        config={fields.device_type}
                                        value={this.props.data.device_type}
                                        onUpdate={this._updateField}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Device ID (IMEI)</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="device_id"
                                        readOnly={true}
                                        value={this.props.data.device_id}
                                        onUpdate={this._updateField}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">OS Version</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="device_os_version"
                                        readOnly={true}
                                        value={this.props.data.android_version}
                                        onUpdate={this._updateField}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Status</div>
                                <div className="ide-setting-label">
                                    <SelectField
                                        name="status"
                                        value={this.props.data.status}
                                        config={fields.status}
                                        onUpdate={this._updateField}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">SMS Gateway</div>
                                <div className="ide-setting-control">
                                    <SelectField
                                        name="gate_id"
                                        value={this.props.data.gate_id}
                                        onUpdate={this._updateField}
                                        config={fields.gate_id}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Device Phone #</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="device_number"
                                        value={this.props.data.device_number}
                                        onUpdate={this._updateField}
                                        readOnly={true}/>

                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Device Name</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="device_name"
                                        value={this.props.data.device_name}
                                        onUpdate={this._updateField}
                                        readOnly={true}/>
                                </div>
                            </div>
                        </div>

                        <div className="ide-section-basic">
                            <div className="header">User</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">User Name</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="user.name"
                                        readOnly={true}
                                        value={this.props.data.user_name}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">User Email</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="user.email"
                                        readOnly={true}
                                        value={this.props.data.user_email}/>
                                </div>
                            </div>

                        </div>

                        <div className="ide-section-basic">
                            <div className="header">Usage Details</div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Last Seen</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="last_seen"
                                        value={lastSeen}
                                        readOnly={true}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">EWARS Mobile App Version</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="version_code"
                                        value={this.props.data.app_version}
                                        readOnly={true}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Last Latitude</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="device_lat"
                                        value={this.props.data.device_lat}
                                        readOnly={true}/>
                                </div>
                            </div>

                            <div className="hsplit-box">
                                <div className="ide-setting-label">Last Longitude</div>
                                <div className="ide-setting-control">
                                    <TextField
                                        name="device_lng"
                                        value={this.props.data.device_lng}
                                        readOnly={true}/>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
});

export default DeviceEditor;
