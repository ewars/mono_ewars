/**
 * Detect if a widget definition is a legacy definition
 * @param definition
 */
function isLegacy(definition) {

}

/**
 * Update a definition from the legacy format to the
 * new format
 * @param definition
 */
function updateDefinition(definition) {

}