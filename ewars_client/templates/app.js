import Component from "./components/Dashboard.react";

import {
    Layout,
    Row,
    Cell,
    Shade,
    Toolbar,
    Panel,
    Button,
    ButtonGroup,
    ActionGroup
} from "../common";

ewars.d = {
    Layout,
    Row,
    Cell,
    Shade,
    Toolbar,
    Panel,
    Button,
    ButtonGroup,
    ActionGroup
}

ReactDOM.render(
    <Component/>,
    document.getElementById("application")
);
