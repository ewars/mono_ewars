import { Form } from "../../../common";
import FORM_FIELDS from "../../constants/settings";

var Settings = React.createClass({
    getInitialState: function () {
        return {}
    },

    _onUpdate: function (data, prop, value, path) {
        this.props.onChange(prop, value);
    },

    render: function () {

        if (window.user.user_type != "SUPER_ADMIN") {
            FORM_FIELDS.source_form_id.optionsSource.query.account_id = {eq: window.user.account_id};
            FORM_FIELDS.source_form_id.optionsSource.query.status = {eq: "ACTIVE"}
        }

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="ide-settings-panel">
                    <div className="ide-settings-content">
                        <div className="ide-settings-header">
                            <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                            <div className="ide-settings-title">Template Settings</div>
                        </div>
                        <div className="ide-settings-intro">
                            <p>Define how this template generates reports and how those reports are made available</p>
                        </div>
                        <div className="ide-settings-form">

                            <Form
                                definition={FORM_FIELDS}
                                data={this.props.data}
                                readOnly={false}
                                updateAction={this._onUpdate}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default Settings;
