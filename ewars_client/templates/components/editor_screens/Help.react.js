var Help = React.createClass({
    render: function () {
        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="ide-settings-panel">
                    <div className="ide-settings-content">
                        <div className="ide-settings-header">
                            <div className="ide-settings-icon"><i className="fal fa-cog"></i></div>
                            <div className="ide-settings-title">Template Help</div>
                        </div>
                        <div className="ide-settings-intro">
                            <p>Information on available widgets, tags, etc... for use in templates</p>
                        </div>
                        <div className="ide-settings-form">

                            <div className="ide-section-basic">
                                <div className="header">Tags</div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{report_date}'}</div>
                                    <div className="ide-setting-control">Displays the report data of the report
                                        currently being viewed
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{report_start_date}'}</div>
                                    <div className="ide-setting-control">Displays the start date of the report</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{report_end_date}'}</div>
                                    <div className="ide-setting-control">Displays the end date of the report</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{location_name}'}</div>
                                    <div className="ide-setting-control">Displays the location name that the report
                                        corresponds to
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{year}'}</div>
                                    <div className="ide-setting-control">Year the report is in</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{month}'}</div>
                                    <div className="ide-setting-control">Month of the report</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{day}'}</div>
                                    <div className="ide-setting-control">Day of the report</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{day_week}'}</div>
                                    <div className="ide-setting-control">Day of the week (Mon, Tues, Thur, etc...)</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{week}'}</div>
                                    <div className="ide-setting-control">The ISO8601 week number of the year</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{name}'}</div>
                                    <div className="ide-setting-control">Instance name for the report as defined in
                                        settings
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{today}'}</div>
                                    <div className="ide-setting-control">Displays the current time and date at render time</div>
                                </div>
                            </div>

                            <div className="ide-section-basic">
                                <div className="header">Per-Report Functions</div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{get_field("field_name")}'}</div>
                                    <div className="ide-setting-control">Get the raw value of a field in the source report</div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">{'{get_field_display("field.name")}'}</div>
                                    <div className="ide-setting-control">Retrieve the displayable value for a field in the source report</div>
                                </div>

                            </div>

                            <div className="ide-section-basic">
                                <div className="header">Events</div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">WIDGETS_LOADED</div>
                                    <div className="ide-setting-control">Fired after all widgets have loaded their
                                        data.
                                    </div>
                                </div>

                                <div className="hsplit-box">
                                    <div className="ide-setting-label">PRE_PDF_RENDER</div>
                                    <div className="ide-setting-control">Fired immediately before PDF rendering of the
                                        document begins.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

export default Help;
