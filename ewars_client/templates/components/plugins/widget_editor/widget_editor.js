import WidgetEditorEditing from "./widget_editor_editing";
import WidgetEditorUI from "./widget_editor_ui";

import Plugin from "@ckeditor/ckeditor5-core/src/plugin";

export default class WidgetEditor extends Plugin {
    static get requires() {
        return [ WidgetEditorEditing, WidgetEditorUI ];
    }
}

