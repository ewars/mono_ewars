import {
    DataTable,
    Button,
    Spinner,
    Filler
} from "../../common";

import TemplateEditor from "./TemplateEditor.react";
import TemplateTree from "./TemplateTree.react";

const CONSTANTS = {
    DEFAULT: "DEFAULT",
    EDIT: "EDIT"
};

var TemplatePrototype = {
    template_name: {en: null},
    instance_name: {en: null},
    content: null,
    definition: {},
    status: 'DRAFT',
    generation: {
        document_date: new Date(),
        interval: "DAY",
        start_date: new Date(),
        end_date: new Date(),
        location_type: null,
        location_spec: "TYPE",
        location_uuid: null,
        notify: false,
        notifiers: []
    },
    shareable: false,
    access: "PRIVATE"
};

const COLUMNS = [
    {
        name: "template_name",
        sortKey: "template_name.en",
        config: {label: "Template Name", type: "text"},
        width: 350,
        fmt: ewars.I18N
    },
    {
        name: "created",
        config: {
            label: "Created",
            type: "date"
        }
    },
    {
        name: "created_by",
        config: {
            label: "Created By",
            type: "user"
        }
    },
    {
        name: "status",
        config: {
            label: "Status",
            type: "select",
            options: [
                ["ACTIVE", "Active"],
                ["DRAFT", "Draft"]
            ]
        }
    },
    {
        name: "shareable",
        config: {
            label: "Shareable",
            type: "text"
        }
    },
    {
        name: "access",
        config: {
            label: "Access",
            type: "text"
        }
    }
];

const ACTIONS = [
    {label: "Edit", icon: "fa-pencil", action: "EDIT"},
    {label: "Delete", icon: "fa-trash", action: "DELETE"},
    {label: "Duplicate", icon: "fa-copy", action: "DUPLICATE"}
];

const JOINS = [];

var FILTER = {};

if (window.user.user_type == "ACCOUNT_ADMIN") FILTER.account_id = {eq: window.user.account_id};

var ReportItem = React.createClass({
    onClick: function () {
        this.props.onClick(this.props.data);
    },

    render: function () {
        var name = ewars.formatters.I18N_FORMATTER(this.props.data.template_name);
        return (
            <div className="report" onClick={this.onClick}>
                <div className="name">{name}</div>
            </div>
        )
    }
});

var Dashboard = React.createClass({
    _isLoading: true,

    getInitialState: function () {
        return {
            view: CONSTANTS.DEFAULT,
            editing: null
        }
    },

    componentWillMount: function () {
    },

    onReportSelect: function (template) {
        this.setState({
            editing: template,
            view: CONSTANTS.EDIT
        })
    },

    _onNewTemplate: function () {
        this.setState({
            editing: _.extend({}, TemplatePrototype),
            view: CONSTANTS.EDIT
        })
    },

    _closeReport: function () {
        this.setState({
            editing: null,
            view: CONSTANTS.DEFAULT
        })
    },

    _onCellAction: function (action, cell, data) {
        if (action == CONSTANTS.EDIT) this.onReportSelect(data);
        if (action == "CLICK") this.onReportSelect(data);

        if (action == "DELETE") {
            this._delete(data.uuid);
        }

        if (action == "DUPLICATE") {
            let newItem = ewars.copy(data);
            newItem.uuid = null;
            this.onReportSelect(newItem);
        }
    },

    _onChange: function (prop, value) {
        if (prop == "data") {
            this.setState({
                ...this.state,
                editing: {
                    ...this.state.editing,
                    data: value[1],
                    content: value[0]
                }
            });
            return;
        }

        if (prop.indexOf(".") >= 0) {
            let copy = ewars.copy(this.state.editing);
            ewars.setKeyPath(copy, prop, value);
            this.setState({
                editing: copy
            })
        } else {
            this.setState({
                ...this.state,
                editing: {
                    ...this.state.editing,
                    [prop]: value
                }
            })
        }
    },

    _save: function () {
        var blocker = new ewars.Blocker(null, "Saving template...");
        if (this.state.editing.uuid) {
            ewars.tx("com.ewars.template.update", [this.state.editing.uuid, this.state.editing])
                .then(function (resp) {
                    blocker.destroy();
                    ewars.growl("Template updated");
                    ewars.emit("RELOAD_DT");
                }.bind(this))
        } else {
            ewars.tx("com.ewars.template.create", [this.state.editing])
                .then(function (resp) {
                    blocker.destroy();
                    ewars.growl("Template created");
                    ewars.emit("RELOAD_DT");
                    this.setState({
                        editing: resp
                    })
                }.bind(this))
        }
    },

    _delete: function (uuid) {
        ewars.prompt("fa-trash", "Delete Template?", "Are you sure you want to delete this template, any documents and reports associated with this template will be lost.", function () {
            var blocker = new ewars.Blocker(null, "Deleting template...");

            ewars.tx("com.ewars.template.delete", [uuid])
                .then((resp) => {
                    blocker.destroy();
                    ewars.emit("RELOAD_DT");
                    ewars.growl("Template deleted");
                })
        }.bind(this))
    },

    _clearCache: function () {
        let bl = new ewars.Blocker(null, "Clearing document cache...");
        ewars.tx("com.ewars.template.cache.clear", [this.state.editing.uuid])
            .then((resp) => {
                bl.destroy();
                ewars.growl("Cache cleared");
            })
    },


    render: function () {

        let view;

        if (this.state.editing) view = <TemplateEditor
            data={this.state.editing}
            onClose={this._closeReport}
            onChange={this._onChange}/>;

        if (!this.state.editing) view = <DataTable
            onCellAction={this._onCellAction}
            resource="template"
            filter={FILTER}
            actions={ACTIONS}
            join={JOINS}
            columns={COLUMNS}/>;

        let showCacheClear;
        if (this.state.editing && this.state.editing.uuid) {
            if (ewars.isSet(this.state.editing.expires) && this.state.editing.expires != 0) {
                showCacheClear = (
                    <ewars.d.Button
                        label="Clear Cache"
                        icon="fa-times-circle"
                        onClick={this._clearCache}/>
                )
            }
        }

        return (
            <div className="ide">
                <div className="ide-layout">
                    <div className="ide-row" style={{maxHeight: 35}}>
                        <div className="ide-col">
                            <div className="ide-tbar">
                                <div className="ide-tbar-text">Document Templates</div>

                                {this.state.view == CONSTANTS.EDIT ?
                                    <div className="btn-group pull-right">
                                        <ewars.d.Button
                                            label="Save Change(s)"
                                            icon="fa-save"
                                            color="green"
                                            onClick={this._save}/>
                                        {showCacheClear}
                                        <ewars.d.Button
                                            label="Cancel"
                                            color="amber"
                                            icon="fa-times"
                                            onClick={this._closeReport}/>
                                    </div>
                                    :

                                    <div className="btn-group pull-right">
                                        <ewars.d.Button
                                            label="New"
                                            icon="fa-plus"
                                            onClick={this._onNewTemplate}/>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="ide-row">
                        <div className="ide-col">
                            {view}
                        </div>
                    </div>
                </div>
            </div>
        )

    }
});

export default Dashboard;
