import { Spinner } from "../../common";

var NodeItem = React.createClass({
    _onClick: function () {
        this.props.onNodeClick(this.props.data);
    },

    render: function () {
        var name = ewars.I18N(this.props.data.template_name);

        let className = "block-content";
        if (this.props.data.status == "ACTIVE") className += " status-green";
        if (this.props.data.status == "INACTIVE") className += " status-red";
        if (this.props.data.status == "DRAFT") className += " status-red";

        return (
            <div className="block" onClick={this._onClick}>
                <div className={className}>{name}</div>
            </div>
        )
    }
});

var TemplateTreeNode = React.createClass({
    getInitialState: function () {
        return {
            isCollapsed: true,
            data: [],
            childrenLoaded: false
        }
    },

    componentWillMount: function () {
        ewars.subscribe("RELOAD_TEMPLATES", this._reload);
    },

    _reload: function () {
        if (!this.state.isCollapsed) {
            ewars.tx("com.ewars.query", ["template", ["uuid", "template_name", "status"], {account_id: {eq: this.props.data.id}}, {"template_name.en": "DESC"}, null, null, null])
                .then(function (resp) {
                    this.state.data = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    _expand: function () {
        if (this.state.childrenLoaded) {
            this.state.isCollapsed = this.state.isCollapsed ? false : true;
            this.forceUpdate();
        } else {
            this.refs.icon.setAttribute("class", "fa fa-spin fa-gear");
            ewars.tx("com.ewars.query", ["template", ["uuid", "template_name", "status"], {account_id: {eq: this.props.data.id}}, {"template_name.en": "DESC"}, null, null, null])
                .then(function (resp) {
                    this.state.data = resp;
                    this.state.childrenLoaded = true;
                    this.state.isCollapsed = false;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this))
        }
    },

    render: function () {

        var handleClass = "fa fa-folder";
        if (!this.state.isCollapsed) handleClass = "fa fa-folder-open";

        var accountName = ewars.I18N(this.props.data.name);

        var children;
        if (!this.state.isCollapsed) {
            children = this.state.data.map(function (item) {
                return <NodeItem data={item} onNodeClick={this.props.onNodeClick} key={item.uuid}/>
            }.bind(this));
        }

        return (
            <div className="block">
                <div className="block-content" onClick={this._expand}>
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 25}}><i ref="icon" className={handleClass}></i></div>
                        <div className="ide-col">{accountName}</div>
                    </div>
                </div>
                {!this.state.isCollapsed ?
                    <div className="block-children">
                        {children}
                    </div>
                    : null}
            </div>
        )
    }
});

var TemplateTree = React.createClass({
    _isLoading: true,
    getInitialState: function () {
        return {
            data: []
        }
    },

    componentWillMount: function () {
        var query = {
            instance_id: {eq: window.user.instance.uuid},
            id: {neq: 1}
        };

        ewars.tx("com.ewars.query", ["account", ["id", "name"], query, {name: "DESC"}, null, null, null])
            .then(function (resp) {
                this.state.data = resp;
                this._isLoading = false;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this))


    },

    _reload: function () {
        var query = {
            instance_id: {eq: window.user.instance.uuid},
            id: {neq: 1}
        };

        ewars.tx("com.ewars.query", ["account", ["id", "name"], query, {name: "DESC"}, null, null, null])
            .then(function (resp) {
                this._isLoading = false;
                this.setState({
                    data: resp
                })
            }.bind(this))
    },

    render: function () {
        if (this._isLoading) return <Spinner/>;

        let rootNodes = this.state.data.map(function (item) {
            return <TemplateTreeNode data={item} onNodeClick={this.props.onNodeClick} key={item.id}/>;
        }.bind(this));

        return (
            <div className="ide-panel ide-panel-absolute ide-scroll">
                <div className="block-tree">
                    {rootNodes}
                </div>
            </div>
        )
    }
});

export default TemplateTree;
