import ace from "ace-builds/src-min-noconflict/ace";

class HtmlEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this._editor = ace.edit(this._el, {
            mode: "ace/mode/html"
        });
        this._editor.setTheme("ace/theme/monokai");
    }

    componentWillUnmount() {
        console.log(this._editor.getValue());
        this.props.onChange(this._editor.getValue());
    }

    render() {
        return (
            <ewars.d.Layout>
                <div ref={(el) => this._el = el} className="editor">
                    {this.props.data.content}

                </div>
            </ewars.d.Layout>
        )
    }
}

export default HtmlEditor;
