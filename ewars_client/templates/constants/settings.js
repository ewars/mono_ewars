export default {
    h_general: {
        type: "header",
        label: _l("GENERAL")
    },
    template_name: {
        type: "language_string",
        label: _l("TEMPLATE_NAME"),
        required: true
    },
    instance_name: {
        type: "language_string",
        label: _l("INSTANCE_NAME"),
        required: true
    },
    uuid: {
        type: "display",
        label: "UUID"
    },
    template_type: {
        type: "button_group",
        label: _l("TEMPLATE_TYPE"),
        options: [
            ["GENERATED", _l("GENERATED")],
            ["PER_REPORT", _l("PER_REPORT")],
            ["ONE_OFF", _l("ONE_OFF")]
        ],
        default: "GENERATED"
    },
    orientation: {
        type: "button_group",
        label: _l("PDF_PAGE_ORIENTATION"),
        options: [
            ["PORTRAIT", _l("PORTRAIT")],
            ["LANDSCAPE", _l("LANDSCAPE")]
        ]
    },
    document_date: {
        type: "date",
        label: _l("DOCUMENT_DATE"),
        nameOverride: "generation.document_date",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "ONE_OFF"]
            ]
        }
    },
    status: {
        type: "button_group",
        label: _l("STATUS"),
        options: [
            ["ACTIVE", _l("ACTIVE")],
            ["DRAFT", _l("DRAFT")],
            ["INACTIVE", _l("INACTIVE")]
        ],
        default: "DRAFT"
    },
    description: {
        type: "textarea",
        label: _l("DESCRIPTION"),
        i18n: true
    },
    h_generation: {
        type: "header",
        label: _l("GENERATION")
    },
    interval: {
        type: "button_group",
        nameOverride: "generation.interval",
        label: _l("INTERVAL"),
        options: [
            ["DAY", _l("DAILY")],
            ["WEEK", _l("WEEKLY")],
            ["MONTH", _l("MONTHLY")],
            ["YEAR", _l("YEARLY")]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "GENERATED"]
            ]
        }
    },
    location_spec: {
        type: "button_group",
        label: _l("LOCATION_SPEC"),
        nameOverride: "generation.location_spec",
        options: [
            ["TYPE", _l("GENERATE_FOR_TYPE")],
            ["SPECIFIC", _l("GENERATE_FOR_LOC")]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "GENERATED"]
            ]
        }
    },
    location: {
        type: "location",
        label: _l("LOCATION"),
        nameOverride: "generation.location",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "GENERATED"]
            ]
        }
    },
    location_type: {
        type: "select",
        label: _l("LOCATION_TYPE"),
        nameOverride: "generation.location_type",
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "GENERATED"],
                ["generation.location_spec", "eq", "TYPE"]
            ]
        }
    },
    start_date: {
        type: "date",
        nameOverride: "generation.start_date",
        label: _l("START_DATE"),
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "GENERATED"]
            ]
        }
    },
    end_date: {
        type: "date",
        nameOverride: "generation.end_date",
        label: _l("END_DATE"),
        default: new Date(),
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "GENERATED"]
            ]
        }
    },
    source_form_id: {
        type: "select",
        nameOverride: "generation.source_form_id",
        label: _l("SOURCE_FORM"),
        required: true,
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {}
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["template_type", "eq", "PER_REPORT"]
            ]
        }
    },
    h_access: {
        type: "header",
        label: _l("ACCESS_CONSTRAINTS")
    },
    access: {
        type: "button_group",
        label: _l("ACCESS"),
        options: [
            ["PUBLIC", _l("PUBLIC")],
            ["PRIVATE", _l("PRIVATE")]
        ],
        default: "PRIVATE"
    },
    shareable: {
        type: "button_group",
        label: _l("SHARING"),
        options: [
            [true, _l("ENABLED")],
            [false, _l("DISABLED")]
        ],
        default: false
    },
    h_additional: {
        type: "header",
        label: _l("ADDITIONAL")
    },
    notify: {
        type: "button_group",
        options: [
            [true, _l("Enabled")],
            [false, _l("Disabled")]
        ],
        label: _l("NOTIFY_ON_AVAILABLE"),
        default: false
    },
    expires: {
        type: "number",
        label: "Cache Expiry (hours)",
        help: "0 or empty disables caching"
    }

}
