use std::io::{ErrorKind};
use failure::{Error};

use crate::models::{Query, QueryResult, RecordsQuery, EventLogEntry};
use uuid::Uuid;

pub trait Creatable<T> {
    type Item;

    fn create(&self, conn: T, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        bail!("Not implemented");
    }

    fn create_returning(&self, conn: T, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        bail!("Not implemented");
    }
}

pub trait Deletable<T> {
    fn delete(&self, conn: T, tki: &'static str) -> Result<(), Error> {
        bail!("Not implemented!");
    }

    fn delete_by_id(conn: T, tki: &'static str, id: &Uuid) -> Result<(), Error> {
        bail!("Not implemented");
    }
}

pub trait Updatable<T> {
    type Item;

    fn update(&self, conn: T, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        unimplemented!()
    }

    fn update_returning(&self, conn: T, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        unimplemented!()
    }
}

pub trait Insertable<T> {
    fn insert(&self, conn: T, tki: &'static str) -> Result<(), Error> {
        unimplemented!()
    }
}

pub trait Queryable<T> {
    type Item;
    type Query;
    type QueryResult;

    fn get_by_id(conn: T, tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        bail!("not implemented");
    }

    fn get_base_by_id(conn: T, tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        bail!("not implemented");
    }

    fn query(conn: T, tki: &'static str, query: &Self::Query) -> Result<Self::QueryResult, Error> {
        bail!("not implemented");
    }   

    fn query_base(conn: T, tki: &'static str, query: &Self::Query) -> Result<Self::QueryResult, Error> {
        bail!("not implemented");
    }
}

// An item which can have an event log created when it changes
pub trait EventControls<T> {
    type Item;

    fn get_stream(&self, conn: T, tki: &'static str) -> Result<Self::Item, Error> {
        bail!("Not implemented");
    }
}

pub trait Expand<T> {
    type Item;

    fn expand(&self, conn: T, tki: &'static str) -> Result<Self::Item, Error> {
        bail!("unimplemented");
    }
}


pub trait Upgrade<T> {
    type Item;

    fn upgrade(&self, conn: T, tki: &'static str) -> Result<Self::Item, Error> {
        bail!("unimplemented!");
    }
}

pub struct Row {

}

pub struct RowCollection {

}

pub trait DatabaseInterface<T> {

    fn query(&self) -> Result<RowCollection, Error> {
        unimplemented!()
    }

    fn query_one(&self) -> Result<Option<Row>, Error> {

    }

    fn query_map(&self, fn: &Fn(row: Row, index: u32) -> Result<RowCollection, Error> {

    }

    fn insert(&self) -> Result<Option<Row>, Error> {
        unimplemented!()
    }

    fn execute(&self) -> Result<bool, Error> {

    }
}

