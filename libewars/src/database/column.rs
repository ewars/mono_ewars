pub struct Column {
    pub name: String,
    pub nullable: bool,
    pub col_type: String,
    pub default: String,
}
