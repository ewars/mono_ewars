pub static SQLITE_BASE: &'static [&str] = &[
    r#"
        CREATE TABLE forms (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            version INTEGER NOT NULL DEFAULT 1,
            status TEXT NOT NULL DEFAULT 'DRAFT',
            description TEXT NOT NULL DEFAULT '',
            guidance TEXT NOT NULL DEFAULT '',
            eid_prefix TEXT NULL,
            features JSON NOT NULL DEFAULT '{}',
            definition JSON NOT NULL DEFAULT '{}',
            constraints JSON NOT NULL DEFAULT '[]',
            stages JSON NOT NULL DEFAULT '[]',
            etl JSON NOT NULL DEFAULT '{}',
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    #",
    r#"
        CREATE TABLE indicators (
            uuid TEXT PRIMARY KEY NOT NULL,
            group_id TEXT NULL,
            name TEXT NOT NULL DEFAULT 'New Indicator',
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            description TEXT NULL,
            itype TEXT NULL,
            icode TEXT NULL,
            permissions JSON NOT NULL DEFAULT '{}',
            definition JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE indicator_groups (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            description TEXT NULL,
            pid TEXT NULL,
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE locations (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            status TEXT NOT NULL DEFAULT 'ACTIVE',
            pcode TEXT NOT NULL,
            codes JSON NOT NULL DEFAULT '{}',
            groups JSON NOT NULL DEFAULT '[]',
            data JSON NOT NULL DEFAULT '{}',
            lti TEXT NOT NULL,
            pid TEXT NULL,
            lineage JSON NOT NULL DEFAULT '[]',
            organizations JSON NOT NULL DEFAULT '[]',
            image BYTEA NULL,
            geometry_type TEXT NOT NULL DEFAULT 'POINT',
            geojson JSON NOT NULL DEFAULT '{}',
            population JSON NOT NULL DEFAULT '[]',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    #",
    r#"
        CREATE TABLE location_types (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            description TEXT NOT NULL DEFAULT '',
            restrictions JSON NOT NULL DEFAULT '[]',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE reporting (
            uuid TEXT PRIMARY KEY NOT NULL,
            lid TEXT NOT NULL,
            fid TEXT NOT NULL,
            start_date DATE NOT NULL,
            end_date DATE NULL,
            status TEXT NOT NULL DEFAULT 'ACTIVE',
            pid TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    #",
    r#"
        CREATE TABLE records (
            uuid TEXT PRIMARY KEY NOT NULL,
            status TEXT NOT NULL DEFAULT 'SUBMITTED',
            stage TEXT NULL,
            workflow JSON NOT NULL DEFAULT '[]',
            fid TEXT NOT NULL,
            data JSON NOT NULL DEFAULT '{}',
            submitted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            submitted_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL,
            revisions JSON NOT NULL DEFAULT '[]',
            comments JSON NOT NULL DEFAULT '[]',
            eid TEXT NULL,
            import_id TEXT NULL,
            source TEXT NOT NULL DEFAULT 'SYSTEM'
        );
    "#,
    r#"
        CREATE TABLE alarms (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL DEFAULT 'New alarm',
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            description TEXT NULL,
            version INTEGER NOT NULL DEFAULT 1,
            definition JSON NOT NULL DEFAULT '{}',
            workflow JSON NOT NULL DEFAULT '[]',
            permissions JSON NOT NULL DEFAULT '{}',
            gudiance TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE alerts (
            uuid TEXT PRIAMRY KEY NOT NULL,
            alid TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'OPEN',
            eid TEXT NULL,
            alert_date DATE NOT NULL,
            lid TEXT NULL,
            stage TEXT NOT NULL DEFAULT 'VERIFICATION',
            data JSON NOT NULL DEFAULT '{}',
            workflow JSON NOT NULL DEFAULT '[]',
            events JSON NULL,
            raised DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            closed DATETIME NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    #",
    r#"
        CREATE TABLE resources (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            version INTEGER NOT NULL DEFAULT 1,
            description TEXT NULL,
            content_type TEXT NOT NULL DEFAULT 'DASHBOARD',
            shared BOOL NULL,
            layout JSON NOT NULL DEFAULT '[]',
            data JSON NOT NULL DEFAULT '{}',
            style JSON NOT NULL DEFAULT '{}',
            variables JSON NOT NULL DEFAULT '{}',
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE documents (
            uuid TEXT PRIMARY KEY NOT NULL,
            template_name JSON NOT NULL DEFAULT '{}',
            instance_name JSON NOT NULL DEFAULT '{}',
            status TEXT NOT NULL DEFAULT 'DRAFT',
            version INTEGER NOT NULL DEFAULT 1,
            theme TEXT NOT NULL DEFAULT 'DEFAULT',
            layout JSON NOT NULL DEFAULT '[]',
            content TEXT NULL,
            data JSON NOT NULL DEFAULT '{}',
            generation JSON NOT NULL DEFAULT '{}',
            orientation TEXT NOT NULL DEFAULT 'PORTRAIT',
            template_type TEXT NOT NULL DEFAULT 'GENERATED',
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            creaed_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE assignments (
            uuid TEXT PRIMARY KEY NOT NULL,
            uid TEXT NOT NULL,
            lid TEXT NULL,
            fid TEXT NULL,
            assign_type TEXT NOT NULL DEFAULT 'DEFAULT',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL,
        );
    "#,
    r#"
        CREATE TABLE devices (
            uuid TEXT PRIMARY KEY NOT NULL,
            device_type TEXT NOT NULL,
            last_seed DATETIME NULL,
            did TEXT NULL,
            uid TEXT NULL,
            info JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE drafts (
            uuid TEXT PRIMARY KEY NOT NULL,
            fid TEXT NOT NULL,
            data JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE event_log (
            uuid TEXT PRIMARY KEY NOT NULL,
            ref_type TEXT NOT NULL,
            ref_id TEXT NOT NULL,
            ref_ts INTEGER NOT NULL,
            command TEXT NOT NULL,
            data JSON NULL,
            source TEXT NULL,
            ts_ms INTEGER NOT NULL,
            applied BOOL NOT NULL DEFAULT FALSE,
            source_version INTEGER NULL,
            device_id TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE organizations (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            description TEXT NULL,
            acronym TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL,
        );
    "#,
    r#"
        CREATE TABLE roles (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            description TEXT NULL,
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            forms JSON NOT NULL DEFAULT '[]',
            permissions JSON NOT NULL DEFAULT '{}',
            inh TEXT NOT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE tasks (
            uuid TEXT PRIMARY KEY NOT NULL,
            status TEXT NOT NULL DEFAULT 'OPEN',
            task_type TEXT NULL,
            data JSON NOT NULL DEFAULT '{}',
            actions JSON NOT NULL DEFAULT '[]',
            version INTEGER NOT NULL DEFAULT 2,
            form JSON NOT NULL DEFAULT '{}',
            context JSON NOT NULL DEFAULT '{}',
            roles JSON NOT NULL DEFAULT '[]',
            location TEXT NULL,
            outcome TEXT NULL,
            actioned DATETIME NULL,
            actioned_by TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
];
