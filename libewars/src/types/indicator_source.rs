use uuid::Uuid;

use crate::types::{Numeric};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SystemIndicator {
    pub uuid: Uuid,
    pub metric: Option<String>,
    pub dimension: Option<String>,
    pub form_id: Option<Uuid>,
    pub alarm_id: Option<Uuid>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum IndicatorSource {
    System(SystemIndicator),
    Standard(Uuid),
    Empty(String),
}
