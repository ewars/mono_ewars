use r2d2::PooledConnection;
use r2d2_postgres::PostgresConnectionManager;

pub type PgConnection = PooledConnection<PostgresConnectionManager>;
