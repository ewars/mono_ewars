use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum Label {
    I18N(HashMap<String, String>),
    Standard(String),
}
