mod outbreak;
mod outbreak_create;
mod outbreak_update;

pub use self::outbreak_update::{OutbreakUpdate};
pub use self::outbreak_create::{OutbreakCreate};
pub use self::outbreak::{Outbreak};
