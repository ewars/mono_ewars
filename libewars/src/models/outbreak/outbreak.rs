use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;
use failure::Error;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Outbreak {
    pub uuid: Uuid,
    pub name: String,
    pub description: String,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub locations: Vec<Uuid>,
    pub forms: Vec<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for Outbreak {
    fn from(row: PostgresRow) -> Self {
        Outbreak {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            start_date: row.get("start_date"),
            end_date: row.get("end_date"),
            status: row.get("status"),
            locations: row.get("locations"),
            forms: row.get("forms"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Outbreak {
    fn from(row: &SqliteRow) -> Self {
        Outbreak {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: row.get("name"),
            description: row.get("description"),
            start_date: row.get("start_date"),
            end_date: row.get("end_date"),
            status: row.get("status"),
            locations: opt_value!(row, "locations").unwrap(),
            forms: opt_value!(row, "forms").unwrap(),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO outbreaks
    (uuid, name, description, start_date, end_date, status, locations, forms, created, created_by, modified, modified_by)
    VALUES (:uuid, :name, :description, :start_date, :end_date, :status, :locations, :forms, :created, :created_by, :modified, :modified_by);
"#;

impl Insertable<&SqliteConnection> for Outbreak {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute_named(&[
            (":uuid", &self.uuid.to_string() as &ToSql),
            (":name", &self.name),
            (":description", &self.description),
            (":start_date", &self.start_date),
            (":end_date", &self.end_date),
            (":status", &self.status),
            (":locations", &json::to_value(&self.locations).unwrap()),
            (":forms", &json::to_value(&self.forms).unwrap()),
            (":created", &self.created),
            (":created_by", &in_opt_uuid!(&self.created_by)),
            (":modified", &self.modified),
            (":modified_by", &in_opt_uuid!(&self.modified_by)),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                bail!("{:?}", err);
            }
        }
    }
}
