use std::collections::HashMap;

use serde_json::Value;
use serde_json as json;
use uuid::Uuid;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use postgres::rows::{Row as PgRow};

use crate::types::{PgConnection};

#[derive(Debug, Serialize, Deserialize)]
pub struct LocationTreeNode {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub lti: Uuid,
    pub lti_name: HashMap<String, String>,
    pub pid: Option<Uuid>,
    pub pcode: Option<String>,
    pub codes: HashMap<String, String>,
    pub groups: Option<Vec<String>>,
    pub lineage: Vec<Uuid>,
    pub status: String,
    pub child_count: i64,
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for LocationTreeNode {
    fn from(row: &SqliteRow) -> Self {
        let codes: HashMap<String, String> = json::from_value(row.get("codes")).unwrap();
        let groups: Option<Vec<String>> = dbg!(opt_value!(row, "groups"));
        let lineage: Vec<Uuid> = json::from_value(row.get("lineage")).unwrap();
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        let lti_name: Option<HashMap<String, String>> = opt_value!(row, "lti_name");

        let lti_name: HashMap<String, String> = match lti_name {
            Some(res) => res,
            None => HashMap::new(),
        };

        LocationTreeNode {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name,
            lti: opt_uuid!(row, "lti").unwrap(),
            lti_name,
            pid: opt_uuid!(row, "pid"),
            pcode: opt_string!(row, "pcode"),
            codes,
            groups,
            lineage,
            status: row.get("status"),
            child_count: row.get("child_count"),
        }
    }
}

