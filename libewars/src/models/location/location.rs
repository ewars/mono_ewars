use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::{PgConnection};
use crate::traits::{Deletable, Insertable, Queryable};
use crate::models::{Query, QueryResult, EventLogEntry, EventDataType, LocationUpdate, LocationCreate};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PopulationRecord {
    pub uuid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Location {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub status: String,
    pub pcode: String,
    pub codes: HashMap<String, String>,
    pub groups: Option<Vec<String>>,
    pub data: Option<HashMap<String, Value>>,
    pub lti: Uuid,
    pub pid: Option<Uuid>,
    pub lineage: Vec<Uuid>,
    pub organizations: Option<Vec<Uuid>>,
    pub geometry_type: String,
    pub geojson: Option<Value>,
    pub population: Option<HashMap<Uuid, PopulationRecord>>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lti_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parent_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fqdn: Option<Vec<HashMap<String, String>>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub child_count: Option<i64>,
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for Location {
    fn from(row: PostgresRow) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        let codes: HashMap<String, String> = json::from_value(row.get("codes")).unwrap();
        Location {
            uuid: row.get("uuid"),
            name,
            status: row.get("status"),
            pcode: row.get("pcode"),
            codes,
            groups: row.get("groups"),
            data: opt_field_json!(row, "data"),
            lti: row.get("lti"),
            pid: row.get("pid"),
            lineage: row.get("lineage"),
            organizations: row.get("organizations"),
            geometry_type: row.get("geometry_type"),
            geojson: row.get("geojson"),
            population: opt_field_json!(row, "population"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
            lti_name: opt_field_json!(row, "lti_name"),
            parent_name: opt_field_json!(row, "parent_name"),
            fqdn: opt_field_json!(row, "fqdn"),
            child_count: opt_field!(row, "child_count"),
        }
    }
}

#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Location {
    fn from(row: &SqliteRow) -> Self {
        Location {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: opt_value!(row, "name").unwrap(),
            status: row.get("status"),
            pcode: row.get("pcode"),
            codes: opt_value!(row, "codes").unwrap_or(HashMap::new()),
            groups: opt_value!(row, "groups"),
            data: opt_value!(row, "data"),
            lti: opt_uuid!(row, "lti").unwrap(),
            pid: opt_uuid!(row, "pid"),
            lineage: opt_value!(row, "lineage").unwrap(),
            organizations: opt_value!(row, "organizations"),
            geometry_type: row.get("geometry_type"),
            geojson: row.get("geojson"),
            population: opt_value!(row, "population"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
            lti_name: opt_value!(row, "lti_name"),
            parent_name: opt_value!(row, "parent_name"),
            fqdn: opt_value!(row, "fqdn"),
            child_count: opt_int!(row, "child_count"),
        }
    }
}


static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO locations
    (uuid, name, status, pcode, codes, groups, data, lti, pid, lineage, organizations, geometry_type, geojson, population, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18);
"#;

static SQLITE_GET_BY_ID: &'static str = r#"
    SELECT * FROM locations WHERE uuid = :uuid;
"#;

impl Insertable<&SqliteConnection> for Location {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;
        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &json::to_value(&self.name).unwrap(),
            &self.status,
            &self.pcode,
            &json::to_value(&self.codes).unwrap(),
            &json::to_value(&self.groups).unwrap(),
            &in_opt_value!(&self.data),
            &self.lti.to_string(),
            &in_opt_uuid!(&self.pid),
            &json::to_value(&self.lineage).unwrap(),
            &in_opt_value!(&self.organizations),
            &self.geometry_type,
            &self.geojson,
            &in_opt_value!(&self.population),
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => Err(format_err!("{:?}", err))
        }

    }
}

static PG_DELETE_LOCATION: &'static str = r#"
    DELETE FROM {SCHEMA}.locations
    WHERE uuid = $1;
"#;

static PG_INSERT_LOCATION: &'static str = r#"
    INSERT INTO {SCHEMA}.locations
    (name, status, pcode, codes, groups, data, lti, pid, lineage, organizations, geometry_type, geojson, population, created, created_by, modified, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18) RETURNING *;
"#;

static PG_GET_BY_ID: &'static str = r#"
    SELECT * FROM {SCHEMA}.locations WHERE uuid = $1;
"#;


#[cfg(feature = "with-postgres")]
impl Queryable<&PgConnection> for Location {
    type Item = Location;
    type Query = Query;
    type QueryResult = QueryResult;

    fn get_by_id(conn: &PgConnection, tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        let result: Option<Location> = pg_get_row!(conn, PG_GET_BY_ID, Location, tki, &[&id]);
        if let Some(c) = result {
            Ok(c)
        } else {
            bail!("ERR_NOT_FOUND");
        }
    }

}

static SQL_GET_BY_ID: &'static str = r#"
    SELECT * FROM locations WHERE uuid = ?;
"#;

#[cfg(feature = "with-sqlite")]
impl Queryable<&SqliteConnection> for Location {
    type Item = Location;
    type Query = Query;
    type QueryResult = QueryResult;

    fn get_by_id(conn: &SqliteConnection, _tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        match conn.query_row(&SQL_GET_BY_ID, &[&id.to_string() as &ToSql], |row| {
            Location::from(row)
        }) {
            Ok(res) => Ok(Some(res)),
            Err(err) => bail!(err)
        }

    }
}


impl Location {
    pub fn ingest_event(&mut self, data: &EventLogEntry) -> Result<(), Error> {
        if let Some(d) = &data.data {
            match d {
                EventDataType::Partial(c) => {
                    for item in c {
                        match item.0.as_ref() {
                            "name" => self.name = json::from_value(item.1.clone()).unwrap(),
                            "status" => self.status = item.1.clone().to_string(),
                            "pcode" => self.pcode = item.1.clone().to_string(),
                            "codes" => self.codes = json::from_value(item.1.clone()).unwrap(),
                            "groups" => self.groups = json::from_value(item.1.clone()).unwrap(),
                            "data" => {},
                            _ => {}
                        }
                    }
                },
                _ => {}
            }
        }

        Ok(())
    }
}


