use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

use rusqlite::{Connection as SqliteConnection, Row as SqliteRow};

pub struct LocationSynopsis {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub fqdn: Option<Vec<HashMap<String, String>>>,
    pub lti: Uuid,
    pub lti_name: HashMap<String, String>,
    pub lineage: Vec<Uuid>,
    pub status: String,
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for LocationSynopsis {
    fn from(row: &SqliteRow) -> Self {
        LocationSynopsis {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: opt_value!(row, "name").unwrap(),
            fqdn: opt_value!(row, "fqdn"),
            lti: opt_uuid!(row, "lti").unwrap(),
            lti_name: opt_value!(row, "lti_name").unwrap(),
            lineage: opt_value!(row, "lineage").unwrap(),
            status: row.get("status"),
        }
    }
}

