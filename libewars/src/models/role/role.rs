use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;
use failure::Error;

use postgres::rows::{Row as PostgresRow};
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Role {
    pub uuid: Uuid,
    pub name: String,
    pub description: String,
    pub status: String,
    pub forms: Vec<Uuid>,
    pub permissions: Option<HashMap<String, Value>>,
    pub inh: String,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}


impl<'a> From<PostgresRow<'a>> for Role {
    fn from(row: PostgresRow) -> Self {
        let permissions: Option<HashMap<String, Value>> = opt_field_json!(row, "permissions");
        Role {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            status: row.get("status"),
            forms: row.get("forms"),
            permissions,
            inh: row.get("inh"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),


            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Role {
    fn from(row: &SqliteRow) -> Self {
        Role {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: row.get("name"),
            description: row.get("description"),
            status: row.get("status"),
            forms: opt_value!(row, "forms").unwrap(),
            permissions: opt_value!(row, "permissions"),
            inh: row.get("inh"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO roles
    (uuid, name, description, status, forms, permissions, inh, created, created_by, modified, modified_by)
    VALUES (:uuid, :name, :description, :status, :forms, :permissions, :inh, :created, :created_by, :modified, :modified_by);
"#;

impl Insertable<&SqliteConnection> for Role {
    fn insert(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute_named(&[
            (":uuid", &self.uuid.to_string()),
            (":name", &self.name),
            (":description", &self.description),
            (":status", &self.status),
            (":forms", &json::to_value(&self.forms).unwrap()),
            (":permissions", &in_opt_value!(&self.permissions)),
            (":inh", &self.inh),
            (":created", &self.created),
            (":created_by", &in_opt_uuid!(&self.created_by)),
            (":modified", &self.modified),
            (":modified_by", &in_opt_uuid!(&self.modified_by)),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

