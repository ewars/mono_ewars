use std::collections::HashMap;
use failure::{Error};

use serde_json::Value;
use serde_json as json;
use chrono::prelude::*;
use uuid::Uuid;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::{ToSql};
use postgres::rows::{Row as PostgresRow};

use crate::models::{Assignment, Query, QueryResult};

use crate::traits::{Insertable};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct User {
    #[serde(rename = "uid")]
    pub uuid: Uuid,
    pub role: String,
    pub permissions: Value,
    pub settings: Value,
    pub profile: Value,
    pub org_id: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub status: String,
    pub name: String,
    pub email: String,
    pub password: String,
    pub accounts: Vec<i32>,
}

impl<'a> From<PostgresRow<'a>> for User {
    fn from(row: PostgresRow) -> Self {
        User {
            uuid: row.get("uuid"),
            role: row.get("role"),
            permissions: row.get("permissions"),
            settings: row.get("settings"),
            profile: row.get("profile"),
            org_id: row.get("org_id"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            status: row.get("status"),
            name: row.get("name"),
            email: row.get("email"),
            password: row.get("password"),
            accounts: row.get("accounts"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for User {
    fn from(row: &SqliteRow) -> Self {
        User {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            role: row.get("role"),
            permissions: json::from_value(row.get("permissions")).unwrap(),
            settings: json::from_value(row.get("settings")).unwrap(),
            profile: json::from_value(row.get("profile")).unwrap(),
            org_id: opt_uuid!(row, "org_id"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            status: row.get("status"),
            name: row.get("name"),
            email: row.get("email"),
            password: row.get("password"),
            accounts: json::from_value(row.get("accounts")).unwrap(),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO users 
    (uuid, name, email, password, role, permissions, settings, profile, org_id, status, accounts, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15);
"#;

impl Insertable<&SqliteConnection> for User {

    fn insert(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &self.name,
            &self.email,
            &self.password,
            &self.role,
            &json::to_value(&self.permissions).unwrap(),
            &json::to_value(&self.settings).unwrap(),
            &json::to_value(&self.profile).unwrap(),
            &in_opt_uuid!(&self.org_id),
            &self.status,
            &json::to_value(&self.accounts).unwrap(),
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }

    }
}


static SQLITE_GET_NAME: &'static str = r#"
    SELECT name || " (" || email || ")" AS name
    FROM users
    WHERE uuid = :uuid;
"#;

static SQLITE_GET_USER_ASSIGNS: &'static str = r#"
    SELECT * FROM assignments WHERE uid = :uid;
"#;

#[cfg(feature = "with-sqlite")]
impl User {
    pub fn get_assignments(conn: &SqliteConnection, uid: &Uuid) -> Result<Vec<Assignment>, Error> {
        let mut stmt = conn.prepare(&SQLITE_GET_USER_ASSIGNS)?;
        let rows = stmt.query_map_named(&[
            (":uid", &uid.to_string())
        ], |row| {
            Assignment::from(row)
        })?;

        Ok(rows.into_iter().map(|x| x.unwrap()).collect())
    }

    pub fn get_assignments_derived(conn: &SqliteConnection, uid: &Uuid) -> Result<Vec<Assignment>, Error> {
        let assigns: Vec<Assignment> = match User::get_assignments(&conn, &uid) {
            Ok(res) => res,
            Err(err) => {
                eprintln!("{:?}", err);
                bail!(err);
            }
        };

        let mut results: Vec<Assignment> = Vec::new();

        for assign in assigns {
            results.extend(assign.expand_assignment(&conn)?);
        }

        Ok(results)
    }

    pub fn get_name(conn: &SqliteConnection, uid: &Uuid) -> Result<String, Error> {
        conn.query_row(&SQLITE_GET_NAME, &[&uid.to_string()], |row| {
            Ok(row.get(0))
        })?
    }
}
