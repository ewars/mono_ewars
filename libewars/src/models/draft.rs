use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;

use postgres::rows::{Row as PostgresRow};
use rusqlite::{Row as SqliteRow};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Draft {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub data: HashMap<String, Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub form_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

impl<'a> From<PostgresRow<'a>> for Draft {
    fn from(row: PostgresRow) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();
        Draft {
            uuid: row.get("uuid"),
            fid: row.get("fid"),
            data,
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            form_name: opt_field_json!(row, "form_name"),
            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Draft {
    fn from(row: &SqliteRow) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();

        Draft {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            fid: opt_uuid!(row, "fid").unwrap(),
            data,
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            form_name: opt_value!(row, "form_name"),
            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

