use serde_json::Value;
use chrono::prelude::*;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Invite {
    pub uuid: Uuid,
    pub email: String,
    pub user_id: Option<Uuid>,
    pub details: Option<Value>,
    pub send: DateTime<Utc>,
    pub expires: DateTime<Utc>,
}
