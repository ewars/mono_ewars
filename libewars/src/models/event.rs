use failure::{Error};
use std::collections::{HashMap};

use serde_json::Value;
use serde_json as json;
use chrono::prelude::*;
use uuid::Uuid;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::{PgConnection};
use crate::traits::{Creatable, EventControls};

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum EventDataType {
    Partial(Vec<(String, Value)>),
    Complete(Value),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventLogEntry {
    pub uuid: Uuid,
    pub event_type: String,
    pub res_type: String,
    pub res_id: Uuid,
    pub did: String,
    pub ts_ms: u32,
    pub data: Option<EventDataType>,
    pub version: i32,
    pub created: DateTime<Utc>,
    pub created_by: Uuid,
    
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for EventLogEntry {
    fn from(row: PostgresRow) -> Self {
        let data: Option<EventDataType> = opt_field_json!(row, "data");
        EventLogEntry {
            uuid: row.get("uuid"),
            event_type: row.get("event_type"),
            res_type: row.get("res_type"),
            res_id: row.get("res_id"),
            did: row.get("did"), 
            ts_ms: row.get("ts_ms"),
            data,
            version: row.get("version"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            creator: opt_field!(row, "creator"),
        }
    }
}

#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for EventLogEntry {
    fn from(row: &SqliteRow) -> Self {
        EventLogEntry {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            event_type: row.get("event_type"),
            res_type: row.get("res_type"),
            res_id: opt_uuid!(row, "res_id").unwrap(),
            did: row.get("did"),
            ts_ms: row.get("ts_ms"),
            data: opt_value!(row, "data"),
            version: row.get("version"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by").unwrap(),
            creator: opt_string!(row, "creator"),
        }
    }
}

static PG_GET_STREAM: &'static str = r#"
    SELECT * FROM {SCHEMA}.event_log
    WHERE res_id = $1
    ORDER BY ts_ms;
"#;

#[cfg(feature = "with-postgres")]
impl EventControls<&PgConnection> for EventLogEntry {
    type Item = EventStream;

    fn get_stream(&self, conn: &PgConnection, tki: &'static str) -> Result<Self::Item, Error> {
        match &conn.query(&PG_GET_STREAM.replace("{SCHEMA}", tki), &[&self.res_id]) {
            Ok(rows) => Ok(EventStream(rows.iter().map(|x| EventLogEntry::from(x)).collect())),
            Err(err) => {
                bail!("{:?}", err);
            }
        }
    }
}

static SQLITE_GET_STREAM: &'static str = r#"
    SELECT * FROM event_log
    WHERE res_id = :uuid
    ORDER BY ts_ms;
"#;

#[cfg(feature = "with-sqlite")]
impl EventControls<&SqliteConnection> for EventLogEntry {
    type Item = EventStream;

    fn get_stream(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {
        let mut stmt = conn.prepare(&SQLITE_GET_STREAM)?;

        let rows = stmt.query_map(&[&self.res_id.to_string() as &ToSql], |row| {
            EventLogEntry::from(row)
        })?;

        let results: EventStream = EventStream(rows.map(|x| x.unwrap()).collect());
        Ok(results)
    }
}

#[derive(Debug, Clone)]
pub struct EventStream(Vec<EventLogEntry>);

impl EventStream {
    pub fn insert_event(&mut self, event: &EventLogEntry) -> Result<(), Error> {
        let event_ids: Vec<Uuid> = self.0.iter().map(|x| x.uuid).collect();
        
        // This event already exists in the stream, we don't need to process it again
        if event_ids.contains(&event.uuid) {
            return Ok(());
        }

        if self.is_tombstoned() {
            // Special case, we need to insert this new item before the timestamp
            let mut events: Vec<EventLogEntry> = Vec::new();
            let mut tomb: Option<EventLogEntry> = Option::None;
            for item in &self.0 {
                if item.event_type == "DELETE" {
                    tomb = Some(item.clone());
                } else {
                    events.push(item.clone());
                }
            }

            events.sort_by(|a, b| a.ts_ms.partial_cmp(&b.ts_ms).unwrap());

            if let Some(c) = tomb {
                events.push(c);
            }

            self.0 = events;

        } else {
            self.0.push(event.clone());
            self.0.sort_by(|a, b| a.ts_ms.partial_cmp(&b.ts_ms).unwrap());
        }
        Ok(())
    }

    pub fn replay(&self) -> Result<EventLogEntry, Error> {
        if self.is_tombstoned() {
            bail!("TOMBSTONE");
        }

        let mut data: HashMap<String, Value> = HashMap::new();

        unimplemented!()
    }

    // Replay the event stream to a specific date
    pub fn replay_to(&self, dt: &Date<Utc>) -> Result<EventLogEntry, Error> {
        unimplemented!()
    }

    pub fn replay_to_version(&self, version: usize) -> Result<EventLogEntry, Error> {
        unimplemented!()
    }

    // Check if the stream has a tombstone in it, if it does we have to take special care
    pub fn is_tombstoned(&self) -> bool {
        for item in &self.0 {
            if item.event_type == "DELETE" {
                return true;
            }
        }

        false
    }
}


