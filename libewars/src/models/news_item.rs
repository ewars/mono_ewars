
use failure::Error;
use serde_json::Value;
use serde_json as json;
use uuid::Uuid;
use chrono::prelude::*;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::{ToSql};
use postgres::rows::{Row as PostgresRow};

use crate::types::{PgConnection};
use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewsItem {
    pub uuid: Uuid,
    pub title: String,
    pub content: String,
    pub status: String,
    pub published: DateTime<Utc>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for NewsItem {
    fn from(row: &SqliteRow) -> Self {
        NewsItem {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            title: row.get("title"),
            content: row.get("content"),
            status: row.get("status"),
            published: row.get("published"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
        }
    }
}

impl<'a> From<PostgresRow<'a>> for NewsItem {
    fn from(row: PostgresRow) -> Self {
        NewsItem {
            uuid: row.get("uuid"),
            title: row.get("title"),
            content: row.get("content"),
            status: row.get("status"),
            published: row.get("published"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO news_items 
    (uuid, title, content, status, published, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9);
"#;

#[cfg(feature = "with-sqlite")]
impl Insertable<&SqliteConnection> for NewsItem {
    fn insert(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &self.title,
            &self.content,
            &self.status,
            &self.published,
            &self.created,
            &in_opt_uuid!(self.created_by),
            &self.modified,
            &in_opt_uuid!(self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                bail!(err);
            }
        }
    }
}
