use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;
use failure::Error;

use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::ToSql;
use postgres::rows::{Row as PostgresRow};

use crate::types::{PgConnection};
use crate::traits::{Insertable};


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct IndicatorGroup {
    pub uuid: Uuid,
    pub name: String,
    pub description: Option<String>,
    pub pid: Option<Uuid>,
    pub permissions: Option<HashMap<String, Value>>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

impl<'a> From<PostgresRow<'a>> for IndicatorGroup {
    fn from(row: PostgresRow) -> Self {
        let permissions: Option<HashMap<String, Value>> = opt_field_json!(row, "permissions");
        
        IndicatorGroup {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            pid: row.get("pid"),
            permissions,
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for IndicatorGroup {
    fn from(row: &SqliteRow) -> Self {
        IndicatorGroup {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: row.get("name"),
            description: row.get("description"),
            pid: opt_uuid!(row, "pid"),
            permissions: opt_value!(row, "permissions"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),

        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO indicator_groups 
    (uuid, name, description, pid, permissions, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9);
"#;

impl Insertable<&SqliteConnection> for IndicatorGroup {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &self.name,
            &self.description,
            &in_opt_uuid!(&self.pid),
            &in_opt_value!(&self.permissions),
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

