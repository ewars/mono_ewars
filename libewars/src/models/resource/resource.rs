use std::collections::HashMap;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;
use serde_json::Value;
use serde_json as json;

use postgres::rows::{Row as PostgresRow};
use rusqlite::{Connection as SqliteConnection, Row as SqliteRow};

use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Resource {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub version: i32,
    pub description: Option<String>,
    pub content_type: String,
    pub shared: Option<bool>,
    pub layout: Option<Value>,
    pub data: Option<Value>,
    pub style: Option<Value>,
    pub variables: Option<Value>,
    pub permissions: Option<HashMap<String, Value>>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}


impl<'a> From<PostgresRow<'a>> for Resource {
    fn from(row: PostgresRow) -> Self {
        let permissions: Option<HashMap<String, Value>> = opt_field_json!(row, "permissions");
        Resource {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            version: row.get("version"),
            description: row.get("description"),
            content_type: row.get("content_type"),
            shared: row.get("shared"),
            layout: row.get("layout"),
            data: row.get("data"),
            style: row.get("style"),
            variables: row.get("variables"),
            permissions,
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            // Optional fields
            creator: match row.get_opt("creator") {
                Some(res) => res.unwrap(),
                None => Option::None
            },
            modifier: match row.get_opt("modifier") {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Resource {
    fn from(row: &SqliteRow) -> Self {
        Resource {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: row.get("name"),
            status: row.get("status"),
            version: row.get("version"),
            description: row.get("description"),
            content_type: row.get("content_type"),
            shared: row.get("shared"),
            layout: opt_value!(row, "layout"),
            data: opt_value!(row, "data"),
            style: opt_value!(row, "style"),
            variables: opt_value!(row, "variables"),
            permissions: opt_value!(row, "permissions"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO resources 
    (uuid, name, status, version, description, content_type, shared, layout, data, style, variables, permissions, created, created_by, modified, modified_by)
    VALUES (:uuid, :name, :status, :version, :description, :content_type, :shared, :layout, :data, :style, :variables, :permissions, :created, :created_by, :modified, :modified_by);
"#;

impl Insertable<&SqliteConnection> for Resource {
    fn insert(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute_named(&[
            (":uuid", &self.uuid.to_string()),
            (":name", &self.name),
            (":status", &self.status),
            (":version", &self.version),
            (":description", &self.description),
            (":content_type", &self.content_type),
            (":shared", &self.shared),
            (":layout", &self.layout),
            (":data", &self.data),
            (":style", &self.style),
            (":variables", &self.variables),
            (":permissions", &in_opt_value!(&self.permissions)),
            (":created", &self.created),
            (":created_by", &in_opt_uuid!(&self.created_by)),
            (":modified", &self.modified),
            (":modified_by", &in_opt_uuid!(&self.modified_by)),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}
