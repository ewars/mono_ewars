use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::types::PgConnection;
use crate::models::{Document};
use crate::traits::{Creatable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DocumentCreate {
    pub template_name: Value,
    pub instance_name: Value,
    pub status: String,
    pub theme: Option<String>,
    pub layout: Option<Value>,
    pub content: Option<String>,
    pub data: Option<Value>,
    pub generation: Value,
    pub orientation: String,
    pub template_type: String,
    pub permissions: Value,
}

static PG_CREATE: &'static str = r#"
    INSERT INTO {SCHEMA}.documents 
    (template_name, instance_name, status, theme, layout, content, data, generation, orientation, template_type, permissions, created_by, modified_by)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
"#;

static PG_CREATE_RETURNING: &'static str = r#"
    INSERT INTO {SCHEMA}.documents 
    (template_name, instance_name, status, theme, layout, content, data, generation, orientation, template_type, permissions, created_by, modified_by)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING *;
"#;

impl Creatable<&PgConnection> for DocumentCreate {
    type Item = Document;

    fn create(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_CREATE.replace("{SCHEMA}", tki), &[
            &self.template_name,
            &self.instance_name,
            &self.status,
            &self.theme,
            &self.layout,
            &self.content,
            &self.data,
            &self.generation,
            &self.orientation,
            &self.template_type,
            &self.permissions,
            &uid,
            &uid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err),
        }
    }

    fn create_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_CREATE_RETURNING.replace("{SCHEMA}", tki), &[
            &self.template_name,
            &self.instance_name,
            &self.status,
            &self.theme,
            &self.layout,
            &self.content,
            &self.data,
            &self.generation,
            &self.orientation,
            &self.template_type,
            &self.permissions,
            &uid,
            &uid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Document::from(row))
                } else {
                    bail!("ERR");
                }
            },
            Err(err) => bail!(err)
        }
    }
}
