use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;


#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum CmpVal {
    Str(String),
    Date(NaiveDate),
    Numeric(i32),
    Float(f64),
    Id(Uuid),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Filter {
    pub key: String,
    pub cmp: String,
    pub val_type: String,
    pub value: Option<Value>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Order {
    pub key: String,
    pub val_type: String,
    pub dir: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Query {
    pub filters: Vec<Filter>,
    pub orders: Vec<Order>,
    pub limit: i32,
    pub offset: i32,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordsQuery {
    pub filters: Vec<Filter>,
    pub orders: Vec<Order>,
    pub limit: i32,
    pub offset: i32,
    pub fid: Uuid,
    pub param: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct QueryResult {
    pub results: Vec<Value>,
    pub count: i32,
}

