use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;

use failure::Error;
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use postgres::rows::{Row as PostgresRow};

use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Organization {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub description: Option<String>,
    pub acronym: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for Organization {
    fn from(row: PostgresRow) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        Organization {
            uuid: row.get("uuid"),
            name,
            description: row.get("description"),
            acronym: row.get("acronym"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),


            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Organization {
    fn from(row: &SqliteRow) -> Self {
        Organization {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: opt_value!(row, "name").unwrap(),
            description: row.get("description"),
            acronym: row.get("acronym"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO organizations 
    (uuid, name, description, acronym, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8);
"#;

impl Insertable<&SqliteConnection> for Organization {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &json::to_value(&self.name).unwrap(),
            &self.description,
            &self.acronym,
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                bail!("{:?}", err);
            }
        }
    }
}

