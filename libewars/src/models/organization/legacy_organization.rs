use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::types::ToSql;

use crate::models::{Organization};
use crate::utils::*;
use crate::traits::{Upgrade};

#[derive(Debug, Deserialize)]
pub struct LegacyOrganizaton(HashMap<String, Value>);

impl Upgrade<&SqliteConnection>  for LegacyOrganization {
    fn upgrade(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {

    }
}
