use std::collections::HashMap;

use failure::Error;
use chrono::prelude::*;
use serde_json::Value;
use serde_json as json;
use uuid::Uuid;


use crate::traits::{Updatable};
use crate::types::{PgConnection};
use crate::models::alarm::{Alarm, AlarmDefinition, AlarmStage};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlarmUpdate {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub description: String,
    pub definition: AlarmDefinition,
    pub workflow: Option<Vec<AlarmStage>>,
    pub permissions: Option<HashMap<String, Value>>,
    pub guidance: Option<String>,
}

static PG_UPDATE: &'static str = r#"
    UPDATE {SCHEMA}.alarms 
        set name = ?,
            status = ?,
            description = ?,
            definition = ?,
            workflow = ?,
            permissions = ?,
            guidance = ?,
            modified = NOW(),
            modified_by = ?
        WHERE uuid = ?;
"#;

static PG_UPDATE_RETURNING: &'static str = r#"
    UPDATE {SCHEMA}.alarms 
        set name = ?,
            status = ?,
            description = ?,
            definition = ?,
            workflow = ?,
            permissions = ?,
            guidance = ?,
            modified = NOW(),
            modified_by = ?
        WHERE uuid = ? RETURNING *;
"#;

#[cfg(feature = "with-postgres")]
impl Updatable<&PgConnection> for AlarmUpdate {
    type Item = Alarm;

    fn update(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_UPDATE.replace("{SCHEMA}", &tki), &[
            &self.name,
            &self.status,
            &self.description, 
            &json::to_value(&self.definition).unwrap(),
            &json::to_value(&self.workflow).unwrap(),
            &json::to_value(&self.permissions).unwrap(),
            &self.guidance,
            &uid,
            &self.uuid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }

    fn update_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_UPDATE_RETURNING.replace("{SCHEMA}", &tki), &[
            &self.name,
            &self.status,
            &self.description, 
            &json::to_value(&self.definition).unwrap(),
            &json::to_value(&self.workflow).unwrap(),
            &json::to_value(&self.permissions).unwrap(),
            &self.guidance,
            &uid,
            &self.uuid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Alarm::from(row))
                } else {
                    bail!("ERR_NOT_FOUND");
                }
            },
            Err(err) => bail!(err)
        }
    }
}
