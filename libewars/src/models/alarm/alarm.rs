use std::collections::HashMap;

use failure::{Error};
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row as PostgresRow};
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::types::{PgConnection, Numeric, IndicatorSource};
use crate::utils::*;
use crate::models::{Query, QueryResult};
use crate::traits::{Insertable, Queryable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlarmDefinition {
    pub sti: Option<Uuid>,
    pub loc_id: Option<Uuid>,
    pub ds_type: Option<String>,
    pub loc_spec: Option<String>,
    pub ad_period: Option<Numeric>,
    pub ad_enabled: Option<bool>,
    pub crit_floor: Option<Numeric>,
    pub crit_value: Option<Numeric>,
    pub crit_source: String,
    pub ds_interval: Option<String>,
    pub ds_modifier: Option<Numeric>,
    pub inv_enabled: Option<bool>,
    pub crit_formula: Option<String>,
    pub crit_series: Option<Vec<Value>>,
    pub crit_sd_spec: Option<String>,
    pub crit_ed_spec: Option<String>,
    pub ds_agg_lower: Option<bool>,
    pub ds_aggregate: Option<String>,
    pub ds_indicator: Option<IndicatorSource>,
    pub inv_form_ids: Option<Vec<Uuid>>,
    pub loc_restrict: Option<bool>,
    pub monitor_type: Option<String>,
    pub sti_restrict: Option<bool>,
    pub crit_modifier: Option<Numeric>,
    pub crit_indicator: Option<IndicatorSource>,
    pub crit_reduction: Option<String>,
    pub sti_agg_rollup: Option<bool>,
    pub crit_comparator: Option<String>,
    pub ds_interval_type: Option<String>,
    pub crit_ed_intervals: Option<Numeric>,
    pub crit_sd_intervals: Option<Numeric>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlarmStage {}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Alarm {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub version: i32,
    pub definition: AlarmDefinition,
    pub workflow: Option<HashMap<String, AlarmStage>>,
    pub permissions: Option<HashMap<String, Value>>,
    pub guidance: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for Alarm {
    fn from(row: PostgresRow) -> Self {
        let definition: AlarmDefinition = json::from_value(row.get("definition")).unwrap();
        let workflow: Option<HashMap<String, AlarmStage>> = opt_field_json!(row, "workflow");
        let permissions: Option<HashMap<String, Value>> = opt_field_json!(row, "permissions");

        Alarm {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            version: row.get("version"),
            definition,
            workflow,
            permissions,
            guidance: row.get("guidance"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
        }
    }
}

#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Alarm {
    fn from(row: &SqliteRow) -> Self {
        let definition: AlarmDefinition = json::from_value(row.get("definition")).unwrap();
        let workflow: Option<HashMap<String, AlarmStage>> = opt_value!(row, "workflow");
        let permissions: Option<HashMap<String, Value>> = opt_value!(row, "permissions");

        Alarm {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            version: row.get("version"),
            definition,
            workflow,
            permissions,
            guidance: row.get("guidance"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            creator: opt_string!(row, "creator"),
            modifier: opt_string!(row, "modifier"),
        }
    }
}

static SQLITE_GET_BY_ID: &'static str = r#"
    SELECT * FROM alarms WHERE uuid = ?;
"#;

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO alarms
    (uuid, name, status, description, version, definition, workflow, permissions, guidance, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13);
"#;

impl Insertable<&SqliteConnection> for Alarm {
    fn insert(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        match conn.execute(&SQLITE_INSERT, &[
            &self.uuid.to_string() as &ToSql,
            &self.name,
            &self.status, 
            &self.description,
            &self.version,
            &json::to_value(&self.definition).unwrap(),
            &in_opt_value!(&self.workflow).unwrap(),
            &in_opt_value!(&self.permissions).unwrap(),
            &self.guidance,
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

impl Queryable<&SqliteConnection> for Alarm {
    type Item = Alarm;
    type Query = Query;
    type QueryResult = QueryResult;

    fn get_by_id(conn: &SqliteConnection, _tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        let mut stmt = conn.prepare(&SQLITE_GET_BY_ID)?;

        match stmt.query_row(&[&id.to_string() as &ToSql], |row| {
            Alarm::from(row)
        }) {
            Ok(res) => Ok(Some(res)),
            Err(err) => bail!(err)
        }
    }
}

