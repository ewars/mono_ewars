use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use failure::Error;

use crate::models::{Assignment};
use crate::types::PgConnection;
use crate::traits::{Updatable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AssignmentUpdate {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub lid: Option<Uuid>,
    pub assign_type: String,
    pub assign_group: Option<String>,
    pub status: String,
}

static PG_UPDATE: &'static str = r#"
    UPDATE {SCHEMA}.assignments
    SET fid = ?,
        lid = ?,
        assign_type = ?,
        assign_group = ?,
        status = ?,
        modified = NOW(),
        modified_by = ?
    WHERE uuid = ?;
"#;

static PG_UPDATE_RETURNING: &'static str = r#"
    UPDATE {SCHEMA}.assignments
    SET fid = ?,
        lid = ?,
        assign_type = ?,
        assign_group = ?,
        status = ?,
        modified = NOW(),
        modified_by = ?
    WHERE uuid = ? RETURNING *;
"#;


#[cfg(feature = "with-postgres")]
impl Updatable<&PgConnection> for AssignmentUpdate {
    type Item = Assignment;

    fn update(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_UPDATE.replace("{SCHEMA}", &tki), &[
            &self.fid,
            &self.lid,
            &self.assign_type,
            &self.assign_group,
            &self.status,
            &uid,
            &self.uuid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }

    fn update_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_UPDATE_RETURNING.replace("{SCHEMA}", &tki), &[
            &self.fid,
            &self.lid,
            &self.assign_type,
            &self.assign_group,
            &self.status,
            &uid,
            &self.uuid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Assignment::from(row))
                } else {
                    bail!("ERR_RETR_ASSIG");
                }
            },
            Err(err) => bail!(err)
        }
    }
}

