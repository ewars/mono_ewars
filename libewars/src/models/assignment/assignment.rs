use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;

use postgres::rows::{Row as PostgresRow};
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::{ToSql};

use crate::types::{PgConnection};
use crate::traits::{Insertable, Deletable, Updatable, Creatable, Queryable};
use crate::models::{FormFeature, FeatureDefinition, Field, Query, QueryResult};
use crate::types::{SqlitePoolConnection};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Assignment {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub lid: Option<Uuid>,
    pub fid: Option<Uuid>,
    pub status: String,
    pub assign_type: String,
    pub assign_group: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub location_status: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub form_status: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reporting_status: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub start_date: Option<NaiveDate>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub end_date: Option<NaiveDate>,
    
    #[serde(skip_serializing_if = "Option::is_none")]
    pub assignee: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub form_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub definition: Option<HashMap<String, Field>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub features: Option<HashMap<String, Option<FeatureDefinition>>>,
}

#[cfg(feature = "with-sqlite")]
impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Assignment {
    fn from(row: &SqliteRow) -> Self {
        Assignment {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            uid: opt_uuid!(row, "uid").unwrap(),
            lid: opt_uuid!(row, "lid"),
            fid: opt_uuid!(row, "fid"),
            status: row.get("status"),
            assign_type: row.get("assign_type"),
            assign_group: opt_string!(row, "assign_group"),
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),

            location_status: opt_string!(row, "location_status"),
            form_status: opt_string!(row, "form_status"),
            reporting_status: opt_string!(row, "reporting_status"),

            start_date: row.get("start_date"),
            end_date: row.get("end_date"),

            assignee: opt_string!(row, "assignee"),
            creator: opt_string!(row, "assignee"),
            modifier: opt_string!(row, "modifier"),
            form_name: opt_value!(row, "form_name"),
            location_name: opt_value!(row, "location_name"),
            definition: opt_value!(row, "definition"),
            features: opt_value!(row, "features"),
        }
    }
}

#[cfg(feature = "with-postgres")]
impl<'a> From<PostgresRow<'a>> for Assignment {
    fn from(row: PostgresRow) -> Self {
        Assignment {
            uuid: row.get("uuid"),
            uid: row.get("uid"),
            lid: row.get("lid"),
            fid: row.get("fid"),
            status: row.get("status"),
            assign_type: row.get("assign_type"),
            assign_group: row.get("assign_group"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            location_status: opt_field!(row, "location_status"),
            form_status: opt_field!(row, "form_status"),
            reporting_status: opt_field!(row, "reporting_status"),

            start_date: opt_field!(row, "start_date"),
            end_date: opt_field!(row, "end_date"),

            assignee: opt_field!(row, "assignee"),
            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
            form_name: opt_field_json!(row, "form_name"),
            location_name: opt_field_json!(row, "location_name"),
            definition: opt_field_json!(row, "definition"),
            features: opt_field_json!(row, "features"),
        }
    }
}

static SQLITE_DELETE: &'static str = r#"
    DELETE FROM assignments WHERE uuid = :uuid;
"#;

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO assignments 
    (uuid, uid, status, lid, fid, assign_type, assign_group, created, created_by, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11);
"#; 

static SQLITE_DELETE_BY_ID: &'static str = r#"
    DELETE FROM assignments WHERE uuid = :uuid;
"#;

static SQLITE_GET_BY_ID: &'static str = r#"
    SELECT * FROM assignments WHERE uuid = ?1;
"#;

#[cfg(feature = "with-sqlite")]
impl Insertable<&SqlitePoolConnection> for Assignment {
    fn insert(&self, conn: &SqlitePoolConnection, _tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &self.uid.to_string(),
            &self.status,
            &in_opt_uuid!(&self.lid),
            &in_opt_uuid!(&self.fid),
            &self.assign_type,
            &self.assign_group,
            &self.created,
            &in_opt_uuid!(&self.created_by),
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

impl Deletable<&SqliteConnection> for Assignment {
    fn delete(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<(), Error> {
        match conn.execute(&SQLITE_DELETE_BY_ID, &[&self.uuid.to_string()]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }

    fn delete_by_id(conn: &SqliteConnection, _tki: &'static str, id: &Uuid) -> Result<(), Error> {
        match conn.execute(&SQLITE_DELETE_BY_ID, &[&id.to_string()]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }


}

// Base implementation
impl Assignment {
    // Get report dates that are expected for this assignment
    pub fn get_expected_dates(&self) -> Result<Vec<Date<Utc>>, Error> {
        unimplemented!()
    }

    // Get the location id from this
    pub fn lid(&self) -> Uuid {
        match self.lid {
            Some(res) => res.clone(),
            None => {
                panic!("ERR_RETR_LID");
            }
        }
    }

    // Get the location type from the form that this should be reporting for
    pub fn get_location_type(&self) -> Option<Uuid> {
        if let Some(def) = &self.definition {
            if let Some(c) = def.get("__lid__") {
                if let Some(lti) = c.lti {
                    Some(lti.clone())
                } else {
                    Option::None
                }
            } else {
                Option::None
            }
        } else {
            Option::None
        }
    }
}

#[cfg(feature = "with-postgres")]
impl Assignment {

}

static SQLITE_GET_UNDERS: &'static str = r#"
    SELECT l.uuid
    FROM locations AS l
    WHERE l.lineage LIKE :uuid
        AND l.lti = :lti_id
"#;

static SQLITE_GET_USER_ASSIGNS: &'static str = r#"
    SELECT a.*,
        f.features AS features,
        l.name AS location_name,
        f.name AS form_name,
        l.status AS location_status,
        f.status AS form_status,
        a.status AS assign_status,
        rp.status AS reporting_status
        rp.start_date AS start_date,
        rp.end_date AS end_date
    FROM assignments AS a
        LEFT JOIN forms AS f ON f.uuid = a.fid
        LEFT JOIN locations AS l ON l.uuid = a.lid
        LEFT JOIN reporting AS rp ON rp.lid = a.lid AND rp.fid = f.uuid
    WHERE a.uid = :uid
        AND f.status IN ('ACTIVE', 'ARCHIVED');
"#;

static SQLITE_GET_GROUP_LOCATIONS: &'static str = r#"
    SELECT l.uuid
    FROM locations AS l 
    WHERE l.gropus LIKE :group
        AND l.lti = :lti_uuid;
"#;

#[cfg(feature = "with-sqlite")]
impl Assignment {
    pub fn get_lti(&self) -> Option<Uuid> {
        if let Some(def) = &self.definition {
            if let Some(c) = def.get("__lid__") {
                if let Some(lti) = c.lti {
                    Some(lti.clone())
                } else {
                    Option::None
                }
            } else {
                Option::None
            }
        } else {
            Option::None
        }
    }

    // Get all of a users assignments
    // Expand a GROUP, UNDER assignment to a concrete list of locations
    pub fn expand_assignment(&self, conn: &SqliteConnection) -> Result<Vec<Assignment>, Error> {
        let assign_type: String = self.assign_type.clone();

        let lti: Option<Uuid> = self.get_lti();
        if lti.is_none() {
            bail!("ERR_NO_LTI");
        } 
        let lti: Uuid = lti.unwrap();

        let mut locations: Vec<Uuid> = Vec::new();

        let mut new_assigns: Vec<Assignment> = Vec::new();

        match assign_type.as_ref() {
            "UNDER" => {
                let mut stmt = conn.prepare(&SQLITE_GET_UNDERS)?;
                let lid: String = self.lid.clone().unwrap().to_string();
                let rows = stmt.query_map_named(&[
                   (":uuid", &lid),
                   (":lti_id", &lti.to_string()),
                ], |row| {
                    let data: String = row.get(0);
                    Uuid::parse_str(&data).unwrap()
                })?;

                locations = rows.into_iter().map(|x| x.unwrap()).collect();
            },
            "GROUP" => {
                let group: String = self.assign_group.clone().unwrap();
                let mut stmt = conn.prepare(&SQLITE_GET_GROUP_LOCATIONS)?;
                let rows = stmt.query_map_named(&[
                    (":group", &group),
                    (":lti_uuid", &lti.to_string()),
                ], |row| {
                    let data: String = row.get(0);
                    Uuid::parse_str(&data).unwrap()
                })?;

                locations = rows.into_iter().map(|x| x.unwrap()).collect();

            },
            _ => {
                // It's a default assignment, just swap back in the location
                locations.push(self.lid.clone().unwrap());
            }
        }

        for loc in locations {
            let mut new_assign = self.clone();
            new_assign.assign_type = "DEFAULT".to_string();
            new_assign.lid = Some(loc.clone());
            new_assigns.push(new_assign.clone());
        }

        Ok(new_assigns)
    }
}

static PG_DELETE: &'static str = r#"
    DELETE FROM {SCHEMA}.assignments WHERE uuid = ?;
"#;

impl Deletable<&PgConnection> for Assignment {
    fn delete_by_id(conn: &PgConnection, tki: &'static str, id: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_DELETE.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }
}   

static PG_BY_ID: &'static str = r#"
    SELECT * FROM {SCHEMA}.assignments WHERE uuid = ?;
"#;

static PG_QUERY_ASSIGNMENTS: &'static str = r#"
    SELECT r.*,
        f.name AS form_name,
        l.name AS location_name,
        aa.name || ' ' || aa.email AS assignee,
        uu.name || ' ' || uu.email as creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.forms AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
        LEFT JOIN {SCHEMA}.users AS aa ON aa.uuid = r.uid
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
"#;

static PG_COUNT_ASSIGNMENTS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.forms AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
        LEFT JOIN {SCHEMA}.users AS aa ON aa.uuid = r.uid
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
    
"#;

#[cfg(feature = "with-postgres")]
impl Queryable<&PgConnection> for Assignment {
    type Item = Assignment;
    type Query = Query;
    type QueryResult = QueryResult;

    fn query(conn: &PgConnection, tki: &'static str, query: &Self::Query) -> Result<Self::QueryResult, Error> {
        query_fn!(conn, query, PG_QUERY_ASSIGNMENTS, PG_COUNT_ASSIGNMENTS, tki, Assignment)
    }

    fn get_by_id(conn: &PgConnection, tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        match conn.query(&PG_BY_ID.replace("{SCHEMA}", &tki), &[&id.to_string()]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Some(Assignment::from(row)))
                } else {
                    Ok(Option::None)
                }
            },
            Err(err) => bail!(err)
        }
    }

}
