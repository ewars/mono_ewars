use std::collections::HashMap;

use chrono::prelude::*;
use uuid::Uuid;
use serde_json::Value;
use serde_json as json;
use failure::Error;

use postgres::rows::{Row as PostgresRow};
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::traits::{Insertable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ReportingPeriod {
    pub uuid: Uuid,
    pub lid: Uuid,
    pub fid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub pid: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub form_name: Option<HashMap<String, String>>,
}


impl<'a> From<PostgresRow<'a>> for ReportingPeriod {
    fn from(row: PostgresRow) -> Self {
        ReportingPeriod {
            uuid: row.get("uuid"),
            lid: row.get("lid"),
            fid: row.get("fid"),
            start_date: row.get("start_date"),
            end_date: row.get("end_date"),
            status: row.get("status"),
            pid: row.get("pid"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),

            creator: opt_field!(row, "creator"),
            modifier: opt_field!(row, "modifier"),
            location_name: opt_field_json!(row, "location_name"),
            form_name: opt_field_json!(row, "form_name"),
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO reporting
    (uuid, lid, fid, start_date, end_date, status, pid, created, created_by, modified, modified_by)
    VALUES (:uuid, :lid, :fid, :start_date, :end_date, :status, :pid, :created, :created_by, :modified, :modified_by);
"#;

impl Insertable<&SqliteConnection> for ReportingPeriod {
    fn insert(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute_named(&[
            (":uuid", &self.uuid.to_string()),
            (":lid", &self.lid.to_string()),
            (":fid", &self.fid.to_string()),
            (":start_date", &self.start_date),
            (":end_date", &self.end_date),
            (":status", &self.status),
            (":pid", &in_opt_uuid!(&self.pid)),
            (":created", &self.created),
            (":created_by", &in_opt_uuid!(&self.created_by)),
            (":modified", &self.modified),
            (":modified_by", &in_opt_uuid!(&self.modified_by)),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}
