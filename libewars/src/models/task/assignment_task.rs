use uuid::Uuid;
use serde_json::Value;
use chrono::prelude::*;
use serde_json as json;

use failure::{Error};

use rusqlite::{Connection as SqliteConnection};

use crate::traits::{Creatable};
use crate::models::{User, Assignment, Task, NewTask, TaskContext, Location, LocationSynopsis};

#[derive(Debug, Serialize, Deseriailze, Clone)]
pub struct AssignmentTaskData {
    pub fid: Option<Uuid>,
    pub uid: Uuid,
    pub lid: Option<Uuid>,
    pub user: String,
    pub location_name: Option<String>,
    pub location_fqdn: Option<String>,
    pub form_name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AssignmentRequest {
    pub fid: Uuid,
    pub lid: Option<Uuid>,
    pub uid: Uuid,
    pub assign_type: String,
    pub assign_group: Option<String>,
}

impl AssignmentRequest {
    // Does this request have an assignment
    pub fn has_location(&self) -> bool {
        if self.lid.is_some() {
            true 
        } else {
            false
        }
    }
}


#[cfg(feature = "with-sqlite")]
impl Creatable<&SqliteConnection> for AssignmentRequest {
    type Item = Task;

    // Create a new assignment request task
    fn create<A: Into<Option<&String>>>(&self, conn: &SqliteConnection, tki: A, uid: &Uuid) -> Result<(), Error> {

        // TODO: Check if the user already has an assignment as such, even based on an UNDER or
        // GROUP

        let assigns: Vec<Assignment> = User::get_derived_assignments(&conn, &uid)?;

        // Filter to the same form as this assignment request
        let same_form: Vec<Assignment> = assigns.iter().filter(|x| x.fid == self.fid).cloned().collect();
        if let Some(lid) = self.lid {
           let same_loc: Vec<Assignment> = same_form.iter().filter(|x| x.lid() == lid).cloned().collect(); 

           if same_loc.len() > 0 {
               return Err(Error::new(ErrorKind::Other, "ERR_EXISTING_ASSIGN"));
           }
        } else {
            if same_form.len() > 0 {
                return Err(Error::new(ErrorKind::Other, "ERR_EXISTING_ASSIGN"));
            }
        } 


        // Ok, we've checked that they dont' have an existing assignment
        let form: Form = Form::get_by_id(&conn, Option::None, &self.fid)?;

        let mut location: Option<(Uuid, Value)> = Option::None;
        let mut location_name: Option<HashMap<String, String>> = Option::None;
        let mut location_fqdn: Option<String> = Option::None;
        let mut lti_name: Option<HashMap<String, String>> = Option::None;

        // Get information about the location
        if let Some(lid) = self.lid {
            let res: LocationSynopsis = LocationSynopsis::get_by_id(&conn, &lid)?;
            location_name = Some(res.name.clone());
            location_fqdn = Some(res.fqdn.clone());
            lti_name = Some(res.lti_name.clone());
        }

        let task_data: Value = json::to_value(AssignmentTaskData {
            fid: self.fid.clone(),
            uid: uid.clone(),
            lid: self.lid.clone(),
            location_name: location_name,
            location_fqdn: location_fqdn,
            lti_name: lti_name,
            form_name: form.name.clone(),
        }).unwrap();


        let new_task = NewTask {
            uuid: Uuid::new_v4(),
            status: "OPEN".to_string(),
            data: task_data,
            actions: Option::None,
            version: 1,
            form: Option::None,
            context: TaskContext {

            },
            roles: vec!["ACCOUNT_ADMIN", "REGIONAL_ADMIN"],
            location: self.lid.clone(),
        };

        new_task.create(&conn, Option::None)?

        // TODO: Set up event 
    }

}

