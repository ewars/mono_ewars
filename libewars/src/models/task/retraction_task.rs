use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use uuid: Uuid;
use chrono::prelude::*;
use serde_json::Value;
use serde_json as json;

use crate::traits::{Creatable};
use crate::models::{Form, Record, NewTask, Task, User};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RetractionTaskData {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub form_name: HashMap<String, String>,
    pub data: HashMap<String, Value>,
    pub reason: String,
    pub user: String,
    pub record_date: String,
    pub location_name: Option<(HashMap<String, String>, Vec<HashMap<String, String>>)>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct RetractionRequest {
    pub uuid: Uuid,
    pub reason: String,
}

#[cfg(feature = "with-sqlite")]
impl Creatable<&SqliteConnection> for RetractionRequest {
    type Item = Task;

    fn create<A: Into<Option<&String>>>(&self, conn: &SqliteConnection, tki: A, uid: &Uuid) -> Result<(), Error> {
        let record: Record = Record::get_by_id(&conn, Option::None, &self.uuid)?;
        let form: Form = Form::get_by_id(&conn, Option::None, &record.fid)?;

        record.update_status(&conn, "PENDING_RETRACTION")?;

        let mut lid: Option<Uuid> = Option::None;
        let mut location_name: Option<HashMap<String, String>, Vec<HashMap<String, String>>> = Option::None;

        if let Some(c) = record.data.get("__lid__") {
            lid = Some(c.clone());
            if let Some(l) = lid {
                let location: LocationSynopsis = LocationSynopsis::get_by_id(&conn, &l)?;
                location_name = Some((location.name.clone(), location.fqdn.clone()));
            }
        }


        let dd: Date<Utc> = record.get_record_date().unwrap();
        let creator: String = User::get_name(&conn, &uid)?;
        let formatted_date: String = form.format_date(&dd);

        let task_data: Value = json::to_value(RetractionTaskData {
            uuid: &self.uuid.clone(),
            uid: uid.clone(),
            form_name: form.name.clone(),
            data: record.data.clone(),
            user: creator,
            location_name: location_name,
            record_date: formatted_date,
        })?;

        let task_context = TaskContext {
            fid: Some(form.uuid.clone()),
            lid: lid.clone(),
            roles: vec!["ACCOUNT_ADMIN", "REGIONAL_ADMIN"],
        };

        let task = NewTask {
            uuid: Uuid::new_v4(),
            status: "OPEN".to_string(),
            data: task_data,
            actions: Option::None,
            version: 1,
            form: Option::None,
            context: TaskContext {
                fid: Some(form.uuid.clone()),
                lid: lid.clone(),
                roles: vec!["ACCOUNT_ADMIN", "REGIONAL_ADMIN"],
            },
            roles: vec!["ACCOUNT_ADMIN", "REGIONAL_ADMIN"],
            location: lid.clone(),
        };

        task.create(&conn, Option::None, &uid)?

    
    }
}
