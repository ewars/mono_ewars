use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use rusqlite::types::ToSql;
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};

use crate::models::{Alert};
use crate::types::{Numeric};
use crate::utils:*;
use crate::traits::{Upgrade};

#[derive(Debug, Deserialize)]
pub struct LegacyConf(HashMap<String, Value>);

impl Upgrade<&SqliteConnection> for LegacyConf {
    type Item = Conf;

    fn upgrade(&self, conn: &SqliteConnection, _tki: &'static str) -> Result<Self::Item, Error> {
        bail!("unimplemented");
    }
}
