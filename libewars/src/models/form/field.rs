use std::collections::HashMap;

use serde_json::Value;
use uuid::Uuid;

use crate::types::{Label, Numeric};

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum FieldCondition {
    Rule((String, String, String)),
    RuleGroup(Vec<(String, String, String)>),
}


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Field {
    pub uuid: Option<Uuid>,
    #[serde(default = "default_name")]
    pub label: Label,
    pub options: Option<Vec<(String, Label)>>,
    pub required: Option<bool>,
    #[serde(rename = "type")]
    pub field_type: String,
    pub allow_future_dates: Option<bool>,
    pub show_mobile: Option<bool>,
    #[serde(default = "default_order")]
    pub order: Numeric,
    pub help: Option<Label>,
    pub unique: Option<bool>,
    pub conditions: Option<Value>,
    pub conditional_bool: Option<bool>,
    pub interval: Option<String>,
    pub date_type: Option<String>,
    pub min: Option<Numeric>,
    pub max: Option<Numeric>,
    pub multi_select: Option<bool>,
    pub header_style: Option<String>,
    pub fields: Option<HashMap<String, Field>>,
    pub lti: Option<Uuid>,
}

fn default_name() -> Label {
    Label::Standard("Unlabelled".to_string())
}

fn default_field_type() -> String {
    "text".to_string()
}

fn default_order() -> Numeric {
    Numeric::Integer(0)
}

impl Field {
    // Get the display field value for a select field option
    pub fn get_select_display_value(&self, val: &String) -> Option<String> {
        let mut dis_val: Option<Label> = Option::None;

        if let Some(options) = self.options.clone() {
            for item in options.iter() {
                if &item.0 == val {
                    dis_val = Some(item.1.clone());
                }
            }
        }

        if let Some(t) = dis_val {
            match t {
                Label::Standard(c) => Some(c.clone()),
                Label::I18N(c) => {
                    if let Some(en) = c.get("en") {
                        Some(en.clone())
                    } else {
                        Option::None
                    }
                },
                _ => Option::None
            }
        } else {
            Option::None
        }
    }

    // Get the label for the field
    pub fn get_label(&self, lang: &str) -> String {
        match &self.label {
            Label::Standard(c) => c.to_string(),
            Label::I18N(c) => {
                if let Some(d) = c.get(lang) {
                    d.to_string()
                } else {
                    "".to_string()
                }
            },
            _ => "".to_string()
        }
    }

    // Get the order position for this field
    pub fn order(&self) -> i32 {
        match self.order.clone() {
            Numeric::Integer(c) => {
                c as i32
            },
            Numeric::Float(c) => {
                let aint: i32 = c as i32;
                aint
            },
            Numeric::Str(c) => {
                let aint: i32 = c.parse().unwrap();
                aint
            },
            _ => 0
        }
    }

    pub fn get_sub_field(&self, key: &String) -> Option<Field> {
        let path: Vec<String> = key.split(".").map(|x| x.to_string()).collect();

        let row_key: String = path[0].clone();
        let cell_key: String = path[1].clone();

        let mut result: Option<Field> = Option::None;

        if let Some(fields) = &self.fields {
            let row: Option<Field> = match &fields.get(&row_key) {
                Some(res) => Some(res.clone().to_owned()),
                None => Option::None
            };

            if let Some(row) = row {
                if let Some(row_fields) = &row.fields {
                    let cell: Option<Field> = match &row_fields.get(&cell_key) {
                        Some(res) => Some(res.clone().to_owned()),
                        None => Option::None
                    };

                    cell
                } else {
                    Option::None
                }
            } else {
                Option::None
            }
        } else {
            Option::None
        }
    }
}
