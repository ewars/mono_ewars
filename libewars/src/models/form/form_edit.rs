use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;
use rusqlite::{Connection as SqliteConnection};

use crate::types::{PgConnection};
use crate::models::{Form, Field, FormFeature, FormStage, EtlDefinition, FormRevision};
use crate::traits::{Creatable, Updatable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FormEdit {
    pub uuid: Option<Uuid>,
    pub name: HashMap<String, String>,
    pub status: String,
    pub description: String,
    pub guidance: String,
    pub eid_prefix: Option<String>,
    pub features: HashMap<String, FormFeature>,
    pub definition: HashMap<String, Field>,
    pub constraints: Option<Value>,
    pub stages: Option<Vec<FormStage>>,
    pub etl: Option<HashMap<Uuid, EtlDefinition>>,
    pub changes: Vec<FormRevision>,
    pub permissions: Option<HashMap<String, Value>>,
}

static PG_CREATE: &'static str = r#"
    INSERT INTO {SCHEMA}.forms
    (name, status, description, guidance, eid_prefix, features, definition, constraints, stages, etl, changes, permissions, created_by, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14);
"#;

static PG_CREATE_RETURN: &'static str = r#"
    INSERT INTO {SCHEMA}.forms
    (name, status, description, guidance, eid_prefix, features, definition, constraints, stages, etl, changes, permissions, created_by, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14) RETURNING *;
"#;



#[cfg(feature = "with-postgres")]
impl Creatable<&PgConnection> for FormEdit {
    type Item = Form;

    fn create(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_CREATE.replace("{SCHEMA}", tki), &[
            &json::to_value(&self.name).unwrap(),
            &self.status,
            &self.description,
            &self.guidance,
            &self.eid_prefix,
            &json::to_value(&self.features).unwrap(),
            &json::to_value(&self.definition).unwrap(),
            &in_opt_value!(&self.constraints),
            &in_opt_value!(&self.stages),
            &in_opt_value!(&self.etl),
            &json::to_value(&self.changes).unwrap(),
            &in_opt_value!(&self.permissions),
            &uid,
            &uid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }

    fn create_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> { 
        match conn.query(&PG_CREATE_RETURN.replace("{SCHEMA}", tki), &[
            &json::to_value(&self.name).unwrap(),
            &self.status,
            &self.description,
            &self.guidance,
            &self.eid_prefix,
            &json::to_value(&self.features).unwrap(),
            &json::to_value(&self.definition).unwrap(),
            &in_opt_value!(&self.constraints),
            &in_opt_value!(&self.stages),
            &in_opt_value!(&self.etl),
            &json::to_value(&self.changes).unwrap(),
            &in_opt_value!(&self.permissions),
            &uid,
            &uid,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Form::from(row))
                } else {
                    bail!("ERR");
                }
            },
            Err(err) => bail!(err)
        }
    }
}

static PG_UPDATE: &'static str = r#"
    UPDATE %s.forms 
        SET name = ?1,
            status = ?2, 
            description = ?3,
            guidance = ?4,
            eid_prefix = ?5,
            features = ?6,
            definition = ?7,
            constraints = ?8,
            stages = ?9,
            etl = ?10,
            changes = ?11, 
            permissions = ?12, 
            modified_by = ?13,
            modified = NOW()
    WHERE uuid = ?14;
"#;

static PG_UPDATE_RETURN: &'static str = r#"
    UPDATE %s.forms 
        SET name = ?1,
            status = ?2, 
            description = ?3,
            guidance = ?4,
            eid_prefix = ?5,
            features = ?6,
            definition = ?7,
            constraints = ?8,
            stages = ?9,
            etl = ?10,
            changes = ?11, 
            permissions = ?12, 
            modified_by = ?13,
            modified = NOW()
    WHERE uuid = ?14 RETURNING *;
"#;

#[cfg(feature = "with-postgres")]
impl Updatable<&PgConnection> for FormEdit {
    type Item = Form;

    fn update(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        match conn.execute(&PG_UPDATE.replace("{SCHEMA}", tki), &[
            &json::to_value(&self.name).unwrap(),
            &self.status,
            &self.description,
            &self.guidance,
            &self.eid_prefix,
            &json::to_value(&self.features).unwrap(),
            &json::to_value(&self.definition).unwrap(),
            &in_opt_value!(&self.constraints),
            &in_opt_value!(&self.stages),
            &in_opt_value!(&self.etl),
            &json::to_value(&self.changes).unwrap(),
            &in_opt_value!(&self.permissions),
            &uid,
            &self.uuid.unwrap(),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!(err)
        }
    }

    fn update_returning(&self, conn: &PgConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        match conn.query(&PG_UPDATE_RETURN.replace("{SCHEMA}", tki), &[
            &json::to_value(&self.name).unwrap(),
            &self.status,
            &self.description,
            &self.guidance,
            &self.eid_prefix,
            &json::to_value(&self.features).unwrap(),
            &json::to_value(&self.definition).unwrap(),
            &in_opt_value!(&self.constraints),
            &in_opt_value!(&self.stages),
            &in_opt_value!(&self.etl),
            &json::to_value(&self.changes).unwrap(),
            &in_opt_value!(&self.permissions),
            &uid,
            &self.uuid.unwrap(),
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Form::from(row))
                } else {
                    bail!("ERR");
                }
            },
            Err(err) => bail!(err)
        }
    }
}
