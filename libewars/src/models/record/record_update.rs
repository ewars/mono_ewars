use std::io::{ErrorKind};
use std::collections::HashMap;

use failure::{Error};
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use crate::traits::{Updatable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordUpdate {
    pub uuid: Uuid,
    pub data: HashMap<String, Value>,
}

static SQLITE_UPDATE_RECORD: &'static str = r#"
    UPDATE records
        SET 
"#;

#[cfg(feature = "with-sqlite")]
impl Updatable<&SqliteConnection> for RecordUpdate {
    type Item = Record;

    fn update_returning(&self, conn: &SqliteConnection, tki: &'static str, uid: &Uuid) -> Result<Self::Item, Error> {
        unimplemented!()
    }

    fn update(&self, conn: &SqliteConnection, tki: &'static str, uid: &Uuid) -> Result<(), Error> {
        unimplemented!()
    }
}



