use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use failure::Error;
use chrono::prelude::*;
use uuid::Uuid;

use crate::models::{AlarmDefinition, EventLogEntry};
use postgres::rows::{Row as PostgresRow};
use rusqlite::{Row as SqliteRow, Connection as SqliteConnection};
use rusqlite::types::{ToSql};
use rusqlite::{NO_PARAMS};

use crate::models::{Query, QueryResult};

use crate::types::PgConnection;
use crate::utils::*;
use crate::traits::{Insertable, Queryable};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlarmDetails {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub definition: Option<AlarmDefinition>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alarm_description: Option<String>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocationGeometry {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub loc_geom: Option<Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub loc_geom_type: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlertEvent {
    pub uuid: Uuid,
    pub uid: Option<Uuid>,
    pub event_type: String,
    pub transition: Option<Vec<String>>,
    pub reason: Option<String>,
    pub created: NaiveDateTime,
    pub comment: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AlertWorkflowItem {

}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Alert {
    pub uuid: Uuid,
    pub alid: Uuid,
    pub status: String,
    pub eid: Option<String>,
    pub alert_date: Option<NaiveDate>,
    pub lid: Option<Uuid>,
    pub stage: String,
    pub data: HashMap<String, Value>,
    pub workflow: Option<HashMap<Uuid, AlertWorkflowItem>>,
    pub events: Option<Vec<AlertEvent>>,
    pub raised: DateTime<Utc>,
    pub closed: Option<DateTime<Utc>>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub alarm_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location_full_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lti_name: Option<HashMap<String, String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
    #[serde(flatten)]
    pub alarm_details: AlarmDetails,
    #[serde(flatten)]
    pub location_geom: LocationGeometry,
}

impl<'a> From<PostgresRow<'a>> for Alert {
    fn from(row: PostgresRow) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();
        let workflow: Option<HashMap<Uuid, AlertWorkflowItem>> = opt_field_json!(row, "workflow");
        let events: Option<Vec<AlertEvent>> = opt_field_json!(row, "events");

        Alert {
            uuid: row.get("uuid"),
            alid: row.get("uuid"),
            status: row.get("status"),
            eid: row.get("eid"),
            alert_date: row.get("alert_date"),
            lid: row.get("lid"),
            stage: row.get("stage"),
            data,
            workflow,
            events,
            raised: row.get("raised"),
            closed: row.get("closed"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            alarm_name: opt_field_json!(row, "alarm_name"),
            location_name: opt_field_json!(row, "location_name"),
            location_full_name: opt_field_json!(row, "location_full_name"),
            lti_name: opt_field_json!(row, "lti_name"),
            modifier: opt_field!(row, "modifier"),
            alarm_details: AlarmDetails {
                definition: opt_field_json!(row, "alarm_definition"),
                alarm_description: opt_field_json!(row, "alarm_description"),
            },
            location_geom: LocationGeometry {
                loc_geom: opt_field_json!(row, "location_geom"),
                loc_geom_type: opt_field!(row, "geometry_type"),
            }
        }
    }
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for Alert {
    fn from(row: &SqliteRow) -> Self {
        let data: HashMap<String, Value> = opt_value!(row, "data").unwrap();
        let workflow: Option<HashMap<Uuid, AlertWorkflowItem>> = opt_value!(row, "workflow");
        let events: Option<Vec<AlertEvent>> = opt_value!(row, "events");

        Alert {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            alid: opt_uuid!(row, "alid").unwrap(),
            status: row.get("status"),
            eid: row.get("eid"),
            alert_date: row.get("alert_date"),
            lid: opt_uuid!(row, "lid"),
            stage: row.get("stage"),
            data,
            workflow,
            events,
            raised: row.get("raised"),
            closed: row.get("closed"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
            alarm_name: opt_string!(row, "alarm_name"),
            location_name: opt_value!(row, "location_name"),
            location_full_name: opt_string!(row, "location_full_name"),
            lti_name: opt_value!(row, "lti_name"),
            modifier: opt_string!(row, "modifier"),
            alarm_details: AlarmDetails {
                definition: opt_value!(row, "alarm_definition"),
                alarm_description: opt_value!(row, "alarm_description"),
            },
            location_geom: LocationGeometry {
                loc_geom: opt_value!(row, "location_geom"),
                loc_geom_type: opt_string!(row, "geometry_type"),
            }
        }
    }
}

static SQLITE_INSERT: &'static str = r#"
    INSERT OR REPLACE INTO alerts
    (uuid, alid, status, eid, alert_date, lid, stage, data, workflow, events, raised, closed, modified, modified_by)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14);
"#;

impl Insertable<&SqliteConnection> for Alert {
    fn insert(&self, conn: &SqliteConnection, tki: &'static str) -> Result<(), Error> {
        let mut stmt = conn.prepare(&SQLITE_INSERT)?;

        match stmt.execute(&[
            &self.uuid.to_string() as &ToSql,
            &self.alid.to_string(),
            &self.status,
            &self.eid,
            &self.alert_date,
            &in_opt_uuid!(&self.lid),
            &self.stage,
            &json::to_value(&self.data).unwrap(),
            &in_opt_value!(&self.workflow),
            &in_opt_value!(&self.events),
            &self.raised,
            &self.closed,
            &self.modified,
            &in_opt_uuid!(&self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => bail!("{:?}", err)
        }
    }
}

static SQLITE_GET_BY_ID: &'static str = r#"
    SELECT * FROM alerts WHERE uuid = ?;
"#;

impl Queryable<&SqliteConnection> for Alert {
    type Item = Alert;
    type Query = Query;
    type QueryResult = QueryResult;
    
    fn get_by_id(conn: &SqliteConnection, _tki: &'static str, id: &Uuid) -> Result<Option<Self::Item>, Error> {
        let mut stmt = conn.prepare(&SQLITE_GET_BY_ID)?;

        match stmt.query_row(&[&id.to_string() as &ToSql], |row| {
            Alert::from(row)
        }) {
            Ok(res) => Ok(Some(res)),
            Err(err) => bail!(err)
        }
    }

    fn query(conn: &SqliteConnection, _tki: &'static str, query: &Self::Query) -> Result<Self::QueryResult, Error> {
        let mut sql: Vec<String> = vec![
            "SELECT".to_string(),
            "r.*,".to_string(),
            "l.name AS location_name,".to_string(),
            "l.lineage AS lineage,".to_string(),
            "json_extract(sti.name, '$.en') AS sti_name,".to_string(),
            "al.name AS alarm_name,".to_string(),
            "NULL as alarm_workflow".to_string(),
            "FROM alerts AS r".to_string(),
            "LEFT JOIN locations AS l ON l.uuid = r.lid".to_string(),
            "LEFT JOIN location_types AS sti ON sti.uuid = l.lti".to_string(),
            "LEFT JOIN alarms AS al ON al.uuid = r.alid".to_string()
        ];

        let mut sql_count: Vec<String> = vec![
            "SELECT COUNT(*) AS total_records".to_string(),
            "FROM alerts AS r".to_string(),
            "LEFT JOIN locations AS l ON l.uuid = r.lid".to_string(),
            "LEFT JOIN location_types AS sti ON sti.uuid = l.lti".to_string(),
            "LEFT JOIN alarms AS al ON al.uuid = r.alid".to_string()
        ];

        let mut wheres: Vec<String> = Vec::new();
        let mut orderings: Vec<String> = Vec::new();

        for item in &query.filters {
            let mut flt_str: Vec<String> = Vec::new();
            match item.key.as_ref() {
                "submitted" => {
                    flt_str.push("submitter".to_string());
                },
                _ => {
                    if item.key.contains("data.") {
                        flt_str.push(format!("json_extract(r.data, '$.{}')", item.key.replace("data.", "")));
                    } else {
                        flt_str.push(format!("r.{}", item.key));
                    }
                }
            }

            let mut use_val = true;
            let s_cmp = match item.cmp.as_ref() {
                "eq" => "=",
                "neq" => "!=",
                "gt" => ">",
                "gte" => ">=",
                "lt" => "lt",
                "lte" => "lte",
                "null" => {
                    use_val = false;
                    "IS NULL"
                },
                "nnull" => {
                    use_val = false;
                    "IS NOT NULL"
                },
                _ => "=",
            };
            flt_str.push(s_cmp.to_string());

            if use_val {
                if let Some(c) = &item.value {
                    let as_str: String = c.clone().as_str().unwrap().to_string(); 
                    flt_str.push(format!("'{}'", as_str));
                }
            }

            wheres.push(flt_str.join(" "));
        }

        /*
        for item in &query.orders {
            let mut ord_str: Vec<String> = Vec::new();

            match item.key.as_ref() {
                "submitter" => {
                    ord_str.push("submitter".to_string());
                }
                "modifier" => {
                    ord_str.push("modifier".to_string());
                }
                _ => {
                    if item.key.contains("data.") {
                        ord_str.push(format!("json_extract(r.data, '$.{}')", item.key.replace("data.", "")));

                        match item.val_type.as_ref() {
                            "number" => {
                                ord_str.push("::NUMERIC".to_string());
                            }
                            _ => {}
                        }
                    } else {
                        ord_str.push(format!("r.{}", item.key));
                    }
                }
            }

            ord_str.push(format!("{}", item.dir));

            orders.push(ord_str.join(" "));
        }
        */

        if wheres.len() > 0 {
            sql.push("WHERE".to_string());
            sql_count.push("WHERE".to_string());

            let mut sql_f = true;
            for item in wheres {
                if sql_f != true {
                    sql.push("AND".to_string());
                    sql_count.push("AND".to_string());
                }
                sql.push(item.clone());
                sql_count.push(item.clone());
                sql_f = false;
            }
        }


        if orderings.len() > 0 {
            sql.push("ORDER BY".to_string());

            sql.push(orderings.join(", "));
        }

        sql.push(format!("LIMIT {}", query.limit));
        sql.push(format!("OFFSET {}", query.offset));


        let c_sql = sql.join("\n");
        let c_sql_count = sql_count.join("\n");

        eprintln!("{}", c_sql);

        let mut stmt = conn.prepare(&c_sql)?;

        let rows = stmt.query_map(NO_PARAMS, |row| {
            Alert::from(row)
        })?;

        let results: Vec<Value> = rows.map(|x| json::to_value(x.unwrap()).unwrap()).collect();

        let count: i32 = conn.query_row(&c_sql_count, NO_PARAMS, |row| {
            row.get(0)
        })?;

        let wr = QueryResult {
            results,
            count,
        };

        Ok(wr)
    }
}

