#[repr(C)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum ConnectorType {
    Postgres,
    MySQL,
    SQLite,
    HBase,
    JSON,
    Mongo,
    SqlServer,
    Cassandra
}
