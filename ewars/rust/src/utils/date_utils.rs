use std::collections::HashMap;
use failure::Error;
use std::time::{Duration};

use chrono::prelude::*;

lazy_static! {
    static ref MONTH_DAYS: HashMap<u32, u32> = {
        let mut m = HashMap::new();

        m.insert(1, 31);
        m.insert(2, 28);
        m.insert(3, 31);
        m.insert(4, 30);
        m.insert(5, 31);
        m.insert(6, 30);
        m.insert(7, 31);
        m.insert(8, 31);
        m.insert(9, 30);
        m.insert(10, 31);
        m.insert(11, 30);
        m.insert(12, 31);

        m
    }
}

// Get the interval dates between a start and end date
pub fn get_reporting_intervals(start_date: &NaiveDate, end_date: &NaiveDate, interval: String) -> Result<Vec<NaiveDate>, Error> {
    bail!("Unimplemented");
}

// Get the next interval end date from a base date
pub fn get_next_interval(base_date: &NaiveDate, interval: &String) -> Result<NaiveDate, Error> {
    unimplemented!()
}

// Get the previously completed interval from a base_date
pub fn get_prev_interval(base_date: &NaiveDate, interval: &str) -> Result<NaiveDate, Error> {
    unimplemented!()
}

// Get the bounds (start and end date) for an interval type based on a base date
pub fn get_interval_bounds(base_date, &NaiveDate, interval: &str) -> Result<(NaiveDate, NaiveDate), Error> {
    unimplemented!()
}

// Is a date within a given date range
pub fn is_in_period(target_date: &NaiveDate, lower_date: &NaiveDate, upper_date: &NaiveDate) -> Result<bool, Error> {
    unimplemented!()
}

// Is a date one of interval dates
pub fn is_interval(source_date: &NaiveDate, interval: &String) -> Result<bool, Error> {
    unimplemented!()
}

