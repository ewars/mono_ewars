#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import sys
import re
from distutils.sysconfig import get_python_lib
from setuptools import setup, Extension

long_description = (
    "EWARS web and server applications"
)

def get_version(package):
    init_py = open(os.path.join(package, '__init__.py')).read()
    return re.search("__version__ = ['\"]([^'\"]+)['\"]", init_py).group(1)

def get_packages(package):
    return [dirpath for dirpath, dirnames, filenames in os.walk(package) if os.path.exists(os.path.join(dirpath, '__init__.py'))]

if sys.argv[-1] == 'publish':
    if os.system("pip freeze | grep wheel"):
        print("Wheel not installed.\n Use `pip install wheel`.\nExiting.")
        sys.exit()
    if os.system("pip freeze | grep twine"):
        print("twine not installed.\nUse `pip install twine`.\nExiting.")
        sys.exit()

    os.system("python setup.py sdist bdist_wheel")
    us.system("twine upload dist/*")
    print("You probably want to also tag the version now:")
    print("  git tag -a {0} -m 'version {0}'".format(get_version("jdu")))
    print("  git push --tags")
    sys.exist()

def build_native(spec):
    build = spec.add_external_build(
        cmd=["cargo", "build", "--release"],
        path="./rust"
    )

    spec.add_cffi_module(
        module_path="ewars._libsonoma",
        dylib=lambda: build.find_dylib("libsonoma", in_path="target/release"),
        header_filename=lambda: build.find_header('libsonoma.h', in_path="target"),
        rtld_flags=["NOW", "NODELETE"]
    )

setup(
    name="ewars",
    version=get_version("ewars"),
    url="http://ewars-project.org",
    author="Jeff Uren",
    packages=get_packages("ewars"),
    author_email="jduren@protonmail.com",
    long_description=long_description,
    license='Private',
    include_package_data=True,
    python_requires=">=3.7.0",
    setup_requires=['milksnake'],
    install_requires=['milksnake', 'psycopg2', 'tornado'],
    milksnake_tasks=[
        build_native
    ],
    zip_safe=False,
    classifiers=[
        'Development Status :: 1 - Pre-Alpha',
        'Environment :: Web Environment',
        'Framework :: Valhalla',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: WSGI',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)

