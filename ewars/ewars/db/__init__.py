from contextlib import contextmanager

import psycopg2
from psycopg2.extras import RealDictCursor
from psycopg2.pool import ThreadedConnectionPool, SimpleConnectionPool
import asyncpg

from ewars.conf import settings

import tornado
import aiopg
from tornado.ioloop import IOLoop

pool = None
read_pool = None

async_pool = None
async_read_pool = None


class SafePool(asyncpg.pool.Pool):
    """changes the connection acquire implementation to deal with connections
    disconnecting when not in use"""

    async def _acquire_impl(self):
        while True:
            con = await super(SafePool, self)._acquire_impl()
            if con.is_closed():
                await self.release(con)
            else:
                return con


class CustomCursor(psycopg2.extensions.cursor):
    def execute(self, sql, args=None):
        try:
            psycopg2.extensions.cursor.execute(self, sql, args)
        except Exception as exc:
            print(exc)


def create_safe_pool(**connect_kwargs):
    return SafePool(
        min_size=5,
        max_size=60,
        setup=None,
        max_queries=50000,
        loop=None,
        **connect_kwargs
    )


class get_context:
    """ Asynchronouse context manger for asyncpg
    
    """
    __slots__ = ('_args', '_kwargs', '_connection', '_pool')

    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs
        self._connection = None
        self._pool = None

    async def __aenter__(self):
        if self._pool is None:
            try:
                self._pool = await asyncpg.create_pool(
                    database=settings.DB_NAME,
                    user=settings.DB_USER,
                    password=settings.DB_PASSWORD,
                    host=settings.DB_HOST,
                    port=settings.DB_PORT
                )
            except Exception as e:
                print(e)

        self._connection = await self._pool.acquire()
        return self._connection

    async def __aexit__(self, exctype, excval, traceback):
        if traceback is not None:
            print(traceback)
        await self._pool.release(self._connection)
        self._connection = None

class get_db_cur:
    __slots__ = ['_args', '_kwargs', '_connection', '_pool', '_cursor']

    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs
        self._connection = None
        self._cursor = None
        self._pool = None

    async def __aenter__(self):
        # Check if there's a pool available
        if self._pool is None:
            self._pool = await aiopg.create_pool(
                dsn="dbname=%s host=%s user=%s password=%s port=%s" % (
                    settings.DB_NAME,
                    settings.DB_HOST,
                    settings.DB_USER,
                    settings.DB_PASSWORD,
                    settings.DB_PORT,
                ),
                minsize=1,
                maxsize=20
            )

        # Acquire a connection
        self._connection = await self._pool.acquire()
        # Acquire a cursor
        self._cursor = await self._connection.cursor(
            cursor_factory=RealDictCursor
        )

        return self._cursor

    async def __aexit__(self, exctype, excval, traceback):
        # If there's an error there will be a traceback, print it out
        if traceback is not None:
            print(traceback)

        # Release the cursor
        self._cursor.close()
        self._cursor = None
        # Release the connection back to the pool
        await self._pool.release(self._connection)
        self._connection = None


async def create_async_pool(loop=None):
    global async_pool
    async_pool = await asyncpg.create_pool(
        database=settings.DB_NAME,
        user=settings.DB_USER,
        password=settings.DB_PASSWORD,
        host=settings.DB_HOST,
        port=settings.DB_PORT,
        loop=loop
    )

    # async asyncwith async_pool.acquire() as con:
    #     await test(con)

    return async_pool


async def test(con):
    result = await con.fetchval("SELECT 1 ^ 10")
    print(result)

    return


def create_pool():
    global pool
    global read_pool

    if pool is None:
        pool = ThreadedConnectionPool(1, 30,
                                      database=settings.DB_NAME,
                                      user=settings.DB_USER,
                                      password=settings.DB_PASSWORD,
                                      host=settings.DB_HOST,
                                      port=settings.DB_PORT)

    if settings.DB_READ_HOST:
        if read_pool is None:
            read_pool = ThreadedConnectionPool(1, 50,
                                               database=settings.DB_READ_NAME,
                                               user=settings.DB_READ_USER,
                                               password=settings.DB_READ_PASSWORD,
                                               host=settings.DB_READ_HOST,
                                               port=settings.DB_READ_PORT)

    return pool


def get_plain_connect():
    if pool is None:
        create_pool()
    connection = pool.getconn()
    return connection


def release_connect(connection):
    pool.putconn(connection)


@contextmanager
def get_db_connection():
    """Yields a database connections

    Returns:

    """
    connection = None
    try:
        if pool is None:
            create_pool()
        connection = pool.getconn()
        yield connection
    finally:
        pool.putconn(connection)


@contextmanager
def get_read_connection():
    """ Get a read-only connection

    Returns:

    """
    connection = None
    try:
        connection = read_pool.getconn()
        yield connection
    finally:
        read_pool.putconn(connection)


@contextmanager
def get_db_cursor(commit=False, name=None):
    """Yields a database cursor

    Args:
        commit:

    Returns:

    """
    if read_pool is not None:
        if commit is True:
            with get_db_connection() as connection:
                cursor = connection.cursor(cursor_factory=RealDictCursor, name=name)
                try:
                    yield cursor
                    connection.commit()
                finally:
                    cursor.close()
        else:
            with get_read_connection() as connection:
                cursor = connection.cursor(cursor_factory=RealDictCursor, name=name)
                try:
                    yield cursor
                finally:
                    cursor.close()
    else:
        with get_db_connection() as connection:
            cursor = connection.cursor(cursor_factory=RealDictCursor, name=name)
            try:
                yield cursor
                if commit:
                    connection.commit()
            finally:
                cursor.close()
