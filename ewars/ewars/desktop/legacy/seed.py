import brotli
import uuid
import json
import os
from io import StringIO, BytesIO
import socket
import gzip

from ewars.db import get_db_cur, get_db_cursor
from ewars.core.serializers import JSONEncoder
from ewars.conf import settings

from ewars.models import EventLog

from psycopg2.extensions import AsIs


def _get_seed_form_user(form_id, user=None):
    """ Get seed package for a reporting user

    Args:
        form_id:
        user:

    Returns:

    """
    data = StringIO()
    seed_id = None

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    WITH assignments_ AS (
                        SELECT location_id
                        FROM %s.assignments
                        WHERE form_id = %s
                            AND user_id = %s
                            AND status != 'DELETED'
                    )
                    SELECT 
                        uuid,
                        data_date,
                        location_id,
                        form_version_id,
                        data,
                        submitted_date,
                        created_by
                    FROM %s.collections
                    WHERE form_id = %s
                        AND location_id = ANY(SELECT location_id FROM assignments_)
                        AND status = 'SUBMITTED'
                ) as t
            ) TO STDOUT
        """ % (
            AsIs(user.get('tki')),
            form_id,
            user.get('id'),
            user.get('tki'),
            form_id,
        )
        cur.copy_expert(sql, data)

    data.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(data.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, seed_id, end_uri)


def _get_seed_form_geo_admin(form_id, user=None):
    """ Get seed for a geo admin

    Args:
        form_id:
        user:

    Returns:

    """
    data = StringIO()
    seed_id = None

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    WITH locations_ AS (
                        SELECT uuid::TEXT
                        FROM %s.locations 
                        WHERE lineage::TEXT @> ARRAY['%s']::TEXT[]
                        AND status = 'ACTIVE'
                    )
                    SELECT 
                        uuid,
                        data_date,
                        location_id,
                        form_version_id,
                        data,
                        submitted_date,
                        created_by
                    FROM %s.collections
                    WHERE form_id = %s
                    AND location_id = ANY(SELECT uuid FROM locations_)
                    AND status = 'SUBMITTED'
                ) as t
            ) TO STDOUT
        """ % (
            user.get('tki'),
            user.get('location_id'),
            user.get('tki'),
            form_id,
        )
        cur.copy_expert(sql, data)

    data.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(data.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, seed_id, end_uri)


def _get_seed_form(form_id, user=None):
    data = StringIO()

    seed_id = None

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t) 
                FROM (
                SELECT
                    uuid,
                    data_date,
                    location_id,
                    form_version_id,
                    data,
                    submitted_date,
                    created_by
                FROM %s.collections
                WHERE form_id = %s
                AND status = 'SUBMITTED'
                LIMIT 20000
                ) AS t
            ) TO STDOUT
        """ % (
            user.get('tki'),
            form_id,
        )
        cur.copy_expert(sql, data)

    data.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(data.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, seed_id, end_uri)


async def _get_seed_locations(user=None):
    """ Get a seed package for the locations
    within this account
    
    Args:
        user: 

    Returns:

    """
    locations = []

    out = StringIO()

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    SELECT 
                        uuid::TEXT,
                        name,
                        site_type_id,
                        lineage,
                        parent_id::TEXT,
                        status
                    FROM %s.locations
                    WHERE status != 'DELETED'
                ) AS t
            ) TO STDOUT
        """ % (
            AsIs(user.get('tki')),
        )
        cur.copy_expert(sql, out)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    out.seek(0)

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(out.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, None, end_uri)


async def get_subscription_seed(type, id, user=None):
    result = None
    if type == 'FORM':
        if user.get('role') == 'USER':
            result = _get_seed_form_user(id, user=user)
        elif user.get('role') == 'REGIONAL_ADMIN':
            result = _get_seed_form_geo_admin(id, user=user)
        else:
            result = _get_seed_form(id, user=user)

    if type == 'LOCATIONS':
        result = await _get_seed_locations(user=user)

    if type == 'ALARMS':
        result = await _get_seed_alarms(user=user)

    if type == 'ALARM':
        result = await _get_seed_alarm_alerts(id, user=user)

    if type == 'INDICATORS':
        result = await _get_seed_indicators(user=user)

    return result


async def _get_seed_alarm_alerts_user(id, user=None):
    out = StringIO()

    assignments = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.assignments
            WHERE user_id = %s
              AND status != 'DELETED';
        """, (
            AsIs(user.get('tki')),
            user.get('id'),
        ))
        assignments = await cur.fetchall()

    loc_ids = [x.get('uuid') for x in assignments]

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    SELECT *
                    FROM %s.alerts
                    WHERE alarm_id = %s
                    AND location_id IN ARRAY[VALUES('%s')]
                ) AS t
            ) TO STDOUT
        """ % (
            AsIs(user.get('tki')),
            id,
            "'),('".join(str(x) for x in loc_ids),
        )
        cur.copy_expert(sql, out)

    out.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    out.seek(0)

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(out.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, None, end_uri)


async def _get_seed_geom_alarm_alerts(id, user=None):
    """ Get alerts under a geo admin

    Args:
        id:
        user:

    Returns:

    """

    out = StringIO()

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    WITH locations_ AS (
                        SELECT uuid
                        FROM %s.locations
                        WHERE status = 'ACTIVE'
                            AND lineage::TEXT[] @> ARRAY['%s']::TEXT[]
                    )
                    SELECT * 
                    FROM %s.alerts
                    WHERE alarm_id = '%s'
                        AND location_id = ANY(SELECT uuid FROM locations_)
                ) AS t
            ) TO STDOUT
        """ % (
            AsIs(user.get('tki')),
            user.get('location_id'),
            AsIs(user.get('tki')),
            id,
        )
        cur.copy_expert(sql, out)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    out.seek(0)

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(out.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, None, end_uri)


async def _get_seed_alarm_alerts(id, user=None):
    out = StringIO()

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    SELECT * 
                    FROM %s.alerts
                    WHERE alarm_id = '%s'
                ) AS t
            ) TO STDOUT
        """ % (
            AsIs(user.get('tki')),
            id,
        )
        cur.copy_expert(sql, out)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    out.seek(0)

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(out.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, None, end_uri)


async def _get_seed_alarms(user=None):
    """ Get the seed for alarms

    Args:
        user:

    Returns:

    """

    out = StringIO()

    data = None
    with get_db_cursor() as cur:
        cur.execute('''
            SELECT * FROM %s.alarms_v2
        ''', (
            AsIs(user.get('tki')),
        ))
        data = cur.fetchall()

    lines = []
    for item in data:
        if '{' in item.get('ds_indicator', ''):
            item['ds_indicator'] = json.loads(item.get('ds_indicator'))

        lines.append(json.dumps(item, cls=JSONEncoder))

    out.write('\n'.join(lines) + '\n')

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    out.seek(0)

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(out.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, None, end_uri)


async def _get_seed_indicators(user=None):
    """ Get the seed for indicators within the system

    Args:
        user:

    Returns:

    """
    out = StringIO()

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t) 
                FROM (
                    SELECT i.*, g.name 
                    FROM %s.indicators AS i 
                        LEFT JOIN %s.indicator_groups AS g ON g.id = i.group_id 
                    WHERE i.status != 'DELETED'
                ) AS t
            ) TO STDOUT
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
        )

        cur.copy_export(sql, out)

    out.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(out.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, None, end_uri)


async def get_manifest(did, dts, peers, user=None):
    system_ts = await EventLog.get_highest_ts(user=user)
    user_ts = await EventLog.get_user_highest(user.get('id'), did, user=user)

    # Check if we're missing some of this users ts items
    desired_ts = None
    if user_ts < dts:
        desired_ts = dts

    user_rts = []

    for item in peers:
        highest_rts = await EventLog.get_user_highest(item[0], item[1], user=user)
        if item[2] < highest_rts:
            user_rts.append([item[0], item[1], highest_rts])

    return dict(
        rts=system_ts,  # EWARS' current highest ts
        uts=desired_ts,  # EWARS's
        pts=user_rts  # data from remotes peers that we want
    )


async def get_manifest(did, eid, rts, user=None):
    """ Given and event id and TS for a remote user and device,
        figure out if we need anything from that user and if the user needs anything from us.
    
    Args:
        eid: 
        rts: 
        user: 

    Returns:

    """
    # TODO: Load up most recent for users manifest on server
    # TODO: Get subscriptionables
    # TODO: Get most recent events for subscriptions
    # TODO: Figure out what items we need from the user


    # Largest TS
    system_ts = await EventLog.get_highest_ts(user=user)
    user_ts = await EventLog.get_peer_stored_highest(did, user=user)

    return dict(
        rts=system_ts,
        uts=user_ts
    )
