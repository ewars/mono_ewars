from ewars.db import get_db_cur
from psycopg2.extensions import AsIs


async def get_forms_seed(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT f.uuid,
              f.id,
              f.name,
              1 AS version,
              fv.uuid AS version_id,
              fv.definition AS definition,
              '{}'::JSONB AS workflow,
              fv.etl AS etl,
              '[]'::JSONB AS uniques,
              f.features,
              f.eid_prefix,
              f.status,
              '' AS guidance,
              '{}'::JSONB AS changes,
              NOW() AS created,
              1 AS created_by,
              NOW() AS modified,
              1 AS modified_by,
              TRUE as exportable
            FROM %s.forms AS f 
              LEFT JOIN %s.form_versions AS fv on fv.uuid = f.version_id;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_users_seed(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id,
              uuid,
              name,
              email,
              status,
              role,
              location_id,
              123132 AS pin_code,
              accounts,
              '{}'::JSONB AS permissions,
              'en' as lang,
              password,
              org_id, 
              to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS') AS created,
              1 AS created_by,
              to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS') AS modified,
              1 AS created_by,
              to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS') AS registered
             FROM %s.users;
        """, (
            AsIs(user.get('tki')),
        ))
        results = await cur.fetchall()

    return results


async def get_locations_seed(user=None):
    locations = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid,
                  name,
                  status,
                  site_type_id AS sti,
                  parent_id AS pid,
                  pcode,
                  '{}'::JSONB AS codes,
                  lineage,
                  '[]'::JSONB AS reporting,
                  '[]'::JSONB AS population,
                  COALESCE(groups, ARRAY[]::TEXT[]) AS groups,
                  geometry_type,
                  geojson,
                  to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS') AS created,
                  1 AS created_by,
                  to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS') AS modified,
                  1 AS modified_by
            FROM %s.locations;
        """, (
            AsIs(user.get("tki")),
        ))
        locations = await cur.fetchall()

    return locations


async def get_location_types_seed(user=None):
    location_types = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT 
            id, 
            name,
            to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS') AS created,
            1 AS created_by,
            to_char(NOW(), 'YYYY-MM-DD HH24:MI:SS') AS modified,
            1 AS modified_by
            FROM %s.location_types
        """, (
            AsIs(user.get("tki")),
        ))
        location_types = await cur.fetchall()

    return location_types


async def get_indicators_seed(user=None):
    indicators = []
    indicator_groups = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
             FROM %s.indicators;
        """, (
            AsIs(user.get('tki')),
        ))
        indicators = await cur.fetchall()

    return dict(
        indicators=indicators,
        indicator_groups=indicator_groups
    )


async def get_indicator_groups(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.indicator_groups;
        """, (
            AsIs(user.get("tki"))
        ))
        results = await cur.fetchall()

    return results


async def get_assignments_seed(user=None):
    results = []

    if user.get("tki") == "_4134c0e5ce1b":

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT 
                  uuid,
                  form_id,
                  location_id,
                  status,
                  user_id As uid,
                  type AS assign_type,
                  definition,
                  to_char(created, 'YYYY-MM-DD HH24:MI:SS') as created,
                  created_by,
                  to_char(COALESCE(modified, NOW()), 'YYYY-MM-DD HH24:MI:SS') as modified,
                  1 AS modified_by
                FROM %s.assignments;
            """, (
                AsIs(user.get('tki')),
            ))
            results = await cur.fetchall()
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT 
                  uuid,
                  form_id,
                  location_id,
                  status,
                  user_id As uid,
                  'DEFAULT' AS assign_type,
                  '{}' AS definition,
                  to_char(created, 'YYYY-MM-DD HH24:MI:SS') as created,
                  created_by,
                  to_char(COALESCE(modified, NOW()), 'YYYY-MM-DD HH24:MI:SS') as modified,
                  1 AS modified_by
                FROM %s.assignments;
            """, (
                AsIs(user.get('tki')),
            ))
            results = await cur.fetchall()

    return results


async def get_alarms_seed(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT t.uuid,
                  t.name,
                  '' AS description,
                  1 AS version,
                  row_to_json(t)::JSONB AS definition,
                  t.status,
                  '{}'::JSONB AS workflow,
                  to_char(t.created, 'YYYY-MM-DD HH24:MI:SS') AS created,
                  created_by,
                  to_char(t.modified, 'YYYY-MM-DD HH24:MI:SS') as modified,
                  t.modified_by
             FROM %s.alarms_v2 AS t;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_organizations(user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT
                uuid,
                name
            FROM %s.organizations;
        """, (
            AsIs(user.get('tki')),
        ))
        results = await cur.fetchall()

    return results


async def get_seed_other(user=None):
    notebooks = []
    maps = []
    templates = []
    plots = []
    teams = []
    dashboards = []

    tki = user.get('tki')
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.notebooks;
        """, (
            AsIs(tki),
        ))
        notebooks = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.mapping;
        """, (
            AsIs(tki),
        ))
        maps = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.plots;
        """, (
            AsIs(tki),
        ))
        plots = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.templates;
        """, (
            AsIs(tki),
        ))
        templates = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.layouts;
        """, (
            AsIs(tki),
        ))
        dashboards = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.teams;
        """, (
            AsIs(tki),
        ))
        teams = await cur.fetchall()

    return dict(
        notebooks=notebooks,
        maps=maps,
        plots=plots,
        templates=templates,
        dashboards=dashboards,
        teams=teams
    )


async def seed_records(form_id, period, user=None):
    results = []

    start_date = period[0]
    end_date = period[1]

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid,
            1 AS version,
              form_id,
              location_id,
              status,
              data_date,
              data,
              history,
              '[]'::JSONB AS related,
              to_char(created, 'YYYY-MM-DD HH24:MI:SS') AS created,
              created_by,
              to_char(submitted, 'YYYY-MM-DD HH24:MI:SS') AS submitted,
              1 AS submitted_by,
              to_char(last_modified, 'YYYY-MM-DD HH24:MI:SS') AS modified,
              1 AS modified_by,
              form_version_id AS version_id,
              source,
              eid,
              '{}'::JSONB AS workflow,
              '[]'::JSONB as revisions,
              '[]'::JSONB AS comments
            FROM %s.collections
            WHERE form_id = %s 
            AND status = 'SUBMITTED'
            AND data_date >= %s 
            AND data_date <= %s
            ORDER BY submitted DESC;
        """, (
            AsIs(user.get('tki')),
            form_id,
            start_date,
            end_date,
        ))

        results = await cur.fetchall()

    return results


async def get_oldest_date(form_id, user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT data_date 
            FROM %s.collections
            WHERE form_id = %s 
            AND status = 'SUBMITTED'
            LIMIT 1;
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        result = await cur.fetchone()

    result = result.get('data_date', None)

    return result


async def get_alerts(aid, period, user=None):
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT
                uuid,
                location_id
            FROM %s.alerts
            WHERE alarm_id = %s 
            AND trigger_end_date >= %s,
            AND trigger_end_date <= %s;
        """, (
            AsIs(user.get("tki")),
            aid,
            period[0],
            period[1],
        ))
        results = await cur.fetchall()

    return results


async def get_notebooks(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid,
                name,
                shared,
                1 AS version,
                definition,
                '[]' AS layout,
                COALESCE(created, NOW()) AS created,
                created_by,
                COALESCE(modified, NOW()) as modified,
                modified_by
            FROM %s.notebooks;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_documents(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid,
                template_name,
                instance_name,
                status,
                1 AS version,
                generation,
                layout,
                content,
                data,
                '{}' AS permissions,
                COALESCE(created, NOW()) as created,
                created_by,
                COALESCE(modified, NOW()) as modified,
                modified_by
            FROM %s.templates;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_plots(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid,
                name,
                shared,
                description,
                1 AS version,
                definition,
                COALESCE(created, NOW()) AS created,
                created_by,
                COALESCE(modified, NOW()) as modified,
                modified_by
            FROM %s.plots
        """, (
            AsIs(user.get("tki")),
        ))

        results = await cur.fetchall()

    return results


async def get_dashboards(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT
                uuid,
                name, 
                status,
                1 AS version,
                definition,
                layout,
                variables,
                permissions,
                COALESCE(created, NOW()) AS created,
                created_by,
                COALESCE(modified, NOW()) as modified,
                modified_by
            FROM %s.dashboards;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_maps(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT 
                uuid,
                name,
                status,
                1 AS version,
                permissions,
                shared,
                layers,
                annotations,
                config,
                
                COALESCE(created, NOW()) as created,
                created_by,
                COALESCE(modified, NOW()) as modified,
                modified_by
            FROM %s.maps;
        """, (
            AsIs(user.get("tki")),
        ))
        results = await cur.fetchall()

    return results


async def get_devices(user=None):
    return []


async def get_sites(user=None):
    return []


async def get_tasks(user=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT 
                uuid,
                created,
                modified,
                modified_by,
                actioned_by,
                actioned_date,
                definition,
                stauts,
                permissions
            FROM %s.tasks;
        """, (
            AsIs(user.get("tki,"))
        ))
        results = await cur.fetchall()

    return results
