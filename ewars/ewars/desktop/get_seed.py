import time
import uuid
import json
import datetime
from six import string_types

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

FORM_FIELDS = [
    "form_id"
]

LOC_TYPE_FIELDS = [
    "location_type",
    "generator_locations_type",
    "sti",
    "lti",
    "loc_type",
    "site_type_id"
]

SYNC_DATA = dict(
    #records="SELECT * FROM %s.d_records ORDER BY submitted LIMIT 10000",
    alerts="SELECT * FROM %s.d_alerts",
    forms="SELECT * FROM %s.d_forms",
    users="SELECT * FROM %s.users",
    locations="SELECT * FROM %s.d_locations",
    assignments="SELECT * FROM %s.d_assignments",
    reporting="SELECT * FROM %s.d_reporting WHERE fid IS NOT NULL",
    location_types="SELECT * FROM %s.location_types",
    organizations="SELECT * FROM %s.orgs_full",
    alarms="SELECT * FROM %s.d_alarms",
    conf="SELECT * FROM %s.conf"
)


def fix_object(loc_dict, form_dict, source):
    if isinstance(source, (int, float, string_types,)):
        return source

    if isinstance(source, (list,)):
        for idx, item in enumerate(source):
            if isinstance(item, (dict,)):
                source[idx] = fix_object(loc_dict, form_dict, item)
            elif isinstance(item, (int, float, string_types,)):
                source[idx] = item
            elif isinstance(item, (list,)):
                source[idx] = fix_object(loc_dict, form_dict, item)

        return source

    if isinstance(source, (dict,)):
        data = source
        for key, value in data.items():
            if isinstance(value, (dict,)):
                data[key] = fix_object(loc_dict, form_dict, value)
            elif isinstance(value, (list,)):
                if key == "inv_form_ids":
                    data[key] = [real_uid(form_dict, x) for x in value]
                else:
                    subs = value
                    for idx, node in enumerate(subs):
                        subs[idx] = fix_object(loc_dict, form_dict, node)
                    data[key] = subs
            else:
                if key in LOC_TYPE_FIELDS:
                    print(value, real_uid(loc_dict,str(value)))
                    data[key] = real_uid(loc_dict, str(value))
                elif key in FORM_FIELDS:
                    data[key] = real_uid(form_dict, str(value))
        return data

    return source



def real_uid(data, prop):
    if prop is None:
        return None

    if prop == "None":
        return None

    if data.get(str(prop), None) is not None:
        res = data.get(str(prop))
        if res == "None":
            return None
        elif res is None:
            return None
        else:
            return str(data.get(str(prop)))
    else:
        return None

def defaulted(val, fallback):
    if val in (None, "", "None", "NULL", "null", "Null"):
        return fallback
    else:
        return val

def fix_geojson(val):
    if isinstance(val, (string_types,)):
        if "{" in val:
            clean = json.loads(val)
            return json.dumps(clean)
        else:
            return None
    else:
        return json.dumps(val)

async def get_seed(id, tki, user=None):
    data = dict()

    ts_ms = int(time.time())
    async with get_db_cur() as cur:
        for key, query in SYNC_DATA.items():
            await cur.execute(query, (
                AsIs(tki),
            ))
            data_set = await cur.fetchall()
            data[key] = [dict(x) for x in data_set]

    loc_dict = dict()
    for item in data.get("location_types", []):
        loc_dict[str(item.get("id"))] = item.get("uuid")

    addon_assignments = []
    user_dict = dict()
    form_dict = dict()
    for item in data.get("users", []):
        user_dict[str(item.get("id"))] = item.get("uuid")

        if item.get('role') == "REGIONAL_ADMIN":
            addon_assignments.append(dict(
                uuid=uuid.uuid4(),
                uid=item.get("uuid"),
                lid=item.get("location_id"),
                fid=None,
                status="ACTIVE",
                assign_type="ADMIN",
                assign_group=None,
                created=datetime.datetime.utcnow(),
                created_by=None,
                modified=datetime.datetime.utcnow(),
                modified_by=None
            ))


    for item in data.get("forms", {}):
        form_dict[str(item.get("id"))] = item.get("uuid")

    for item in data.get("users", []):
        item["created_by"] = real_uid(user_dict, item.get("created_by"))
        item['modified_by'] = real_uid(user_dict, item.get("modified_by"))
        item['modified'] = datetime.datetime.utcnow()


    for key, value in data.items():
        if key == "assignments":
            for item in value:
                item['created'] = datetime.datetime.utcnow()

        if key == "forms":
            for item in value:
                item["features"] = fix_object(loc_dict, form_dict, item.get('features', {}))
                item['definition'] = fix_object(loc_dict, form_dict, item.get('definition', {}))
                definition = item.get('definition', {})

                features = item.get('features', {})

                location_reporting = features.get("LOCATION_REPORTING", None)
                if location_reporting is not None:
                    definition["__lid__"] = dict(
                        type="location",
                        unique=True,
                        order=-2,
                        label={"en": "Location"},
                        required=True,
                        canon=True,
                        lti=real_uid(loc_dict, location_reporting.get("site_type_id"))
                    )

                interval_reporting = features.get("INTERVAL_REPORTING", None)
                if interval_reporting is not None:
                    definition["__dd__"] = dict(
                        type="date",
                        unique=True,
                        order=-1,
                        label={"en": "Report Date"},
                        required=True,
                        canon=True,
                        block_future_dates=True,
                        date_type=interval_reporting.get("interval", "DAY")
                    )

                overdue = features.get("OVERDUE", None)
                if overdue is not None:
                    overdue['field'] = "__dd__"
                features["OVERDUE"] = overdue

                item['features'] = features
                item['definition'] = definition

                etl = item.get("etl", {})
                if etl is None:
                    etl = {}

                new_etl = {}
                for ind, defined in etl.items():
                    if "NEW_" not in ind:
                        if "object" not in ind:
                            new_etl[ind] = defined

                item['etl'] = new_etl



        if key == "alarms":
            for item in value:
                item['definition'] = fix_object(loc_dict, form_dict, item.get("definition", {}))
                if item.get('created', None) is None:
                    item['created'] = datetime.datetime.utcnow()

        if key == "reporting":
            for item in value:
                if item.get("created", None) is None:
                    item['created'] = datetime.datetime.utcnow()


        if key == "alerts":
            alerts = []
            for item in value:
                alert_data = dict(
                    outcome=item.get("outcome", None),
                    risk=item.get("risk", None),
                    rid=item.get("triggering_report_id", None),
                )
                events = []

                alert_events = item.get('events', [])
                if alert_events is None:
                    alert_events = []

                for node in alert_events:
                    if node.get("action_type") == "COMMENT":
                        events.append(dict(
                            uuid=uuid.uuid4(),
                            event_type="COMMENT",
                            data=node.get("content"),
                            created=node.get("action_date"),
                            uid=real_uid(user_dict, item.get("user_id"))
                        ))
                    elif node.get("action_type") == "TRIGGERED":
                        events.append(dict(
                            uuid=uuid.uuid4(),
                            event_type="TRIGGERED",
                            created=node.get("action_date"),
                        ))
                    elif node.get("action_type") == "CLOSED":
                        events.append(dict(
                            uuid=uuid.uuid4(),
                            event_type="CLOSED",
                            created=node.get("action_date"),
                        ))
                    elif node.get("action_type") == "DISCARDED":
                        events.append(dict(
                            uuid=uuid.uuid4(),
                            event_type="DISCARDED",
                            created=node.get("action_date"),
                            uid=real_uid(user_dict, node.get("user_id"))
                        ))
                    elif node.get("action_type") == "REOPEN":
                        events.append(dict(
                            uuid=uuid.uuid4(),
                            event_type="REOPEN",
                            data=node.get("content", None),
                            created=node.get("action_date"),
                            uid=real_uid(user_dict, node.get("user_id"))
                        ))

                alert_actions = item.get('actions', [])
                if alert_actions is None:
                    alert_actions = []

                for node in alert_actions:
                    if node.get("status") == "SUBMITTED":
                        node_data = node.get("data", {})
                        if node.get("type") == "VERIFICATION":
                            alert_data['verification_outcome'] = node_data.get("outcome", None)
                            alert_data['verification_comment'] = node_data.get("content", None)

                            events.append(dict(
                                uuid=uuid.uuid4(),
                                event_type="WORKFLOW",
                                stage="VERIFICATION",
                                state="APPLIED",
                                data=dict(
                                    verification_outcome=node_data.get("outcome", None),
                                    verification_comments=node_data.get("content", None),
                                ),
                                uid=real_uid(user_dict, node.get("user_id")),
                                created=node.get("created"),
                                submitted=node.get("submitted_date")
                            ))
                        elif node.get("type") == "RISK_ASSESS":
                            alert_data["assessment_hazard"] = node_data.get("hazard_assessment", None)
                            alert_data["assessment_context"] = node_data.get("context_assessment", None)
                            alert_data["assessment_exposure"] = node_data.get("exposure_assessment", None)

                            events.append(dict(
                                uuid=uuid.uuid4(),
                                event_type="WORKFLOW",
                                stage="RISK_ASSESS",
                                state="APPLIED",
                                data=dict(
                                    assessment_hazard=node_data.get("hazard_assessment", None),
                                    assessment_context=node_data.get("context_assessment", None),
                                    assessment_exposure=node_data.get("exposure_assessment", None)
                                ),
                                uid=real_uid(user_dict, node.get("user_id")),
                                created=node.get("created"),
                                submitted=node.get("submitted_date"),
                            ))
                        elif node.get("type") == "RISK_CHAR":
                            alert_data["riskIndex"] = node_data.get("riskIndex", None)
                            alert_data["risk"] = node_data.get("risk", None)

                            events.append(dict(
                                uuid=uuid.uuid4(),
                                event_type="WORKFLOW",
                                stage="RISK_CHAR",
                                state="APPLIED",
                                data=dict(
                                    risk_index=node_data.get("riskIndex", None),
                                    risk=node_data.get("risk", None)
                                ),
                                uid=real_uid(user_dict, node.get("user_id")),
                                created=node.get("created"),
                                submitted=node.get("submitted_date"),
                            ))
                        elif node.get("type") == "OUTCOME":
                            alert_data["outcome"] = node_data.get("outcome", None)
                            alert_data["outcome_comments"] = node_data.get("comments", None)

                            events.append(dict(
                                uuid=uuid.uuid4(),
                                event_type="WORKFLOW",
                                stage="OUTCOME",
                                state="APPLIED",
                                data=dict(
                                    outcome=node_data.get("outcome", None),
                                    outcome_comments=node_data.get("comments", None)
                                ),
                                uid=real_uid(user_dict, item.get("user_id")),
                                created=node.get("created"),
                                submitted=node.get("submitted_date"),
                            ))


                closed = None
                if item.get("status") == "CLOSED":
                    closed = item.get("modified")


                raised = None
                if item.get("raised", None) is None:
                    print("ALERT_DATE", item.get("created"))

                alerts.append(dict(
                    uuid=item.get("uuid"),
                    alid=item.get("alid"),
                    status=item.get("state"),
                    eid=item.get('eid', None),
                    alert_date=item.get("trigger_end"),
                    analysed_data=dict(),
                    lid=item.get("location_id", None),
                    stage=item.get('stage'),
                    data=alert_data,
                    workflow=dict(),
                    event=events,
                    created=item.get("created"),
                    raised=item.get("created"),
                    modified=item.get("modified"),
                    closed=closed,
                    modified_by=real_uid(user_dict, item.get("modified_by"))
                ))

            data['alerts'] = alerts

    data['assignments'] = data['assignments'] + addon_assignments

    return dict(
        data=data,
        id=id,
        tki=tki,
        protocol=1,
        ts_ms=ts_ms
    )



