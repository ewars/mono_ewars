import collections


def diff(original_data, new_data):
    """Get the diff between two dicts

    Used for calculating the difference between two
    reports in an amendment.

    Args:
        original_data: The unmodified reort
        new_data: The modified report

    Returns:
        A dict representing the changes applied to `original_data`

    """
    difference = {}

    flat_orig = _flatten(original_data)
    flat_new = _flatten(new_data)

    for key, value in flat_new.items():
        if key not in flat_orig:
            difference[key] = dict(
                    original=None,
                    amended=value
            )
        else:
            if value != flat_orig[key]:
                difference[key] = dict(
                        original=flat_orig[key],
                        amended=value
                )

    return difference


def _flatten(item, path=None):
    flat = {}

    for key, value in item.items():
        if isinstance(value, (dict)):
            sub_key = key

            if path is not None:
                sub_key = path + "." + key

            sub_items = _flatten(value, path=sub_key)

            if sub_items is not None:
                flat.update(sub_items)
        else:
            if path is None:
                flat[key] = value
            else:
                new_path = path + "." + key
                flat[new_path] = value

    return flat

def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.Mapping):
            r = update(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d
