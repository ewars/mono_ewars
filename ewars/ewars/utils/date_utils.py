import datetime
import calendar
from .isoweek import Week
from functools import lru_cache

from ewars.constants import CONSTANTS
from ewars.utils.six import string_types
from ewars.utils import date_utils

def get_first_day(dt, d_years=0, d_months=0):
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m-1, 12)
    return datetime.date(y+a, m+1, 1)

def get_last_day(dt):
    return get_first_day(dt, 0, 1) + datetime.timedelta(-1)

@lru_cache(maxsize=1024)
def display(date_value, date_format):
    """Format a date based on a time interval type

    Args:
        :param date_value: {date} A date object
        :param date_format: {str} A string representing the interval type (NONE,DAY,WEEK,MONTH,YEAR)

    Returns:
        A formatted string of the date

    """
    result = None

    date_parsed = date_value

    if isinstance(date_value, (string_types,)):
        date_parsed = parse_date(date_value)

    if date_format in (None, "NONE", "DAY"):
        result = date_parsed.strftime("%Y-%m-%d")
    elif date_format == "MONTH":
        result = date_parsed.strftime("%Y-%m")
    elif date_format == "YEAR":
        return date_parsed.strftime("%Y")
    elif date_format == "WEEK":
        isoWeek = Week.withdate(date_parsed)
        weekStart = isoWeek.monday()
        weekEnd = isoWeek.sunday()

        result = "W%s %s (%s %s-%s %s)" % (
            isoWeek.week,
            isoWeek.year,
            weekStart.strftime('%b'),
            weekStart.strftime('%d'),
            weekEnd.strftime("%b"),
            weekEnd.strftime("%d")
        )

    return result


@lru_cache(maxsize=1024)
def parse_date(string_date):
    """Parse a date

    Args:
        string_date: The date to parse

    Returns:
        a (datetime.date) object

    """
    if isinstance(string_date, datetime.date):
        return string_date

    if "T" in string_date:
        act_date = string_date.split("T")[0]
        return datetime.datetime.strptime(act_date, "%Y-%m-%d").date()
    elif "/" in string_date:
        return datetime.datetime.strptime(string_date, "%d/%m/%Y").date()
    else:
        return datetime.datetime.strptime(string_date, "%Y-%m-%d").date()


def get_intervals_between(start_date, end_date, interval_type):
    """Number of intervals between two dates for an interval type

    Args:
        start_date: The start date
        end_date: The end date
        interval_type: The type of interval

    Returns:
        An integer count of the intervals

    """
    if interval_type == "DAY":
        return get_days_between_dates(start_date, end_date)
    elif interval_type == "WEEK":
        return get_weeks_between_dates(start_date, end_date)
    elif interval_type == "MONTH":
        return get_months_between_dates(start_date, end_date)
    elif interval_type == "YEAR":
        return get_years_between_two_dates(start_date, end_date)


def get_days_between_dates(start_date, end_date):
    """Get the numnber of days between two dates

    Args:
        start_date: The start date
        end_date: The end date

    Returns:
        An integer of the number of days

    """
    return (end_date - start_date).days()


def get_weeks_between_dates(start_date, end_date):
    """Get the number of weeks between two dates

    Args:
        start_date: The Start date
        end_date: The end date

    Returns:
        The `int` of intervals between the dates

    """
    result = 0
    cur_week = Week.withdate(start_date)
    end_week = Week.withdate(end_date)

    while cur_week <= end_week:
        result = result + 1
        cur_week = cur_week + 1

    return result


def get_next_week_from_date(start_date):
    """Get the next ISO week end date from `start_date`

    Args:
        start_date: The start date to compute from

    Returns:
        A `datetime.date` object

    """
    cur_week = Week.withdate(start_date)

    last_day = cur_week.days[:1]
    return last_day


def get_months_between_dates(start_date, end_date):
    """Calculate the number of months between two dates

    Args:
        start_date: The start date
        end_date:  The end date

    Returns:
        The `int` of months

    """

    def is_last_day_of_month(date):
        days_in_month = calendar.monthrange(date.year, date.month)[1]
        return date.day == days_in_month

    imaginary_day_2 = 31 if is_last_day_of_month(end_date) else end_date.day
    month_delta = (
        (end_date.month - start_date.month) +
        (end_date.year - start_date.year) * 12 +
        (-1 if start_date.day > imaginary_day_2 else 0)
    )
    return month_delta


def get_last_completed_week(target_date):
    """ FRom a given date, get the last completed ISO week

    Args:
        target_date: The date to calculate from

    Returns:
        The last day of an iso week
    """
    cur_week = Week.withdate(target_date)
    start_day = cur_week.days[0]
    return start_day - datetime.timedelta(days=1)


def get_years_between_two_dates(start_date, end_date):
    """Calculate the number of years between two dates

    Args:
        start_date: The start date
        end_date: The end date

    Returns:
        The `int` of years

    """
    result = 0

    start_year = start_date.year
    end_year = end_date.year

    if start_year == end_year:
        result = 1
    else:
        while start_year <= end_year:
            result = result + 1
            start_year = start_year + 1

    return result


def get_bounds_for_month(src_date):
    """Get the start and end date of a month

    Args:
        src_date: A date within the month in question

    Returns:
        A tuple of start_date, end_date

    """
    first_day = src_date.replace(day=1)
    last_day = src_date.replace(day=calendar.monthrange(src_date.year, src_date.month)[1])
    return first_day, last_day


def get_date_range(start_date, end_date, interval_type, interval_step=1):
    """Get a list of dates based on an interval between two dates

    Args:
        start_date: The start date
        end_date: The end date
        interval_type: The interval type
        interval_step: The distance of each interval to step along

    Returns:
        A list of `datetime.date`

    """
    items = []

    if isinstance(start_date, (string_types,)):
        start_date = date_utils.parse_date(start_date)

    if isinstance(end_date, (string_types,)):
        end_date = date_utils.parse_date(end_date)

    if isinstance(start_date, (datetime.datetime,)):
        start_date = datetime.date(start_date.year, start_date.month, start_date.day)
    if isinstance(end_date, (datetime.datetime,)):
        end_date = datetime.date(end_date.year, end_date.month, end_date.day)

    if interval_type in ("DAY", "NONE"):
        delta = datetime.timedelta(days=interval_step)
        now = start_date

        while now <= end_date:
            items.append(now)
            new_date = now + delta
            now = new_date

    if interval_type == "WEEK":
        start_date = get_end_of_current_interval(start_date, "WEEK")
        end_date = get_end_of_current_interval(end_date, "WEEK")
        if start_date == end_date:
            items = [start_date]
        else:
            now = Week.withdate(start_date).days()[-1]
            items.append(now)

            while now < end_date:
                new_week = Week.withdate(now) + 1
                items.append(new_week.days()[-1])
                now = new_week.days()[-1]

    if interval_type == "MONTH":
        start_date = datetime.date(start_date.year, start_date.month, 1)
        end_date = get_last_day(end_date)

        now = start_date
        # Add the first month range
        start_end = get_last_day(start_date)
        items.append(start_end)

        ender = datetime.date(end_date.year, end_date.month, 1)

        # Create a month delta for advancing the months
        month_delta = datetime.timedelta(days=32)

        while now < ender:
            dt2 = now + month_delta
            dt2 = datetime.date(dt2.year, dt2.month, 1)

            month_end = get_last_day(dt2)
            items.append(month_end)
            now = dt2


    if interval_type == "YEAR":
        now = None
        if start_date.month == 12 and start_date.day == 31:
            items.append(start_date)
            now = start_date
        else:
            items.append(end_date)
            now = end_date

        while now < end_date:
            new_year = datetime.date(now.year + 1, 12, 31)
            items.append(new_year)
            now = new_year

    return items


def get_range_set(start_date, end_date, interval_type, interval_step=1, default_value=None):
    """
    Get all dates in a range by interval
    :param start_date:
    :param end_date:
    :param interval_type:
    :param interval_step:
    :return:
    """
    items = []
    start_date = datetime.date(start_date.year, start_date.month, start_date.day)
    end_date = datetime.date(end_date.year, end_date.month, end_date.day)

    day_delta = datetime.timedelta(days=1)

    if interval_type in ("DAY", "NONE"):
        delta = datetime.timedelta(days=interval_step)
        now = start_date

        while now <= end_date:
            items.append((now, now, default_value))
            new_date = now + delta
            now = new_date

    if interval_type == CONSTANTS.WEEK:
        start_date = get_end_of_current_interval(start_date, CONSTANTS.WEEK)
        end_date = get_end_of_current_interval(end_date, CONSTANTS.WEEK)

        if start_date == end_date:
            start_of = get_start_of_interval(start_date, CONSTANTS.WEEK)
            items = [(start_of, start_date, default_value)]
        else:
            now = Week.withdate(start_date).days()[-1]
            items.append((Week.withdate(start_date).days()[0], now, default_value))

            while now < end_date:
                new_week = Week.withdate(now) + 1
                items.append((new_week.days()[0], new_week.days()[-1], default_value))

                now = new_week.days()[-1]

    if interval_type == CONSTANTS.MONTH:
        start_date = datetime.date(start_date.year, start_date.month, 1)
        end_date = get_last_day(end_date)

        now = start_date
        # Add the first month range
        start_end = get_last_day(start_date)
        items.append((start_date, start_end, default_value,))

        ender = datetime.date(end_date.year, end_date.month, 1)

        # Create a month delta for advancing the months
        month_delta = datetime.timedelta(days=32)

        while now < ender:
            dt2 = now + month_delta
            dt2 = datetime.date(dt2.year, dt2.month, 1)

            month_end = get_last_day(dt2)
            items.append((dt2, month_end, default_value))
            now = dt2

    if interval_type == CONSTANTS.YEAR:
        now = None
        start = datetime.date(start_date.year, 12, 31)
        end = datetime.date(end_date.year, 12, 31)

        items.append((datetime.date(start.year, 1, 1, ), start, default_value))

        now = end

        while now <= end:
            next_year = now + datetime.timedelta(days=1)
            next_start = datetime.date(next_year.year, 1, 1)
            next_end = datetime.date(next_year.year, 12, 31)
            items.append((next_start, next_end, default_value))
            now = next_end

    return [dict(x=dt[0], y=dt[1], z=default_value) for dt in items]


def get_forward_date_by_interval(start_date, interval_type, step):
    """
    Get a date forward by interval and step
    :param start_date:
    :param interval_type:
    :param step:
    :return:
    """
    if interval_type == "DAY":
        delta = datetime.timedelta(days=step)
        return start_date + delta
    elif interval_type == "WEEK":
        # Check if today is an isoweek end
        last_week = Week.withdate(start_date)
        forward_week = last_week + step
        return forward_week.days()[-1]
    elif interval_type == "MONTH":
        delta = datetime.timedelta(days=32)

        # get to the end of the next month to start
        start_date = datetime.datetime.now()
        first, last = calendar.monthrange(start_date.year, start_date.month)
        start_date = datetime.date(start_date.year, start_date.month, last)

        current_date = start_date
        for i in range(0, step):
            dt1 = datetime.date(current_date.year, current_date.month, 1)
            dt2 = dt1 + delta
            first, last = calendar.monthrange(dt2.year, dt2.month)
            dt3 = datetime.date(dt2.year, dt2.month, last)
            current_date = dt3

        return current_date

    elif interval_type == "YEAR":
        new_year = start_date.year + step
        return datetime.date(new_year, 12, 31)


@lru_cache(maxsize=1024)
def get_end_of_current_interval(cur_date, interval):
    """
    Get the end interval date that provided date exists in
    :param cur_date:
    :param interval:
    :return:
    """
    if interval == "DAY":
        return cur_date

    if interval == "WEEK":
        cur_week = Week.withdate(cur_date)
        return cur_week.days()[-1]

    if interval == "MONTH":
        first, last = calendar.monthrange(cur_date.year, cur_date.month)
        return datetime.date(cur_date.year, cur_date.month, last)

    if interval == "YEAR":
        return datetime.date(cur_date.year, 12, 31)

    # Default to just returning the original date
    return datetime.date(cur_date.year, cur_date.month, cur_date.day)


def monthdelta(date, delta):
    m, y = (date.month + delta) % 12, date.year + ((date.month) + delta - 1) // 12
    if not m: m = 12
    d = min(date.day, [31,
                       29 if y % 4 == 0 and not y % 400 == 0 else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1])
    return date.replace(day=d, month=m, year=y)


def is_late(data_date, submitted_date, interval, threshold):
    """
    Figure out if a report is late
    :param data_date:
    :param submitted_date:
    :param interval:
    :param threshold:
    :return:
    """
    result = False

    overdue_date = get_forward_date_by_interval(data_date, interval, threshold)

    if submitted_date > overdue_date:
        result = True

    return result


def get_trigger_period(source_date, interval, interval_step):
    """
    Get the start and end date of time period as defined by an interval type (DAY, WEEK, MONTH) and a step {int}
        - Always steps back in time, no forward
    :param source_date: {datetime.date}
    :param interval: {str}
    :param interval_step: {int}
    :return:
    """
    end_date = None
    start_date = None

    if interval == "DAY":
        end_date = source_date
        if interval_step > 1:
            start_date = end_date - datetime.timedelta(days=interval_step)
        else:
            start_date = source_date
    elif interval == "WEEK":
        cur_week = Week.withdate(source_date)
        end_date = cur_week.day(6)

        start_week = cur_week - interval_step
        start_week = start_week.day(6)
        start_date = start_week + datetime.timedelta(days=1)

    elif interval == "MONTH":
        bounds = get_bounds_for_month(source_date)
        end_date = datetime.date(source_date.year, source_date.month, bounds[1])

    elif interval == "YEAR":
        end_date = datetime.date(source_date.year, 12, 31)

        start_year = source_date.year - interval_step
        start_date = datetime.date(start_year + 1, 12, 31)

    return start_date, end_date


def get_delta(interval, interval_type):
    """
    Get the delta for a specific # of intervals of a specific interval type
    :param interval: {int} Number of intervals
    :param interval_type: {str} The interval type
    :return:
    """
    if interval_type == "DAY":
        return datetime.timedelta(days=int(interval))
    elif interval_type == "WEEK":
        days = 7 * int(interval)
        return datetime.timedelta(days=int(days))
    elif interval_type == "MONTH":
        days = 30 * int(interval)
        return datetime.timedelta(days=int(days))
    elif interval_type == "YEAR":
        days = 365 * int(interval)
        return datetime.timedelta(days=int(days))
    else:
        return None


def get_start_of_interval(interval_date, interval):
    """
    Based on a date and an interval type, get the start of the interval
    :param interval_date:
    :param interval:
    :return:
    """
    if interval_date is None:
        return interval_date

    if interval == "WEEK":
        return interval_date - datetime.timedelta(days=6)
    elif interval == "DAY":
        return interval_date
    elif interval == "NONE":
        return interval_date
    elif interval == "MONTH":
        year = interval_date.year
        month = interval_date.month
        return datetime.datetime.strptime("%s-%s-01" % (year, month,), "%Y-%m-%d").date()
    elif interval == "YEAR":
        year = interval_date.year
        return datetime.datetime.strptime("%s-01-01" % (year,), "%Y-%m-%d").date()
    else:
        return interval_date


def is_in(dd1, dt1, dd2, dt2):
    """
    Check if an interval date is inside of another interval date
    :param dd1: the date we want to check
    :param dt1: The interval type of the date we ant to check, only used for mapping week upwards
    :param dd2: the date we want to see if dd1 is inside of
    :param dt2: the interval type of dd2
    :return:
    """

    if dt2 == "DAY":
        if dd1.strftime("%Y-%m-%d") == dd2.strftime("%Y-%m-%d"):
            return True
        else:
            return False

    if dt2 == "WEEK":
        start = get_start_of_interval(dd2, "WEEK")

        if dd1 >= start and dd1 <= dd2:
            return True
        else:
            return False

    if dt2 == "YEAR":
        if dt1 == "WEEK":
            weeks = Week.weeks_of_year(dd2.year)
            weeks = [x for x in weeks]
            start = weeks[0].day(0)
            end = weeks[-1].day(6)
        else:
            start = datetime.date(dd2.year, 1, 1)
            end = datetime.date(dd2.year, 12, 31)

        if dd1 >= start and dd1 <= end:
            return True
        else:
            return False


def is_valid_date(date_string, format):
    pass
