from unicodedata import normalize


def slug(text, encoding=None, permitted_chars='abcdefghijklmnopqrstuvwxyz0123456789-'):
    """Convert a string to slug

    Accepts a string which can contain most types of content. Swaps out spaces and
    created a string which is essentially URL friendly.

    Args:
        text (str): The string to convert
        encoding (str):  The encoding to encode the string to
        permitted_chars (str): Characters which are allowed in the string

    Returns:
        The formatted slug string

    """
    if isinstance(text, str):
        text = text.decode(encoding or 'ascii')

    clean_text = text.strip().replace(' ', '-').lower()
    while '--' in clean_text:
        clean_text = clean_text.replace('--', '-')

    ascii_text = normalize('NFKD', clean_text).encode('ascii', 'ignore')
    strict_text = map(lambda x: x if x in permitted_chars else '', ascii_text)
    return ''.join(strict_text)