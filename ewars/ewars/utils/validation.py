from uuid import UUID

from ewars.utils.six import string_types


def validate_field(value, field_type=None, min=None, max=None, required=False):
    """ Validate a field

    Args:
        value:  The value to validate
        field_type: The field type to valideate
        min: Optional minimum value
        max: Optional maximum value
        required: Is the value requires

    Returns:

    """
    if required:
        if value in (None, "", "null", "NULL"):
            return False

    return True


def validate_object(definition, data):
    """ Validate an object against a definition

    Args:
        definition: The definition dictating the validation parameters for the data
        data: The data to validate

    Returns:
        Either None, or a dict of errors corresponding to data items
    """
    return True


def validate_uuid(str_uuid):
    """ Validate a UUID4
     
    Args:
        str_uuid: The UUID to validate as a string

    Returns:
        True|False
    """
    if isinstance(str_uuid, (string_types,)):
        try:
            val = UUID(str_uuid, version=4)
        except ValueError:
            return False
        except TypeError:
            return False

        return str(val) == str_uuid
    else:
        if isinstance(str_uuid, (UUID,)):
            return True

    return False
