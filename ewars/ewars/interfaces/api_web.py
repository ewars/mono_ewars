import json
import inspect
import traceback

from tornado import gen

from ewars.constants import CONSTANTS
from ewars.core.api import get_api
from ewars.core.interface import BaseRequestHandler


ALLOWED_PUBLIC = [
    "com.ewars.table.locations",
    "com.ewars.mapping.loc_geoms",
    "com.ewars.mapping.form_points"
]
class Service(BaseRequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        method = data[0]
        args = data[1] or list()
        kwargs = data[2] or dict()
        api_version = self.request.headers.get("EAPI", "0")

        if method is None:
            self.send_error(status_code=404)

        # If no api version, default to 0
        if api_version is None:
            api_version = "0"

        _api = get_api(CONSTANTS.WEB, api_version)

        user = None
        if self.current_user is not None:
            user = self.current_user

        if user is None and method not in ALLOWED_PUBLIC:
            self.send_error(status_code=403)

        kwargs['user'] = user
        kwargs['application'] = self.application
        args = [method] + args
        result = None
        try:
            result = await _api.execute(args, kwargs)
        except Exception as e:
            print(e)
            traceback.print_exc()
        self.set_header('Content-type', 'application/json')
        self.write(result)
        self.finish()
