import json
import os
import re
import sys
import uuid
import datetime
import time
import stat

from jose import jwt, JWTError

# Jerry rigged for running on localhost
from ewars.conf import settings
from ewars.core.serializers import JSONEncoder

from ewars.core import identity
from ewars.models import User
from ewars.core.sync import legacy
from ewars.utils._os import get_install_root

import tornado
import tornado.web
import tornado.gen
from tornado import iostream
from ewars.core.interface import BaseRequestHandler

"""
# Login error codes
0 - Incorrect Email/Password
1 - Account Inactive/Awaiting Approval
2 - Unknown Error
3 - Email Missing
4 - Password Missing
"""


class SyncPackageService(tornado.web.RequestHandler):
    @classmethod
    def get_content(cls, abspath, start=None, end=None):
        """Retrieve the content of the requested resource which is located
        at the given absolute path.

        This class method may be overridden by subclasses.  Note that its
        signature is different from other overridable class methods
        (no ``settings`` argument); this is deliberate to ensure that
        ``abspath`` is able to stand on its own as a cache key.

        This method should either return a byte string or an iterator
        of byte strings.  The latter is preferred for large files
        as it helps reduce memory fragmentation.

        .. versionadded:: 3.1
        """
        with open(abspath, "rb") as file:
            if start is not None:
                file.seek(start)
            if end is not None:
                remaining = end - (start or 0)
            else:
                remaining = None
            while True:
                chunk_size = 64 * 1024
                if remaining is not None and remaining < chunk_size:
                    chunk_size = remaining
                chunk = file.read(chunk_size)
                if chunk:
                    if remaining is not None:
                        remaining -= len(chunk)
                    yield chunk
                else:
                    if remaining is not None:
                        assert remaining == 0
                    return

    @tornado.gen.coroutine
    def get(self, file_name, include_body=True):
        path = os.path.join(get_install_root(), "static/_sync/%s.txt.gz" % (file_name,))

        self.set_header("Accept-Ranges", "bytes")
        self.set_header("Content-Type", "application/gzip")

        stat_result = os.stat(path)
        size = stat_result[stat.ST_SIZE]

        content_length = None
        start = end = None

        if start is not None and end is not None:
            content_length = end - start
        elif end is not None:
            content_length = end
        elif start is not None:
            content_length = size - start
        else:
            content_length = size

        self.set_header("Content-Length", content_length)

        if include_body:
            content = self.get_content(path, start, end)
            if isinstance(content, bytes):
                content = [content]
            for chunk in content:
                try:
                    self.write(chunk)
                    yield self.flush()
                except iostream.StreamClosedError:
                    return
            os.remove(path)
        else:
            assert self.request.method == "HEAD"

        # with open(path, 'rb') as f:
        #     while 1:
        #         data = f.read(4096)  # or some other nice-sized chunk
        #         if not data: break
        #         self.write(data)
        # self.finish()
