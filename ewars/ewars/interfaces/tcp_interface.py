import json
import inspect
import time
import uuid
import asyncio
import datetime

import tornado
import tornado.ioloop
from tornado.tcpserver import TCPServer
from tornado.iostream import StreamClosedError
from tornado import gen

import psycopg2
import psycopg2.extensions
from psycopg2.extensions import AsIs

from ewars.conf import settings
from ewars.db import get_db_cur
from ewars import desktop

from ewars.server.api import attempt_authentication
from ewars.server.commands import COMMANDS
from ewars.server.events import METHODS
from ewars.server.seed_package import get_seed_package
from ewars.server.event_processor import handle_events


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            res = obj.strftime("%Y-%m-%dT%H:%M:%S.%f000Z")
            return res
        elif isinstance(obj, datetime.date):
            return obj.isoformat()
        elif isinstance(obj, uuid.UUID):
            return str(obj)
        else:
            return str(obj)

        return json.JSONEncoder.default(self, obj)

SEED_DATA = dict(
    forms="SELECT * FROM %s.forms;",
    records="SELECT * FROM %s.collections",
    alerts="SELECT * FROM %s.alerts",
    alarms="SELECT * FROM %s.alarms_v2",
    users="SELECT * FROM %s.users",
    dashboards="SELECT * FROM %s.dashboards",
    assignments="SELECT * FROM %s.assignments",
    reporting="SELECT * FROM %s.location_reporting",
    location_types="SELECT * FROM %s.location_types",
    conf="SELECT * FROM %s.conf",
    organizations="SELECT * FROM %s.orgnaizations",
    outbreaks="SELECT * FROM %s.outbreaks",
    locations="SELECT * FROM %s.locations",
    indicators="SELECT * FROM %s.indicators",
    indicator_groups="SELECT * FROM %s.indicator_groups",
    tasks="SELECT * FROM %s.tasks_v2",
    roles="SELECT * FROM %s.roles",
    templates="SELECT * FROM %s.templates",
    form_versions="SELECT * FROM %s.form_versions",
    notebooks="SELECT * FROM %s.notebooks",
    mapping="SELECT * FROM %s.mapping",
    plots="SELECT * FROM %s.plots",
    layouts="SELECT * FROM %s.layouts",
    views="SELECT * FROM %s.views",
    maps="SELECT * FROM %s.maps",
    devices="SELECT * FROM %s.devices"
)


class SonomaServer(TCPServer):
    waiters = set()
    cache = []
    cache_size = 200
    channels = {}
    cur = None
    conn = None
    listener = None
    _io_loop = None

    def __init__(self, *args, **kwargs):
        super(SonomaServer, self).__init__(*args, **kwargs)
        SonomaServer.listen()

    @classmethod
    def listen(cls):
        if cls.listener is None:
            cls.conn = psycopg2.connect(
                database=settings.DB_NAME,
                user=settings.DB_USER,
                password=settings.DB_PASSWORD,
                host=settings.DB_HOST,
                port=settings.DB_PORT
            )
            cls.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            cls.listener = cls.conn.cursor()
            cls.listener.execute("LISTEN sonoma")

            io_loop = tornado.ioloop.IOLoop.current()
            cls._io_loop = io_loop
            cls._io_loop.add_handler(cls.conn.fileno(), SonomaServer._io_callback, tornado.ioloop.IOLoop.READ)

    @classmethod
    def _io_callback(cls, *args):
        state = cls.conn.poll()
        if state == psycopg2.extensions.POLL_OK:
            while cls.conn.notifies:
                notify = cls.conn.notifies.pop()
                data = json.loads(notify.payload)
                print(data)

                if data.get("tki", None) is not None:
                    for waiter in SonomaServer.waiters:
                        payload = json.dumps(data).encode("utf8")
                        waiter._stream.write(payload + b'\n')

        elif state == psycopg2.extensions.POLL_ERROR:
            print("POLLING ERROR ON postgres socket")

        SonomaServer.listen()

    @classmethod
    def handle_event(cls, fd, events):
        state = cls.conn.poll()
        if state == psycopg2.extensions.POLL_OK:
            while cls.conn.notifies:
                notify = cls.conn.notifies.pop()
                print("GOT NOTIFY: ", notify.pid, notify.channel, notify.payload)
        elif state == psycopg2.extensions.POLL_READ:
            print("HERE")

    @classmethod
    def recv_notify(cls):
        print("HERE")

    @classmethod
    async def shout(cls, group, message):
        print(group, message)

    @classmethod
    async def whisper(cls, uid, message):
        print(uid, message)

    @classmethod
    async def send_channel(cls, chid, uid, message):
        print(chid, uid, message)

    @classmethod
    async def update_chan_msg(cls, msgid, message):
        print(msgid, message)

    @classmethod
    async def delete_chan_msg(cls, msgid):
        print(msgid)

    def get_user(self, tki, uid):
        result = None

        for user in self._users:
            if user.get("tki") == tki and user.get("uid") == int(uid):
                result = user

        return result

    def get_compression_options(self):
        return {}

    def check_origin(self):
        return True

    def on_close(self):
        SonomaServer.waiters.remove(self)

    def has_tki(self, tki):

        has = False
        for user in self._users:
            if user.get("tki") == tki:
                has = True

        return has

    def get_user(self, tki, uid):
        result = None

        for user in self._users:
            if (user.get("tki"), user.get("uid"),) == (tki, uid,):
                result = user

        return user

    async def write_channels(self):
        accounts = list(set(x.get("tki") for x in self._users))

        channels = dict((x, []) for x in accounts)
        for key, acc_channels in channels.items():
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.teams;
                """, (
                    AsIs(key),
                ))
                channels[key] = await cur.fetchall()

        self.write_message(json.dumps(dict(
            op=0,
            t="SEED_CHANNELS",
            d=channels
        ), cls=JSONEncoder))

    async def write_users(self, tkis):
        all_users = []

        for waiter in SonomaServer.waiters:
            all_users = all_users = [x for x in waiter._users if x.get("tki") in tkis]

        self.write_message(json.dumps(dict(
            op=0,
            t="ACTIVES",
            d={
                "USERS": all_users
            }
        ), cls=JSONEncoder))


    async def _handle_command(self, cmd, args, tki=None, uid=None):
        if cmd in COMMANDS._methods.keys():
            method_fn = COMMANDS._methods.get(cmd)

            kwargs = dict(
                user=self.get_user(tki, uid)
            )

            if inspect.iscoroutinefunction(method_fn):
                result = await method_fn(*args, **kwargs)
            else:
                result = method_fn(*args, **kwargs)

            payload = json.dumps(dict(
                type="Command",
                cmd=cmd,
                args=result
            ), cls=JSONEncoder).encode("utf8")
            await self._stream.write(payload + b"\n")
        else:
            payload = json.dumps(dict(
                type="Command",
                cmd=cmd,
                args=dict(
                    error=True,
                    description="NO_METHOD"
                )
            ), cls=JSONEncoder).encode("utf8")
            await self._stream.write(payload + b"\n")

    async def _handle_sync(self, manifest):
        results = dict()
        items = list(manifest.get("manifest", {}).values())

        for dt in items:
            async with get_db_cur() as cur:
                # Get the accounts highest ts_ms for generated events log
                # items where the device is not the one we're communicating with now
                await cur.execute("""
                    SELECT COALESCE(MAX(ts_ms), 0) AS ts_ms
                    FROM %s.event_log
                    WHERE source != %s;
                """, (
                    AsIs(dt.get("tki")),
                    manifest.get("device_id"),
                ))
                glob_result = await cur.fetchone()

                # Get all events above the canon_ts_ms that the peer
                # is holding that weren't generated by the peer we're talking to
                await cur.execute("""
                    SELECT * FROM %s.event_log
                    WHERE ts_ms > %s
                    AND source != %s;
                """, (
                    AsIs(dt.get("tki")),
                    dt.get("canon_ts_ms", 0),
                    manifest.get("device_id"),
                ))
                events = await cur.fetchall()

                # Get the highest ts_ms we hold for the user we're communicating with
                await cur.execute("""
                    SELECT COALESCE(MAX(ts_ms), 0) as ts_ms FROM %s.event_log
                    WHERE source = %s;
                """, (
                    AsIs(dt.get("tki")),
                    dt.get("device_id", 0),
                ))
                cl_ts_ms = await cur.fetchone()

            results[dt.get("tki")] = dict(
                tki=dt.get("tki"),
                ts_ms=glob_result.get("ts_ms"),
                events=events,
                wants=[
                    cl_ts_ms.get("ts_ms", 0),
                    dt.get("local_ts_ms", 0)
                ]
            )

        payload = json.dumps(dict(
            type="EventBatch",
            manifests=results,
            job_id=manifest.get("job_id")
        )).encode("utf8")
        await self._stream.write(payload + b"\n")

    async def _handle_event_stream(self, batch):
        tki = batch.get("tki", None)
        device_id = batch.get("device_id", None)

        cur_high_ts_ms = 0

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COALESCE(MAX(ts_ms), 0) AS ts_ms
                FROM %s.event_log
                WHERE source = %s;
            """, (
                AsIs(tki),
                device_id,
            ))
            result = await cur.fetchone()
            cur_high_ts_ms = result.get("ts_ms", 0)

        # Sort the event items by their ts_ms ascending so that they're
        # processed in order
        batch_data = batch.get("data", [])
        batch_data = sorted(batch_data, key=lambda k: k['ts_ms'])
        # Process the batch events
        for item in batch_data:
            if item.get("ts_ms") > cur_high_ts_ms:
                event_data = item.get('data', {})
                # Get the class type to handle this events resource change
                instance_type = METHODS.get(event_data.get("ref_type"), None)

                if instance_type is not None:
                    if event_data.get("event_type") == "DELETE":
                        await instance_type.delete(event_data, tki=tki, device_id=device_id)
                    elif event_data.get("event_type") == "UPDATE":
                        await instance_type.update(event_data, tki=tki, device_id=device_id)
                    elif event_data.get("event_type") == "CREATE":
                        await instance_type.create(event_data, tki=tki, device_id=device_id)

    async def _write(self, data):
        payload = json.dumps(data, cls=JSONEncoder).encode("utf8")
        await self._stream.write(payload + b"\n")

    async def _handle_auth(self, id, email, password):
        if email in (None, ""):
            await self._write(dict(
                type="AuthenticationResult",
                result=False,
                error="NO_EMAIL"
            ))
            return

        if password in (None, ""):
            await self._write(dict(
                type="AuthenticationResult",
                result=False,
                error="NO_PASSWORD"
            ))
            return

        auth_result = await attempt_authentication(email, password)
        print(auth_result)

        await self._write(dict(
            type="AuthenticationResult",
            id=id,
            success=auth_result.get("success"),
            code=auth_result.get("code", None),
            users=auth_result.get("users", []),
            accounts=auth_result.get("accounts", [])
        ))

    async def _handle_seed_request(self, tki):
        result = await get_seed_package(tki)

        await self._write(result)

    async def _handle_ping(self, data):
        print(data)
        ts_ms = int(time.time())

        own_events = []
        peer_events = []

        for key, value in data.get('manifest', {}).items():
            splat = key.split(",")
            if splat[1] == "CANON":
                own_events.append((
                    splat[0],
                    value,
                ))
            else:
                peer_events.append((
                    splat[0],
                    splat[1],
                    value,
                ))

        canon_events = dict()

        async with get_db_cur() as cur:
            for item in own_events:
                await cur.execute("""
                    SELECT *
                    FROM %s.event_log
                    WHERE ts_ms > %s;
                """, (
                    AsIs(item[0]),
                    item[1],
                ))
                events = await cur.fetchall()

                canon_events[item[0]] = [dict(x) for x in events]

        await self._write(dict(
            type="Pong",
            canon_ts_ms=ts_ms,
            events=canon_events,
            manifest=dict()
        ))

    async def _handle_seed_request(self, id, tki):
        result = await desktop.get_seed(id, tki)
        print(result.get("ts_ms"))

        await self._write(dict(
            type="SeedData",
            id=id,
            tki=tki,
            protocol=1,
            data=result.get("data"),
            ts_ms=result.get("ts_ms")
        ))

    async def handle_stream(self, stream, address):
        self._users = [{}]
        self._stream = stream
        self._address = address

        self.authenticated = False
        self._channels = []
        self._ts_ms = None
        SonomaServer.waiters.add(self)

        while True:
            try:
                data = await stream.read_until(b"\n")
                j_data = json.loads(data.decode("utf8"))
                print(j_data)
                if j_data.get("type") == "Command":
                    cmd = j_data.get("cmd")
                    args = j_data.get("args")
                    print(cmd, args)
                    if cmd == "com.sonoma.authenticate":
                        await self._authenticate(args.get("email"), args.get("password"))
                    else:
                        uid = j_data.get("uid", None)
                        tki = j_data.get("tki", None)
                        await self._handle_command(cmd, args, tki=tki, uid=uid)
                elif j_data.get("type") == "SeedRequest":
                    await self._handle_seed_request(j_data.get("id"), j_data.get("tki"))
                elif j_data.get("type") == "Ping":
                    await self._handle_ping(j_data)
                elif j_data.get("type") == "AuthRequest":
                    await self._handle_auth(
                        j_data.get("id", None),
                        j_data.get('email', None),
                        j_data.get("password", None)
                    )
                elif j_data.get('type') == "GetSeed":
                    await self._handle_seed_request(j_data.get('tki', None))
                elif j_data.get("type") == "RegistrationRequest":
                    pass
                elif j_data.get("type") == "AccessRequest":
                    pass

            except StreamClosedError:
                break
