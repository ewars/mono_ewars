import json

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

from ewars.core.api import get_api
from ewars.core.interface import BaseRequestHandler
from ewars.constants import CONSTANTS
from ewars.conf import settings
from jose import jwt, JWTError


class Service(BaseRequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        method = data[0]
        args = data[1] or list()
        kwargs = data[2] or dict()

        api_version = self.request.headers.get("EAPI", 0)

        if method is None:
            self.send_error(status_code=404)

        # If no api version, default to 0
        if api_version is None:
            api_version = "0"

        user = None

        if self.current_user is not None:
            user = self.current_user

        recovery = False
        if user is None:
            if self.request.headers.get("Ec_token", None) not in (None, ""):
                token = self.request.headers.get("Ec_token", None)

                if token is None and user is None:
                    self.send_error(status_code=403)
                else:
                    try:
                        user = jwt.decode(token, settings.JWT_KEY, algorithms=["HS256"])
                        if "user" in user.keys():
                            user = user.get("user")
                    except Exception as e:
                        self.send_error(status_code=403)
            elif "E_c_recovery_id" in self.request.headers.keys():
                recovery = True
                user_id = self.request.headers.get("E_c_recovery_id")
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT id, accounts
                        FROM _iw.users 
                        WHERE id = %s;
                    """, (
                        user_id,
                    ))
                    tmp_user = await cur.fetchone()

                    await cur.execute("""
                        SELECT tki
                        FROM _iw.accounts 
                        WHERE id = %s;
                    """, (
                        tmp_user.get("accounts", [])[0],
                    ))
                    account = await cur.fetchone()

                    await cur.execute("""
                        SELECT * FROM %s.users
                        WHERE id = %s;
                    """, (
                        AsIs(account.get("tki")),
                        user_id,
                    ))
                    user = await cur.fetchone()

        if user is None:
            self.send_error(status_code=403)
        else:
            # check if the tki is there
            if user.get('tki', None) is None:
                if user.get('aid', None) is not None:
                    async with get_db_cur() as cur:
                        await cur.execute("""
                            SELECT tki FROM _iw.accounts
                            WHERE id = %s;
                        """, (
                            user.get('aid'),
                        ))
                        acc = await cur.fetchone()
                        user['tki'] = acc.get('tki')

            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.users
                    WHERE id = %s;
                """, (
                    AsIs(user.get("tki")),
                    user.get("id"),
                ))
                user = await cur.fetchone()


        _api = get_api(CONSTANTS.MOBILE, str(api_version))

        kwargs['user'] = user
        args = [method] + args
        result = await _api.execute(args, kwargs)

        if recovery:
            # Has to deal with an issue where users were sending cookies along and the system needed to use TOKENs
            user['created'] = user['created'].strftime("%Y-%M-%d %H:%m")
            user['registered'] = user['registered'].strftime("%Y-%M-%d %H:%m")
            user['last_modified'] = user['last_modified'].strftime("%Y-%M-%d %H:%m")
            del user['password']

            token = jwt.encode(dict(user=user), settings.JWT_KEY, algorithm="HS256")
            self.write(dict(
                recover=True,
                token=token,
                data=result
            ))
        else:
            self.write(result)
