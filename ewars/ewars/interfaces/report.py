import json
import uuid
import base64
import datetime
import time
import asyncio
from calendar import timegm
from ewars.db import get_db_cur
from psycopg2.extensions import AsIs
import re
import os
import urllib
import urllib.parse
from pyppeteer import launch

from jose import jwt

from tornado.web import os

from ewars.constants import CONSTANTS

from ewars.core.interface import BaseRequestHandler
from ewars.core import clients
from ewars.conf import settings
from ewars.core.serializers.json_encoder import JSONEncoder

from ewars.models import Template


def utc_mktime(utc_tuple):
    if len(utc_tuple) == 6:
        utc_tuple += (0, 0, 0)

    return time.mktime(utc_tuple) - time.mktime((1970, 1, 1, 0, 0, 0, 0, 0, 0))


def datetime_to_timestamp(dt):
    return int(utc_mktime(dt.timetuple()))


def timedelta_total_seconds(delta):
    return delta.days * 24 * 60 * 60 + delta.seconds

class PDFCreateInterface(BaseRequestHandler):
    def options(self):
        self.set_status(204)
        self.finish()

    async def get(self, document):
        data = base64.urlsafe_b64decode(document)
        data = data.decode("utf-8")
        data = data.split(";")

        config = dict()
        for item in data:
            key, value = item.split(":")
            config[key] = value

        new_uuid = str(uuid.uuid4())

        report_date = config.get('d', None)
        template_id = config.get('t', None)
        location_id = config.get("l", None)
        report_uuid = config.get("c", None)
        tki = config.get("k", None)

        account = None
        template = None
        location = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.templates
                WHERE uuid = %s
            """, (
                AsIs(tki),
                template_id,
            ))
            template = await cur.fetchone()

            if location_id is not None:
                await cur.execute("""
                    SELECT uuid, name FROM %s.locations WHERE uuid = %s;
                """, (
                    AsIs(tki),
                    location_id,
                ))
                location = await cur.fetchone()

        # Generate doc title
        doc_title = Template.format_document_title(
            template.get("instance_name"),
            definition=template,
            report_date=report_date,
            template_type=template.get("template_type"),
            report_uuid=report_uuid,
            location=location
        )

        cached = None
        async with get_db_cur() as cur:
            # Check for cached file
            await cur.execute("""
                SELECT * FROM %s.template_cache
                WHERE template_id = %s 
                  AND doc_id = %s;
            """, (
                AsIs(tki),
                template.get("uuid"),
                document,
            ))
            cached = await cur.fetchone()

        cached = None

        # Create file name
        b64coded = base64.urlsafe_b64encode(doc_title.encode("utf-8"))
        file_path = "%s.pdf" % (b64coded.decode("utf-8"))

        # Handle caching
        serve_cache = False
        cache_expire = template.get("expires", None)
        now = datetime.datetime.utcnow()

        # Check if there's a cached file already
        if cached is not None:
            diff = now - cached.get("created_date")
            hours = diff.seconds / 3600

            # Check if the cached file has expired
            if hours > template.get("expires"):
                # Destroy the cache and rebuild the document
                serve_cache = False
                storage_path = "%s/%s" % (settings.FILE_UPLOAD_TEMP_DIR, file_path)

                # remove teh file
                try:
                    os.remove(storage_path)
                except OSError:
                    pass

                # Remove the cache hit in template_cache
                async with get_db_cur() as cur:
                    await cur.execute("""
                        DELETE FROM %s.template_cache
                        WHERE doc_id = %s
                        AND template_id = %s;
                    """, (
                        AsIs(tki),
                        document,
                        template.get("uuid"),
                    ))
            else:
                serve_cache = True

        # If this is a draft, we don't serve the cache ever
        if template.get("status") == "DRAFT":
            serve_cache = False

        # if there's no expiration set for the template, we don't cache
        if cache_expire in ("", None, 0, "0",):
            serve_cache = False

        # If we're not serving the cache, serve the
        serve_cache = False
        failed = False
        if serve_cache is False:
            storage_path = "%s/%s" % (settings.FILE_UPLOAD_TEMP_DIR, file_path)

            if cache_expire not in ("", None, 0, '0'):
                async with get_db_cur() as cur:
                    await cur.execute("""
                        INSERT INTO %s.template_cache (doc_id, template_id, created_date, expires, filename) 
                        VALUES (%s, %s, %s, %s, %s);
                    """, (
                        AsIs(tki),
                        document,
                        template.get("uuid"),
                        datetime.datetime.utcnow(),
                        cache_expire,
                        storage_path,
                    ))

            # Make sure a file doesn't already exist
            try:
                os.remove(storage_path)
            except OSError:
                pass

            # Get the orientation for the PDF
            orientation = template.get("orientation", "PORTRAIT")
            landscape = False

            if orientation in ("LANDSCAPE", "Landscape"):
                landscape = True

            # Here for testing purposes
            domain = None
            if settings.DEBUG == True:
                domain = "127.0.0.1:9000"
            else:
                domain = "ewars.ws"

            timestamp = time.time()
            report_uri = "http://" + domain + "/document/%s?pdf=1&queryt=%s" % (document, str(timestamp))
            # report_uri = "http://" + domain + "/document/%s?pdf=1" % (document,)

            browser = await launch({
                "headless": True
            })
            page = await browser.newPage()

            await page.goto(report_uri)
            try:
                res = await page.waitForSelector(
                    "#uxflagged",
                    dict(
                        timeout=500000
                    )
                )
            except Exception as e:
                failed = True

            if not failed:
                await page.pdf(dict(
                    path=storage_path,
                    printBackground=True,
                    landscape=landscape,
                    format="A4"
                ))

            await browser.close()
            asyncio.sleep(5)


            # ret = await result.wait_for_exit(raise_error=True)

            if not failed:
                self.file_path = base64.urlsafe_b64encode(doc_title.encode("utf-8"))
                # self._complete_request(None)
                # result.set_exit_callback(self._complete_request)
                self.write(dict(
                    success=True,
                    name=self.file_path.decode("utf-8")
                ))
            else:
                self.write(dict(
                    success=False,
                    err="TIMEOUT"
                ))
        else:
            self.file_path = base64.urlsafe_b64encode(doc_title.encode("utf-8"))
            self.write(json.dumps(dict(
                success=True,
                name=self.file_path.decode("utf-8")
            )))


class PDFDownloadInterface(BaseRequestHandler):
    """
    Download a PDF generated and stored in the tmp directory
    """

    def get(self, file_name):
        content = None

        is_xlsx = False
        is_zip = False
        is_pdf = False
        raw_file_name = file_name
        file_type = "application/pdf"
        file_title = base64.urlsafe_b64decode(raw_file_name)

        if isinstance(file_title, (bytes,)):
            file_title = file_title.decode("utf-8")

        encoding = "utf-8"
        if "xlsx" in file_title:
            is_xlsx = True
            encoding = "latin-1"

        if 'zip' in file_title:
            is_zip = True

        if is_xlsx:
            file_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            file_name = "%s.xlsx" % (raw_file_name,)
        elif is_zip:
            file_type = "application/zip, application/octet-stream"
        else:
            is_pdf = True
            file_title = "%s.pdf" % (file_title)
            file_name = "%s.pdf" % (file_name)

        with open("%s/%s" % (settings.FILE_UPLOAD_TEMP_DIR, file_name), "rb") as f:
            content = f.read()

        if is_xlsx:
            # content = content.decode("latin-1").encode("utf-8")
            content = content.decode("latin-1").encode("latin-1", "ignore")

        # if is_xlsx:
        #     content = content.encode("utf-8")

        if is_xlsx:
            try:
                os.remove("%s/%s" % (settings.FILE_UPLOAD_TEMP_DIR, file_name))
            except OSError:
                pass

        self.set_header("Content-Type", file_type)
        self.set_header("Content-Disposition", "attachment; filename=%s" % (urllib.parse.quote(file_title, safe='')))
        # self.set_header('Content-Disposition', 'attachment; filename=%s' % email.utils.collapse_rfc2231_value(file_title))

        self.write(content)


class ReportViewInterface(BaseRequestHandler):
    async def get(self, document):
        ewars_client = clients.get_client("ewars")
        report_client = clients.get_client("report")

        if "cache" in document:
            document = document.split("&")[0]

        data = base64.urlsafe_b64decode(document)
        data = data.decode("utf-8")
        data = data.split(";")

        pdf = self.request.arguments.get("pdf", False)
        if pdf is not False:
            pdf = True

        config = dict()
        for item in data:
            key, value = item.split(":")
            config[key] = value

        report_date = config.get("d", None)
        template_id = config.get("t", None)
        location_id = config.get("l", None)
        report_uuid = config.get("c", None)
        aid = config.get("a", None)

        if template_id is None:
            self.send_error(status_code=404)

        account = None
        template = None
        location = None
        collection = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, tki FROM _iw.accounts WHERE id = %s;
            """, (
                aid,
            ))
            account = await cur.fetchone()

            await cur.execute("""
                SELECT *
                FROM %s.templates
                WHERE uuid = %s;
            """, (
                AsIs(account.get("tki")),
                template_id,
            ))
            template = await cur.fetchone()

            if location_id is not None:
                await cur.execute("""
                    SELECT uuid, name
                    FROM %s.locations
                    WHERE uuid = %s;
                """, (
                    AsIs(account.get("tki")),
                    location_id,
                ))
                location = await cur.fetchone()

            if report_uuid is not None:
                await cur.execute("""
                    SELECT *
                    FROM %s.collections
                    WHERE uuid = %s;
                """, (
                    AsIs(account.get("tki")),
                    report_uuid,
                ))
                collection = await cur.fetchone()

        instance = None
        report = {}

        if template is None:
            self.send_error(status_code=404)

        if report_date is None:
            self.send_error(status_code=404)

        user = None
        token = None

        if template.get("access") == CONSTANTS.PUBLIC:
            # We need a token as well

            gm = datetime.datetime.utcnow() + datetime.timedelta(0, 0, 0, 0, 45)
            gm = timegm(gm.utctimetuple())

            user = dict(
                tki=account.get("tki"),
                aid=account.get("id"),
                tid=str(template.get("uuid")),
                exp=gm
            )
            token = jwt.encode(user, settings.JWT_KEY, algorithm="HS256")

        if template.get("access") == CONSTANTS.PRIVATE:
            user = self.current_user

            # If no user, dupm them
            if user is None:
                self.send_error(status_code=404)

            # If the user does not belong to the account in question, dupm them
            if user.get("aid", None) != account.get("id", 'NO_ACCOUNT'):
                self.send_error(status_code=404)

        template['tki'] = account.get('tki')

        template_content = template.get("content")

        del template["content"]

        template_data = template

        template_content = await Template.format_template(template_content,
                                                          definition=template,
                                                          report_date=report_date,
                                                          report_uuid=report_uuid,
                                                          template_type=template.get("template_type"),
                                                          location=location)

        if pdf:
            template_content = re.sub(r"(display\:(\s*)none(;?))", "", template_content)

        ewars_version = "%s.js.gz" % (ewars_client.get("version"),)
        if settings.DEBUG:
            ewars_version = "dev.js"
        ewars_file = 'ewars-%s' % (ewars_version,)

        client_version = "%s.js.gz" % (report_client.get("version"),)
        if settings.DEBUG:
            client_version = "dev.js"
        client_file = '%s-%s' % (report_client.get("token"), client_version)

        location_name = "null"
        if location is not None:
            location['name']['en']

        report_data = None
        if report is not None:
            report_data = report.get("data")
        #
        # t_data = template.get('data', {})
        #
        # map_loaders = []
        # for d_key, d_def in t_data.items():
        #     if d_def.get('type', None) == 'MAP':
        #         map_loaders.append((
        #             d_def.get('location', None),
        #             d_def.get('site_type_id', None)
        #         ))
        #
        # geom_distinct_loaders = [x for x in map_loaders if x[0] is not None]
        # geom_distinct_loaders = list(set(geom_distinct_loaders))
        #
        # print(geom_distinct_loaders)
        #
        # geom_dict = dict()
        # if len(geom_distinct_loaders) > 0:
        #     async with get_db_cur() as cur:
        #         for geom_get in geom_distinct_loaders:
        #             await cur.execute("""
        #                 SELECT uuid::TEXT, geojson, geometry_type
        #                 FROM %s.locations
        #                 WHERE lineage::TEXT[] @> %s::TEXT[]
        #                   AND site_type_id = %s;
        #             """, (
        #                 AsIs(user.get('tki')),
        #                 [geom_get[0]],
        #                 geom_get[1],
        #             ))
        #
        #             tmp_locations = await cur.fetchall()
        #
        #             for loc in tmp_locations:
        #                 geom_dict[loc.get('uuid')] = (loc.get('geometry_type'), loc.get('geojson', None))
        #
        # print(geom_dict)
        #
        # geom_data = json.dumps(geom_dict)
        # geom_data = geom_data.replace("'", "\\'")

        template_data = json.dumps(template, cls=JSONEncoder)
        template_data = template_data.replace("\n", "\\n")
        self.render("document.html",
                    template=template_data,
                    template_content=template_content,
                    template_id=template_id,
                    report_date=report_date,
                    location_id=location_id,
                    pdf=pdf,
                    token=token,
                    inline=True,
                    location_name=location_name,
                    debug=True,
                    orientation=template.get("orientation", "PORTRAIT"),
                    report=json.dumps(report, cls=JSONEncoder),
                    ewars_file=ewars_file,
                    client_file=client_file,
                    template_type=template.get('template_type'),
                    report_data=report_data,
                    report_uuid=report_uuid)


document_uris = [
    (r"/document/pdf/(.*)", PDFCreateInterface),
    (r"/document/download/(.*)", PDFDownloadInterface),
    (r"/document/(.*)", ReportViewInterface)
]
