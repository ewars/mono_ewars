import json
import datetime
import asyncio
import uuid

from ewars.conf import settings
from ewars.constants import CONSTANTS
from ewars.db import get_db_cur
from ewars.core.serializers import JSONEncoder
from ewars.core.interface import BaseRequestHandler

from ewars.core import registration, authentication, verification, clients, invitations

import tornado
from tornado.web import RequestHandler
import tornado.gen


class RegisterInterface(BaseRequestHandler):
    async def get(self):
        client = clients.get_client("register")
        data = await self.render_client(client, public=True)
        self.render("template.html", **data)

    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        domain = self.request.host

        if ":" in domain:
            domain = domain.split(":")[0]

        result = dict()

        email = data.get("email", None)
        password = data.get("password", None)
        org_id = data.get("org_id", None)
        account_id = data.get("account_id", None)
        name = data.get("name", None)
        organization_name = data.get("organization_name", None)
        if org_id in ("", None, "NEW"):
            org_id = None

        # Check if they're registering from the root domain
        # if this is the case, then they'll have to specify
        # an account to register for

        acc_token = None
        has_sso_user = await registration.has_sso_user(email)

        # Figure out what token to use for account based queries
        if domain == "127.0.0.1" and settings.DEBUG:
            domain = 'ewars.ws'

        if domain == settings.ROOT_DOMAIN:
            acc_token = await registration.get_token_by_id(account_id)
        else:
            acc_token = await registration.get_token_by_domain(domain)

        if acc_token is None:
            self.write(json.dumps(dict(
                err=True,
                code="NO_ACCOUNT"
            )))
            return

        # An root user exists, check it out
        if has_sso_user is not None:
            status = has_sso_user.get("status")

            # Already attempted registering, system awaiting email verification
            if status == "PENDING_VERIFICATION":
                self.write(dict(
                    err=True,
                    code="PENDING_VERIFICATION"
                ))
                self.finish()
                return

            # already a user with this email, user was removed
            # TODO: Add a reason for removal to display to user
            if status == "DELETED":
                self.write(dict(
                    err=True,
                    code="USER_REMOVED"
                ))
                self.finish()
                return

            # root user account is OK
            # Check if this user already has a record in the account
            has_account_user = await registration.has_account_user(email, acc_token)

            # User has a record in the context already
            if has_account_user is not None:
                acc_user_status = has_account_user.get("status")

                # User context record is pending approval
                if status == "PENDING_APPROVAL":
                    self.write(dict(
                        err=True,
                        code="PENDING_APPROVAL"
                    ))
                    self.finish()
                    return

                # User context record was revoked
                if status == "DELETED":
                    self.write(dict(
                        err=True,
                        code="USER_REMOVED"
                    ))
                    self.finish()
                    return

                # This user already exists, prompt to log in instead
                # of register for the site
                self.write(dict(
                    err=True,
                    code="USER_EXISTS"
                ))
                self.finish()
                return

            # If we're here, the user has an SSO user, but no user record in th
            # account context, ask them to authenticate
            # and request access to the account using their
            # credentials instead of registering a whole new user
            self.write(dict(
                err=True,
                code="SSO_EXISTS_NO_CONTEXT"
            ))
            self.finish()
            return

        else:
            # There's no user in the system,
            # move on to a full registration

            # Create a root-level SSO record for the user
            sso_user = await registration.create_sso_user(
                email,
                password,
                name,
                org_id,
                account_id,
                "PENDING_VERIFICATION",
                org_name=organization_name
            )

            # Create an account-level context user for the user
            await registration.create_acc_context_user(
                acc_token,
                sso_user.get("id"),
                account_id,
                "PENDING",
                "USER",
                None,
                sso_user.get("id")
            )

            account = {}
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT name FROM _iw.accounts
                    WHERE tki = %s;
                """, (
                    acc_token,
                ))
                account = await cur.fetchone()

            await verification.send_email_verification(sso_user, acc_token, account.get('name', "Uknown"))

            self.write(dict(
                err=False,
                code="REGISTERED"
            ))
            self.finish()
            return


class RegistrationInterface(RequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        results = []

        param = data.get("param", None)
        action = data.get("action", None)

        if action == "organization":
            results = await registration.query_organizations(param)
        elif action == "account":
            results = await registration.query_accounts(param)

        self.write(json.dumps(results, cls=JSONEncoder))


class RegisterVerification(RequestHandler):
    """
        Verification API route
            Users verify their email address against this URI
    """

    async def get(self, verification_code):
        result = await verification.verify_user(verification_code)
        if result is True:
            self.render("verified_email.html")
        else:
            self.render("bad_code.html")


class RequestAccessRoute(BaseRequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))
        user = self.current_user

        account_id = data.get("account_id")
        acc_token = None

        # get the domain
        domain = self.request.host
        if ":" in domain:
            domain = domain.split(":")[0]

        # Fix for debugging build
        if domain == "127.0.0.1":
            domain = "ewars.ws"

        if domain != settings.ROOT_DOMAIN:
            acc_token = await registration.get_token_by_domain(domain)
        else:
            acc_token = await registration.get_token_by_id(account_id)

        authenticated = False
        # User isn't currently logged in, accepting email/password combo
        if user is None:
            password = data.get("password")
            email = data.get("email")

            # User didn't enter a password
            if password in (None, ""):
                self.write(dict(
                    err=True,
                    code="NO_PASSWORD"
                ))
                self.finish()
                return

            # User didn't enter an email address
            if email in (None, ""):
                self.write(dict(
                    err=True,
                    code="NO_EMAIL"
                ))
                self.finish()
                return

            # Authenticate the password against SSO
            auth_result = await authentication.authenticate_user(
                self.request.host,
                email,
                password
            )

            if auth_result.get("err", False):
                self.write(json.dumps(auth_result))
                self.finish()
                return
            else:
                sso_user = auth_result.get("user")
                # Check if there's a user for the context already
                ctx_user = await registration.has_account_user(email, acc_token)

                # This user is already in the account,
                # lets see why
                if ctx_user is not None:
                    # Access if currently pending approval
                    if ctx_user.get("status") == "PENDING":
                        self.write(dict(
                            err=True,
                            code="PENDING_APPROVAL"
                        ))
                        self.finish()
                        return

                    # Someone revoked their access
                    if ctx_user.get("status") == "DELETED":
                        self.write(dict(
                            err=True,
                            code="USER_REVOKED"
                        ))
                        self.finish()
                        return

                    # They already have an account and it seems to
                    # be perfectly fine
                    self.write(dict(
                        err=True,
                        code="USER_EXISTS"
                    ))
                    self.finish()
                    return
                else:
                    # Create tmp user in context tables
                    _res = await registration.create_acc_context_user(
                        acc_token,
                        sso_user.get("id"),
                        account_id,
                        "PENDING",
                        "USER",
                        None,
                        sso_user.get("id")
                    )

                    # Create task in acc
                    _res = registration.create_approval_task(
                        sso_user.get("id"),
                        acc_token
                    )

                    # We're done here, let the user know
                    # they need to wait for approval
                    self.write(dict(
                        err=False,
                        code="SUCCESS"
                    ))
                    self.finish()
                    return


        # user is logged in and we have access to their
        # current session
        else:
            # See if this user already belongs to the account
            ctx_user = await registration.has_account_user(user.get("email"), acc_token)

            if ctx_user is not None:
                # They have an account,it's awaiting admin approval
                if ctx_user.get("status") == "PENDING":
                    self.write(dict(
                        err=True,
                        code="PENDING_APPROVAL"
                    ))
                    self.finish()
                    return

                # They had access, but it was revoked
                if ctx_user.get("status") == "DELETED":
                    self.write(dict(
                        err=True,
                        code="USER_REVOKED"
                    ))
                    self.finish()
                    return

                # They already have a valid user in the account
                self.write(dict(
                    err=True,
                    code="USER_EXISTS"
                ))
                self.finish()
                return

            # They don't have a user in the context
            # create one
            else:
                # Create their context account
                _res = await registration.create_acc_context_user(
                    acc_token,
                    user.get("id"),
                    account_id,
                    "PENDING",
                    "USER",
                    None,
                    user.get("id")
                )

                # Create context access approval task
                _res = await registration.create_approval_task(
                    user.get("id"),
                    acc_token
                )

                # We're done here
                self.write(dict(
                    err=False,
                    code="SUCCESS"
                ))
                self.finish()
                return


class InvitationInterface(BaseRequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with,E_C_TYPE,Content-Type")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    async def get(self, invite_uuid):
        client = clients.get_client("invitation")
        # self.render_client(client, public=True)
        data = await self.render_client(client, public=True)

        invite = await invitations.get_invite(invite_uuid)

        if invite is None:
            self.send_error(status_code=404)
            self.finish()

        for key, value in invite.items():
            if isinstance(value, uuid.UUID):
                invite[key] = str(value)

        data['invitation'] = invite

        self.render("template.html", **data)

    async def post(self, invite_uuid):
        data = json.loads(self.request.body.decode("utf-8"))

        # Get and check the invite
        invite = await invitations.get_invite(invite_uuid)
        if invite is None:
            self.write(json.dumps(dict(
                err=True,
                code="INVITE_DOES_NOT_EXIST"
            )))
            self.finish()
            return

        # Check if this invite was based on creating a whole new user
        create_user = False
        if invite.get("user_id", None) is None:
            create_user = True

        # Create data for creating a new user
        if create_user:
            user_data = dict(
                name=data.get("name"),
                email=data.get("email"),
                org_id=data.get("org_id"),
            )

            # Create root-level user
            new_user = await registration.create_sso_user(
                data.get("email"),
                data.get("password"),
                data.get("name"),
                data.get("org_id"),
                invite.get("aid"),
                'ACTIVE'
            )

            # Create context user
            await registration.create_acc_context_user(
                invite.get("tki"),
                new_user.get("id"),
                invite.get("aid"),
                invite['details'].get("status", "ACTIVE"),
                invite['details'].get("role", "USER"),
                invite['details'].get("location", None),
                new_user.get('id')
            )

            # Add and activity feed item for the join
            await registration.add_activity_new_user(
                new_user.get("id"),
                [("USER", new_user.get("id"))],
                tki=invite.get("tki")
            )

            # Send the user a welcome email
            await registration.send_welcome_invite_create(
                new_user.get("id"),
                data.get("password"),
                tki=invite.get("tki")
            )

            await registration.delete_invite(invite_uuid)

            self.write(json.dumps(dict(
                err=False
            )))
            self.finish()
            return

        else:
            # We're mapping an existing user into the system

            # Create the context user record
            await registration.create_acc_context_user(
                invite.get("tki"),
                invite.get("user_id"),
                invite.get("aid"),
                invite['details'].get("status", "ACTIVE"),
                invite['details'].get("role", "USER"),
                invite['details'].get("location", None),
                invite.get("user_id")
            )

            # Create an activity feed item
            await registration.add_activity_new_user(
                invite.get("user_id"),
                [("USER", invite.get("user_id"))],
                tki=invite.get("tki")
            )

            # Send a welcome email to the user
            await registration.send_welcome_existing(
                invite.get("user_id"),
                invite.get("tki")
            )

            await registration.delete_invite(invite_uuid)

            self.write(json.dumps(dict(
                err=False
            )))
            self.finish()
            return


register_applications = [
    (r"/register", RegisterInterface),
    (r"/register/verify/(.*)", RegisterVerification),
    (r"/register/access", RequestAccessRoute),
    (r"/registration", RegistrationInterface),
    (r"/invitation/(.*)", InvitationInterface),
]
