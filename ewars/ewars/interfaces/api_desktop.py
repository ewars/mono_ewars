import json

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

from ewars.core.api import get_api
from ewars.core.interface import BaseRequestHandler
from ewars.constants import CONSTANTS
from ewars.conf import settings
from jose import jwt, JWTError

from tornado.web import RequestHandler

ALLOWED_HEADERS = [
    "x-requested-with",
    "Access-Control-Request-Method",
    "Content-Type",
    "Access-Control-Request-Headers",
    "x-forwarded-for",
    "content-type",
    "ec_token",
    "accept",
    "EC_TOKEN",
    "Ec_token",
    "Origin",
    "X-Auth-Token"
]


class Service(RequestHandler):
    SUPPORT_METHODS = ("CONNECT", "HEAD", "POST", "DELETE", "PATCH", "OPTIONS", "PUT")

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", ",".join(ALLOWED_HEADERS))
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Allow", "POST, GET, OPTIONS")

    def options(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", ",".join(ALLOWED_HEADERS))
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Allow", "POST, GET, OPTIONS")

        self.set_status(204)
        self.finish()

    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))

        method = data[0]
        args = data[1] or dict()

        api_version = self.request.headers.get("EAPI", 0)

        if method is None:
            self.send_error(status_code=404)

        # If no api version, default to 0
        if api_version is None:
            api_version = "0"

        if "Ec_token" in self.request.headers.keys():
            token = self.request.headers.get("Ec_token", None)

            if token is None and user is None:
                self.send_error(status_code=403)
            else:
                try:
                    user = jwt.decode(token, settings.JWT_KEY, algorithms=["HS256"])
                    if "user" in user.keys():
                        user = user.get("user")
                except Exception as e:
                    self.send_error(status_code=403)

        if user is None:
            self.send_error(status_code=403)
        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.users
                    WHERE id = %s;
                """, (
                    AsIs(user.get("tki")),
                    user.get("uid"),
                ))
                user = await cur.fetchone()

        _api = get_api(CONSTANTS.DESKTOP, str(api_version))

        kwargs = dict()
        kwargs['user'] = user
        kwargs['application'] = self.application
        args = (method, args)
        result = await _api.execute(args, kwargs)

        self.set_header("Content-Type", "application/json")
        self.write(result)
