import logging
import uuid
import json
import time
from select import select

from ewars.conf import settings

import tornado.websocket
import tornado.web
import tornado.escape
from tornado import ioloop

from ewars.conf import settings
from ewars.core.serializers import JSONEncoder
from ewars.core.authentication import get_encrypted_password
from jose import jwt, JWTError

from ewars.db import get_db_cur, get_db_cursor, get_plain_connect

from psycopg2.extensions import AsIs
import psycopg2.extensions

from ewars.models import Submission

"""
Store connected clients in the database as session when they connect
When a client disconnects, remove them from the table.

Periodically clear the table and force a restart to get all new clients.

Periodically have the class in the background check a queue of messages to see if there are any
if there are messages, iterates through the connected clients to find any matching users for the message
and send down a message to that connection.


"""

# Simple directory of tables to set up for the remote
LOADERS = dict(
    forms="SELECT * FROM %s.forms WHERE (SELECT EXTRACT(EPOCH FROM last_modified) * 1000) >= %s",
    form_versions="SELECT * FROM %s.form_versions WHERE (SELECT EXTRACT(EPOCH FROM last_modified) * 1000) >= %s;",
    location_types="SELECT * FROM %s.location_types WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    indicators="SELECT * FROM %s.indicators WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    indicator_groups="SELECT * FROM %s.indicator_groups WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    alarms="SELECT * FROM %s.alarms_v2 WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    # workflows="SELECT * FROM %s.workflows WHERE (SELECT EXTRACT(EPOCH FROM last_modified) * 1000) >= %s;",
    notebooks="SELECT * FROM %s.notebooks WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    huds="SELECT * FROM %s.huds WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    maps="SELECT * FROM %s.maps WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    devices="SELECT * FROM %s.devices WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    assignments="SELECT * FROM %s.assignments WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    layouts="SELECT * FROM %s.layouts WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    organizations="SELECT * FROM %s.orgs_full;",
    channels="SELECT * FROM %s.channels WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;",
    tasks="SELECT * FROM %s.tasks_v2 WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;"
)

# Simple directory of tables to set up for the remote
BASE_LOADERS = dict(
    forms="SELECT * FROM %s.forms",
    form_versions="SELECT * FROM %s.form_versions;",
    location_types="SELECT * FROM %s.location_types;",
    indicators="SELECT * FROM %s.indicators;",
    indicator_groups="SELECT * FROM %s.indicator_groups;",
    alarms="SELECT * FROM %s.alarms_v2;",
    # workflows="SELECT * FROM %s.workflows",
    notebooks="SELECT * FROM %s.notebooks",
    tasks="SELECT * FROM %s.tasks_v2 WHERE state = 'OPEN'",
    huds="SELECT * FROM %s.huds;",
    maps="SELECT * FROM %s.maps;",
    devices="SELECT * FROM %s.devices;",
    assignments="SELECT * FROM %s.assignments;",
    layouts="SELECT * FROM %s.layouts;",
    organizations="SELECT * FROM %s.orgs_full;",
    channels="SELECT * FROM %s.channels;"
)


async def update_channel(data, tki=None, sender=None):
    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO %s.channel_items (uuid, chid, item_type, content, created, ts_ms, uid)
            VALUES (%s, %s, %s, %s, %s, %s, %s);
        """, (
            AsIs(tki),
            data.get('uuid'),
            data.get('chid'),
            "MESSAGE",
            json.dumps(data.get('data')),
            data.get('created'),
            data.get('ts_ms'),
            data.get('uid'),
        ))

        await cur.execute("""
            NOTIFY sonoma, %s;
        """, (
            json.dumps({
                "op": 0,
                "t": "CHANNEL_MESSAGE",
                "tki": tki,
                "d": data
            }),
        ))


async def submit_record(data, tki=None, uid=None, waiter=None):
    user = waiter.get_user(tki, uid)
    if user is not None:
        user["id"] = uid
        result = await Submission.submit(
            data.get("form_id"),
            data.get("form_version_id"),
            data,
            source="DESKTOP",
            user=user
        )
        print(result)


async def peer_message(data):
    """ Send a notify on a peer message for private chats

    Args:
        data:

    Returns:

    """
    payload = dict(
        t="PEER_MESSAGE",
        op=0,
        d=data
    )
    async with get_db_cur() as cur:
        cur.execute("""
            NOTIFY sonoma, %s;
        """, (
            json.dumps(payload),
        ))


async def rx_events(data, tki=None, uid=None, waiter=None):
    if data is not None:
        print(data)
        for item in data:
            print(item)

    if waiter is not None:
        waiter.write_message(json.dumps(dict(
            op=0,
            t="NODE_PUSH_RESULT",
            d=dict(
                uid=uid,
                tki=tki,
                d=True,
                errs=[]
            )
        )))


async def rx_resources(data, tki=None, uid=None, waiter=None):
    print(data)
    ts_ms = data.get('ts_ms', 0)

    payload = dict()
    permissions = []
    user_details = None
    locations = []
    perm_manifest = {}

    async with get_db_cur() as cur:
        ts_tuple = (AsIs(tki), ts_ms,)
        nonts_tuple = (AsIs(tki),)
        for key, query in LOADERS.items():
            q = nonts_tuple
            if 'modified' in query:
                q = ts_tuple

            if 'last_modified' in query:
                q = ts_tuple

            await cur.execute(
                query,
                q
            )
            items = await cur.fetchall()
            payload[key] = [dict(x) for x in items]

        await cur.execute("""
            SELECT l.uuid, l.name, l.site_type_id, l.lineage, l.parent_id, l.status, l.groups,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.location_reporting AS t WHERE t.location_id = l.uuid) AS reporting
            FROM %s.locations AS l
            WHERE (SELECT EXTRACT(EPOCH FROM l.modified) * 1000) >= %s;
        """, (
            AsIs(tki),
            AsIs(tki),
            ts_ms,
        ))
        locations = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.users
            WHERE id = %s
            AND (SELECT EXTRACT(EPOCH FROM last_modified) * 1000) >= %s;
        """, (
            AsIs(tki),
            uid,
            ts_ms,
        ))
        user_details = await cur.fetchone()

        await cur.execute("""
            SELECT * FROM %s.assignments
            WHERE user_id = %s
            AND (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s;
        """, (
            AsIs(tki),
            uid,
            ts_ms,
        ))
        permissions = await cur.fetchall()

        for perm in permissions:
            is_interval = False
            is_location = False
            for form in payload['forms']:
                if form.get('id') == perm.get('form_id'):
                    if form.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
                        is_interval = True

                    if 'LOCATION_REPORTING' in form.get('features', {}).keys():
                        is_location = True

            topic_key = str(perm.get('form_id'))
            if is_location:
                topic_key += '_' + str(perm.get('location_id'))

        await cur.execute("""
            SELECT u.*,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.assignments AS t WHERE t.user_id = u.id AND t.status = 'ACTIVE') AS assignments
            FROM %s.users AS u;
        """, (
            AsIs(tki),
            AsIs(tki),
        ))
        users = await cur.fetchall()

        await cur.execute("""
            SELECT l.uuid, l.name, l.site_type_id, l.lineage, l.parent_id, l.groups, l.status,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.location_reporting AS t WHERE t.location_id = l.uuid AND t.status = 'ACTIVE') AS reporting
            FROM %s.locations AS l
            WHERE (SELECT EXTRACT(EPOCH FROM l.modified) * 1000) >= %s;
        """, (
            AsIs(tki),
            AsIs(tki),
            ts_ms,
        ))
        locations = await cur.fetchall()

        payload["user"] = user_details
        payload['users'] = users
        payload['locations'] = locations
        payload["permissions"] = permissions

    manifests = []
    for key, val in perm_manifest.items():
        manifests.append(dict(
            id=key,
            records=val,
            ts_ms=int(time.time())
        ))

    payload["uid"] = uid
    payload["tki"] = tki

    if waiter is not None:
        waiter.write_message(json.dumps(dict(
            op=0,
            t="NODE_RES_UPDATES",
            d=payload
        ), cls=JSONEncoder))


async def tx_baseline(data, tki=None, uid=None, waiter=None):
    ts_ms = int(time.time())

    payload = dict()

    async with get_db_cur() as cur:
        for key, query in BASE_LOADERS.items():
            await cur.execute(
                query,
                (
                    AsIs(tki),
                )
            )
            items = await cur.fetchall()
            payload[key] = [dict(x) for x in items]

        await cur.execute("""
            SELECT l.uuid, l.name, l.site_type_id, l.lineage, l.parent_id, l.status, l.groups,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.location_reporting AS t WHERE t.location_id = l.uuid) AS reporting
            FROM %s.locations AS l;
        """, (
            AsIs(tki),
            AsIs(tki),
        ))
        locations = await cur.fetchall()
        payload["locations"] = locations

        await cur.execute("""
            SELECT * FROM %s.users
            WHERE id = %s;
        """, (
            AsIs(tki),
            uid,
        ))
        user_details = await cur.fetchone()
        payload["user"] = user_details

        await cur.execute("""
            SELECT * FROM %s.assignments
            WHERE user_id = %s;
        """, (
            AsIs(tki),
            uid,
        ))
        permissions = await cur.fetchall()

        for perm in permissions:
            await cur.execute("""
                SELECT features 
                FROM %s.forms
                WHERE id = %s;
            """, (
                AsIs(tki),
                perm.get("form_id"),
            ))
            form = await cur.fetchone()

            features = form.get("features", {})
            if features.get("INTERVAL_REPORTING", None) is not None:

                if perm.get("location_id", None) is not None:
                    await cur.execute("""
                        SELECT DISTINCT(data_date) AS data_date
                        FROM %s.collections
                        WHERE form_id = %s
                          AND location_id = %s
                          AND status = 'SUBMITTED'
                        ORDER BY data_date DESC;
                    """, (
                        AsIs(tki),
                        perm.get('form_id'),
                        perm.get('location_id'),
                    ))
                else:
                    await cur.execute("""
                        SELECT DISTINCT(data_date) AS data_date
                        FROM %s.collections
                        WHERE form_id = %s
                          AND status = 'SUBMITTED';
                    """, (
                        AsIs(tki),
                        perm.get("form_id"),
                    ))
                dates = await cur.fetchall()

                perm["manifest"] = [x.get("data_date") for x in dates]

        payload["permissions"] = permissions

        await cur.execute("""
            SELECT u.*,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.assignments AS t WHERE t.user_id = u.id AND t.status = 'ACTIVE') AS assignments
            FROM %s.users AS u;
        """, (
            AsIs(tki),
            AsIs(tki),
        ))
        users = await cur.fetchall()
        payload["users"] = users

    if waiter is not None:
        waiter.write_message(json.dumps(dict(
            op=0,
            t="RX_BASELINE",
            tki=tki,
            uid=uid,
            d=dict(
                tki=tki,
                uid=uid,
                d=payload
            )
        ), cls=JSONEncoder))


async def rx_seed(data, tki=None, uid=None, waiter=None):
    results = []

    data_type = data.get('type', None)
    data_id = data.get('id', None)
    period = data.get("period", None)


async def rx_pull(data, tki=None, uid=None, waiter=None):
    ts_ms = data.get('ts_ms')
    payload = dict(
        user=None,
        permissions=None
    )

    user = None

    records = []
    alerts = []
    tasks = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.users
            WHERE id = %s;
        """, (
            AsIs(tki),
            uid,
        ))
        user = await cur.fetchone()

        await cur.execute("""
         SELECT t.* 
         FROM (
            SELECT DISTINCT ON (key) *
            FROM %s.topics 
            WHERE topic ILIKE 'records_%%'
            AND ts_ms >= %s
            ORDER BY key DESC
         ) t
         ORDER BY t.ts_ms ASC;
        """, (
            AsIs(tki),
            ts_ms,
        ))
        records = await cur.fetchall()

        await cur.execute("""
            SELECT t.* 
            FROM (
                SELECT DISTINCT ON (key) *
                FROM %s.topics
                WHERE topic ILIKE 'alerts_%%'
                AND ts_ms >= %s
                ORDER BY key DESC
            ) t
            ORDER BY t.ts_ms ASC;
        """, (
            AsIs(tki),
            ts_ms,
        ))
        alerts = await cur.fetchall()

        await cur.execute("""
          SELECT *
          FROM %s.tasks_v2
          WHERE (SELECT EXTRACT(EPOCH FROM modified) * 1000) >= %s
          AND state = 'OPEN';
        """, (
            AsIs(tki),
            ts_ms,
        ))
        tasks = await cur.fetchall()

    if waiter is not None:
        waiter.write_message(json.dumps(dict(
            op=0,
            t="NODE_MANIFEST_RECEIVED",
            d=dict(
                tki=tki,
                uid=uid,
                alerts=alerts,
                records=records,
                tasks=tasks
            )
        ), cls=JSONEncoder))


async def tx_context_seed(data, tki=None, uid=None, waiter=None):
    user = waiter.get_user(tki, uid)

    if user.get("role") == "USER":
        await _tx_context_seed_user(data, tki=tki, uid=uid, waiter=waiter)
    elif user.get("role") == "ACCOUNT_ADMIN":
        await _tx_context_seed_admin(data, tki=tki, uid=uid, waiter=waiter)
    else:
        pass


async def _tx_context_seed_admin(data, tki=None, uid=None, waiter=None):
    results = []

    fid = data.get("fid", None)

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM %s.collections
            WHERE status = ANY(ARRAY['SUBMITTED', 'PENDING_AMEND', 'PENDING_AMENDMENT'])
            AND form_id = %s;
        """, (
            AsIs(tki),
            fid,
        ))
        results = cur.fetchall()

    if waiter is not None:
        waiter.write_message(json.dumps(dict(
            op=0,
            t='RX_CONTEXT_SEED',
            tki=tki,
            uid=uid,
            d=dict(
                uid=uid,
                tki=tki,
                fid=fid,
                records=results
            )
        ), cls=JSONEncoder))


async def _tx_context_seed_user(data, tki=None, uid=None, waiter=None):
    results = []

    fid = data.get('fid', None)
    lid = data.get('lid', None)

    parameters = None
    query = """
         SELECT * FROM %s.collections
         WHERE form_id = %s
          AND status = ANY(ARRAY['SUBMITTED', 'PENDING_AMEND', 'PENDING_AMENDMENT'])
    """

    if lid is not None:
        parameters = (AsIs(tki), fid, lid,)
        query = query + ' AND location_id = %s'
    else:
        parameters = (AsIs(tki), fid,)

    query = query + " ORDER BY data_date DESC "
    query = query + " LIMIT 10000"

    with get_db_cursor() as cur:
        cur.execute(query, parameters)
        results = cur.fetchall()

    if waiter is not None:
        waiter.write_message(json.dumps(dict(
            op=0,
            t='RX_CONTEXT_SEED',
            tki=tki,
            uid=uid,
            d=dict(
                uid=uid,
                tki=tki,
                fid=fid,
                lid=lid,
                records=results
            )
        ), cls=JSONEncoder))


async def tx_alarm_seed(data, tki=None, uid=None, waiter=None):
    results = []
    assignments = []
    user = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * from %s.users
            WHERE id = %s;
        """, (
            AsIs(tki),
            uid,
        ))
        user = cur.fetchone()

        if user.get("role") == "USER":
            cur.execute("""
                SELECT location_id 
                FROM %s.assignments
                WHERE user_id = %s
                AND status = 'ACTIVE';
            """, (
                AsIs(tki),
                uid,
            ))
            assignments = cur.fetchall()
            assignments = [x.get("location_id", None) for x in assignments if x.get("location_id", None) is not None]

            if len(assignments) > 0:
                cur.execute("""
                    SELECT a.*, 
                       (SELECT json_agg(row_to_json(t.*)) FROM %s.alert_events AS t WHERE t.alert_id = a.uuid) AS events
                       FROM %s.alerts AS a
                       WHERE a.location_id::TEXT = ANY(%s);
                """, (
                    AsIs(tki),
                    AsIs(tki),
                    assignments,
                ))
                results = cur.fetchall()
            else:
                assignments = []

        if user.get("role") == 'REGIONAL_ADMIN':
            cur.execute("""
                SELECT a.*,
                (SELECT json_agg(row_to_json(t.*)) FROM %s.alert_events AS t WHERE t.alert_id = a.uuid) AS events
                FROM %s.alerts AS a
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                WHERE l.lineage::TEXT[] @> %s::TEXT[],
            """, )(
                AsIs(tki),
                AsIs(tki),
                [user.get('location_id')],
            )
            results = cur.fetchall()

        if user.get("role") == "ACCOUNT_ADMIN":
            cur.execute("""
                SELECT a.*,
                    (SELECT json_agg(row_to_json(t.*)) FROM %s.alert_events AS t WHERE t.alert_id = a.uuid) AS events
                FROM %s.alerts AS a;
            """, (
                AsIs(tki),
                AsIs(tki),
            ))
            results = cur.fetchall()

    if waiter is not None:
        waiter.write_message(json.dumps(dict(
            op=0,
            t='RX_ALARM_SEED',
            tki=tki,
            uid=uid,
            d=dict(
                uid=uid,
                tki=tki,
                records=results
            )
        ), cls=JSONEncoder))


async def rx_auth_request(data, tki=None, uid=None, waiter=None):
    email = data.get("email", None)
    password = data.get("password", None)

    tenancies = []

    if email == "" or email is None:
        waiter.write_message(dict(
            op=0,
            t="AUTH_RESULT",
            d=dict(
                success=False,
                res="NO_EMAIL"
            )
        ))
        return

    if password == "" or password is None:
        waiter.write_message(dict(
            op=0,
            t="AUTH_RESULT",
            d=dict(
                success=False,
                res="NO_PASS"
            )
        ))
        return

    user = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.users
            WHERE email = %s
              AND status = 'ACTIVE';
        """, (
            email,
        ))
        user = await cur.fetchone()

    if user is None:
        waiter.write_message(dict(
            op=0,
            t="AUTH_RESULT",
            d=dict(
                success=False,
                res="NO_USER"
            )
        ))
        return

    enc_pass = user.get("password", None)
    pass_components = enc_pass.split(":")
    crypted = get_encrypted_password(password, pass_components[1])

    authenticated = crypted == pass_components[0]

    if not authenticated:
        waiter.write_message(dict(
            op=0,
            t="AUTH_RESULT",
            d=dict(
                success=False,
                res="BAD_AUTH"
            )
        ))
        return

    accounts = user.get("accounts", [])

    if len(accounts) <= 0:
        waiter.write_message(dict(
            op=0,
            t="AUTH_RESULT",
            d=dict(
                success=False,
                res="NO_TENANT_ACCESS"
            )
        ))
        return

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.accounts
            WHERE id = ANY(%s)
              AND status = 'ACTIVE';
        """, (
            accounts,
        ))
        accounts = await cur.fetchall()

    if len(accounts) <= 0:
        waiter.write_message(dict(
            op=0,
            t="AUTH_RESULT",
            d=dict(
                success=False,
                res="NO_ACTIVE_TENANTS"
            )
        ))
        return

    for account in accounts:
        tenant_user = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.users
                WHERE id = %s;
            """, (
                AsIs(account.get("tki")),
                user.get("id"),
            ))
            tenant_user = await cur.fetchone()

        if tenant_user.get("status") == 'ACTIVE':
            tenant_ob = dict(
                uid=tenant_user.get('id'),
                name=tenant_user.get("name"),
                email=tenant_user.get("email"),
                account_name=account.get("name"),
                domain=account.get("domain"),
                role=tenant_user.get("role"),
                status=account.get("status"),
                lid=tenant_user.get("location_id", None),
                tki=account.get("tki"),
                token=jwt.encode(dict(
                    user=dict(
                        uid=tenant_user.get("id"),
                        name=tenant_user.get("name"),
                        email=tenant_user.get("email"),
                        account_name=account.get("name"),
                        domain=account.get("domain"),
                        role=tenant_user.get("role"),
                        status=tenant_user.get("status"),
                        lid=tenant_user.get("location_id", None),
                        tki=account.get("tki")
                    )
                ),
                    settings.JWT_KEY,
                    algorithm="HS256"
                )
            )
            tenancies.append(tenant_ob)

    if len(tenancies) <= 0:
        waiter.write_message(dict(
            op=0,
            t="AUTH_RESULT",
            d=dict(
                success=False,
                res="NO_ACTIVE_TENANCIES"
            )
        ))
        return

    waiter._users = waiter._users + tenancies

    waiter.write_message(json.dumps({
        "op": 0,
        "t": "READY",
        "d": {
            "heartbeat_interval": 30 * 1000,
            "v": 1
        }
    }))

    tkis = list(set([x.get("tki") for x in waiter._users]))
    res = await waiter.write_users(tkis)
    res = await waiter.write_channels()

    waiter.write_message(json.dumps(dict(
        op=0,
        t="AUTH_RESULT",
        d=dict(
            success=True,
            tenancies=tenancies
        )
    ), cls=JSONEncoder))


async def rx_request_assign(data, tki=None, uid=None, waiter=None):
    """ Receive an assignment request

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def rx_task_action(data, tki=None, uid=None, waiter=None):
    """ Receive an update to a task

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def retract_record(data, tki=None, uid=None, waiter=None):
    """ Receive a request to retract a record

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def amend_record(data, tki=None, uid=None, waiter=None):
    """ Receive an amendment for a record

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def rx_alert_update(data, tki=None, uid=None, waiter=None):
    """ Receive an update(s) to am alert

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def rx_version_check(data, tki=None, uid=None, waiter=None):
    """ Receive a desktop application version check for updates

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def rx_register_request(data, tki=None, uid=None, waiter=None):
    """ REceive a registration request from desktop application

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def rx_access_request(data, tki=None, uid=None, waiter=None):
    """ REceive a request for access to an account

    Args:
        data:
        tki:
        uid:
        waiter:

    Returns:

    """
    pass


async def rx_org_request(data, tki=None, uid=None, waiter=None):
    pass


commands = {
    "RECORD_NEW": submit_record,
    "RECORD_DELETE": retract_record,
    "RECORD_UPDATE": amend_record,
    "ALERT_UPDATE": rx_alert_update,
    "CHANNEL_MESSAGE": update_channel,
    "PEER_MESSAGE": peer_message,
    "PEER_MESSAGE_UPDATE": None,
    "PEER_MESSAGE_DELETE": None,
    "NODE_PUSH": rx_events,
    "NODE_MANIFEST": rx_pull,
    "NODE_RESOURCES": rx_resources,
    "GET_BASE": tx_baseline,
    "GET_CONTEXT_SEED": tx_context_seed,
    "GET_ALARMS_SEED": tx_alarm_seed,
    "AUTH": rx_auth_request,
    "REQUEST_ASSIGN": rx_request_assign,
    "REGISTER": rx_register_request,
    "ACCESS_REQUEST": rx_access_request,
    "TASK_ACTION": rx_task_action,
    "VERSION_CHECK": rx_version_check,
    "ORG_REQUEST": rx_org_request
}


class SonomaDesktopSocket(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = []
    cache_size = 200
    channels = {}
    cur = None
    conn = None
    listener = None
    _io_loop = None

    def __init__(self, *args, **kwargs):
        super(SonomaDesktopSocket, self).__init__(*args, **kwargs)
        SonomaDesktopSocket.listen()

    @classmethod
    def listen(cls):
        if cls.listener is None:
            cls.conn = psycopg2.connect(
                database=settings.DB_NAME,
                user=settings.DB_USER,
                password=settings.DB_PASSWORD,
                host=settings.DB_HOST,
                port=settings.DB_PORT
            )
            cls.conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            cls.listener = cls.conn.cursor()
            cls.listener.execute("LISTEN sonoma")

            io_loop = tornado.ioloop.IOLoop.current()
            cls._io_loop = io_loop
            cls._io_loop.add_handler(cls.conn.fileno(), SonomaDesktopSocket._io_callback, tornado.ioloop.IOLoop.READ)

    @classmethod
    def _io_callback(cls, *args):
        state = cls.conn.poll()
        if state == psycopg2.extensions.POLL_OK:
            while cls.conn.notifies:
                notify = cls.conn.notifies.pop()
                data = json.loads(notify.payload)

                if data.get("tki", None) is not None:
                    for waiter in SonomaDesktopSocket.waiters:
                        if waiter.has_tki(data.get('tki')):
                            waiter.write_message(json.dumps(dict(
                                op=0,
                                t=data.get("event_type"),
                                tki=data.get("tki"),
                                d=dict(
                                    data=data.get("data"),
                                    tki=data.get("tki")
                                )
                            ), cls=JSONEncoder))
        elif state == psycopg2.extensions.POLL_ERROR:
            print("POLL ERRROR")

        SonomaDesktopSocket.listen()

    @classmethod
    def handle_event(cls, fd, events):
        state = cls.conn.poll()
        if state == psycopg2.extensions.POLL_OK:
            while cls.conn.notifies:
                notify = cls.conn.notifies.pop()
                print("GOT NOTIFY: ", notify.pid, notify.channel, notify.payload)
        elif state == psycopg2.extensions.POLL_READ:
            print("HERE")

    @classmethod
    def recv_notify(cls):
        print("HERE")

    @classmethod
    async def shout(cls, group, message):
        print(group, message)

    @classmethod
    async def whisper(cls, uid, message):
        print(uid, message)

    @classmethod
    async def send_channel(cls, chid, uid, message):
        print(chid, uid, message)

    @classmethod
    async def update_chan_msg(cls, msgid, message):
        print(msgid, message)

    @classmethod
    async def delete_chan_msg(cls, msgid):
        print(msgid)

    def get_user(self, tki, uid):
        result = None

        for user in self._users:
            if user.get("tki") == tki and user.get("uid") == int(uid):
                result = user

        return result

    def get_compressions_options(self):
        # NOne-None enables compressions with default options
        return {}

    def check_origin(self, origin):
        return True

    def open(self):
        self.authenticated = None
        self._users = []
        self._tki = None
        self._did = None
        self._channels = []
        self._ts_ms = None
        SonomaDesktopSocket.waiters.add(self)

    def on_close(self):
        SonomaDesktopSocket.waiters.remove(self)

    def has_tki(self, tki):
        """ Check if a socket connection
        has access to a specific account

        Args:
            tki: The account token to check

        Returns:

        """
        has = False
        for user in self._users:
            if user.get("tki") == tki:
                has = True

        return has

    async def write_channels(self):
        accounts = list(set(x.get("tki") for x in self._users))

        channels = dict((x, []) for x in accounts)
        for key, acc_channels in channels.items():
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.channels;
                """, (
                    AsIs(key),
                ))
                acc_channels = await cur.fetchall()

        self.write_message(json.dumps({
            "op": 2,
            "t": "SEED_CHANNELS",
            'd': channels
        }))

    async def write_users(self, tkis):
        all_users = []
        for waiter in SonomaDesktopSocket.waiters:
            all_users = all_users + [x for x in waiter._users if x.get('tki') in tkis]

        self.write_message(json.dumps(dict(
            op=0,
            t="ACTIVES",
            d={
                "USERS": all_users
            }
        )))

    async def on_message(self, message):
        logging.info("Got message %r", message)
        parsed = tornado.escape.json_decode(message)

        op_code = parsed.get("op", None)
        payload = parsed.get("d", {})

        print(message)

        if op_code is None:
            return

        if int(op_code) == 1:
            print("RECV HEARTBEAT")
            tkis = list(set([x.get("tki") for x in self._users]))
            res = await self.write_users(tkis)

        if int(op_code) == 2:
            # authentication request
            tokens = payload.get("token", "")
            self._ts_ms = payload.get("ts_ms", int(round(time.time() * 1000)))
            tokens = tokens.split(',')

            if len(tokens) > 0:
                for token in tokens:
                    try:
                        user = jwt.decode(token, settings.JWT_KEY, algorithms=["HS256"])
                        user = user.get("user", {})
                        self._users.append(user)
                    except Exception as e:
                        pass

                if len(self._users) > 0:
                    self.write_message(json.dumps({
                        "op": 0,
                        "t": "READY",
                        "d": {
                            "heartbeat_interval": 30 * 1000,
                            "v": 1
                        }
                    }))

                    tkis = list(set([x.get("tki") for x in self._users]))
                    res = await self.write_users(tkis)
                    res = await self.write_channels()

            else:
                return

        if int(op_code) == 0:
            event = parsed.get("t", None)

            tki = parsed.get('tki', None)
            uid = parsed.get("uid", None)

            if event in commands.keys():
                result = await commands.get(event)(payload, tki=tki, uid=uid, waiter=self)
            else:
                self.write_message(dict(
                    op=9,
                    t="NO_METHOD",
                    d={}
                ))


socket_mob_routes = [
    (r'/ws/mobile', SonomaDesktopSocket),
]
