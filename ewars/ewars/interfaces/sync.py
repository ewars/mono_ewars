import json
import os
import re
import sys
import uuid
import datetime

from jose import jwt, JWTError

# Jerry rigged for running on localhost
from ewars.conf import settings
from ewars.core.serializers import JSONEncoder

from ewars.models import User
from ewars.core.sync import legacy
from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

import tornado
import tornado.web
from ewars.core.interface import BaseRequestHandler

"""
# Login error codes
0 - Incorrect Email/Password
1 - Account Inactive/Awaiting Approval
2 - Unknown Error
3 - Email Missing
4 - Password Missing
"""

def get_full_user(user_id):
    """
    Retrieve a full users account
    :param user_id: {int} The ID of the user to retrieve
    :return:
    """
    result = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM _iw.users WHERE id = %s
        """, (
            user_id,
        ))
        result = cur.fetchone()

    return result


class SyncService(BaseRequestHandler):
    async def post(self):
        data = json.loads(self.request.body.decode("utf-8"))
        authenticated = False

        manifest = data.get("manifest")
        action_data = manifest.get("data")
        platform = manifest.get("platform", None)
        sync_version = manifest.get("sync_version", None)
        device_id = manifest.get("device_id", None)
        action = manifest.get("action", None)
        phone = manifest.get("phone", None)
        lat = manifest.get("lat", None)
        lng = manifest.get("lng", None)
        version_code = manifest.get("versionCode", None)
        version_name = manifest.get("versionName", None)
        android_version = manifest.get("androidVersion", None)
        device = manifest.get("device", None)

        user_data = None
        if "token" in data:
            token = data.get("token")
            try:
                user_data = jwt.decode(token, settings.JWT_KEY, algorithms=["HS256"])
                authenticated = True
            except Exception as e:
                print(str(e))
        else:
            authenticated = False

        # Check if the user has a tki in their session
        if user_data is not None:
            if "tki" not in user_data.keys():
                print(user_data)
                aid = None
                user_id = None
                if "user" in user_data.keys():
                    tmp_user = user_data.get("user", dict())
                    user_id = tmp_user.get("id")
                    if "aid" in tmp_user.keys():
                        aid = tmp_user.get("aid", None)

                    if "account_id" in tmp_user.keys() and aid is None:
                        aid = tmp_user.get("account_id", None)

                    if "account" in tmp_user.keys() and aid is None:
                        aid = tmp_user['account'].get("id", None)
                else:
                    user_id = user_data.get("id")
                    if "aid" in user_data.keys():
                        aid = user_data.get("aid", None)

                    if "account_id" in user_data.keys() and aid is None:
                        aid = user_data.get("account_id", None)

                    if "account" in user_data.keys() and aid is None:
                        aid = user_data['account'].get("id", None)

                account = None
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT tki 
                        FROM _iw.accounts 
                        WHERE id = %s; 
                    """, (
                        aid,
                    ))
                    account = await cur.fetchone()

                    await cur.execute("""
                         SELECT * FROM %s.users
                         WHERE id = %s;
                    """, (
                        AsIs(account.get("tki")),
                        user_id,
                    ))
                    user_data = await cur.fetchone()
            else:
                if "user" in user_data.keys():
                    user_data = get_full_user(user_data['user']['id'])
                else:
                    if "id" in user_data.keys():
                        user_data = get_full_user(user_data['id'])
                    else:
                        user_data = None
                        authenticated = False

        if authenticated:
            device_update = await legacy.update_device_info(
                device_id=device_id,
                platform=platform,
                sync_version=sync_version,
                phone=phone,
                lat=lat,
                lng=lng,
                version_code=version_code,
                version_name=version_name,
                android_version=android_version,
                device=device,
                user=user_data
            )
            sync_result = await legacy.perform_sync(action, action_data, user=user_data, device_id=device_id)
            self.write(json.dumps(dict(
                    result=True,
                    data=sync_result
            ), cls=JSONEncoder))
        else:
            self.send_error(status_code=400)

# new_route = "%s%s" % (CONFIG.get("api_prefix"), "/sync/android/v2")
