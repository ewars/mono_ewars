import json

from tornado.web import RequestHandler, asynchronous, os, HTTPError
import tornado

from ewars.core.interface import BaseRequestHandler
from ewars.core import clients
from ewars.conf import settings
from ewars.core.serializers import JSONEncoder

from ewars.utils import markdown

from ewars.models import Site

def encoder(object):
    return json.dumps(object, cls=JSONEncoder)

def render_menu(items, root):
    result_string = ""

    for item in items:
        menu_item_string = '<div class="menu-item">'
        menu_item_string += """
                    <div class="menu-label">
                        <a href="%s%s">%s</a>
                    </div>
                """ % (
            root,
            item.get("sef"),
            item.get("title"),
        )

        if item.get("children", None) is not None:
            menu_item_string += '<div class="menu-children">'
            menu_item_string += render_menu(
                item.get("children"),
                root
            )

            menu_item_string += '</div>'

        menu_item_string += "</div>"

        result_string += menu_item_string

    return result_string


class SiteViewInterface(BaseRequestHandler):
    @tornado.web.asynchronous
    def get(self, route):

        components = route.split("/")
        site_id = components[0]

        page = None
        if len(components) <= 1:
            page = Site.get_site_root(site_id)
        else:
            page = Site.get_site_page(site_id, components[-1:][0])

        content = None
        page_type = page.get("page_type", None)
        if page_type == "MARKDOWN":
            content = markdown.markdown(
                page['definition'].get("content"),
                extras=[
                    'fenced-code-blocks',
                    'footnotes',
                    'numbering',
                    'header-ids',
                    'nofollow',
                    'tables'
                ])
        elif page_type == "RICH_TEXT":
            content = page['definition'].get("content")

        site = Site.get_site(site_id)
        site_menu = Site.get_site_menu(site_id)

        root = "/site/%s/" % (site_id,)

        nest_menu = dict((x.get("id"), x) for x in site_menu)

        for key, menu_item in nest_menu.items():
            if menu_item.get("parent_id", None) is not None:
                try:
                    nest_menu[menu_item.get("parent_id")]['children'].append(menu_item)
                except KeyError:
                    nest_menu[menu_item.get("parent_id")]['children'] = [menu_item]

        menu_items = [x for x in nest_menu.values() if x.get("parent_id", None) is None]
        menu = render_menu(menu_items, root)

        ewars_client = clients.get_client("ewars")
        report_client = clients.get_client("site")

        ewars_version = ewars_client.get("version")
        if settings.DEBUG:
            ewars_version = "dev"
        ewars_file = 'ewars-%s' % (ewars_version,)

        client_version = report_client.get("version")
        if settings.DEBUG:
            client_version = "dev"
        client_file = '%s-%s' % (report_client.get("token"), client_version)

        root = "/site/%s/" % (site_id,)

        self.render("site.html",
                    site=dict(site),
                    page=dict(page),
                    page_type=page_type,
                    debug=settings.DEBUG,
                    menu=menu,
                    route=root,
                    encoder=encoder,
                    render_menu=render_menu,
                    content=content,
                    ewars_file=ewars_file,
                    client_file=client_file)


site_uris = [
    (r"/site/(.*)", SiteViewInterface)
]
