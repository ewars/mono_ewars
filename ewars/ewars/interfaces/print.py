import json
import datetime
import asyncio
import uuid
from operator import itemgetter

from ewars.conf import settings
from ewars.constants import CONSTANTS
from ewars.db import get_db_cur
from ewars.core.serializers import JSONEncoder
from ewars.core.interface import BaseRequestHandler
from ewars.utils import date_utils

from ewars.core import registration, authentication, verification, clients, invitations

import tornado
from tornado.web import RequestHandler
import tornado.gen

from psycopg2.extensions import AsIs


class PrintRecordService(BaseRequestHandler):
    async def get(self, res_id):
        record = None

        user = self.current_user

        if user is None:
            self.send_error(status_code=400)

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT c.*, l.name AS location_name, u.name AS user_name, u.email AS user_email
                FROM %s.collections AS c 
                  LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                  LEFT JOIN %s.users AS u ON u.id = c.created_by
                WHERE c.uuid = %s;
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                res_id,
            ))
            record = await cur.fetchone()

        if record is None:
            self.send_error(status_code=404)

        form = None
        version = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.*
                FROM %s.forms AS f 
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                record.get('form_id'),
            ))
            form = await cur.fetchone()

            await cur.execute("""
                SELECT * 
                FROM %s.form_versions
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                record.get('form_version_id'),
            ))
            version = await cur.fetchone()

        location_name = None
        if record.get("location_name", None) is not None:
            location_name = record.get("location_name", {}).get("en", "No Name")

        # Parse out the fields into a set of fields and values
        fields = []

        reporting_interval = form.get('features', {}).get('INTERVAL_REPORTING', {}).get('interval', 'DAY')
        record_date = record.get('data_date', None)
        if record_date is not None:
            record_date = date_utils.display(record_date, reporting_interval)

        record_data = record.get('data', {})
        for key, value in version.get('definition', {}).items():
            disp_value = ''
            label = value.get('label', 'No label')
            order = int(value.get('order', 0))
            childs = []
            value_type = value.get('type')
            if order in (None, ''):
                order = 0

            if value.get("type") == 'text':
                disp_value = record_data.get(key, '')
            elif value.get('type') == 'textarea':
                disp_value = record_data.get(key, '')
            elif value.get('type') == 'date':

                disp_value = record_data.get(key, '')
            elif value.get("type") == 'select':
                options = value.get('options', [])
                tmp_value = record_data.get(key, '')
                disp_value = ''
                if isinstance(tmp_value, list):
                    for item in options:
                        if item[0] in tmp_value:
                            if disp_value == '':
                                disp_value = item[1]
                            else:
                                disp_value = disp_value + ', ' + item[1]
                else:
                    for item in options:
                        if item[0] == tmp_value:
                            disp_value = item[1]
            elif value.get('type') == 'header':
                value_type = value.get('headerStyle', 'style-title')
            elif value.get('type') == 'number':
                disp_value = record_data.get(key, '')
            elif value.get('type') == 'matrix':
                disp_value = None

                sub_rows = []

                for row_key, row in value.get('fields', {}).items():
                    row_label = row.get('label', None)
                    if isinstance(row_label, dict):
                        row_label = row_label.get('en')
                    sub_row = [
                        row.get('order'),
                        row_label,
                        None,
                        'ROW',
                        []
                    ]

                    cells = []

                    for cell_key, cell in row.get('fields', {}).items():
                        field_key = "%s.%s.%s" % (
                            key,
                            row_key,
                            cell_key,
                        )
                        label = cell.get('label', None)
                        if isinstance(label, dict):
                            label = label.get('en', 'No label')

                        cell_value = record_data.get(field_key, '')

                        cells.append([
                            cell.get('order'),
                            label,
                            cell_value,
                            cell.get('type')
                        ])

                    cells = sorted(cells, key=itemgetter(0))
                    sub_row[4] = cells

                    sub_rows.append(sub_row)

                sub_rows = sorted(sub_rows, key=itemgetter(0))
                childs = sub_rows

            if isinstance(label, dict):
                label = label.get('en', 'No label')

            fields.append([
                order,
                label,
                disp_value,
                value_type,
                childs
            ])

        fields = sorted(fields, key=itemgetter(0))

        data = dict(
            debug=settings.DEBUG,
            theme="flex",
            version=settings.VERSION,
            form=dict(form),
            form_version=dict(version),
            record=dict(record),
            location_name=location_name,
            record_date=record_date,
            fields=fields

        )
        self.render("print_record.html", **data)


class PrintAlertService(RequestHandler):
    async def get(self, alert_id):
        self.render("template.html")


print_routes = [
    (r"/print/record/(.*)", PrintRecordService),
    (r"/print/alert/(.*)", PrintAlertService),
]
