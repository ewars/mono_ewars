import json

from psycopg2.extensions import AsIs

from ewars.db import get_db_cur
from ewars.conf import settings
from ewars.core.serializers import JSONEncoder
from ewars.core.authentication import get_encrypted_password
from jose import jwt, JWTError

SQL_GET_ACCOUNTS = """
    SELECT * FROM _iw.accounts
    WHERE id = ANY(%s)
        AND status = 'ACTIVE';
"""

SQL_GET_LOCAL_ACCOUNT = """
    SELECT * FROM %s.users
    WHERE uuid = %s;
"""

SQL_GET_USER = """
    SELECT * FROM _iw.users
    WHERE email = %s;
"""

async def attempt_authentication(email, password, waiter=None):
    tenancies = []

    if email == "" or email is None:
        return dict(
            success=False,
            code="INVALID_CREDS",
            account=[],
            users=[]
        )

    if password == "" or password is None:
        return dict(
            success=False,
            code="INVALID_CREDS",
            account=[],
            users=[]
        )

    user = None
    async with get_db_cur() as cur:
        await cur.execute(SQL_GET_USER, (
            email,
        ))
        user = await cur.fetchone()

    if user is None:
        return dict(
            success=False,
            code="INVALID_CREDS",
            users=[],
            accounts=[]
        )

    enc_pass = user.get("password", None)
    pass_components = enc_pass.split(":")
    crypted = get_encrypted_password(password, pass_components[1])

    authenticated = crypted == pass_components[0]

    if not authenticated:
        return dict(
            success=False,
            code="INVALID_CREDS",
            users=[],
            accounts=[]
        )

    if user.get("status") == "PENDING_VERIFICATION":
        return dict(
                success=False,
                code="EMAIL_PENDING_VERIFICATION",
                users=[],
                accounts=[]
                )

    if user.get("status") != "ACTIVE":
        return dict(
                success=False,
                code="USER_INACTIVE",
                users=[],
                accounts=[]
                )

    if len(user.get("accounts", [])) <= 0:
        return dict(
                success=False,
                code="NO_ACCOUNTS",
                users=[],
                accounts=[]
                )

    accounts = user.get("accounts", [])

    async with get_db_cur() as cur:
        await cur.execute(SQL_GET_ACCOUNTS, (
            accounts,
        ))
        accounts = await cur.fetchall()

    end_accounts = []
    users = []
    claim_key = "secret"

    for account in accounts:
        tenant_user = None
        async with get_db_cur() as cur:
            await cur.execute(SQL_GET_LOCAL_ACCOUNT, (
                AsIs(account.get("tki")),
                user.get("uuid"),
            ))
            tenant_user = await cur.fetchone()

        if tenant_user is not None:
            tenant_user['uid'] = tenant_user.get("uuid")
            end_accounts.append({
                "uuid": account.get("uuid"),
                "name": account.get("name"),
                "status": account.get("status"),
                "tki": account.get("tki"),
                "domain": account.get("domain"),
            })

            if tenant_user.get("status") == "ACTIVE":
                claims = dict(
                    user_id=str(user.get("uuid")),
                    role=tenant_user.get("role"),
                    tki=account.get("tki")
                )

                token = jwt.encode(claims, settings.JWT_KEY, algorithm="HS256")

                users.append(dict(
                    uuid=tenant_user.get("uuid"),
                    uid=str(user.get("uuid")),
                    email=user.get("email"),
                    name=user.get("name"),
                    password=user.get("password"),
                    role=tenant_user.get("role"),
                    lid=tenant_user.get("location_id", None),
                    org_id=user.get("org_id", None),
                    tki=account.get("tki"),
                    status=user.get("status"),
                    token=token
                ))

    if len(users) <= 0:
        return dict(
            success=False,
            code="NO_TENANCIES",
            users=[],
            accounts=[]
        )

    return dict(
        success=True,
        code=None,
        users=users,
        accounts=end_accounts
    )
