import time

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

packages = [
    "forms",
    "locations",
    "location_types",
    "users",
    "alarms",
    "alerts",
    "assignments",
    "devices",
    "documents",
    "resources",
    "indicator_groups",
    "indicators",
    "intervals",
    "reporting",
    "organizations",
    "roles",
    "tasks",
    "team_messages",
    "teams",
    "conf",
    "records",
    "reporting",
]


async def get_seed_package(tki):
    result = dict()

    ts_ms = int(time.time())
    async with get_db_cur() as cur:
        for key in packages:
            await cur.execute("""
                SELECT * FROM %s.%s;
            """, (
                AsIs(tki),
                AsIs(key),
            ))
            sub_results = await cur.fetchall()
            result[key] = [dict(x) for x in sub_results]

    return dict(
        type="Seed",
        tki=tki,
        data=result,
        ts_ms=ts_ms,
        result=True
    )