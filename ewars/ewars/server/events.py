import json

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

from ewars.server.handlers.record import Record
from ewars.server.handlers.location import Location
from ewars.server.handlers.alarm import Alarm
from ewars.server.handlers.alert import Alert
from ewars.server.handlers.assignment import Assignment
from ewars.server.handlers.dashboard import Dashboard
from ewars.server.handlers.device import Device
from ewars.server.handlers.document import Document
from ewars.server.handlers.form import Form
from ewars.server.handlers.hud import HUD
from ewars.server.handlers.indicator import Indicator

METHODS = dict(
    RECORD=Record,
    LOCATION=Location,
    ALARM=Alarm,
    ALERT=Alert,
    ASSIGNMENT=Assignment,
    DASHBOARD=Dashboard,
    DEVICE=Device,
    DOCUMENT=Document,
    FORM=Form,
    HUD=HUD,
    INDICATOR=Indicator
)
