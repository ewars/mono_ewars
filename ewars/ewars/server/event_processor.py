from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

RESOURCES = {
    "RECORD": "records",
    "DOCUMENTS": "documents",
    "RESOURCE": "resources",
    "FORM": "forms",
    "INTERVAL": "intervals",
    "INDICATOR": "indicators",
    "INDICATOR_GROUP": "indicator_groups",
    "ORGANIZATION": "organizations",
    "REPORTING": "reporting",
    "ROLE": "roles",
    "TASK": "tasks",
    "TEAM": "teams",
    "TEAM_MESSAGE": "team_messages",
    "ASSIGNMENT": "assignments",
    "USER": "users",
    "ALARM": "alarms",
    "ALERT": "alerts"
}


async def insert_record(data, tki=None):
    pass


async def insert_document(data, tki=None):
    pass


async def insert_resource(data, tki=None):
    pass


async def insert_form(data, tki=None):
    pass


async def insert_interval(data, tki=None):
    pass


async def insert_indicator(data, tki=None):
    pass


async def insert_indicator_group(data, tki=None):
    pass


async def insert_organization(data, tki=None):
    pass


async def insert_reporting(data, tki=None):
    pass


async def insert_role(data, tki=None):
    pass


async def insert_task(data, tki=None):
    pass


async def insert_team(data, tki=None):
    pass


async def insert_team_message(data, tki=None):
    pass


async def insert_assignment(data, tki=None):
    pass


async def insert_user(data, tki=None):
    pass


async def insert_alarm(data, tki=None):
    pass


async def insert_alert(data, tki=None):
    pass


async def handle_events(ltki, events):
    async with get_db_cur() as cur:
        for data in events:
            event_type = data.get("command", None)
            res_type = data.get("ref_type", None)
            res_id = data.get("ref_id", None)

            tki = AsIs(ltki)

            if event_type == "DELETE":
                if res_type == "DOCUMENT":
                    await cur.execute("DELETE FROM %s.documents WHERE uuid = %s", (tki, res_id,))
                elif res_type == "RESOURCE":
                    await cur.execute("DELETE FROM %s.documents WHERE uuid = %s", (tki, res_id,))
                elif res_type == "FORM":
                    await cur.execute("DELETE FROM %s.records WHERE fid = %s", (tki, res_id,))
                    await cur.execute("DELETE FROM %s.reporting WHERE fid = %s", (tki, res_id,))
                    await cur.execute("DELETE FROM %s.forms WHERE uuid = %s", (tki, res_id,))
                elif res_type == "ALARM":
                    await cur.execute("DELETE FROM %s.alerts WHERE alid = %s", (tki, res_id,))
                    await cur.execute("DELETE FROM %s.alarms WHERE uuid = %s", (tki, res_id,))
                elif res_type == "USER":
                    await cur.execute("DELETE FROM %s.accounts WHERE uuid = %s", (tki, res_id,))
                elif res_type == "ASSIGNMENT":
                    await cur.execute("DELETE FROM %s.assignments WHERE uuid = %s", (tki, res_id,))
                elif res_type == "RECORD":
                    await cur.execute("DELETE FROM %s.records WHERE uuid = %s", (tki, res_id,))
                elif res_type == "LOCATION":
                    await cur.execute("DELETE FROM %s.locations WHERE uuid =%s", (tki, res_id,))
                elif res_type == "LOCATION_TYPE":
                    await cur.exeucte("UPDATE %s.locations SET lti = NULL WHERE lti = %s", (tki, res_id,))
                    cur.execute("DELETE FROM %s.location_types WHERE uuid = %s", (tki, res_id,))
                elif res_type == "TASK":
                    await cur.execute("DELETE FROM %s.tasks WHERE uuid = %s", (tki, res_id,))
                elif res_type == "DEVICE":
                    await cur.execute("DELETE FROM %s.devices WHERE uuid = %s", (tki, res_id,))
                elif res_type == "INDICATOR":
                    await cur.execute("DELETE FROM %s.indicators WHERE uuid = %s", (tki, res_id,))
                elif res_type == "INDICATOR_GROUP":
                    await cur.execute("DELETE FROM %s.indicator_groups WHERE uuid = %s", (tki, res_id,))
                elif res_type == "ORGANIZATION":
                    await cur.execute("DELETE FROM %s.organizations WHERE uuid = %s", (tki, res_id,))
                elif res_type == "ROLE":
                    await cur.execute("DELETE FROM %s.roles WHERE uuid = %s", (tki, res_id,))
                elif res_type == "TEAM":
                    await cur.execute("DELETE FROM %s.teams WHERE uuid = %s", (tki, res_id,))
                elif res_type == "TEAM_MESSAGE":
                    await cur.execute("DELETE FROM %s.team_messages WHERE uuid = %s", (tki, res_id,))
                elif res_type == "INTERVAL":
                    await cur.execute("DELETE FROM %s.intervals WHERE uuid = %s", (tki, res_id,))
                else:
                    print("UNKNOWN EVENT")
            elif event_type == "UPDATE":
                table = RESOURCES.get(res_type)

                prop_updates = []
                tuple_params = [tki, AsIs(table)]
                for key, value in data.get('data', []):
                    setter = "%s = " % (key,)
                    setter = setter + "%s"
                    prop_updates.append(setter)
                    tuple_params.append(value)

                sql = "UPDATE %s.%s SET "
                sql = sql + ", ".join(prop_updates)
                sql = sql + " WHERE uuid = %s;"

                tuple_params.append(res_id)

                await cur.execute(sql, tuple(tuple_params))
            elif event_type == "INSERT":
                if res_type == "RECORD":
                    await insert_record(data.get('data', {}), tki=tki)
                elif res_type == "DOCUMENT":
                    await insert_document(data.get('data', {}), tki=tki)
                elif res_type == "RESOURCE":
                    await insert_resource(data.get('data', {}), tki=tki)
                elif res_type == "FORM":
                    await insert_form(data.get('data', {}), tki=tki)
                elif res_type == "INTERVAL":
                    await insert_form(data.get('data', {}), tki=tki)
                elif res_type == "INDICATOR":
                    await insert_indicator(data.get('data', {}), tki=tki)
                elif res_type == "INDICATOR_GROUP":
                    await insert_indicator_group(data.get('data', {}), tki=tki)
                elif res_type == "ORGANIZATION":
                    await insert_organization(data.get('data', {}), tki=tki)
                elif res_type == "REPORTING":
                    await insert_reporting(data.get('data', {}), tki=tki)
                elif res_type == "ROLE":
                    await insert_role(data.get('data', {}), tki=tki)
                elif res_type == "TASK":
                    await insert_task(data.get('data', {}), tki=tki)
                elif res_type == "TEAM":
                    await insert_team(data.get('data', {}), tki=tki)
                elif res_type == "TEAM_MESSAGE":
                    await insert_team_message(data.get('data', {}), tki=tki)
                elif res_type == "ASSIGNMENT":
                    await insert_assignment(data.get('data', {}), tki=tki)
                elif res_type == "USER":
                    await insert_user(data.get('data', {}), tki=tki)
                elif res_type == "ALARM":
                    await insert_alarm(data.get('data', {}), tki=tki)
                elif res_type == "ALERT":
                    await insert_alert(data.get('data', {}), tki=tki)

