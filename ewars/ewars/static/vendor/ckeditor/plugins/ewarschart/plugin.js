var TYPES = {
    "SERIES": "Series widget",
    "PIE": "Category widget",
    "RAW": "Raw widget",
    "MAP": "Map widget",
    "SINGLE": "Indicator Display",
    "TABLE": "Table widget",
    "CONTENT": "Content widget",
    "CATEGORY": "Category widget",
    "MAPPING": "Mapping widget"
}


CKEDITOR.plugins.add("ewarschart", {
    requires: "widget",


    icons: "ewarschart",

    init: function (editor) {
        editor.widgets.add("ewarschart", {
            allowedContent: "div(*)[*]{*};",

            requiredContent: 'div(ewarschart)',

            defaults: {
                type: "SERIES",
                typeName: "Widget",
                id: ewars.utils.uuid()
            },

            editables: {},
            inline: true,


            // Define the template of a new Simple Box widget.
            // The template will be used when creating new instances of the Simple Box widget.
            template: '<div class="ewarschart" data-type="" data-id="">' +
            '<div class="ewarschart-label">{typeName}</div>' +
            '</div>',

            // Define the label for a widget toolbar button which will be automatically
            // created by the Widgets System. This button will insert a new widget instance
            // created from the template defined above, or will edit selected widget
            // (see second part of this tutorial to learn about editing widgets).
            //
            // Note: In order to be able to translate your widget you should use the
            // editor.lang.simplebox.* property. A string was used directly here to simplify this tutorial.
            button: 'Add Widget',

            // Check the elements that need to be converted to widgets.
            //
            // Note: The "element" argument is an instance of http://docs.ckeditor.com/#!/api/CKEDITOR.htmlParser.element
            // so it is not a real DOM element yet. This is caused by the fact that upcasting is performed
            // during data processing which is done on DOM represented by JavaScript objects.
            upcast: function (element) {
                // Return "true" (that element needs to converted to a Simple Box widget)
                // for all <div> elements with a "simplebox" class.
                return element.name == 'div' && element.hasClass('ewarschart');
            },

            edit: function () {
                var id = this.element.getAttribute("data-id");
                if (!id) {
                    id = ewars.utils.uuid();
                    this.element.setAttribute("data-id", id);
                }
                ewars.emit("TEMPLATE_EDIT_SERIES", [this, id]);
            },


            init: function () {
                if (this.element) {
                    type = this.element.getAttribute("data-type");
                    if (type == "") type = "SERIES";
                }

                this.setData("typeName", TYPES[type] || "Unknown");
            },

            data: function (data) {
                var names = {
                    "SERIES": "Series Widget",
                    "PIE": "Category Widget",
                    "RAW": "Raw Value",
                    "MAP": "Map Widget",
                    "SINGLE": "Indicator Display",
                    "TABLE": "Table Widget",
                    "CONTENT": "Content Widget",
                    "CATEGORY": "Category Widget",
                    "MAPPING": "Mapping widget"
                }

                this.element.setAttribute("data-type", data.data.type);
                if (this.element.getChildren().count() > 0) {
                    if (this.element.getChildren().getItem(0).$.className == "ewarschart-label") {
                        this.element.getChildren().getItem(0).setText(names[data.data.type]);
                    }
                }

                if (data.data.type == "RAW") {
                    this.element.setAttribute("class", "ewarschart raw-wyg")
                }
                this.editor.fire("change");
            }
        })
    }
})