(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var _Component = require("./components/Component.react");

var _Component2 = _interopRequireDefault(_Component);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

ReactDOM.render(React.createElement(_Component2.default, null), document.getElementById("application"));


},{"./components/Component.react":3}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("../../common/cmp/Layout");

var _Toolbar = require("../../common/cmp/Toolbar");

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _Button = require("../../common/cmp/Button");

var _Button2 = _interopRequireDefault(_Button);

var _SelectField = require("../../common/components/fields/SelectField.react");

var _SelectField2 = _interopRequireDefault(_SelectField);

var _DateField = require("../../common/components/fields/DateField.react");

var _DateField2 = _interopRequireDefault(_DateField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextField = require("../../common/components/fields/TextInputField.react");


var FIELDS = {
    status: {
        options: [["PUBLISHED", "Published"], ["DRAFT", "Draft"]]
    },
    access: {
        multiple: true,
        options: [["ACCOUNT_ADMIN", "Account Administrators"], ["REGIONAL_ADMIN", "Location Administrators"], ["USER", "Users"], ["PUBLIC", "Public"]]
    },
    FEEDS: {
        multiple: true
    }
};

var STYLES = {
    label: {
        marginTop: 14
    }
};

var ArticleEditor = function (_React$Component) {
    _inherits(ArticleEditor, _React$Component);

    function ArticleEditor(props) {
        _classCallCheck(this, ArticleEditor);

        var _this = _possibleConstructorReturn(this, (ArticleEditor.__proto__ || Object.getPrototypeOf(ArticleEditor)).call(this, props));

        _this._loadFeeds = function () {
            var q = {
                account_id: { eq: window.user.account_id }
            };
            ewars.tx("com.ewars.query", ["feed", ["id", "name"], q, null, null, null, null]).then(function (resp) {
                _this.setState({
                    feeds: resp
                });
            });
        };

        _this._onContentChange = function (e) {
            _this.setState({
                data: _extends({}, _this.state.data, {
                    content: e.target.value
                })
            });
        };

        _this._onFieldChange = function (prop, value) {
            _this.setState({
                data: _extends({}, _this.state.data, _defineProperty({}, prop, value))
            });
        };

        _this._save = function () {
            var bl = new ewars.Blocker(null, "Saving article...");
            ewars.tx("com.ewars.article", ["UPDATE", _this.state.data.id, _this.state.data]).then(function (resp) {
                bl.destroy();
            });
        };

        _this.state = {
            feeds: []
        };
        return _this;
    }

    _createClass(ArticleEditor, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            this.state.data = ewars.copy(this.props.data);
            this._loadFeeds();
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            this.state.data = ewars.copy(nextProps.data);
        }
    }, {
        key: "render",
        value: function render() {
            FIELDS.FEEDS.options = this.state.feeds.map(function (feed) {
                return [feed.id, feed.name];
            });
            return React.createElement(
                _Layout.Layout,
                null,
                React.createElement(
                    _Toolbar2.default,
                    { label: "Article Editor" },
                    React.createElement(
                        "div",
                        { className: "btn-group pull-right" },
                        React.createElement(_Button2.default, {
                            icon: "fa-save",
                            color: "green",
                            onClick: this._save,
                            label: "Save Change(s)" }),
                        React.createElement(_Button2.default, {
                            icon: "fa-close",
                            color: "red",
                            onClick: this.props.onCancel,
                            label: "Cancel" })
                    )
                ),
                React.createElement(
                    _Layout.Row,
                    { height: 56, borderBottom: true },
                    React.createElement(
                        _Layout.Cell,
                        { padding: 8 },
                        React.createElement(TextField, {
                            name: "title",
                            placeholder: "Article title...",
                            value: this.state.data.title,
                            onUpdate: this._onFieldChange })
                    )
                ),
                React.createElement(
                    _Layout.Row,
                    null,
                    React.createElement(
                        _Layout.Cell,
                        { borderTop: true },
                        React.createElement(
                            _Layout.Row,
                            null,
                            React.createElement(
                                _Layout.Cell,
                                { borderRight: true },
                                React.createElement("textarea", {
                                    ref: "editor",
                                    style: { height: "100%", padding: "16px 30px 16px 30px", fontSize: "14px" },
                                    name: "md",
                                    onChange: this._onContentChange,
                                    value: this.state.data.content })
                            )
                        )
                    ),
                    React.createElement(
                        _Layout.Cell,
                        { width: 250, padding: 8, borderTop: true },
                        React.createElement(
                            "label",
                            { style: STYLES.label, htmlFor: "" },
                            "Status"
                        ),
                        React.createElement(_SelectField2.default, {
                            name: "status",
                            config: FIELDS.status,
                            onUpdate: this._onFieldChange,
                            value: this.state.data.status }),
                        React.createElement(
                            "label",
                            { style: STYLES.label, htmlFor: "" },
                            "Publish Date"
                        ),
                        React.createElement(_DateField2.default, {
                            name: "publish_date",
                            value: this.state.data.publish_date,
                            onUpdate: this._onFieldChange }),
                        React.createElement(
                            "label",
                            { style: STYLES.label, htmlFor: "" },
                            "Access"
                        ),
                        React.createElement(_SelectField2.default, {
                            name: "access",
                            value: this.state.data.access,
                            config: FIELDS.access,
                            onUpdate: this._onFieldChange }),
                        React.createElement(
                            "label",
                            { style: STYLES.label, htmlFor: "" },
                            "Feeds"
                        ),
                        React.createElement(_SelectField2.default, {
                            name: "feeds",
                            value: this.state.data.feeds,
                            onUpdate: this._onFieldChange,
                            config: FIELDS.FEEDS })
                    )
                )
            );
        }
    }]);

    return ArticleEditor;
}(React.Component);

exports.default = ArticleEditor;


},{"../../common/cmp/Button":18,"../../common/cmp/Layout":21,"../../common/cmp/Toolbar":23,"../../common/components/fields/DateField.react":37,"../../common/components/fields/SelectField.react":47,"../../common/components/fields/TextInputField.react":51}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("../../common/cmp/Layout");

var _Toolbar = require("../../common/cmp/Toolbar");

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _DataTable = require("../../common/DataTable/DataTable.react");

var _DataTable2 = _interopRequireDefault(_DataTable);

var _Button = require("../../common/cmp/Button");

var _Button2 = _interopRequireDefault(_Button);

var _Shade = require("../../common/cmp/Shade");

var _Shade2 = _interopRequireDefault(_Shade);

var _columns = require("../constants/columns");

var _columns2 = _interopRequireDefault(_columns);

var _form_article = require("../constants/form_article");

var _form_article2 = _interopRequireDefault(_form_article);

var _form_feed = require("../constants/form_feed");

var _form_feed2 = _interopRequireDefault(_form_feed);

var _ArticleEditor = require("./ArticleEditor");

var _ArticleEditor2 = _interopRequireDefault(_ArticleEditor);

var _FeedManager = require("./FeedManager");

var _FeedManager2 = _interopRequireDefault(_FeedManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GRID_ACTIONS = [{ label: "Edit", icon: "fa-pencil", action: "EDIT" }, { label: "Delete", icon: "fa-trash", action: "DELETE" }];

var Component = function (_React$Component) {
    _inherits(Component, _React$Component);

    function Component(props) {
        _classCallCheck(this, Component);

        var _this = _possibleConstructorReturn(this, (Component.__proto__ || Object.getPrototypeOf(Component)).call(this, props));

        _this._create = function () {
            _this.setState({
                view: "EDIT",
                showShade: true,
                article: {}
            });
        };

        _this._onAction = function (action, cell, row) {
            console.log(action);
            if (["EDIT", "CLICK"].indexOf(action) >= 0) {
                _this.setState({
                    view: "EDIT",
                    article: row,
                    showShade: true

                });
            }

            if (action == "CLOSE") {
                _this.setState({
                    showShade: false,
                    article: null,
                    view: "DEFAULT"
                });
            }
        };

        _this._manage = function () {
            _this.setState({
                view: "FEED_MANAGER",
                showShade: true
            });
        };

        _this._cancelShade = function () {
            _this.setState({
                view: "DEFAULT",
                showShade: false,
                article: null
            });
        };

        _this.state = {
            editing: null,
            view: "DEFAULT",
            showShade: false
        };
        return _this;
    }

    _createClass(Component, [{
        key: "render",
        value: function render() {

            var view = void 0;

            if (this.state.view == "EDIT") view = React.createElement(_ArticleEditor2.default, {
                onCancel: this._cancelShade,
                data: this.state.article });
            if (this.state.view == "FEED_MANAGER") view = React.createElement(_FeedManager2.default, {
                onCancel: this._cancelShade });

            return React.createElement(
                "div",
                { className: "ide" },
                React.createElement(
                    _Layout.Layout,
                    null,
                    React.createElement(
                        _Toolbar2.default,
                        { label: "Articles" },
                        React.createElement(
                            "div",
                            { className: "btn-group pull-right" },
                            React.createElement(_Button2.default, {
                                label: "Manage Feeds",
                                icon: "fa-cog",
                                onClick: this._manage }),
                            React.createElement(_Button2.default, {
                                label: "New Article",
                                icon: "fa-plus",
                                onClick: this._create })
                        )
                    ),
                    React.createElement(
                        _Layout.Row,
                        null,
                        React.createElement(
                            _Layout.Cell,
                            null,
                            React.createElement(_DataTable2.default, {
                                resource: "article",
                                actions: GRID_ACTIONS,
                                columns: _columns2.default,
                                onCellAction: this._onAction,
                                filter: {},
                                join: {} })
                        )
                    )
                ),
                React.createElement(
                    _Shade2.default,
                    {
                        toolbar: false,
                        onAction: this._onAction,
                        shown: this.state.showShade },
                    view
                )
            );
        }
    }]);

    return Component;
}(React.Component);

exports.default = Component;


},{"../../common/DataTable/DataTable.react":13,"../../common/cmp/Button":18,"../../common/cmp/Layout":21,"../../common/cmp/Shade":22,"../../common/cmp/Toolbar":23,"../constants/columns":5,"../constants/form_article":6,"../constants/form_feed":7,"./ArticleEditor":2,"./FeedManager":4}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("../../common/cmp/Layout");

var _Toolbar = require("../../common/cmp/Toolbar");

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _Button = require("../../common/cmp/Button");

var _Button2 = _interopRequireDefault(_Button);

var _EditableList = require("../../common/cmp/EditableList");

var _EditableList2 = _interopRequireDefault(_EditableList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextField = require("../../common/components/fields/TextInputField.react");

var FeedManager = function (_React$Component) {
    _inherits(FeedManager, _React$Component);

    function FeedManager(props) {
        _classCallCheck(this, FeedManager);

        var _this = _possibleConstructorReturn(this, (FeedManager.__proto__ || Object.getPrototypeOf(FeedManager)).call(this, props));

        _this._reload = function () {
            ewars.tx("com.ewars.query", ["feed", null, {}, null, null, null, null]).then(function (resp) {
                _this.setState({
                    feeds: resp
                });
            });
        };

        _this._create = function () {
            _this.setState({
                feed: {},
                showEditor: true
            });
        };

        _this._editFeed = function (feed) {};

        _this._save = function () {
            var bl = new ewars.Blocker(null, "Saving feed");
            ewars.tx('com.ewars.feed', ["UPDATE_FEED", _this.state.feed.id, _this.state.feed]).then(function (resp) {
                _this.setState({
                    feed: null,
                    showEditor: false
                });
                _this._reload();
            });
        };

        _this._cancelEdit = function () {
            _this.setState({
                showEditor: false,
                feed: null
            });
        };

        _this.state = {
            feeds: [],
            showEditor: false,
            feed: null
        };
        return _this;
    }

    _createClass(FeedManager, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            this._reload();
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement(
                _Layout.Layout,
                null,
                React.createElement(
                    _Toolbar2.default,
                    { label: "Feed Manager" },
                    React.createElement(
                        "div",
                        { className: "btn-group pull-right" },
                        React.createElement(_Button2.default, {
                            icon: "fa-close",
                            onClick: this.props.onCancel,
                            label: "Close" })
                    ),
                    React.createElement(
                        "div",
                        { className: "btn-group pull-right" },
                        React.createElement(_Button2.default, {
                            icon: "fa-plus",
                            onClick: this._create,
                            label: "Add Feed" })
                    )
                ),
                this.state.showEditor ? React.createElement(
                    _Layout.Row,
                    { height: 100 },
                    React.createElement(
                        _Layout.Cell,
                        null,
                        React.createElement(
                            "div",
                            { className: "widget", style: { padding: 8 } },
                            React.createElement(
                                "label",
                                { htmlFor: "" },
                                "Feed Name"
                            ),
                            React.createElement(TextField, {
                                name: "name",
                                value: this.state.feed.name,
                                onUpdate: this._onFieldChange }),
                            React.createElement("div", { className: "clearer", style: { marginBottom: 8 } }),
                            React.createElement(
                                "div",
                                { className: "btn-group pull-right" },
                                React.createElement(_Button2.default, {
                                    icon: "fa-save",
                                    color: "green",
                                    onClick: this._save,
                                    label: "Save" }),
                                React.createElement(_Button2.default, {
                                    icon: "fa-close",
                                    color: "amber",
                                    onClick: this._cancelEdit,
                                    label: "Cancel" })
                            )
                        )
                    )
                ) : null,
                React.createElement(
                    _Layout.Row,
                    null,
                    React.createElement(
                        _Layout.Cell,
                        null,
                        React.createElement(
                            "div",
                            { className: "ide-panel ide-panel-absolute ide-scroll" },
                            React.createElement(
                                "div",
                                { className: "block-tree" },
                                this.state.feeds.map(function (feed) {
                                    return React.createElement(
                                        "div",
                                        { className: "block" },
                                        React.createElement(
                                            "div",
                                            { className: "block-content" },
                                            feed.name
                                        )
                                    );
                                })
                            )
                        )
                    )
                )
            );
        }
    }]);

    return FeedManager;
}(React.Component);

exports.default = FeedManager;


},{"../../common/cmp/Button":18,"../../common/cmp/EditableList":20,"../../common/cmp/Layout":21,"../../common/cmp/Toolbar":23,"../../common/components/fields/TextInputField.react":51}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var COLUMNS = [{
    name: "title",
    config: {
        label: "Article Title",
        type: "text"
    }
}, {
    name: "status",
    config: {
        label: "Status",
        type: "select",
        options: [["ACTIVE", _l("ACTIVE")], ["INACTIVE", _l("INACTIVE")]]
    }
}, {
    name: "last_modified",
    config: {
        label: "Last Modified",
        type: "date"
    },
    fmt: ewars.DATE
}, {
    name: "created",
    config: {
        label: "Created Date",
        type: "date"
    },
    fmt: ewars.DATE
}, {
    name: "publish_date",
    config: {
        label: "Publish Date"
    },
    fmt: ewars.DATE
}, {
    name: "access",
    config: {
        label: "Access"
    }
}];

exports.default = COLUMNS;


},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ["General Settings", {
    t: "text",
    l: "Page Title",
    n: "title"
}, {
    t: "text",
    l: "URL",
    n: "sef"
}, {
    t: "button_group",
    l: "Status",
    n: "status",
    c: {
        options: [["ACTIVE", "Active"], ["INACTIVE", "Inactive"]]
    }
}, {
    t: "select",
    l: "Parent Page",
    n: "parent_id",
    c: {
        optionsSource: {
            resource: "site_page",
            labelSource: "title",
            valSource: "id",
            query: {}
        }
    }
}, {
    t: "textarea",
    l: "Description",
    n: "description"
}, {
    t: "select",
    l: "Type",
    n: "page_type",
    c: {
        options: [["MARKDOWN", "Markdown"], ["ANALYSIS", "Analysis"], ["RICH_TEXT", "Rich Text"], ["FILES", "Filed"], ["DOCUMENTS", "Documents"], ["FEED", "News Feed"], ["MAP", "Map"], ["DOWNLOADS", "Downloads"], ["LOCATIONS", "Location Data"], ["FORM", "Form"]]
    }
}];


},{}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ["General Settings", {
    n: "title",
    r: true,
    t: "text",
    l: "Site Title"
}, {
    n: "status",
    r: true,
    t: "button_group",
    l: "Status",
    c: {
        options: [["ACTIVE", "Active"], ["INACTIVE", "Inactive"]]
    }
}, {
    t: "textarea",
    l: "Description",
    n: "description"
}, "Security & Authentication", {
    t: "select",
    l: "Access",
    n: "access",
    c: {
        options: [["PRIVATE", "Private"], ["CUSTOM", "Custom Authentication"], ["PUBLIC", "Public"]]
    }
}, "Styling", {
    t: "select",
    l: "Theme",
    n: "theme",
    c: {
        options: [["DEFAULT", "Default"], ["DARKEST", "Darkest Theme"], ["EWARS", "EWARS Theme"], ["DUSK", "Dusk Theme"], ["CORNAVIN", "Cornavin Theme"]]
    }
}, "Miscellaneous", {
    t: "button_group",
    l: "Hub Indexed",
    n: "hub_indexed",
    h: "If Indexed, this site will be made available to navigate to from the main hub site. This is useful for disseminating information to the public globally.",
    c: {
        options: [["YES", "Indexed"], ["NO", "Not Indexed"]]
    }
}, {
    t: "text",
    l: "Google Analytics Code",
    n: "goog",
    h: "Add a Google Analytics code to track activity and visits to the site"
}, {
    t: "text",
    l: "CNAME",
    n: "cname",
    h: "If you would like to use your own domain to display this site, you can specify the domain here, you must set up a CNAME record in your DNS pointing to this sites URL."
}];


},{}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ContextMenuItem = function (_React$Component) {
    _inherits(ContextMenuItem, _React$Component);

    function ContextMenuItem(props) {
        _classCallCheck(this, ContextMenuItem);

        var _this = _possibleConstructorReturn(this, (ContextMenuItem.__proto__ || Object.getPrototypeOf(ContextMenuItem)).call(this, props));

        _this._onClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            _this.props.onClick(_this.props.data.action);
        };

        return _this;
    }

    _createClass(ContextMenuItem, [{
        key: "render",
        value: function render() {
            var iconClass = "fa " + this.props.data.icon;

            return React.createElement(
                "div",
                { onClick: this._onClick, className: "context-menu-item" },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 25 } },
                        React.createElement("i", { className: iconClass })
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        this.props.data.label
                    )
                )
            );
        }
    }]);

    return ContextMenuItem;
}(React.Component);

var ContextMenu = function (_React$Component2) {
    _inherits(ContextMenu, _React$Component2);

    function ContextMenu(props) {
        _classCallCheck(this, ContextMenu);

        var _this2 = _possibleConstructorReturn(this, (ContextMenu.__proto__ || Object.getPrototypeOf(ContextMenu)).call(this, props));

        _this2._onClick = function (action) {
            _this2.props.onClick(action);
        };

        return _this2;
    }

    _createClass(ContextMenu, [{
        key: "render",
        value: function render() {
            var style = {};
            if (!this.props.absolute) style.position = "relative";

            var actions = this.props.actions.map(function (action) {
                return React.createElement(ContextMenuItem, {
                    key: action.action,
                    data: action,
                    onClick: this._onClick });
            }.bind(this));

            return React.createElement(
                "div",
                { className: "context-menu", style: style },
                actions
            );
        }
    }]);

    return ContextMenu;
}(React.Component);

exports.default = ContextMenu;


},{}],9:[function(require,module,exports){
"use strict";

var DTDispatcher = require("./DTDispatcher");
var CONSTANTS = require("../constants");

var Actions = function Actions(action, data) {
    DTDispatcher.handleViewAction({
        actionType: action,
        data: data
    });
};

module.exports = Actions;


},{"../constants":70,"./DTDispatcher":10}],10:[function(require,module,exports){
"use strict";

var Dispatcher = require("../Dispatcher");
var assign = require("object-assign");

var AppDispatcher = assign({}, Dispatcher.prototype, {
    handleViewAction: function handleViewAction(action) {
        this.dispatch({
            source: "VIEW_ACTION",
            action: action
        });
    }
});

module.exports = AppDispatcher;


},{"../Dispatcher":17,"object-assign":87}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
function _updatePagination(STATE, data) {
    console.log(STATE, data);
    switch (data.page) {
        case "FF":
            if (STATE.offset < STATE.count) {
                var pages = STATE.count / STATE.limit;
                STATE.page = Math.ceil(pages);
                STATE.offset = (STATE.page - 1) * STATE.limit;
            }
            break;
        case "F":
            if (STATE.offset < STATE.count) {
                STATE.offset = (STATE.page + 1) * STATE.limit;
                STATE.page++;
            }
            break;
        case "B":
            STATE.offset = (STATE.page - 1) * STATE.limit;
            STATE.page--;
            break;
        case "BB":
            STATE.offset = 0;
            STATE.page = 0;
            break;
        default:
            break;
    }

    if (STATE.page < 0) STATE.page = 0;
    if (STATE.page <= 0) STATE.offset = 0;

    return STATE;
}

function _updateSort(state, cell, fieldType) {
    if (!state.order[cell]) {
        state.order = {};
        state.order[cell] = "DESC:" + fieldType;
    } else if (state.order[cell] == "DESC:" + fieldType) {
        state.order[cell] = "ASC:" + fieldType;
    } else if (state.order[cell] == "ASC:" + fieldType) {
        delete state.order[cell];
    }

    return state;
}

function _updateFilter(state, col, data) {
    if (!data) {
        delete state.filter[col];
        return state;
    }

    if (!col) return state;

    state.filter[col] = data;

    return state;
}

exports.default = function (rawState, action, data) {
    var state = ewars.copy(rawState);

    switch (action) {
        case "SET_ITEMS":
            data.state.initialized = true;
            return Object.assign({}, state, data.state, { data: data.results, count: data.count });
        case "PAGINATE":
            return _updatePagination(state, data);
        case "UPDATE_SORT":
            return _updateSort(state, data.field, data.fieldType);
        case "UPDATE_FILTER":
            return _updateFilter(state, data.key, data.config);
        default:
            return state;
    }
};


},{}],12:[function(require,module,exports){
"use strict";

var AppDispatcher = require("./DTDispatcher");
var EventEmitter = require("events").EventEmitter;
var assign = require('object-assign');

var CONSTANTS = require("../constants");

var STATE = {
    data: [],
    order: {},
    filter: {},
    limit: 10,
    offset: 0,
    page: 0,
    search: {},
    count: null,
    joins: null,
    select: null,
    filterControl: {
        shown: false,
        x: null,
        y: null,
        ref: null
    },
    context: {
        shown: false,
        ref: null,
        rowIndex: null
    },
    editor: {}
};

function _updateSort(cell, fieldType) {
    if (!STATE.order[cell]) {
        STATE.order = {};
        STATE.order[cell] = "DESC:" + fieldType;
    } else if (STATE.order[cell] == "DESC:" + fieldType) {
        STATE.order[cell] = "ASC:" + fieldType;
    } else if (STATE.order[cell] == "ASC:" + fieldType) {
        delete STATE.order[cell];
    }
}

function _updateFilter(col, data) {
    if (!data) {
        delete STATE.filter[col];
        return;
    }

    if (!col) return;

    STATE.filter[col] = data;
}

function _updatePagination(data) {
    switch (data.page) {
        case "FF":
            if (STATE.offset < STATE.count) {
                var pages = STATE.count / STATE.limit;
                STATE.page = Math.ceil(pages);
                STATE.offset = (STATE.page - 1) * STATE.limit;
            }
            break;
        case "F":
            if (STATE.offset < STATE.count) {
                STATE.offset = (STATE.page + 1) * STATE.limit;
                STATE.page++;
            }
            break;
        case "B":
            STATE.offset = (STATE.page - 1) * STATE.limit;
            STATE.page--;
            break;
        case "BB":
            STATE.offset = 0;
            STATE.page = 0;
            break;
        default:
            break;
    }

    if (STATE.page < 0) STATE.page = 0;
    if (STATE.page <= 0) STATE.offset = 0;
}

function _setRowContext(rowIndex, reference) {
    STATE.context.shown = true;
    STATE.context.ref = reference;
    STATE.context.rowIndex = rowIndex;
}

function _showFilterControl(cell, ref) {
    STATE.filterControl.shown = true;
    STATE.filterControl.cell = cell;
    STATE.filterControl.ref = ref;
}

function _setData(data, count) {
    STATE.data = data;
    STATE.count = count;
}

var Store = assign({}, EventEmitter.prototype, {
    getAll: function getAll() {},

    emitChange: function emitChange(event) {
        this.emit(event);
    },

    addChangeListener: function addChangeListener(event, callback) {
        this.on(event, callback);
    },

    removeChangeListener: function removeChangeListener(event, callback) {
        this.removeListener(event, callback);
    },

    getState: function getState() {
        return STATE;
    },

    setDefaults: function setDefaults(props) {
        // if (props.filter) STATE.filter = props.filter;
        if (props.join) STATE.join = props.join;
        if (props.select) STATE.select = props.select;
        if (props.order) STATE.order = props.order;
    },

    dispatcherIndex: AppDispatcher.register(function (payload) {
        var action = payload.action.actionType;
        var data = payload.action.data;

        // Add a new tab to the interface
        switch (action) {
            case "UPDATE_SORT":
                _updateSort(data.field, data.fieldType);
                Store.emitChange("QUERY_CHANGE");
                break;
            case "SHOW_FILTER":
                _showFilterControl(data.cell, data.ref);
                Store.emitChange("QUERY_CHANGE");
                break;
            case "SET_ROW_CONTEXT":
                _setRowContext(data.rowIndex, data.ref);
                Store.emitChange("CONTEXT_CHANGE");
                break;
            case "RECV_DATA":
                _setData(data.results, data.count);
                Store.emitChange("GRID_CHANGE");
                break;
            case "PAGINATE":
                _updatePagination(data);
                Store.emitChange("QUERY_CHANGE");
                break;
            case "UPDATE_FILTER":
                _updateFilter(data.key, data.config);
                Store.emitChange("QUERY_CHANGE");
                break;
            default:
            // no op
        }

        return true;
    })
});

module.exports = Store;


},{"../constants":70,"./DTDispatcher":10,"events":84,"object-assign":87}],13:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _DTStore = require("./DTStore");

var _DTStore2 = _interopRequireDefault(_DTStore);

var _DataTableHeaderCell = require("./components/DataTableHeaderCell.react");

var _DataTableHeaderCell2 = _interopRequireDefault(_DataTableHeaderCell);

var _DataTableFilterControl = require("./components/DataTableFilterControl.react");

var _DataTableFilterControl2 = _interopRequireDefault(_DataTableFilterControl);

var _DataTableCell = require("./components/DataTableCell.react");

var _DataTableCell2 = _interopRequireDefault(_DataTableCell);

var _DTReducer = require("./DTReducer");

var _DTReducer2 = _interopRequireDefault(_DTReducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = require("../components/ButtonComponent.react");

var Form = require("../components/Form.react");
var FormUtils = require("../../common/utils/FormUtils");

// if (!Array.prototype.unique) {
//     Array.prototype.unique = function () {
//         var a = this.concat();
//         for (var i = 0; i < a.length; ++i) {
//             for (var j = i + 1; j < a.length; ++j) {
//                 if (a[i] === a[j])
//                     a.splice(j--, 1);
//             }
//         }
//
//         return a;
//     };
// }

var DataControlCell = function (_React$Component) {
    _inherits(DataControlCell, _React$Component);

    function DataControlCell(props) {
        _classCallCheck(this, DataControlCell);

        var _this = _possibleConstructorReturn(this, (DataControlCell.__proto__ || Object.getPrototypeOf(DataControlCell)).call(this, props));

        _this._onClick = function () {
            _this.props.onAction(_this.props.data.action, null, _this.props.row);
        };

        _this._show = function () {
            _this.setState({
                show: true
            });
        };

        _this._hide = function () {
            _this.setState({
                show: false
            });
        };

        _this.state = {
            show: false
        };
        return _this;
    }

    _createClass(DataControlCell, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "td",
                { width: "30px", style: { width: 30 }, onMouseEnter: this._show, onMouseLeave: this._hide },
                React.createElement(
                    "div",
                    { className: "dt-row-control", onClick: this._onClick },
                    React.createElement("i", { className: "fa " + this.props.data.icon }),
                    this.state.show ? React.createElement(
                        "div",
                        { className: "popover" },
                        this.props.data.label
                    ) : null
                )
            );
        }
    }]);

    return DataControlCell;
}(React.Component);

var addEvent = function addEvent(object, type, callback) {
    if (object == null || typeof object == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on" + type] = callback;
    }
};

var removeEvent = function removeEvent(object, type, callback) {
    if (object == null || typeof object == "undefined") return;
    if (object.removeEventListener) {
        object.removeEventListener(type, callback);
    } else if (object.removeEvent) {
        object.removeEvent("on" + type, callback);
    } else {
        object['on' + type] = null;
    }
};

var DataTable = function (_React$Component2) {
    _inherits(DataTable, _React$Component2);

    function DataTable(props) {
        _classCallCheck(this, DataTable);

        var _this2 = _possibleConstructorReturn(this, (DataTable.__proto__ || Object.getPrototypeOf(DataTable)).call(this, props));

        _this2._forceWidth = false;

        _this2._recalc = function () {
            if (_this2._timer) {
                clearTimeout(_this2._timer);
                _this2._timer = null;
            }
            _this2._timer = setTimeout(function () {
                var height = _this2.refs.grid.clientHeight;

                var numRows = (height - height % 25) / 25;
                _this2.state.limit = numRows + 5;

                _this2.refs.dt.style.height = height + "px";

                _this2._timer = null;
                _this2.query(_this2.props, _this2.state);
            }, 500);
        };

        _this2._onChange = function (action) {
            var newState = ewars.z.getState(_this2.props.id || "DT_TRANSIENT");

            if (["PAGINATE", "UPDATE_SORT", "UPDATE_FILTER"].indexOf(action) >= 0) {
                _this2.query(_this2.props, newState);
            } else {
                _this2.setState(_extends({}, newState));
            }
        };

        _this2._onQueryChange = function () {
            _this2.query(_this2.props, _this2.state);
        };

        _this2.onButtonClick = function (data) {
            if (data.action) {
                data.action();
            }
        };

        _this2.onAction = function (action, row) {
            _this2.props.onAction(action, row);
        };

        _this2._move = function (data) {
            ewars.z.dispatch(_this2.props.id || "DT_TRANSIENT", "PAGINATE", data);
        };

        _this2._onCellAction = function (action, cell, row) {
            _this2.props.onCellAction(action, cell, row);
        };

        _this2._modifyRow = function (index, path, value) {
            var data = ewars.copy(_this2.state.data);
            var row = data[index];

            if (path.split(".")[0] == "data") {
                var realPath = path.replace("data.", "");
                row.data[realPath] = value;
            } else {
                row[path] = value;
            }

            data[index] = row;
            _this2.setState({
                data: data
            });
        };

        ewars.z.register(_this2.props.id || "DT_TRANSIENT", _DTReducer2.default, {
            data: [],
            initialized: false,
            order: _this2.props.order || {},
            select: _this2.props.select || null,
            filter: {},
            limit: 10,
            offset: 0,
            page: 0,
            search: {},
            count: null,
            join: _this2.props.join || null,
            filterControl: {
                shown: false,
                x: null,
                y: null,
                ref: null
            },
            context: {
                shown: false,
                ref: null,
                rowIndex: null
            },
            editor: {}
        });

        _this2.state = ewars.z.getState(_this2.props.id || "DT_TRANSIENT");
        return _this2;
    }

    _createClass(DataTable, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            this.state = _DTStore2.default.getState(this.props.id || "DT_TRANSIENT");

            ewars.z.subscribe(this.props.id || "DT_TRANSIENT", this._onChange);
            ewars.subscribe("RELOAD_DT", this._onQueryChange);

            addEvent(window, "resize", this._recalc);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            removeEvent(window, "resize", this.recalc);
            ewars.z.unsubscribe(this.props.id || "DT_TRANSIENT", this._onChange);
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps, nextState) {
            var id = nextProps.id || "DT_TRANSIENT";

            if (id != this.props.id) {
                ewars.z.unsubscribe(this.props.id, this._onChange);
                ewars.z.subscribe(id, this._onChange);
            }

            if (ewars.z.getState(id)) {
                var state = ewars.z.getState(id);
                if (!state.initialized) {
                    this.query(nextProps, state);
                } else {
                    this.setState(_extends({}, state));
                }
            } else {
                var height = this.refs.grid.clientHeight;

                var numRows = (height - height % 25) / 25;
                var limit = numRows + 5;

                this.refs.dt.style.height = height + "px";
                ewars.z.register(id, _DTReducer2.default, {
                    data: [],
                    intialized: false,
                    order: nextProps.order || {},
                    select: nextProps.select || null,
                    filter: {},
                    limit: limit || 10,
                    offset: 0,
                    page: 0,
                    search: {},
                    count: null,
                    join: nextProps.join || null,
                    filterControl: {
                        shown: false,
                        x: null,
                        y: null,
                        ref: null
                    },
                    context: {
                        shown: false,
                        ref: null,
                        rowIndex: null
                    },
                    editor: {}
                });

                var _state = ewars.z.getState(nextProps.id || "DT_TRANSIENT");
                ewars.z.subscribe(id, this._onChange);
                this.query(nextProps, _state);
            }
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {
            var height = this.refs.grid.clientHeight;
            var width = this.refs.grid.clientWidth;

            var numRows = (height - height % 25) / 25;
            this.state.limit = numRows + 5;
            this.state.order = Object.assign({}, this.props.order);

            this.refs.dt.style.height = height + "px";

            this.query(this.props, this.state);
        }
    }, {
        key: "query",
        value: function query(props, state) {
            console.log(state);
            if (props.paused == true) return;

            var blocker = new ewars.Blocker(this.refs.wrapper, "Loading...");
            var com = "com.ewars.query";
            if (props.isReports) com = "com.ewars.collections.get";

            var args = [];

            var order = [];
            if (this.props.grid) {

                for (var i in state.order) {
                    FormUtils;
                }
            }

            if (props.isReports) {
                args = [props.formId, Object.assign({}, props.filter || {}, state.filter), state.order, state.limit || null, state.offset, _.uniq((state.join || []).concat(props.join || []))];
            } else {
                args = [props.resource, state.select || null, Object.assign({}, props.filter || {}, state.filter), state.order, state.limit, state.offset, _.uniq((state.join || []).concat(props.join || []))];
            }

            ewars.tx(com, args, { count: true }).then(function (resp) {
                blocker.destroy();
                ewars.z.dispatch(this.props.id || "DT_TRANSIENT", "SET_ITEMS", _extends({}, resp, {
                    state: state
                }));
            }.bind(this));
        }
    }, {
        key: "render",
        value: function render() {
            var _this3 = this;

            var colHeaders = void 0,
                colGroupsMaster = [],
                cellWidth = 200;

            if (this._forceWidth) cellWidth = this.refs.dt.clientWidth / this.props.columns.length;

            //Used in report manager, if we're showing a grid composition
            if (this.props.grid) {
                colHeaders = [];

                if (this.props.actions) {
                    this.props.actions.forEach(function (cell) {
                        colGroupsMaster.push(React.createElement("col", null));
                    });
                }

                var grouped = {};
                this.props.grid.definition.forEach(function (item) {
                    if (!grouped[item.row]) grouped[item.row] = [];
                    grouped[item.row].push(item);
                });

                this.props.grid.columns.forEach(function (item) {
                    var labelLength = ewars.I18N(item.config.label).length;
                    var width = labelLength * 6.3;
                    if (width < 200) width = 200;
                    if (this._forceWidth) width = cellWidth < item.width || 0 ? cellWidth : item.width;
                    if (item.config.width) width = item.config.width;
                    colGroupsMaster.push(React.createElement("col", { width: width + "px" }));
                }.bind(this));

                var _loop = function _loop() {
                    // Group
                    grouped[k].sort(function (a, b) {
                        if (a.cell < b.cell) return -1;
                        if (a.cell > b.cell) return 1;
                        return 0;
                    });

                    var cells = [];

                    if (_this3.props.actions) {
                        _this3.props.actions.forEach(function (cell) {
                            cells.push(React.createElement("th", { style: { width: 30 } }));
                        });
                    }

                    grouped[k].forEach(function (cell) {
                        cells.push(React.createElement(_DataTableHeaderCell2.default, {
                            id: this.props.id,
                            filter: this.state.filter,
                            sort: this.state.order,
                            data: cell }));
                    }.bind(_this3));

                    colHeaders.push(React.createElement(
                        "tr",
                        null,
                        cells
                    ));
                };

                for (var k in grouped) {
                    _loop();
                }
            } else {
                var headers = [];

                if (this.props.actions) {
                    this.props.actions.forEach(function (cell) {
                        colGroupsMaster.push(React.createElement("col", { width: "30px" }));
                        headers.push(React.createElement("th", { style: { width: 30 } }));
                    });
                }

                this.props.columns.forEach(function (col, index) {
                    var width = col.width || 200;
                    var style = { width: width };
                    if (this._forceWidth) style.width = cellWidth;
                    colGroupsMaster.push(React.createElement("col", { style: style }));
                    headers.push(React.createElement(_DataTableHeaderCell2.default, {
                        id: this.props.id || "DT_TRANSIENT",
                        filter: this.state.filter,
                        sort: this.state.order,
                        data: col }));
                }.bind(this));
                colHeaders = React.createElement(
                    "tr",
                    { style: { height: "100%" } },
                    headers
                );
            }

            var rowHeaders = void 0;

            var rows = this.state.data.map(function (item, index) {
                var _this4 = this;

                var cells = [];

                if (this.props.actions) {
                    this.props.actions.forEach(function (cell) {
                        cells.push(React.createElement(DataControlCell, {
                            data: cell,
                            id: _this4.props.id || "DT_TRANSIENT",
                            onAction: _this4._onCellAction,
                            row: item }));
                    });
                }

                if (this.props.grid) {
                    this.props.grid.columns.forEach(function (col) {
                        cells.push(React.createElement(_DataTableCell2.default, {
                            col: col,
                            row: item,
                            modifyRow: this._modifyRow,
                            inline: this.props.editable,
                            onAction: this._onCellAction,
                            actions: this.props.actions,
                            rowIndex: index }));
                    }.bind(this));
                } else {
                    this.props.columns.forEach(function (col) {
                        cells.push(React.createElement(_DataTableCell2.default, {
                            rowIndex: index,
                            row: item,
                            inline: this.props.editable,
                            modifyRow: this._modifyRow,
                            onAction: this._onCellAction,
                            actions: this.props.actions,
                            col: col }));
                    }.bind(this));
                }

                return React.createElement(
                    "tr",
                    { style: { height: "100%" } },
                    cells
                );
            }.bind(this));

            var buttons = void 0;
            if (this.props.buttons) {
                buttons = this.props.buttons.map(function (btn) {
                    return React.createElement(Button, {
                        label: btn.label,
                        icon: btn.icon,
                        data: btn,
                        onClick: this.onButtonClick });
                }.bind(this));
            }

            var paginationString;
            if (this.state.count) {
                var startShow, endShow;
                if (this.state.page == 0) {
                    startShow = 1;
                    endShow = this.state.limit;
                } else {
                    startShow = this.state.limit * this.state.page + 1;
                    endShow = startShow + 9;
                }

                if (this.state.count == this.state.data.length) {
                    endShow = this.state.count;
                }

                paginationString = ["Showing ", startShow, " to ", endShow, " of ", this.state.count].join("");
            } else {
                paginationString = "No items found";
            }

            return React.createElement(
                "div",
                { className: "dt-wrapper", ref: "wrapper" },
                React.createElement(
                    "div",
                    { className: "ide-layout" },
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        React.createElement(
                            "div",
                            { className: "ide-col", ref: "grid" },
                            React.createElement(
                                "div",
                                { className: "dt", ref: "dt" },
                                React.createElement(
                                    "table",
                                    { className: "dtCore" },
                                    React.createElement(
                                        "thead",
                                        null,
                                        colHeaders
                                    ),
                                    React.createElement(
                                        "tbody",
                                        null,
                                        rows
                                    )
                                )
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { maxHeight: 35 } },
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement(
                                "div",
                                { className: "ide-tbar" },
                                React.createElement(
                                    "div",
                                    { className: "ide-tbar-text" },
                                    paginationString
                                ),
                                React.createElement(
                                    "div",
                                    { className: "btn-group pull-right" },
                                    this.state.offset > 0 ? React.createElement(Button, {
                                        icon: "fa-fast-backward",
                                        onClick: this._move,
                                        data: { page: "BB" } }) : null,
                                    this.state.offset > 0 ? React.createElement(Button, {
                                        icon: "fa-caret-left",
                                        onClick: this._move,
                                        data: { page: "B" } }) : null,
                                    this.state.data.length >= this.state.limit ? React.createElement(Button, {
                                        icon: "fa-caret-right",
                                        onClick: this._move,
                                        data: { page: "F" } }) : null,
                                    this.state.data.length >= this.state.limit ? React.createElement(Button, {
                                        icon: "fa-fast-forward",
                                        onClick: this._move,
                                        data: { page: "FF" } }) : null
                                )
                            )
                        )
                    )
                ),
                this.state.showFilterControls ? React.createElement(_DataTableFilterControl2.default, {
                    id: this.props.id,
                    cell: this.state.cell,
                    top: this.state.filtY,
                    left: this.state.filtX }) : null
            );
        }
    }]);

    return DataTable;
}(React.Component);

DataTable.defaultProps = {
    resource: null,
    freezeColumns: 0,
    freezeRows: 0,
    editable: false,
    columns: [],
    filter: {},
    searchParams: [],
    baseFilters: {},
    actions: [],
    isReports: false,
    grid: null,
    join: null,
    select: null
};
exports.default = DataTable;


},{"../../common/utils/FormUtils":80,"../components/ButtonComponent.react":30,"../components/Form.react":31,"./DTReducer":11,"./DTStore":12,"./components/DataTableCell.react":14,"./components/DataTableFilterControl.react":15,"./components/DataTableHeaderCell.react":16}],14:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _DateField = require("../../components/fields/DateField.react");

var _DateField2 = _interopRequireDefault(_DateField);

var _ContextMenu = require("../../ContextMenu.react");

var _ContextMenu2 = _interopRequireDefault(_ContextMenu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SelectField = require("../../components/fields/SelectField.react");
var TextField = require("../../components/fields/TextInputField.react");
var TextAreaField = require("../../components/fields/TextAreaField.react");
var SwitchField = require("../../components/fields/SwitchField.react");
var LocationSelect = require("../../components/fields/LocationSelectField.react");
var NumberField = require("../../components/fields/NumberField.react");

var Button = require("../../components/ButtonComponent.react");

var TYPES = {
    date: _DateField2.default,
    select: SelectField,
    text: TextField,
    textarea: TextAreaField,
    switch: SwitchField,
    location: LocationSelect,
    number: NumberField
};

var DTActions = require("../DTActions");

var DataTableCell = function (_React$Component) {
    _inherits(DataTableCell, _React$Component);

    function DataTableCell(props) {
        _classCallCheck(this, DataTableCell);

        var _this = _possibleConstructorReturn(this, (DataTableCell.__proto__ || Object.getPrototypeOf(DataTableCell)).call(this, props));

        _this._unmountEdit = function () {
            _this.setState({
                edit: false
            });
        };

        _this._clear = function (evt) {
            if (_this.refs.item) {
                if (!_this.refs.item.contains(evt.target)) {
                    _this.setState({
                        context: false
                    });
                }
            }
        };

        _this._edit = function () {};

        _this._context = function (e) {
            e.preventDefault();
            _this.setState({
                context: true
            });
        };

        _this._onAction = function (data) {
            _this.setState({
                context: false
            });
            _this.props.onAction(data, _this.props.col, _this.props.row);
        };

        _this._onDblClick = function (e) {
            e.preventDefault();
            if (!_this.props.inline) {
                _this.props.onAction("CLICK", _this.props.col, _this.props.row);
            } else {
                var val = void 0;
                if (_this.props.col.name.startsWith("data.")) {
                    val = _this.props.row.data[_this.props.col.name.replace("data.", "")];
                } else {
                    val = ewars.getKeyPath(_this.props.col.name, _this.props.row);
                }
                ewars.emit("UNMOUNT_EDIT");
                _this.setState({
                    edit: true,
                    tempVal: val
                });
            }
        };

        _this._cancelEdit = function () {
            _this.setState({
                edit: false,
                tempVal: null
            });
        };

        _this._changeVal = function (prop, value) {
            _this.setState({
                tempVal: value
            });
        };

        _this._saveChange = function () {
            var bl = new ewars.Blocker(null, "Saving...");

            ewars.tx("com.ewars.collection", ["PATCH", _this.props.row.uuid, {
                prop: _this.props.col.name,
                value: _this.state.tempVal
            }]).then(function (resp) {
                bl.destroy();
                _this.props.modifyRow(_this.props.rowIndex, _this.props.col.name, _this.state.tempVal);
                _this.setState({
                    edit: false,
                    tempVal: null
                });
                ewars.growl("Updated");
            });
        };

        _this.state = {
            context: false,
            edit: false,
            tempVal: null
        };
        return _this;
    }

    _createClass(DataTableCell, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._clear);
            window.__hack__.addEventListener("contextmenu", this._clear);
            ewars.subscribe("UNMOUNT_EDIT", this._unmountEdit);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._clear);
            window.__hack__.removeEventListener("contextmenu", this._clear);
        }
    }, {
        key: "render",
        value: function render() {
            var style = { width: this.props.width };

            var val = void 0;
            if (this.props.col.name.startsWith("data.")) {
                val = this.props.row.data[this.props.col.name.replace("data.", "")];
            } else if (this.props.col.name.indexOf(".") > 0) {
                val = ewars.getKeyPath(this.props.col.name, this.props.row);
            } else {
                val = this.props.row[this.props.col.name] || null;
            }

            if (this.props.col.config.type == "select" && this.props.col.config.options) {
                if (this.props.col.config.multiple) {
                    if (val) {
                        var newVal = [];
                        this.props.col.config.options.forEach(function (item) {
                            if (val.indexOf(item[0]) >= 0) {
                                newVal.push(item[1]);
                            }
                        });

                        val = newVal.join(", ");
                    }
                } else {
                    this.props.col.config.options.forEach(function (item) {
                        if (item[0] == val) val = item[1];
                    });
                }
            }

            if (this.props.col.fmt) val = this.props.col.fmt(val, this.props.row);

            if (val instanceof Object) val = ewars.I18N(val);

            var valueClass = "dt-cell-value";
            if (this.props.col.config.type == "number") valueClass += " rh-align";

            var EditControl = void 0;
            if (this.state.edit) {
                EditControl = TYPES[this.props.col.config.type];
            }

            return React.createElement(
                "td",
                { style: style, ref: "cell", onContextMenu: this._context, onDoubleClick: this._onDblClick },
                React.createElement(
                    "div",
                    { style: { position: "relative", height: "100%" } },
                    React.createElement(
                        "div",
                        { className: valueClass },
                        this.state.edit ? React.createElement(
                            "div",
                            { className: "ide-row", style: { padding: 0 } },
                            React.createElement(
                                "div",
                                { className: "ide-col" },
                                React.createElement(EditControl, {
                                    onUpdate: this._changeVal,
                                    value: this.state.tempVal,
                                    config: this.props.col.config })
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-col" },
                                React.createElement(
                                    "div",
                                    { className: "btn-group", style: { marginTop: 2 } },
                                    React.createElement(Button, {
                                        onClick: this._saveChange,
                                        icon: "fa-save" }),
                                    React.createElement(Button, {
                                        onClick: this._cancelEdit,
                                        icon: "fa-close" })
                                )
                            )
                        ) : val
                    ),
                    this.state.context ? React.createElement(
                        "div",
                        { style: { position: "absolute", top: 22, left: 0 }, ref: "item" },
                        React.createElement(_ContextMenu2.default, {
                            absolute: false,
                            onClick: this._onAction,
                            actions: this.props.actions,
                            visible: this.state.context })
                    ) : null
                )
            );
        }
    }]);

    return DataTableCell;
}(React.Component);

exports.default = DataTableCell;


},{"../../ContextMenu.react":8,"../../components/ButtonComponent.react":30,"../../components/fields/DateField.react":37,"../../components/fields/LocationSelectField.react":42,"../../components/fields/NumberField.react":45,"../../components/fields/SelectField.react":47,"../../components/fields/SwitchField.react":49,"../../components/fields/TextAreaField.react":50,"../../components/fields/TextInputField.react":51,"../DTActions":9}],15:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _DateField = require("../../components/fields/DateField.react");

var _DateField2 = _interopRequireDefault(_DateField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = require("../../components/ButtonComponent.react");

var SelectField = require("../../components/fields/SelectField.react");
var NumberField = require("../../components/fields/NumberField.react");

var TextField = require("../../components/fields/TextInputField.react");
var LocationSelectField = require("../../components/fields/LocationSelectField.react");

var CONDITION_OPTIONS = [["EQ", "Is equal to"], ["NEQ", "Is not equal to"], ["NULL", "Is empty"], ["NOT_NULL", "Is not empty"], ["STARTS_WITH", "Begins with"], ["ENDS_WITH", "Ends with"], ["GT", "Is greater than"], ["GTE", "Is greater than or equal to"], ["LT", "Is less than"], ["LTE", "Is less than or equal to"], ["IN", "Is between"], ["NIN", "Is not between"], ["WITHIN", "Is within"], ["NWITHIN", "Is not within"], ["UNDER", "Under"]];

var FIELDS = {
    filter_condition: {
        type: "select",
        options: []
    },
    number_field: {}
};

var TYPE_MAPS = {
    "user": ["EQ", "NEQ"],
    "number": ["NULL", "NOT_NULL", "EQ", "NEQ", "GT", "GTE", "LT", "LTE", "IN", "NIN"],
    "text": ["EQ", "NEQ", "NULL", "NOT_NULL", "STARTS_WITH", "ENDS_WITH"],
    "date": ["NULL", "NOT_NULL", "EQ", "NEQ", "GT", "GTE", "LT", "LTE", "IN", "NIN"],
    "location": ["EQ", "UNDER"],
    "select": ["NULL", "NOT_NULL", "EQ", "NEQ"]
};

var User = function (_React$Component) {
    _inherits(User, _React$Component);

    function User(props) {
        _classCallCheck(this, User);

        var _this = _possibleConstructorReturn(this, (User.__proto__ || Object.getPrototypeOf(User)).call(this, props));

        _this._select = function () {
            _this.props.onSelect(_this.props.data);
        };

        return _this;
    }

    _createClass(User, [{
        key: "render",
        value: function render() {

            var className = "fa fa-square-o";
            if (this.props.value) {
                if (this.props.value.indexOf(this.props.data.id) >= 0) className = "fa fa-check-square-o";
            }

            return React.createElement(
                "div",
                { className: "block" },
                React.createElement(
                    "div",
                    { className: "block-content", style: { padding: 0 }, onClick: this._select },
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        React.createElement(
                            "div",
                            { className: "ide-col", style: { maxWidth: 18, padding: 8 } },
                            React.createElement("i", { className: className })
                        ),
                        React.createElement(
                            "div",
                            { className: "ide-col", style: { padding: 8 } },
                            this.props.data.name,
                            React.createElement("div", { style: { height: 8 } }),
                            "[",
                            this.props.data.email,
                            "]"
                        )
                    )
                )
            );
        }
    }]);

    return User;
}(React.Component);

var UserFilter = function (_React$Component2) {
    _inherits(UserFilter, _React$Component2);

    function UserFilter(props) {
        _classCallCheck(this, UserFilter);

        var _this2 = _possibleConstructorReturn(this, (UserFilter.__proto__ || Object.getPrototypeOf(UserFilter)).call(this, props));

        _this2.query = function (term) {
            ewars.tx("com.ewars.query", ["sso", ['id', 'name', 'email'], { name: { like: term } }, null, null, null, null]).then(function (resp) {
                this.setState({
                    data: resp
                });
            }.bind(_this2));
        };

        _this2._onSearchChange = function (e) {
            if (e.target.value.length > 2) {
                _this2.setState({ search: e.target.value });
                _this2.query(_this2.state.search);
            } else {
                _this2.setState({ search: e.target.value, data: [] });
            }
        };

        _this2._onSelect = function (user) {
            var data = _this2.props.value;
            if (!data) data = [];

            if (data.indexOf(user.id) < 0) {
                data.push(user.id);
                _this2.props.onUpdate(_this2.props.name, data);
            } else {
                var place = data.indexOf(user.id);
                data.splice(place, 1);
                _this2.props.onUpdate(_this2.props.name, data);
            }
        };

        _this2.state = {
            search: "",
            data: []
        };
        return _this2;
    }

    _createClass(UserFilter, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col", style: { paddingBottom: 8 } },
                        React.createElement("input", { type: "text", value: this.state.search, onChange: this._onSearchChange })
                    )
                ),
                this.state.data.length > 0 ? React.createElement(
                    "div",
                    { className: "ide-row", style: { height: 200, border: "1px solid #CCC" } },
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement(
                            "div",
                            { className: "ide-panel ide-panel-absolute ide-scroll" },
                            React.createElement(
                                "div",
                                { className: "block-tree", style: { padding: 0, margin: 0, textAlign: "left" } },
                                this.state.data.map(function (user) {
                                    return React.createElement(User, { data: user, onSelect: this._onSelect, value: this.props.value });
                                }.bind(this))
                            )
                        )
                    )
                ) : null
            );
        }
    }]);

    return UserFilter;
}(React.Component);

var DataTableFilterControl = function (_React$Component3) {
    _inherits(DataTableFilterControl, _React$Component3);

    function DataTableFilterControl(props) {
        _classCallCheck(this, DataTableFilterControl);

        var _this3 = _possibleConstructorReturn(this, (DataTableFilterControl.__proto__ || Object.getPrototypeOf(DataTableFilterControl)).call(this, props));

        _this3._filterChange = function (prop, value) {
            _this3.setState(_extends({}, _this3.state, _defineProperty({}, prop, value)));
        };

        _this3._clear = function () {
            _this3.setState(_extends({}, _this3.state, {
                condition: null,
                value: null,
                upper: null
            }));
            _this3.props.onSave(null);
        };

        _this3._onSave = function () {
            if (!_this3.state.condition) {
                _this3.props.onSave(null);
                return;
            }

            var filter = {};
            switch (_this3.state.condition) {
                case "EQ":
                    filter.eq = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
                case "NEQ":
                    filter.neq = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
                case "GT":
                    filter.gt = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
                case "GTE":
                    filter.gte = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
                case "LT":
                    filter.lt = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
                case "LTE":
                    filter.lte = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
                case "UNDER":
                    filter.under = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
                default:
                    filter.eq = _this3.state.value;
                    filter.type = _this3.props.cell.config.type || null;
                    break;
            }

            _this3.props.onSave(filter);
        };

        _this3.state = {
            filter: {},
            condition: "EQ",
            value: null,
            upper: null
        };
        return _this3;
    }

    _createClass(DataTableFilterControl, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            if (this.props.cell.config.type == "date") {
                this.state.value = new Date();
            }
        }
    }, {
        key: "render",
        value: function render() {
            var Control = void 0;
            if (this.props.cell.config.type == "date") Control = _DateField2.default;
            if (this.props.cell.config.type == "select") Control = SelectField;
            if (this.props.cell.config.type == "number") Control = NumberField;
            if (this.props.cell.config.type == "location") Control = LocationSelectField;
            if (this.props.cell.config.type == "text") Control = TextField;
            if (this.props.cell.config.type == "textarea") Control = TextField;

            if (this.props.cell.config.type == "user") Control = UserFilter;

            var SecondControl = void 0;
            if (["NIN", "IN", "WITHIN", "NWITHIN"].indexOf(this.state.condition) >= 0) {
                SecondControl = Control;
            }

            var options = void 0;
            if (TYPE_MAPS[this.props.cell.config.type]) {
                options = [];
                CONDITION_OPTIONS.forEach(function (item) {
                    if (TYPE_MAPS[this.props.cell.config.type].indexOf(item[0]) >= 0) {
                        options.push(item);
                    }
                }.bind(this));
                FIELDS.filter_condition.options = options;
            }

            var controlConfig = ewars.copy(this.props.cell.config);
            if (this.props.cell.config.type == "location") {
                if (window.user.user_type != "SUPER_ADMIN") {
                    controlConfig.parentId = window.user.account.location_id;
                    if (window.user.user_type == "REGIONAL_ADMIN") {
                        controlConfig.parentId = window.user.location_id;
                    }
                }
            }

            var hasFilter = false;
            var comparator = this.state.condition;
            var value = this.state.value;
            var cellName = this.props.cell.config.filterKey || this.props.cell.name;
            if (this.props.filter[cellName]) {
                // There is filtering applied
                comparator = Object.keys(this.props.filter[cellName])[0];
                value = this.props.filter[cellName][Object.keys(this.props.filter[cellName])[0]];
            }

            return React.createElement(
                "div",
                { className: "dt-filter-controls" },
                React.createElement(
                    "div",
                    { className: "control-action", onClick: this._clear },
                    "Clear"
                ),
                React.createElement(
                    "div",
                    { className: "control-set" },
                    React.createElement(
                        "label",
                        { htmlFor: "" },
                        "Filter by condition:"
                    ),
                    React.createElement(SelectField, {
                        value: comparator,
                        name: "condition",
                        onUpdate: this._filterChange,
                        config: FIELDS.filter_condition })
                ),
                React.createElement(
                    "div",
                    { className: "control-set" },
                    React.createElement(
                        "label",
                        { htmlFor: "" },
                        "Filter by Value"
                    ),
                    React.createElement(Control, {
                        onUpdate: this._filterChange,
                        name: "value",
                        value: value,
                        config: controlConfig }),
                    SecondControl ? React.createElement(SecondControl, {
                        onUpdate: this._filterChange,
                        name: "upper",
                        value: this.state.upper,
                        config: this.props.cell.config }) : null,
                    React.createElement("div", { className: "selectables" })
                ),
                React.createElement(
                    "div",
                    { className: "control-set" },
                    React.createElement(
                        "div",
                        { className: "btn-group" },
                        React.createElement(Button, {
                            onClick: this._onSave,
                            label: "Save",
                            colour: "green" }),
                        React.createElement(Button, {
                            onClick: this.props.onCancel,
                            label: "Cancel" })
                    )
                )
            );
        }
    }]);

    return DataTableFilterControl;
}(React.Component);

exports.default = DataTableFilterControl;


},{"../../components/ButtonComponent.react":30,"../../components/fields/DateField.react":37,"../../components/fields/LocationSelectField.react":42,"../../components/fields/NumberField.react":45,"../../components/fields/SelectField.react":47,"../../components/fields/TextInputField.react":51}],16:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _DataTableFilterControl = require("./DataTableFilterControl.react");

var _DataTableFilterControl2 = _interopRequireDefault(_DataTableFilterControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DataTableHeaderCell = function (_React$Component) {
    _inherits(DataTableHeaderCell, _React$Component);

    function DataTableHeaderCell(props) {
        _classCallCheck(this, DataTableHeaderCell);

        var _this = _possibleConstructorReturn(this, (DataTableHeaderCell.__proto__ || Object.getPrototypeOf(DataTableHeaderCell)).call(this, props));

        _this._clear = function (evt) {
            if (_this.refs.item) {
                if (!_this.refs.item.contains(evt.target)) {
                    _this.setState({
                        controls: false
                    });
                }
            }
        };

        _this._filter = function () {
            _this.setState({
                controls: !_this.state.controls
            });
        };

        _this._sort = function (e) {
            e.preventDefault();
            var colName = void 0;
            if (_this.props.data.config) {
                if (_this.props.data.config.name) colName = _this.props.data.config.name;
            }
            if (_this.props.data.name) colName = _this.props.data.name;

            if (_this.props.data.config.sortKey) colName = _this.props.data.config.sortKey;
            ewars.z.dispatch(_this.props.id || "DT_TRANSIENT", "UPDATE_SORT", {
                field: colName,
                fieldType: _this.props.data.type || _this.props.data.config.type
            });
        };

        _this._cancelFilter = function () {
            _this.setState({
                controls: false
            });
        };

        _this._applyFilter = function (value) {
            _this.setState({
                controls: false
            });

            ewars.z.dispatch(_this.props.id || "DT_TRANSIENT", "UPDATE_FILTER", {
                key: _this.props.data.config.filterKey || _this.props.data.name,
                config: value
            });
        };

        _this.state = {
            showControl: false,
            controls: false
        };
        return _this;
    }

    _createClass(DataTableHeaderCell, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._clear);
            window.__hack__.addEventListener("contextmenu", this._clear);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._clear);
            window.__hack__.removeEventListener("contextmenu", this._clear);
        }
    }, {
        key: "render",
        value: function render() {
            var label = ewars.I18N(this.props.data.config.label);
            var fieldType = this.props.data.config.type;

            var hasControls = true;
            if (["matrix", "display", "row"].indexOf(this.props.data.config.type) >= 0) hasControls = false;

            var colSpan = this.props.data.cols || 1,
                rowSpan = this.props.data.rows || 1;

            var width = (this.props.data.width || 90) + "px";

            var sortIcon = void 0;

            var colName = void 0;
            if (this.props.data.config) {
                if (this.props.data.config.name) colName = this.props.data.config.name;
            }
            if (this.props.data.name) colName = this.props.data.name;

            sortIcon = React.createElement("i", { className: "fa fa-sort" });
            if (this.props.sort[colName]) {
                if (this.props.sort[colName].indexOf("DESC") >= 0) sortIcon = React.createElement("i", { className: "fa fa-caret-up" });
                if (this.props.sort[colName].indexOf("ASC") >= 0) sortIcon = React.createElement("i", { className: "fa fa-caret-down" });
            }

            var actionControlClass = "ide-col dt-filter-btn";
            if (this.props.filter[this.props.data.config.filterKey || this.props.data.name]) actionControlClass += " active";

            var filterIconClass = "fa fa-filter";
            if (this.props.filter[this.props.data.config.filterKey || this.props.data.name]) filterIconClass += " green";

            return React.createElement(
                "th",
                { ref: "cell", rowSpan: rowSpan, colSpan: colSpan, className: "dt-header-cell", style: { position: "relative" } },
                React.createElement(
                    "div",
                    { className: "ide-row", style: { position: "relative", height: "100%" } },
                    React.createElement(
                        "div",
                        { className: "ide-col border-right", style: { padding: "5px 10px" } },
                        label
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 20, padding: "5px", cursor: "pointer" }, onClick: this._sort },
                        sortIcon
                    ),
                    hasControls ? React.createElement(
                        "div",
                        { className: "ide-col border-left", style: { maxWidth: 20, padding: 5, cursor: "pointer" }, onClick: this._filter },
                        React.createElement("i", { className: filterIconClass })
                    ) : null
                ),
                this.state.controls ? React.createElement(
                    "div",
                    { style: { position: "absolute", top: 22, left: 0, zIndex: 1 }, ref: "item" },
                    React.createElement(_DataTableFilterControl2.default, {
                        id: this.props.id,
                        filter: this.props.filter,
                        onCancel: this._cancelFilter,
                        onSave: this._applyFilter,
                        cell: this.props.data })
                ) : null
            );
        }
    }]);

    return DataTableHeaderCell;
}(React.Component);

DataTableHeaderCell.defaultProps = {
    sortable: false,
    filterable: false
};
exports.default = DataTableHeaderCell;


},{"./DataTableFilterControl.react":15}],17:[function(require,module,exports){
'use strict';

var assign = require('object-assign');

var _callbacks = [];
var _promises = [];

var CoreDispatcher = function CoreDispatcher() {};
CoreDispatcher.prototype = assign({}, CoreDispatcher.prototype, {

    /**
     * Register a Store's callback so that it may be invoked by an action.
     * @param {function} callback The callback to be registered.
     * @return {number} The index of the callback within the _callbacks array.
     */
    register: function register(callback) {
        _callbacks.push(callback);
        return _callbacks.length - 1; // index
    },

    /**
     * dispatch
     * @param  {object} payload The data from the action.
     */
    dispatch: function dispatch(payload) {
        // First create array of promises for callbacks to reference.
        var resolves = [];
        var rejects = [];
        _promises = _callbacks.map(function (_, i) {
            return new Promise(function (resolve, reject) {
                resolves[i] = resolve;
                rejects[i] = reject;
            });
        });
        // Dispatch to callbacks and resolve/reject promises.
        _callbacks.forEach(function (callback, i) {
            // Callback can return an obj, to resolve, or a promise, to chain.
            // See waitFor() for why this might be useful.
            Promise.resolve(callback(payload)).then(function () {
                resolves[i](payload);
            }, function () {
                rejects[i](new Error('Dispatcher callback unsuccessful'));
            });
        });
        _promises = [];
    }
});

module.exports = CoreDispatcher;


},{"object-assign":87}],18:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("./Layout");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = function (_React$Component) {
    _inherits(Button, _React$Component);

    function Button(props) {
        _classCallCheck(this, Button);

        var _this = _possibleConstructorReturn(this, (Button.__proto__ || Object.getPrototypeOf(Button)).call(this, props));

        _this._onClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            _this.props.onClick(_this.props.data);
        };

        return _this;
    }

    _createClass(Button, [{
        key: "render",
        value: function render() {

            var className = "bttn";
            if (this.props.color) className += " " + this.props.color;
            if (this.props.active) className += " active";

            var icon = void 0;

            if (this.props.icon) {
                if (this.props.icon.constructor == Array) {
                    icon = React.createElement(
                        "div",
                        { className: "fa-stack" },
                        this.props.icon.map(function (ic) {
                            return React.createElement("i", { className: "fa " + ic });
                        })
                    );
                } else {
                    icon = React.createElement("i", { className: "fa " + this.props.icon });
                }
            }

            return React.createElement(
                "div",
                { className: className, onClick: this._onClick },
                icon,
                this.props.label ? ewars.I18N(this.props.label) : null
            );
        }
    }]);

    return Button;
}(React.Component);

Button.defaultProps = {
    color: false,
    active: false
};
exports.default = Button;


},{"./Layout":21}],19:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Control = function (_React$Component) {
    _inherits(Control, _React$Component);

    function Control(props) {
        _classCallCheck(this, Control);

        var _this = _possibleConstructorReturn(this, (Control.__proto__ || Object.getPrototypeOf(Control)).call(this, props));

        _this.onClick = function () {
            _this.props.onClick(_this.props.action);
        };

        return _this;
    }

    _createClass(Control, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "iw-control", onClick: this.onClick },
                React.createElement("i", { className: "fa " + this.props.icon })
            );
        }
    }]);

    return Control;
}(React.Component);

exports.default = Control;


},{}],20:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Control = require("./Control");

var _Control2 = _interopRequireDefault(_Control);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Dashboard = function (_React$Component) {
    _inherits(Dashboard, _React$Component);

    function Dashboard(props) {
        _classCallCheck(this, Dashboard);

        return _possibleConstructorReturn(this, (Dashboard.__proto__ || Object.getPrototypeOf(Dashboard)).call(this, props));
    }

    _createClass(Dashboard, [{
        key: "render",
        value: function render() {
            var _this2 = this;

            var counter = 0;
            return React.createElement(
                "div",
                { className: "ide-panel ide-panel-absolute ide-scroll dashboard" },
                React.createElement(
                    "div",
                    { className: "grid" },
                    this.props.data.definition.map(function (row, index) {
                        counter++;
                        var key = _this2.props.data.uuid + "_" + counter;
                        return React.createElement(Row, {
                            data: row,
                            uuid: _this2.props.data.uuid,
                            key: key,
                            index: index });
                    })
                )
            );
        }
    }]);

    return Dashboard;
}(React.Component);

var EditableList = function (_React$Component2) {
    _inherits(EditableList, _React$Component2);

    function EditableList(props) {
        _classCallCheck(this, EditableList);

        var _this3 = _possibleConstructorReturn(this, (EditableList.__proto__ || Object.getPrototypeOf(EditableList)).call(this, props));

        _this3._handleAction = function (action) {
            _this3.props.onAction(action, _this3.state.selectedIndex, _this3.props.items[_this3.state.selectedIndex]);
        };

        _this3._onItemClick = function (index) {
            _this3.setState({
                selectedIndex: index
            });
        };

        _this3._clearSelected = function (evt) {};

        _this3.state = {
            selectedIndex: null
        };
        return _this3;
    }

    _createClass(EditableList, [{
        key: "render",
        value: function render() {
            var _this4 = this;

            var selected = void 0;
            if (this.state.selectedIndex != null) {
                selected = this.props.items[this.state.selectedIndex];
            }

            return React.createElement(
                "div",
                { className: "iw-edit-list" },
                React.createElement(
                    "div",
                    { className: "iw-edit-list-items", onClick: this._clearSelected },
                    this.props.items.map(function (item, index) {
                        return React.createElement(_this4.props.Template, {
                            key: "EL_ITEM_" + index,
                            active: index == _this4.state.selectedIndex,
                            data: item,
                            onChange: _this4.props.onChange || null,
                            onAction: _this4.props.onAction,
                            index: index,
                            onClick: _this4._onItemClick });
                    })
                ),
                React.createElement(
                    "div",
                    { className: "iw-edit-list-controls" },
                    this.props.actions.map(function (action) {
                        var show = true;
                        if (action.show) {
                            show = false;
                        }
                        return React.createElement(_Control2.default, {
                            key: "CONTROL_" + action.action,
                            icon: action.icon,
                            action: action.action,
                            onClick: _this4._handleAction });
                    })
                )
            );
        }
    }]);

    return EditableList;
}(React.Component);

EditableList.defaultProps = {
    orderable: false,
    onAdd: null,
    onRemove: null,
    onOrder: null,
    items: [],
    onChange: null,
    Template: null
};
exports.default = EditableList;


},{"./Control":19}],21:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Cell = function (_React$Component) {
    _inherits(Cell, _React$Component);

    function Cell(props) {
        _classCallCheck(this, Cell);

        var _this = _possibleConstructorReturn(this, (Cell.__proto__ || Object.getPrototypeOf(Cell)).call(this, props));

        _this._onClick = function () {
            if (_this.props.onClick) _this.props.onClick();
        };

        return _this;
    }

    _createClass(Cell, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxWidth = this.props.width || null;
            style.padding = this.props.padding || null;

            var className = "ide-col";
            className += this.props.borderRight ? " border-right" : "";
            className += this.props.borderBottom ? " border-bottom" : "";
            className += this.props.borderLeft ? " border-left" : "";
            className += this.props.borderTop ? " border-top" : "";
            className += this.props.addClass ? " " + this.props.addClass : "";

            if (this.props.onClick) style.cursor = "pointer";

            Object.assign(style, this.props.style || {});

            return React.createElement(
                "div",
                { className: className, style: style, onClick: this._onClick },
                this.props.children
            );
        }
    }]);

    return Cell;
}(React.Component);

Cell.defaultProps = {
    width: null,
    borderTop: false,
    borderRight: false,
    borderBottom: false,
    borderLeft: false,
    addClass: null,
    padding: null,
    onClick: null,
    style: {}
};

var Row = function (_React$Component2) {
    _inherits(Row, _React$Component2);

    function Row(props) {
        _classCallCheck(this, Row);

        return _possibleConstructorReturn(this, (Row.__proto__ || Object.getPrototypeOf(Row)).call(this, props));
    }

    _createClass(Row, [{
        key: "render",
        value: function render() {
            var style = {};

            style.maxHeight = this.props.height || null;

            if (this.props.style) Object.assign(style, this.props.style);

            return React.createElement(
                "div",
                { className: "ide-row", style: style },
                this.props.children
            );
        }
    }]);

    return Row;
}(React.Component);

Row.defaultProps = {
    height: null,
    borderTop: false,
    borderBottom: false,
    borderRight: false,
    borderLeft: false,
    style: null
};

var Layout = function (_React$Component3) {
    _inherits(Layout, _React$Component3);

    function Layout(props) {
        _classCallCheck(this, Layout);

        return _possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).call(this, props));
    }

    _createClass(Layout, [{
        key: "render",
        value: function render() {
            var style = {};

            if (this.props.style) Object.assign(style, this.props.style);
            return React.createElement(
                "div",
                { className: "ide-layout", style: style },
                this.props.children
            );
        }
    }]);

    return Layout;
}(React.Component);

var Panel = function (_React$Component4) {
    _inherits(Panel, _React$Component4);

    function Panel(props) {
        _classCallCheck(this, Panel);

        return _possibleConstructorReturn(this, (Panel.__proto__ || Object.getPrototypeOf(Panel)).call(this, props));
    }

    _createClass(Panel, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "ide-panel ide-panel-absolute ide-scroll" },
                this.props.children
            );
        }
    }]);

    return Panel;
}(React.Component);

exports.Layout = Layout;
exports.Row = Row;
exports.Cell = Cell;
exports.Panel = Panel;


},{}],22:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("./Layout");

var _Toolbar = require("./Toolbar");

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _Button = require("./Button");

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Shade = function (_React$Component) {
    _inherits(Shade, _React$Component);

    function Shade(props) {
        _classCallCheck(this, Shade);

        var _this = _possibleConstructorReturn(this, (Shade.__proto__ || Object.getPrototypeOf(Shade)).call(this, props));

        _this.onClose = function () {
            _this.props.onAction("CLOSE", null);
        };

        _this._onAction = function (data) {
            _this.props.onAction(data.action);
        };

        return _this;
    }

    _createClass(Shade, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            if (this.props.shown) {} else {}
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            if (nextProps.shown && !this.props.shown) {}
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            if (!this.props.shown) return React.createElement("div", { className: "shade hidden" });

            return React.createElement(
                "div",
                { className: "shade shown" },
                React.createElement(
                    _Layout.Layout,
                    null,
                    React.createElement(
                        _Layout.Row,
                        null,
                        React.createElement(
                            _Layout.Cell,
                            { width: "20%", onClick: this.onClose, addClass: "shade-shoulder" },
                            React.createElement(
                                "div",
                                { className: "shade-close" },
                                React.createElement("i", { className: "fa fa-close" })
                            )
                        ),
                        React.createElement(
                            _Layout.Cell,
                            null,
                            React.createElement(
                                "div",
                                { className: "shade-content", ref: "content" },
                                React.createElement(
                                    _Layout.Layout,
                                    null,
                                    this.props.toolbar ? React.createElement(
                                        _Toolbar2.default,
                                        { label: this.props.title, icon: this.props.icon },
                                        React.createElement(
                                            "div",
                                            { className: "btn-group pull-right" },
                                            this.props.actions.map(function (item) {
                                                return React.createElement(_Button2.default, {
                                                    label: ewars.I18N(item.label),
                                                    icon: item.icon || null,
                                                    onClick: _this2._onAction,
                                                    color: item.color || null,
                                                    data: item });
                                            })
                                        )
                                    ) : null,
                                    React.createElement(
                                        _Layout.Row,
                                        null,
                                        React.createElement(
                                            _Layout.Cell,
                                            null,
                                            this.props.children
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return Shade;
}(React.Component);

Shade.defaultProps = {
    shown: false,
    title: "",
    actions: [],
    onAction: null,
    toolbar: true
};
exports.default = Shade;


},{"./Button":18,"./Layout":21,"./Toolbar":23}],23:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Toolbar = function (_React$Component) {
    _inherits(Toolbar, _React$Component);

    function Toolbar(props) {
        _classCallCheck(this, Toolbar);

        return _possibleConstructorReturn(this, (Toolbar.__proto__ || Object.getPrototypeOf(Toolbar)).call(this, props));
    }

    _createClass(Toolbar, [{
        key: "render",
        value: function render() {
            console.log(this.props.style);

            return React.createElement(
                "div",
                { className: "ide-row", style: { maxHeight: 35 } },
                React.createElement(
                    "div",
                    { className: "ide-col" },
                    React.createElement(
                        "div",
                        { className: "ide-tbar", style: this.props.style || {} },
                        this.props.icon ? React.createElement(
                            "div",
                            { className: "ide-tbar-icon" },
                            React.createElement("i", { className: "fa " + this.props.icon })
                        ) : null,
                        this.props.label ? React.createElement(
                            "div",
                            { className: "ide-tbar-text" },
                            this.props.label
                        ) : null,
                        this.props.children
                    )
                )
            );
        }
    }]);

    return Toolbar;
}(React.Component);

Toolbar.defaultProps = {
    label: null
};
exports.default = Toolbar;


},{}],24:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Button = require("../Button");

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ButtonGroup = function (_React$Component) {
    _inherits(ButtonGroup, _React$Component);

    function ButtonGroup() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, ButtonGroup);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ButtonGroup.__proto__ || Object.getPrototypeOf(ButtonGroup)).call.apply(_ref, [this].concat(args))), _this), _this._onClick = function (data) {
            _this.props.onUpdate(_this.props.config.nameOverride || _this.props.name, data[0], _this.props.path || _this.props.name);
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(ButtonGroup, [{
        key: "render",
        value: function render() {
            var _this2 = this;

            console.log(this.props.config);
            return React.createElement(
                "div",
                { className: "btn-group formed" },
                this.props.config.options.map(function (button) {
                    return React.createElement(_Button2.default, {
                        icon: button[2] || null,
                        label: button[1],
                        active: _this2.props.value == button[0],
                        onClick: _this2._onClick,
                        data: button });
                })
            );
        }
    }]);

    return ButtonGroup;
}(React.Component);

exports.default = ButtonGroup;


},{"../Button":18}],25:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("../Layout");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextField = require("../../components/fields/TextInputField.react");

var COLOURS = [["#000000", "#191919", "#333333", "#4C4C4C", "#666666"], ["#7F7F7F", "#999999", "#B3B3B3", "#E6E6E6", "#FFFFFF"], ["#004080", "#000080", "#0000FF", "#0080FF", "#66CCFF"], ["#66FFFF", "#00FFFF", "#008080", "#008040", "#408000"], ["#008000", "#00FF00", "#80FF00", "#66FF66", "#00FF80"], ["#66FFCC", "#804000", "#800000", "#FF0000", "#FF6666"], ["#FF8000", "#FFFF00", "#FFFF66", "#FFCC66", "#400080"], ["#800080", "#800040", "#8000FF", "#FF00FF", "#FF0080"], ["#CC66FF", "#FF66FF"]];

var ColorCell = function (_React$Component) {
    _inherits(ColorCell, _React$Component);

    function ColorCell(props) {
        _classCallCheck(this, ColorCell);

        var _this = _possibleConstructorReturn(this, (ColorCell.__proto__ || Object.getPrototypeOf(ColorCell)).call(this, props));

        _this._onClick = function () {
            _this.props.onClick(_this.props.data);
        };

        return _this;
    }

    _createClass(ColorCell, [{
        key: "render",
        value: function render() {
            return React.createElement(_Layout.Cell, { style: { backgroundColor: this.props.data, height: 20 }, onClick: this._onClick });
        }
    }]);

    return ColorCell;
}(React.Component);

var Colour = function (_React$Component2) {
    _inherits(Colour, _React$Component2);

    function Colour(props) {
        _classCallCheck(this, Colour);

        var _this2 = _possibleConstructorReturn(this, (Colour.__proto__ || Object.getPrototypeOf(Colour)).call(this, props));

        _this2._onBodyClick = function (evt) {
            if (_this2.refs.selector) {
                var area = _this2.refs.selector.getDOMNode ? _this2.refs.selector.getDOMNode() : _this2.refs.selector;

                if (!area.contains(evt.target)) {
                    _this2.setState({
                        shown: false
                    });
                }
            }
        };

        _this2._trigger = function () {
            _this2.setState({
                shown: !_this2.state.shown
            });
        };

        _this2._onColourSelect = function (colour) {
            _this2.setState({ shown: false });
            _this2.props.onUpdate(_this2.props.name, colour);
        };

        _this2._onCustomChange = function (prop, value) {
            _this2.props.onUpdate(_this2.props.name, value);
        };

        _this2.state = {
            shown: false
        };
        return _this2;
    }

    _createClass(Colour, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._onBodyClick);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._onBodyClick);
        }
    }, {
        key: "render",
        value: function render() {
            var _this3 = this;

            var STYLE = {
                height: 30,
                backgroundColor: "#FFFFFF",
                padding: 5,
                textAlign: "center"
            };
            if (this.props.value) STYLE.backgroundColor = this.props.value;

            return React.createElement(
                "div",
                { className: "ew-select", ref: "selector", style: { minWidth: 0 } },
                React.createElement(
                    "div",
                    { className: "handle", onClick: this._trigger, style: { minWidth: 0 } },
                    React.createElement(
                        _Layout.Row,
                        null,
                        React.createElement(
                            _Layout.Cell,
                            { width: 31, style: STYLE },
                            React.createElement("i", { className: "fa fa-eyedropper" })
                        )
                    )
                ),
                this.state.shown ? React.createElement(
                    "div",
                    { className: "ew-select-data", style: { padding: 5, width: 250 } },
                    React.createElement(
                        _Layout.Layout,
                        null,
                        React.createElement(
                            _Layout.Row,
                            null,
                            React.createElement(
                                _Layout.Cell,
                                null,
                                React.createElement(
                                    _Layout.Layout,
                                    null,
                                    COLOURS.map(function (row) {
                                        return React.createElement(
                                            _Layout.Row,
                                            null,
                                            row.map(function (cell) {
                                                return React.createElement(ColorCell, {
                                                    onClick: _this3._onColourSelect,
                                                    data: cell });
                                            })
                                        );
                                    })
                                )
                            )
                        ),
                        React.createElement(
                            _Layout.Row,
                            null,
                            React.createElement(
                                _Layout.Cell,
                                null,
                                React.createElement("div", { style: { height: 10 } }),
                                React.createElement(
                                    "label",
                                    { htmlFor: "" },
                                    "Custom"
                                ),
                                React.createElement(TextField, {
                                    name: "colour",
                                    onUpdate: this._onCustomChange,
                                    value: this.props.value })
                            )
                        )
                    )
                ) : null
            );
        }
    }]);

    return Colour;
}(React.Component);

exports.default = Colour;


},{"../../components/fields/TextInputField.react":51,"../Layout":21}],26:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _EditableList = require("../EditableList");

var _EditableList2 = _interopRequireDefault(_EditableList);

var _Layout = require("../Layout");

var _Colour = require("./Colour");

var _Colour2 = _interopRequireDefault(_Colour);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumberField = require("../../components/fields/NumberField.react");

var ACTIONS = [{ icon: "fa-plus", action: "ADD" }, { icon: "fa-minus", action: "REMOVE" }, { icon: "fa-caret-up", action: "MOVE_UP" }, { icon: "fa-caret-down", action: "MOVE_DOWN" }];

var SEP_STYLE = {
    textAlign: "center",
    lineHeight: "30px"
};

var Threshold = function (_React$Component) {
    _inherits(Threshold, _React$Component);

    function Threshold(props) {
        _classCallCheck(this, Threshold);

        var _this = _possibleConstructorReturn(this, (Threshold.__proto__ || Object.getPrototypeOf(Threshold)).call(this, props));

        _this.onClick = function () {
            _this.props.onClick(_this.props.index);
        };

        _this._onChange = function (prop, value) {
            _this.props.onChange(_this.props.index, prop, value);
        };

        return _this;
    }

    _createClass(Threshold, [{
        key: "render",
        value: function render() {
            var className = "iw-list-edit-item";
            if (this.props.active) className += " active";

            return React.createElement(
                "div",
                { className: className, onClick: this.onClick, style: { padding: 2 } },
                React.createElement(
                    _Layout.Row,
                    null,
                    React.createElement(
                        _Layout.Cell,
                        { width: 38, style: { paddingRight: 5 } },
                        React.createElement(_Colour2.default, {
                            name: "colour",
                            value: this.props.data[2] || "#333",
                            onUpdate: this._onChange })
                    ),
                    React.createElement(
                        _Layout.Cell,
                        null,
                        React.createElement(NumberField, {
                            name: "min",
                            onUpdate: this._onChange,
                            value: this.props.data[0] })
                    ),
                    React.createElement(
                        _Layout.Cell,
                        { width: 20, style: SEP_STYLE },
                        "to"
                    ),
                    React.createElement(
                        _Layout.Cell,
                        null,
                        React.createElement(NumberField, {
                            name: "max",
                            onUpdate: this._onChange,
                            value: this.props.data[1] })
                    )
                )
            );
        }
    }]);

    return Threshold;
}(React.Component);

var ColourThresholds = function (_React$Component2) {
    _inherits(ColourThresholds, _React$Component2);

    function ColourThresholds(props) {
        _classCallCheck(this, ColourThresholds);

        var _this2 = _possibleConstructorReturn(this, (ColourThresholds.__proto__ || Object.getPrototypeOf(ColourThresholds)).call(this, props));

        _this2._onListAction = function (action, selectedIndex) {
            if (action == "ADD") {
                var thresholds = _this2.props.value || [];
                thresholds.push([0, 0, "#333"]);
                _this2.props.onUpdate(_this2.props.name, thresholds);
            }

            if (action == "REMOVE") {
                var _thresholds = _this2.props.value || [];
                _thresholds.splice(selectedIndex, 1);
                _this2.props.onUpdate(_this2.props.name, _thresholds);
            }
        };

        _this2._onPropChange = function (index, prop, value) {
            var threshes = _this2.props.value;
            if (prop == "colour") threshes[index][2] = value;
            if (prop == "min") threshes[index][0] = value;
            if (prop == "max") threshes[index][1] = value;

            _this2.props.onUpdate(_this2.props.name, threshes);
        };

        return _this2;
    }

    _createClass(ColourThresholds, [{
        key: "render",
        value: function render() {
            return React.createElement(_EditableList2.default, {
                onAction: this._onListAction,
                Template: Threshold,
                onChange: this._onPropChange,
                items: this.props.value || [],
                actions: ACTIONS });
        }
    }]);

    return ColourThresholds;
}(React.Component);

exports.default = ColourThresholds;


},{"../../components/fields/NumberField.react":45,"../EditableList":20,"../Layout":21,"./Colour":25}],27:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("../Layout");

var _ButtonGroup = require("./ButtonGroup");

var _ButtonGroup2 = _interopRequireDefault(_ButtonGroup);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumberField = require("../../components/fields/NumberField.react");

var PX_REG = /([px])/g;
var EM_REG = /([em])/g;
var PERC_REG = /([%])/g;

var BUTTON_OPTIONS = [["px", "px"], ["%", "%"], ["em", "em"]];

var UNITS_CONFIG = {
    options: BUTTON_OPTIONS
};

var DOMSizeField = function (_React$Component) {
    _inherits(DOMSizeField, _React$Component);

    function DOMSizeField(props) {
        _classCallCheck(this, DOMSizeField);

        var _this = _possibleConstructorReturn(this, (DOMSizeField.__proto__ || Object.getPrototypeOf(DOMSizeField)).call(this, props));

        _this._onUnitsChange = function (prop, units) {
            var value = 0;
            if (PX_REG.test(_this.props.value)) {
                value = _this.props.value.replace("px", "");
                _this.props.onUpdate(_this.props.name, value + units);
                return;
            }

            if (EM_REG.test(_this.props.value)) {
                value = _this.props.value.replace("em", "");
                _this.props.onUpdate(_this.props.name, value + units);
                return;
            }

            if (PERC_REG.test(_this.props.value)) {
                value = _this.props.value.replace("%", "");
                _this.props.onUpdate(_this.props.name, value + units);
                return;
            }
        };

        _this._onValueChange = function (prop, value) {
            var units = void 0;
            if (PX_REG.test(_this.props.value)) units = "px";
            if (EM_REG.test(_this.props.value)) units = "em";
            if (PERC_REG.test(_this.props.value)) units = "%";

            if (!units) units = "px";

            var update = void 0;
            if (value) update = value + units;

            _this.props.onUpdate(_this.props.name, update);
        };

        _this.state = {
            units: "px"
        };
        return _this;
    }

    _createClass(DOMSizeField, [{
        key: "render",
        value: function render() {
            console.log("DOMVALE", this.props.value);
            var value = this.props.value;
            var units = "px";
            if (isNaN(value)) {
                if (value.indexOf("px") >= 0) {
                    value = value.replace("px", "");
                } else if (value.indexOf("em") >= 0) {
                    value = value.replace("em", "");
                    units = "em";
                } else if (value.indexOf("%") >= 0) {
                    value = value.replace("%", "");
                    units = "%";
                }
            }

            return React.createElement(
                _Layout.Layout,
                null,
                React.createElement(
                    _Layout.Row,
                    null,
                    React.createElement(
                        _Layout.Cell,
                        null,
                        React.createElement(NumberField, {
                            name: "value",
                            onUpdate: this._onValueChange,
                            value: value })
                    ),
                    React.createElement(
                        _Layout.Cell,
                        { style: { paddingTop: 3, paddingLeft: 8 } },
                        React.createElement(_ButtonGroup2.default, {
                            name: "units",
                            value: units,
                            onUpdate: this._onUnitsChange,
                            config: UNITS_CONFIG })
                    )
                )
            );
        }
    }]);

    return DOMSizeField;
}(React.Component);

exports.default = DOMSizeField;


},{"../../components/fields/NumberField.react":45,"../Layout":21,"./ButtonGroup":24}],28:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ALLOWED_TYPES = ["image/png", "image/jpg", "image/jpeg", "application/pdf", "text/csv", "image/gif"];

var FileUploadField = function (_React$Component) {
    _inherits(FileUploadField, _React$Component);

    function FileUploadField(props) {
        _classCallCheck(this, FileUploadField);

        var _this = _possibleConstructorReturn(this, (FileUploadField.__proto__ || Object.getPrototypeOf(FileUploadField)).call(this, props));

        _this._onFileChange = function (event) {
            event.preventDefault();

            var reader = new FileReader();
            var file = event.target.files[0];

            reader.onloadend = function () {
                _this.setState({
                    file: file,
                    imagePreviewUrl: reader.result,
                    uploading: true
                });
                _this._upload(reader.result);
            };

            reader.readAsDataURL(file);
        };

        _this._upload = function (fileData) {
            // console.log(fileData);
            var bl = new ewars.Blocker(null, "Uploading file...");
            var formData = new FormData();
            formData.append("file", _this.refs.filer.files[0]);
            formData.append("fileName", _this.state.file.name);
            formData.append("fileType", _this.state.file.type);
            formData.append("accountId", window.user.account_id);
            var oReq = new XMLHttpRequest();

            oReq.onreadystatechange = function () {
                var DONE = 4;
                var OK = 200;

                if (oReq.readyState == DONE) {
                    bl.destroy();
                    if (oReq.status == OK) {
                        var resp = JSON.parse(oReq.responseText);
                        _this.props.onUpdate(_this.props.name, resp.fn);
                    } else {
                        if (oReq.status == 500) {}

                        if (oReq.status == 101) {}
                    }
                }
            };

            oReq.open("POST", "/upload", true);
            oReq.send(formData);
        };

        _this.state = {
            uploading: false,
            file: null,
            imagePreviewUrl: null
        };
        return _this;
    }

    _createClass(FileUploadField, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            this._uuid = ewars.utils.uuid();
            if (this.props.value) this.state.imagePreviewUrl = this.props.value;
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            if (this.props.value != nextProps.value) this.state.imagePreviewUrl = nextProps.value;
        }
    }, {
        key: "render",
        value: function render() {
            var imagePreviewUrl = this.state.imagePreviewUrl;

            var imagePreview = void 0;
            if (imagePreviewUrl) {
                imagePreview = React.createElement("img", { width: "100px", src: this.props.value });
            }
            return React.createElement(
                "div",
                { className: "form-field" },
                React.createElement("input", { onChange: this._onFileChange, ref: "filer", type: "file", name: "file", id: this._uuid }),
                React.createElement(
                    "label",
                    { className: "fileLabel", htmlFor: this._uuid },
                    "Choose file"
                ),
                imagePreview
            );
        }
    }]);

    return FileUploadField;
}(React.Component);

FileUploadField.defaultProps = {
    filePath: "/{account_id}/assets/",
    allowed_types: ALLOWED_TYPES,
    file_size: 2
};
exports.default = FileUploadField;


},{}],29:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("../Layout");

var _Toolbar = require("../Toolbar");

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _RangeUtils = require("../../utils/RangeUtils");

var _RangeUtils2 = _interopRequireDefault(_RangeUtils);

var _DayPicker = require("../../components/fields/date/DayPicker.react");

var _DayPicker2 = _interopRequireDefault(_DayPicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Moment = require("moment");

var NumberField = require("../../components/fields/NumberField.react");
var TextField = require("../../components/fields/TextInputField.react");

var QUICK_RANGES = [[["-30D", "Last 30 days"], ["-60D", "Last 60 days"], ["-6M", "Last 6 months"], ["-1Y", "Last 1 year"], ["-2Y", "Last 2 years"], ["-5Y", "Last 5 years"]], [["-1D", "Yesterday"], ["-2D", "Day before yesterday"], ["-7D", "This day last week"], ["PREV_WEEK", "Previous week"], ["PREV_MONTH", "Previous month"], ["PREV_YEAR", "Previous year"]], [["NOW", "Today"], ["CUR_WEEK", "This week"], ["CUR_MONTH", "This month"], ["CUR_YEAR", "This year"]]];

var QUICK_RANGES_TEMPLATE = [[["DD-30D", "Previous 30 days"], ["DD-60D", "Previous 60 days"], ["DD-6M", "Previous 6 months"], ["DD-1Y", "Previous 1 year"], ["DD-2Y", "Previous 2 years"], ["DD-5Y", "Previous 5 years"]], [["DD-1D", "Previous Day"], ["DD-2D", "Day before previous day"], ["DD-7D", "Same day previous week"], ["D_PREV_WEEK", "Previous week"], ["D_PREV_MONTH", "Previous month"], ["D_PREV_YEAR", "Previous year"]], [["D_DATE", "Document date"], ["D_CUR_WEEK", "Document week"], ["D_CUR_MONTH", "Document month"], ["D_CUR_YEAR", "Document year"]]];

var PRESETS = [["NOW", "Today"], ["END", "End date"], ["N_W_S", "This week start"], ["N_W_E", "This week end"], ["N_M_S", "This month start"], ["N_M_E", "This month end"], ["N_Y_S", "This year start"], ["N_Y_E", "This year end"]];

var PRESETS_TEMPLATE = [["D_DATE", "Document date"], ["NOW", "Today"], ["END", "End date"], ["D_W_S", "Document week start"], ["D_W_E", "Document week end"], ["D_M_S", "Document month start"], ["D_M_E", "Document month end"], ["D_Y_S", "Document year start"], ["D_Y_E", "Document year end"], ["N_W_S", "This week start"], ["N_W_E", "This week end"], ["N_M_S", "This month start"], ["N_M_E", "This month end"], ["N_Y_S", "This year start"], ["N_Y_E", "This year end"]];

var QuickRange = function (_React$Component) {
    _inherits(QuickRange, _React$Component);

    function QuickRange(props) {
        _classCallCheck(this, QuickRange);

        var _this = _possibleConstructorReturn(this, (QuickRange.__proto__ || Object.getPrototypeOf(QuickRange)).call(this, props));

        _this._onClick = function () {
            _this.props.onClick(_this.props.data);
        };

        return _this;
    }

    _createClass(QuickRange, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "iw-qk-range", onClick: this._onClick },
                this.props.data[1]
            );
        }
    }]);

    return QuickRange;
}(React.Component);

var Offset = function (_React$Component2) {
    _inherits(Offset, _React$Component2);

    function Offset(props) {
        _classCallCheck(this, Offset);

        var _this2 = _possibleConstructorReturn(this, (Offset.__proto__ || Object.getPrototypeOf(Offset)).call(this, props));

        _this2._onChange = function (prop, value) {
            _this2.props.onChange(value);
        };

        return _this2;
    }

    _createClass(Offset, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "dr-offset" },
                React.createElement(
                    "label",
                    { htmlFor: "" },
                    "Offset"
                ),
                React.createElement("div", { className: "clearer" }),
                React.createElement(TextField, {
                    name: "",
                    value: this.props.value,
                    onUpdate: this._onChange })
            );
        }
    }]);

    return Offset;
}(React.Component);

var DateSelect = function (_React$Component3) {
    _inherits(DateSelect, _React$Component3);

    function DateSelect(props) {
        _classCallCheck(this, DateSelect);

        var _this3 = _possibleConstructorReturn(this, (DateSelect.__proto__ || Object.getPrototypeOf(DateSelect)).call(this, props));

        _this3._handleBodyClick = function (evt) {
            if (_this3.refs.selector) {
                var area = _this3.refs.selector.getDOMNode ? _this3.refs.selector.getDOMNode() : _this3.refs.selector;

                if (!area.contains(evt.target)) {
                    _this3.setState({
                        shown: false
                    });
                }
            }
        };

        _this3._trigger = function () {
            _this3.setState({
                shown: !_this3.state.shown
            });
        };

        _this3._setMode = function (mode) {
            _this3.setState({ mode: mode });
        };

        _this3._onPickerChange = function (value) {
            _this3.props.onChange(_this3.props.prop, Moment.utc(value).format("YYYY-MM-DD"));
            _this3.setState({
                shown: false
            });
        };

        _this3._selectpreset = function (preset) {
            _this3.props.onChange(_this3.props.prop, "{" + preset[0] + "}");
        };

        _this3._onOffsetChange = function (value) {
            var newValue = _RangeUtils2.default.applyOffset(_this3.props.value, value);
            _this3.props.onChange(_this3.props.prop, newValue);
        };

        _this3.state = {
            shown: false,
            mode: "CUSTOM"
        };
        return _this3;
    }

    _createClass(DateSelect, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            window.__hack__.addEventListener("click", this._handleBodyClick);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._handleBodyClick);
        }
    }, {
        key: "render",
        value: function render() {
            var _this4 = this;

            var label = void 0;

            var pickerValue = void 0;
            if (_RangeUtils2.default.isDate(this.props.value)) {
                pickerValue = _RangeUtils2.default.getDate(this.props.value);
            }
            label = _RangeUtils2.default.formatDateSpec(this.props.value);

            var code = void 0;
            if (_RangeUtils2.default.hasCode(this.props.value)) {
                code = _RangeUtils2.default.getCode(this.props.value);
            }

            var offset = _RangeUtils2.default.getOffset(this.props.value);

            return React.createElement(
                "div",
                { className: "ew-select", onClick: this._handleBodyClick, ref: "selector" },
                React.createElement(
                    "div",
                    { className: "handle", onClick: this._trigger },
                    React.createElement(
                        _Layout.Row,
                        null,
                        React.createElement(
                            _Layout.Cell,
                            null,
                            label
                        ),
                        React.createElement(
                            _Layout.Cell,
                            { width: "31" },
                            React.createElement("i", { className: "fa fa-calendar" })
                        )
                    )
                ),
                this.state.shown ? React.createElement(
                    "div",
                    { className: "ew-select-data", style: { minWidth: 500 } },
                    React.createElement(
                        _Layout.Layout,
                        null,
                        React.createElement(
                            _Layout.Row,
                            null,
                            React.createElement(
                                _Layout.Cell,
                                { width: 300, borderRight: true },
                                React.createElement(_DayPicker2.default, {
                                    value: pickerValue,
                                    name: "picker",
                                    onChange: this._onPickerChange })
                            ),
                            React.createElement(
                                _Layout.Cell,
                                { width: 200 },
                                React.createElement(
                                    _Layout.Layout,
                                    null,
                                    React.createElement(
                                        _Layout.Row,
                                        null,
                                        React.createElement(
                                            _Layout.Cell,
                                            { borderBottom: true },
                                            React.createElement(
                                                "div",
                                                { className: "ide-panel ide-panel-absolute ide-scroll" },
                                                React.createElement(
                                                    "div",
                                                    { className: "drpresets-list" },
                                                    PRESETS.map(function (preset) {
                                                        return React.createElement(PresetItem, {
                                                            code: code,
                                                            onClick: _this4._selectpreset,
                                                            data: preset });
                                                    })
                                                )
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        _Layout.Row,
                                        { height: 66 },
                                        React.createElement(
                                            _Layout.Cell,
                                            { style: { padding: 8 } },
                                            React.createElement(Offset, {
                                                value: offset,
                                                onChange: this._onOffsetChange })
                                        )
                                    )
                                )
                            )
                        )
                    )
                ) : null
            );
        }
    }]);

    return DateSelect;
}(React.Component);

DateSelect.defaultProps = {
    showEndDate: false,
    showStartDate: false
};

var DatePop = function (_React$Component4) {
    _inherits(DatePop, _React$Component4);

    function DatePop(props) {
        _classCallCheck(this, DatePop);

        return _possibleConstructorReturn(this, (DatePop.__proto__ || Object.getPrototypeOf(DatePop)).call(this, props));
    }

    _createClass(DatePop, [{
        key: "render",
        value: function render() {
            return React.createElement("div", { className: "dummy" });
        }
    }]);

    return DatePop;
}(React.Component);

var PresetItem = function (_React$Component5) {
    _inherits(PresetItem, _React$Component5);

    function PresetItem(props) {
        _classCallCheck(this, PresetItem);

        var _this6 = _possibleConstructorReturn(this, (PresetItem.__proto__ || Object.getPrototypeOf(PresetItem)).call(this, props));

        _this6._onClick = function () {
            _this6.props.onClick(_this6.props.data);
        };

        return _this6;
    }

    _createClass(PresetItem, [{
        key: "render",
        value: function render() {
            var className = "dr-preset";
            if (this.props.code == this.props.data[0]) className += " active";

            return React.createElement(
                "div",
                { className: className, onClick: this._onClick },
                React.createElement(
                    _Layout.Row,
                    null,
                    React.createElement(
                        _Layout.Cell,
                        { width: 20 },
                        React.createElement("div", { className: "indicator" })
                    ),
                    React.createElement(
                        _Layout.Cell,
                        null,
                        this.props.data[1]
                    )
                )
            );
        }
    }]);

    return PresetItem;
}(React.Component);

var RangeSelector = function (_React$Component6) {
    _inherits(RangeSelector, _React$Component6);

    function RangeSelector(props) {
        _classCallCheck(this, RangeSelector);

        var _this7 = _possibleConstructorReturn(this, (RangeSelector.__proto__ || Object.getPrototypeOf(RangeSelector)).call(this, props));

        _this7._onQkSelect = function (qk) {
            var tmp = _RangeUtils2.default.processPreset(qk[0]);
            _this7.props.onUpdate(_this7.props.name, [tmp[0], tmp[1]]);
        };

        _this7._onPropChange = function (prop, value) {
            var data = _this7.props.value || [];

            if (prop == "start") data[0] = value;
            if (prop == "end") data[1] = value;

            _this7.props.onUpdate(_this7.props.name, data);
        };

        _this7.state = {};
        return _this7;
    }

    _createClass(RangeSelector, [{
        key: "render",
        value: function render() {
            var _this8 = this;

            console.log(this.props);

            var startDate = this.props.value[0] || Moment.utc().format("YYYY-MM-DD");
            var endDate = this.props.value[1] || Moment.utc().format("YYYY-MM-DD");
            var RANGES = QUICK_RANGES;
            if (ewars.TEMPLATE_MODE == "TEMPLATE") RANGES = QUICK_RANGES_TEMPLATE;

            return React.createElement(
                "div",
                { className: "iw-range" },
                React.createElement(
                    _Layout.Layout,
                    null,
                    React.createElement(
                        _Layout.Row,
                        null,
                        React.createElement(
                            _Layout.Cell,
                            { addClass: "iw-range-values", borderRight: true, style: { paddingRight: 10 } },
                            React.createElement(
                                "label",
                                { htmlFor: "" },
                                "From"
                            ),
                            React.createElement(DateSelect, {
                                prop: "start",
                                value: startDate,
                                showEndDate: true,
                                mode: this.props.mode,
                                onChange: this._onPropChange }),
                            React.createElement("div", { style: { height: 10 } }),
                            React.createElement(
                                "label",
                                { htmlFor: "" },
                                "To:"
                            ),
                            React.createElement(DateSelect, {
                                prop: "end",
                                value: endDate,
                                mode: this.props.mode,
                                showEndDate: true,
                                onChange: this._onPropChange })
                        ),
                        React.createElement(
                            _Layout.Cell,
                            { addClass: "iw-quick-ranges" },
                            React.createElement(
                                _Layout.Layout,
                                null,
                                React.createElement(_Toolbar2.default, { label: "Quick Ranges" }),
                                React.createElement(
                                    _Layout.Row,
                                    { style: { padding: 8 } },
                                    RANGES.map(function (item) {
                                        return React.createElement(
                                            _Layout.Cell,
                                            null,
                                            item.map(function (qk) {
                                                return React.createElement(QuickRange, { data: qk, onClick: _this8._onQkSelect });
                                            })
                                        );
                                    })
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return RangeSelector;
}(React.Component);

RangeSelector.defaultProps = {
    mode: "DEFAULT", // or TEMPLATE,
    value: []
};
exports.default = RangeSelector;


},{"../../components/fields/NumberField.react":45,"../../components/fields/TextInputField.react":51,"../../components/fields/date/DayPicker.react":55,"../../utils/RangeUtils":81,"../Layout":21,"../Toolbar":23,"moment":86}],30:[function(require,module,exports){
"use strict";

var btnClass = {
    red: "btn-red",
    green: "btn-green",
    amber: "btn-amber"
};

var ButtonComponent = React.createClass({
    displayName: "ButtonComponent",

    _id: null,

    propTypes: {
        label: React.PropTypes.string,
        icon: React.PropTypes.string,
        type: React.PropTypes.string
    },

    getDefaultProps: function getDefaultProps() {
        return {
            enabled: true,
            processing: false,
            active: false
        };
    },

    getInitialState: function getInitialState() {
        return {
            enabled: true,
            processing: false,
            processingMessage: "Processing..."
        };
    },

    /**
     * Public method to set button to processing mode
     * @param message
     */
    setProcessing: function setProcessing(message) {
        this.state.processing = true;
        if (message != null && message != undefined) this.state.processingMessage = message;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method to set button to not processing mode
     */
    unsetProcessing: function unsetProcessing() {
        this.state.processing = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method used to disable the button
     */
    disable: function disable() {
        this.state.enabled = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Pubilc method used to enable button
     */
    enable: function enable() {
        this.state.enabled = true;
        if (this.isMounted()) this.forceUpdate();
    },

    shouldComponentUpdate: function shouldComponentUpdate() {
        return true;
    },

    _onClick: function _onClick(e) {
        if (!this.props.enabled) return;
        if (!this.state.enabled) return;
        if (this.state.processing) return;
        if (this.props.processing) return;

        e.preventDefault();
        e.stopPropagation();
        this.props.onClick(this.props.data ? this.props.data : null);
    },

    render: function render() {
        var colour;
        if (this.props.colour) colour = btnClass[this.props.colour];

        var className = "";
        if (colour) className += " " + colour;
        var iconClass;
        if (this.props.icon) iconClass = "fa " + this.props.icon;

        if (this.props.active) className += " active";

        var title;
        if (this.props.title) title = this.props.title;

        if (!this.state.enabled || !this.props.enabled) className += " disabled";

        var label;
        if (this.props.label) label = ewars.formatters.I18N_FORMATTER(this.props.label);

        if (this.state.processing || this.props.processing) {
            iconClass = "fa fa-circle-o-notch fa-spin";
            label = this.state.processingMessage || "Processing...";
        }

        var view;
        if (iconClass && label) {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                React.createElement("i", { className: iconClass }),
                "\xA0",
                label
            );
        } else if (iconClass && !label) {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                React.createElement("i", { className: iconClass })
            );
        } else {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                label
            );
        }

        return view;
    }
});

module.exports = ButtonComponent;


},{}],31:[function(require,module,exports){
"use strict";

var _DateField = require("./fields/DateField.react");

var _DateField2 = _interopRequireDefault(_DateField);

var _AgeField = require("./fields/AgeField");

var _AgeField2 = _interopRequireDefault(_AgeField);

var _RangeSelector = require("../cmp/fields/RangeSelector");

var _RangeSelector2 = _interopRequireDefault(_RangeSelector);

var _ColourThresholds = require("../cmp/fields/ColourThresholds");

var _ColourThresholds2 = _interopRequireDefault(_ColourThresholds);

var _ButtonGroup = require("../cmp/fields/ButtonGroup");

var _ButtonGroup2 = _interopRequireDefault(_ButtonGroup);

var _DOMSizeField = require("../cmp/fields/DOMSizeField");

var _DOMSizeField2 = _interopRequireDefault(_DOMSizeField);

var _Colour = require("../cmp/fields/Colour");

var _Colour2 = _interopRequireDefault(_Colour);

var _FileUploadField = require("../cmp/fields/FileUploadField");

var _FileUploadField2 = _interopRequireDefault(_FileUploadField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FormField = require("./FormField.react");
var FormUtils = require("../utils/FormUtils");

var MatrixField = require("./fields/MatrixField.react");
var SelectField = require('./fields/SelectField.react');
var TextAreaField = require('./fields/TextAreaField.react');
var TextInputField = require('./fields/TextInputField.react');
var WYSIWYGField = require('./fields/WYSIWYG.react');
var DisplayField = require("./fields/DisplayField.react");
var ConditionsField = require("./fields/ConditionsField.react");
var RowField = require("./fields/RowField.react");
var TopoJSONField = require("./fields/TopoJSONField.react");
var LocationInputField = require("./fields/LocationInputField");
var LocationSelectField = require("./fields/LocationSelectField.react");
var LanguageStringField = require("./fields/LanguageStringInput.react");
var NumberField = require("./fields/NumberField.react");
var SlugField = require("./fields/SlugField.react");
var SwitchField = require("./fields/SwitchField.react");
var IndicatorSelector = require("../components/indicator/IndicatorSelectorComponent.react");
var PointField = require("./fields/point/PointSelectionField.react");
var MapGeometryField = require("./fields/MapGeometryField.react");
var SelectFieldOptionsField = require("./fields/system/SelectFieldOptionsField.react");
var HeaderField = require("./fields/HeaderField.react");
var AssignmentLocationField = require("./fields/report_fields/AssignmentLocationField.react");
var IndicatorDefinition = require("./fields/system/IndicatorDefinition.react");
// Only used in widget editors
var ComplexSourcesDefinition = require("./widget_editors/_shared/ComplexSourcesManager.react");

var FIELDS = {
    select: SelectField,
    text: TextInputField,
    string: TextInputField,
    textarea: TextAreaField,
    wysiwyg: WYSIWYGField,
    WYSIWYG: WYSIWYGField,
    date: _DateField2.default,
    email: TextInputField,
    matrix: MatrixField,
    number: NumberField,
    point: PointField,
    display: DisplayField,
    conditions: ConditionsField,
    row: RowField,
    topojson: TopoJSONField,
    location: LocationSelectField,
    language_string: LanguageStringField,
    slug: SlugField,
    switch: SwitchField,
    indicator: IndicatorSelector,
    geometry: MapGeometryField,
    select_options: SelectFieldOptionsField,
    header: HeaderField,
    assignment_location: AssignmentLocationField,
    indicator_definition: IndicatorDefinition,
    _complex_sources: ComplexSourcesDefinition,
    assignment: AssignmentLocationField,
    age: _AgeField2.default,
    date_range: _RangeSelector2.default,
    colour_thresholds: _ColourThresholds2.default,
    button_group: _ButtonGroup2.default,
    dom_size: _DOMSizeField2.default,
    color: _Colour2.default,
    file: _FileUploadField2.default
};

var NON_FIELDS = ["display", "grid"];

/**
 * Comparator used for sorting the fields in the definition by
 * their order property
 * @param a
 * @param b
 * @returns {number}
 * @private
 */
function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields) {
    var fields = _.map(sourceFields, function (token, name) {
        var field = JSON.parse(JSON.stringify(token));
        field.name = name;

        if (field.fields) {
            var children = _getFieldsAsArray(field.fields);
            field.fields = children;
        }

        return field;
    });

    return _.sortBy(fields, function (item) {
        return parseInt(item.order);
    });
}

function _recursiveSetValue(definition, data, path, errors) {
    for (var i in definition) {
        var field = definition[i];
        if (path) {
            definition[i].path = path + "." + i;
        } else {
            definition[i].path = i;
        }

        if (field.fields) {
            // This field doesn't have a value, it has children which have values
            field.fields = _recursiveSetValue(field.fields, data[i] || {}, field.path, errors);
        }

        var key = i;
        if (field.nameOverride) key = field.nameOverride;

        if (key.indexOf(".") >= 0) {
            field.value = ewars.getKeyPath(key, data);
        } else {
            field.value = data[key] || field.defaultValue || "";
        }

        // Check for errors for this field
        // Only if it's a certain type of field
        if (NON_FIELDS.indexOf(field.type) < 0 && errors) {
            var fieldErrors = ewars.getKeyPath(definition[i].path, errors);
            if (fieldErrors) definition[i].error = fieldErrors;
        }
    }
    return definition;
}

/**
 * Check if a value isn't set, used for required fields
 * @param value
 * @returns {boolean}
 */
function isNull(value) {
    if (value == "") return true;
    if (value == undefined) return true;
    if (value == "null") return true;
    if (value == "None") return true;
    if (value == null) return true;
    if (!value) return true;
    return false;
}

/**
 * Form class - Used to render forms throughout the application*
 */
var Form = React.createClass({
    displayName: "Form",


    _fields: null,

    /**
     * Retrieve the initial state of the form*
     * @returns {{fields: {}, dirty: boolean, tabs: null}}
     */

    getInitialState: function getInitialState() {
        return {
            fields: {},
            dirty: false,
            tabs: null,
            errors: {},
            disabled: false
        };
    },

    componentWillMount: function componentWillMount() {},

    enable: function enable() {
        this.state.disabled = false;
        if (this.isMounted()) this.forceUpdate();
    },

    disable: function disable() {
        this.state.disabled = true;
        if (this.isMounted()) this.forceUpdate();
    },

    getDefaultProps: function getDefaultProps() {
        return {
            readOnly: true,
            light: true
        };
    },

    /**
     * Handles any changes to fields in the form and bubbles them up to the parent component*
     * @param property
     * @param value
     * @param path FQP for where the proeprty should be updated
     * @param * Any additional data that the field type has bubbled up (location, indicator, etc...)
     * @private
     * @param path
     */
    _handleChange: function _handleChange(property, value, path, additionalData) {
        this.props.updateAction(this.props.data, property, value, path, additionalData);

        // TODO: Need to handle when the form is returned to it;s initial state by the users
        // track history of changes in client before a save call
        this.setState({
            dirty: true
        });
    },

    validateField: function validateField(field_name, value) {
        var def = FormUtils.field(this.props.definition, field_name);
        this.state.errors[field_name] = [];

        if (def) {
            // Check if required
            if (def.required) {
                if (isNull(value)) {
                    this.state.errors[field_name].push(ewars.I18N(def.label) + " is a required field");
                }
            }

            if (def.type == "number") {
                if (def.min) {
                    if (value < def.min) {
                        this.state.errors[field_name].push("Can not be below " + def.min);
                    }
                }

                if (def.max) {
                    if (value > def.max) {
                        this.state.errors[field_name].push("Can not be greate than " + def.max);
                    }
                }
            }
        }
    },

    /**
     * Public functions called via ref to check if a forms data is valid
     */
    isValid: function isValid() {
        return true;
    },

    /**
     * Processes and creates any grids defined for the form*
     * @param field
     * @returns {XML}
     * @private
     */
    _processGrid: function _processGrid(field) {
        var grid;

        var columns = [];
        for (var idx in field.fields) {
            var ff = field.fields[idx];
            var key = _.uniqueId("COLUMN_");
            columns.push(React.createElement(GridColumn, {
                key: key,
                name: idx,
                label: ff.label }));
        }

        var data = null;
        if (this.props.data[field.name]) data = this.props.data[field.name];

        grid = React.createElement(
            "div",
            { className: "form-grid" },
            React.createElement(
                GridComponent,
                {
                    editable: true,
                    data: data },
                columns
            )
        );

        return grid;
    },

    _getRuleResult: function _getRuleResult(rule, data) {
        var result;

        var sourceValue = ewars.getKeyPath(rule[0], data);
        if (sourceValue == null || sourceValue == undefined) return false;

        switch (rule[1]) {
            case "eq":
                result = sourceValue == rule[2];
                break;
            case "ne":
                result = sourceValue != rule[2];
                break;
            case "gt":
                result = sourceValue > rule[2];
                break;
            case "lt":
                result = sourceValue < rule[2];
                break;
            default:
                result = false;
                break;
        }

        return result;
    },

    _checkFieldVisible: function _checkFieldVisible(field, data) {
        var result = false;

        if (!field.conditions) return true;
        if (!field.conditions.rules) return true;

        if (field.conditions.application == "any") {
            // Only one of the rules has to pass

            for (var conIdx in field.conditions.rules) {
                var rule = field.conditions.rules[conIdx];

                var tmpResult = this._getRuleResult(rule, data);
                if (result != true && tmpResult == true) result = true;
            }
        } else {
            var ruleCount = field.conditions.rules.length;
            var rulePassCount = 0;

            for (var ruleIdx in field.conditions.rules) {
                var rule = field.conditions.rules[ruleIdx];
                var ruleResult = this._getRuleResult(rule, data);
                if (ruleResult) rulePassCount++;
            }

            if (ruleCount == rulePassCount) result = true;
        }
        return result;
    },

    /**
     * Renders the form into the parent container*
     * @returns {XML}
     */
    render: function render() {
        var tabs = [];

        var readOnly = true;
        if (this.props.readOnly == false) readOnly = false;
        if (this.state.disabled == true) readOnly = true;

        var cancelText = "Close";
        if (this.state.dirty == true) {
            cancelText = "Cancel";
        }

        var definition;
        var defCopy = JSON.parse(JSON.stringify(this.props.definition));
        if (this.props.data) definition = _recursiveSetValue(defCopy, this.props.data, null, this.props.errors);

        var nodes = [];

        // Sort and set up the fields
        this._fields = _getFieldsAsArray(definition);

        // Instantiate defaults
        _.each(this._fields, function (field) {
            if (field.type != "tab") {
                if (FIELDS[field.type]) {
                    var FieldControl = FIELDS[field.type];
                    var options = field.options ? field.options : null;

                    if (field.type == "grid") {
                        nodes.push(this._processGrid(field));
                    } else {

                        // Handle default definition
                        if (field.value == null || field.value == undefined) {
                            if (field.defaultValue != null && field.defaultValue != undefined) {
                                field.value = field.defaultValue;
                            }
                        }

                        var visible = true;
                        if (field.conditions) {
                            visible = this._checkFieldVisible(field, this.props.data);
                        }

                        var fields;
                        if (this.props.conditionalFieldOptions) {
                            fields = this.props.conditionalFieldOptions;
                        }

                        if (visible) {
                            var children;

                            if (field.fields && field.type != "grid") {
                                children = [];

                                _.each(field.fields, function (subField, fieldKey) {
                                    var childOptions = field.options ? field.options : null;
                                    var SubFieldControl = FIELDS[field.type];

                                    var wrapperKey = "WRAPPER_" + subField.type + "_" + fieldKey;
                                    var controlKey = "CONTROL_" + subField.type + "_" + fieldKey;

                                    children.push(React.createElement(
                                        FormField,
                                        {
                                            key: wrapperKey,
                                            field: field,
                                            errors: this.props.errors,
                                            readOnly: readOnly },
                                        React.createElement(SubFieldControl, {
                                            readOnly: readOnly,
                                            fieldOptions: fields,
                                            form: this.props.definition,
                                            map: FIELDS,
                                            errors: this.props.errors,
                                            name: fieldKey,
                                            definition: definition,
                                            value: field.value,
                                            options: childOptions,
                                            Form: Form,
                                            config: field,
                                            key: controlKey,
                                            onUpdate: this._handleChange })
                                    ));
                                }, this);
                            }

                            var wrapperKey = "WRAPPER_" + field.type + "_" + field.name;
                            var controlKey = "CONTROL_" + field.type + "_" + field.name;

                            nodes.push(React.createElement(
                                FormField,
                                {
                                    key: wrapperKey,
                                    field: field,
                                    errors: this.props.errors,
                                    readOnly: readOnly },
                                React.createElement(
                                    FieldControl,
                                    {
                                        readOnly: readOnly,
                                        key: controlKey,
                                        fieldOptions: fields,
                                        form: this.props.definition,
                                        map: FIELDS,
                                        errors: this.props.errors,
                                        name: field.name,
                                        value: field.value,
                                        definition: definition,
                                        options: options,
                                        config: field,
                                        Form: Form,
                                        onUpdate: this._handleChange },
                                    children
                                )
                            ));
                        }
                    }
                }
            }
        }, this);

        var formClass = "form";
        if (this.props.readOnly) formClass += " read-only";
        if (this.props.light) formClass += " light";

        return React.createElement(
            "div",
            { className: formClass },
            nodes
        );
    }
});

module.exports = Form;


},{"../cmp/fields/ButtonGroup":24,"../cmp/fields/Colour":25,"../cmp/fields/ColourThresholds":26,"../cmp/fields/DOMSizeField":27,"../cmp/fields/FileUploadField":28,"../cmp/fields/RangeSelector":29,"../components/indicator/IndicatorSelectorComponent.react":66,"../utils/FormUtils":80,"./FormField.react":32,"./fields/AgeField":35,"./fields/ConditionsField.react":36,"./fields/DateField.react":37,"./fields/DisplayField.react":38,"./fields/HeaderField.react":39,"./fields/LanguageStringInput.react":40,"./fields/LocationInputField":41,"./fields/LocationSelectField.react":42,"./fields/MapGeometryField.react":43,"./fields/MatrixField.react":44,"./fields/NumberField.react":45,"./fields/RowField.react":46,"./fields/SelectField.react":47,"./fields/SlugField.react":48,"./fields/SwitchField.react":49,"./fields/TextAreaField.react":50,"./fields/TextInputField.react":51,"./fields/TopoJSONField.react":52,"./fields/WYSIWYG.react":53,"./fields/point/PointSelectionField.react":62,"./fields/report_fields/AssignmentLocationField.react":63,"./fields/system/IndicatorDefinition.react":64,"./fields/system/SelectFieldOptionsField.react":65,"./widget_editors/_shared/ComplexSourcesManager.react":69}],32:[function(require,module,exports){
"use strict";

var FormField = React.createClass({
    displayName: "FormField",

    getInitialState: function getInitialState() {
        return {
            content: "",
            helpShown: false,
            linkagesShown: false,
            errors: [],
            hasErrors: false,
            error: false
        };
    },

    _showHelp: function _showHelp(e) {
        e.preventDefault();
        var title = ewars.I18N(this.props.field.label);
        ewars.prompt("fa-question-circle", "Help: " + title, ewars.I18N(this.props.field.help));
    },

    _helpClick: function _helpClick() {
        if (this.props.field.help) {
            ewars.prompt("fa-question-circle", ewars.I18N(this.props.field.label), ewars.I18N(this.props.field.help), function () {});
        }
    },

    render: function render() {
        var hasHelp = this.props.field.help ? true : false;
        var hasLinkages = this.props.field.linkages ? true : false;

        var fieldClass = "form-field";
        if (this.props.field.type == "row") fieldClass += " form-row";
        if (this.props.field.type == "switch") fieldClass += " switch";
        if (this.props.field.type == "header") fieldClass += " header";

        var labelClass = "label";
        if (this.props.field.type == "row") labelClass += " row-label";
        if (this.props.field.type == "matrix") labelClass += " matrix-label";

        var features = {
            help: true,
            error: true,
            label: true,
            fieldControls: true
        };

        if (this.props.readOnly || ["row", "header", "matrix"].indexOf(this.props.field.type) >= 0) {
            features.fieldControls = false;
        }

        var label = false;
        if (this.props.field.label !== false) {
            label = ewars.I18N(this.props.field.label);
            if (this.props.field.required) label += " *";
            if (this.props.field.help) labelClass += " has-help";
        }

        var children = this.props.children;
        if (React.Children.count(this.props.children) == 1) {
            children = [this.props.children];
        }

        var showError = true;
        if (["matrix", "row", "header"].indexOf(this.props.field.type) >= 0) showError = false;

        var errors = void 0;
        if (this.props.errors) {
            if (this.props.errors[this.props.field.path]) {
                errors = this.props.errors[this.props.field.path];
            }
        }

        var wrapper_style = {};
        if (label == false) wrapper_style.paddingLeft = 15;

        return React.createElement(
            "div",
            { className: fieldClass },
            label ? React.createElement(
                "div",
                { className: labelClass },
                React.createElement(
                    "label",
                    { htmlFor: "", onClick: this._helpClick },
                    label
                )
            ) : null,
            React.createElement(
                "div",
                { className: "field-wrapper", style: wrapper_style },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        children
                    )
                )
            ),
            features.fieldControls ? React.createElement(
                "div",
                { className: "field-controls" },
                React.createElement(
                    "table",
                    { width: "100%" },
                    React.createElement(
                        "tbody",
                        null,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement(
                                "td",
                                null,
                                errors ? React.createElement(
                                    "div",
                                    { className: "field-error" },
                                    React.createElement(
                                        "div",
                                        { className: "icon" },
                                        React.createElement("i", { className: "fa fa-exclamation-circle" })
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "error-message" },
                                        errors
                                    )
                                ) : React.createElement(
                                    "span",
                                    null,
                                    "\xA0"
                                )
                            )
                        )
                    )
                )
            ) : null
        );
    }

});

module.exports = FormField;


},{}],33:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var chroma = require("chroma-js");

// need to extend leaflet with TopoJSON support
if (window.L) {
    L.TopoJSON = L.GeoJSON.extend({
        addData: function addData(jsonData) {
            if (jsonData.type === "Topology") {
                for (key in jsonData.objects) {
                    geojson = topojson.feature(jsonData, jsonData.objects[key]);
                    L.GeoJSON.prototype.addData.call(this, geojson);
                }
            } else {
                L.GeoJSON.prototype.addData.call(this, jsonData);
            }
        }
    });

    var _redIcon = L.icon({
        iconUrl: 'static/icons/map-markers/marker-red.png',

        iconSize: [21, 34], // size of the icon
        shadowSize: [26, 26], // size of the shadow
        iconAnchor: [10.5, 34], // point of the icon which will correspond to marker's location
        shadowAnchor: [10.5, 24], // the same for the shadow
        popupAnchor: [-3, -9] // point from which the popup should open relative to the iconAnchor
    });
}

var _colorScale = chroma.scale(['#D5E3FF', '#003171']).domain([0, 1]);

var MapComponent = React.createClass({
    displayName: "MapComponent",

    _map: null,
    _markers: [],
    _marker: null,

    propTypes: {
        layer: React.PropTypes.object,
        showTiles: React.PropTypes.bool,
        height: React.PropTypes.number,
        lat: React.PropTypes.number,
        lng: React.PropTypes.number,
        pointEditMode: React.PropTypes.bool,
        defaultLat: React.PropTypes.number,
        defaultLng: React.PropTypes.number
    },

    getDefaultProps: function getDefaultProps() {
        return {
            showTiles: true,
            height: 300,
            pointEditMode: false,
            defaultLat: 51.505,
            defaultLng: -0.09
        };
    },

    getInitialState: function getInitialState() {
        return {};
    },

    shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
        if (this.props.height != nextProps.height) {
            return true;
        }

        if (nextProps.data) {
            if (nextProps.data.length > 0) {
                this._renderMarkers(nextProps.data);
            }
        }

        if (nextProps.lat != this.props.lat || nextProps.lng != this.props.lng) {
            if (this._marker) {
                if (nextProps.lat != "-" && nextProps.lat != "" && nextProps.lng != "-" && nextProps.lng != "") {
                    this._map.setView([nextProps.lat, nextProps.lng]);
                    this._marker.setLatLng([nextProps.lat, nextProps.lng]);
                }
            }
        }

        if (nextProps.defaultLat && nextProps.defaultLng) {
            this._map.setView([nextProps.defaultLat, nextProps.defaultLng]);
        }
        return false;
    },

    componentDidMount: function componentDidMount() {
        if (!this._map) this.renderEditor();
    },

    componentDidUpdate: function componentDidUpdate() {
        if (!this._map) this.renderEditor();
        if (this._map) this._map.invalidateSize();
        this._map.fitBounds(this._topoLayer.getBounds());
    },

    componentWillUnmount: function componentWillUnmount() {
        if (this._map) {
            this._map.off("click", this.onMapClick);
            this._map = null;
        }
    },

    onChange: function onChange(name, value) {
        this.props.onChange(name, value);
    },

    render: function render() {
        if (this.props.height) {
            var style = {
                height: this.props.height + "px"
            };
        }

        return React.createElement("div", { className: "map", style: style });
    },

    _renderMarkers: function _renderMarkers(data) {
        for (var i in this._markers) {
            var item = this._markers[i];

            this._map.removeLayer(item);
        }

        this._markers = [];

        var redIcon = L.icon({
            iconUrl: 'static/icons/map-markers/marker-red.png',

            iconSize: [21, 34], // size of the icon
            shadowSize: [26, 26], // size of the shadow
            iconAnchor: [10.5, 34], // point of the icon which will correspond to marker's location
            shadowAnchor: [10.5, 24], // the same for the shadow
            popupAnchor: [-3, -9] // point from which the popup should open relative to the iconAnchor
        });

        for (i in data) {
            var item = data[i];

            var lat = ewars.getKeyPath(this.props.latProp, item);
            var lng = ewars.getKeyPath(this.props.lngProp, item);

            if (lat && lng && lat != "NULL") {
                var marker = L.marker([lat, lng], {
                    icon: redIcon,
                    zIndexOffset: 10000
                });

                this._markers.push(marker);
                this._map.addLayer(marker);
            }
        }
    },

    _onPointChange: function _onPointChange(lat, lng) {
        this.props.onPointChange(lat, lng);
    },

    renderEditor: function renderEditor() {
        this._map = L.map(this.getDOMNode(), {
            minZoom: 0,
            maxZoom: 20,
            attributionControl: false,
            scrollWheelZoom: this.props.pointEditMode ? false : true
        });

        if (this.props.lat && this.props.lng) {
            this._map.setView([this.props.lat, this.props.lng], 1);
        } else {
            this._map.setView([this.props.defaultLat, this.props.defaultLng], 1);
        }

        var tiles = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>' });
        tiles.addTo(this._map);

        if (this.props.pointEditMode && this.props.defaultLat) {
            this._marker = L.marker([this.props.defaultLat, this.props.defaultLng], {
                icon: _redIcon,
                draggable: true,
                riseOnHover: true
            });

            this._map.addLayer(this._marker);
            this._map.setView([this.props.defaultLat, this.props.defaultLng], 10);
        }

        if (this.props.pointEditMode && this.props.lat) {
            this._marker = L.marker([this.props.lat, this.props.lng], {
                icon: _redIcon,
                draggable: true,
                riseOnHover: true
            });

            this._map.addLayer(this._marker);
            this._map.setView([this.props.lat, this.props.lng], 10);
        }

        if (this.props.data) {

            for (i in this.state.data) {
                var item = this.state.data[i];

                var lat = ewars.getKeyPath(this.props.latProp, item);
                var lng = ewars.getKeyPath(this.props.lngProp, item);

                if (lat && lng) {
                    var marker = L.marker([lat, lng], {
                        icon: _redIcon,
                        zIndexOffset: 10000
                    });

                    this._markers.push(marker);
                    this._map.addLayer(marker);
                }
            }
        }

        // Point editing features
        if (this.props.pointEditMode) {
            var self = this;
            this._map.on("click", function (e) {
                if (self._marker) self._marker.setLatLng([e.latlng.lat, e.latlng.lng]);
                self._onPointChange(e.latlng.lat, e.latlng.lng);
            });

            if (this._marker) {
                this._marker.on("dragend", function (e) {
                    self._onPointChange(e.target.latlng.lat, e.target.latlng.lng);
                });
            }
        }

        if (this.props.layer) {
            this._topoLayer = new L.geoJson();

            this._topoLayer.addData(this.props.layer);
            this._topoLayer.setStyle({
                fillColor: _colorScale(Math.random()).hex(),
                fillOpacity: 0.1,
                color: "#555",
                weight: 1,
                opacity: 0.1
            });
            this._topoLayer.addTo(this._map);
            this._map.fitBounds(this._topoLayer.getBounds());
        }
    }

});

module.exports = MapComponent;


},{"chroma-js":83}],34:[function(require,module,exports){
"use strict";

var Spinner = React.createClass({
    displayName: "Spinner",

    render: function render() {
        return React.createElement(
            "div",
            { className: "spinner-wrap" },
            React.createElement(
                "div",
                { className: "spinner" },
                React.createElement("i", { className: "fa fa-spin fa-circle-o-notch" })
            )
        );
    }
});

module.exports = Spinner;


},{}],35:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Layout = require("../../cmp/Layout");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumberField = require("./NumberField.react");
var SelectField = require("./SelectField.react");

var FIELD_CONFIG = {
    options: [["DAY", "Day(s)"], ["WEEK", "Week(s)"], ["MONTH", "Month(s)"], ["YEAR", "Year(s)"]]
};

var LABEL = {
    "DAY": "Day(s)",
    "WEEK": "Week(s)",
    "MONTH": "Month(s)",
    "YEAR": "Year(s)"
};

var AgeField = function (_React$Component) {
    _inherits(AgeField, _React$Component);

    function AgeField(props) {
        _classCallCheck(this, AgeField);

        var _this = _possibleConstructorReturn(this, (AgeField.__proto__ || Object.getPrototypeOf(AgeField)).call(this, props));

        _this._onChange = function (prop, value) {
            var out = ewars.copy(_this.props.value || { units: null, value: null });

            out[prop] = value;
            _this.props.onUpdate(_this.props.name, out, _this.props.config.path || null);
        };

        return _this;
    }

    _createClass(AgeField, [{
        key: "render",
        value: function render() {
            var age_value = void 0,
                units = void 0;

            age_value = this.props.value.value || null;
            units = this.props.value.units || null;

            if (this.props.readOnly) {
                return React.createElement(
                    "div",
                    { className: "display-field" },
                    age_value,
                    " ",
                    LABEL[units]
                );
            }

            return React.createElement(
                _Layout.Layout,
                null,
                React.createElement(
                    _Layout.Row,
                    null,
                    React.createElement(
                        _Layout.Cell,
                        null,
                        React.createElement(NumberField, {
                            name: "value",
                            onUpdate: this._onChange,
                            value: age_value })
                    ),
                    React.createElement(
                        _Layout.Cell,
                        null,
                        React.createElement(SelectField, {
                            name: "units",
                            value: units,
                            onUpdate: this._onChange,
                            config: FIELD_CONFIG })
                    )
                )
            );
        }
    }]);

    return AgeField;
}(React.Component);

AgeField.defaultProps = {
    readOnly: true
};
exports.default = AgeField;


},{"../../cmp/Layout":21,"./NumberField.react":45,"./SelectField.react":47}],36:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var ConditionsRow = require("./conditions/ConditionRow.react");
var Button = require("../ButtonComponent.react");

var ConditionsField = React.createClass({
    displayName: "ConditionsField",


    getInitialState: function getInitialState() {
        return {
            data: {
                rules: [["none", "eq", null]],
                application: "ALL"
            }
        };
    },

    _addRule: function _addRule() {
        var newConditions = this.state.data.rules ? this.state.data.rules : [];
        newConditions.push([null, "eq", null]);
        this.state.data.rules = newConditions;
        this.props.onUpdate(this.props.name, this.state.data);
    },

    _removeRule: function _removeRule(index) {
        var rules = ewars.copy(this.state.data);
        rules.rules.splice(index, 1);
        this.props.onUpdate(this.props.name, rules);
    },

    _defaultRow: ["none", "eq", "none"],

    componentWillMount: function componentWillMount() {
        if (!this.props.value || this.props.value.length >= 0) {
            this.state.data = {
                application: "ALL",
                rules: [["none", "eq", false]]
            };
        } else {
            if (this.props.config.value != undefined) this.state.data = this.props.config.value;
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        this.state.data = nextProps.value;
    },

    _rowUpdate: function _rowUpdate(index, data) {
        this.state.data.rules[index] = data;
        this.props.onUpdate(this.props.name, this.state.data);
    },

    _onApplicationChange: function _onApplicationChange(e) {
        this.state.data.application = e.target.value;
        this.props.onUpdate(this.props.name, this.state.data);
    },

    render: function render() {
        var rules = [];

        if (this.state.data.rules) {
            rules = _.map(this.state.data.rules, function (rule, index) {
                return React.createElement(ConditionsRow, {
                    index: index,
                    data: rule,
                    map: this.props.map,
                    fieldOptions: this.props.fieldOptions,
                    onUpdate: this._rowUpdate,
                    onRemove: this._removeRule,
                    onAdd: this._addCondition });
            }, this);
        }

        return React.createElement(
            "div",
            { className: "conditions-wrapper" },
            React.createElement(
                "div",
                { className: "conditions-rule-application" },
                React.createElement(
                    "table",
                    { width: "100%" },
                    React.createElement(
                        "tbody",
                        null,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement(
                                "td",
                                { className: "passer" },
                                "Pass When"
                            ),
                            React.createElement(
                                "td",
                                null,
                                React.createElement(
                                    "select",
                                    { value: this.state.data.application, onChange: this._onApplicationChange },
                                    React.createElement(
                                        "option",
                                        { value: "all" },
                                        "All rules are met"
                                    ),
                                    React.createElement(
                                        "option",
                                        { value: "any" },
                                        "Any rules are met"
                                    )
                                )
                            )
                        )
                    )
                ),
                rules,
                React.createElement(
                    "div",
                    { className: "btn-group pull-right" },
                    React.createElement(Button, {
                        label: "Add Rule",
                        colour: "green",
                        onClick: this._addRule })
                )
            )
        );
    }
});

module.exports = ConditionsField;


},{"../ButtonComponent.react":30,"./conditions/ConditionRow.react":54}],37:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _DayPicker = require("./date/DayPicker.react");

var _DayPicker2 = _interopRequireDefault(_DayPicker);

var _WeekPicker = require("./date/WeekPicker.react");

var _WeekPicker2 = _interopRequireDefault(_WeekPicker);

var _MonthPicker = require("./date/MonthPicker.react");

var _MonthPicker2 = _interopRequireDefault(_MonthPicker);

var _YearPicker = require("./date/YearPicker.react");

var _YearPicker2 = _interopRequireDefault(_YearPicker);

var _DisplayPicker = require("./date/DisplayPicker.react");

var _DisplayPicker2 = _interopRequireDefault(_DisplayPicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Moment = require("moment");

var PICKERS = {
    NONE: _DayPicker2.default,
    DAY: _DayPicker2.default,
    WEEK: _WeekPicker2.default,
    MONTH: _MonthPicker2.default,
    YEAR: _YearPicker2.default,
    DISPLAY: _DisplayPicker2.default
};

var Handle = function (_React$Component) {
    _inherits(Handle, _React$Component);

    function Handle(props) {
        _classCallCheck(this, Handle);

        return _possibleConstructorReturn(this, (Handle.__proto__ || Object.getPrototypeOf(Handle)).call(this, props));
    }

    _createClass(Handle, [{
        key: "render",
        value: function render() {
            var name = "No date selected";

            if (this.props.value) {
                name = ewars.DATE(this.props.value, this.props.format);
            }

            return React.createElement(
                "div",
                { className: "handle", onClick: this.props.onClick },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        name
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col icon", style: { maxWidth: 31 } },
                        React.createElement("i", { className: "fa fa-calendar" })
                    )
                )
            );
        }
    }]);

    return Handle;
}(React.Component);

var DateField = function (_React$Component2) {
    _inherits(DateField, _React$Component2);

    function DateField(props) {
        _classCallCheck(this, DateField);

        var _this2 = _possibleConstructorReturn(this, (DateField.__proto__ || Object.getPrototypeOf(DateField)).call(this, props));

        _this2._handleBodyClick = function (evt) {
            if (_this2.refs.selector) {
                var area = _this2.refs.selector;

                if (!area.contains(evt.target)) {
                    _this2.state.showOptions = false;
                    _this2.forceUpdate();
                }
            }
        };

        _this2._onValueChange = function (newValue) {
            var name = _this2.props.name;
            if (_this2.props.config.nameOverride) name = _this2.props.config.nameOverride;
            _this2.state.showOptions = false;
            _this2.props.onUpdate(name, newValue.clone().format("YYYY-MM-DD"), _this2.props.config.path || null);
        };

        _this2._toggle = function () {
            _this2.setState({
                showOptions: !_this2.state.showOptions
            });
        };

        _this2.state = {
            showOptions: false,
            value: _this2.props.value || Moment()
        };
        return _this2;
    }

    _createClass(DateField, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._handleBodyClick);
            if (!this.props.value) {
                this.state.value = Date.now();
            } else {
                this.state.value = this.props.value;
            }
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            if (!nextProps.value) {
                this.state.value = Date.now();
            } else {
                this.state.value = nextProps.value;
            }
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._handleBodyClick);
        }
    }, {
        key: "render",
        value: function render() {
            var view = void 0;
            var date_type = this.props.config.date_type || this.props.date_type;
            var ViewCmp = PICKERS[date_type || "DAY"];

            if (this.props.readOnly) {
                return React.createElement("input", { type: "text", value: ewars.DATE(this.state.value, date_type) });
            }

            view = React.createElement(ViewCmp, {
                onChange: this._onValueChange,
                format: date_type,
                block_future: this.props.config.block_future,
                value: this.state.value });

            if (date_type == "YEAR") return view;

            return React.createElement(
                "div",
                { ref: "selector", className: "ew-select", onClick: this._handleBodyClick },
                React.createElement(Handle, {
                    onClick: this._toggle,
                    value: this.props.value,
                    format: date_type }),
                this.state.showOptions ? React.createElement(
                    "div",
                    { className: "ew-select-data", style: { maxWidth: 300, right: 0, left: "initial" } },
                    view
                ) : null
            );
        }
    }]);

    return DateField;
}(React.Component);

DateField.defaultProps = {
    value: null,
    dispValue: null,
    name: null,
    config: {
        block_future: false,
        date_type: "DAY"
    }
};
exports.default = DateField;


},{"./date/DayPicker.react":55,"./date/DisplayPicker.react":56,"./date/MonthPicker.react":57,"./date/WeekPicker.react":58,"./date/YearPicker.react":59,"moment":86}],38:[function(require,module,exports){
"use strict";

var DisplayField = React.createClass({
    displayName: "DisplayField",

    getInitialState: function getInitialState() {
        return {};
    },

    render: function render() {
        return React.createElement(
            "div",
            { className: "display-field" },
            this.props.config.defaultValue || this.props.value
        );
    }
});

module.exports = DisplayField;


},{}],39:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var HeaderField = React.createClass({
    displayName: "HeaderField",

    getInitialState: function getInitialState() {
        return {};
    },

    render: function render() {
        var className = "form-header";

        if (this.props.config.headerStyle) {
            className += " " + this.props.config.headerStyle;
        } else {
            className += " style-title";
        }

        var label = ewars.formatters.I18N_FORMATTER(this.props.config.label);

        return React.createElement(
            "div",
            { className: className },
            label
        );
    }
});

module.exports = HeaderField;


},{}],40:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var _languages = ["en", "fr", "ar"];

var LanguageNode = React.createClass({
    displayName: "LanguageNode",

    render: function render() {
        var className = "language";
        if (this.props.active) className += " active";

        return React.createElement(
            "div",
            { className: className },
            this.props.value
        );
    }
});

var LanguageStringField = React.createClass({
    displayName: "LanguageStringField",

    _defaultLang: "en",

    getInitialState: function getInitialState() {
        return {
            curLang: "en",
            showTree: false
        };
    },

    componentWillMount: function componentWillMount() {
        // A little clean up, fix labels which are not dicts to dicts
        this.state.value = this.props.value;
        if (!this.props.value) {
            this.state.value = { en: "" };
        }

        if (typeof this.props.value == "string") {
            this.state.value = { en: this.props.value };
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        this.state.value = nextProps.value;
        if (!nextProps.value) {
            this.state.value = { en: "" };
        }

        if (typeof nextProps.value == "string") {
            this.state.value = { en: nextProps.value };
        }
    },

    onChange: function onChange(e) {
        var copy = this.state.value;
        copy[this.state.curLang] = e.target.value;
        this.props.onUpdate(this.props.name, copy);
    },

    _handleLanguageChange: function _handleLanguageChange(e) {
        e.preventDefault();
        e.stopPropagation();
        var language = e.target.dataset.lang;
        this.setState({
            showTree: false,
            curLang: language
        });
    },

    _hideTree: function _hideTree() {
        this.setState({
            showTree: false
        });
    },

    _toggleTree: function _toggleTree(e) {
        this.setState({
            showTree: this.state.showTree == true ? false : true
        });
    },

    render: function render() {
        var curValue = this.state.value[this.state.curLang];

        var languages = _languages.map(function (language) {
            var active = false;
            if (this.state.curLang == language) active = true;
            return React.createElement(LanguageNode, {
                key: language,
                value: language,
                active: active });
        }.bind(this));

        var nodes = [];
        for (var i in _languages) {
            var language = _languages[i];
            nodes.push(React.createElement(
                "li",
                null,
                React.createElement(
                    "div",
                    { className: "label", onClick: this._handleLanguageChange, "data-lang": language },
                    language
                )
            ));
        }

        var disabled = false;
        if (this.props.readOnly) disabled = true;

        return React.createElement(
            "div",
            { className: "location-selector" },
            React.createElement(
                "div",
                { className: "handle" },
                React.createElement(
                    "div",
                    { className: "current no-pad" },
                    React.createElement("input", { type: "text", disabled: disabled, value: curValue, name: "lang_string", onChange: this.onChange })
                ),
                React.createElement(
                    "div",
                    { className: "grip", onClick: this._toggleTree },
                    React.createElement(
                        "div",
                        { className: "languages" },
                        languages
                    ),
                    React.createElement("i", { className: "fa fa-language" })
                )
            ),
            this.state.showTree ? React.createElement(
                "div",
                { className: "location-tree-wrapper" },
                React.createElement(
                    "div",
                    { className: "location-tree" },
                    React.createElement(
                        "h4",
                        null,
                        "Available Languages"
                    ),
                    React.createElement(
                        "ul",
                        { className: "languages-options" },
                        nodes
                    )
                )
            ) : null
        );
    }
});

module.exports = LanguageStringField;


},{}],41:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var MapComponent = require("../MapComponent.react");
var Button = require("../ButtonComponent.react");

var LocationInputField = React.createClass({
    displayName: "LocationInputField",

    propTypes: {
        value: React.PropTypes.object,
        name: React.PropTypes.string
    },

    getInitialState: function getInitialState() {
        return {
            lat: 51.505,
            lng: -0.09
        };
    },

    componentDidMount: function componentDidMount() {
        this._getUsersPosition();
    },

    getDefaultProps: function getDefaultProps() {
        return {
            value: {
                lat: 12.0000,
                lng: 8.33333
            }
        };
    },

    _getUsersPosition: function _getUsersPosition() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this._handleGeoLocate);
        }
    },

    _handleGeoLocate: function _handleGeoLocate(data) {
        //this._handleChange(data.coords.latitude, data.coords.longitude);
    },

    _handleChange: function _handleChange(lat, lng) {
        if (this.props.readOnly) return;
        this.props.onUpdate(this.props.name, { lat: lat, lng: lng }, this.props.config.path);
    },

    _handleSingleChange: function _handleSingleChange(e) {
        var copy = JSON.parse(JSON.stringify({ lat: this.props.value.lat, lng: this.props.value.lng }));
        copy[e.target.name] = e.target.value;
        this.props.onUpdate(this.props.name, copy, this.props.config.path);
    },

    render: function render() {

        var pointEditMode = true;
        if (this.props.readOnly) pointEditMode = false;

        return React.createElement(
            "div",
            { className: "location-selector-field" },
            React.createElement(
                "div",
                { className: "map-wrapper" },
                React.createElement(MapComponent, {
                    lat: this.props.value.lat,
                    lng: this.props.value.lng,
                    defaultZoom: 10,
                    pointEditMode: pointEditMode,
                    onPointChange: this._handleChange,
                    showTiles: true })
            ),
            !this.props.readOnly ? React.createElement(
                "div",
                { className: "controls-wrapper" },
                React.createElement(
                    "div",
                    { className: "grid" },
                    React.createElement(
                        "div",
                        { className: "col-6" },
                        React.createElement(
                            "span",
                            null,
                            "Latitude"
                        ),
                        React.createElement("input", { type: "text", name: "lat", onChange: this._handleSingleChange, value: this.props.value.lat })
                    ),
                    React.createElement(
                        "div",
                        { className: "col-6" },
                        React.createElement(
                            "span",
                            null,
                            "Longitude"
                        ),
                        React.createElement("input", { type: "text", name: "lng", onChange: this._handleSingleChange, value: this.props.value.lng })
                    )
                )
            ) : null
        );
    }
});

module.exports = LocationInputField;


},{"../ButtonComponent.react":30,"../MapComponent.react":33}],42:[function(require,module,exports){
"use strict";

var _Layout = require("../../cmp/Layout");

var TextField = require('./TextInputField.react');
var Spinner = require("../Spinner.react");
var Button = require("../ButtonComponent.react");

var LOCATION_SELECT = ["uuid", "name", "parent_id", "@children", "site_type_id"];

var _backupLocations = null;

var LocationTreeNode = React.createClass({
    displayName: "LocationTreeNode",

    _isLoaded: false,

    getDefaultProps: function getDefaultProps() {
        return {
            hideInactive: true,
            lineageRoot: null,
            selectionTypeId: null
        };
    },

    getInitialState: function getInitialState() {
        return {
            showLocations: false
        };
    },

    _onSelect: function _onSelect() {
        this.props.onSelect(this.props.data);
    },

    _onClick: function _onClick() {
        if (this.props.selectionTypeId && this.props.data.site_type_id == this.props.selectionTypeId) {
            this._onSelect();
            return;
        }

        if (this.props.data.children <= 0) return;

        this.state.showLocations = this.state.showLocations ? false : true;
        if (this.isMounted()) this.forceUpdate();

        if (!this._isLoaded && this.state.showLocations) {
            var filters = {
                parent_id: { eq: this.props.data.uuid }
            };

            if (this.props.hideInactive) filters.status = { eq: "ACTIVE" };
            if (!this.props.hideInactive) filters.status = { neq: "DELETED" };

            ewars.tx("com.ewars.query", ["location", LOCATION_SELECT, filters, { "name.en": "ASC" }, null, null, ["location_type:site_type_id:id"]]).then(function (resp) {
                this.state.locations = resp;
                this._isLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    render: function render() {
        var className = "item locale-select";
        if (this.props.value == this.props.data.uuid) className += " active";

        var name = ewars.I18N(this.props.data.name);
        name += " (" + ewars.I18N(this.props.data.location_type.name) + ")";

        var children;
        if (this.state.showLocations && this._isLoaded) {
            children = _.map(this.state.locations, function (location) {
                return React.createElement(LocationTreeNode, {
                    onSelect: this.props.onSelect,
                    selectionTypeId: this.props.selectionTypeId,
                    lineageRoot: this.props.lineageRoot,
                    hideInactive: this.props.hideInactive,
                    key: location.uuid,
                    data: location });
            }, this);
        } else {
            children = React.createElement(Spinner, null);
        }

        var iconClass = "fa";
        if (this.state.showLocations) iconClass += " fa-caret-down";
        if (!this.state.showLocations) iconClass += " fa-caret-right";
        if (!this._isLoaded && this.state.showLocations) iconClass = "fa fa-spin fa-cog";

        if (this.props.data.children <= 0) iconClass = "fa fa-map-marker";
        if (this.props.selectionTypeId && this.props.data.site_type_id == this.props.selectionTypeId) iconClass = "fa fa-map-marker";

        var childStyle = { display: "none" };
        if (this.state.showLocations) childStyle.display = "block";

        var handleClass = "locale-tree-node";
        if (this.props.data.site_type_id == this.props.selectionTypeId) handleClass += " green";

        var showSelect = false;
        if (!this.props.selectionTypeId || this.props.selectionTypeId == this.props.data.site_type_id) showSelect = true;
        if (showSelect) handleClass += " has-button";

        return React.createElement(
            "div",
            { className: className },
            React.createElement(
                "div",
                { className: handleClass, onClick: this._onClick },
                React.createElement(
                    "div",
                    { className: "expander" },
                    React.createElement("i", { className: iconClass })
                ),
                React.createElement(
                    "div",
                    { className: "labeler" },
                    name
                ),
                showSelect ? React.createElement(
                    "div",
                    { className: "ew-select-btn" },
                    React.createElement(Button, {
                        icon: "fa-caret-right",
                        onClick: this._onSelect })
                ) : null
            ),
            React.createElement(
                "div",
                { className: "locale-tree-node-children", style: childStyle },
                children
            )
        );
    }
});

var Handle = React.createClass({
    displayName: "Handle",

    render: function render() {
        var name = "No Selection";

        if (this.props.value.uuid) {
            name = ewars.I18N(this.props.value.name);
            name += " (" + ewars.I18N(this.props.value.location_type.name) + ")";
        }
        return React.createElement(
            "div",
            { className: "handle", onClick: this.props.onClick },
            React.createElement(
                _Layout.Layout,
                null,
                React.createElement(
                    _Layout.Row,
                    null,
                    React.createElement(
                        _Layout.Cell,
                        { addClass: "handleText" },
                        name
                    ),
                    React.createElement(
                        _Layout.Cell,
                        { width: 31, addClass: "handleIcon" },
                        React.createElement("i", { className: "fa fa-caret-down" })
                    )
                )
            )
        );
    }
});

var LocationSelectField = React.createClass({
    displayName: "LocationSelectField",

    _isLoaded: false,
    _initialLoad: false,
    _isSearch: false,

    propTypes: {
        _onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        emptyText: React.PropTypes.string
    },

    getDefaultProps: function getDefaultProps() {
        return {
            config: {
                allowCheck: false,
                location_type: null,
                hideInactive: false,
                parentId: null,
                selectionTypeId: null,
                lineageRoot: null
            },
            emptyText: "No location selected"
        };
    },

    getInitialState: function getInitialState() {
        return {
            showLocations: false,
            location: {
                uuid: null
            },
            search: ""
        };
    },

    _init: function _init(props) {
        if (props.value) {
            ewars.tx("com.ewars.resource", ["location", props.value, ["uuid", "name", "site_type_id"], ["location_type:site_type_id:id"]]).then(function (resp) {
                this.state.location = resp;
                this._initialLoad = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    componentWillMount: function componentWillMount() {
        if (this.props.value && this.props.value != this.state.location.uuid) {
            this._init(this.props);
        } else {
            this._initialLoad = true;
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        if (this.props.value != nextProps.value) {
            if (!nextProps.value) {
                this.state.location = {};
            } else {
                this._initialLoad = false;
                if (nextProps.value) this._init(nextProps);
            }
        } else {
            this._initialized = true;
        }
    },

    componentDidMount: function componentDidMount() {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function componentWillUnmount() {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
    },

    componentDidUpdate: function componentDidUpdate() {
        if (this.state.showLocations) {
            this.refs.localeInput.focus();
        }
    },

    handleBodyClick: function handleBodyClick(evt) {
        if (this.refs.selector) {
            var area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showLocations = false;
                this.forceUpdate();
            }
        }
    },

    handleClick: function handleClick(e) {
        e.stopPropagation();
    },

    _onChange: function _onChange(location) {
        var name = this.props.config.nameOverride || this.props.name;
        var path = this.props.config.path || null;

        this.state.showLocations = false;

        if (this._isSearch) {
            this._isSearch = false;
            this.state.search = "";
            this.state.locations = JSON.parse(_backupLocations);
        }

        this.state.location = location;
        this.props.onUpdate(name, location.uuid, path);
    },

    _toggle: function _toggle(e) {
        e.stopPropagation();
        this.state.showLocations = this.state.showLocations ? false : true;

        this.forceUpdate();

        if (this.state.showLocations && !this._isLoaded) {
            // We need to load the options
            var resource = "location";
            var select = ["uuid", "name", "@children", "site_type_id"];
            var filters = {};
            if (this.props.config.hideInactive) filters.status = { eq: "ACTIVE" };
            if (!this.props.config.hideInactive) filters.status = { neq: "DELETED" };

            if (window.user.role != "SUPER_ADMIN") {
                if (window.user.role == "REGIONAL_ADMIN") {
                    filters.uuid = { eq: window.user.location_id };
                } else {
                    filters.uuid = { eq: window.user.clid };
                }
            } else {
                filters.parent_id = { eq: "NULL" };
            }

            ewars.tx("com.ewars.query", [resource, select, filters, null, null, null, ["location_type:site_type_id:id"]]).then(function (resp) {
                this.state.locations = resp;
                this._isLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    _onSearchChange: function _onSearchChange(e) {
        this.state.search = e.target.value;

        if (!this._isSearch) {
            _backupLocations = JSON.stringify(this.state.locations);
            this.state.locations = [];
            this._isSearch = true;
        }

        var query = {
            "name.en": { like: this.state.search }
        };
        if (this.props.config.parentId) {
            query.lineage = { has: this.props.config.parentId };
        } else {
            if (window.user.role != "SUPER_ADMIN") {
                if (window.user.role == "ACCOUNT_ADMIN") {
                    query.lineage = { under: window.user.clid };
                } else if (window.user.role == "REGIONAL_ADMIN") {
                    query.lienage = { under: window.user.location_id };
                } else {
                    query.lineage = { under: window.user.clid };
                }
            }
        }

        if (this.state.search.length >= 3) {
            ewars.tx("com.ewars.query", ["location", LOCATION_SELECT, query, null, null, null, ["location_type:site_type_id:id"]]).then(function (resp) {
                this.state.locations = resp;
                this.forceUpdate();
            }.bind(this));
        } else {
            this.forceUpdate();
        }
    },

    _clearSearch: function _clearSearch() {
        this.state.search = "";
        this._isSearch = false;
        this.state.locations = JSON.parse(_backupLocations);
        this.forceUpdate();
    },

    render: function render() {
        if (!this._initialLoad) {
            return React.createElement(
                "div",
                { className: "ew-select" },
                React.createElement(
                    "div",
                    { className: "handle" },
                    React.createElement("i", { className: "fa fa-spin fa-circle-o-notch" })
                )
            );
        }

        if (this.props.readOnly) {
            var name = ewars.I18N(this.state.location.name);
            name += " (" + ewars.I18N(this.state.location.name) + ")";
            return React.createElement("input", { type: "text", disabled: true, value: name });
        }

        var parent_id;
        if (this.props.config) {
            if (this.props.config.parent_id) parent_id = this.props.config.parent_id;
        }

        var rootNodes;
        if (!this._isLoaded && this.state.showLocations) rootNodes = React.createElement(Spinner, null);

        if (this._isLoaded && this.state.showLocations) {
            rootNodes = _.map(this.state.locations, function (location) {
                return React.createElement(LocationTreeNode, {
                    onSelect: this._onChange,
                    selectionTypeId: this.props.config.selectionTypeId,
                    lineageRoot: this.props.config.lineageRoot,
                    hideInactive: this.props.config.hideInactive,
                    key: location.uuid,
                    data: location });
            }, this);
        }

        return React.createElement(
            "div",
            { ref: "selector", className: "ew-select locale-selector", onClick: this.handleBodyClick },
            React.createElement(Handle, {
                onClick: this._toggle,
                readOnly: this.props.readOnly,
                value: this.state.location }),
            this.state.showLocations ? React.createElement(
                "div",
                { className: "ew-select-data" },
                React.createElement(
                    "div",
                    { className: "locale-search" },
                    React.createElement(
                        "div",
                        { className: "input" },
                        React.createElement("input", { ref: "localeInput", type: "text", placeholder: "Search locations...",
                            value: this.state.search,
                            onChange: this._onSearchChange }),
                        this.state.search.length > 0 ? React.createElement(
                            "div",
                            { className: "search-button", onClick: this._clearSearch },
                            React.createElement("i", {
                                className: "fa fa-times-circle" })
                        ) : null
                    )
                ),
                rootNodes
            ) : null
        );
    }
});

module.exports = LocationSelectField;


},{"../../cmp/Layout":21,"../ButtonComponent.react":30,"../Spinner.react":34,"./TextInputField.react":51}],43:[function(require,module,exports){
'use strict';

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var chroma = require("chroma-js");

var _colorScale = chroma.scale(['wheat', 'maroon']);

// need to extend leaflet with TopoJSON support
if (window.L) {
    L.TopoJSON = L.GeoJSON.extend({
        addData: function addData(jsonData) {
            if (jsonData.type === "Topology") {
                for (key in jsonData.objects) {
                    geojson = topojson.feature(jsonData, jsonData.objects[key]);
                    L.GeoJSON.prototype.addData.call(this, geojson);
                }
            } else {
                L.GeoJSON.prototype.addData.call(this, jsonData);
            }
        }
    });

    var _redIcon = L.icon({
        iconUrl: 'static/icons/map-markers/marker-red.png',

        iconSize: [21, 34], // size of the icon
        shadowSize: [26, 26], // size of the shadow
        iconAnchor: [10.5, 34], // point of the icon which will correspond to marker's location
        shadowAnchor: [10.5, 24], // the same for the shadow
        popupAnchor: [-3, -9] // point from which the popup should open relative to the iconAnchor
    });
}

var MapGeometryField = React.createClass({
    displayName: 'MapGeometryField',

    _map: null,
    _markers: [],
    _marker: null,

    getDefaultProps: function getDefaultProps() {
        return {
            showTiles: true,
            height: 300,
            pointEditMode: false,
            defaultLat: 51.505,
            defaultLng: -0.09
        };
    },

    getInitialState: function getInitialState() {
        return {};
    },

    componentDidMount: function componentDidMount() {
        if (!this._map) this.renderEditor();
    },

    componentWillReceiveProps: function componentWillReceiveProps() {
        if (!this._map) this.renderEditor();

        if (this._multiPGroup) {
            this._map.removeLayer(this._multiPGroup);
            this._multiPGroup = null;
            this._map.removeControl(this._drawControl);
            this._drawControl = null;
        }

        if (this.props.value) {
            // Need to determin geometry type
            var value = JSON.parse(this.props.value);
            if (value.type == "MultiPolygon") {

                this._featureGroup = new L.featureGroup().addTo(this._map);
                _.each(value.coordinates[0][0], function (coordSet) {
                    coordSet.reverse();
                });

                this._multiPGroup = new L.Polygon(value.coordinates[0][0]).addTo(this._featureGroup);

                this._drawControl = new L.Control.Draw({
                    position: 'topright',
                    edit: {
                        featureGroup: this._featureGroup
                    }
                });
                this._map.addControl(this._drawControl);
            } else if (value.type == "Point") {} else {
                if (this._drawControl) this._map.removeControl(this._drawControl);
                this._drawControl = null;

                if (!this._marker) {
                    this._marker = L.marker(value.coordinates.reverse(), { draggable: true }).addTo(this._map);

                    this._map.on("dblclick", function (e) {
                        this._handleMapLocationChange(e.latlng.lat, e.latlng.lng);
                    }.bind(this));

                    this._marker.on('dragend', function (e) {
                        var loc = this._marker.getLatLng();
                        this._handleMapLocationChange(loc.lat, loc.lng);
                    }.bind(this));
                }

                //this._marker = L.marker(value.coordinates).addTo(this._map);
                this._marker.setLatLng(value.coordinates.reverse());
                this._map.panTo(new L.LatLng(value.coordinates[0], value.coordinates[1]));
            }
        }
    },

    componentWillUnmount: function componentWillUnmount() {
        if (this._map) {
            this._map.off("click", this.onMapClick);
            this._map = null;
        }
    },

    onChange: function onChange(name, value) {
        this.props.onChange(name, value);
    },

    render: function render() {
        if (this.props.height) {
            var style = {
                height: this.props.height + "px"
            };
        }

        return React.createElement(
            'div',
            { className: 'administration-field' },
            React.createElement('div', { className: 'map', style: style, ref: 'geometryField' }),
            !this.props.readOnly ? React.createElement(
                'div',
                { className: 'edit-controls' },
                React.createElement(
                    'p',
                    null,
                    'Upload a new geometry for this administration, accpeted files are GeoJSON, TopoJSON and Shapefile (.shp)'
                ),
                React.createElement('input', { type: 'file' })
            ) : null
        );
    },

    _createGeoJSON: function _createGeoJSON(lat, lng) {
        return JSON.stringify({ "type": "Point", "coordinates": [lng, lat] });
    },

    _handleMapLocationChange: function _handleMapLocationChange(lat, lng) {
        this.props.onUpdate(this.props.name, this._createGeoJSON(lat, lng));
    },

    renderEditor: function renderEditor() {
        this._map = L.map(this.refs.geometryField.getDOMNode(), {
            minZoom: 0,
            maxZoom: 20,
            attributionControl: false,
            doubleClickZoom: false,
            scrollWheelZoom: this.props.pointEditMode ? false : true
        });

        if (this.props.lat && this.props.lng) {
            this._map.setView([this.props.lat, this.props.lng], 1);
        } else {
            this._map.setView([this.props.defaultLat, this.props.defaultLng], 1);
        }

        L.mapbox.accessToken = 'pk.eyJ1IjoiamR1cmVuIiwiYSI6IkQ5YXQ2UFUifQ.acHWe_O-ybfg7SN2qrAPHg';
        var tiles = L.mapbox.tileLayer('jduren.lomc08mg').addTo(this._map);

        if (this.props.value) {
            // Need to determine geometry type
            var value = JSON.parse(this.props.value);

            if (value.type == "MultiPolygon") {

                this._featureGroup = new L.featureGroup().addTo(this._map);
                _.each(value.coordinates[0][0], function (coordSet) {
                    coordSet.reverse();
                });

                this._multiPGroup = new L.Polygon(value.coordinates[0][0]).addTo(this._featureGroup);

                this._drawControl = new L.Control.Draw({
                    position: 'topright',
                    edit: {
                        featureGroup: this._featureGroup
                    }
                });
                this._map.addControl(this._drawControl);

                //this._map.setView(this._multiPGroup.getBounds());
            } else {
                this._marker = L.marker(value.coordinates.reverse(), { draggable: true }).addTo(this._map);
                this._map.setView(new L.LatLng(value.coordinates[0], value.coordinates[1]), 8);

                this._map.on("dblclick", function (e) {
                    this._handleMapLocationChange(e.latlng.lat, e.latlng.lng);
                }.bind(this));

                this._marker.on('dragend', function (e) {
                    var loc = this._marker.getLatLng();
                    this._handleMapLocationChange(loc.lat, loc.lng);
                }.bind(this));
            }
        }
    }
});

module.exports = MapGeometryField;


},{"chroma-js":83}],44:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var MatrixRow = require("./matrix/MatrixRow.react");
var MatrixCell = require("./matrix/MatrixCell.react");

/**
 * Retrieve the form definition fields as a flat array
 * @param sourceFields
 * @returns {Array.<T>}
 * @private
 */
function _getFieldsAsArray(sourceFields) {
    var fields = [];
    for (var token in sourceFields) {
        var field = sourceFields[token];
        if (_.isObject(field)) {
            field.name = token;
            fields.push(field);
        }
    }

    return _.sortBy(fields, function (field) {
        return parseInt(field.order);
    }, this);
}

var MatrixField = React.createClass({
    displayName: "MatrixField",

    getInitialState: function getInitialState() {
        return {};
    },

    _handleCellChange: function _handleCellChange(prop, value, path) {
        this.props.onUpdate(prop, value, path);
    },

    _processheaders: function _processheaders() {
        if (this.props.config.headers) {
            var headers = [];
            for (var i in this.props.config.headers) {
                headers.push(React.createElement(
                    "th",
                    { className: "matrix-header-cell" },
                    "\xA0"
                ));
            }

            return headers;
        }
    },

    _processDefinition: function _processDefinition() {
        var rows = [];

        var arrayed = _.map(this.props.config.fields, function (row, key) {
            row.name = key;
            return row;
        }, this);

        var sorted = _.sortBy(arrayed, function (item) {
            return parseInt(item.order);
        }, this);

        _.each(sorted, function (rowDefinition) {
            var row;

            var cells = [];
            console.log(rowDefinition.fields);
            var cellDefinition = _getFieldsAsArray(rowDefinition.fields);

            cellDefinition = _.sortBy(cellDefinition, function (cell) {
                return parseInt(cell.order);
            }, this);

            for (var cellToken in cellDefinition) {
                var cell = cellDefinition[cellToken];

                if (this.props.map[cell.type]) {
                    var FieldControl = this.props.map[cell.type];
                    cells.push(React.createElement(
                        MatrixCell,
                        {
                            key: "CELL_" + cell.uuid,
                            errors: this.props.errors,
                            field: cell },
                        React.createElement(FieldControl, {
                            name: cell.name,
                            value: cell.value,
                            key: "FIELD_" + cell.uuid,
                            options: cell.options ? cell.options : null,
                            config: cell,
                            readOnly: this.props.readOnly,
                            onUpdate: this._handleCellChange })
                    ));
                } else {
                    var Field = this.props.map._typeMap.none;
                    cells.push(React.createElement(
                        MatrixCell,
                        null,
                        React.createElement(Field, null)
                    ));
                }
            }

            rows.push(React.createElement(
                MatrixRow,
                {
                    key: rowDefinition.uuid,
                    data: rowDefinition },
                cells
            ));
        }, this);

        return rows;
    },

    _processRow: function _processRow(rowDefinition) {
        return row;
    },

    render: function render() {
        var content = this._processDefinition();
        return React.createElement(
            "div",
            { className: "matrix" },
            React.createElement(
                "table",
                { className: "matrix-table" },
                React.createElement(
                    "tbody",
                    null,
                    content
                )
            )
        );
    }
});

module.exports = MatrixField;


},{"./matrix/MatrixCell.react":60,"./matrix/MatrixRow.react":61}],45:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var reg = /^\d+$/;

var NumberField = React.createClass({
    displayName: "NumberField",

    propTypes: {
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        config: React.PropTypes.object
    },

    getDefaultProps: function getDefaultProps() {
        return {
            placeholder: "Enter number",
            name: null,
            readOnly: false,
            path: null,
            config: {
                allow_negative: true,
                decimal_allowed: true
            }
        };
    },

    _options: {},

    componentDidMount: function componentDidMount() {
        if (this.props.focus) {
            this.refs.inputField.focus();
        }
    },

    getInitialState: function getInitialState() {
        return {};
    },

    _onChange: function _onChange(e) {
        var value = e.target.value;

        if (!this.props.config.decimal_allowed) value = value.split(".")[0];
        if (!this.props.config.allow_negative) value = value.replace("-", "");

        value = value.replace(/[^\d.-]/g, '');

        var name = this.props.config.nameOverride || this.props.name;
        var path = this.props.config.path || this.props.name;

        //value = value.replace(/^0+/, '');
        this.props.onUpdate(name, value, path);
    },

    render: function render() {
        var value = this.props.value || "";

        return React.createElement("input", { ref: "inputField",
            disabled: this.props.readOnly,
            className: "form-control number-field",
            style: { paddingRight: '5px' },
            onChange: this._onChange,
            type: "text",
            value: value,
            placeholder: this.props.placeholder || 0,
            name: this.props.name });
    }

});

module.exports = NumberField;


},{}],46:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Button = require("../ButtonComponent.react");

var RowField = React.createClass({
    displayName: "RowField",

    getInitialState: function getInitialState() {
        return {};
    },

    render: function render() {
        var childs = [];
        if (this.props.children) {

            var childLength = this.props.children.length;
            var colWidth = 12 / childLength;
            var colString = "col-" + colWidth + " column";

            childs = [];
            for (var childIndex in this.props.children) {
                var child = this.props.children[childIndex];
                childs.push(React.createElement(
                    "div",
                    { className: colString },
                    child
                ));
            }
        }

        return React.createElement(
            "div",
            { className: "grid" },
            React.createElement(
                "div",
                { className: "row" },
                childs,
                React.createElement("div", { className: "clearer" })
            ),
            React.createElement("div", { className: "clearer" })
        );
    }
});

module.exports = RowField;


},{"../ButtonComponent.react":30}],47:[function(require,module,exports){
"use strict";

var Spinner = require("../Spinner.react");

window.__hack__ = document.getElementById("application");

var PRESET_TEMPLATES = {
    form: React.createClass({
        displayName: "form",

        _onClick: function _onClick() {
            this.props.onClick(this.props.data);
        },

        render: function render() {
            var formName, accountName;
            var item = this.props.data[2];
            var className = 'item';
            formName = ewars.I18N(item.name);
            if (item.account) accountName = ewars.I18N(item.account.name);
            if (this.props.value == this.props.data[0]) className += " active";

            return React.createElement(
                "div",
                { onClick: this._onClick, className: className },
                React.createElement(
                    "div",
                    { className: "ew-list-item-title" },
                    formName
                ),
                React.createElement(
                    "span",
                    null,
                    accountName
                )
            );
        }
    }),
    alarm: React.createClass({
        displayName: "alarm",

        _onClick: function _onClick() {
            this.props.onClick(this.props.data);
        },

        render: function render() {
            var alarmName, accountName;
            var item = this.props.data[2];
            var className = 'item';
            if (this.props.value == this.props.data[0]) className += " active";
            alarmName = ewars.I18N(item.name);
            accountName = ewars.I18N(item.account.name);

            return React.createElement(
                "div",
                { onClick: this._onClick, className: className },
                React.createElement(
                    "div",
                    { className: "ew list-item-title" },
                    alarmName
                ),
                React.createElement(
                    "span",
                    null,
                    accountName
                )
            );
        }
    }),
    assignment: React.createClass({
        displayName: "assignment",

        _onClick: function _onClick() {
            this.props.onClick(this.props.data);
        },

        render: function render() {
            var shortName = ewars.I18N(this.props.data[2][0]);
            var longName = this.props.data[2][1];
            longName = longName.split(",");
            var longNameDOM = longName.map(function (item) {
                return React.createElement(
                    "span",
                    null,
                    item
                );
            });
            var className = 'item';
            if (this.props.value == this.props.data[0]) className += " active";

            return React.createElement(
                "div",
                { onClick: this._onClick, className: className },
                React.createElement(
                    "div",
                    { className: "ew list-item-title" },
                    shortName
                ),
                React.createElement(
                    "div",
                    { className: "loc-long-name" },
                    React.createElement(
                        "div",
                        { className: "wrap" },
                        longNameDOM
                    )
                )
            );
        }
    })
};

function _recurseOptions(items, label, val, level) {
    var options = [];
    var dashes = "";

    if (level) {
        dashes = _.times(level, function () {
            return " ";
        });
        dashes += "-";
    }

    _.each(items, function (option) {
        var itemLabel = dashes + " ";
        itemLabel += ewars.I18N(option[label]);

        options.push([option[val], itemLabel]);

        if (option.children.length > 0) {
            var subOptions = _recurseOptions(option.children, label, val, level + 1);
            options.push.apply(options, subOptions);
        }
    }, this);

    return options;
}

function _processExternalOptions(options, hProp) {
    var optionsMap = {};
    _.each(options, function (option) {
        if (!optionsMap[option.id]) optionsMap[option.id] = option;
        optionsMap[option.id].children = [];
    });

    _.each(optionsMap, function (option) {
        if (option.parent_id) {
            optionsMap[option.parent_id].children.push(option);
        }
    });

    var cleaned = _.filter(optionsMap, function (item) {
        if (item.parent_id) return false;
        return true;
    }, this);

    return cleaned;
}

var Handle = React.createClass({
    displayName: "Handle",

    render: function render() {
        var name = "No Selection";

        if (this.props.value) {
            var item = _.find(this.props.options, function (item) {
                if (item[0] == this.props.value) return true;
            }, this);

            if (item) name = item[1];
        }
        return React.createElement(
            "div",
            { className: "handle", onClick: this.props.onClick },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            null,
                            name
                        ),
                        React.createElement(
                            "td",
                            { width: "20px", className: "icon" },
                            React.createElement("i", { className: "fa fa-caret-down" })
                        )
                    )
                )
            )
        );
    }
});

var MultiSelectWrapper = React.createClass({
    displayName: "MultiSelectWrapper",

    getInitialState: function getInitialState() {
        return {};
    },

    _onClick: function _onClick() {
        this.props.onClick(this.props.data);
    },

    render: function render() {
        var iconClass = "fa";
        if (this.props.value) {
            if (this.props.value.indexOf(this.props.data[0]) >= 0) {
                iconClass += " fa-check-square-o";
            } else {
                iconClass += " fa-square-o";
            }
        } else {
            iconClass += " fa-square-o";
        }
        return React.createElement(
            "div",
            { className: "ew-select-multi-item" },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            { width: "25px" },
                            React.createElement(
                                "div",
                                { className: "ew-select-check" },
                                React.createElement("i", { className: iconClass })
                            )
                        ),
                        React.createElement(
                            "td",
                            null,
                            this.props.children
                        )
                    )
                )
            )
        );
    }
});

var Item = React.createClass({
    displayName: "Item",

    _onClick: function _onClick() {
        this.props.onClick(this.props.data);
    },

    render: function render() {
        var className = 'item';
        if (this.props.value == this.props.data[0]) className += " active";

        return React.createElement(
            "div",
            { onClick: this._onClick, className: className },
            this.props.data[1]
        );
    }
});

var SelectField = React.createClass({
    displayName: "SelectField",

    _isLoaded: false,
    _initialLoaded: false,

    PropTypes: {
        config: React.PropTypes.object
    },

    getDefaultProps: function getDefaultProps() {
        return {
            path: null,
            name: null,
            config: {}
        };
    },

    getInitialProps: function getInitialProps() {
        return {
            name: null,
            config: {
                multiple: false,
                path: null,
                groups: false
            },
            emptyText: "No Selection"
        };
    },

    getInitialState: function getInitialState() {
        return {
            options: [],
            rawOptions: [],
            showOptions: false,
            placeKey: _.uniqueId("SELECT_")
        };
    },

    _init: function _init(props) {
        if (props.value && props.config.optionsSource) {
            if (props.config.multiple) {
                this._initialLoaded = true;
                if (this.isMounted()) this.forceUpdate();
                this._toggle(null);
                return;
            }
            var resource = props.config.optionsSource.resource.toLowerCase();
            var select = [props.config.optionsSource.labelSource, props.config.optionsSource.valSource];
            if (props.config.optionsSource.select) {
                select = props.config.optionsSource.select;
            }

            if (this.props.config.optionsSource.additionalProperties) {
                select = select.concat(this.props.config.optionsSource.additionalProperties);
            }

            ewars.tx("com.ewars.resource", [resource, props.value, select, null]).then(function (resp) {
                this.state.options = [[resp[props.config.optionsSource.valSource], ewars.I18N(resp[props.config.optionsSource.labelSource])]];

                if (props.config) {
                    if (props.config.optionsSource) {
                        if (props.config.optionsSource.additional) {
                            _.each(props.config.optionsSource.additional, function (option) {
                                var optionValue = option[0];
                                if (optionValue == null) optionValue = "null";
                                this.state.options.unshift([optionValue, option[1]]);
                            }, this);
                        }
                    }
                }

                this._initialLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    componentWillMount: function componentWillMount() {
        this._id = _.uniqueId("SELECT_");
        if (this.props.config && this.props.config.optionsSource && this.props.value) {
            this._init(this.props);
        } else {
            this._initialLoaded = true;
            this.state.options = this.props.config.options;
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        if (nextProps.config && nextProps.config.optionsSource && nextProps.value) {
            if (this.props.value != nextProps.value) this._init(nextProps);
        } else {
            this._initialLoaded = true;
            this.state.options = nextProps.config.options;
        }
    },

    componentDidMount: function componentDidMount() {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function componentWillUnmount() {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
    },

    handleBodyClick: function handleBodyClick(evt) {
        if (this.refs.selector) {
            var area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showOptions = false;
                this.forceUpdate();
            }
        }
    },

    handleClick: function handleClick(e) {
        e.stopPropagation();
    },

    _selectNone: function _selectNone(e) {
        e.preventDefault();

        var name = this.props.config.nameOverride || this.props.name;

        this.props.onUpdate(name, [], this.props.path, null);
    },

    _selectAll: function _selectAll(e) {
        e.preventDefault();

        var name = this.props.config.nameOverride || this.props.name;

        var value = void 0;
        if (this.props.config.optionsSource) {
            value = this.state.rawOptions.map(function (item) {
                return item[this.props.config.optionsSource.valSource];
            }.bind(this));
        } else {
            value = this.props.config.options.map(function (item) {
                return item[0];
            }.bind(this));
        }

        this.props.onUpdate(name, value, this.props.path, null);
    },

    onChange: function onChange(item) {
        if (this.props.readOnly) return;
        this.state.showOptions = false;

        var name = this.props.config.nameOverride || this.props.name;

        var path = this.props.path || null;

        var node = item;
        if (this.props.config.optionsSource) {
            node = _.find(this.state.rawOptions, function (result) {
                if (result[this.props.config.optionsSource.valSource] == item[0]) return true;
            }, this);
        }

        if (!this.props.config.multiple) {
            if (this.props.config) this.props.onUpdate(name, item[0], path, node);
            if (!this.props.config) this.props.onUpdate(name, item[0], path, node);
        } else {
            var newVal = [];
            if (this.props.value && this.props.value.constructor === Array) {
                newVal = this.props.value;
            }

            if (newVal.indexOf(item[0]) < 0) {
                newVal.push(item[0]);
            } else {
                var tmp = _.filter(newVal, function (p) {
                    if (p == item[0]) return false;
                    return true;
                }, this);
                newVal = tmp;
            }

            this.props.onUpdate(name, newVal, path, node);
        }
    },

    _processConfig: function _processConfig() {
        var config = {};
        if (this.props.config) config = this.props.config;

        if (config.multiple == undefined || config.multiple == null) config.multiple = false;
        return config;
    },

    _getOptionDisplay: function _getOptionDisplay() {
        var result;
        for (var i in this.state.options) {
            if (this.state.options[i][0] == this.props.value) {
                result = this.state.options[i][1];
            }
        }
        return result;
    },

    _getOptions: function _getOptions() {
        if (this.props.options) return this.props.options;
        if (this.props.config.options) return this.props.config.options;
        if (this.state.options) return this.state.options;
        return [];
    },

    _toggle: function _toggle(e) {
        var _this = this;

        if (e) e.stopPropagation();
        this.state.showOptions = this.state.showOptions ? false : true;

        this.forceUpdate();

        if (this.props.config.optionsSource && this._isLoaded) {
            var options = [];

            if (this.props.config.optionsSource.hierarchical) {
                var optionsH = _processExternalOptions(this.state.rawOptions, this.props.config.optionsSource.hierarchyProp);
                options = _recurseOptions(optionsH, this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource, 0);
            } else {

                this.state.rawOptions.forEach(function (item) {
                    options.push([item[_this.props.config.optionsSource.valSource], ewars.I18N(item[_this.props.config.optionsSource.labelSource]), item]);
                });

                if (this.props.config) {
                    if (this.props.config.optionsSource) {
                        if (this.props.config.optionsSource.additional) {
                            _.each(this.props.config.optionsSource.additional, function (option) {
                                var optionValue = option[0];
                                if (optionValue == null) optionValue = "null";
                                options.unshift([optionValue, option[1]]);
                            }, this);
                        }
                    }
                }
            }

            this.state.options = options;
            if (this.isMounted()) this.forceUpdate();
        }

        if (this.props.config.optionsSource && !this._isLoaded) {
            // We need to load the options
            var resource = this.props.config.optionsSource.resource.toLowerCase();
            var select = [this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource];
            if (this.props.config.optionsSource.select) {
                select = this.props.config.optionsSource.select;
            }

            if (this.props.config.optionsSource.additionalProperties) {
                select = select.concat(this.props.config.optionsSource.additionalProperties);
            }

            var join = this.props.config.optionsSource.join || null;

            ewars.tx("com.ewars.query", [resource, select, this.props.config.optionsSource.query, null, null, null, join]).then(function (resp) {
                var options = [];

                if (this.props.config.optionsSource.hierarchical) {
                    var optionsH = _processExternalOptions(resp, this.props.config.optionsSource.hierarchyProp);
                    options = _recurseOptions(optionsH, this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource, 0);
                } else {

                    _.each(resp, function (item) {
                        options.push([item[this.props.config.optionsSource.valSource], ewars.I18N(item[this.props.config.optionsSource.labelSource]), item]);
                    }, this);

                    if (this.props.config) {
                        if (this.props.config.optionsSource) {
                            if (this.props.config.optionsSource.additional) {
                                _.each(this.props.config.optionsSource.additional, function (option) {
                                    var optionValue = option[0];
                                    if (optionValue == null) optionValue = "null";
                                    options.unshift([optionValue, option[1]]);
                                }, this);
                            }
                        }
                    }

                    this.state.rawOptions = resp;
                }

                this.state.options = options;
                this._isLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    render: function render() {
        var _this2 = this;

        var dataClassName = "ew-select-data";

        if (!this._initialLoaded) {
            return React.createElement(
                "div",
                { className: "ew-select" },
                React.createElement(
                    "div",
                    { className: "handle" },
                    React.createElement("i", { className: "fa fa-spin fa-circle-o-notch" })
                )
            );
        }

        var multiple = this.props.config.multiple;
        if (multiple) {
            dataClassName += " multi-select";
        }

        if (this.props.config) {
            if (this.props.config.multiple != undefined || this.props.config.multiple != null) {
                multiple = this.props.config.multiple;
            }
        }

        var config = this._processConfig();

        if (this.props.readOnly && !multiple) {
            var value = this._getOptionDisplay();
            return React.createElement("input", { type: "text", disabled: true, value: value });
        }

        var rawOptions = this._getOptions();

        var ItemTemplate = this.props.ItemTemplate || this.props.config.ItemTemplate;
        var templateName = this.props.template || this.props.config.template || null;
        if (templateName) ItemTemplate = PRESET_TEMPLATES[templateName];
        if (!ItemTemplate) ItemTemplate = Item;

        var options = rawOptions.map(function (option) {
            var id = ewars.utils.uuid();

            var item = void 0;
            if (ItemTemplate) {
                item = React.createElement(ItemTemplate, {
                    readOnly: _this2.props.readOnly,
                    multiple: multiple,
                    data: option,
                    onClick: _this2.onChange,
                    value: _this2.props.value,
                    key: id });
            }

            if (multiple) {
                item = React.createElement(
                    MultiSelectWrapper,
                    {
                        readOnly: _this2.props.readOnly,
                        data: option,
                        onClick: _this2.onChange,
                        value: _this2.props.value },
                    item
                );
            }

            return item;
        });

        if (!multiple) {
            options.unshift(React.createElement(
                "div",
                {
                    key: this.state.placeKey,
                    className: "item",
                    "data-key": this.state.placeKey,
                    "data-value": "null",
                    onClick: this.onChange },
                "No Selection"
            ));
        }

        if (this.props.config.optionsSource && !this._isLoaded) {
            options = React.createElement(Spinner, null);
        }

        var onClick = multiple ? null : this.handleBodyClick;
        var show = multiple ? true : this.state.showOptions;
        var handleClass = "ew-select";
        if (show) handleClass += " ew-select-open";

        return React.createElement(
            "div",
            { ref: "selector", className: handleClass, onClick: onClick },
            !multiple ? React.createElement(Handle, {
                onClick: this._toggle,
                emptyText: this.props.emptyText,
                value: this.props.value,
                options: rawOptions }) : null,
            show ? React.createElement(
                "div",
                { className: dataClassName },
                options
            ) : null,
            multiple && !this.props.readOnly ? React.createElement(
                "div",
                { style: { padding: 8, borderTop: "1px solid #CCC" } },
                React.createElement(
                    "a",
                    { onClick: this._selectAll, href: "#" },
                    "Select All"
                ),
                " | ",
                React.createElement(
                    "a",
                    { onClick: this._selectNone, href: "#" },
                    "Select None"
                )
            ) : null
        );
    }
});

module.exports = SelectField;


},{"../Spinner.react":34}],48:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var TextInputField = React.createClass({
    displayName: "TextInputField",

    propTypes: {
        _onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool
    },

    _options: {},

    componentWillMount: function componentWillMount() {
        this.state.value = this.props.value;
    },

    componentDidMount: function componentDidMount() {
        if (this.props.focus) {
            this.refs.inputField.getDOMNode().focus();
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        this.state.value = nextProps.value;
    },

    getInitialState: function getInitialState() {
        return {
            value: ""
        };
    },

    _handleKeyDown: function _handleKeyDown(e) {
        if (this.props.onKeyDown) this.props.onKeyDown(e);
    },

    _onBlur: function _onBlur(event) {
        if (this.props.config.trigger != "change") {
            if (this.state.value != this.props.value) {
                this.props.onUpdate(this.props.name, this.state.value);
            }
        }
    },

    _onChange: function _onChange(e) {
        var value = e.target.value;
        value = value.replace(/\s/g, "_").toLowerCase();
        value = value.replace("-", "_");
        value = value.replace(";", "_");

        if (this.props.config.trigger == "change") {
            this.props.onUpdate(this.props.name, value);
        } else {
            this.setState({
                value: value
            });
        }
    },

    render: function render() {

        return React.createElement("input", { id: "slug_field", ref: "inputField", disabled: this.props.readOnly, className: "form-control input-sm", onChange: this._onChange, onBlur: this._onBlur, type: "text", value: this.state.value, placeholder: this.props.placeholder, name: this.props.name, onKeyDown: this._handleKeyDown });
    }

});

module.exports = TextInputField;


},{}],49:[function(require,module,exports){
"use strict";

var SwitchField = React.createClass({
    displayName: "SwitchField",

    getInitialState: function getInitialState() {
        return {};
    },

    onChange: function onChange() {
        var value = false;
        if (this.props.value) value = true;

        if (this.props.config && this.props.config.path) {
            this.props.onUpdate(this.props.name, !value, this.props.config.path);
        } else {
            this.props.onUpdate(this.props.name, !value);
        }
    },

    render: function render() {
        var value = false;
        if (this.props.value) value = true;

        var name = ewars.utils.uniqueId("switch_");

        return React.createElement(
            "div",
            { className: "onoffswitch", onClick: this.onChange },
            React.createElement("input", {
                type: "checkbox", name: name, className: "onoffswitch-checkbox", id: "myonoffswitch",
                checked: this.props.value, onChange: this.onChange }),
            React.createElement(
                "label",
                { className: "onoffswitch-label", "for": "myonoffswitch" },
                React.createElement("span", { className: "onoffswitch-inner" }),
                React.createElement("span", { className: "onoffswitch-switch" })
            )
        );
    }
});

module.exports = SwitchField;


},{}],50:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var _languages = ["en", "fr", "ar"];

var LanguageNode = React.createClass({
    displayName: "LanguageNode",

    _onClick: function _onClick() {
        this.props.onClick(this.props.value);
    },

    render: function render() {
        var className = "language";
        if (this.props.active) className += " active";

        return React.createElement(
            "li",
            { className: className },
            React.createElement(
                "div",
                { onClick: this._onClick },
                this.props.value
            )
        );
    }
});

var TextAreaField = React.createClass({
    displayName: "TextAreaField",

    _value: null,

    getInitialProps: function getInitialProps() {
        return {
            config: {
                i18n: false,
                markdown: false
            }
        };
    },

    getInitialState: function getInitialState() {
        return {
            curLang: "en"
        };
    },

    componentWillMount: function componentWillMount() {
        // A little clean up, fix labels which are not dicts to dicts

    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {},

    onChange: function onChange(event) {
        var path = this.props.config ? this.props.config.path : null;

        if (this.props.config.i18n) {
            this._value[this.state.curLang] = event.target.value;
            this.props.onUpdate(this.props.name, this._value, path);
        } else {
            this.props.onUpdate(this.props.name, event.target.value, path);
        }
    },

    _handleLanguageChange: function _handleLanguageChange(lang) {
        this.setState({
            curLang: lang
        });
    },

    render: function render() {
        this._value = this.props.value;
        var viewValue = this.props.value;

        if (this.props.config.i18n) {

            if (!this.props.value) {
                this._value = { en: "", fr: null };
            }

            if (typeof this.props.value == "string") {
                this._value = {
                    en: this.props.value,
                    fr: null
                };
            }

            if (!this._value[this.state.curLang]) this._value[this.state.curLang] = "";
            viewValue = this._value[this.state.curLang];
        }

        if (this.props.config.i18n) {
            var nodes = _.map(_languages, function (language) {
                return React.createElement(LanguageNode, {
                    value: language,
                    key: language,
                    onClick: this._handleLanguageChange,
                    active: this.state.curLang == language });
            }, this);

            nodes.unshift(React.createElement(
                "li",
                { className: "icon" },
                React.createElement(
                    "div",
                    { className: "icon" },
                    React.createElement("i", { className: "fa fa-language" })
                )
            ));
        }

        if (!viewValue) viewValue = "";

        return React.createElement(
            "div",
            { className: "textarea-wrapper" },
            this.props.config.i18n ? React.createElement(
                "div",
                { className: "text-area-controls" },
                React.createElement(
                    "ul",
                    { className: "language-list" },
                    nodes
                )
            ) : null,
            React.createElement("textarea", {
                onChange: this.onChange,
                disabled: this.props.readOnly,
                placeholder: this.props.placeholder,
                className: "form-control",
                name: this.props.name,
                value: viewValue }),
            this.props.config.markdown ? React.createElement(
                "p",
                { className: "small" },
                React.createElement("i", { className: "fa fa-markdown" }),
                " \xA0This field supports markdown"
            ) : null
        );
    }

});

module.exports = TextAreaField;


},{}],51:[function(require,module,exports){
"use strict";

var TextInputField = React.createClass({
    displayName: "TextInputField",

    propTypes: {
        _onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool
    },

    _options: {},

    componentDidMount: function componentDidMount() {
        if (this.props.focus) {
            this.refs.inputField.focus();
        }
    },

    getDefaultProps: function getDefaultProps() {
        return {
            path: null,
            name: null,
            config: {}
        };
    },

    getInitialState: function getInitialState() {
        return {};
    },

    _handleKeyDown: function _handleKeyDown(e) {
        if (this.props.onKeyDown) this.props.onKeyDown(e);
    },

    _onChange: function _onChange(event) {
        this.props.onUpdate(this.props.name, event.target.value, this.props.path);
    },

    _onBlur: function _onBlur() {
        // Perform validation here

    },

    render: function render() {

        return React.createElement("input", {
            ref: "inputField",
            disabled: this.props.readOnly,
            className: "form-control input",
            onBlur: this._onBlur,
            onChange: this._onChange,
            type: "text",
            value: this.props.value,
            placeholder: this.props.placeholder,
            name: this.props.name,
            onKeyDown: this._handleKeyDown });
    }

});

module.exports = TextInputField;


},{}],52:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

// need to extend leaflet with TopoJSON support
if (window.L != undefined && window.L != null) {
    L.TopoJSON = L.GeoJSON.extend({
        addData: function addData(jsonData) {
            if (jsonData.type === "Topology") {
                for (key in jsonData.objects) {
                    geojson = topojson.feature(jsonData, jsonData.objects[key]);
                    L.GeoJSON.prototype.addData.call(this, geojson);
                }
            } else {
                L.GeoJSON.prototype.addData.call(this, jsonData);
            }
        }
    });
}

var TopoJSONField = React.createClass({
    displayName: "TopoJSONField",

    getInitialState: function getInitialState() {
        return {};
    },

    _render: function _render() {},

    componentWillReceiveProps: function componentWillReceiveProps() {},

    shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
        return false;
    },

    componentDidMount: function componentDidMount() {
        this.renderEditor();
    },

    componentDidUpdate: function componentDidUpdate() {
        this.renderEditor();
    },

    componentWillUnmount: function componentWillUnmount() {
        this.map.off("click", this.onMapClick);
        this.map = null;
    },

    onChange: function onChange(name, value) {
        this.props.onChange(name, value);
    },

    render: function render() {
        return React.createElement("div", { className: "map" });
    },

    renderEditor: function renderEditor() {

        var map = this.map = L.map(this.getDOMNode(), {
            minZoom: 2,
            maxZoom: 20,
            attributionControl: false
        });

        var topoLayer = new L.TopoJSON();

        topoLayer.addData(this.props.value);
        topoLayer.addTo(map);

        map.fitWorld();
    }

});

module.exports = TopoJSONField;


},{}],53:[function(require,module,exports){
"use strict";

var languages = ["en", "fr"];

var LanguageNode = React.createClass({
    displayName: "LanguageNode",

    _onClick: function _onClick() {
        this.props.onClick(this.props.value);
    },

    render: function render() {
        var className = "language";
        if (this.props.active) className += " active";

        return React.createElement(
            "li",
            { className: className },
            React.createElement(
                "div",
                { onClick: this._onClick },
                this.props.value
            )
        );
    }
});

var WYSIWYG = React.createClass({
    displayName: "WYSIWYG",

    _value: null,
    _redactor: null,

    gtDefaultProps: function gtDefaultProps() {
        return {
            config: {
                upload: false,
                uploadKey: null,
                i18n: false
            }
        };
    },

    getInitialState: function getInitialState() {
        return {
            curLang: "en"
        };
    },

    shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
        if (nextState.curLang != this.state.curLang) return true;
        if (nextProps.value != this.props.value) return true;
        return false;
    },

    componentDidMount: function componentDidMount() {
        this.renderEditor();
    },

    onChange: function onChange(name, value) {
        var path = this.props.config ? this.props.config.path : null;

        if (this.props.config.i18n) {
            this._value[this.state.curLang] = value;
            this.props.onUpdate(this.props.name, this._value, path);
        } else {
            this.props.onUpdate(this.props.name, value, path);
        }
    },

    render: function render() {
        this._value = this.props.value;
        var viewValue = this.props.value;

        if (this.props.config.i18n == true) {

            if (!this.props.value) {
                this._value = { en: "", fr: null };
            }

            if (typeof this.props.value == "string") {
                this._value = {
                    en: this.props.value,
                    fr: null
                };
            }

            if (!this._value[this.state.curLang]) this._value[this.state.curLang] = "";
            viewValue = this._value[this.state.curLang];

            var nodes = _.map(languages, function (language) {
                return React.createElement(LanguageNode, {
                    value: language,
                    onClick: this._handleLanguageChange,
                    active: this.state.curLang == language });
            }, this);

            nodes.unshift(React.createElement(
                "li",
                { className: "icon" },
                React.createElement(
                    "div",
                    { className: "icon" },
                    React.createElement("i", { className: "fa fa-language" })
                )
            ));
        }

        if (this.props.readOnly) {
            return React.createElement("div", { className: "article", dangerouslySetInnerHTML: { __html: viewValue } });
        }

        return React.createElement(
            "div",
            { className: "wysiwyg-editor" },
            this.props.config.i18n ? React.createElement(
                "div",
                { className: "text-area-controls" },
                React.createElement(
                    "ul",
                    { className: "language-list" },
                    nodes
                )
            ) : null,
            React.createElement("textarea", { id: "editor", name: this.props.name, value: viewValue, readOnly: false })
        );
    },

    _handleLanguageChange: function _handleLanguageChange(lang) {
        var value = this._value[lang];
        if (!value) value = "";
        $('#editor').redactor('code.set', value);

        this.setState({
            curLang: lang
        });
    },

    renderEditor: function renderEditor() {
        var self = this;
        if ($("#redactor").length <= 0) {
            var uploadUrl;
            if (this.props.config.upload) {
                uploadUrl = "/upload/" + this.props.config.uploadKey;
            }
            this._redactor = $('#editor').redactor({
                buttons: ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'image', 'file', 'link', 'alignment', 'horizontalrule', 'underline', 'table'],
                minHeight: 400,
                toolbarFixed: true,
                imageUpload: uploadUrl,
                changeCallback: function changeCallback() {
                    self.onChange(self.props.name, this.code.get());
                }
            });
        }
    }

});

module.exports = WYSIWYG;


},{}],54:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var FormFieldTree = require("../../trees/FormFieldTree.react");

var FormUtils = require("../../../utils/FormUtils");

function _findFieldByPath(options, path) {
    var found;

    _.each(options, function (field) {
        if (field.path == path) found = field;
        if (!found && field.children) {
            var subFound = _findFieldByPath(field.children, path);
            if (subFound) found = subFound;
        }
    }, this);

    return found;
}

var ConditionRow = React.createClass({
    displayName: "ConditionRow",

    _data: [null, "eq", null],
    _node: null,

    getInitialProps: function getInitialProps() {
        return {
            data: [null, "eq", null]
        };
    },

    getInitialState: function getInitialState() {
        return {
            data: [null, "eq", null]
        };
    },

    componentWillMount: function componentWillMount() {
        if (this.props.data) {
            this._node = _findFieldByPath(this.props.fieldOptions, this.props.data[0]);

            this.state.data = ewars.copy(this.props.data);
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        this.state.data = ewars.copy(nextProps.data);
    },

    _removeRule: function _removeRule() {
        this.props.onRemove(this.props.index);
    },

    _onFieldSelect: function _onFieldSelect(e) {
        this.state.data[0] = e.target.value;
        this.props.onUpdate(this.props.index, this.state.data);
    },

    _onValueChange: function _onValueChange(prop, value) {
        this.state.data[2] = value;
        this.props.onUpdate(this.props.index, this.state.data);
    },

    _onCmpChange: function _onCmpChange(e) {
        this.state.data[1] = e.target.value;
        this.props.onUpdate(this.props.index, this.state.data);
    },

    render: function render() {
        var valueControl;

        if (this.state.data[0] && this.state.data[0] != "none") {
            // Find the field definition
            var fieldDef = _.find(this.props.fieldOptions, function (field) {
                if (field[0] == this.state.data[0]) return true;
            }, this);

            var FieldControl = this.props.map[fieldDef[2].type];

            valueControl = React.createElement(FieldControl, {
                name: "null",
                value: this.props.data[2],
                onUpdate: this._onValueChange,
                config: fieldDef[2] });
        } else {
            valueControl = React.createElement(
                "span",
                null,
                "Please select a field"
            );
        }

        var options = _.map(this.props.fieldOptions, function (optionSet) {
            var fieldLabel = optionSet[1];
            return React.createElement(
                "option",
                { value: optionSet[0] },
                fieldLabel
            );
        }, this);
        options.unshift(React.createElement(
            "option",
            { value: "" },
            "-- Select a Field --"
        ));

        return React.createElement(
            "div",
            { className: "condition-row" },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            null,
                            React.createElement(
                                "select",
                                { name: "fieldName", value: this.props.data[0], id: "", "data-index": this.props.index, onChange: this._onFieldSelect },
                                options
                            )
                        ),
                        React.createElement(
                            "td",
                            null,
                            React.createElement(
                                "select",
                                { value: this.props.data[1], onChange: this._onCmpChange },
                                React.createElement(
                                    "option",
                                    { value: "eq" },
                                    "eq"
                                ),
                                React.createElement(
                                    "option",
                                    { value: "ne" },
                                    "ne"
                                ),
                                React.createElement(
                                    "option",
                                    { value: "gt" },
                                    "gt"
                                ),
                                React.createElement(
                                    "option",
                                    { value: "gte" },
                                    "gte"
                                ),
                                React.createElement(
                                    "option",
                                    { value: "lt" },
                                    "lt"
                                ),
                                React.createElement(
                                    "option",
                                    { value: "lte" },
                                    "lte"
                                )
                            )
                        ),
                        React.createElement(
                            "td",
                            null,
                            valueControl
                        ),
                        React.createElement(
                            "td",
                            { width: "30px" },
                            React.createElement(
                                "div",
                                { className: "btn-group" },
                                React.createElement(
                                    "button",
                                    { className: "condition-action red", onClick: this._removeRule },
                                    React.createElement("i", { className: "fa fa-close" })
                                )
                            )
                        )
                    )
                )
            )
        );
    }
});

module.exports = ConditionRow;


},{"../../../utils/FormUtils":80,"../../trees/FormFieldTree.react":68}],55:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CONSTANTS = require("../../../constants");
var Moment = require("moment");

var Day = function (_React$Component) {
    _inherits(Day, _React$Component);

    function Day(props) {
        _classCallCheck(this, Day);

        var _this = _possibleConstructorReturn(this, (Day.__proto__ || Object.getPrototypeOf(Day)).call(this, props));

        _this._onClick = function () {
            if (_this.props.active) {
                _this.props.onClick(_this.props.date);
            }
        };

        return _this;
    }

    _createClass(Day, [{
        key: "render",
        value: function render() {

            var className = "cal-day";
            if (this.props.value.date() == this.props.date.date() && this.props.value.month() == this.props.date.month() && this.props.value.year() == this.props.date.year()) {
                className += " selected";
            }

            if (!this.props.active) className += " inactive";

            if (!this.props.curDate.isSame(this.props.date, "M")) className = "cal-day-nan";

            var day = this.props.date.date();

            return React.createElement(
                "td",
                { onClick: this._onClick, className: className },
                day
            );
        }
    }]);

    return Day;
}(React.Component);

var DayPicker = function (_React$Component2) {
    _inherits(DayPicker, _React$Component2);

    function DayPicker(props) {
        _classCallCheck(this, DayPicker);

        var _this2 = _possibleConstructorReturn(this, (DayPicker.__proto__ || Object.getPrototypeOf(DayPicker)).call(this, props));

        _this2._prevMonth = function () {
            _this2.setState({
                curDate: _this2.state.curDate.clone().subtract(1, 'M')
            });
        };

        _this2._nextMonth = function () {
            _this2.setState({
                curDate: _this2.state.curDate.clone().add(1, "M")
            });
        };

        _this2._onDateSelect = function (newDate) {
            _this2.props.onChange(newDate);
        };

        _this2.state = {
            curDate: Moment.utc(_this2.props.value) || Moment.utc()
        };
        return _this2;
    }

    _createClass(DayPicker, [{
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            this.setState({
                curDate: Moment.utc(nextProps.value) || Moment.utc()
            });
        }
    }, {
        key: "render",
        value: function render() {

            var selected = Moment.utc(this.props.value);
            var value = this.state.curDate || Moment.utc();
            var year = value.year();

            var logical_month = value.month() - 1;

            // get the first day of the month
            var first_day = Moment([value.year(), value.month(), 1]);
            var first_day_weekday = first_day.day() == 0 ? 7 : first_day.day();

            // Find number of dasy in month
            var month_length = this.state.curDate.daysInMonth();
            var previous_month_length = Moment.utc([this.state.curDate.year(), this.state.curDate.month(), 1]).subtract(1, "M").daysInMonth();

            var monthShortName = this.state.curDate.format("MMM");

            var day = 1,
                prev = 1,
                next = 1;

            var rows = [];
            for (var i = 0; i < 9; i++) {
                var cells = [];
                var weekNo = Moment.utc([value.year(), value.month(), day]).isoWeek();
                cells.push(React.createElement(
                    "td",
                    { className: "cal-week-no" },
                    weekNo
                ));
                for (var j = 1; j <= 7; j++) {
                    if (day <= month_length && (i > 0 || j >= first_day_weekday)) {
                        var active = true;
                        if (this.props.block_future) {
                            var targetDate = Moment.utc([value.year(), value.month(), day]);
                            var today = Moment.utc();

                            if (targetDate.isAfter(today, "d")) active = false;
                        }
                        cells.push(React.createElement(Day, {
                            date: Moment.utc([value.year(), value.month(), day]),
                            value: selected,
                            active: active,
                            onClick: this._onDateSelect,
                            curDate: this.state.curDate }));
                        day++;
                    } else {
                        if (day <= month_length) {
                            var dayValue = previous_month_length - first_day_weekday + prev + 1;
                            cells.push(React.createElement(
                                "td",
                                { className: "cal-day-nan" },
                                dayValue
                            ));
                            prev++;
                        } else {
                            cells.push(React.createElement(
                                "td",
                                { className: "cal-day-nan" },
                                next
                            ));
                            next++;
                        }
                    }
                }

                // Stop making rows if we've run out of days
                rows.push(React.createElement(
                    "tr",
                    null,
                    cells
                ));
                if (day > month_length) {
                    break;
                }
            }

            return React.createElement(
                "div",
                { className: "date-picker" },
                React.createElement(
                    "div",
                    { className: "ide-layout" },
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { maxHeight: 30 } },
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement(
                                "div",
                                { className: "ide-row" },
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-left", style: { maxWidth: 25 }, onClick: this._prevMonth },
                                    React.createElement("i", { className: "fa fa-caret-left" })
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-header" },
                                    year,
                                    " - ",
                                    monthShortName
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-right", style: { maxWidth: 25 }, onClick: this._nextMonth },
                                    React.createElement("i", { className: "fa fa-caret-right" })
                                )
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { padding: 8 } },
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement(
                                "table",
                                null,
                                React.createElement(
                                    "thead",
                                    null,
                                    React.createElement(
                                        "tr",
                                        null,
                                        React.createElement(
                                            "th",
                                            null,
                                            "W"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Mon"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Tue"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Wed"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Thurs"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Fri"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Sat"
                                        ),
                                        React.createElement(
                                            "th",
                                            null,
                                            "Sun"
                                        )
                                    )
                                ),
                                React.createElement(
                                    "tbody",
                                    null,
                                    rows
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return DayPicker;
}(React.Component);

exports.default = DayPicker;


},{"../../../constants":70,"moment":86}],56:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DisplayPicker = function (_React$Component) {
    _inherits(DisplayPicker, _React$Component);

    function DisplayPicker(props) {
        _classCallCheck(this, DisplayPicker);

        return _possibleConstructorReturn(this, (DisplayPicker.__proto__ || Object.getPrototypeOf(DisplayPicker)).call(this, props));
    }

    _createClass(DisplayPicker, [{
        key: "render",
        value: function render() {
            var value = ewars.DATE(this.props.value, this.props.interval);
            return React.createElement(
                "div",
                { className: "display-field" },
                value
            );
        }
    }]);

    return DisplayPicker;
}(React.Component);

exports.default = DisplayPicker;


},{}],57:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CONSTANTS = require("../../../constants");
var Moment = require("moment");

var Month = function (_React$Component) {
    _inherits(Month, _React$Component);

    function Month(props) {
        _classCallCheck(this, Month);

        var _this = _possibleConstructorReturn(this, (Month.__proto__ || Object.getPrototypeOf(Month)).call(this, props));

        _this._onClick = function () {
            var newDate = Moment.utc([_this.props.year, _this.props.month, 1]);
            newDate = newDate.clone().endOf("M");
            _this.props.onClick(newDate);
        };

        return _this;
    }

    _createClass(Month, [{
        key: "render",
        value: function render() {
            var name = CONSTANTS.MONTHS_SHORT[this.props.month];

            var className = "cal-month";

            var nominalDate = Moment.utc([this.props.year, this.props.month, 1]).endOf("M");

            if (this.props.value.isSame(nominalDate, "day")) className += " selected";

            return React.createElement(
                "td",
                { onClick: this._onClick, className: className },
                name
            );
        }
    }]);

    return Month;
}(React.Component);

var MONTHS = [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11]];

var MonthPicker = function (_React$Component2) {
    _inherits(MonthPicker, _React$Component2);

    function MonthPicker(props) {
        _classCallCheck(this, MonthPicker);

        var _this2 = _possibleConstructorReturn(this, (MonthPicker.__proto__ || Object.getPrototypeOf(MonthPicker)).call(this, props));

        _this2._prevMonth = function () {
            _this2.setState({
                curDate: _this2.state.curDate.clone().subtract(1, 'Y')
            });
        };

        _this2._nextMonth = function () {
            _this2.setState({
                curDate: _this2.state.curDate.clone().add(1, "Y")
            });
        };

        _this2._onDateSelect = function (newDate) {
            _this2.props.onChange(newDate);
        };

        _this2.state = {
            curDate: Moment.utc(_this2.props.value) || Moment.utc()
        };
        return _this2;
    }

    _createClass(MonthPicker, [{
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            this.setState({
                curDate: Moment.utc(nextProps.value) || Moment.utc()
            });
        }
    }, {
        key: "render",
        value: function render() {

            var months = [];
            var selected = Moment.utc(this.props.value);
            var year = this.state.curDate.year();

            for (var i = 0; i <= 2; i++) {
                var cells = [];
                for (var j = 0; j <= 3; j++) {
                    cells.push(React.createElement(Month, {
                        onClick: this._onDateSelect,
                        year: year,
                        value: selected,
                        month: MONTHS[i][j] }));
                }
                months.push(React.createElement(
                    "tr",
                    null,
                    cells
                ));
            }

            return React.createElement(
                "div",
                { className: "date-picker" },
                React.createElement(
                    "div",
                    { className: "ide-layout" },
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { maxHeight: 30 } },
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement(
                                "div",
                                { className: "ide-row" },
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-left", style: { maxWidth: 25 }, onClick: this._prevMonth },
                                    React.createElement("i", { className: "fa fa-caret-left" })
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-header" },
                                    year
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-right", style: { maxWidth: 25 }, onClick: this._nextMonth },
                                    React.createElement("i", { className: "fa fa-caret-right" })
                                )
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { padding: 8 } },
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement(
                                "table",
                                null,
                                React.createElement(
                                    "tbody",
                                    null,
                                    months
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return MonthPicker;
}(React.Component);

exports.default = MonthPicker;


},{"../../../constants":70,"moment":86}],58:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Moment = require("moment");

var Week = function (_React$Component) {
    _inherits(Week, _React$Component);

    function Week(props) {
        _classCallCheck(this, Week);

        var _this = _possibleConstructorReturn(this, (Week.__proto__ || Object.getPrototypeOf(Week)).call(this, props));

        _this._onClick = function () {
            _this.props.onClick(_this.props.week);
        };

        return _this;
    }

    _createClass(Week, [{
        key: "render",
        value: function render() {

            var weekStart = Moment.utc(this.props.week).subtract(7, 'd');

            var label = ewars.DATE(this.props.week, "WEEK");
            var weekNo = this.props.week.isoWeek();

            var className = "cal-week";

            if (this.props.block_future) {
                if (this.props.week.isAfter(Moment.utc())) className += " inactive";
            }

            return React.createElement(
                "tr",
                { className: className, onClick: this._onClick },
                React.createElement(
                    "td",
                    null,
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        React.createElement(
                            "div",
                            { className: "ide-col cal-week-no", style: { maxWidth: 25 } },
                            weekNo
                        ),
                        React.createElement(
                            "div",
                            { className: "ide-col cal-week-label" },
                            label
                        )
                    )
                )
            );
        }
    }]);

    return Week;
}(React.Component);

var WeekPicker = function (_React$Component2) {
    _inherits(WeekPicker, _React$Component2);

    function WeekPicker(props) {
        _classCallCheck(this, WeekPicker);

        var _this2 = _possibleConstructorReturn(this, (WeekPicker.__proto__ || Object.getPrototypeOf(WeekPicker)).call(this, props));

        _this2._prevMonth = function () {
            _this2.setState({
                curYear: _this2.state.curYear - 1
            });
        };

        _this2._nextMonth = function () {
            _this2.setState({
                curYear: _this2.state.curYear + 1
            });
        };

        _this2._onDateSelect = function (newDate) {
            _this2.props.onChange(newDate);
        };

        _this2.state = {
            curYear: Moment.utc(_this2.props.value).year() || Moment.utc().year()
        };
        return _this2;
    }

    _createClass(WeekPicker, [{
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            this.setState({
                curYear: Moment.utc(nextProps.value).year() || Moment.utc().year()
            });
        }
    }, {
        key: "render",
        value: function render() {

            var isoWeeksInYear = Moment.utc(this.state.curYear).isoWeeksInYear();

            // For each week in the year, generate a list item.
            var weeks2 = [];

            console.log(isoWeeksInYear);

            for (var i = 0; i <= isoWeeksInYear; i++) {
                if (this.props.block_future) {
                    if (Moment.utc().isoWeekYear(this.state.curYear).isoWeek(i + 1).isoWeekday(7).isBefore(Moment.utc())) {
                        weeks2.push(Moment.utc().isoWeekYear(this.state.curYear).isoWeek(i + 1).isoWeekday(7));
                    }
                } else {
                    var dt = Moment.utc().isoWeekYear(this.state.curYear).isoWeek(i + 1).isoWeekday(7);

                    if (dt.isoWeekYear() == this.state.curYear) {
                        weeks2.push(Moment.utc().isoWeekYear(this.state.curYear).isoWeek(i + 1).isoWeekday(7));
                    }
                }
            }

            weeks2.reverse();

            // Given the month of the date, get the isoweeks in the month

            var selected = void 0;
            var value = void 0;

            var weeks = weeks2.map(function (week) {
                return React.createElement(Week, {
                    onClick: this._onDateSelect,
                    value: selected,
                    block_future: this.props.block_future,
                    week: week });
            }.bind(this));

            return React.createElement(
                "div",
                { className: "date-picker" },
                React.createElement(
                    "div",
                    { className: "ide-layout" },
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { maxHeight: 30 } },
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement(
                                "div",
                                { className: "ide-row" },
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-left", style: { maxWidth: 25 }, onClick: this._prevMonth },
                                    React.createElement("i", { className: "fa fa-caret-left" })
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-header" },
                                    this.state.curYear
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-col cal-right", style: { maxWidth: 25 }, onClick: this._nextMonth },
                                    React.createElement("i", { className: "fa fa-caret-right" })
                                )
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { padding: 8 } },
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement(
                                "table",
                                null,
                                React.createElement(
                                    "tbody",
                                    null,
                                    weeks
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return WeekPicker;
}(React.Component);

exports.default = WeekPicker;


},{"moment":86}],59:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Moment = require('moment');

var YearPicker = function (_React$Component) {
    _inherits(YearPicker, _React$Component);

    function YearPicker(props) {
        _classCallCheck(this, YearPicker);

        var _this = _possibleConstructorReturn(this, (YearPicker.__proto__ || Object.getPrototypeOf(YearPicker)).call(this, props));

        _this._movePrevious = function () {
            var newDate = void 0;
            if (Moment.isMoment(_this.props.value)) {
                newDate = _this.props.value.clone().subtract(1, "y").endOf("Y");
            } else {
                newDate = Moment.utc(_this.props.value || new Date()).subtract(1, 'y').endOf("Y");
            }
            _this.props.onChange(newDate);
        };

        _this._moveNext = function () {
            var newDate = void 0;
            if (Moment.isMoment(_this.props.value)) {
                newDate = _this.props.value.clone().add(1, "y").endOf("Y");
            } else {
                newDate = Moment.utc(_this.props.value || new Date()).add(1, 'y').endOf("Y");
            }
            _this.props.onChange(newDate);
        };

        return _this;
    }

    _createClass(YearPicker, [{
        key: "render",
        value: function render() {
            var value = Moment.utc(this.props.value || new Date());
            var year = value.year();

            return React.createElement(
                "div",
                { className: "date-picker" },
                React.createElement(
                    "div",
                    { className: "ide-layout" },
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        React.createElement(
                            "div",
                            { className: "ide-col cal-left", style: { maxWidth: 25 }, onClick: this._movePrevious },
                            React.createElement("i", { className: "fa fa-caret-left" })
                        ),
                        React.createElement(
                            "div",
                            { className: "ide-col cal-header" },
                            year
                        ),
                        React.createElement(
                            "div",
                            { className: "ide-col cal-right", style: { maxWidth: 25 }, onClick: this._moveNext },
                            React.createElement("i", { className: "fa fa-caret-right" })
                        )
                    )
                )
            );
        }
    }]);

    return YearPicker;
}(React.Component);

exports.default = YearPicker;


},{"moment":86}],60:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var MatrixCell = React.createClass({
    displayName: "MatrixCell",

    getInitialState: function getInitialState() {
        return {};
    },

    render: function render() {
        var label = ewars.formatters.I18N_FORMATTER(this.props.field.label);
        if (this.props.field.required) label += " *";

        var errors = void 0;
        if (this.props.errors) {
            if (this.props.errors[this.props.field.path]) {
                errors = this.props.errors[this.props.field.path];
            }
        }

        return React.createElement(
            "td",
            { className: "cell" },
            React.createElement(
                "div",
                { className: "cell-label" },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    errors ? React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 22 }, onHover: this._onHover },
                        React.createElement("i", { className: "fa fa-exclamation-triangle red" })
                    ) : null,
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        label
                    )
                )
            ),
            React.createElement(
                "div",
                { className: "cell-input" },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        this.props.children
                    ),
                    this.props.field.help ? React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 25 } },
                        React.createElement(
                            "a",
                            { href: "#", className: "field-control" },
                            React.createElement("i", { className: "fa fa-question-circle" })
                        )
                    ) : null
                ),
                errors ? React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col errors" },
                        errors.map(function (err) {
                            return React.createElement(
                                "div",
                                null,
                                err
                            );
                        })
                    )
                ) : null
            ),
            this.state.showErrors ? React.createElement("div", { className: "field-errors" }) : null
        );
    }
});

module.exports = MatrixCell;


},{}],61:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var MatrixRow = React.createClass({
    displayName: "MatrixRow",

    getInitialState: function getInitialState() {
        return {};
    },

    render: function render() {

        var label;

        if (this.props.data.show_row_label) {
            var labelContent = ewars.I18N(this.props.data.label);
            label = React.createElement(
                "th",
                null,
                labelContent
            );
        }

        var children = this.props.children;
        if (!Array.isArray(children)) {
            children = [children];
        }

        return React.createElement(
            "tr",
            null,
            label,
            children
        );
    }
});

module.exports = MatrixRow;


},{}],62:[function(require,module,exports){
"use strict";

var PointSelectionField = React.createClass({
    displayName: "PointSelectionField",

    _map: null,
    _marker: null,

    getDefaultProps: function getDefaultProps() {
        return {
            showTiles: true,
            height: 345,
            pointEditMode: false,
            defaultLat: 51.505,
            defaultLng: -0.09
        };
    },

    getInitialState: function getInitialState() {
        return {};
    },

    componentDidMount: function componentDidMount() {
        if (!this._map) this.renderEditor();
    },

    componentDidUpdate: function componentDidUpdate() {
        if (!this._map) this.renderEditor();

        if (this.props.value) {
            var value = JSON.parse(this.props.value);
            //this._marker = L.marker(value.coordinates).addTo(this._map);
            this._marker.setLatLng(value.coordinates);
            this._map.panTo(new L.LatLng(value.coordinates[0], value.coordinates[1]));
        }
    },

    componentWillUnmount: function componentWillUnmount() {
        if (this._map) {
            this._map.off("click", this.onMapClick);
            this._map = null;
        }
    },

    _useUsersLocation: function _useUsersLocation() {
        var startPos;
        var geoSuccess = function (position) {
            startPos = position;

            this._handleMapLocationChange(startPos.coords.latitude, startPos.coords.longitude);
        }.bind(this);

        var geoError = function geoError(error) {

            if (error.code == 1) {
                ewars.notifications.notification("fa-bell-o", "Geocoding Error", "You have previously selected that you do not want to allow this site to use your location, please clear your browsers location settings in order to use this function.");
            }

            if (error.code == 0) {
                ewars.notifications.notifications("fa-bell-o", "Unknown Error", "An unknown error occurred while trying to determing your location, please try again later.");
            }

            if (error.code == 2) {
                ewars.notifications.notification("fa-bell-o", "Location Unkown", "We could not reliably determine your current location.");
            }

            if (error.code == 3) {
                ewars.notifications.notification("fa-bell-o", "Geolocation Timeout", "Geolocation took too long and has been cancelled, please enter your location manually.");
            }
        };

        navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
    },

    _handleInputChange: function _handleInputChange(e) {
        var prop = e.target.name;
        var value = e.target.value;

        var data = JSON.parse(this.props.value);
        if (prop == "lat") data.coordinates[0] = value;
        if (prop == "lng") data.coordinates[1] = value;

        this.props.onUpdate(this.props.name, JSON.stringify(data));
    },

    render: function render() {
        if (this.props.value) {
            var value = JSON.parse(this.props.value);
            var lat = value.coordinates[0];
            var lng = value.coordinates[1];
        }

        return React.createElement(
            "div",
            { className: "administration-field", style: { height: 450 } },
            React.createElement("div", { className: "map", ref: "mapControl" }),
            React.createElement(
                "div",
                { className: "help" },
                "Double Click on the map to set the location or manually enter lat/lng below"
            ),
            React.createElement(
                "div",
                { className: "point-controls" },
                React.createElement(
                    "table",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            null,
                            React.createElement(
                                "span",
                                null,
                                "Lat"
                            ),
                            React.createElement("input", { type: "text", name: "lat", onChange: this._handleInputChange,
                                value: lat })
                        ),
                        React.createElement(
                            "td",
                            null,
                            React.createElement(
                                "span",
                                null,
                                "Lng"
                            ),
                            React.createElement("input", { type: "text", name: "lng", onChange: this._handleInputChange,
                                value: lng })
                        ),
                        React.createElement(
                            "td",
                            null,
                            React.createElement(
                                "button",
                                { onClick: this._useUsersLocation },
                                "Use my Location"
                            )
                        )
                    )
                )
            )
        );
    },

    _createGeoJSON: function _createGeoJSON(lat, lng) {
        return JSON.stringify({ "type": "Point", "coordinates": [lat, lng] });
    },

    _handleMapLocationChange: function _handleMapLocationChange(lat, lng) {
        this.props.onUpdate(this.props.name, this._createGeoJSON(lat, lng));
    },

    renderEditor: function renderEditor() {
        this._map = mapboxgl.Map(this.refs.mapControl.getDOMNode(), {
            minZoom: 0,
            maxZoom: 20,
            doubleClickZoom: false,
            worldCopyJump: true,
            attributionControl: false,
            scrollWheelZoom: false
        });

        if (this.props.value) {
            var value = JSON.parse(this.props.value);
            this._marker = L.marker(value.coordinates).addTo(this._map);
            this._map.setView(new L.LatLng(value.coordinates[0], value.coordinates[1]), 8);
        } else {
            this._marker = L.marker([46.232727, 6.134357]).addTo(this._map);
            this._map.setView(new L.LatLng(46.232727, 6.134357), 5);
        }

        this._map.on("dblclick", function (e) {
            this._handleMapLocationChange(e.latlng.lat, e.latlng.lng);
        }.bind(this));
    }
});

module.exports = PointSelectionField;


},{}],63:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Spinner = require("../../Spinner.react");

var AssignmentItem = React.createClass({
    displayName: "AssignmentItem",

    getInitialState: function getInitialState() {
        return {};
    },

    onNodeClick: function onNodeClick() {
        this.props.onClick(this.props.data);
    },

    render: function render() {
        var locationName = ewars.formatters.I18N_FORMATTER(this.props.data.location_name);

        return React.createElement(
            "div",
            { className: "item", onClick: this.onNodeClick },
            locationName
        );
    }
});

var Handle = React.createClass({
    displayName: "Handle",

    render: function render() {
        var name = ewars.I18N(this.props.current.name);

        return React.createElement(
            "div",
            { className: "handle", onClick: this.props.onClick },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            null,
                            name
                        ),
                        React.createElement(
                            "td",
                            { width: "20px", className: "icon" },
                            React.createElement("i", { className: "fa fa-caret-down" })
                        )
                    )
                )
            )
        );
    }
});

var Item = React.createClass({
    displayName: "Item",

    _onClick: function _onClick() {
        this.props.onClick(this.props.data);
    },

    render: function render() {
        var className = 'item';
        if (this.props.value == this.props.data[0]) className += " active";

        return React.createElement(
            "div",
            { onClick: this._onClick, className: className },
            this.props.data[1]
        );
    }
});

var AssignmentLocationField = React.createClass({
    displayName: "AssignmentLocationField",

    _isLoaded: false,
    _initialLoad: false,

    getInitialState: function getInitialState() {
        return {
            assignments: [],
            showTree: false,
            currentLocation: {
                name: { en: "No Location Selected" }
            }
        };
    },

    componentWillMount: function componentWillMount() {
        if (this.props.value) {
            this._loadSetLocation();
        } else {
            this._initialLoad = true;
        }
    },

    _loadSetLocation: function _loadSetLocation() {
        ewars.tx("com.ewars.resource", ["location", this.props.value, ["uuid", "name"], null]).then(function (resp) {
            this.state.currentLocation = resp;
            this._initialLoad = true;
            if (this.isMounted()) this.forceUpdate();
        }.bind(this));
    },

    _loadAssignments: function _loadAssignments() {
        if (this.props.config.form_id) {
            ewars.tx("com.ewars.user.assignments", []).then(function (resp) {
                this.state.assignments = resp;
                this._isLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        if (nextProps.config.form_id != this.props.config.form_id) {
            if (nextProps.config.form_id) {
                ewars.tx("com.ewars.user.assignments", []).then(function (resp) {
                    this.state.assignments = resp;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this));

                if (this.props.value != nextProps.value) {
                    ewars.tx("com.ewars.resource", ["location", nextProps.value, ["uuid", "name"], null]).then(function (resp) {
                        this.state.currentLocation = resp;
                        if (this.isMounted()) this.forceUpdate();
                    }.bind(this));
                }
            }
        } else if (this.props.value != nextProps.value) {
            ewars.tx("com.ewars.resource", ["location", nextProps.value, ["uuid", "name"], null]).then(function (resp) {
                this.state.currentLocation = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    onNodeSelect: function onNodeSelect(node) {
        this.state.currentLocation = node;
        this.state.showTree = false;
        this.forceUpdate();
        this.props.onUpdate(this.props.name, node.location_uuid, this.props.config.path || "");
    },

    _processLocations: function _processLocations() {
        var items = _.map(this.state.assignments, function (item) {
            if (item.form_id == this.props.config.form_id) {
                return React.createElement(AssignmentItem, { data: item, onClick: this.onNodeSelect });
            }
        }, this);

        return items;
    },

    _toggle: function _toggle() {
        this.state.showTree = this.state.showTree ? false : true;
        this.forceUpdate();

        if (!this._isLoaded) {
            this._loadAssignments();
        }
    },

    componentDidMount: function componentDidMount() {
        window.__assign_hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function componentWillUnmount() {
        window.__assign_hack__.removeEventListener('click', this.handleBodyClick);
    },

    handleBodyClick: function handleBodyClick(evt) {
        if (this.refs.selector) {
            var area = this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showTree = false;
                this.forceUpdate();
            }
        }
    },

    render: function render() {
        if (this.props.value && !this._initialLoad) {
            return React.createElement(
                "div",
                { className: "ew-select" },
                React.createElement(
                    "div",
                    { className: "handle" },
                    React.createElement("i", { className: "fa fa-spin fa-circle-o-notch" })
                )
            );
        }

        var items = _.map(this.state.assignments, function (item) {
            if (item.form_id == this.props.config.form_id) {
                return React.createElement(AssignmentItem, { data: item, onClick: this.onNodeSelect, key: item.assignment_id });
            }
        }, this);

        var selectedName = "No Location Selected";
        if (!this.props.config.form_id) selectedName = "Please select a form first.";
        if (this.state.currentLocation) selectedName = ewars.formatters.I18N_FORMATTER(this.state.currentLocation.location_name);

        if (this.props.readOnly) {
            return React.createElement("input", { type: "text", disable: true, value: selectedName });
        }

        if (!this._isLoaded && this.state.showTree) {
            items = React.createElement(Spinner, null);
        }

        return React.createElement(
            "div",
            { ref: "selector", className: "ew-select" },
            React.createElement(Handle, { onClick: this._toggle,
                current: this.state.currentLocation,
                value: this.props.value,
                options: this.state.assignments }),
            this.state.showTree ? React.createElement(
                "div",
                { className: "ew-select-data" },
                items
            ) : null
        );
    }

});

module.exports = AssignmentLocationField;


},{"../../Spinner.react":34}],64:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Button = require("../../../../common/components/ButtonComponent.react");
var TextField = require("../../../../common/components/fields/TextInputField.react");
var SelectField = require("../../../../common/components/fields/SelectField.react");

var REDUCTION = {
    options: [["SUM", "Sum"], ["MEDIAN", "Median"]]
};

var CompoundManager = function (_React$Component) {
    _inherits(CompoundManager, _React$Component);

    function CompoundManager(props) {
        _classCallCheck(this, CompoundManager);

        var _this = _possibleConstructorReturn(this, (CompoundManager.__proto__ || Object.getPrototypeOf(CompoundManager)).call(this, props));

        _this._onUpdate = function (prop, value) {
            _this.props.onUpdate(prop, value);
        };

        return _this;
    }

    _createClass(CompoundManager, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "basic" },
                React.createElement(
                    "div",
                    { className: "hsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Formula"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(TextField, {
                            name: "formula",
                            value: this.props.data.definition.formula,
                            onUpdate: this._onCompoundChange })
                    )
                ),
                React.createElement(
                    "div",
                    { className: "vsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Indicators"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(IndicatorList, { data: this.props.data, onUpdate: this._onUpdate })
                    )
                )
            );
        }
    }]);

    return CompoundManager;
}(React.Component);

var IndicatorItem = function (_React$Component2) {
    _inherits(IndicatorItem, _React$Component2);

    function IndicatorItem(props) {
        _classCallCheck(this, IndicatorItem);

        var _this2 = _possibleConstructorReturn(this, (IndicatorItem.__proto__ || Object.getPrototypeOf(IndicatorItem)).call(this, props));

        _this2._remove = function () {
            _this2.props.onRemove(_this2.props.index);
        };

        _this2.state = {
            indicator: {}
        };

        return _this2;
    }

    _createClass(IndicatorItem, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            ewars.tx("com.ewars.resource", ["indicator", this.props.data, ["uuid", "name", "definition", "itype", "icode"], null]).then(function (resp) {
                this.setState({
                    indicator: resp
                });
            }.bind(this));
        }
    }, {
        key: "render",
        value: function render() {
            var indName = this.state.indicator.icode || "" + " " + ewars.I18N(this.state.indicator.name || "Loading");

            return React.createElement(
                "div",
                { className: "block", style: { padding: 0 } },
                React.createElement(
                    "div",
                    { className: "block-content", style: { padding: 0 } },
                    React.createElement(
                        "div",
                        { className: "ide-layout" },
                        React.createElement(
                            "div",
                            { className: "ide-row" },
                            React.createElement(
                                "div",
                                { className: "ide-col", style: { padding: 8 } },
                                indName
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-col", style: { padding: 8 } },
                                React.createElement(
                                    "div",
                                    { className: "btn-group pull-right" },
                                    React.createElement(Button, {
                                        onClick: this._remove,
                                        icon: "fa-close" })
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return IndicatorItem;
}(React.Component);

var IndicatorList = function (_React$Component3) {
    _inherits(IndicatorList, _React$Component3);

    function IndicatorList(props) {
        _classCallCheck(this, IndicatorList);

        var _this3 = _possibleConstructorReturn(this, (IndicatorList.__proto__ || Object.getPrototypeOf(IndicatorList)).call(this, props));

        _this3._onDragEnter = function (e) {
            e.preventDefault();
        };

        _this3._onDragLeave = function (e) {};

        _this3._onDropped = function (e) {
            e.preventDefault();
            e.stopPropagation();

            var data = JSON.parse(e.dataTransfer.getData("item"));

            var definition = ewars.copy(_this3.props.data.definition);
            definition.series.push(data.uuid);

            _this3.props.onUpdate("series", definition.series);
        };

        _this3._remove = function (index) {
            var definition = ewars.copy(_this3.props.data.definition);
            definition.series.splice(index, 1);
            _this3.props.onUpdate("series", definition.series);
        };

        return _this3;
    }

    _createClass(IndicatorList, [{
        key: "render",
        value: function render() {
            var view = void 0;
            if (this.props.data.definition.series.length <= 0) {
                view = React.createElement(
                    "div",
                    { className: "placeholder" },
                    "Drag 'n' Drop indicators here"
                );
            } else {
                view = this.props.data.definition.series.map(function (item, index) {
                    return React.createElement(IndicatorItem, {
                        data: item,
                        key: item.uuid || item,
                        onRemove: this._remove,
                        index: index });
                }.bind(this));
            }

            return React.createElement(
                "div",
                { className: "ind-drop-zone", onDragOver: this._onDragEnter, onDragLeave: this._onDragLeave,
                    onDrop: this._onDropped },
                React.createElement(
                    "div",
                    { className: "block-tree" },
                    view
                )
            );
        }
    }]);

    return IndicatorList;
}(React.Component);

var AggregateManager = function (_React$Component4) {
    _inherits(AggregateManager, _React$Component4);

    function AggregateManager(props) {
        _classCallCheck(this, AggregateManager);

        var _this4 = _possibleConstructorReturn(this, (AggregateManager.__proto__ || Object.getPrototypeOf(AggregateManager)).call(this, props));

        _this4._onUpdate = function (prop, value) {
            _this4.props.onUpdate(prop, value);
        };

        return _this4;
    }

    _createClass(AggregateManager, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "basic" },
                React.createElement(
                    "div",
                    { className: "hsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Aggregation Type"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(SelectField, {
                            name: "reduction",
                            onUpdate: this._onUpdate,
                            config: REDUCTION,
                            value: this.props.data.definition.reduction })
                    )
                ),
                React.createElement(
                    "div",
                    { className: "vsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Indicators"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(IndicatorList, { data: this.props.data, onUpdate: this._onUpdate })
                    )
                )
            );
        }
    }]);

    return AggregateManager;
}(React.Component);

var IndicatorDefinition = React.createClass({
    displayName: "IndicatorDefinition",

    getInitialState: function getInitialState() {
        return {
            definition: {}
        };
    },

    _updateDefinition: function _updateDefinition(prop, value) {
        var data = ewars.copy(this.props.value.definition);
        data[prop] = value;

        this.props.onUpdate("definition", data);
    },

    render: function render() {

        var edit = void 0;
        if (this.props.value.itype == "COMPOUND") edit = React.createElement(CompoundManager, { data: this.props.value, onUpdate: this._updateDefinition });
        if (this.props.value.itype == "AGGREGATE") edit = React.createElement(AggregateManager, { data: this.props.value, onUpdate: this._updateDefinition });

        return edit;
    }
});

module.exports = IndicatorDefinition;


},{"../../../../common/components/ButtonComponent.react":30,"../../../../common/components/fields/SelectField.react":47,"../../../../common/components/fields/TextInputField.react":51}],65:[function(require,module,exports){
"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Button = require("../../ButtonComponent.react");

var TextField = require("../TextInputField.react");

var OptionRow = React.createClass({
    displayName: "OptionRow",

    getInitialState: function getInitialState() {
        return {};
    },

    _onUpdate: function _onUpdate() {},

    _addRow: function _addRow() {
        this.props.onAddRow();
    },

    _deleteRow: function _deleteRow() {
        this.props.onRemoveRow(this.props.index);
    },

    _updateLabel: function _updateLabel(prop, value) {
        this.props.onUpdate(this.props.index, "LABEL", value);
    },

    _updateValue: function _updateValue(prop, value) {
        this.props.onUpdate(this.props.index, "VALUE", value);
    },

    render: function render() {
        return React.createElement(
            "tr",
            null,
            React.createElement(
                "td",
                null,
                React.createElement(TextField, {
                    name: "value",
                    onUpdate: this._updateValue,
                    value: this.props.data[0],
                    data: { index: this.props.index } })
            ),
            React.createElement(
                "td",
                null,
                React.createElement(TextField, {
                    name: "label",
                    onUpdate: this._updateLabel,
                    value: this.props.data[1],
                    data: { index: this.props.index } })
            ),
            React.createElement(
                "td",
                { width: "70px" },
                React.createElement(
                    "div",
                    { className: "btn-group pull-right" },
                    React.createElement(Button, {
                        icon: "fa-plus",
                        colour: "green",
                        onClick: this._addRow }),
                    React.createElement(Button, {
                        icon: "fa-trash",
                        colour: "red",
                        data: { index: this.props.index },
                        onClick: this._deleteRow })
                )
            )
        );
    }
});

var SelectFieldOptionsField = React.createClass({
    displayName: "SelectFieldOptionsField",

    getInitialState: function getInitialState() {
        return {
            options: [["EXAMPLE", "Example"]]
        };
    },

    componentWillMount: function componentWillMount() {
        if (this.props.value) {
            var value = JSON.parse(JSON.stringify(this.props.value));
            this.state.options = value;
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            var value = JSON.parse(JSON.stringify(nextProps.value));
            this.state.options = value;
        }
    },

    _addRow: function _addRow() {
        this.state.options.push(["", ""]);
        this.props.onUpdate(this.props.name, this.state.options, this.props.config.path || null);
    },

    _removeRow: function _removeRow(index) {
        var value = JSON.parse(JSON.stringify(this.state.options));
        value.splice(index, 1);
        this.props.onUpdate(this.props.name, value, this.props.config.path || null);
    },

    _onUpdate: function _onUpdate(index, type, value) {
        if (type == "LABEL") this.state.options[index][1] = value;
        if (type == "VALUE") this.state.options[index][0] = value;
        this.props.onUpdate(this.props.name, this.state.options, this.props.config.path || null);
    },

    render: function render() {

        var options = this.state.options.map(function (item, index) {
            var _React$createElement;

            return React.createElement(OptionRow, (_React$createElement = {
                data: item,
                onUpdate: this._onUpdate,
                onAddRow: this._addRow,
                onRemoveRow: this._removeRow
            }, _defineProperty(_React$createElement, "onUpdate", this._onUpdate), _defineProperty(_React$createElement, "index", index), _React$createElement));
        }.bind(this));

        return React.createElement(
            "div",
            { style: { padding: "5px" } },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "th",
                            { style: { textAlign: "left", fontWeight: "bold" } },
                            "Value"
                        ),
                        React.createElement(
                            "th",
                            { style: { textAlign: "left", fontWeight: "bold" } },
                            "Label"
                        ),
                        React.createElement("th", null)
                    ),
                    options
                )
            ),
            React.createElement("div", { className: "btn-group" })
        );
    }
});

module.exports = SelectFieldOptionsField;


},{"../../ButtonComponent.react":30,"../TextInputField.react":51}],66:[function(require,module,exports){
"use strict";

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _IndicatorTreeComponent = require("../../../indicators/components/IndicatorTreeComponent.react");

var _IndicatorTreeComponent2 = _interopRequireDefault(_IndicatorTreeComponent);

var _USERS = require("../../forms/indicators/USERS");

var _USERS2 = _interopRequireDefault(_USERS);

var _OLD_ALERTS = require("../../forms/indicators/OLD_ALERTS");

var _OLD_ALERTS2 = _interopRequireDefault(_OLD_ALERTS);

var _ALERTS = require("../../forms/indicators/ALERTS");

var _ALERTS2 = _interopRequireDefault(_ALERTS);

var _ASSIGNMENTS = require("../../forms/indicators/ASSIGNMENTS");

var _ASSIGNMENTS2 = _interopRequireDefault(_ASSIGNMENTS);

var _DEVICES = require("../../forms/indicators/DEVICES");

var _DEVICES2 = _interopRequireDefault(_DEVICES);

var _FORM_SUBMISSIONS = require("../../forms/indicators/FORM_SUBMISSIONS");

var _FORM_SUBMISSIONS2 = _interopRequireDefault(_FORM_SUBMISSIONS);

var _LOCATIONS = require("../../forms/indicators/LOCATIONS");

var _LOCATIONS2 = _interopRequireDefault(_LOCATIONS);

var _TASKS = require("../../forms/indicators/TASKS");

var _TASKS2 = _interopRequireDefault(_TASKS);

var _FORMS = require("../../forms/indicators/FORMS");

var _FORMS2 = _interopRequireDefault(_FORMS);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = require("../ButtonComponent.react");

var Spinner = require("../Spinner.react");

var SelectField = require("../fields/SelectField.react");
var TextField = require("../fields/TextInputField.react");

var CONSTANTS = require("../../constants");

var TreeNode = require("./TreeNode.react");

var UUID_REG = /^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}/g;

var Form = require("../Form.react");

var SYSTEM_INDS = {
    USERS: _USERS2.default,
    ALERTS: _ALERTS2.default,
    OLD_ALERTS: _OLD_ALERTS2.default,
    ASSIGNMENTS: _ASSIGNMENTS2.default,
    DEVICES: _DEVICES2.default,
    FORM_SUBMISSIONS: _FORM_SUBMISSIONS2.default,
    LOCATIONS: _LOCATIONS2.default,
    TASKS: _TASKS2.default,
    FORMS: _FORMS2.default
};

var SYSTEM_TYPES = {
    ALARM: null,
    FORM: null,
    ORGANIZATION: null,
    ALERT: null,
    TASK: null
};

var resourceId = {
    "form": "id",
    alarm: "uuid",
    organization: "uuid"
};

var LABELS = {
    form: _l("FORM"),
    organization: _l("ORGANIZATION"),
    metric: _l("METRIC"),
    submitter_type: _l("SUBMITTER_TYPE"),
    form_id: _l("FORM"),
    organization_id: _l("ORGANIZATION"),
    source: _l("SOURCE")
};

var queryDef = {
    form: {
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: { eq: "ACTIVE" }
            },
            orderby: { "name.en": "ASC" }
        }
    },
    alarm: {
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            orderby: { "name.en": "ASC" }
        }
    },
    organization: {
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            orderby: { "name.en": "ASC" }
        }
    }

};

var itemStyle = {
    marginTop: 8
};

var ALARM_CONFIG = {
    optionsSource: {
        resource: "alarm",
        valSource: "uuid",
        labelSource: "name",
        query: function () {
            var q = {
                status: { eq: true }
            };

            if (window.user) {
                if ([CONSTANTS.SUPER_ADMIN, CONSTANTS.GLOBAL_ADMIN, CONSTANTS.INSTANCE_ADMIN].indexOf(window.user.role) < 0) {
                    q.account_id = window.user.account_id;
                }
            }

            return q;
        }()
    }
};

var COMPARATORS = [["EQ", "Equal to"], ["NEQ", "Not equal to"]];

var FilterControl = function (_React$Component) {
    _inherits(FilterControl, _React$Component);

    function FilterControl(props) {
        _classCallCheck(this, FilterControl);

        var _this = _possibleConstructorReturn(this, (FilterControl.__proto__ || Object.getPrototypeOf(FilterControl)).call(this, props));

        _this._onChange = function (prop, value) {
            var settings = ewars.copy(_this.props.settings);
            if (value) {
                settings[prop] = value;
            } else {
                if (settings[prop]) delete settings[prop];
            }
            _this.props.onChange(settings);
        };

        return _this;
    }

    _createClass(FilterControl, [{
        key: "render",
        value: function render() {
            var label = LABELS[this.props.data[0]];
            var valOpts = {};

            if (this.props.data[1]) {
                if (this.props.data[1].resource) {
                    valOpts = queryDef[this.props.data[1].resource];
                } else {
                    var options = this.props.data[1].map(function (item) {
                        return [item, _l(item)];
                    });
                    valOpts.options = options;
                }
            }

            var value = this.props.settings[this.props.data[0]] || null;

            return React.createElement(
                "div",
                { style: itemStyle },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col", style: { padding: "5px 5px 5px 0" } },
                        label
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement(SelectField, {
                            name: this.props.data[0],
                            onUpdate: this._onChange,
                            value: value,
                            config: valOpts })
                    )
                )
            );
        }
    }]);

    return FilterControl;
}(React.Component);

var IndicatorControl = function (_React$Component2) {
    _inherits(IndicatorControl, _React$Component2);

    function IndicatorControl(props) {
        _classCallCheck(this, IndicatorControl);

        var _this2 = _possibleConstructorReturn(this, (IndicatorControl.__proto__ || Object.getPrototypeOf(IndicatorControl)).call(this, props));

        _this2._filterChange = function (data, prop, value) {
            var settings = ewars.copy(_this2.props.data);
            settings[prop] = value;
            _this2.props.onChange(settings);
        };

        _this2.state = {
            items: []
        };
        return _this2;
    }

    _createClass(IndicatorControl, [{
        key: "render",
        value: function render() {

            var FORM_DEF = SYSTEM_INDS[this.props.itype];

            if (!FORM_DEF) {
                return React.createElement(
                    "div",
                    { className: "ind-options" },
                    React.createElement(
                        "div",
                        { className: "placeholder" },
                        "Loading..."
                    )
                );
            }

            var IndForm = this.props.Form || Form;

            return React.createElement(IndForm, {
                definition: FORM_DEF,
                data: this.props.data,
                readOnly: false,
                updateAction: this._filterChange });
        }
    }]);

    return IndicatorControl;
}(React.Component);

var Handle = React.createClass({
    displayName: "Handle",

    render: function render() {
        var name = "No Selection";

        if (this.props.value) {
            name = ewars.I18N(this.props.value.name);
        }
        return React.createElement(
            "div",
            { className: "handle", onClick: this.props.onClick },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            null,
                            name
                        ),
                        React.createElement(
                            "td",
                            { width: "20px", className: "icon" },
                            React.createElement("i", { className: "fa fa-caret-down" })
                        )
                    )
                )
            )
        );
    }
});

var IndicatorNode = function (_React$Component3) {
    _inherits(IndicatorNode, _React$Component3);

    function IndicatorNode(props) {
        _classCallCheck(this, IndicatorNode);

        var _this3 = _possibleConstructorReturn(this, (IndicatorNode.__proto__ || Object.getPrototypeOf(IndicatorNode)).call(this, props));

        _this3._isLoaded = false;

        _this3._onClick = function () {
            if (!_this3._isLoaded && _this3.props.data.context == "FOLDER") {
                ewars.tx("com.ewars.indicators", [_this3.props.data.id]).then(function (resp) {
                    this._isLoaded = true;
                    this.setState({
                        data: resp,
                        show: !this.state.show
                    });
                }.bind(_this3));
                return;
            }

            _this3.setState({
                show: !_this3.state.show
            });
        };

        _this3.state = {
            data: [],
            show: false
        };
        return _this3;
    }

    _createClass(IndicatorNode, [{
        key: "render",
        value: function render() {
            var _this4 = this;

            var className = "item locale-select";
            if (this.props.value == this.props.data.uuid) className += " active";

            var name = ewars.I18N(this.props.data.name);

            var children;
            if (this.state.show && this._isLoaded) {
                children = this.state.data.map(function (item) {
                    return React.createElement(IndicatorNode, {
                        onSelect: this.props.onSelect,
                        hideInactive: true,
                        key: item.uuid || item.id,
                        data: item });
                }.bind(this));
            } else {
                children = React.createElement(Spinner, null);
            }

            var iconClass = void 0;
            if (this.props.data.context == "FOLDER") iconClass = "fa fa-folder-o";
            if (this.props.data.context == "FOLDER" && this.state.show) iconClass = "fa fa-folder-open-o";

            var handleClass = "locale-tree-node";

            var showSelect = false;
            if (this.props.data.context == "INDICATOR") showSelect = true;
            if (showSelect) handleClass += " has-button";

            var handler = this._onClick;
            if (this.props.data.context == "INDICATOR") handler = function handler() {
                _this4.props.onSelect(_this4.props.data);
            };

            return React.createElement(
                "div",
                { className: className },
                React.createElement(
                    "div",
                    { className: handleClass, onClick: handler },
                    iconClass ? React.createElement(
                        "div",
                        { className: "expander" },
                        React.createElement("i", { className: iconClass })
                    ) : null,
                    React.createElement(
                        "div",
                        { className: "labeler no-pad" },
                        name
                    )
                ),
                this.state.show ? React.createElement(
                    "div",
                    { className: "locale-tree-node-children", style: { display: "block" } },
                    children
                ) : null
            );
        }
    }]);

    return IndicatorNode;
}(React.Component);

var IndicatorSelectorComponent = React.createClass({
    displayName: "IndicatorSelectorComponent",

    getDefaultProps: function getDefaultProps() {
        return {
            config: {
                showSystem: true
            },
            name: null,
            showSystem: true
        };
    },

    _isLoaded: false,

    getInitialState: function getInitialState() {
        return {
            showTree: false,
            data: [],
            search: "",
            nodeName: "Select Indicator",
            nodeUUID: null,
            multiple: false,
            indicator: null
        };
    },

    _onSearchChange: function _onSearchChange(e) {
        if (e.target.value == "") {
            ewars.tx("com.ewars.indicators", [null]).then(function (resp) {
                this._isLoaded = true;
                this.setState({
                    search: "",
                    data: resp
                });
            }.bind(this));
        } else {
            if (e.target.value.length > 3) {
                this._search(e.target.value);
            } else {
                this.setState(_extends({}, this.state, {
                    search: e.target.value
                }));
            }
        }
    },

    _clearSearch: function _clearSearch() {
        ewars.tx("com.ewars.indicators", [null]).then(function (resp) {
            this._isLoaded = true;
            this.setState({
                search: "",
                data: resp
            });
        }.bind(this));
    },

    _search: function _search(term) {
        var _this5 = this;

        var filter = {
            "name.en": { LIKE: term }
        };
        if (!this.props.showSystem) filter.itype = { eq: "DEFAULT" };
        ewars.tx("com.ewars.query", ["indicator", ["uuid", "name", "itype", "definition", "status"], filter, null, null, null, null]).then(function (resp) {
            _this5.setState({
                search: term,
                data: resp.map(function (item) {
                    return _extends({}, item, {
                        context: "INDICATOR"
                    });
                })
            });
        });
    },

    _toggle: function _toggle() {
        if (!this._isLoaded) {
            ewars.tx("com.ewars.indicators", [null]).then(function (resp) {
                this._isLoaded = true;
                this.setState({
                    data: resp,
                    showTree: !this.state.showTree
                });
            }.bind(this));
            return;
        }

        this.setState({
            showTree: !this.state.showTree
        });
    },
    componentDidMount: function componentDidMount() {
        if (_.isObject(this.props.value) && this.props.value.uuid) {
            this._init(this.props.value.uuid);
        } else {
            this._init(this.props.value);
        }
    },

    componentWillMount: function componentWillMount() {
        if (_.isObject(this.props.value) && this.props.value.uuid) {
            this._init(this.props.value.uuid);
        } else {
            this._init(this.props.value);
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        if (this.state.node) {
            if (_.isObject(nextProps.value)) {
                // This is a dict based indicator
                if (nextProps.value.uuid != this.state.node.uuid) this._init(nextProps.value.uuid);
            } else {
                if (this.state.node.uuid != nextProps.value) this._init(nextProps.value);
            }
        } else {
            if (_.isObject(nextProps.value)) this._init(nextProps.value.uuid);
            if (!_.isObject(nextProps.value)) this._init(nextProps.value);
        }
    },

    _init: function _init(value) {

        if (value && value.context) {
            // This is a legacy indicator
            this.setState({
                nodeName: null,
                nodeUUID: null,
                node: null
            });
            return;
        }
        if (value && !_.isObject(value)) {
            if (UUID_REG.test(value)) {
                ewars.tx("com.ewars.resource", ["indicator", value, null, null]).then(function (resp) {
                    this.setState({
                        nodeName: ewars.formatters.I18N_FORMATTER(resp.name),
                        nodeUUID: resp.uuid,
                        node: resp
                    });
                }.bind(this));
            }
        } else if (value && _.isObject(value)) {
            if (value.itype == "CUSTOM") {
                this.state.nodeName = value.name;
                this.state.nodeUUID = null;
                this.state.node = value;
            } else {
                ewars.tx("com.ewars.resource", ["indicator", value.uuid, null, null]).then(function (resp) {
                    if (resp != null) {
                        this.state.nodeName = ewars.I18N(resp.name);
                        this.state.nodeUUID = resp.uuid;
                        this.state.node = resp;
                    }
                }.bind(this));
            }
        }
    },

    _onNodeSelection: function _onNodeSelection(node) {
        if (node.context != "INDICATOR") return;

        this.setState({
            nodeName: ewars.formatters.I18N_FORMATTER(node.name),
            nodeUUID: node.uuid || null,
            node: node,
            showTree: false
        });

        var name = this.props.config.nameOverride || this.props.name;

        var path = this.props.config ? this.props.config.path : null;
        if (node.itype == "DEFAULT") {
            this.props.onUpdate(name, node.uuid, path, node);
        } else {
            this.props.onUpdate(name, { uuid: node.uuid, definition: {} }, path, node);
        }
    },

    _systemIndUpdate: function _systemIndUpdate(data) {
        var name = this.props.config.nameOverride || this.props.name;
        this.props.onUpdate(name, data, this.props.config ? this.props.config.path : null);
    },

    render: function render() {
        var nodeName = "Select Indicator";
        if (this.state.node) {
            nodeName = ewars.formatters.I18N_FORMATTER(this.state.node.name);
            if (this.state.node.itype == "CUSTOM") nodeName += " [legacy]";
        }

        var isConfigurable = false;
        if (this.state.node) {
            if (this.state.node.itype != "DEFAULT") {
                isConfigurable = true;
            }
        }

        var rootNodes = void 0;
        if (this._isLoaded && this.state.showTree) {
            rootNodes = this.state.data.map(function (item) {
                return React.createElement(IndicatorNode, { data: item, onSelect: this._onNodeSelection });
            }.bind(this));
        }

        return React.createElement(
            "div",
            { className: "depth" },
            React.createElement(
                "div",
                { ref: "selector", className: "ew-select locale-selector" },
                React.createElement(Handle, {
                    onClick: this._toggle,
                    readOnly: this.props.readOnly,
                    value: this.state.node }),
                this.state.showTree ? React.createElement(
                    "div",
                    { className: "ew-select-data" },
                    React.createElement(
                        "div",
                        { className: "locale-search" },
                        React.createElement(
                            "div",
                            { className: "input" },
                            React.createElement("input", { ref: "localeInput", type: "text", placeholder: "Search indicators...",
                                value: this.state.search,
                                onChange: this._onSearchChange }),
                            this.state.search.length > 0 ? React.createElement(
                                "div",
                                { className: "search-button", onClick: this._clearSearch },
                                React.createElement("i", {
                                    className: "fa fa-times-circle" })
                            ) : null
                        )
                    ),
                    rootNodes
                ) : null
            ),
            isConfigurable ? React.createElement(
                "div",
                { className: "add-on" },
                React.createElement(IndicatorControl, {
                    data: this.props.value,
                    readOnly: false,
                    Form: this.props.Form,
                    itype: this.state.node.itype || null,
                    onChange: this._systemIndUpdate,
                    definition: this.state.node.definition })
            ) : null
        );
    }
});

module.exports = IndicatorSelectorComponent;


},{"../../../indicators/components/IndicatorTreeComponent.react":82,"../../constants":70,"../../forms/indicators/ALERTS":71,"../../forms/indicators/ASSIGNMENTS":72,"../../forms/indicators/DEVICES":73,"../../forms/indicators/FORMS":74,"../../forms/indicators/FORM_SUBMISSIONS":75,"../../forms/indicators/LOCATIONS":76,"../../forms/indicators/OLD_ALERTS":77,"../../forms/indicators/TASKS":78,"../../forms/indicators/USERS":79,"../ButtonComponent.react":30,"../Form.react":31,"../Spinner.react":34,"../fields/SelectField.react":47,"../fields/TextInputField.react":51,"./TreeNode.react":67}],67:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var TreeNodeComponent = React.createClass({
    displayName: "TreeNodeComponent",

    _hasLoaded: false,

    getInitialState: function getInitialState() {
        return {
            children: [],
            showChildren: false
        };
    },

    onCaretClick: function onCaretClick(e) {
        e.stopPropagation();
        e.preventDefault();
        this.toggleCaret();
    },

    toggleCaret: function toggleCaret() {
        if (this._hasLoaded) {
            this.setState({
                showChildren: this.state.showChildren ? false : true
            });
            return;
        }

        var self = this;
        ewars._connect().then(function (session) {
            session.call("com.ewars.indicators", [self.props.data.id]).then(function (resp) {
                this._hasLoaded = true;

                if (resp.d.length > 0) {
                    this.setState({
                        children: resp.d,
                        showChildren: true
                    });
                } else {
                    this.forceUpdate();
                }
            }.bind(self));
        });
    },

    _onLabelClick: function _onLabelClick(e) {
        e.stopPropagation();
        e.preventDefault();

        if (this.props.data.type == "node") {
            this.props.onNodeSelect(this.props.data);
        } else {
            this.toggleCaret();
        }
    },

    _hasChildren: function _hasChildren() {
        if (this.props.data.child_count > 0) return true;
        return false;
    },

    _hasIndicators: function _hasIndicators() {
        if (this.props.data.indicator_count > 0) return true;
        return false;
    },

    _processChildren: function _processChildren() {
        var childs = [];

        for (var i in this.state.children) {
            var child = this.state.children[i];

            childs.push(React.createElement(TreeNodeComponent, {
                index: i,
                onNodeSelect: this.props.onNodeSelect,
                data: child }));
        }

        return childs;
    },

    render: function render() {

        var childs;
        if (this._hasChildren() || this._hasIndicators()) {
            childs = this._processChildren();
        }

        var iconClass = "fa fa-circle-o";
        if (this.props.data.type == "folder") iconClass = "fa fa-caret-right";
        if (this.props.data.type == "folder" && this._hasLoaded && this.state.showChildren) iconClass = "fa fa-caret-down";

        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.data.name);

        return React.createElement(
            "li",
            null,
            React.createElement(
                "div",
                { className: "node-control", onClick: this.onCaretClick },
                React.createElement("i", { className: iconClass })
            ),
            React.createElement(
                "div",
                { className: "label", onClick: this._onLabelClick },
                nodeName
            ),
            this.state.showChildren ? React.createElement(
                "div",
                { className: "children" },
                React.createElement(
                    "ul",
                    null,
                    childs
                )
            ) : null
        );
    }
});

module.exports = TreeNodeComponent;


},{}],68:[function(require,module,exports){
"use strict";

var TreeNode = React.createClass({
    displayName: "TreeNode",

    _hasLoaded: false,

    getInitialState: function getInitialState() {
        return {
            showChildren: false
        };
    },

    onCaretClick: function onCaretClick(e) {
        e.stopPropagation();
        e.preventDefault();

        if (["row", "group", "matrix", "form"].indexOf(this.props.data.nodeType) >= 0) {
            this.setState({
                showChildren: this.state.showChildren ? false : true
            });
        } else {
            this._onLabelClick(e);
        }
    },

    _onLabelClick: function _onLabelClick(e) {
        e.stopPropagation();
        e.preventDefault();

        if (["row", "group", "matrix", "form"].indexOf(this.props.data.field.type) < 0) {
            this.props.onNodeSelect(this.props.data);
        } else {
            this.onCaretClick(e);
        }
    },

    _hasChildren: function _hasChildren() {
        if (!this.props.data.children) return false;
        if (this.props.data.children.length > 0) return true;
        return false;
    },

    _getIcon: function _getIcon() {
        var icon = "fa ";

        if (["form", "row", "matrix"].indexOf(this.props.data.nodeType) >= 0) {
            icon += "fa-circle";
            if (this.props.data.children) {
                if (this.props.data.children.length > 0) icon = "fa fa-caret-right";
                if (this.props.data.children.length > 0 && this.state.showChildren) icon = "fa fa-caret-down";
            }
        } else {
            icon += "fa-th";
        }

        return icon;
    },

    render: function render() {
        var childs;

        if (this.state.showChildren) {
            childs = _.map(this.props.data.children, function (child) {
                return React.createElement(TreeNode, {
                    onNodeSelect: this.props.onNodeSelect,
                    data: child });
            }, this);
        }

        var iconClass = this._getIcon();

        var nodeName = ewars.formatters.I18N_FORMATTER(this.props.data.label);

        return React.createElement(
            "li",
            null,
            React.createElement(
                "div",
                { className: "node-control", onClick: this.onCaretClick },
                React.createElement("i", { className: iconClass })
            ),
            React.createElement(
                "div",
                { className: "label", onClick: this._onLabelClick },
                nodeName
            ),
            this.state.showChildren ? React.createElement(
                "div",
                { className: "children" },
                React.createElement(
                    "ul",
                    null,
                    childs
                )
            ) : null
        );
    }
});

function _findFieldByPath(options, path) {
    var found;

    _.each(options, function (field) {
        if (field.path == path) found = field;
        if (!found && field.children) {
            var subFound = _findFieldByPath(field.children, path);
            if (subFound) found = subFound;
        }
    }, this);

    return found;
}

var FormFieldTree = React.createClass({
    displayName: "FormFieldTree",

    getInitialState: function getInitialState() {
        return {
            nodeName: "None Selected",
            nodePath: null,
            showTree: false
        };
    },

    componentWillMount: function componentWillMount() {
        if (this.props.value) {
            var node = _findFieldByPath(this.props.options, this.props.value);

            if (node) {
                this.state.nodeName = node.pathLabel;
                this.state.nodePath = node.path;
            }
        }
    },

    _processTree: function _processTree() {
        return _.map(this.props.options, function (option) {
            return React.createElement(TreeNode, {
                data: option,
                onNodeSelect: this._onNodeSelection });
        }, this);
    },

    _hideTree: function _hideTree(e) {
        this.setState({
            showTree: false
        });
    },

    _toggleTree: function _toggleTree() {
        this.setState({
            showTree: this.state.showTree ? false : true
        });
    },

    _onNodeSelection: function _onNodeSelection(node) {
        this.setState({
            nodeName: node.pathLabel,
            nodePath: node.path,
            showTree: false
        });

        this.props.onUpdate(node);
    },

    render: function render() {
        var rootNodes = this._processTree();

        return React.createElement(
            "div",
            { className: "location-selector" },
            React.createElement(
                "div",
                { className: "handle" },
                React.createElement(
                    "div",
                    { className: "current" },
                    React.createElement(
                        "div",
                        { className: "current-location" },
                        this.state.nodeName
                    )
                ),
                React.createElement(
                    "div",
                    { className: "grip", onClick: this._toggleTree },
                    React.createElement("i", { className: "fa fa-globe" })
                )
            ),
            this.state.showTree ? React.createElement(
                "div",
                { className: "location-tree-wrapper" },
                React.createElement(
                    "div",
                    { className: "location-tree", style: { padding: 0, paddingTop: 0, paddingLeft: 0 } },
                    React.createElement(
                        "div",
                        { className: "location-stack" },
                        React.createElement(
                            "div",
                            { className: "location-tree" },
                            React.createElement(
                                "ul",
                                null,
                                rootNodes
                            )
                        )
                    )
                )
            ) : null
        );
    }
});

module.exports = FormFieldTree;


},{}],69:[function(require,module,exports){
"use strict";

var Button = require("../../ButtonComponent.react");

var TextField = require("../../fields/TextInputField.react");
var SelectField = require("../../fields/SelectField.react");
var IndicatorField = require("../../indicator/IndicatorSelectorComponent.react");

if (!ewars.copy) ewars.copy = function (item) {
    return JSON.parse(JSON.stringify(item));
};

var DEFAULTS = {
    variable_name: "var_name",
    indicator: null,
    reduction: "NONE"
};

var FORM = {
    reduction: {
        type: "select",
        label: { en: "Reduction" },
        options: [["NONE", "None"], ["SUM", "Sum"]]
    }
};

var ComplexSource = React.createClass({
    displayName: "ComplexSource",

    getInitialState: function getInitialState() {
        return {
            isExpanded: false
        };
    },

    _update: function _update(prop, value) {
        console.log(prop, value);
        if (prop == "variable_name") {
            value = value.replace(" ", "_");
        }
        this.props.onChange(this.props.index, prop, value);
    },

    _expand: function _expand() {
        this.setState({
            isExpanded: this.state.isExpanded ? false : true
        });
    },

    _delete: function _delete() {
        this.props.onDelete(this.props.index);
    },

    render: function render() {
        return React.createElement(
            "div",
            { className: "variable-source" },
            React.createElement(
                "div",
                { className: "header" },
                React.createElement(
                    "div",
                    { className: "label" },
                    this.props.data.variable_name
                ),
                React.createElement(
                    "div",
                    { className: "controls" },
                    React.createElement(
                        "div",
                        { className: "btn-group pull-right" },
                        React.createElement(Button, {
                            onClick: this._expand,
                            icon: "fa-pencil" }),
                        React.createElement(Button, {
                            onClick: this._delete,
                            colour: "red",
                            icon: "fa-trash" })
                    )
                )
            ),
            this.state.isExpanded ? React.createElement(
                "div",
                { className: "details" },
                React.createElement(
                    "div",
                    { className: "hsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Variable Name"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(TextField, {
                            name: "variable_name",
                            readOnly: false,
                            value: this.props.data.variable_name,
                            onUpdate: this._update })
                    )
                ),
                React.createElement(
                    "div",
                    { className: "hsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Indicator"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(IndicatorField, {
                            name: "indicator",
                            Form: this.props.Form,
                            readOnly: false,
                            value: this.props.data.indicator,
                            onUpdate: this._update })
                    )
                ),
                React.createElement(
                    "div",
                    { className: "hsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Reduction"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(SelectField, {
                            name: "reduction",
                            readOnly: false,
                            value: this.props.data.reduction,
                            onUpdate: this._update,
                            config: FORM.reduction })
                    )
                )
            ) : null
        );
    }
});

var ComplexSourcesManager = React.createClass({
    displayName: "ComplexSourcesManager",

    getInitialState: function getInitialState() {
        return {};
    },

    _onChange: function _onChange(index, prop, value) {
        var copy = ewars.copy(this.props.value || []);
        copy[index][prop] = value;
        this.props.onUpdate(this.props.name, copy);
    },

    _addVariable: function _addVariable() {
        var copy = ewars.copy(this.props.value || []);
        copy.push(_.extend({}, DEFAULTS));
        this.props.onUpdate(this.props.name, copy);
    },

    _delete: function _delete(index) {
        var copy = ewars.copy(this.props.value || []);
        copy.splice(index, 1);
        this.props.onUpdate(this.props.name, copy);
    },

    render: function render() {
        var variables = _.map(this.props.value, function (series, index) {
            return React.createElement(ComplexSource, {
                onDelete: this._delete,
                data: series,
                Form: this.props.Form,
                index: index,
                onChange: this._onChange });
        }, this);

        return React.createElement(
            "div",
            { className: "variable-manager" },
            React.createElement(
                "div",
                { className: "ide-tbar" },
                React.createElement(Button, {
                    icon: "fa-plus",
                    onClick: this._addVariable,
                    label: "Add Variable" })
            ),
            React.createElement(
                "div",
                { className: "series-list" },
                variables
            )
        );
    }
});

module.exports = ComplexSourcesManager;


},{"../../ButtonComponent.react":30,"../../fields/SelectField.react":47,"../../fields/TextInputField.react":51,"../../indicator/IndicatorSelectorComponent.react":66}],70:[function(require,module,exports){
"use strict";

var KeyMirror = require("keymirror");

module.exports = {
    // User Types
    SUPER_ADMIN: "SUPER_ADMIN",
    INSTANCE_ADMIN: "INSTANCE_ADMIN",
    GLOBAL_ADMIN: "GLOBAL_ADMIN",
    ACCOUNT_ADMIN: "ACCOUNT_ADMIN",
    USER: "USER",
    ORG_ADMIN: "ORG_ADMIN",
    LAB_USER: "LAB_USER",
    REGIONAL_ADMIN: "REGIONAL_ADMIN",
    COUNTRY_SUPERVISOR: "ACCOUNT_ADMIN",
    BOT: "BOT",
    API_USER: "API_USER",

    // General states
    DISABLED: "DISABLED",

    // Reporting intervals
    NONE: "NONE",
    DAY: "DAY",
    WEEK: "WEEK",
    MONTH: "MONTH",
    YEAR: "YEAR",

    // Filterable time periods
    ONEM: "1M",
    THREEM: "3M",
    SIXM: "6M",
    ONEY: "1Y",
    YTD: "YTD",
    ALL: "ALL",

    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    DRAFT: "DRAFT",
    DELETED: "DELETED",

    DEFAULT: "DEFAULT",

    DESC: "DESC",
    ASC: "ASC",

    FORWARD: "FORWARD",
    BACKWARD: "BACKWARD",

    // Icons
    ICO_DASHBOARD: "fa fa-dashboard",
    ICO_USERS: "fa fa-users",
    ICO_USER: "fa fa-user",
    ICO_REPORT: "fa fa-file",
    ICO_REPORTS: "fa fa-clipboard",
    ICO_GRAPH: "fa fa-bar-chart",
    ICO_INVESTIGATION: "fa fa-search",
    ICO_RELATED: "fa fa-link",
    ICO_GEAR: "fa fa-gear",
    ICO_CARET_DOWN: "fa fa-caret-down",
    ICO_SEND: "fa fa-envelope-o",
    ICO_SUBMIT: "fa-send",
    ICO_SAVE: "fa-save",
    ICO_CANCEL: "fa-close",

    // Chart Types
    SERIES: "SERIES",
    BAR: "BAR",
    MAP: "MAP",
    PIE: "PIE",
    SCATTER: "SCATTER",

    // Indicator types
    CUSTOM: "CUSTOM",

    OPEN: "OPEN",
    CLOSED: "CLOSED",

    // Form types
    PRIMARY: "PRIMARY",
    INVESTIGATIVE: "INVESTIGATIVE",
    LAB: "LAB",
    SURVEY: "SURVEY",

    // Alert stages
    VERIFICATION: "VERIFICATION",
    RISK_ASSESS: "RISK_ASSESS",
    RISK_CHAR: "RISK_CHAR",
    OUTCOME: "OUTCOME",

    // OUtcomes

    // Stage states
    DISCARD: "DISCARD",
    MONITOR: "MONITOR",
    ADVANCE: "ADVANCE",
    COMPLETED: "COMPLETED",
    PENDING: "PENDING",
    RESPOND: "RESPOND",
    DISCARDED: "DISCARDED",
    AUTODISCARDED: "AUTODISCARDED",

    // Risk levels
    LOW: "LOW",
    MODERATE: "MODERATE",
    HIGH: "HIGH",
    SEVERE: "SEVERE",

    // Colors
    RED: "red",
    GREEN: "green",
    ORANGE: "orange",
    YELLOW: "yellow",

    // Default Formats
    DEFAULT_DATE: "YYYY-MM-DD",

    // Dates
    MONTHS_LONG: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    MONTHS_SHORT: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    CAL_DAYS_LABELS: ["Sun", "Mon", "Tue", "Wed", "Thurs", "Fri", "Sat"],
    CAL_DAYS_IN_MONTH: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
};


},{"keymirror":85}],71:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var ALERTS = {
    alarm_id: {
        type: "select",
        label: "Alarm",
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {},
            additionalOptions: [["ANY", "Any"]]
        }
    },
    dimension: {
        type: "select",
        label: "Dimension",
        options: [["ALERTS_OPEN", "Open alerts"], ["ALERTS_CLOSED", "Closed alerts (incl. auto-discarded)"], ["ALERTS_CLOSED_NON_AUTO", "Closed alerts (not incl. auto-discarded"], ["ALERTS_AUTO_DISCARDED", "Auto-discarded alerts"], ["ALERTS_DISCARDED", "Alerts discarded"], ["ALERTS_MONITORED", "Alerts monitor"], ["ALERTS_RESPOND", "Alerts response"], ["ALERTS_IN_VERIFICATION", "Alerts in verification"], ["ALERTS_AWAIT_VERIFICATION", "Alerts awaiting verification"], ["ALERTS_VERIFIED", "Alerts verified"], ["ALERTS_DISCARDED_VERIFICATION", "Alerts discarded at verification"], ["ALERTS_MONITORED_VERIFICATION", "Alerts monitoring at verification"], ["ALERTS_IN_RISK_ASSESS", "Alerts in risk assessment"], ["ALERTS_AWAIT_RISK_ASSESS", "Alerts awaiting risk assessment"], ["ALERTS_RISK_ASSESSED", "Alerts risk assessed"], ["ALERTS_IN_RISK_CHAR", "Alerts in risk characterization"], ["ALERTS_AWAIT_RISK_CHAR", "Alerts awaiting risk characterization"], ["ALERTS_RISK_CHAR", "Alerts risk characterized"], ["ALERTS_IN_OUTCOME", "Alerts in outcome"], ["ALERTS_AWAIT_OUTCOME", "Alerts awaiting outcome"], ["ALERTS_OUTCOME", "Alerts with outcome"], ["ALERTS_DISCARD_OUTCOME", "Alerts discarded at outcome"], ["ALERTS_MONITORED_OUTCOME", "Alerts monitoring at outcome"], ["ALERTS_RESPOND_OUTCOME", "Alerts respond at outcome"], ["ALERTS_RISK_LOW", "Alerts (low risk)"], ["ALERTS_RISK_MODERATE", "Alerts (moderate risk)"], ["ALERTS_RISK_HIGH", "Alerts (high risk)"], ["ALERTS_RISK_SEVERE", "Alerts (very high risk)"]]
    }
};

exports.default = ALERTS;


},{}],72:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var ASSIGNMENTS = {
    form_id: {
        type: "select",
        label: "Form",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    },
    status: {
        type: "select",
        label: "Status",
        options: [["ACTIVE", "Active"], ["ELAPSED", "Elapsed"], ["INACTIVE", "Inactive"]]
    }
};

exports.default = ASSIGNMENTS;


},{}],73:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var DEVICES = {
    type: {
        type: "select",
        label: "Device Type",
        options: [["ANDROID", "Android"], ["IOS", "iOS"], ["DESKTOP", "Desktop"], ["NODE", "Node"]]
    },
    organization_id: {
        type: "select",
        label: "Organization",
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name",
            query: {}
        }
    },
    status: {
        type: "select",
        label: "Status",
        options: [["ANY", "Any"], ["SYNCED", "Synced"], ["UNSYNCED", "UnSynced"]]
    }
};

exports.default = DEVICES;


},{}],74:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var FORMS = {
    status: {
        type: "select",
        label: "Status",
        options: [["ACTIVE", "Active"], ["DRAFT", "Draft"]]
    },
    dimension: {
        type: "select",
        label: "Dimension",
        options: [["COUNT", "Count"], ["LOCATIONS", "Reporting Locations"]]
    },
    form_id: {
        type: "select",
        label: "Form",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: { eq: "ACTIVE" }
            }
        }
    }
};

exports.default = FORMS;


},{}],75:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var FORM_SUBMISSIONS = {
    form_id: {
        type: "select",
        label: "Form",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: { eq: "ACTIVE" }
            }
        }
    },
    metric: {
        type: "select",
        label: "Dimension",
        options: [["SUBMITTED", "Submitted"], ["LATE", "Late"], ["EXPECTED", "Expected"], ["MISSING", "Missing"], ["COMPLETENESS", "Completeness"], ["TIMELINESS", "Timeliness"]]
    },
    organization_id: {
        type: "select",
        label: "Organization",
        optionsSource: {
            resource: "organization",
            valSource: "uuid",
            labelSource: "name",
            query: {}
        }
    },
    source: {
        type: "select",
        label: "Source",
        options: [["ANDROID", "Mobile"], ["SYSTEM", "Web"], ["DESKTOP", "Desktop"], ["SMS", "SMS"]]
    }
};

exports.default = FORM_SUBMISSIONS;


},{}],76:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var LOCATIONS = {
    status: {
        type: "select",
        label: "Status",
        options: [["ACTIVE", "Active"], ["DISABLED", "Disabled"]]
    },
    site_type_id: {
        type: "select",
        label: "Location Type",
        optionsSource: {
            resource: "location_type",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    }
};

exports.default = LOCATIONS;


},{}],77:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var OLD_ALERTS = {
    alarm_id: {
        type: "select",
        label: "Alarm",
        optionsSource: {
            resource: "alarm",
            valSource: "uuid",
            labelSource: "name",
            query: {}
        }
    },
    state: {
        type: "select",
        label: "State",
        options: [["OPEN", "Open"], ["CLOSED", "Closed"], ["AUTODISCARDED", "Auto-Discarded"]]
    },
    stage: {
        type: "select",
        label: "Stage",
        options: [["VERIFICATION", "Verification"], ["RISK_ASSESS", "Risk Assessment"], ["RISK_CHAR", "Risk Characterization"], ["OUTCOME", "Outcome"]]
    },
    stage_state: {
        type: "select",
        label: "Stage State",
        options: [["PENDING", "Pending"], ["COMPLETED", "Completed"]]
    },
    risk: {
        type: "select",
        label: "Risk",
        options: [["LOW", "Low risk"], ["MODERATE", "Moderate risk"], ["HIGH", "High risk"], ["SEVERE", "Very high risk"]]
    }

};

exports.default = OLD_ALERTS;


},{}],78:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var TASKS = {
    type: {
        type: "select",
        label: "Task Type",
        options: [["REGISTRATION", "Registration"], ["AMENDMENT", "Amendment"], ["DISCARD", "Discard"]]
    },
    status: {
        type: "select",
        label: "Status",
        options: [["ANY", "Any"], ["PENDING", "Pending"], ["APPROVED", "Approved"], ["REJECTED", "Rejected"]]
    }
};

exports.default = TASKS;


},{}],79:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var USERS = {
    status: {
        type: "select",
        label: "Status",
        options: [["ACTIVE", "Active"], ["INACTIVE", "Inactive"], ["PENDING_APPROVAL"]]
    },
    organization_id: {
        type: "select",
        label: "Organization",
        optionsSource: {
            resource: "organization",
            labelSource: "name",
            valSource: "uuid",
            query: {}
        }
    },
    type: {
        type: "select",
        label: "User Type",
        options: [["USER", _l("USER")], ["REGIONAL_ADMIN", _l("REGIONAL_ADMIN")], ["ACCOUNT_ADMIN", _l("ACCOUNT_ADMIN")]]
    }
};

exports.default = USERS;


},{}],80:[function(require,module,exports){
"use strict";

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

function _convertFormToOptions(definition, path) {
    var options = [];

    _.each(definition, function (field, key) {
        var fieldName = ewars.formatters.I18N_FORMATTER(field.label);
        options.push([key, fieldName, field]);

        if (field.fields) {
            var subPath;
            if (path) {
                subPath = path + "." + key;
            } else {
                subPath = key;
            }
            var subFields = _convertFormToOptions(field.fields, subPath);
            options.push.apply(options, subFields);
        }
    }, this);

    return options;
}

/**
 * Converts a form definition to a tree
 * @param definition
 * @param place
 * @param placeName
 * @returns {Array}
 * @private
 */
function _convertFormToTree(definition, place, placeName) {
    var items = [];

    var asArray = _.map(definition, function (field, fieldName) {
        var newField = _.extend({}, field, { name: fieldName });
        return newField;
    }, this);

    var asSorted = _.sortBy(asArray, function (item) {
        return item.order;
    }, true);

    _.each(asSorted, function (field) {
        if (field.fields) {
            var path;
            if (!place) path = field.name;
            if (place) path = place + "/" + field.name;

            var pathName;
            if (!placeName) pathName = ewars.formatters.I18N_FORMATTER(field.label);
            if (placeName) pathName = placeName + " / " + ewars.formatters.I18N_FORMATTER(field.label);

            var children = _convertFormToTree(field.fields, path, pathName);

            items.push({
                label: ewars.formatters.I18N_FORMATTER(field.label),
                pathLabel: pathName,
                name: field.name,
                path: path,
                field: field,
                nodeType: field.type,
                children: children
            });
        } else {
            var path;
            if (!place) path = field.name;
            if (place) path = place + "/" + field.name;

            var pathName;
            if (!placeName) pathName = ewars.formatters.I18N_FORMATTER(field.label);
            if (placeName) pathName = placeName + " / " + ewars.formatters.I18N_FORMATTER(field.label);

            items.push({
                label: ewars.formatters.I18N_FORMATTER(field.label),
                name: field.name,
                pathLabel: pathName,
                path: path,
                field: field,
                nodeType: field.type
            });
        }
    }, this);

    return items;
}

function _findField(definition, fieldName) {
    var current = definition;

    if (definition[fieldName]) {
        current = definition[fieldName];
    }
}

/**
 * Retrieve a field from within the definition
 * @param definition
 * @param path
 * @returns {*}
 * @private
 */
function _getField(definition, specPath) {
    var item = definition;

    var path = specPath.split(".");
    if (path.indexOf("data.") >= 0) path.shift();

    path.forEach(function (pathItem) {
        if (item[pathItem]) {
            if (item[pathItem].fields) {
                item = item[pathItem].fields;
            } else {
                item = item[pathItem];
            }
        }
    }.bind(this));

    return item;
}

function _getFieldProperty(definition, field, prop) {
    if (field.indexOf(".") >= 0) {
        var splat = field.split(".");

        var defn = definition;
        var splatLen = splat.length;
        for (var i in splat) {
            if (defn[i]) {
                if (defn[i].fields) {
                    defn = defn[i].fields;
                } else {
                    defn = defn[i];
                }
            } else {}
        }
        return defn[prop];
    } else {
        return definition[field][prop] || null;
    }
}

/**
 * Check if a field is visible if it isnt' we shouldn'y
 * try to vlaidate it
 * @param data The data in the system
 * @param fieldDef The definition for the field
 * @param fieldKey
 * @private
 */
function _isVisible(data, fieldDef, fieldKey) {
    if (!fieldDef.conditions) return true;
}

/**
 * Instantiate an object with the defaults from a form definition
 * @param definition
 * @param flattened
 * @returns {{}}
 */
function setFormDefaults(definition, flattened) {
    var data = {};

    return data;
}

function isSet(value) {
    if (value == null) return false;
    if (value == undefined) return false;
    if (value == "") return false;
    if (value == "null") return false;
    if (value == "NULL") return false;
    if (value == "None") return false;

    return true;
};

function _validate(definition, data, flattened) {
    var errors = {};
    var hasErrors = false;

    for (var i in definition) {
        var field = definition[i];

        var value = void 0;
        if (flattened) {
            value = data[i] || null;
        } else {
            value = ewars.getKeyPath(i, data);
        }

        errors[i] = [];

        if (_isVisible(data, field, i)) {
            if (field.required) {
                if (!isSet(value)) {
                    hasErrors = true;
                    errors[i].push(ewars.I18N(field.label) + " is a required field");
                } else {
                    var typeErrors = [];
                    switch (field.type) {
                        case "number":
                            typeErrors = _validateNumber(field, value);
                            break;
                        case "text":
                            typeErrors = _validateText(field, value);
                            break;
                        case "select":
                            typeErrors = _validateSelect(field, value);
                            break;
                        case "date":
                            typeErrors = _validateDate(field, value);
                            break;
                        case "location":
                            typeErrors = _validateLocation(field, value);
                            break;
                        default:
                            break;
                    }

                    if (typeErrors.length > 0) hasErrors = true;
                    errors[i].push.apply(errors[i], typeErrors);
                }
            }
        }
    }

    for (var f in errors) {
        if (errors[f].length <= 0) delete errors[f];
    }

    return { hasErrors: hasErrors, errors: errors };
}

function _validateNumber(def, value) {
    var errors = [];

    return errors;
}

function _validateText(def, value) {
    return [];
}

function _validateSelect(def, value) {
    return [];
}

function _validateDate(def, value) {
    return [];
}

function _validateLocation(def, value) {
    return [];
}

function _getFieldsAsArray(sourceFields, parent) {
    var result = {};

    for (var i in sourceFields) {
        var field = sourceFields[i];

        var key = void 0;
        if (parent) {
            key = parent + "." + i;
        } else {
            key = "" + i;
        }

        if (field.type == "matrix") {
            var results = _getFieldsAsArray(field.fields, key);
            for (var f in results) {
                result[f] = results[f];
            }
        } else if (field.type == "row") {
            var _results = _getFieldsAsArray(field.fields, key);
            for (var f in _results) {
                result[f] = _results[f];
            }
        } else {
            result[key] = field;
        }
    }

    return result;
}

module.exports = {
    validate: _validate,
    flattenDefinition: function flattenDefinition(definition) {},
    asOptions: function asOptions(definition) {
        return _convertFormToOptions(definition, null);
    },
    getField: function getField(definition, fieldPath) {
        return _findField(definition, fieldPath);
    },

    nest: function nest(data) {
        var unnested = {};

        var _loop = function _loop() {
            var path = i.split(".");
            var last = path[path.length - 1];

            var term = unnested;

            path.forEach(function (item) {
                if (term[item]) {
                    term = term[item];
                } else {
                    if (item != last) {
                        term[item] = {};
                        term = term[item];
                    } else {
                        term[item] = data[i];
                    }
                }
            });
        };

        for (var i in data) {
            _loop();
        }

        return unnested;
    },
    flatten: function flatten(definition) {
        return _getFieldsAsArray(definition, null);
    },
    field: _getField,
    fieldProperty: _getFieldProperty,
    asTree: function asTree(form) {
        var formItems = void 0;
        if (form.version) formItems = _convertFormToTree(form.version.definition, null, "Form");
        if (!form.version) formItems = _convertFormToTree(form, null, "Form");

        if (form.location_aware) {
            formItems.unshift({
                label: ewars.formatters.I18N_FORMATTER({
                    en: "Report Location"
                }),
                field: {
                    type: "location",
                    required: true
                },
                nodeType: "location",
                type: "location",
                name: "location_id",
                path: "form/location_id",
                pathLabel: "Form / Location"
            });
        }

        formItems.unshift({
            label: ewars.formatters.I18N_FORMATTER({
                en: "Report Date"
            }),
            type: "date",
            field: {
                type: "date",
                required: true
            },
            nodeType: "date",
            name: "data_date",
            path: "form/data_date",
            pathLabel: "Form / Report Date"
        });

        return [{
            nodeType: "form",
            label: "Form",
            name: "form",
            field: {
                type: "form"
            },
            type: "form",
            children: formItems
        }];
    }
};


},{}],81:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var Moment = require("moment");

var RangeUtils = {};

var OFFSET_REG = /([-|+][0-9]*[DWMY])/;
var DATE_REG = /([0-9]{4}-[0-9]{2}-[0-9]{2})/;
var CODE_REG = /^([{][A-Z_]*[}])/;

var NUMERALS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

var CODES = {
    "{D_DATE}": "Document date",
    "{NOW}": "Today",
    "{END}": "End date",
    "{D_W_S}": "Document week start",
    "{D_W_E}": "Document week end",
    "{D_M_S}": "Document month start",
    "{D_M_E}": "Document month end",
    "{D_Y_S}": "Document year start",
    "{D_Y_E}": "Document year end",
    "{N_W_S}": "Current week start",
    "{N_W_E}": "Current week end",
    "{N_M_S}": "Current month start",
    "{N_M_E}": "Current month end",
    "{N_Y_S}": "Current year start",
    "{N_Y_E}": "Current year end"
};

var PRESETS_DEFAULT = {
    "-30D": ["{NOW}-30D", "{NOW}"],
    "-60D": ["{NOW}-60D", "{NOW}"],
    "-6M": ["{NOW}-6M", "{NOW}"],
    "-1Y": ["{NOW}-1Y", "{NOW}"],
    "-2Y": ["{NOW}-2Y", "{NOW}"],
    "-5Y": ["{NOW}-5Y", "{NOW}"],
    "-1D": ["{NOW}-1D", "{NOW}-1D"],
    "-2D": ["{NOW}-2D", "{NOW}-2D"],
    "-7D": ["{NOW}-7D", "{NOW}-7D"],
    "PREV_WEEK": ["{N_W_S}-1W", "{N_W_E}-1W"],
    "PREV_MONTH": ["{N_M_S}-1M", "{N_M_E}-1M"],
    "PREV_YEAR": ["{N_Y_S}-1Y", "{N_Y_E}-1Y"],
    "NOW": ["{NOW}", "{NOW}"],
    "CUR_WEEK": ["{N_W_S}", "{N_W_E}"],
    "CUR_MONTH": ["{N_M_S}", "{N_M_E}"],
    "CUR_YEAR": ["{N_Y_S}", "{N_Y_E}"],

    // TEMPLATE PRESETS
    "DD-30D": ["{D_DATE}-30D", "{D_DATE}"],
    "DD-60D": ["{D_DATE}-60D", "{D_DATE}"],
    "DD-6M": ["{D_DATE}-6M", "{D_DATE}"],
    "DD-1Y": ["{D_DATE}-1Y", "{D_DATE}"],
    "DD-2Y": ["{D_DATE}-2Y", "{D_DATE}"],
    "DD-5Y": ["{D_DATE}-5Y", "{D_DATE}"],
    "DD-1D": ["{D_DATE}-1D", "{D_DATE}-1D"],
    "DD-2D": ["{D_DATE}-2D", "{D_DATE}-2D"],
    "DD-7D": ["{D_DATE}-7D", "{D_DATE}-7D"],
    "D_PREV_WEEK": ["{D_W_S}-1W", "{D_W_E}-1W"],
    "D_PREV_MONTH": ["{D_M_S}-1M", "{D_M_E}-1M"],
    "D_PREV_YEAR": ["{D_Y_S}-1Y", "{D_Y_E}-1Y"],
    "D_DATE": ["{D_DATE}", "{D_DATE}"],
    "D_CUR_WEEK": ["{D_W_S}", "{D_W_E}"],
    "D_CUR_MONTH": ["{D_M_S}", "{D_M_E}"],
    "D_CUR_YEAR": ["{D_Y_S}", "{D_Y_E}"]
};

var PRESETS_TEMPLATE = {
    "-30D": ["{D_DATE}-30D", "{D_DATE}"],
    "-60D": ["{D_DATE}-60D", "{D_DATE}"],
    "-6M": ["{D_DATE}-6M", "{D_DATE}"],
    "-1Y": ["{D_DATE}-1Y", "{D_DATE}"],
    "-2Y": ["{D_DATE}-2Y", "{D_DATE}"],
    "-5Y": ["{D_DATE}-5Y", "{D_DATE}"],
    "-1D": ["{D_DATE}-1D", "{D_DATE}"],
    "-2D": ["{D_DATE}-2D", "{D_DATE}"],
    "-7D": ["{D_DATE}-7D", "{D_DATE}"],
    "PREV_WEEK": ["{D_W_S}-7D", "{D_W_E}-7D"],
    "PREV_MONTH": ["{D_M_S}-1M", "{D_M_E}-1M"],
    "PREV_YEAR": ["{D_Y_S}-1Y", "{D_Y_E}-1Y"],
    "NOW": ["{D_DATE}", "{D_DATE}"],
    "CUR_WEEK": ["{D_W_S}", "{D_W_E}"],
    "CUR_MONTH": ["{D_M_S}", "{D_M_E}"],
    "CUR_YEAR": ["{D_Y_S}", "{D_Y_E}"]
};

var INT_MARKERS = ["D", "W", "M", "Y"];
var DIGITS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

var DIGIT_STRINGS = {
    D: "day(s)",
    W: "week(s)",
    M: "month(s)",
    Y: "year(s)"
};

RangeUtils.processPreset = function (preset) {
    return PRESETS_DEFAULT[preset];
};

RangeUtils.formatDateSpec = function (dateSpec) {
    var result = "";

    if (DATE_REG.test(dateSpec)) {
        var datePortion = dateSpec.match(DATE_REG)[0];
        var offset = RangeUtils.getOffset(dateSpec);

        if (offset == "") {
            return datePortion;
        }

        result = datePortion;

        if (offset.indexOf("-") >= 0) {
            result = result + " - ";
            offset = offset.replace("-", "");
        }

        if (offset.indexOf("+") >= 0) {
            result = result + " + ";
            offset = offset.replace("+", "");
        }

        var lastChar = offset.substr(offset.length - 1);
        var period = void 0;
        if (INT_MARKERS.indexOf(lastChar) >= 0) {
            period = DIGIT_STRINGS[lastChar];
            offset = offset.replace(lastChar, "");
        } else {
            period = "day(s)";
        }

        result = result + " " + offset + " " + period;
        return result;
    } else if (CODE_REG.test(dateSpec)) {
        var codePortion = dateSpec.match(CODE_REG)[0];
        var _offset = RangeUtils.getOffset(dateSpec);

        result = CODES[codePortion];

        if (_offset == "") {
            return result;
        }

        if (_offset.indexOf("-") >= 0) {
            result = result + " - ";
            _offset = _offset.replace("-", "");
        }

        if (_offset.indexOf("+") >= 0) {
            result = result + " + ";
            _offset = _offset.replace("+", "");
        }

        var _lastChar = _offset.substr(_offset.length - 1);
        var _period = void 0;
        if (INT_MARKERS.indexOf(_lastChar) >= 0) {
            _period = DIGIT_STRINGS[_lastChar];
            _offset = _offset.replace(_lastChar, "");
        } else {
            _period = "day(s)";
        }

        result = result + " " + _offset + " " + _period;
        return result;
    }

    return dateSpec;
};

/**
 * Check if the string is a date
 * @param dateSpec
 * @returns {boolean}
 */
RangeUtils.isDate = function (dateSpec) {
    return DATE_REG.test(dateSpec);
};

RangeUtils.getDate = function (dateSpec) {
    return dateSpec.match(DATE_REG)[0];
};

/**
 * Determine if a date specification contains a code
 * @param dateSpec
 * @returns {boolean}
 */
RangeUtils.hasCode = function (dateSpec) {
    var hasCode = false;

    for (var i in CODES) {
        if (dateSpec.indexOf(i) >= 0) hasCode = true;
    }

    return hasCode;
};

RangeUtils.getCode = function (dateSpec) {
    var code = void 0;

    for (var i in CODES) {
        if (dateSpec.indexOf(i) >= 0) code = i;
    }

    code = code.replace("{", "");
    code = code.replace("}", "");

    return code;
};

/**
 * Test if a string contains an offset
 * @param dateSpec
 * @returns {boolean}
 */
RangeUtils.hasOffset = function (dateSpec) {
    var result = OFFSET_REG.test(dateSpec); // We don't have a full offset, but we might have a partial

    return result;
};

/**
 * Get the offset out of a string spec
 * @param dateSpec
 * @returns {string}
 */
RangeUtils.getOffset = function (dateSpec) {
    var result = "";

    if (DATE_REG.test(dateSpec)) {
        // This has a date in it, let's get it and strip it
        var tested = dateSpec.match(DATE_REG);
        result = dateSpec.replace(tested[0], "");
        return result;
    } else if (CODE_REG.test(dateSpec)) {
        var codeTest = dateSpec.match(CODE_REG);
        result = dateSpec.replace(codeTest[0], "");
        return result;
    }
};

/**
 * Apply an offset to the date spec
 * @param dateSpec
 * @param newOffset
 * @returns {*}
 */
RangeUtils.applyOffset = function (dateSpec, newOffset) {
    var curOffset = RangeUtils.getOffset(dateSpec);

    if (curOffset == "") {
        return dateSpec + newOffset;
    } else {
        if (DATE_REG.test(dateSpec)) {
            var origDate = dateSpec.match(DATE_REG)[0];
            return origDate + newOffset;
        } else if (CODE_REG.test(dateSpec)) {
            var origCode = dateSpec.match(CODE_REG)[0];
            return origCode + newOffset;
        }
    }

    return dateSpec;
};

RangeUtils.parseCode = function (code, reportDate, endDate) {
    switch (code) {
        case "{NOW}":
            return Moment.utc().format("YYYY-MM-DD");
        case "{D_DATE}":
            return Moment.utc(reportDate).format("YYYY-MM-DD");
        case "{END}":
            return endDate;
        case "{D_W_S}":
            return Moment.utc(reportDate).clone().startOf("isoweek").format("YYYY-MM-DD");
        case "{D_W_E}":
            return Moment.utc(reportDate).clone().endOf("isoweek").format("YYYY-MM-DD");
        case "{D_M_S}":
            return Moment.utc(reportDate).clone().startOf("month").format("YYYY-MM-DD");
        case "{D_M_E}":
            return Moment.utc(reportDate).clone().endOf("month").format("YYYY-MM-DD");
        case "{D_Y_S}":
            return Moment.utc(reportDate).clone().startOf("year").format("YYYY-MM-DD");
        case "{D_Y_E}":
            return Moment.utc(reportDate).clone().endOf("year").format("YYYY-MM-DD");
        case "{N_W_S}":
            return Moment.utc().startOf("isoweek").format("YYYY-MM-DD");
        case "{N_W_E}":
            return Moment.utc().endOf("isoweek").format("YYYY-MM-DD");
        case "{N_M_S}":
            return Moment.utc().startOf("month").format("YYYY-MM-DD");
        case "{N_M_E}":
            return Moment.utc().endOf("month").format("YYYY-MM-DD");
        case "{N_Y_S}":
            return Moment.utc().startOf("year").format("YYYY-MM-DD");
        case "{N_Y_E}":
            return Moment.utc().endOf("year").format("YYYY-MM-DD");
        default:
            return Moment.utc().format("YYYY-MM-DD");
    }
};

RangeUtils.getOperator = function (offset) {
    if (offset.indexOf("-") >= 0) {
        return "-";
    }

    if (offset.indexOf("+") >= 0) {
        return "+";
    }

    return "+";
};

RangeUtils.getRawOffset = function (offset) {
    var result = offset.replace("-", "");
    result = result.replace("+", "");
    result = result.replace("D", "");
    result = result.replace("W", "");
    result = result.replace("M", "");
    result = result.replace("Y", "");

    return result;
};

RangeUtils.getInterval = function (offset) {
    var result = offset.replace("-", "");
    result = result.replace("+", "");

    for (var i in NUMERALS) {
        result = result.replace(NUMERALS[i], "");
    }

    return result;
};

RangeUtils.getAsDays = function (count, period) {
    if (period == "D") return count;
    if (period == "W") return count * 7;
    if (period == "M") return count * 30;
    if (period == "Y") return count * 365;
};

/**
 * Process a date spec into an actual date
 */
RangeUtils.process = function (dateSpec, reportDate, endDate) {
    var code = void 0,
        offset = void 0,
        spec = void 0;

    var resultDate = void 0;

    if (CODE_REG.test(dateSpec)) {
        code = dateSpec.match(CODE_REG)[0];
    }

    if (DATE_REG.test(dateSpec)) {
        spec = dateSpec.match(DATE_REG)[0];
    }

    if (OFFSET_REG.test(dateSpec)) {
        offset = dateSpec.match(OFFSET_REG)[0];
    }

    if (!code && !offset) return spec;

    if (code && !offset) {
        return RangeUtils.parseCode(code, reportDate);
    }

    if (code && offset) {
        var result = RangeUtils.parseCode(code, reportDate, endDate);
        var operator = RangeUtils.getOperator(offset);
        var rawValue = RangeUtils.getRawOffset(offset);
        var interval = RangeUtils.getInterval(offset);

        var calcOffsetDays = RangeUtils.getAsDays(rawValue, interval);

        if (operator == "-") result = Moment.utc(result).subtract(calcOffsetDays, "d");
        if (operator == "+") result = Moment.utc(result).add(calcOffsetDays, "d");

        return result.format("YYYY-MM-DD");
    }
};

RangeUtils.getDaysBetween = function (startDate, endDate) {
    var resultantDates = [startDate];

    var cur_date = Moment.utc(startDate).clone();
    var end_date = Moment.utc(endDate).clone();

    while (cur_date.isBefore(end_date, "d")) {
        var newDate = cur_date.clone().add(1, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;
};

RangeUtils.getWeeksBetween = function (startDate, endDate) {
    var resultantDates = [Moment.utc(startDate).clone()];

    var cur_date = startDate.clone();
    var end_date = Moment.utc(endDate).clone();

    while (cur_date.isBefore(end_date, "day")) {
        var newDate = cur_date.clone().add(7, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;
};

RangeUtils.getDatesInRange = function (startDate, endDate, interval) {
    if (interval == "DAY") return RangeUtils.getDaysBetween(startDate, endDate);
    if (interval == "WEEK") return RangeUtils.getWeeksBetween(startDate, endDate);
};

var INTERVAL_REMAP = {
    "DAY": "D",
    "WEEK": "W",
    "MONTH": "M",
    "YEAR": "Y"
};
/**
 * Convert a legacy time period to the new format
 * @param config
 */
RangeUtils.convertLegacy = function (config) {
    var startDate = void 0;
    var endDate = void 0;

    if (config.start_date_spec) {
        if (config.start_date_spec == "REPORT_DATE") {
            startDate = "{D_DATE}";
        } else if (config.start_date_spec == "MANUAL") {
            startDate = config.start_date;
        } else if (config.start_date_spec == "CURRENT") {
            startDate = "{TODAY}";
        } else if (config.start_date_spec == "END_SUBTRACTION") {
            startDate = "{END}-" + config.start_intervals + INTERVAL_REMAP[config.timeInterval || config.interval];
        }
    } else {
        startDate = config.start_date;
    }

    if (config.end_date_spec) {
        if (config.end_date_spec == "REPORT_DATE") {
            endDate = "{D_DATE}";
        } else if (config.end_date_spec == "MANUAL") {
            endDate = config.end_date;
        } else if (config.end_date_spec == "CURRENT") {
            endDate = "{NOW}";
        } else if (config.end_date_spec == "REPORT_DATE_SUBTRACTION") {
            endDate = "{D_DATE}-" + config.end_intervals + INTERVAL_REMAP[config.timeInterval || config.interval];
        }
    } else {
        endDate = config.end_date;
    }

    return [startDate, endDate];
};

exports.default = RangeUtils;


},{"moment":86}],82:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ContextMenu = require("../../common/ContextMenu.react");

var _ContextMenu2 = _interopRequireDefault(_ContextMenu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IndicatorNode = function (_React$Component) {
    _inherits(IndicatorNode, _React$Component);

    function IndicatorNode(props) {
        _classCallCheck(this, IndicatorNode);

        var _this = _possibleConstructorReturn(this, (IndicatorNode.__proto__ || Object.getPrototypeOf(IndicatorNode)).call(this, props));

        _this._onBodyClick = function (evt) {
            if (!_this.refs.item.contains(evt.target)) {
                _this.setState({
                    showContext: false
                });
            }
        };

        _this._onContext = function (e) {
            e.preventDefault();
            _this.setState({
                showContext: true
            });
        };

        _this._onContextAction = function (action) {
            _this.setState({
                showContext: false
            });
            _this.props.onAction(action, _this.props.data);
        };

        _this._onClick = function () {
            _this.props.onAction("CLICK", _this.props.data);
        };

        _this._onCheck = function () {
            _this.props.onAction("CHECK", _this.props.data);
        };

        _this._onSelect = function () {
            _this.props.onAction("INDICATOR_SELECT", _this.props.data);
        };

        _this._dragStart = function (e) {
            e.dataTransfer.setData("item", JSON.stringify(_this.props.data));
        };

        _this.state = {
            showContext: false
        };
        return _this;
    }

    _createClass(IndicatorNode, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._onBodyClick);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._onBodyClick);
        }
    }, {
        key: "render",
        value: function render() {
            var label = ewars.I18N(this.props.data.name);

            var icon = void 0;
            if (this.props.allowCheck) {
                icon = "fa fa-square-o";
                if (this.props.checked.indexOf(this.props.data.uuid) >= 0) icon = "fa fa-check-square-o";
            }

            var className = "block-content";
            className = className + (this.props.data.status == "ACTIVE" ? " status-green" : " status-red");

            return React.createElement(
                "div",
                { className: "block", onClick: this._onClick, onDragStart: this._dragStart, draggable: true, ref: "item",
                    onContextMenu: this._onContext },
                React.createElement(
                    "div",
                    { className: className },
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        this.props.allowCheck ? React.createElement(
                            "div",
                            { className: "ide-col", style: { maxWidth: 25 }, onClick: this._onCheck },
                            React.createElement("i", { className: icon })
                        ) : null,
                        React.createElement(
                            "div",
                            { className: "ide-col", onClick: this._onSelect },
                            label
                        )
                    ),
                    this.props.actions && this.state.showContext ? React.createElement(_ContextMenu2.default, {
                        absolute: true,
                        onClick: this._onContextAction,
                        actions: this.props.actions }) : null
                )
            );
        }
    }]);

    return IndicatorNode;
}(React.Component);

var IndicatorFolderNode = function (_React$Component2) {
    _inherits(IndicatorFolderNode, _React$Component2);

    function IndicatorFolderNode(props) {
        _classCallCheck(this, IndicatorFolderNode);

        var _this2 = _possibleConstructorReturn(this, (IndicatorFolderNode.__proto__ || Object.getPrototypeOf(IndicatorFolderNode)).call(this, props));

        _this2._onBodyClick = function (evt) {
            if (!_this2.refs.item.contains(evt.target)) {
                _this2.setState({
                    showContext: false
                });
            }
        };

        _this2._reload = function () {
            ewars.tx("com.ewars.indicators", [_this2.props.data.id]).then(function (resp) {
                this.setState({
                    data: resp
                });
            }.bind(_this2));
        };

        _this2.getChildren = function () {
            if (_this2.state.data) {
                _this2.setState({
                    showChildren: !_this2.state.showChildren
                });
                return;
            }

            _this2.refs.iconHandle.setAttribute("class", "fa fa-spin fa-gear");

            // Get groups first
            ewars.tx("com.ewars.indicators", [_this2.props.data.id]).then(function (resp) {
                this.setState({
                    data: resp,
                    showChildren: true
                });
            }.bind(_this2));
        };

        _this2._onCaretClick = function () {
            _this2.getChildren();
        };

        _this2._dragStart = function (e) {
            e.dataTransfer.setData("item", JSON.stringify(_this2.props.data));
        };

        _this2._onContext = function (e) {
            e.preventDefault();
            _this2.setState({
                showContext: true
            });
        };

        _this2._onContextAction = function (action) {
            _this2.setState({
                showContext: false
            });

            _this2.props.onAction(action, _this2.props.data);
        };

        _this2.state = ewars.g["FOLDER_" + _this2.props.data.id] || {
            showChildren: false,
            data: null,
            showContext: false
        };
        return _this2;
    }

    _createClass(IndicatorFolderNode, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._onBodyClick);
            ewars.subscribe("RELOAD_INDICATORS", this._reload);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._onBodyClick);
            ewars.g["FOLDER_" + this.props.data.id] = ewars.copy(this.state);
        }
    }, {
        key: "render",
        value: function render() {

            var childs = void 0;

            if (this.state.data) {
                childs = this.state.data.map(function (item) {
                    if (item.context == "INDICATOR") {
                        return React.createElement(IndicatorNode, _extends({}, this.props, {
                            key: "IND_" + item.uuid,
                            data: item }));
                    }

                    if (item.context == "FOLDER") {
                        return React.createElement(IndicatorFolderNode, _extends({}, this.props, {
                            key: "FOLDER_" + item.id,
                            data: item }));
                    }
                }.bind(this));
            }

            var icon = "fa";
            if (this.state.showChildren) icon += " fa-folder-open-o";
            if (!this.state.showChildren) icon += " fa-folder-o";

            var label = ewars.I18N(this.props.data.name);

            return React.createElement(
                "div",
                { className: "block", ref: "item" },
                React.createElement(
                    "div",
                    { className: "block-content", onDragStart: this._dragStart, draggable: true,
                        onContextMenu: this._onContext },
                    React.createElement(
                        "div",
                        { className: "ide-row", onClick: this._onCaretClick },
                        React.createElement(
                            "div",
                            { className: "ide-col", style: { maxWidth: 25 } },
                            React.createElement("i", { className: icon, ref: "iconHandle" })
                        ),
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            label
                        )
                    ),
                    this.props.actions && this.state.showContext ? React.createElement(_ContextMenu2.default, {
                        absolute: true,
                        onClick: this._onContextAction,
                        actions: this.props.actions }) : null
                ),
                this.state.showChildren ? React.createElement(
                    "div",
                    { className: "block-children" },
                    childs
                ) : null
            );
        }
    }]);

    return IndicatorFolderNode;
}(React.Component);

var TreeNodeComponent = React.createClass({
    displayName: "TreeNodeComponent",

    _hasLoaded: false,

    getDefaultProps: function getDefaultProps() {
        return {
            dark: false,
            hideInactive: true
        };
    },

    getInitialState: function getInitialState() {
        return {
            children: [],
            showChildren: false,
            showContext: false
        };
    },

    componentWillMount: function componentWillMount() {
        window.__hack__.addEventListener("click", this._onBodyClick);
        ewars.subscribe("RELOAD_INDICATORS", this._reload);
    },

    componentWillUnmount: function componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._onBodyClick);
    },

    _onBodyClick: function _onBodyClick(evt) {
        if (!evt.contains(this.refs.item)) {
            this.setState({
                showContext: false
            });
        }
    },

    _onContext: function _onContext(e) {
        e.preventDefault();
        this.setState({
            showContext: true
        });
    },

    _onContextAction: function _onContextAction(action) {
        this.props.onAction(action, this.props.data);
    },

    _reload: function _reload() {
        if (this._hasLoaded && this.props.type != "INDICATOR") {
            // Get groups first

            ewars.tx("com.ewars.indicators", [this.props.data.id]).then(function (resp) {
                this._hasLoaded = true;
                this.setState({
                    children: resp,
                    showChildren: true
                });
            }.bind(this));
        }
    },

    _onClick: function _onClick() {
        if (this.state.showChildren) {
            this.setState({
                showChildren: false
            });
            return;
        }

        if (!this._hasLoaded) {
            ewars.tx("com.ewars.indicators", [this.props.data.id]).then(function (resp) {
                this._hasLoaded = true;
                this.setState({
                    children: resp,
                    showChildren: true
                });
            }.bind(this));
        } else {

            this.setState({
                showChildren: true
            });
        }
    },

    _hasChildren: function _hasChildren() {
        return true;
    },

    _onEdit: function _onEdit() {
        this.props.onEdit(null, this.props.data);
    },

    render: function render() {
        var className = "block";

        var label = ewars.I18N(this.props.data.name);

        var showSelect = false;

        var caret = void 0;
        if (["SYSTEM", "FOLDER"].indexOf(this.props.data.context) >= 0) {
            if (this.state.showChildren) caret = "fa fa-folder-open-o";
            if (!this.state.showChildren) caret = "fa fa-folder-o";
        }

        var children = void 0;

        if (this.state.showChildren && this._hasLoaded) {
            children = [];
            this.state.children.forEach(function (item) {
                children.push(React.createElement(TreeNodeComponent, {
                    key: item.uuid || "FOLDER-" + item.id,
                    actions: this.props.actions,
                    onSelect: this.props.onSelect,
                    data: item }));
            }.bind(this));
        }

        if (this.state.showChildren && !this._hasLoaded) {
            children = React.createElement(Spinner, null);
        }

        if (this.props.data.context == "INDICATOR") showSelect = true;

        var handleStyle = { flexGrow: 1, flexBasis: 0 };

        return React.createElement(
            "div",
            { className: className, ref: "item" },
            React.createElement(
                "div",
                { className: "block-content", onClick: this._onClick, onContextMenu: this._onContext },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    caret ? React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 25 } },
                        React.createElement("i", { className: caret })
                    ) : null,
                    React.createElement(
                        "div",
                        { className: "ide-col", style: handleStyle },
                        label
                    )
                ),
                this.props.actions && this.state.showContext ? React.createElement(_ContextMenu2.default, {
                    absolute: true,
                    onClick: this._onContextAction,
                    actions: this.props.actions }) : null
            ),
            this.state.showChildren ? React.createElement(
                "div",
                { className: "block-children" },
                children
            ) : null
        );
    }
});

var IndicatorTreeView = function (_React$Component3) {
    _inherits(IndicatorTreeView, _React$Component3);

    function IndicatorTreeView(props) {
        _classCallCheck(this, IndicatorTreeView);

        var _this3 = _possibleConstructorReturn(this, (IndicatorTreeView.__proto__ || Object.getPrototypeOf(IndicatorTreeView)).call(this, props));

        _this3._init = function () {
            ewars.tx("com.ewars.indicators", [null]).then(function (resp) {
                this.setState({
                    data: resp
                });
                ewars.emit("RELOAD_CHILDS");
            }.bind(_this3));
        };

        _this3._handleSearchChange = function (e) {
            var val = e.target.value;
            _this3.setState({
                search: val
            });

            if (val.length > 3) {
                _this3._search(e.target.value);
            } else {
                _this3._init();
            }
        };

        _this3._search = function (val) {
            ewars.tx("com.ewars.query", ["indicator", null, { "name.en": { like: val } }, null, null, null, null]).then(function (resp) {
                _this3.setState({
                    data: resp
                });
            });
        };

        _this3._clearSearch = function () {
            _this3.setState({
                search: ""
            });

            _this3._init();
        };

        _this3._onItemAction = function (action, data) {
            _this3.props.onAction(action, data);
        };

        _this3.state = {
            search: "",
            data: []
        };
        return _this3;
    }

    _createClass(IndicatorTreeView, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            this._init();
            ewars.subscribe("RELOAD_INDICATORS", this._init);
        }
    }, {
        key: "render",
        value: function render() {
            var _this4 = this;

            var nodes = this.state.data.map(function (item) {
                if (_this4.state.search != "" && _this4.state.search != null) {
                    return React.createElement(IndicatorNode, {
                        key: item.uuid || "FOLDER-" + item.id,
                        actions: _this4.props.actions,
                        onEdit: _this4.props.onEdit,
                        onAction: _this4._onItemAction,
                        data: item });
                } else {
                    return React.createElement(IndicatorFolderNode, _extends({
                        key: "FOLDER_" + item.id
                    }, _this4.props, {
                        onAction: _this4._onItemAction,
                        data: item }));
                }
            });

            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Row,
                    { height: 45 },
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            "div",
                            { className: "search" },
                            React.createElement(
                                "div",
                                { className: "search-inner" },
                                React.createElement(
                                    ewars.d.Row,
                                    null,
                                    React.createElement(
                                        ewars.d.Cell,
                                        { addClass: "search-left", width: 25 },
                                        React.createElement("i", { className: "fa fa-search" })
                                    ),
                                    React.createElement(
                                        ewars.d.Cell,
                                        { addClass: "search-mid" },
                                        React.createElement("input", { type: "text", value: this.state.search,
                                            onChange: this._handleSearchChange })
                                    ),
                                    this.state.search != "" ? React.createElement(
                                        ewars.d.Cell,
                                        { addClass: "search-right", width: 25,
                                            onClick: this._clearSearch },
                                        React.createElement("i", { className: "fa fa-close" })
                                    ) : null
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            "div",
                            { className: "ide-panel ide-panel-absolute ide-scroll" },
                            React.createElement(
                                "div",
                                { className: "block-tree" },
                                nodes
                            )
                        )
                    )
                )
            );
        }
    }]);

    return IndicatorTreeView;
}(React.Component);

IndicatorTreeView.defaultProps = {
    allowCheck: false,
    checked: [],
    value: null,
    hideInactive: true
};
exports.default = IndicatorTreeView;


},{"../../common/ContextMenu.react":8}],83:[function(require,module,exports){

/**
 * @license
 *
 * chroma.js - JavaScript library for color conversions
 * 
 * Copyright (c) 2011-2017, Gregor Aisch
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. The name Gregor Aisch may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL GREGOR AISCH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

(function() {
  var Color, DEG2RAD, LAB_CONSTANTS, PI, PITHIRD, RAD2DEG, TWOPI, _guess_formats, _guess_formats_sorted, _input, _interpolators, abs, atan2, bezier, blend, blend_f, brewer, burn, chroma, clip_rgb, cmyk2rgb, colors, cos, css2rgb, darken, dodge, each, floor, hcg2rgb, hex2rgb, hsi2rgb, hsl2css, hsl2rgb, hsv2rgb, interpolate, interpolate_hsx, interpolate_lab, interpolate_num, interpolate_rgb, lab2lch, lab2rgb, lab_xyz, lch2lab, lch2rgb, lighten, limit, log, luminance_x, m, max, multiply, normal, num2rgb, overlay, pow, rgb2cmyk, rgb2css, rgb2hcg, rgb2hex, rgb2hsi, rgb2hsl, rgb2hsv, rgb2lab, rgb2lch, rgb2luminance, rgb2num, rgb2temperature, rgb2xyz, rgb_xyz, rnd, root, round, screen, sin, sqrt, temperature2rgb, type, unpack, w3cx11, xyz_lab, xyz_rgb,
    slice = [].slice;

  type = (function() {

    /*
    for browser-safe type checking+
    ported from jQuery's $.type
     */
    var classToType, len, name, o, ref;
    classToType = {};
    ref = "Boolean Number String Function Array Date RegExp Undefined Null".split(" ");
    for (o = 0, len = ref.length; o < len; o++) {
      name = ref[o];
      classToType["[object " + name + "]"] = name.toLowerCase();
    }
    return function(obj) {
      var strType;
      strType = Object.prototype.toString.call(obj);
      return classToType[strType] || "object";
    };
  })();

  limit = function(x, min, max) {
    if (min == null) {
      min = 0;
    }
    if (max == null) {
      max = 1;
    }
    if (x < min) {
      x = min;
    }
    if (x > max) {
      x = max;
    }
    return x;
  };

  unpack = function(args) {
    if (args.length >= 3) {
      return [].slice.call(args);
    } else {
      return args[0];
    }
  };

  clip_rgb = function(rgb) {
    var i, o;
    rgb._clipped = false;
    rgb._unclipped = rgb.slice(0);
    for (i = o = 0; o < 3; i = ++o) {
      if (i < 3) {
        if (rgb[i] < 0 || rgb[i] > 255) {
          rgb._clipped = true;
        }
        if (rgb[i] < 0) {
          rgb[i] = 0;
        }
        if (rgb[i] > 255) {
          rgb[i] = 255;
        }
      } else if (i === 3) {
        if (rgb[i] < 0) {
          rgb[i] = 0;
        }
        if (rgb[i] > 1) {
          rgb[i] = 1;
        }
      }
    }
    if (!rgb._clipped) {
      delete rgb._unclipped;
    }
    return rgb;
  };

  PI = Math.PI, round = Math.round, cos = Math.cos, floor = Math.floor, pow = Math.pow, log = Math.log, sin = Math.sin, sqrt = Math.sqrt, atan2 = Math.atan2, max = Math.max, abs = Math.abs;

  TWOPI = PI * 2;

  PITHIRD = PI / 3;

  DEG2RAD = PI / 180;

  RAD2DEG = 180 / PI;

  chroma = function() {
    if (arguments[0] instanceof Color) {
      return arguments[0];
    }
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, arguments, function(){});
  };

  _interpolators = [];

  if ((typeof module !== "undefined" && module !== null) && (module.exports != null)) {
    module.exports = chroma;
  }

  if (typeof define === 'function' && define.amd) {
    define([], function() {
      return chroma;
    });
  } else {
    root = typeof exports !== "undefined" && exports !== null ? exports : this;
    root.chroma = chroma;
  }

  chroma.version = '1.3.3';

  _input = {};

  _guess_formats = [];

  _guess_formats_sorted = false;

  Color = (function() {
    function Color() {
      var arg, args, chk, len, len1, me, mode, o, w;
      me = this;
      args = [];
      for (o = 0, len = arguments.length; o < len; o++) {
        arg = arguments[o];
        if (arg != null) {
          args.push(arg);
        }
      }
      mode = args[args.length - 1];
      if (_input[mode] != null) {
        me._rgb = clip_rgb(_input[mode](unpack(args.slice(0, -1))));
      } else {
        if (!_guess_formats_sorted) {
          _guess_formats = _guess_formats.sort(function(a, b) {
            return b.p - a.p;
          });
          _guess_formats_sorted = true;
        }
        for (w = 0, len1 = _guess_formats.length; w < len1; w++) {
          chk = _guess_formats[w];
          mode = chk.test.apply(chk, args);
          if (mode) {
            break;
          }
        }
        if (mode) {
          me._rgb = clip_rgb(_input[mode].apply(_input, args));
        }
      }
      if (me._rgb == null) {
        console.warn('unknown format: ' + args);
      }
      if (me._rgb == null) {
        me._rgb = [0, 0, 0];
      }
      if (me._rgb.length === 3) {
        me._rgb.push(1);
      }
    }

    Color.prototype.toString = function() {
      return this.hex();
    };

    return Color;

  })();

  chroma._input = _input;


  /**
  	ColorBrewer colors for chroma.js
  
  	Copyright (c) 2002 Cynthia Brewer, Mark Harrower, and The 
  	Pennsylvania State University.
  
  	Licensed under the Apache License, Version 2.0 (the "License"); 
  	you may not use this file except in compliance with the License.
  	You may obtain a copy of the License at	
  	http://www.apache.org/licenses/LICENSE-2.0
  
  	Unless required by applicable law or agreed to in writing, software distributed
  	under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
  	CONDITIONS OF ANY KIND, either express or implied. See the License for the
  	specific language governing permissions and limitations under the License.
  
      @preserve
   */

  chroma.brewer = brewer = {
    OrRd: ['#fff7ec', '#fee8c8', '#fdd49e', '#fdbb84', '#fc8d59', '#ef6548', '#d7301f', '#b30000', '#7f0000'],
    PuBu: ['#fff7fb', '#ece7f2', '#d0d1e6', '#a6bddb', '#74a9cf', '#3690c0', '#0570b0', '#045a8d', '#023858'],
    BuPu: ['#f7fcfd', '#e0ecf4', '#bfd3e6', '#9ebcda', '#8c96c6', '#8c6bb1', '#88419d', '#810f7c', '#4d004b'],
    Oranges: ['#fff5eb', '#fee6ce', '#fdd0a2', '#fdae6b', '#fd8d3c', '#f16913', '#d94801', '#a63603', '#7f2704'],
    BuGn: ['#f7fcfd', '#e5f5f9', '#ccece6', '#99d8c9', '#66c2a4', '#41ae76', '#238b45', '#006d2c', '#00441b'],
    YlOrBr: ['#ffffe5', '#fff7bc', '#fee391', '#fec44f', '#fe9929', '#ec7014', '#cc4c02', '#993404', '#662506'],
    YlGn: ['#ffffe5', '#f7fcb9', '#d9f0a3', '#addd8e', '#78c679', '#41ab5d', '#238443', '#006837', '#004529'],
    Reds: ['#fff5f0', '#fee0d2', '#fcbba1', '#fc9272', '#fb6a4a', '#ef3b2c', '#cb181d', '#a50f15', '#67000d'],
    RdPu: ['#fff7f3', '#fde0dd', '#fcc5c0', '#fa9fb5', '#f768a1', '#dd3497', '#ae017e', '#7a0177', '#49006a'],
    Greens: ['#f7fcf5', '#e5f5e0', '#c7e9c0', '#a1d99b', '#74c476', '#41ab5d', '#238b45', '#006d2c', '#00441b'],
    YlGnBu: ['#ffffd9', '#edf8b1', '#c7e9b4', '#7fcdbb', '#41b6c4', '#1d91c0', '#225ea8', '#253494', '#081d58'],
    Purples: ['#fcfbfd', '#efedf5', '#dadaeb', '#bcbddc', '#9e9ac8', '#807dba', '#6a51a3', '#54278f', '#3f007d'],
    GnBu: ['#f7fcf0', '#e0f3db', '#ccebc5', '#a8ddb5', '#7bccc4', '#4eb3d3', '#2b8cbe', '#0868ac', '#084081'],
    Greys: ['#ffffff', '#f0f0f0', '#d9d9d9', '#bdbdbd', '#969696', '#737373', '#525252', '#252525', '#000000'],
    YlOrRd: ['#ffffcc', '#ffeda0', '#fed976', '#feb24c', '#fd8d3c', '#fc4e2a', '#e31a1c', '#bd0026', '#800026'],
    PuRd: ['#f7f4f9', '#e7e1ef', '#d4b9da', '#c994c7', '#df65b0', '#e7298a', '#ce1256', '#980043', '#67001f'],
    Blues: ['#f7fbff', '#deebf7', '#c6dbef', '#9ecae1', '#6baed6', '#4292c6', '#2171b5', '#08519c', '#08306b'],
    PuBuGn: ['#fff7fb', '#ece2f0', '#d0d1e6', '#a6bddb', '#67a9cf', '#3690c0', '#02818a', '#016c59', '#014636'],
    Viridis: ['#440154', '#482777', '#3f4a8a', '#31678e', '#26838f', '#1f9d8a', '#6cce5a', '#b6de2b', '#fee825'],
    Spectral: ['#9e0142', '#d53e4f', '#f46d43', '#fdae61', '#fee08b', '#ffffbf', '#e6f598', '#abdda4', '#66c2a5', '#3288bd', '#5e4fa2'],
    RdYlGn: ['#a50026', '#d73027', '#f46d43', '#fdae61', '#fee08b', '#ffffbf', '#d9ef8b', '#a6d96a', '#66bd63', '#1a9850', '#006837'],
    RdBu: ['#67001f', '#b2182b', '#d6604d', '#f4a582', '#fddbc7', '#f7f7f7', '#d1e5f0', '#92c5de', '#4393c3', '#2166ac', '#053061'],
    PiYG: ['#8e0152', '#c51b7d', '#de77ae', '#f1b6da', '#fde0ef', '#f7f7f7', '#e6f5d0', '#b8e186', '#7fbc41', '#4d9221', '#276419'],
    PRGn: ['#40004b', '#762a83', '#9970ab', '#c2a5cf', '#e7d4e8', '#f7f7f7', '#d9f0d3', '#a6dba0', '#5aae61', '#1b7837', '#00441b'],
    RdYlBu: ['#a50026', '#d73027', '#f46d43', '#fdae61', '#fee090', '#ffffbf', '#e0f3f8', '#abd9e9', '#74add1', '#4575b4', '#313695'],
    BrBG: ['#543005', '#8c510a', '#bf812d', '#dfc27d', '#f6e8c3', '#f5f5f5', '#c7eae5', '#80cdc1', '#35978f', '#01665e', '#003c30'],
    RdGy: ['#67001f', '#b2182b', '#d6604d', '#f4a582', '#fddbc7', '#ffffff', '#e0e0e0', '#bababa', '#878787', '#4d4d4d', '#1a1a1a'],
    PuOr: ['#7f3b08', '#b35806', '#e08214', '#fdb863', '#fee0b6', '#f7f7f7', '#d8daeb', '#b2abd2', '#8073ac', '#542788', '#2d004b'],
    Set2: ['#66c2a5', '#fc8d62', '#8da0cb', '#e78ac3', '#a6d854', '#ffd92f', '#e5c494', '#b3b3b3'],
    Accent: ['#7fc97f', '#beaed4', '#fdc086', '#ffff99', '#386cb0', '#f0027f', '#bf5b17', '#666666'],
    Set1: ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00', '#ffff33', '#a65628', '#f781bf', '#999999'],
    Set3: ['#8dd3c7', '#ffffb3', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5', '#d9d9d9', '#bc80bd', '#ccebc5', '#ffed6f'],
    Dark2: ['#1b9e77', '#d95f02', '#7570b3', '#e7298a', '#66a61e', '#e6ab02', '#a6761d', '#666666'],
    Paired: ['#a6cee3', '#1f78b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c', '#fdbf6f', '#ff7f00', '#cab2d6', '#6a3d9a', '#ffff99', '#b15928'],
    Pastel2: ['#b3e2cd', '#fdcdac', '#cbd5e8', '#f4cae4', '#e6f5c9', '#fff2ae', '#f1e2cc', '#cccccc'],
    Pastel1: ['#fbb4ae', '#b3cde3', '#ccebc5', '#decbe4', '#fed9a6', '#ffffcc', '#e5d8bd', '#fddaec', '#f2f2f2']
  };

  (function() {
    var key, results;
    results = [];
    for (key in brewer) {
      results.push(brewer[key.toLowerCase()] = brewer[key]);
    }
    return results;
  })();


  /**
  	X11 color names
  
  	http://www.w3.org/TR/css3-color/#svg-color
   */

  w3cx11 = {
    aliceblue: '#f0f8ff',
    antiquewhite: '#faebd7',
    aqua: '#00ffff',
    aquamarine: '#7fffd4',
    azure: '#f0ffff',
    beige: '#f5f5dc',
    bisque: '#ffe4c4',
    black: '#000000',
    blanchedalmond: '#ffebcd',
    blue: '#0000ff',
    blueviolet: '#8a2be2',
    brown: '#a52a2a',
    burlywood: '#deb887',
    cadetblue: '#5f9ea0',
    chartreuse: '#7fff00',
    chocolate: '#d2691e',
    coral: '#ff7f50',
    cornflower: '#6495ed',
    cornflowerblue: '#6495ed',
    cornsilk: '#fff8dc',
    crimson: '#dc143c',
    cyan: '#00ffff',
    darkblue: '#00008b',
    darkcyan: '#008b8b',
    darkgoldenrod: '#b8860b',
    darkgray: '#a9a9a9',
    darkgreen: '#006400',
    darkgrey: '#a9a9a9',
    darkkhaki: '#bdb76b',
    darkmagenta: '#8b008b',
    darkolivegreen: '#556b2f',
    darkorange: '#ff8c00',
    darkorchid: '#9932cc',
    darkred: '#8b0000',
    darksalmon: '#e9967a',
    darkseagreen: '#8fbc8f',
    darkslateblue: '#483d8b',
    darkslategray: '#2f4f4f',
    darkslategrey: '#2f4f4f',
    darkturquoise: '#00ced1',
    darkviolet: '#9400d3',
    deeppink: '#ff1493',
    deepskyblue: '#00bfff',
    dimgray: '#696969',
    dimgrey: '#696969',
    dodgerblue: '#1e90ff',
    firebrick: '#b22222',
    floralwhite: '#fffaf0',
    forestgreen: '#228b22',
    fuchsia: '#ff00ff',
    gainsboro: '#dcdcdc',
    ghostwhite: '#f8f8ff',
    gold: '#ffd700',
    goldenrod: '#daa520',
    gray: '#808080',
    green: '#008000',
    greenyellow: '#adff2f',
    grey: '#808080',
    honeydew: '#f0fff0',
    hotpink: '#ff69b4',
    indianred: '#cd5c5c',
    indigo: '#4b0082',
    ivory: '#fffff0',
    khaki: '#f0e68c',
    laserlemon: '#ffff54',
    lavender: '#e6e6fa',
    lavenderblush: '#fff0f5',
    lawngreen: '#7cfc00',
    lemonchiffon: '#fffacd',
    lightblue: '#add8e6',
    lightcoral: '#f08080',
    lightcyan: '#e0ffff',
    lightgoldenrod: '#fafad2',
    lightgoldenrodyellow: '#fafad2',
    lightgray: '#d3d3d3',
    lightgreen: '#90ee90',
    lightgrey: '#d3d3d3',
    lightpink: '#ffb6c1',
    lightsalmon: '#ffa07a',
    lightseagreen: '#20b2aa',
    lightskyblue: '#87cefa',
    lightslategray: '#778899',
    lightslategrey: '#778899',
    lightsteelblue: '#b0c4de',
    lightyellow: '#ffffe0',
    lime: '#00ff00',
    limegreen: '#32cd32',
    linen: '#faf0e6',
    magenta: '#ff00ff',
    maroon: '#800000',
    maroon2: '#7f0000',
    maroon3: '#b03060',
    mediumaquamarine: '#66cdaa',
    mediumblue: '#0000cd',
    mediumorchid: '#ba55d3',
    mediumpurple: '#9370db',
    mediumseagreen: '#3cb371',
    mediumslateblue: '#7b68ee',
    mediumspringgreen: '#00fa9a',
    mediumturquoise: '#48d1cc',
    mediumvioletred: '#c71585',
    midnightblue: '#191970',
    mintcream: '#f5fffa',
    mistyrose: '#ffe4e1',
    moccasin: '#ffe4b5',
    navajowhite: '#ffdead',
    navy: '#000080',
    oldlace: '#fdf5e6',
    olive: '#808000',
    olivedrab: '#6b8e23',
    orange: '#ffa500',
    orangered: '#ff4500',
    orchid: '#da70d6',
    palegoldenrod: '#eee8aa',
    palegreen: '#98fb98',
    paleturquoise: '#afeeee',
    palevioletred: '#db7093',
    papayawhip: '#ffefd5',
    peachpuff: '#ffdab9',
    peru: '#cd853f',
    pink: '#ffc0cb',
    plum: '#dda0dd',
    powderblue: '#b0e0e6',
    purple: '#800080',
    purple2: '#7f007f',
    purple3: '#a020f0',
    rebeccapurple: '#663399',
    red: '#ff0000',
    rosybrown: '#bc8f8f',
    royalblue: '#4169e1',
    saddlebrown: '#8b4513',
    salmon: '#fa8072',
    sandybrown: '#f4a460',
    seagreen: '#2e8b57',
    seashell: '#fff5ee',
    sienna: '#a0522d',
    silver: '#c0c0c0',
    skyblue: '#87ceeb',
    slateblue: '#6a5acd',
    slategray: '#708090',
    slategrey: '#708090',
    snow: '#fffafa',
    springgreen: '#00ff7f',
    steelblue: '#4682b4',
    tan: '#d2b48c',
    teal: '#008080',
    thistle: '#d8bfd8',
    tomato: '#ff6347',
    turquoise: '#40e0d0',
    violet: '#ee82ee',
    wheat: '#f5deb3',
    white: '#ffffff',
    whitesmoke: '#f5f5f5',
    yellow: '#ffff00',
    yellowgreen: '#9acd32'
  };

  chroma.colors = colors = w3cx11;

  lab2rgb = function() {
    var a, args, b, g, l, r, x, y, z;
    args = unpack(arguments);
    l = args[0], a = args[1], b = args[2];
    y = (l + 16) / 116;
    x = isNaN(a) ? y : y + a / 500;
    z = isNaN(b) ? y : y - b / 200;
    y = LAB_CONSTANTS.Yn * lab_xyz(y);
    x = LAB_CONSTANTS.Xn * lab_xyz(x);
    z = LAB_CONSTANTS.Zn * lab_xyz(z);
    r = xyz_rgb(3.2404542 * x - 1.5371385 * y - 0.4985314 * z);
    g = xyz_rgb(-0.9692660 * x + 1.8760108 * y + 0.0415560 * z);
    b = xyz_rgb(0.0556434 * x - 0.2040259 * y + 1.0572252 * z);
    return [r, g, b, args.length > 3 ? args[3] : 1];
  };

  xyz_rgb = function(r) {
    return 255 * (r <= 0.00304 ? 12.92 * r : 1.055 * pow(r, 1 / 2.4) - 0.055);
  };

  lab_xyz = function(t) {
    if (t > LAB_CONSTANTS.t1) {
      return t * t * t;
    } else {
      return LAB_CONSTANTS.t2 * (t - LAB_CONSTANTS.t0);
    }
  };

  LAB_CONSTANTS = {
    Kn: 18,
    Xn: 0.950470,
    Yn: 1,
    Zn: 1.088830,
    t0: 0.137931034,
    t1: 0.206896552,
    t2: 0.12841855,
    t3: 0.008856452
  };

  rgb2lab = function() {
    var b, g, r, ref, ref1, x, y, z;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    ref1 = rgb2xyz(r, g, b), x = ref1[0], y = ref1[1], z = ref1[2];
    return [116 * y - 16, 500 * (x - y), 200 * (y - z)];
  };

  rgb_xyz = function(r) {
    if ((r /= 255) <= 0.04045) {
      return r / 12.92;
    } else {
      return pow((r + 0.055) / 1.055, 2.4);
    }
  };

  xyz_lab = function(t) {
    if (t > LAB_CONSTANTS.t3) {
      return pow(t, 1 / 3);
    } else {
      return t / LAB_CONSTANTS.t2 + LAB_CONSTANTS.t0;
    }
  };

  rgb2xyz = function() {
    var b, g, r, ref, x, y, z;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    r = rgb_xyz(r);
    g = rgb_xyz(g);
    b = rgb_xyz(b);
    x = xyz_lab((0.4124564 * r + 0.3575761 * g + 0.1804375 * b) / LAB_CONSTANTS.Xn);
    y = xyz_lab((0.2126729 * r + 0.7151522 * g + 0.0721750 * b) / LAB_CONSTANTS.Yn);
    z = xyz_lab((0.0193339 * r + 0.1191920 * g + 0.9503041 * b) / LAB_CONSTANTS.Zn);
    return [x, y, z];
  };

  chroma.lab = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['lab']), function(){});
  };

  _input.lab = lab2rgb;

  Color.prototype.lab = function() {
    return rgb2lab(this._rgb);
  };

  bezier = function(colors) {
    var I, I0, I1, c, lab0, lab1, lab2, lab3, ref, ref1, ref2;
    colors = (function() {
      var len, o, results;
      results = [];
      for (o = 0, len = colors.length; o < len; o++) {
        c = colors[o];
        results.push(chroma(c));
      }
      return results;
    })();
    if (colors.length === 2) {
      ref = (function() {
        var len, o, results;
        results = [];
        for (o = 0, len = colors.length; o < len; o++) {
          c = colors[o];
          results.push(c.lab());
        }
        return results;
      })(), lab0 = ref[0], lab1 = ref[1];
      I = function(t) {
        var i, lab;
        lab = (function() {
          var o, results;
          results = [];
          for (i = o = 0; o <= 2; i = ++o) {
            results.push(lab0[i] + t * (lab1[i] - lab0[i]));
          }
          return results;
        })();
        return chroma.lab.apply(chroma, lab);
      };
    } else if (colors.length === 3) {
      ref1 = (function() {
        var len, o, results;
        results = [];
        for (o = 0, len = colors.length; o < len; o++) {
          c = colors[o];
          results.push(c.lab());
        }
        return results;
      })(), lab0 = ref1[0], lab1 = ref1[1], lab2 = ref1[2];
      I = function(t) {
        var i, lab;
        lab = (function() {
          var o, results;
          results = [];
          for (i = o = 0; o <= 2; i = ++o) {
            results.push((1 - t) * (1 - t) * lab0[i] + 2 * (1 - t) * t * lab1[i] + t * t * lab2[i]);
          }
          return results;
        })();
        return chroma.lab.apply(chroma, lab);
      };
    } else if (colors.length === 4) {
      ref2 = (function() {
        var len, o, results;
        results = [];
        for (o = 0, len = colors.length; o < len; o++) {
          c = colors[o];
          results.push(c.lab());
        }
        return results;
      })(), lab0 = ref2[0], lab1 = ref2[1], lab2 = ref2[2], lab3 = ref2[3];
      I = function(t) {
        var i, lab;
        lab = (function() {
          var o, results;
          results = [];
          for (i = o = 0; o <= 2; i = ++o) {
            results.push((1 - t) * (1 - t) * (1 - t) * lab0[i] + 3 * (1 - t) * (1 - t) * t * lab1[i] + 3 * (1 - t) * t * t * lab2[i] + t * t * t * lab3[i]);
          }
          return results;
        })();
        return chroma.lab.apply(chroma, lab);
      };
    } else if (colors.length === 5) {
      I0 = bezier(colors.slice(0, 3));
      I1 = bezier(colors.slice(2, 5));
      I = function(t) {
        if (t < 0.5) {
          return I0(t * 2);
        } else {
          return I1((t - 0.5) * 2);
        }
      };
    }
    return I;
  };

  chroma.bezier = function(colors) {
    var f;
    f = bezier(colors);
    f.scale = function() {
      return chroma.scale(f);
    };
    return f;
  };


  /*
      chroma.js
  
      Copyright (c) 2011-2013, Gregor Aisch
      All rights reserved.
  
      Redistribution and use in source and binary forms, with or without
      modification, are permitted provided that the following conditions are met:
  
      * Redistributions of source code must retain the above copyright notice, this
        list of conditions and the following disclaimer.
  
      * Redistributions in binary form must reproduce the above copyright notice,
        this list of conditions and the following disclaimer in the documentation
        and/or other materials provided with the distribution.
  
      * The name Gregor Aisch may not be used to endorse or promote products
        derived from this software without specific prior written permission.
  
      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
      AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
      IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED. IN NO EVENT SHALL GREGOR AISCH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
      INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
      BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
      DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
      OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
      NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
      EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
      @source: https://github.com/gka/chroma.js
   */

  chroma.cubehelix = function(start, rotations, hue, gamma, lightness) {
    var dh, dl, f;
    if (start == null) {
      start = 300;
    }
    if (rotations == null) {
      rotations = -1.5;
    }
    if (hue == null) {
      hue = 1;
    }
    if (gamma == null) {
      gamma = 1;
    }
    if (lightness == null) {
      lightness = [0, 1];
    }
    dh = 0;
    if (type(lightness) === 'array') {
      dl = lightness[1] - lightness[0];
    } else {
      dl = 0;
      lightness = [lightness, lightness];
    }
    f = function(fract) {
      var a, amp, b, cos_a, g, h, l, r, sin_a;
      a = TWOPI * ((start + 120) / 360 + rotations * fract);
      l = pow(lightness[0] + dl * fract, gamma);
      h = dh !== 0 ? hue[0] + fract * dh : hue;
      amp = h * l * (1 - l) / 2;
      cos_a = cos(a);
      sin_a = sin(a);
      r = l + amp * (-0.14861 * cos_a + 1.78277 * sin_a);
      g = l + amp * (-0.29227 * cos_a - 0.90649 * sin_a);
      b = l + amp * (+1.97294 * cos_a);
      return chroma(clip_rgb([r * 255, g * 255, b * 255]));
    };
    f.start = function(s) {
      if (s == null) {
        return start;
      }
      start = s;
      return f;
    };
    f.rotations = function(r) {
      if (r == null) {
        return rotations;
      }
      rotations = r;
      return f;
    };
    f.gamma = function(g) {
      if (g == null) {
        return gamma;
      }
      gamma = g;
      return f;
    };
    f.hue = function(h) {
      if (h == null) {
        return hue;
      }
      hue = h;
      if (type(hue) === 'array') {
        dh = hue[1] - hue[0];
        if (dh === 0) {
          hue = hue[1];
        }
      } else {
        dh = 0;
      }
      return f;
    };
    f.lightness = function(h) {
      if (h == null) {
        return lightness;
      }
      if (type(h) === 'array') {
        lightness = h;
        dl = h[1] - h[0];
      } else {
        lightness = [h, h];
        dl = 0;
      }
      return f;
    };
    f.scale = function() {
      return chroma.scale(f);
    };
    f.hue(hue);
    return f;
  };

  chroma.random = function() {
    var code, digits, i, o;
    digits = '0123456789abcdef';
    code = '#';
    for (i = o = 0; o < 6; i = ++o) {
      code += digits.charAt(floor(Math.random() * 16));
    }
    return new Color(code);
  };

  chroma.average = function(colors, mode) {
    var A, alpha, c, cnt, dx, dy, first, i, l, len, o, xyz, xyz2;
    if (mode == null) {
      mode = 'rgb';
    }
    l = colors.length;
    colors = colors.map(function(c) {
      return chroma(c);
    });
    first = colors.splice(0, 1)[0];
    xyz = first.get(mode);
    cnt = [];
    dx = 0;
    dy = 0;
    for (i in xyz) {
      xyz[i] = xyz[i] || 0;
      cnt.push(!isNaN(xyz[i]) ? 1 : 0);
      if (mode.charAt(i) === 'h' && !isNaN(xyz[i])) {
        A = xyz[i] / 180 * PI;
        dx += cos(A);
        dy += sin(A);
      }
    }
    alpha = first.alpha();
    for (o = 0, len = colors.length; o < len; o++) {
      c = colors[o];
      xyz2 = c.get(mode);
      alpha += c.alpha();
      for (i in xyz) {
        if (!isNaN(xyz2[i])) {
          xyz[i] += xyz2[i];
          cnt[i] += 1;
          if (mode.charAt(i) === 'h') {
            A = xyz[i] / 180 * PI;
            dx += cos(A);
            dy += sin(A);
          }
        }
      }
    }
    for (i in xyz) {
      xyz[i] = xyz[i] / cnt[i];
      if (mode.charAt(i) === 'h') {
        A = atan2(dy / cnt[i], dx / cnt[i]) / PI * 180;
        while (A < 0) {
          A += 360;
        }
        while (A >= 360) {
          A -= 360;
        }
        xyz[i] = A;
      }
    }
    return chroma(xyz, mode).alpha(alpha / l);
  };

  _input.rgb = function() {
    var k, ref, results, v;
    ref = unpack(arguments);
    results = [];
    for (k in ref) {
      v = ref[k];
      results.push(v);
    }
    return results;
  };

  chroma.rgb = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['rgb']), function(){});
  };

  Color.prototype.rgb = function(round) {
    if (round == null) {
      round = true;
    }
    if (round) {
      return this._rgb.map(Math.round).slice(0, 3);
    } else {
      return this._rgb.slice(0, 3);
    }
  };

  Color.prototype.rgba = function(round) {
    if (round == null) {
      round = true;
    }
    if (!round) {
      return this._rgb.slice(0);
    }
    return [Math.round(this._rgb[0]), Math.round(this._rgb[1]), Math.round(this._rgb[2]), this._rgb[3]];
  };

  _guess_formats.push({
    p: 3,
    test: function(n) {
      var a;
      a = unpack(arguments);
      if (type(a) === 'array' && a.length === 3) {
        return 'rgb';
      }
      if (a.length === 4 && type(a[3]) === "number" && a[3] >= 0 && a[3] <= 1) {
        return 'rgb';
      }
    }
  });

  hex2rgb = function(hex) {
    var a, b, g, r, rgb, u;
    if (hex.match(/^#?([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/)) {
      if (hex.length === 4 || hex.length === 7) {
        hex = hex.substr(1);
      }
      if (hex.length === 3) {
        hex = hex.split("");
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
      }
      u = parseInt(hex, 16);
      r = u >> 16;
      g = u >> 8 & 0xFF;
      b = u & 0xFF;
      return [r, g, b, 1];
    }
    if (hex.match(/^#?([A-Fa-f0-9]{8})$/)) {
      if (hex.length === 9) {
        hex = hex.substr(1);
      }
      u = parseInt(hex, 16);
      r = u >> 24 & 0xFF;
      g = u >> 16 & 0xFF;
      b = u >> 8 & 0xFF;
      a = round((u & 0xFF) / 0xFF * 100) / 100;
      return [r, g, b, a];
    }
    if ((_input.css != null) && (rgb = _input.css(hex))) {
      return rgb;
    }
    throw "unknown color: " + hex;
  };

  rgb2hex = function(channels, mode) {
    var a, b, g, hxa, r, str, u;
    if (mode == null) {
      mode = 'rgb';
    }
    r = channels[0], g = channels[1], b = channels[2], a = channels[3];
    r = Math.round(r);
    g = Math.round(g);
    b = Math.round(b);
    u = r << 16 | g << 8 | b;
    str = "000000" + u.toString(16);
    str = str.substr(str.length - 6);
    hxa = '0' + round(a * 255).toString(16);
    hxa = hxa.substr(hxa.length - 2);
    return "#" + (function() {
      switch (mode.toLowerCase()) {
        case 'rgba':
          return str + hxa;
        case 'argb':
          return hxa + str;
        default:
          return str;
      }
    })();
  };

  _input.hex = function(h) {
    return hex2rgb(h);
  };

  chroma.hex = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['hex']), function(){});
  };

  Color.prototype.hex = function(mode) {
    if (mode == null) {
      mode = 'rgb';
    }
    return rgb2hex(this._rgb, mode);
  };

  _guess_formats.push({
    p: 4,
    test: function(n) {
      if (arguments.length === 1 && type(n) === "string") {
        return 'hex';
      }
    }
  });

  hsl2rgb = function() {
    var args, b, c, g, h, i, l, o, r, ref, s, t1, t2, t3;
    args = unpack(arguments);
    h = args[0], s = args[1], l = args[2];
    if (s === 0) {
      r = g = b = l * 255;
    } else {
      t3 = [0, 0, 0];
      c = [0, 0, 0];
      t2 = l < 0.5 ? l * (1 + s) : l + s - l * s;
      t1 = 2 * l - t2;
      h /= 360;
      t3[0] = h + 1 / 3;
      t3[1] = h;
      t3[2] = h - 1 / 3;
      for (i = o = 0; o <= 2; i = ++o) {
        if (t3[i] < 0) {
          t3[i] += 1;
        }
        if (t3[i] > 1) {
          t3[i] -= 1;
        }
        if (6 * t3[i] < 1) {
          c[i] = t1 + (t2 - t1) * 6 * t3[i];
        } else if (2 * t3[i] < 1) {
          c[i] = t2;
        } else if (3 * t3[i] < 2) {
          c[i] = t1 + (t2 - t1) * ((2 / 3) - t3[i]) * 6;
        } else {
          c[i] = t1;
        }
      }
      ref = [round(c[0] * 255), round(c[1] * 255), round(c[2] * 255)], r = ref[0], g = ref[1], b = ref[2];
    }
    if (args.length > 3) {
      return [r, g, b, args[3]];
    } else {
      return [r, g, b];
    }
  };

  rgb2hsl = function(r, g, b) {
    var h, l, min, ref, s;
    if (r !== void 0 && r.length >= 3) {
      ref = r, r = ref[0], g = ref[1], b = ref[2];
    }
    r /= 255;
    g /= 255;
    b /= 255;
    min = Math.min(r, g, b);
    max = Math.max(r, g, b);
    l = (max + min) / 2;
    if (max === min) {
      s = 0;
      h = Number.NaN;
    } else {
      s = l < 0.5 ? (max - min) / (max + min) : (max - min) / (2 - max - min);
    }
    if (r === max) {
      h = (g - b) / (max - min);
    } else if (g === max) {
      h = 2 + (b - r) / (max - min);
    } else if (b === max) {
      h = 4 + (r - g) / (max - min);
    }
    h *= 60;
    if (h < 0) {
      h += 360;
    }
    return [h, s, l];
  };

  chroma.hsl = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['hsl']), function(){});
  };

  _input.hsl = hsl2rgb;

  Color.prototype.hsl = function() {
    return rgb2hsl(this._rgb);
  };

  hsv2rgb = function() {
    var args, b, f, g, h, i, p, q, r, ref, ref1, ref2, ref3, ref4, ref5, s, t, v;
    args = unpack(arguments);
    h = args[0], s = args[1], v = args[2];
    v *= 255;
    if (s === 0) {
      r = g = b = v;
    } else {
      if (h === 360) {
        h = 0;
      }
      if (h > 360) {
        h -= 360;
      }
      if (h < 0) {
        h += 360;
      }
      h /= 60;
      i = floor(h);
      f = h - i;
      p = v * (1 - s);
      q = v * (1 - s * f);
      t = v * (1 - s * (1 - f));
      switch (i) {
        case 0:
          ref = [v, t, p], r = ref[0], g = ref[1], b = ref[2];
          break;
        case 1:
          ref1 = [q, v, p], r = ref1[0], g = ref1[1], b = ref1[2];
          break;
        case 2:
          ref2 = [p, v, t], r = ref2[0], g = ref2[1], b = ref2[2];
          break;
        case 3:
          ref3 = [p, q, v], r = ref3[0], g = ref3[1], b = ref3[2];
          break;
        case 4:
          ref4 = [t, p, v], r = ref4[0], g = ref4[1], b = ref4[2];
          break;
        case 5:
          ref5 = [v, p, q], r = ref5[0], g = ref5[1], b = ref5[2];
      }
    }
    return [r, g, b, args.length > 3 ? args[3] : 1];
  };

  rgb2hsv = function() {
    var b, delta, g, h, min, r, ref, s, v;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    min = Math.min(r, g, b);
    max = Math.max(r, g, b);
    delta = max - min;
    v = max / 255.0;
    if (max === 0) {
      h = Number.NaN;
      s = 0;
    } else {
      s = delta / max;
      if (r === max) {
        h = (g - b) / delta;
      }
      if (g === max) {
        h = 2 + (b - r) / delta;
      }
      if (b === max) {
        h = 4 + (r - g) / delta;
      }
      h *= 60;
      if (h < 0) {
        h += 360;
      }
    }
    return [h, s, v];
  };

  chroma.hsv = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['hsv']), function(){});
  };

  _input.hsv = hsv2rgb;

  Color.prototype.hsv = function() {
    return rgb2hsv(this._rgb);
  };

  num2rgb = function(num) {
    var b, g, r;
    if (type(num) === "number" && num >= 0 && num <= 0xFFFFFF) {
      r = num >> 16;
      g = (num >> 8) & 0xFF;
      b = num & 0xFF;
      return [r, g, b, 1];
    }
    console.warn("unknown num color: " + num);
    return [0, 0, 0, 1];
  };

  rgb2num = function() {
    var b, g, r, ref;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    return (r << 16) + (g << 8) + b;
  };

  chroma.num = function(num) {
    return new Color(num, 'num');
  };

  Color.prototype.num = function(mode) {
    if (mode == null) {
      mode = 'rgb';
    }
    return rgb2num(this._rgb, mode);
  };

  _input.num = num2rgb;

  _guess_formats.push({
    p: 1,
    test: function(n) {
      if (arguments.length === 1 && type(n) === "number" && n >= 0 && n <= 0xFFFFFF) {
        return 'num';
      }
    }
  });

  hcg2rgb = function() {
    var _c, _g, args, b, c, f, g, h, i, p, q, r, ref, ref1, ref2, ref3, ref4, ref5, t, v;
    args = unpack(arguments);
    h = args[0], c = args[1], _g = args[2];
    c = c / 100;
    g = g / 100 * 255;
    _c = c * 255;
    if (c === 0) {
      r = g = b = _g;
    } else {
      if (h === 360) {
        h = 0;
      }
      if (h > 360) {
        h -= 360;
      }
      if (h < 0) {
        h += 360;
      }
      h /= 60;
      i = floor(h);
      f = h - i;
      p = _g * (1 - c);
      q = p + _c * (1 - f);
      t = p + _c * f;
      v = p + _c;
      switch (i) {
        case 0:
          ref = [v, t, p], r = ref[0], g = ref[1], b = ref[2];
          break;
        case 1:
          ref1 = [q, v, p], r = ref1[0], g = ref1[1], b = ref1[2];
          break;
        case 2:
          ref2 = [p, v, t], r = ref2[0], g = ref2[1], b = ref2[2];
          break;
        case 3:
          ref3 = [p, q, v], r = ref3[0], g = ref3[1], b = ref3[2];
          break;
        case 4:
          ref4 = [t, p, v], r = ref4[0], g = ref4[1], b = ref4[2];
          break;
        case 5:
          ref5 = [v, p, q], r = ref5[0], g = ref5[1], b = ref5[2];
      }
    }
    return [r, g, b, args.length > 3 ? args[3] : 1];
  };

  rgb2hcg = function() {
    var _g, b, c, delta, g, h, min, r, ref;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    min = Math.min(r, g, b);
    max = Math.max(r, g, b);
    delta = max - min;
    c = delta * 100 / 255;
    _g = min / (255 - delta) * 100;
    if (delta === 0) {
      h = Number.NaN;
    } else {
      if (r === max) {
        h = (g - b) / delta;
      }
      if (g === max) {
        h = 2 + (b - r) / delta;
      }
      if (b === max) {
        h = 4 + (r - g) / delta;
      }
      h *= 60;
      if (h < 0) {
        h += 360;
      }
    }
    return [h, c, _g];
  };

  chroma.hcg = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['hcg']), function(){});
  };

  _input.hcg = hcg2rgb;

  Color.prototype.hcg = function() {
    return rgb2hcg(this._rgb);
  };

  css2rgb = function(css) {
    var aa, ab, hsl, i, m, o, rgb, w;
    css = css.toLowerCase();
    if ((chroma.colors != null) && chroma.colors[css]) {
      return hex2rgb(chroma.colors[css]);
    }
    if (m = css.match(/rgb\(\s*(\-?\d+),\s*(\-?\d+)\s*,\s*(\-?\d+)\s*\)/)) {
      rgb = m.slice(1, 4);
      for (i = o = 0; o <= 2; i = ++o) {
        rgb[i] = +rgb[i];
      }
      rgb[3] = 1;
    } else if (m = css.match(/rgba\(\s*(\-?\d+),\s*(\-?\d+)\s*,\s*(\-?\d+)\s*,\s*([01]|[01]?\.\d+)\)/)) {
      rgb = m.slice(1, 5);
      for (i = w = 0; w <= 3; i = ++w) {
        rgb[i] = +rgb[i];
      }
    } else if (m = css.match(/rgb\(\s*(\-?\d+(?:\.\d+)?)%,\s*(\-?\d+(?:\.\d+)?)%\s*,\s*(\-?\d+(?:\.\d+)?)%\s*\)/)) {
      rgb = m.slice(1, 4);
      for (i = aa = 0; aa <= 2; i = ++aa) {
        rgb[i] = round(rgb[i] * 2.55);
      }
      rgb[3] = 1;
    } else if (m = css.match(/rgba\(\s*(\-?\d+(?:\.\d+)?)%,\s*(\-?\d+(?:\.\d+)?)%\s*,\s*(\-?\d+(?:\.\d+)?)%\s*,\s*([01]|[01]?\.\d+)\)/)) {
      rgb = m.slice(1, 5);
      for (i = ab = 0; ab <= 2; i = ++ab) {
        rgb[i] = round(rgb[i] * 2.55);
      }
      rgb[3] = +rgb[3];
    } else if (m = css.match(/hsl\(\s*(\-?\d+(?:\.\d+)?),\s*(\-?\d+(?:\.\d+)?)%\s*,\s*(\-?\d+(?:\.\d+)?)%\s*\)/)) {
      hsl = m.slice(1, 4);
      hsl[1] *= 0.01;
      hsl[2] *= 0.01;
      rgb = hsl2rgb(hsl);
      rgb[3] = 1;
    } else if (m = css.match(/hsla\(\s*(\-?\d+(?:\.\d+)?),\s*(\-?\d+(?:\.\d+)?)%\s*,\s*(\-?\d+(?:\.\d+)?)%\s*,\s*([01]|[01]?\.\d+)\)/)) {
      hsl = m.slice(1, 4);
      hsl[1] *= 0.01;
      hsl[2] *= 0.01;
      rgb = hsl2rgb(hsl);
      rgb[3] = +m[4];
    }
    return rgb;
  };

  rgb2css = function(rgba) {
    var mode;
    mode = rgba[3] < 1 ? 'rgba' : 'rgb';
    if (mode === 'rgb') {
      return mode + '(' + rgba.slice(0, 3).map(round).join(',') + ')';
    } else if (mode === 'rgba') {
      return mode + '(' + rgba.slice(0, 3).map(round).join(',') + ',' + rgba[3] + ')';
    } else {

    }
  };

  rnd = function(a) {
    return round(a * 100) / 100;
  };

  hsl2css = function(hsl, alpha) {
    var mode;
    mode = alpha < 1 ? 'hsla' : 'hsl';
    hsl[0] = rnd(hsl[0] || 0);
    hsl[1] = rnd(hsl[1] * 100) + '%';
    hsl[2] = rnd(hsl[2] * 100) + '%';
    if (mode === 'hsla') {
      hsl[3] = alpha;
    }
    return mode + '(' + hsl.join(',') + ')';
  };

  _input.css = function(h) {
    return css2rgb(h);
  };

  chroma.css = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['css']), function(){});
  };

  Color.prototype.css = function(mode) {
    if (mode == null) {
      mode = 'rgb';
    }
    if (mode.slice(0, 3) === 'rgb') {
      return rgb2css(this._rgb);
    } else if (mode.slice(0, 3) === 'hsl') {
      return hsl2css(this.hsl(), this.alpha());
    }
  };

  _input.named = function(name) {
    return hex2rgb(w3cx11[name]);
  };

  _guess_formats.push({
    p: 5,
    test: function(n) {
      if (arguments.length === 1 && (w3cx11[n] != null)) {
        return 'named';
      }
    }
  });

  Color.prototype.name = function(n) {
    var h, k;
    if (arguments.length) {
      if (w3cx11[n]) {
        this._rgb = hex2rgb(w3cx11[n]);
      }
      this._rgb[3] = 1;
      this;
    }
    h = this.hex();
    for (k in w3cx11) {
      if (h === w3cx11[k]) {
        return k;
      }
    }
    return h;
  };

  lch2lab = function() {

    /*
    Convert from a qualitative parameter h and a quantitative parameter l to a 24-bit pixel.
    These formulas were invented by David Dalrymple to obtain maximum contrast without going
    out of gamut if the parameters are in the range 0-1.
    
    A saturation multiplier was added by Gregor Aisch
     */
    var c, h, l, ref;
    ref = unpack(arguments), l = ref[0], c = ref[1], h = ref[2];
    h = h * DEG2RAD;
    return [l, cos(h) * c, sin(h) * c];
  };

  lch2rgb = function() {
    var L, a, args, b, c, g, h, l, r, ref, ref1;
    args = unpack(arguments);
    l = args[0], c = args[1], h = args[2];
    ref = lch2lab(l, c, h), L = ref[0], a = ref[1], b = ref[2];
    ref1 = lab2rgb(L, a, b), r = ref1[0], g = ref1[1], b = ref1[2];
    return [r, g, b, args.length > 3 ? args[3] : 1];
  };

  lab2lch = function() {
    var a, b, c, h, l, ref;
    ref = unpack(arguments), l = ref[0], a = ref[1], b = ref[2];
    c = sqrt(a * a + b * b);
    h = (atan2(b, a) * RAD2DEG + 360) % 360;
    if (round(c * 10000) === 0) {
      h = Number.NaN;
    }
    return [l, c, h];
  };

  rgb2lch = function() {
    var a, b, g, l, r, ref, ref1;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    ref1 = rgb2lab(r, g, b), l = ref1[0], a = ref1[1], b = ref1[2];
    return lab2lch(l, a, b);
  };

  chroma.lch = function() {
    var args;
    args = unpack(arguments);
    return new Color(args, 'lch');
  };

  chroma.hcl = function() {
    var args;
    args = unpack(arguments);
    return new Color(args, 'hcl');
  };

  _input.lch = lch2rgb;

  _input.hcl = function() {
    var c, h, l, ref;
    ref = unpack(arguments), h = ref[0], c = ref[1], l = ref[2];
    return lch2rgb([l, c, h]);
  };

  Color.prototype.lch = function() {
    return rgb2lch(this._rgb);
  };

  Color.prototype.hcl = function() {
    return rgb2lch(this._rgb).reverse();
  };

  rgb2cmyk = function(mode) {
    var b, c, f, g, k, m, r, ref, y;
    if (mode == null) {
      mode = 'rgb';
    }
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    r = r / 255;
    g = g / 255;
    b = b / 255;
    k = 1 - Math.max(r, Math.max(g, b));
    f = k < 1 ? 1 / (1 - k) : 0;
    c = (1 - r - k) * f;
    m = (1 - g - k) * f;
    y = (1 - b - k) * f;
    return [c, m, y, k];
  };

  cmyk2rgb = function() {
    var alpha, args, b, c, g, k, m, r, y;
    args = unpack(arguments);
    c = args[0], m = args[1], y = args[2], k = args[3];
    alpha = args.length > 4 ? args[4] : 1;
    if (k === 1) {
      return [0, 0, 0, alpha];
    }
    r = c >= 1 ? 0 : 255 * (1 - c) * (1 - k);
    g = m >= 1 ? 0 : 255 * (1 - m) * (1 - k);
    b = y >= 1 ? 0 : 255 * (1 - y) * (1 - k);
    return [r, g, b, alpha];
  };

  _input.cmyk = function() {
    return cmyk2rgb(unpack(arguments));
  };

  chroma.cmyk = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['cmyk']), function(){});
  };

  Color.prototype.cmyk = function() {
    return rgb2cmyk(this._rgb);
  };

  _input.gl = function() {
    var i, k, o, rgb, v;
    rgb = (function() {
      var ref, results;
      ref = unpack(arguments);
      results = [];
      for (k in ref) {
        v = ref[k];
        results.push(v);
      }
      return results;
    }).apply(this, arguments);
    for (i = o = 0; o <= 2; i = ++o) {
      rgb[i] *= 255;
    }
    return rgb;
  };

  chroma.gl = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['gl']), function(){});
  };

  Color.prototype.gl = function() {
    var rgb;
    rgb = this._rgb;
    return [rgb[0] / 255, rgb[1] / 255, rgb[2] / 255, rgb[3]];
  };

  rgb2luminance = function(r, g, b) {
    var ref;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    r = luminance_x(r);
    g = luminance_x(g);
    b = luminance_x(b);
    return 0.2126 * r + 0.7152 * g + 0.0722 * b;
  };

  luminance_x = function(x) {
    x /= 255;
    if (x <= 0.03928) {
      return x / 12.92;
    } else {
      return pow((x + 0.055) / 1.055, 2.4);
    }
  };

  _interpolators = [];

  interpolate = function(col1, col2, f, m) {
    var interpol, len, o, res;
    if (f == null) {
      f = 0.5;
    }
    if (m == null) {
      m = 'rgb';
    }

    /*
    interpolates between colors
    f = 0 --> me
    f = 1 --> col
     */
    if (type(col1) !== 'object') {
      col1 = chroma(col1);
    }
    if (type(col2) !== 'object') {
      col2 = chroma(col2);
    }
    for (o = 0, len = _interpolators.length; o < len; o++) {
      interpol = _interpolators[o];
      if (m === interpol[0]) {
        res = interpol[1](col1, col2, f, m);
        break;
      }
    }
    if (res == null) {
      throw "color mode " + m + " is not supported";
    }
    return res.alpha(col1.alpha() + f * (col2.alpha() - col1.alpha()));
  };

  chroma.interpolate = interpolate;

  Color.prototype.interpolate = function(col2, f, m) {
    return interpolate(this, col2, f, m);
  };

  chroma.mix = interpolate;

  Color.prototype.mix = Color.prototype.interpolate;

  interpolate_rgb = function(col1, col2, f, m) {
    var xyz0, xyz1;
    xyz0 = col1._rgb;
    xyz1 = col2._rgb;
    return new Color(xyz0[0] + f * (xyz1[0] - xyz0[0]), xyz0[1] + f * (xyz1[1] - xyz0[1]), xyz0[2] + f * (xyz1[2] - xyz0[2]), m);
  };

  _interpolators.push(['rgb', interpolate_rgb]);

  Color.prototype.luminance = function(lum, mode) {
    var cur_lum, eps, max_iter, test;
    if (mode == null) {
      mode = 'rgb';
    }
    if (!arguments.length) {
      return rgb2luminance(this._rgb);
    }
    if (lum === 0) {
      this._rgb = [0, 0, 0, this._rgb[3]];
    } else if (lum === 1) {
      this._rgb = [255, 255, 255, this._rgb[3]];
    } else {
      eps = 1e-7;
      max_iter = 20;
      test = function(l, h) {
        var lm, m;
        m = l.interpolate(h, 0.5, mode);
        lm = m.luminance();
        if (Math.abs(lum - lm) < eps || !max_iter--) {
          return m;
        }
        if (lm > lum) {
          return test(l, m);
        }
        return test(m, h);
      };
      cur_lum = rgb2luminance(this._rgb);
      this._rgb = (cur_lum > lum ? test(chroma('black'), this) : test(this, chroma('white'))).rgba();
    }
    return this;
  };

  temperature2rgb = function(kelvin) {
    var b, g, r, temp;
    temp = kelvin / 100;
    if (temp < 66) {
      r = 255;
      g = -155.25485562709179 - 0.44596950469579133 * (g = temp - 2) + 104.49216199393888 * log(g);
      b = temp < 20 ? 0 : -254.76935184120902 + 0.8274096064007395 * (b = temp - 10) + 115.67994401066147 * log(b);
    } else {
      r = 351.97690566805693 + 0.114206453784165 * (r = temp - 55) - 40.25366309332127 * log(r);
      g = 325.4494125711974 + 0.07943456536662342 * (g = temp - 50) - 28.0852963507957 * log(g);
      b = 255;
    }
    return [r, g, b];
  };

  rgb2temperature = function() {
    var b, eps, g, maxTemp, minTemp, r, ref, rgb, temp;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    minTemp = 1000;
    maxTemp = 40000;
    eps = 0.4;
    while (maxTemp - minTemp > eps) {
      temp = (maxTemp + minTemp) * 0.5;
      rgb = temperature2rgb(temp);
      if ((rgb[2] / rgb[0]) >= (b / r)) {
        maxTemp = temp;
      } else {
        minTemp = temp;
      }
    }
    return round(temp);
  };

  chroma.temperature = chroma.kelvin = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['temperature']), function(){});
  };

  _input.temperature = _input.kelvin = _input.K = temperature2rgb;

  Color.prototype.temperature = function() {
    return rgb2temperature(this._rgb);
  };

  Color.prototype.kelvin = Color.prototype.temperature;

  chroma.contrast = function(a, b) {
    var l1, l2, ref, ref1;
    if ((ref = type(a)) === 'string' || ref === 'number') {
      a = new Color(a);
    }
    if ((ref1 = type(b)) === 'string' || ref1 === 'number') {
      b = new Color(b);
    }
    l1 = a.luminance();
    l2 = b.luminance();
    if (l1 > l2) {
      return (l1 + 0.05) / (l2 + 0.05);
    } else {
      return (l2 + 0.05) / (l1 + 0.05);
    }
  };

  chroma.distance = function(a, b, mode) {
    var d, i, l1, l2, ref, ref1, sum_sq;
    if (mode == null) {
      mode = 'lab';
    }
    if ((ref = type(a)) === 'string' || ref === 'number') {
      a = new Color(a);
    }
    if ((ref1 = type(b)) === 'string' || ref1 === 'number') {
      b = new Color(b);
    }
    l1 = a.get(mode);
    l2 = b.get(mode);
    sum_sq = 0;
    for (i in l1) {
      d = (l1[i] || 0) - (l2[i] || 0);
      sum_sq += d * d;
    }
    return Math.sqrt(sum_sq);
  };

  chroma.deltaE = function(a, b, L, C) {
    var L1, L2, a1, a2, b1, b2, c1, c2, c4, dH2, delA, delB, delC, delL, f, h1, ref, ref1, ref2, ref3, sc, sh, sl, t, v1, v2, v3;
    if (L == null) {
      L = 1;
    }
    if (C == null) {
      C = 1;
    }
    if ((ref = type(a)) === 'string' || ref === 'number') {
      a = new Color(a);
    }
    if ((ref1 = type(b)) === 'string' || ref1 === 'number') {
      b = new Color(b);
    }
    ref2 = a.lab(), L1 = ref2[0], a1 = ref2[1], b1 = ref2[2];
    ref3 = b.lab(), L2 = ref3[0], a2 = ref3[1], b2 = ref3[2];
    c1 = sqrt(a1 * a1 + b1 * b1);
    c2 = sqrt(a2 * a2 + b2 * b2);
    sl = L1 < 16.0 ? 0.511 : (0.040975 * L1) / (1.0 + 0.01765 * L1);
    sc = (0.0638 * c1) / (1.0 + 0.0131 * c1) + 0.638;
    h1 = c1 < 0.000001 ? 0.0 : (atan2(b1, a1) * 180.0) / PI;
    while (h1 < 0) {
      h1 += 360;
    }
    while (h1 >= 360) {
      h1 -= 360;
    }
    t = (h1 >= 164.0) && (h1 <= 345.0) ? 0.56 + abs(0.2 * cos((PI * (h1 + 168.0)) / 180.0)) : 0.36 + abs(0.4 * cos((PI * (h1 + 35.0)) / 180.0));
    c4 = c1 * c1 * c1 * c1;
    f = sqrt(c4 / (c4 + 1900.0));
    sh = sc * (f * t + 1.0 - f);
    delL = L1 - L2;
    delC = c1 - c2;
    delA = a1 - a2;
    delB = b1 - b2;
    dH2 = delA * delA + delB * delB - delC * delC;
    v1 = delL / (L * sl);
    v2 = delC / (C * sc);
    v3 = sh;
    return sqrt(v1 * v1 + v2 * v2 + (dH2 / (v3 * v3)));
  };

  Color.prototype.get = function(modechan) {
    var channel, i, me, mode, ref, src;
    me = this;
    ref = modechan.split('.'), mode = ref[0], channel = ref[1];
    src = me[mode]();
    if (channel) {
      i = mode.indexOf(channel);
      if (i > -1) {
        return src[i];
      } else {
        return console.warn('unknown channel ' + channel + ' in mode ' + mode);
      }
    } else {
      return src;
    }
  };

  Color.prototype.set = function(modechan, value) {
    var channel, i, me, mode, ref, src;
    me = this;
    ref = modechan.split('.'), mode = ref[0], channel = ref[1];
    if (channel) {
      src = me[mode]();
      i = mode.indexOf(channel);
      if (i > -1) {
        if (type(value) === 'string') {
          switch (value.charAt(0)) {
            case '+':
              src[i] += +value;
              break;
            case '-':
              src[i] += +value;
              break;
            case '*':
              src[i] *= +(value.substr(1));
              break;
            case '/':
              src[i] /= +(value.substr(1));
              break;
            default:
              src[i] = +value;
          }
        } else {
          src[i] = value;
        }
      } else {
        console.warn('unknown channel ' + channel + ' in mode ' + mode);
      }
    } else {
      src = value;
    }
    return chroma(src, mode).alpha(me.alpha());
  };

  Color.prototype.clipped = function() {
    return this._rgb._clipped || false;
  };

  Color.prototype.alpha = function(a) {
    if (arguments.length) {
      return chroma.rgb([this._rgb[0], this._rgb[1], this._rgb[2], a]);
    }
    return this._rgb[3];
  };

  Color.prototype.darken = function(amount) {
    var lab, me;
    if (amount == null) {
      amount = 1;
    }
    me = this;
    lab = me.lab();
    lab[0] -= LAB_CONSTANTS.Kn * amount;
    return chroma.lab(lab).alpha(me.alpha());
  };

  Color.prototype.brighten = function(amount) {
    if (amount == null) {
      amount = 1;
    }
    return this.darken(-amount);
  };

  Color.prototype.darker = Color.prototype.darken;

  Color.prototype.brighter = Color.prototype.brighten;

  Color.prototype.saturate = function(amount) {
    var lch, me;
    if (amount == null) {
      amount = 1;
    }
    me = this;
    lch = me.lch();
    lch[1] += amount * LAB_CONSTANTS.Kn;
    if (lch[1] < 0) {
      lch[1] = 0;
    }
    return chroma.lch(lch).alpha(me.alpha());
  };

  Color.prototype.desaturate = function(amount) {
    if (amount == null) {
      amount = 1;
    }
    return this.saturate(-amount);
  };

  Color.prototype.premultiply = function() {
    var a, rgb;
    rgb = this.rgb();
    a = this.alpha();
    return chroma(rgb[0] * a, rgb[1] * a, rgb[2] * a, a);
  };

  blend = function(bottom, top, mode) {
    if (!blend[mode]) {
      throw 'unknown blend mode ' + mode;
    }
    return blend[mode](bottom, top);
  };

  blend_f = function(f) {
    return function(bottom, top) {
      var c0, c1;
      c0 = chroma(top).rgb();
      c1 = chroma(bottom).rgb();
      return chroma(f(c0, c1), 'rgb');
    };
  };

  each = function(f) {
    return function(c0, c1) {
      var i, o, out;
      out = [];
      for (i = o = 0; o <= 3; i = ++o) {
        out[i] = f(c0[i], c1[i]);
      }
      return out;
    };
  };

  normal = function(a, b) {
    return a;
  };

  multiply = function(a, b) {
    return a * b / 255;
  };

  darken = function(a, b) {
    if (a > b) {
      return b;
    } else {
      return a;
    }
  };

  lighten = function(a, b) {
    if (a > b) {
      return a;
    } else {
      return b;
    }
  };

  screen = function(a, b) {
    return 255 * (1 - (1 - a / 255) * (1 - b / 255));
  };

  overlay = function(a, b) {
    if (b < 128) {
      return 2 * a * b / 255;
    } else {
      return 255 * (1 - 2 * (1 - a / 255) * (1 - b / 255));
    }
  };

  burn = function(a, b) {
    return 255 * (1 - (1 - b / 255) / (a / 255));
  };

  dodge = function(a, b) {
    if (a === 255) {
      return 255;
    }
    a = 255 * (b / 255) / (1 - a / 255);
    if (a > 255) {
      return 255;
    } else {
      return a;
    }
  };

  blend.normal = blend_f(each(normal));

  blend.multiply = blend_f(each(multiply));

  blend.screen = blend_f(each(screen));

  blend.overlay = blend_f(each(overlay));

  blend.darken = blend_f(each(darken));

  blend.lighten = blend_f(each(lighten));

  blend.dodge = blend_f(each(dodge));

  blend.burn = blend_f(each(burn));

  chroma.blend = blend;

  chroma.analyze = function(data) {
    var len, o, r, val;
    r = {
      min: Number.MAX_VALUE,
      max: Number.MAX_VALUE * -1,
      sum: 0,
      values: [],
      count: 0
    };
    for (o = 0, len = data.length; o < len; o++) {
      val = data[o];
      if ((val != null) && !isNaN(val)) {
        r.values.push(val);
        r.sum += val;
        if (val < r.min) {
          r.min = val;
        }
        if (val > r.max) {
          r.max = val;
        }
        r.count += 1;
      }
    }
    r.domain = [r.min, r.max];
    r.limits = function(mode, num) {
      return chroma.limits(r, mode, num);
    };
    return r;
  };

  chroma.scale = function(colors, positions) {
    var _classes, _colorCache, _colors, _correctLightness, _domain, _fixed, _max, _min, _mode, _nacol, _out, _padding, _pos, _spread, _useCache, classifyValue, f, getClass, getColor, resetCache, setColors, tmap;
    _mode = 'rgb';
    _nacol = chroma('#ccc');
    _spread = 0;
    _fixed = false;
    _domain = [0, 1];
    _pos = [];
    _padding = [0, 0];
    _classes = false;
    _colors = [];
    _out = false;
    _min = 0;
    _max = 1;
    _correctLightness = false;
    _colorCache = {};
    _useCache = true;
    setColors = function(colors) {
      var c, col, o, ref, ref1, w;
      if (colors == null) {
        colors = ['#fff', '#000'];
      }
      if ((colors != null) && type(colors) === 'string' && (chroma.brewer != null)) {
        colors = chroma.brewer[colors] || chroma.brewer[colors.toLowerCase()] || colors;
      }
      if (type(colors) === 'array') {
        colors = colors.slice(0);
        for (c = o = 0, ref = colors.length - 1; 0 <= ref ? o <= ref : o >= ref; c = 0 <= ref ? ++o : --o) {
          col = colors[c];
          if (type(col) === "string") {
            colors[c] = chroma(col);
          }
        }
        _pos.length = 0;
        for (c = w = 0, ref1 = colors.length - 1; 0 <= ref1 ? w <= ref1 : w >= ref1; c = 0 <= ref1 ? ++w : --w) {
          _pos.push(c / (colors.length - 1));
        }
      }
      resetCache();
      return _colors = colors;
    };
    getClass = function(value) {
      var i, n;
      if (_classes != null) {
        n = _classes.length - 1;
        i = 0;
        while (i < n && value >= _classes[i]) {
          i++;
        }
        return i - 1;
      }
      return 0;
    };
    tmap = function(t) {
      return t;
    };
    classifyValue = function(value) {
      var i, maxc, minc, n, val;
      val = value;
      if (_classes.length > 2) {
        n = _classes.length - 1;
        i = getClass(value);
        minc = _classes[0] + (_classes[1] - _classes[0]) * (0 + _spread * 0.5);
        maxc = _classes[n - 1] + (_classes[n] - _classes[n - 1]) * (1 - _spread * 0.5);
        val = _min + ((_classes[i] + (_classes[i + 1] - _classes[i]) * 0.5 - minc) / (maxc - minc)) * (_max - _min);
      }
      return val;
    };
    getColor = function(val, bypassMap) {
      var c, col, i, k, o, p, ref, t;
      if (bypassMap == null) {
        bypassMap = false;
      }
      if (isNaN(val)) {
        return _nacol;
      }
      if (!bypassMap) {
        if (_classes && _classes.length > 2) {
          c = getClass(val);
          t = c / (_classes.length - 2);
          t = _padding[0] + (t * (1 - _padding[0] - _padding[1]));
        } else if (_max !== _min) {
          t = (val - _min) / (_max - _min);
          t = _padding[0] + (t * (1 - _padding[0] - _padding[1]));
          t = Math.min(1, Math.max(0, t));
        } else {
          t = 1;
        }
      } else {
        t = val;
      }
      if (!bypassMap) {
        t = tmap(t);
      }
      k = Math.floor(t * 10000);
      if (_useCache && _colorCache[k]) {
        col = _colorCache[k];
      } else {
        if (type(_colors) === 'array') {
          for (i = o = 0, ref = _pos.length - 1; 0 <= ref ? o <= ref : o >= ref; i = 0 <= ref ? ++o : --o) {
            p = _pos[i];
            if (t <= p) {
              col = _colors[i];
              break;
            }
            if (t >= p && i === _pos.length - 1) {
              col = _colors[i];
              break;
            }
            if (t > p && t < _pos[i + 1]) {
              t = (t - p) / (_pos[i + 1] - p);
              col = chroma.interpolate(_colors[i], _colors[i + 1], t, _mode);
              break;
            }
          }
        } else if (type(_colors) === 'function') {
          col = _colors(t);
        }
        if (_useCache) {
          _colorCache[k] = col;
        }
      }
      return col;
    };
    resetCache = function() {
      return _colorCache = {};
    };
    setColors(colors);
    f = function(v) {
      var c;
      c = chroma(getColor(v));
      if (_out && c[_out]) {
        return c[_out]();
      } else {
        return c;
      }
    };
    f.classes = function(classes) {
      var d;
      if (classes != null) {
        if (type(classes) === 'array') {
          _classes = classes;
          _domain = [classes[0], classes[classes.length - 1]];
        } else {
          d = chroma.analyze(_domain);
          if (classes === 0) {
            _classes = [d.min, d.max];
          } else {
            _classes = chroma.limits(d, 'e', classes);
          }
        }
        return f;
      }
      return _classes;
    };
    f.domain = function(domain) {
      var c, d, k, len, o, ref, w;
      if (!arguments.length) {
        return _domain;
      }
      _min = domain[0];
      _max = domain[domain.length - 1];
      _pos = [];
      k = _colors.length;
      if (domain.length === k && _min !== _max) {
        for (o = 0, len = domain.length; o < len; o++) {
          d = domain[o];
          _pos.push((d - _min) / (_max - _min));
        }
      } else {
        for (c = w = 0, ref = k - 1; 0 <= ref ? w <= ref : w >= ref; c = 0 <= ref ? ++w : --w) {
          _pos.push(c / (k - 1));
        }
      }
      _domain = [_min, _max];
      return f;
    };
    f.mode = function(_m) {
      if (!arguments.length) {
        return _mode;
      }
      _mode = _m;
      resetCache();
      return f;
    };
    f.range = function(colors, _pos) {
      setColors(colors, _pos);
      return f;
    };
    f.out = function(_o) {
      _out = _o;
      return f;
    };
    f.spread = function(val) {
      if (!arguments.length) {
        return _spread;
      }
      _spread = val;
      return f;
    };
    f.correctLightness = function(v) {
      if (v == null) {
        v = true;
      }
      _correctLightness = v;
      resetCache();
      if (_correctLightness) {
        tmap = function(t) {
          var L0, L1, L_actual, L_diff, L_ideal, max_iter, pol, t0, t1;
          L0 = getColor(0, true).lab()[0];
          L1 = getColor(1, true).lab()[0];
          pol = L0 > L1;
          L_actual = getColor(t, true).lab()[0];
          L_ideal = L0 + (L1 - L0) * t;
          L_diff = L_actual - L_ideal;
          t0 = 0;
          t1 = 1;
          max_iter = 20;
          while (Math.abs(L_diff) > 1e-2 && max_iter-- > 0) {
            (function() {
              if (pol) {
                L_diff *= -1;
              }
              if (L_diff < 0) {
                t0 = t;
                t += (t1 - t) * 0.5;
              } else {
                t1 = t;
                t += (t0 - t) * 0.5;
              }
              L_actual = getColor(t, true).lab()[0];
              return L_diff = L_actual - L_ideal;
            })();
          }
          return t;
        };
      } else {
        tmap = function(t) {
          return t;
        };
      }
      return f;
    };
    f.padding = function(p) {
      if (p != null) {
        if (type(p) === 'number') {
          p = [p, p];
        }
        _padding = p;
        return f;
      } else {
        return _padding;
      }
    };
    f.colors = function(numColors, out) {
      var dd, dm, i, o, ref, results, samples, w;
      if (out == null) {
        out = 'hex';
      }
      if (arguments.length === 0) {
        return _colors.map(function(c) {
          return c[out]();
        });
      }
      if (numColors) {
        if (numColors === 1) {
          return f(0.5)[out]();
        }
        dm = _domain[0];
        dd = _domain[1] - dm;
        return (function() {
          results = [];
          for (var o = 0; 0 <= numColors ? o < numColors : o > numColors; 0 <= numColors ? o++ : o--){ results.push(o); }
          return results;
        }).apply(this).map(function(i) {
          return f(dm + i / (numColors - 1) * dd)[out]();
        });
      }
      colors = [];
      samples = [];
      if (_classes && _classes.length > 2) {
        for (i = w = 1, ref = _classes.length; 1 <= ref ? w < ref : w > ref; i = 1 <= ref ? ++w : --w) {
          samples.push((_classes[i - 1] + _classes[i]) * 0.5);
        }
      } else {
        samples = _domain;
      }
      return samples.map(function(v) {
        return f(v)[out]();
      });
    };
    f.cache = function(c) {
      if (c != null) {
        return _useCache = c;
      } else {
        return _useCache;
      }
    };
    return f;
  };

  if (chroma.scales == null) {
    chroma.scales = {};
  }

  chroma.scales.cool = function() {
    return chroma.scale([chroma.hsl(180, 1, .9), chroma.hsl(250, .7, .4)]);
  };

  chroma.scales.hot = function() {
    return chroma.scale(['#000', '#f00', '#ff0', '#fff'], [0, .25, .75, 1]).mode('rgb');
  };

  chroma.analyze = function(data, key, filter) {
    var add, k, len, o, r, val, visit;
    r = {
      min: Number.MAX_VALUE,
      max: Number.MAX_VALUE * -1,
      sum: 0,
      values: [],
      count: 0
    };
    if (filter == null) {
      filter = function() {
        return true;
      };
    }
    add = function(val) {
      if ((val != null) && !isNaN(val)) {
        r.values.push(val);
        r.sum += val;
        if (val < r.min) {
          r.min = val;
        }
        if (val > r.max) {
          r.max = val;
        }
        r.count += 1;
      }
    };
    visit = function(val, k) {
      if (filter(val, k)) {
        if ((key != null) && type(key) === 'function') {
          return add(key(val));
        } else if ((key != null) && type(key) === 'string' || type(key) === 'number') {
          return add(val[key]);
        } else {
          return add(val);
        }
      }
    };
    if (type(data) === 'array') {
      for (o = 0, len = data.length; o < len; o++) {
        val = data[o];
        visit(val);
      }
    } else {
      for (k in data) {
        val = data[k];
        visit(val, k);
      }
    }
    r.domain = [r.min, r.max];
    r.limits = function(mode, num) {
      return chroma.limits(r, mode, num);
    };
    return r;
  };

  chroma.limits = function(data, mode, num) {
    var aa, ab, ac, ad, ae, af, ag, ah, ai, aj, ak, al, am, assignments, best, centroids, cluster, clusterSizes, dist, i, j, kClusters, limits, max_log, min, min_log, mindist, n, nb_iters, newCentroids, o, p, pb, pr, ref, ref1, ref10, ref11, ref12, ref13, ref14, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, repeat, sum, tmpKMeansBreaks, v, value, values, w;
    if (mode == null) {
      mode = 'equal';
    }
    if (num == null) {
      num = 7;
    }
    if (type(data) === 'array') {
      data = chroma.analyze(data);
    }
    min = data.min;
    max = data.max;
    sum = data.sum;
    values = data.values.sort(function(a, b) {
      return a - b;
    });
    if (num === 1) {
      return [min, max];
    }
    limits = [];
    if (mode.substr(0, 1) === 'c') {
      limits.push(min);
      limits.push(max);
    }
    if (mode.substr(0, 1) === 'e') {
      limits.push(min);
      for (i = o = 1, ref = num - 1; 1 <= ref ? o <= ref : o >= ref; i = 1 <= ref ? ++o : --o) {
        limits.push(min + (i / num) * (max - min));
      }
      limits.push(max);
    } else if (mode.substr(0, 1) === 'l') {
      if (min <= 0) {
        throw 'Logarithmic scales are only possible for values > 0';
      }
      min_log = Math.LOG10E * log(min);
      max_log = Math.LOG10E * log(max);
      limits.push(min);
      for (i = w = 1, ref1 = num - 1; 1 <= ref1 ? w <= ref1 : w >= ref1; i = 1 <= ref1 ? ++w : --w) {
        limits.push(pow(10, min_log + (i / num) * (max_log - min_log)));
      }
      limits.push(max);
    } else if (mode.substr(0, 1) === 'q') {
      limits.push(min);
      for (i = aa = 1, ref2 = num - 1; 1 <= ref2 ? aa <= ref2 : aa >= ref2; i = 1 <= ref2 ? ++aa : --aa) {
        p = (values.length - 1) * i / num;
        pb = floor(p);
        if (pb === p) {
          limits.push(values[pb]);
        } else {
          pr = p - pb;
          limits.push(values[pb] * (1 - pr) + values[pb + 1] * pr);
        }
      }
      limits.push(max);
    } else if (mode.substr(0, 1) === 'k') {

      /*
      implementation based on
      http://code.google.com/p/figue/source/browse/trunk/figue.js#336
      simplified for 1-d input values
       */
      n = values.length;
      assignments = new Array(n);
      clusterSizes = new Array(num);
      repeat = true;
      nb_iters = 0;
      centroids = null;
      centroids = [];
      centroids.push(min);
      for (i = ab = 1, ref3 = num - 1; 1 <= ref3 ? ab <= ref3 : ab >= ref3; i = 1 <= ref3 ? ++ab : --ab) {
        centroids.push(min + (i / num) * (max - min));
      }
      centroids.push(max);
      while (repeat) {
        for (j = ac = 0, ref4 = num - 1; 0 <= ref4 ? ac <= ref4 : ac >= ref4; j = 0 <= ref4 ? ++ac : --ac) {
          clusterSizes[j] = 0;
        }
        for (i = ad = 0, ref5 = n - 1; 0 <= ref5 ? ad <= ref5 : ad >= ref5; i = 0 <= ref5 ? ++ad : --ad) {
          value = values[i];
          mindist = Number.MAX_VALUE;
          for (j = ae = 0, ref6 = num - 1; 0 <= ref6 ? ae <= ref6 : ae >= ref6; j = 0 <= ref6 ? ++ae : --ae) {
            dist = abs(centroids[j] - value);
            if (dist < mindist) {
              mindist = dist;
              best = j;
            }
          }
          clusterSizes[best]++;
          assignments[i] = best;
        }
        newCentroids = new Array(num);
        for (j = af = 0, ref7 = num - 1; 0 <= ref7 ? af <= ref7 : af >= ref7; j = 0 <= ref7 ? ++af : --af) {
          newCentroids[j] = null;
        }
        for (i = ag = 0, ref8 = n - 1; 0 <= ref8 ? ag <= ref8 : ag >= ref8; i = 0 <= ref8 ? ++ag : --ag) {
          cluster = assignments[i];
          if (newCentroids[cluster] === null) {
            newCentroids[cluster] = values[i];
          } else {
            newCentroids[cluster] += values[i];
          }
        }
        for (j = ah = 0, ref9 = num - 1; 0 <= ref9 ? ah <= ref9 : ah >= ref9; j = 0 <= ref9 ? ++ah : --ah) {
          newCentroids[j] *= 1 / clusterSizes[j];
        }
        repeat = false;
        for (j = ai = 0, ref10 = num - 1; 0 <= ref10 ? ai <= ref10 : ai >= ref10; j = 0 <= ref10 ? ++ai : --ai) {
          if (newCentroids[j] !== centroids[i]) {
            repeat = true;
            break;
          }
        }
        centroids = newCentroids;
        nb_iters++;
        if (nb_iters > 200) {
          repeat = false;
        }
      }
      kClusters = {};
      for (j = aj = 0, ref11 = num - 1; 0 <= ref11 ? aj <= ref11 : aj >= ref11; j = 0 <= ref11 ? ++aj : --aj) {
        kClusters[j] = [];
      }
      for (i = ak = 0, ref12 = n - 1; 0 <= ref12 ? ak <= ref12 : ak >= ref12; i = 0 <= ref12 ? ++ak : --ak) {
        cluster = assignments[i];
        kClusters[cluster].push(values[i]);
      }
      tmpKMeansBreaks = [];
      for (j = al = 0, ref13 = num - 1; 0 <= ref13 ? al <= ref13 : al >= ref13; j = 0 <= ref13 ? ++al : --al) {
        tmpKMeansBreaks.push(kClusters[j][0]);
        tmpKMeansBreaks.push(kClusters[j][kClusters[j].length - 1]);
      }
      tmpKMeansBreaks = tmpKMeansBreaks.sort(function(a, b) {
        return a - b;
      });
      limits.push(tmpKMeansBreaks[0]);
      for (i = am = 1, ref14 = tmpKMeansBreaks.length - 1; am <= ref14; i = am += 2) {
        v = tmpKMeansBreaks[i];
        if (!isNaN(v) && limits.indexOf(v) === -1) {
          limits.push(v);
        }
      }
    }
    return limits;
  };

  hsi2rgb = function(h, s, i) {

    /*
    borrowed from here:
    http://hummer.stanford.edu/museinfo/doc/examples/humdrum/keyscape2/hsi2rgb.cpp
     */
    var args, b, g, r;
    args = unpack(arguments);
    h = args[0], s = args[1], i = args[2];
    if (isNaN(h)) {
      h = 0;
    }
    h /= 360;
    if (h < 1 / 3) {
      b = (1 - s) / 3;
      r = (1 + s * cos(TWOPI * h) / cos(PITHIRD - TWOPI * h)) / 3;
      g = 1 - (b + r);
    } else if (h < 2 / 3) {
      h -= 1 / 3;
      r = (1 - s) / 3;
      g = (1 + s * cos(TWOPI * h) / cos(PITHIRD - TWOPI * h)) / 3;
      b = 1 - (r + g);
    } else {
      h -= 2 / 3;
      g = (1 - s) / 3;
      b = (1 + s * cos(TWOPI * h) / cos(PITHIRD - TWOPI * h)) / 3;
      r = 1 - (g + b);
    }
    r = limit(i * r * 3);
    g = limit(i * g * 3);
    b = limit(i * b * 3);
    return [r * 255, g * 255, b * 255, args.length > 3 ? args[3] : 1];
  };

  rgb2hsi = function() {

    /*
    borrowed from here:
    http://hummer.stanford.edu/museinfo/doc/examples/humdrum/keyscape2/rgb2hsi.cpp
     */
    var b, g, h, i, min, r, ref, s;
    ref = unpack(arguments), r = ref[0], g = ref[1], b = ref[2];
    TWOPI = Math.PI * 2;
    r /= 255;
    g /= 255;
    b /= 255;
    min = Math.min(r, g, b);
    i = (r + g + b) / 3;
    s = 1 - min / i;
    if (s === 0) {
      h = 0;
    } else {
      h = ((r - g) + (r - b)) / 2;
      h /= Math.sqrt((r - g) * (r - g) + (r - b) * (g - b));
      h = Math.acos(h);
      if (b > g) {
        h = TWOPI - h;
      }
      h /= TWOPI;
    }
    return [h * 360, s, i];
  };

  chroma.hsi = function() {
    return (function(func, args, ctor) {
      ctor.prototype = func.prototype;
      var child = new ctor, result = func.apply(child, args);
      return Object(result) === result ? result : child;
    })(Color, slice.call(arguments).concat(['hsi']), function(){});
  };

  _input.hsi = hsi2rgb;

  Color.prototype.hsi = function() {
    return rgb2hsi(this._rgb);
  };

  interpolate_hsx = function(col1, col2, f, m) {
    var dh, hue, hue0, hue1, lbv, lbv0, lbv1, res, sat, sat0, sat1, xyz0, xyz1;
    if (m === 'hsl') {
      xyz0 = col1.hsl();
      xyz1 = col2.hsl();
    } else if (m === 'hsv') {
      xyz0 = col1.hsv();
      xyz1 = col2.hsv();
    } else if (m === 'hcg') {
      xyz0 = col1.hcg();
      xyz1 = col2.hcg();
    } else if (m === 'hsi') {
      xyz0 = col1.hsi();
      xyz1 = col2.hsi();
    } else if (m === 'lch' || m === 'hcl') {
      m = 'hcl';
      xyz0 = col1.hcl();
      xyz1 = col2.hcl();
    }
    if (m.substr(0, 1) === 'h') {
      hue0 = xyz0[0], sat0 = xyz0[1], lbv0 = xyz0[2];
      hue1 = xyz1[0], sat1 = xyz1[1], lbv1 = xyz1[2];
    }
    if (!isNaN(hue0) && !isNaN(hue1)) {
      if (hue1 > hue0 && hue1 - hue0 > 180) {
        dh = hue1 - (hue0 + 360);
      } else if (hue1 < hue0 && hue0 - hue1 > 180) {
        dh = hue1 + 360 - hue0;
      } else {
        dh = hue1 - hue0;
      }
      hue = hue0 + f * dh;
    } else if (!isNaN(hue0)) {
      hue = hue0;
      if ((lbv1 === 1 || lbv1 === 0) && m !== 'hsv') {
        sat = sat0;
      }
    } else if (!isNaN(hue1)) {
      hue = hue1;
      if ((lbv0 === 1 || lbv0 === 0) && m !== 'hsv') {
        sat = sat1;
      }
    } else {
      hue = Number.NaN;
    }
    if (sat == null) {
      sat = sat0 + f * (sat1 - sat0);
    }
    lbv = lbv0 + f * (lbv1 - lbv0);
    return res = chroma[m](hue, sat, lbv);
  };

  _interpolators = _interpolators.concat((function() {
    var len, o, ref, results;
    ref = ['hsv', 'hsl', 'hsi', 'hcl', 'lch', 'hcg'];
    results = [];
    for (o = 0, len = ref.length; o < len; o++) {
      m = ref[o];
      results.push([m, interpolate_hsx]);
    }
    return results;
  })());

  interpolate_num = function(col1, col2, f, m) {
    var n1, n2;
    n1 = col1.num();
    n2 = col2.num();
    return chroma.num(n1 + (n2 - n1) * f, 'num');
  };

  _interpolators.push(['num', interpolate_num]);

  interpolate_lab = function(col1, col2, f, m) {
    var res, xyz0, xyz1;
    xyz0 = col1.lab();
    xyz1 = col2.lab();
    return res = new Color(xyz0[0] + f * (xyz1[0] - xyz0[0]), xyz0[1] + f * (xyz1[1] - xyz0[1]), xyz0[2] + f * (xyz1[2] - xyz0[2]), m);
  };

  _interpolators.push(['lab', interpolate_lab]);

}).call(this);

},{}],84:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      } else {
        // At least give some kind of context to the user
        var err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
        err.context = er;
        throw err;
      }
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    args = Array.prototype.slice.call(arguments, 1);
    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  // To avoid recursion in the case that type === "newListener"! Before
  // adding it to the listeners, first emit "newListener".
  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    // If we've already got an array, just append.
    this._events[type].push(listener);
  else
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];

  // Check for listener leak
  if (isObject(this._events[type]) && !this._events[type].warned) {
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        // not supported in IE 10
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  // not listening for removeListener, no need to emit
  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else if (listeners) {
    // LIFO order
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.prototype.listenerCount = function(type) {
  if (this._events) {
    var evlistener = this._events[type];

    if (isFunction(evlistener))
      return 1;
    else if (evlistener)
      return evlistener.length;
  }
  return 0;
};

EventEmitter.listenerCount = function(emitter, type) {
  return emitter.listenerCount(type);
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}],85:[function(require,module,exports){
/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

"use strict";

/**
 * Constructs an enumeration with keys equal to their value.
 *
 * For example:
 *
 *   var COLORS = keyMirror({blue: null, red: null});
 *   var myColor = COLORS.blue;
 *   var isColorValid = !!COLORS[myColor];
 *
 * The last line could not be performed if the values of the generated enum were
 * not equal to their keys.
 *
 *   Input:  {key1: val1, key2: val2}
 *   Output: {key1: key1, key2: key2}
 *
 * @param {object} obj
 * @return {object}
 */
var keyMirror = function(obj) {
  var ret = {};
  var key;
  if (!(obj instanceof Object && !Array.isArray(obj))) {
    throw new Error('keyMirror(...): Argument must be an object.');
  }
  for (key in obj) {
    if (!obj.hasOwnProperty(key)) {
      continue;
    }
    ret[key] = key;
  }
  return ret;
};

module.exports = keyMirror;

},{}],86:[function(require,module,exports){
//! moment.js
//! version : 2.18.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global.moment = factory()
}(this, (function () { 'use strict';

var hookCallback;

function hooks () {
    return hookCallback.apply(null, arguments);
}

// This is done to register the method called with moment()
// without creating circular dependencies.
function setHookCallback (callback) {
    hookCallback = callback;
}

function isArray(input) {
    return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
}

function isObject(input) {
    // IE8 will treat undefined and null as object if it wasn't for
    // input != null
    return input != null && Object.prototype.toString.call(input) === '[object Object]';
}

function isObjectEmpty(obj) {
    var k;
    for (k in obj) {
        // even if its not own property I'd still call it non-empty
        return false;
    }
    return true;
}

function isUndefined(input) {
    return input === void 0;
}

function isNumber(input) {
    return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
}

function isDate(input) {
    return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
}

function map(arr, fn) {
    var res = [], i;
    for (i = 0; i < arr.length; ++i) {
        res.push(fn(arr[i], i));
    }
    return res;
}

function hasOwnProp(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b);
}

function extend(a, b) {
    for (var i in b) {
        if (hasOwnProp(b, i)) {
            a[i] = b[i];
        }
    }

    if (hasOwnProp(b, 'toString')) {
        a.toString = b.toString;
    }

    if (hasOwnProp(b, 'valueOf')) {
        a.valueOf = b.valueOf;
    }

    return a;
}

function createUTC (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, true).utc();
}

function defaultParsingFlags() {
    // We need to deep clone this object.
    return {
        empty           : false,
        unusedTokens    : [],
        unusedInput     : [],
        overflow        : -2,
        charsLeftOver   : 0,
        nullInput       : false,
        invalidMonth    : null,
        invalidFormat   : false,
        userInvalidated : false,
        iso             : false,
        parsedDateParts : [],
        meridiem        : null,
        rfc2822         : false,
        weekdayMismatch : false
    };
}

function getParsingFlags(m) {
    if (m._pf == null) {
        m._pf = defaultParsingFlags();
    }
    return m._pf;
}

var some;
if (Array.prototype.some) {
    some = Array.prototype.some;
} else {
    some = function (fun) {
        var t = Object(this);
        var len = t.length >>> 0;

        for (var i = 0; i < len; i++) {
            if (i in t && fun.call(this, t[i], i, t)) {
                return true;
            }
        }

        return false;
    };
}

var some$1 = some;

function isValid(m) {
    if (m._isValid == null) {
        var flags = getParsingFlags(m);
        var parsedParts = some$1.call(flags.parsedDateParts, function (i) {
            return i != null;
        });
        var isNowValid = !isNaN(m._d.getTime()) &&
            flags.overflow < 0 &&
            !flags.empty &&
            !flags.invalidMonth &&
            !flags.invalidWeekday &&
            !flags.nullInput &&
            !flags.invalidFormat &&
            !flags.userInvalidated &&
            (!flags.meridiem || (flags.meridiem && parsedParts));

        if (m._strict) {
            isNowValid = isNowValid &&
                flags.charsLeftOver === 0 &&
                flags.unusedTokens.length === 0 &&
                flags.bigHour === undefined;
        }

        if (Object.isFrozen == null || !Object.isFrozen(m)) {
            m._isValid = isNowValid;
        }
        else {
            return isNowValid;
        }
    }
    return m._isValid;
}

function createInvalid (flags) {
    var m = createUTC(NaN);
    if (flags != null) {
        extend(getParsingFlags(m), flags);
    }
    else {
        getParsingFlags(m).userInvalidated = true;
    }

    return m;
}

// Plugins that add properties should also add the key here (null value),
// so we can properly clone ourselves.
var momentProperties = hooks.momentProperties = [];

function copyConfig(to, from) {
    var i, prop, val;

    if (!isUndefined(from._isAMomentObject)) {
        to._isAMomentObject = from._isAMomentObject;
    }
    if (!isUndefined(from._i)) {
        to._i = from._i;
    }
    if (!isUndefined(from._f)) {
        to._f = from._f;
    }
    if (!isUndefined(from._l)) {
        to._l = from._l;
    }
    if (!isUndefined(from._strict)) {
        to._strict = from._strict;
    }
    if (!isUndefined(from._tzm)) {
        to._tzm = from._tzm;
    }
    if (!isUndefined(from._isUTC)) {
        to._isUTC = from._isUTC;
    }
    if (!isUndefined(from._offset)) {
        to._offset = from._offset;
    }
    if (!isUndefined(from._pf)) {
        to._pf = getParsingFlags(from);
    }
    if (!isUndefined(from._locale)) {
        to._locale = from._locale;
    }

    if (momentProperties.length > 0) {
        for (i = 0; i < momentProperties.length; i++) {
            prop = momentProperties[i];
            val = from[prop];
            if (!isUndefined(val)) {
                to[prop] = val;
            }
        }
    }

    return to;
}

var updateInProgress = false;

// Moment prototype object
function Moment(config) {
    copyConfig(this, config);
    this._d = new Date(config._d != null ? config._d.getTime() : NaN);
    if (!this.isValid()) {
        this._d = new Date(NaN);
    }
    // Prevent infinite loop in case updateOffset creates new moment
    // objects.
    if (updateInProgress === false) {
        updateInProgress = true;
        hooks.updateOffset(this);
        updateInProgress = false;
    }
}

function isMoment (obj) {
    return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
}

function absFloor (number) {
    if (number < 0) {
        // -0 -> 0
        return Math.ceil(number) || 0;
    } else {
        return Math.floor(number);
    }
}

function toInt(argumentForCoercion) {
    var coercedNumber = +argumentForCoercion,
        value = 0;

    if (coercedNumber !== 0 && isFinite(coercedNumber)) {
        value = absFloor(coercedNumber);
    }

    return value;
}

// compare two arrays, return the number of differences
function compareArrays(array1, array2, dontConvert) {
    var len = Math.min(array1.length, array2.length),
        lengthDiff = Math.abs(array1.length - array2.length),
        diffs = 0,
        i;
    for (i = 0; i < len; i++) {
        if ((dontConvert && array1[i] !== array2[i]) ||
            (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
            diffs++;
        }
    }
    return diffs + lengthDiff;
}

function warn(msg) {
    if (hooks.suppressDeprecationWarnings === false &&
            (typeof console !==  'undefined') && console.warn) {
        console.warn('Deprecation warning: ' + msg);
    }
}

function deprecate(msg, fn) {
    var firstTime = true;

    return extend(function () {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(null, msg);
        }
        if (firstTime) {
            var args = [];
            var arg;
            for (var i = 0; i < arguments.length; i++) {
                arg = '';
                if (typeof arguments[i] === 'object') {
                    arg += '\n[' + i + '] ';
                    for (var key in arguments[0]) {
                        arg += key + ': ' + arguments[0][key] + ', ';
                    }
                    arg = arg.slice(0, -2); // Remove trailing comma and space
                } else {
                    arg = arguments[i];
                }
                args.push(arg);
            }
            warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
            firstTime = false;
        }
        return fn.apply(this, arguments);
    }, fn);
}

var deprecations = {};

function deprecateSimple(name, msg) {
    if (hooks.deprecationHandler != null) {
        hooks.deprecationHandler(name, msg);
    }
    if (!deprecations[name]) {
        warn(msg);
        deprecations[name] = true;
    }
}

hooks.suppressDeprecationWarnings = false;
hooks.deprecationHandler = null;

function isFunction(input) {
    return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
}

function set (config) {
    var prop, i;
    for (i in config) {
        prop = config[i];
        if (isFunction(prop)) {
            this[i] = prop;
        } else {
            this['_' + i] = prop;
        }
    }
    this._config = config;
    // Lenient ordinal parsing accepts just a number in addition to
    // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
    // TODO: Remove "ordinalParse" fallback in next major release.
    this._dayOfMonthOrdinalParseLenient = new RegExp(
        (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
            '|' + (/\d{1,2}/).source);
}

function mergeConfigs(parentConfig, childConfig) {
    var res = extend({}, parentConfig), prop;
    for (prop in childConfig) {
        if (hasOwnProp(childConfig, prop)) {
            if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                res[prop] = {};
                extend(res[prop], parentConfig[prop]);
                extend(res[prop], childConfig[prop]);
            } else if (childConfig[prop] != null) {
                res[prop] = childConfig[prop];
            } else {
                delete res[prop];
            }
        }
    }
    for (prop in parentConfig) {
        if (hasOwnProp(parentConfig, prop) &&
                !hasOwnProp(childConfig, prop) &&
                isObject(parentConfig[prop])) {
            // make sure changes to properties don't modify parent config
            res[prop] = extend({}, res[prop]);
        }
    }
    return res;
}

function Locale(config) {
    if (config != null) {
        this.set(config);
    }
}

var keys;

if (Object.keys) {
    keys = Object.keys;
} else {
    keys = function (obj) {
        var i, res = [];
        for (i in obj) {
            if (hasOwnProp(obj, i)) {
                res.push(i);
            }
        }
        return res;
    };
}

var keys$1 = keys;

var defaultCalendar = {
    sameDay : '[Today at] LT',
    nextDay : '[Tomorrow at] LT',
    nextWeek : 'dddd [at] LT',
    lastDay : '[Yesterday at] LT',
    lastWeek : '[Last] dddd [at] LT',
    sameElse : 'L'
};

function calendar (key, mom, now) {
    var output = this._calendar[key] || this._calendar['sameElse'];
    return isFunction(output) ? output.call(mom, now) : output;
}

var defaultLongDateFormat = {
    LTS  : 'h:mm:ss A',
    LT   : 'h:mm A',
    L    : 'MM/DD/YYYY',
    LL   : 'MMMM D, YYYY',
    LLL  : 'MMMM D, YYYY h:mm A',
    LLLL : 'dddd, MMMM D, YYYY h:mm A'
};

function longDateFormat (key) {
    var format = this._longDateFormat[key],
        formatUpper = this._longDateFormat[key.toUpperCase()];

    if (format || !formatUpper) {
        return format;
    }

    this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
        return val.slice(1);
    });

    return this._longDateFormat[key];
}

var defaultInvalidDate = 'Invalid date';

function invalidDate () {
    return this._invalidDate;
}

var defaultOrdinal = '%d';
var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

function ordinal (number) {
    return this._ordinal.replace('%d', number);
}

var defaultRelativeTime = {
    future : 'in %s',
    past   : '%s ago',
    s  : 'a few seconds',
    ss : '%d seconds',
    m  : 'a minute',
    mm : '%d minutes',
    h  : 'an hour',
    hh : '%d hours',
    d  : 'a day',
    dd : '%d days',
    M  : 'a month',
    MM : '%d months',
    y  : 'a year',
    yy : '%d years'
};

function relativeTime (number, withoutSuffix, string, isFuture) {
    var output = this._relativeTime[string];
    return (isFunction(output)) ?
        output(number, withoutSuffix, string, isFuture) :
        output.replace(/%d/i, number);
}

function pastFuture (diff, output) {
    var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
    return isFunction(format) ? format(output) : format.replace(/%s/i, output);
}

var aliases = {};

function addUnitAlias (unit, shorthand) {
    var lowerCase = unit.toLowerCase();
    aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
}

function normalizeUnits(units) {
    return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
}

function normalizeObjectUnits(inputObject) {
    var normalizedInput = {},
        normalizedProp,
        prop;

    for (prop in inputObject) {
        if (hasOwnProp(inputObject, prop)) {
            normalizedProp = normalizeUnits(prop);
            if (normalizedProp) {
                normalizedInput[normalizedProp] = inputObject[prop];
            }
        }
    }

    return normalizedInput;
}

var priorities = {};

function addUnitPriority(unit, priority) {
    priorities[unit] = priority;
}

function getPrioritizedUnits(unitsObj) {
    var units = [];
    for (var u in unitsObj) {
        units.push({unit: u, priority: priorities[u]});
    }
    units.sort(function (a, b) {
        return a.priority - b.priority;
    });
    return units;
}

function makeGetSet (unit, keepTime) {
    return function (value) {
        if (value != null) {
            set$1(this, unit, value);
            hooks.updateOffset(this, keepTime);
            return this;
        } else {
            return get(this, unit);
        }
    };
}

function get (mom, unit) {
    return mom.isValid() ?
        mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
}

function set$1 (mom, unit, value) {
    if (mom.isValid()) {
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
    }
}

// MOMENTS

function stringGet (units) {
    units = normalizeUnits(units);
    if (isFunction(this[units])) {
        return this[units]();
    }
    return this;
}


function stringSet (units, value) {
    if (typeof units === 'object') {
        units = normalizeObjectUnits(units);
        var prioritized = getPrioritizedUnits(units);
        for (var i = 0; i < prioritized.length; i++) {
            this[prioritized[i].unit](units[prioritized[i].unit]);
        }
    } else {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units](value);
        }
    }
    return this;
}

function zeroFill(number, targetLength, forceSign) {
    var absNumber = '' + Math.abs(number),
        zerosToFill = targetLength - absNumber.length,
        sign = number >= 0;
    return (sign ? (forceSign ? '+' : '') : '-') +
        Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
}

var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

var formatFunctions = {};

var formatTokenFunctions = {};

// token:    'M'
// padded:   ['MM', 2]
// ordinal:  'Mo'
// callback: function () { this.month() + 1 }
function addFormatToken (token, padded, ordinal, callback) {
    var func = callback;
    if (typeof callback === 'string') {
        func = function () {
            return this[callback]();
        };
    }
    if (token) {
        formatTokenFunctions[token] = func;
    }
    if (padded) {
        formatTokenFunctions[padded[0]] = function () {
            return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
        };
    }
    if (ordinal) {
        formatTokenFunctions[ordinal] = function () {
            return this.localeData().ordinal(func.apply(this, arguments), token);
        };
    }
}

function removeFormattingTokens(input) {
    if (input.match(/\[[\s\S]/)) {
        return input.replace(/^\[|\]$/g, '');
    }
    return input.replace(/\\/g, '');
}

function makeFormatFunction(format) {
    var array = format.match(formattingTokens), i, length;

    for (i = 0, length = array.length; i < length; i++) {
        if (formatTokenFunctions[array[i]]) {
            array[i] = formatTokenFunctions[array[i]];
        } else {
            array[i] = removeFormattingTokens(array[i]);
        }
    }

    return function (mom) {
        var output = '', i;
        for (i = 0; i < length; i++) {
            output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
        }
        return output;
    };
}

// format date using native date object
function formatMoment(m, format) {
    if (!m.isValid()) {
        return m.localeData().invalidDate();
    }

    format = expandFormat(format, m.localeData());
    formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

    return formatFunctions[format](m);
}

function expandFormat(format, locale) {
    var i = 5;

    function replaceLongDateFormatTokens(input) {
        return locale.longDateFormat(input) || input;
    }

    localFormattingTokens.lastIndex = 0;
    while (i >= 0 && localFormattingTokens.test(format)) {
        format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
        localFormattingTokens.lastIndex = 0;
        i -= 1;
    }

    return format;
}

var match1         = /\d/;            //       0 - 9
var match2         = /\d\d/;          //      00 - 99
var match3         = /\d{3}/;         //     000 - 999
var match4         = /\d{4}/;         //    0000 - 9999
var match6         = /[+-]?\d{6}/;    // -999999 - 999999
var match1to2      = /\d\d?/;         //       0 - 99
var match3to4      = /\d\d\d\d?/;     //     999 - 9999
var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
var match1to3      = /\d{1,3}/;       //       0 - 999
var match1to4      = /\d{1,4}/;       //       0 - 9999
var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

var matchUnsigned  = /\d+/;           //       0 - inf
var matchSigned    = /[+-]?\d+/;      //    -inf - inf

var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

// any word (or two) characters or numbers including two/three word month in arabic.
// includes scottish gaelic two word and hyphenated months
var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;


var regexes = {};

function addRegexToken (token, regex, strictRegex) {
    regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
        return (isStrict && strictRegex) ? strictRegex : regex;
    };
}

function getParseRegexForToken (token, config) {
    if (!hasOwnProp(regexes, token)) {
        return new RegExp(unescapeFormat(token));
    }

    return regexes[token](config._strict, config._locale);
}

// Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
function unescapeFormat(s) {
    return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
        return p1 || p2 || p3 || p4;
    }));
}

function regexEscape(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

var tokens = {};

function addParseToken (token, callback) {
    var i, func = callback;
    if (typeof token === 'string') {
        token = [token];
    }
    if (isNumber(callback)) {
        func = function (input, array) {
            array[callback] = toInt(input);
        };
    }
    for (i = 0; i < token.length; i++) {
        tokens[token[i]] = func;
    }
}

function addWeekParseToken (token, callback) {
    addParseToken(token, function (input, array, config, token) {
        config._w = config._w || {};
        callback(input, config._w, config, token);
    });
}

function addTimeToArrayFromToken(token, input, config) {
    if (input != null && hasOwnProp(tokens, token)) {
        tokens[token](input, config._a, config, token);
    }
}

var YEAR = 0;
var MONTH = 1;
var DATE = 2;
var HOUR = 3;
var MINUTE = 4;
var SECOND = 5;
var MILLISECOND = 6;
var WEEK = 7;
var WEEKDAY = 8;

var indexOf;

if (Array.prototype.indexOf) {
    indexOf = Array.prototype.indexOf;
} else {
    indexOf = function (o) {
        // I know
        var i;
        for (i = 0; i < this.length; ++i) {
            if (this[i] === o) {
                return i;
            }
        }
        return -1;
    };
}

var indexOf$1 = indexOf;

function daysInMonth(year, month) {
    return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
}

// FORMATTING

addFormatToken('M', ['MM', 2], 'Mo', function () {
    return this.month() + 1;
});

addFormatToken('MMM', 0, 0, function (format) {
    return this.localeData().monthsShort(this, format);
});

addFormatToken('MMMM', 0, 0, function (format) {
    return this.localeData().months(this, format);
});

// ALIASES

addUnitAlias('month', 'M');

// PRIORITY

addUnitPriority('month', 8);

// PARSING

addRegexToken('M',    match1to2);
addRegexToken('MM',   match1to2, match2);
addRegexToken('MMM',  function (isStrict, locale) {
    return locale.monthsShortRegex(isStrict);
});
addRegexToken('MMMM', function (isStrict, locale) {
    return locale.monthsRegex(isStrict);
});

addParseToken(['M', 'MM'], function (input, array) {
    array[MONTH] = toInt(input) - 1;
});

addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
    var month = config._locale.monthsParse(input, token, config._strict);
    // if we didn't find a month name, mark the date as invalid.
    if (month != null) {
        array[MONTH] = month;
    } else {
        getParsingFlags(config).invalidMonth = input;
    }
});

// LOCALES

var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
function localeMonths (m, format) {
    if (!m) {
        return isArray(this._months) ? this._months :
            this._months['standalone'];
    }
    return isArray(this._months) ? this._months[m.month()] :
        this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
}

var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
function localeMonthsShort (m, format) {
    if (!m) {
        return isArray(this._monthsShort) ? this._monthsShort :
            this._monthsShort['standalone'];
    }
    return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
        this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
}

function handleStrictParse(monthName, format, strict) {
    var i, ii, mom, llc = monthName.toLocaleLowerCase();
    if (!this._monthsParse) {
        // this is not used
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
        for (i = 0; i < 12; ++i) {
            mom = createUTC([2000, i]);
            this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
            this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'MMM') {
            ii = indexOf$1.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'MMM') {
            ii = indexOf$1.call(this._shortMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._longMonthsParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._longMonthsParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._shortMonthsParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeMonthsParse (monthName, format, strict) {
    var i, mom, regex;

    if (this._monthsParseExact) {
        return handleStrictParse.call(this, monthName, format, strict);
    }

    if (!this._monthsParse) {
        this._monthsParse = [];
        this._longMonthsParse = [];
        this._shortMonthsParse = [];
    }

    // TODO: add sorting
    // Sorting makes sure if one month (or abbr) is a prefix of another
    // see sorting in computeMonthsParse
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        if (strict && !this._longMonthsParse[i]) {
            this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
            this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
        }
        if (!strict && !this._monthsParse[i]) {
            regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
            this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
            return i;
        } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
            return i;
        } else if (!strict && this._monthsParse[i].test(monthName)) {
            return i;
        }
    }
}

// MOMENTS

function setMonth (mom, value) {
    var dayOfMonth;

    if (!mom.isValid()) {
        // No op
        return mom;
    }

    if (typeof value === 'string') {
        if (/^\d+$/.test(value)) {
            value = toInt(value);
        } else {
            value = mom.localeData().monthsParse(value);
            // TODO: Another silent failure?
            if (!isNumber(value)) {
                return mom;
            }
        }
    }

    dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
    mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
    return mom;
}

function getSetMonth (value) {
    if (value != null) {
        setMonth(this, value);
        hooks.updateOffset(this, true);
        return this;
    } else {
        return get(this, 'Month');
    }
}

function getDaysInMonth () {
    return daysInMonth(this.year(), this.month());
}

var defaultMonthsShortRegex = matchWord;
function monthsShortRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsShortStrictRegex;
        } else {
            return this._monthsShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsShortRegex')) {
            this._monthsShortRegex = defaultMonthsShortRegex;
        }
        return this._monthsShortStrictRegex && isStrict ?
            this._monthsShortStrictRegex : this._monthsShortRegex;
    }
}

var defaultMonthsRegex = matchWord;
function monthsRegex (isStrict) {
    if (this._monthsParseExact) {
        if (!hasOwnProp(this, '_monthsRegex')) {
            computeMonthsParse.call(this);
        }
        if (isStrict) {
            return this._monthsStrictRegex;
        } else {
            return this._monthsRegex;
        }
    } else {
        if (!hasOwnProp(this, '_monthsRegex')) {
            this._monthsRegex = defaultMonthsRegex;
        }
        return this._monthsStrictRegex && isStrict ?
            this._monthsStrictRegex : this._monthsRegex;
    }
}

function computeMonthsParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom;
    for (i = 0; i < 12; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, i]);
        shortPieces.push(this.monthsShort(mom, ''));
        longPieces.push(this.months(mom, ''));
        mixedPieces.push(this.months(mom, ''));
        mixedPieces.push(this.monthsShort(mom, ''));
    }
    // Sorting makes sure if one month (or abbr) is a prefix of another it
    // will match the longer piece.
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 12; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
    }
    for (i = 0; i < 24; i++) {
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._monthsShortRegex = this._monthsRegex;
    this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
}

// FORMATTING

addFormatToken('Y', 0, 0, function () {
    var y = this.year();
    return y <= 9999 ? '' + y : '+' + y;
});

addFormatToken(0, ['YY', 2], 0, function () {
    return this.year() % 100;
});

addFormatToken(0, ['YYYY',   4],       0, 'year');
addFormatToken(0, ['YYYYY',  5],       0, 'year');
addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

// ALIASES

addUnitAlias('year', 'y');

// PRIORITIES

addUnitPriority('year', 1);

// PARSING

addRegexToken('Y',      matchSigned);
addRegexToken('YY',     match1to2, match2);
addRegexToken('YYYY',   match1to4, match4);
addRegexToken('YYYYY',  match1to6, match6);
addRegexToken('YYYYYY', match1to6, match6);

addParseToken(['YYYYY', 'YYYYYY'], YEAR);
addParseToken('YYYY', function (input, array) {
    array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
});
addParseToken('YY', function (input, array) {
    array[YEAR] = hooks.parseTwoDigitYear(input);
});
addParseToken('Y', function (input, array) {
    array[YEAR] = parseInt(input, 10);
});

// HELPERS

function daysInYear(year) {
    return isLeapYear(year) ? 366 : 365;
}

function isLeapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

// HOOKS

hooks.parseTwoDigitYear = function (input) {
    return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
};

// MOMENTS

var getSetYear = makeGetSet('FullYear', true);

function getIsLeapYear () {
    return isLeapYear(this.year());
}

function createDate (y, m, d, h, M, s, ms) {
    // can't just apply() to create a date:
    // https://stackoverflow.com/q/181348
    var date = new Date(y, m, d, h, M, s, ms);

    // the date constructor remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
        date.setFullYear(y);
    }
    return date;
}

function createUTCDate (y) {
    var date = new Date(Date.UTC.apply(null, arguments));

    // the Date.UTC function remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
        date.setUTCFullYear(y);
    }
    return date;
}

// start-of-first-week - start-of-year
function firstWeekOffset(year, dow, doy) {
    var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
        fwd = 7 + dow - doy,
        // first-week day local weekday -- which local weekday is fwd
        fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

    return -fwdlw + fwd - 1;
}

// https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
    var localWeekday = (7 + weekday - dow) % 7,
        weekOffset = firstWeekOffset(year, dow, doy),
        dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
        resYear, resDayOfYear;

    if (dayOfYear <= 0) {
        resYear = year - 1;
        resDayOfYear = daysInYear(resYear) + dayOfYear;
    } else if (dayOfYear > daysInYear(year)) {
        resYear = year + 1;
        resDayOfYear = dayOfYear - daysInYear(year);
    } else {
        resYear = year;
        resDayOfYear = dayOfYear;
    }

    return {
        year: resYear,
        dayOfYear: resDayOfYear
    };
}

function weekOfYear(mom, dow, doy) {
    var weekOffset = firstWeekOffset(mom.year(), dow, doy),
        week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
        resWeek, resYear;

    if (week < 1) {
        resYear = mom.year() - 1;
        resWeek = week + weeksInYear(resYear, dow, doy);
    } else if (week > weeksInYear(mom.year(), dow, doy)) {
        resWeek = week - weeksInYear(mom.year(), dow, doy);
        resYear = mom.year() + 1;
    } else {
        resYear = mom.year();
        resWeek = week;
    }

    return {
        week: resWeek,
        year: resYear
    };
}

function weeksInYear(year, dow, doy) {
    var weekOffset = firstWeekOffset(year, dow, doy),
        weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
    return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
}

// FORMATTING

addFormatToken('w', ['ww', 2], 'wo', 'week');
addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

// ALIASES

addUnitAlias('week', 'w');
addUnitAlias('isoWeek', 'W');

// PRIORITIES

addUnitPriority('week', 5);
addUnitPriority('isoWeek', 5);

// PARSING

addRegexToken('w',  match1to2);
addRegexToken('ww', match1to2, match2);
addRegexToken('W',  match1to2);
addRegexToken('WW', match1to2, match2);

addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
    week[token.substr(0, 1)] = toInt(input);
});

// HELPERS

// LOCALES

function localeWeek (mom) {
    return weekOfYear(mom, this._week.dow, this._week.doy).week;
}

var defaultLocaleWeek = {
    dow : 0, // Sunday is the first day of the week.
    doy : 6  // The week that contains Jan 1st is the first week of the year.
};

function localeFirstDayOfWeek () {
    return this._week.dow;
}

function localeFirstDayOfYear () {
    return this._week.doy;
}

// MOMENTS

function getSetWeek (input) {
    var week = this.localeData().week(this);
    return input == null ? week : this.add((input - week) * 7, 'd');
}

function getSetISOWeek (input) {
    var week = weekOfYear(this, 1, 4).week;
    return input == null ? week : this.add((input - week) * 7, 'd');
}

// FORMATTING

addFormatToken('d', 0, 'do', 'day');

addFormatToken('dd', 0, 0, function (format) {
    return this.localeData().weekdaysMin(this, format);
});

addFormatToken('ddd', 0, 0, function (format) {
    return this.localeData().weekdaysShort(this, format);
});

addFormatToken('dddd', 0, 0, function (format) {
    return this.localeData().weekdays(this, format);
});

addFormatToken('e', 0, 0, 'weekday');
addFormatToken('E', 0, 0, 'isoWeekday');

// ALIASES

addUnitAlias('day', 'd');
addUnitAlias('weekday', 'e');
addUnitAlias('isoWeekday', 'E');

// PRIORITY
addUnitPriority('day', 11);
addUnitPriority('weekday', 11);
addUnitPriority('isoWeekday', 11);

// PARSING

addRegexToken('d',    match1to2);
addRegexToken('e',    match1to2);
addRegexToken('E',    match1to2);
addRegexToken('dd',   function (isStrict, locale) {
    return locale.weekdaysMinRegex(isStrict);
});
addRegexToken('ddd',   function (isStrict, locale) {
    return locale.weekdaysShortRegex(isStrict);
});
addRegexToken('dddd',   function (isStrict, locale) {
    return locale.weekdaysRegex(isStrict);
});

addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
    var weekday = config._locale.weekdaysParse(input, token, config._strict);
    // if we didn't get a weekday name, mark the date as invalid
    if (weekday != null) {
        week.d = weekday;
    } else {
        getParsingFlags(config).invalidWeekday = input;
    }
});

addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
    week[token] = toInt(input);
});

// HELPERS

function parseWeekday(input, locale) {
    if (typeof input !== 'string') {
        return input;
    }

    if (!isNaN(input)) {
        return parseInt(input, 10);
    }

    input = locale.weekdaysParse(input);
    if (typeof input === 'number') {
        return input;
    }

    return null;
}

function parseIsoWeekday(input, locale) {
    if (typeof input === 'string') {
        return locale.weekdaysParse(input) % 7 || 7;
    }
    return isNaN(input) ? null : input;
}

// LOCALES

var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
function localeWeekdays (m, format) {
    if (!m) {
        return isArray(this._weekdays) ? this._weekdays :
            this._weekdays['standalone'];
    }
    return isArray(this._weekdays) ? this._weekdays[m.day()] :
        this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
}

var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
function localeWeekdaysShort (m) {
    return (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
}

var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
function localeWeekdaysMin (m) {
    return (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
}

function handleStrictParse$1(weekdayName, format, strict) {
    var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._minWeekdaysParse = [];

        for (i = 0; i < 7; ++i) {
            mom = createUTC([2000, 1]).day(i);
            this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
            this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
            this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
        }
    }

    if (strict) {
        if (format === 'dddd') {
            ii = indexOf$1.call(this._weekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    } else {
        if (format === 'dddd') {
            ii = indexOf$1.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else if (format === 'ddd') {
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        } else {
            ii = indexOf$1.call(this._minWeekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._weekdaysParse, llc);
            if (ii !== -1) {
                return ii;
            }
            ii = indexOf$1.call(this._shortWeekdaysParse, llc);
            return ii !== -1 ? ii : null;
        }
    }
}

function localeWeekdaysParse (weekdayName, format, strict) {
    var i, mom, regex;

    if (this._weekdaysParseExact) {
        return handleStrictParse$1.call(this, weekdayName, format, strict);
    }

    if (!this._weekdaysParse) {
        this._weekdaysParse = [];
        this._minWeekdaysParse = [];
        this._shortWeekdaysParse = [];
        this._fullWeekdaysParse = [];
    }

    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already

        mom = createUTC([2000, 1]).day(i);
        if (strict && !this._fullWeekdaysParse[i]) {
            this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
            this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
            this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
        }
        if (!this._weekdaysParse[i]) {
            regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
            this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
        }
        // test the regex
        if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
            return i;
        } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
            return i;
        }
    }
}

// MOMENTS

function getSetDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
    if (input != null) {
        input = parseWeekday(input, this.localeData());
        return this.add(input - day, 'd');
    } else {
        return day;
    }
}

function getSetLocaleDayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
    return input == null ? weekday : this.add(input - weekday, 'd');
}

function getSetISODayOfWeek (input) {
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }

    // behaves the same as moment#day except
    // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
    // as a setter, sunday should belong to the previous week.

    if (input != null) {
        var weekday = parseIsoWeekday(input, this.localeData());
        return this.day(this.day() % 7 ? weekday : weekday - 7);
    } else {
        return this.day() || 7;
    }
}

var defaultWeekdaysRegex = matchWord;
function weekdaysRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysStrictRegex;
        } else {
            return this._weekdaysRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            this._weekdaysRegex = defaultWeekdaysRegex;
        }
        return this._weekdaysStrictRegex && isStrict ?
            this._weekdaysStrictRegex : this._weekdaysRegex;
    }
}

var defaultWeekdaysShortRegex = matchWord;
function weekdaysShortRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysShortStrictRegex;
        } else {
            return this._weekdaysShortRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysShortRegex')) {
            this._weekdaysShortRegex = defaultWeekdaysShortRegex;
        }
        return this._weekdaysShortStrictRegex && isStrict ?
            this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
    }
}

var defaultWeekdaysMinRegex = matchWord;
function weekdaysMinRegex (isStrict) {
    if (this._weekdaysParseExact) {
        if (!hasOwnProp(this, '_weekdaysRegex')) {
            computeWeekdaysParse.call(this);
        }
        if (isStrict) {
            return this._weekdaysMinStrictRegex;
        } else {
            return this._weekdaysMinRegex;
        }
    } else {
        if (!hasOwnProp(this, '_weekdaysMinRegex')) {
            this._weekdaysMinRegex = defaultWeekdaysMinRegex;
        }
        return this._weekdaysMinStrictRegex && isStrict ?
            this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
    }
}


function computeWeekdaysParse () {
    function cmpLenRev(a, b) {
        return b.length - a.length;
    }

    var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
        i, mom, minp, shortp, longp;
    for (i = 0; i < 7; i++) {
        // make the regex if we don't have it already
        mom = createUTC([2000, 1]).day(i);
        minp = this.weekdaysMin(mom, '');
        shortp = this.weekdaysShort(mom, '');
        longp = this.weekdays(mom, '');
        minPieces.push(minp);
        shortPieces.push(shortp);
        longPieces.push(longp);
        mixedPieces.push(minp);
        mixedPieces.push(shortp);
        mixedPieces.push(longp);
    }
    // Sorting makes sure if one weekday (or abbr) is a prefix of another it
    // will match the longer piece.
    minPieces.sort(cmpLenRev);
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);
    for (i = 0; i < 7; i++) {
        shortPieces[i] = regexEscape(shortPieces[i]);
        longPieces[i] = regexEscape(longPieces[i]);
        mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._weekdaysShortRegex = this._weekdaysRegex;
    this._weekdaysMinRegex = this._weekdaysRegex;

    this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
}

// FORMATTING

function hFormat() {
    return this.hours() % 12 || 12;
}

function kFormat() {
    return this.hours() || 24;
}

addFormatToken('H', ['HH', 2], 0, 'hour');
addFormatToken('h', ['hh', 2], 0, hFormat);
addFormatToken('k', ['kk', 2], 0, kFormat);

addFormatToken('hmm', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
});

addFormatToken('hmmss', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

addFormatToken('Hmm', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2);
});

addFormatToken('Hmmss', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2) +
        zeroFill(this.seconds(), 2);
});

function meridiem (token, lowercase) {
    addFormatToken(token, 0, 0, function () {
        return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
    });
}

meridiem('a', true);
meridiem('A', false);

// ALIASES

addUnitAlias('hour', 'h');

// PRIORITY
addUnitPriority('hour', 13);

// PARSING

function matchMeridiem (isStrict, locale) {
    return locale._meridiemParse;
}

addRegexToken('a',  matchMeridiem);
addRegexToken('A',  matchMeridiem);
addRegexToken('H',  match1to2);
addRegexToken('h',  match1to2);
addRegexToken('k',  match1to2);
addRegexToken('HH', match1to2, match2);
addRegexToken('hh', match1to2, match2);
addRegexToken('kk', match1to2, match2);

addRegexToken('hmm', match3to4);
addRegexToken('hmmss', match5to6);
addRegexToken('Hmm', match3to4);
addRegexToken('Hmmss', match5to6);

addParseToken(['H', 'HH'], HOUR);
addParseToken(['k', 'kk'], function (input, array, config) {
    var kInput = toInt(input);
    array[HOUR] = kInput === 24 ? 0 : kInput;
});
addParseToken(['a', 'A'], function (input, array, config) {
    config._isPm = config._locale.isPM(input);
    config._meridiem = input;
});
addParseToken(['h', 'hh'], function (input, array, config) {
    array[HOUR] = toInt(input);
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
    getParsingFlags(config).bigHour = true;
});
addParseToken('hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
    getParsingFlags(config).bigHour = true;
});
addParseToken('Hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
});
addParseToken('Hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
});

// LOCALES

function localeIsPM (input) {
    // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
    // Using charAt should be more compatible.
    return ((input + '').toLowerCase().charAt(0) === 'p');
}

var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
function localeMeridiem (hours, minutes, isLower) {
    if (hours > 11) {
        return isLower ? 'pm' : 'PM';
    } else {
        return isLower ? 'am' : 'AM';
    }
}


// MOMENTS

// Setting the hour should keep the time, because the user explicitly
// specified which hour he wants. So trying to maintain the same hour (in
// a new timezone) makes sense. Adding/subtracting hours does not follow
// this rule.
var getSetHour = makeGetSet('Hours', true);

// months
// week
// weekdays
// meridiem
var baseConfig = {
    calendar: defaultCalendar,
    longDateFormat: defaultLongDateFormat,
    invalidDate: defaultInvalidDate,
    ordinal: defaultOrdinal,
    dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
    relativeTime: defaultRelativeTime,

    months: defaultLocaleMonths,
    monthsShort: defaultLocaleMonthsShort,

    week: defaultLocaleWeek,

    weekdays: defaultLocaleWeekdays,
    weekdaysMin: defaultLocaleWeekdaysMin,
    weekdaysShort: defaultLocaleWeekdaysShort,

    meridiemParse: defaultLocaleMeridiemParse
};

// internal storage for locale config files
var locales = {};
var localeFamilies = {};
var globalLocale;

function normalizeLocale(key) {
    return key ? key.toLowerCase().replace('_', '-') : key;
}

// pick the locale from the array
// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
function chooseLocale(names) {
    var i = 0, j, next, locale, split;

    while (i < names.length) {
        split = normalizeLocale(names[i]).split('-');
        j = split.length;
        next = normalizeLocale(names[i + 1]);
        next = next ? next.split('-') : null;
        while (j > 0) {
            locale = loadLocale(split.slice(0, j).join('-'));
            if (locale) {
                return locale;
            }
            if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                //the next array item is better than a shallower substring of this one
                break;
            }
            j--;
        }
        i++;
    }
    return null;
}

function loadLocale(name) {
    var oldLocale = null;
    // TODO: Find a better way to register and load all the locales in Node
    if (!locales[name] && (typeof module !== 'undefined') &&
            module && module.exports) {
        try {
            oldLocale = globalLocale._abbr;
            require('./locale/' + name);
            // because defineLocale currently also sets the global locale, we
            // want to undo that for lazy loaded locales
            getSetGlobalLocale(oldLocale);
        } catch (e) { }
    }
    return locales[name];
}

// This function will load locale and then set the global locale.  If
// no arguments are passed in, it will simply return the current global
// locale key.
function getSetGlobalLocale (key, values) {
    var data;
    if (key) {
        if (isUndefined(values)) {
            data = getLocale(key);
        }
        else {
            data = defineLocale(key, values);
        }

        if (data) {
            // moment.duration._locale = moment._locale = data;
            globalLocale = data;
        }
    }

    return globalLocale._abbr;
}

function defineLocale (name, config) {
    if (config !== null) {
        var parentConfig = baseConfig;
        config.abbr = name;
        if (locales[name] != null) {
            deprecateSimple('defineLocaleOverride',
                    'use moment.updateLocale(localeName, config) to change ' +
                    'an existing locale. moment.defineLocale(localeName, ' +
                    'config) should only be used for creating a new locale ' +
                    'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
            parentConfig = locales[name]._config;
        } else if (config.parentLocale != null) {
            if (locales[config.parentLocale] != null) {
                parentConfig = locales[config.parentLocale]._config;
            } else {
                if (!localeFamilies[config.parentLocale]) {
                    localeFamilies[config.parentLocale] = [];
                }
                localeFamilies[config.parentLocale].push({
                    name: name,
                    config: config
                });
                return null;
            }
        }
        locales[name] = new Locale(mergeConfigs(parentConfig, config));

        if (localeFamilies[name]) {
            localeFamilies[name].forEach(function (x) {
                defineLocale(x.name, x.config);
            });
        }

        // backwards compat for now: also set the locale
        // make sure we set the locale AFTER all child locales have been
        // created, so we won't end up with the child locale set.
        getSetGlobalLocale(name);


        return locales[name];
    } else {
        // useful for testing
        delete locales[name];
        return null;
    }
}

function updateLocale(name, config) {
    if (config != null) {
        var locale, parentConfig = baseConfig;
        // MERGE
        if (locales[name] != null) {
            parentConfig = locales[name]._config;
        }
        config = mergeConfigs(parentConfig, config);
        locale = new Locale(config);
        locale.parentLocale = locales[name];
        locales[name] = locale;

        // backwards compat for now: also set the locale
        getSetGlobalLocale(name);
    } else {
        // pass null for config to unupdate, useful for tests
        if (locales[name] != null) {
            if (locales[name].parentLocale != null) {
                locales[name] = locales[name].parentLocale;
            } else if (locales[name] != null) {
                delete locales[name];
            }
        }
    }
    return locales[name];
}

// returns locale data
function getLocale (key) {
    var locale;

    if (key && key._locale && key._locale._abbr) {
        key = key._locale._abbr;
    }

    if (!key) {
        return globalLocale;
    }

    if (!isArray(key)) {
        //short-circuit everything else
        locale = loadLocale(key);
        if (locale) {
            return locale;
        }
        key = [key];
    }

    return chooseLocale(key);
}

function listLocales() {
    return keys$1(locales);
}

function checkOverflow (m) {
    var overflow;
    var a = m._a;

    if (a && getParsingFlags(m).overflow === -2) {
        overflow =
            a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
            a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
            a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
            a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
            a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
            a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
            -1;

        if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
            overflow = DATE;
        }
        if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
            overflow = WEEK;
        }
        if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
            overflow = WEEKDAY;
        }

        getParsingFlags(m).overflow = overflow;
    }

    return m;
}

// iso 8601 regex
// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

var isoDates = [
    ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
    ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
    ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
    ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
    ['YYYY-DDD', /\d{4}-\d{3}/],
    ['YYYY-MM', /\d{4}-\d\d/, false],
    ['YYYYYYMMDD', /[+-]\d{10}/],
    ['YYYYMMDD', /\d{8}/],
    // YYYYMM is NOT allowed by the standard
    ['GGGG[W]WWE', /\d{4}W\d{3}/],
    ['GGGG[W]WW', /\d{4}W\d{2}/, false],
    ['YYYYDDD', /\d{7}/]
];

// iso time formats and regexes
var isoTimes = [
    ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
    ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
    ['HH:mm:ss', /\d\d:\d\d:\d\d/],
    ['HH:mm', /\d\d:\d\d/],
    ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
    ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
    ['HHmmss', /\d\d\d\d\d\d/],
    ['HHmm', /\d\d\d\d/],
    ['HH', /\d\d/]
];

var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

// date from iso format
function configFromISO(config) {
    var i, l,
        string = config._i,
        match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
        allowTime, dateFormat, timeFormat, tzFormat;

    if (match) {
        getParsingFlags(config).iso = true;

        for (i = 0, l = isoDates.length; i < l; i++) {
            if (isoDates[i][1].exec(match[1])) {
                dateFormat = isoDates[i][0];
                allowTime = isoDates[i][2] !== false;
                break;
            }
        }
        if (dateFormat == null) {
            config._isValid = false;
            return;
        }
        if (match[3]) {
            for (i = 0, l = isoTimes.length; i < l; i++) {
                if (isoTimes[i][1].exec(match[3])) {
                    // match[2] should be 'T' or space
                    timeFormat = (match[2] || ' ') + isoTimes[i][0];
                    break;
                }
            }
            if (timeFormat == null) {
                config._isValid = false;
                return;
            }
        }
        if (!allowTime && timeFormat != null) {
            config._isValid = false;
            return;
        }
        if (match[4]) {
            if (tzRegex.exec(match[4])) {
                tzFormat = 'Z';
            } else {
                config._isValid = false;
                return;
            }
        }
        config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
        configFromStringAndFormat(config);
    } else {
        config._isValid = false;
    }
}

// RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
var basicRfcRegex = /^((?:Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d?\d\s(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(?:\d\d)?\d\d\s)(\d\d:\d\d)(\:\d\d)?(\s(?:UT|GMT|[ECMP][SD]T|[A-IK-Za-ik-z]|[+-]\d{4}))$/;

// date and time from ref 2822 format
function configFromRFC2822(config) {
    var string, match, dayFormat,
        dateFormat, timeFormat, tzFormat;
    var timezones = {
        ' GMT': ' +0000',
        ' EDT': ' -0400',
        ' EST': ' -0500',
        ' CDT': ' -0500',
        ' CST': ' -0600',
        ' MDT': ' -0600',
        ' MST': ' -0700',
        ' PDT': ' -0700',
        ' PST': ' -0800'
    };
    var military = 'YXWVUTSRQPONZABCDEFGHIKLM';
    var timezone, timezoneIndex;

    string = config._i
        .replace(/\([^\)]*\)|[\n\t]/g, ' ') // Remove comments and folding whitespace
        .replace(/(\s\s+)/g, ' ') // Replace multiple-spaces with a single space
        .replace(/^\s|\s$/g, ''); // Remove leading and trailing spaces
    match = basicRfcRegex.exec(string);

    if (match) {
        dayFormat = match[1] ? 'ddd' + ((match[1].length === 5) ? ', ' : ' ') : '';
        dateFormat = 'D MMM ' + ((match[2].length > 10) ? 'YYYY ' : 'YY ');
        timeFormat = 'HH:mm' + (match[4] ? ':ss' : '');

        // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
        if (match[1]) { // day of week given
            var momentDate = new Date(match[2]);
            var momentDay = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'][momentDate.getDay()];

            if (match[1].substr(0,3) !== momentDay) {
                getParsingFlags(config).weekdayMismatch = true;
                config._isValid = false;
                return;
            }
        }

        switch (match[5].length) {
            case 2: // military
                if (timezoneIndex === 0) {
                    timezone = ' +0000';
                } else {
                    timezoneIndex = military.indexOf(match[5][1].toUpperCase()) - 12;
                    timezone = ((timezoneIndex < 0) ? ' -' : ' +') +
                        (('' + timezoneIndex).replace(/^-?/, '0')).match(/..$/)[0] + '00';
                }
                break;
            case 4: // Zone
                timezone = timezones[match[5]];
                break;
            default: // UT or +/-9999
                timezone = timezones[' GMT'];
        }
        match[5] = timezone;
        config._i = match.splice(1).join('');
        tzFormat = ' ZZ';
        config._f = dayFormat + dateFormat + timeFormat + tzFormat;
        configFromStringAndFormat(config);
        getParsingFlags(config).rfc2822 = true;
    } else {
        config._isValid = false;
    }
}

// date from iso format or fallback
function configFromString(config) {
    var matched = aspNetJsonRegex.exec(config._i);

    if (matched !== null) {
        config._d = new Date(+matched[1]);
        return;
    }

    configFromISO(config);
    if (config._isValid === false) {
        delete config._isValid;
    } else {
        return;
    }

    configFromRFC2822(config);
    if (config._isValid === false) {
        delete config._isValid;
    } else {
        return;
    }

    // Final attempt, use Input Fallback
    hooks.createFromInputFallback(config);
}

hooks.createFromInputFallback = deprecate(
    'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
    'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
    'discouraged and will be removed in an upcoming major release. Please refer to ' +
    'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
    function (config) {
        config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
    }
);

// Pick the first defined of two or three arguments.
function defaults(a, b, c) {
    if (a != null) {
        return a;
    }
    if (b != null) {
        return b;
    }
    return c;
}

function currentDateArray(config) {
    // hooks is actually the exported moment object
    var nowValue = new Date(hooks.now());
    if (config._useUTC) {
        return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
    }
    return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
}

// convert an array to a date.
// the array should mirror the parameters below
// note: all values past the year are optional and will default to the lowest possible value.
// [year, month, day , hour, minute, second, millisecond]
function configFromArray (config) {
    var i, date, input = [], currentDate, yearToUse;

    if (config._d) {
        return;
    }

    currentDate = currentDateArray(config);

    //compute day of the year from weeks and weekdays
    if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
        dayOfYearFromWeekInfo(config);
    }

    //if the day of the year is set, figure out what it is
    if (config._dayOfYear != null) {
        yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

        if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
            getParsingFlags(config)._overflowDayOfYear = true;
        }

        date = createUTCDate(yearToUse, 0, config._dayOfYear);
        config._a[MONTH] = date.getUTCMonth();
        config._a[DATE] = date.getUTCDate();
    }

    // Default to current date.
    // * if no year, month, day of month are given, default to today
    // * if day of month is given, default month and year
    // * if month is given, default only year
    // * if year is given, don't default anything
    for (i = 0; i < 3 && config._a[i] == null; ++i) {
        config._a[i] = input[i] = currentDate[i];
    }

    // Zero out whatever was not defaulted, including time
    for (; i < 7; i++) {
        config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
    }

    // Check for 24:00:00.000
    if (config._a[HOUR] === 24 &&
            config._a[MINUTE] === 0 &&
            config._a[SECOND] === 0 &&
            config._a[MILLISECOND] === 0) {
        config._nextDay = true;
        config._a[HOUR] = 0;
    }

    config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
    // Apply timezone offset from input. The actual utcOffset can be changed
    // with parseZone.
    if (config._tzm != null) {
        config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
    }

    if (config._nextDay) {
        config._a[HOUR] = 24;
    }
}

function dayOfYearFromWeekInfo(config) {
    var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

    w = config._w;
    if (w.GG != null || w.W != null || w.E != null) {
        dow = 1;
        doy = 4;

        // TODO: We need to take the current isoWeekYear, but that depends on
        // how we interpret now (local, utc, fixed offset). So create
        // a now version of current config (take local/utc/offset flags, and
        // create now).
        weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
        week = defaults(w.W, 1);
        weekday = defaults(w.E, 1);
        if (weekday < 1 || weekday > 7) {
            weekdayOverflow = true;
        }
    } else {
        dow = config._locale._week.dow;
        doy = config._locale._week.doy;

        var curWeek = weekOfYear(createLocal(), dow, doy);

        weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

        // Default to current week.
        week = defaults(w.w, curWeek.week);

        if (w.d != null) {
            // weekday -- low day numbers are considered next week
            weekday = w.d;
            if (weekday < 0 || weekday > 6) {
                weekdayOverflow = true;
            }
        } else if (w.e != null) {
            // local weekday -- counting starts from begining of week
            weekday = w.e + dow;
            if (w.e < 0 || w.e > 6) {
                weekdayOverflow = true;
            }
        } else {
            // default to begining of week
            weekday = dow;
        }
    }
    if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
        getParsingFlags(config)._overflowWeeks = true;
    } else if (weekdayOverflow != null) {
        getParsingFlags(config)._overflowWeekday = true;
    } else {
        temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
        config._a[YEAR] = temp.year;
        config._dayOfYear = temp.dayOfYear;
    }
}

// constant that refers to the ISO standard
hooks.ISO_8601 = function () {};

// constant that refers to the RFC 2822 form
hooks.RFC_2822 = function () {};

// date from string and format string
function configFromStringAndFormat(config) {
    // TODO: Move this to another part of the creation flow to prevent circular deps
    if (config._f === hooks.ISO_8601) {
        configFromISO(config);
        return;
    }
    if (config._f === hooks.RFC_2822) {
        configFromRFC2822(config);
        return;
    }
    config._a = [];
    getParsingFlags(config).empty = true;

    // This array is used to make a Date, either with `new Date` or `Date.UTC`
    var string = '' + config._i,
        i, parsedInput, tokens, token, skipped,
        stringLength = string.length,
        totalParsedInputLength = 0;

    tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

    for (i = 0; i < tokens.length; i++) {
        token = tokens[i];
        parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
        // console.log('token', token, 'parsedInput', parsedInput,
        //         'regex', getParseRegexForToken(token, config));
        if (parsedInput) {
            skipped = string.substr(0, string.indexOf(parsedInput));
            if (skipped.length > 0) {
                getParsingFlags(config).unusedInput.push(skipped);
            }
            string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
            totalParsedInputLength += parsedInput.length;
        }
        // don't parse if it's not a known token
        if (formatTokenFunctions[token]) {
            if (parsedInput) {
                getParsingFlags(config).empty = false;
            }
            else {
                getParsingFlags(config).unusedTokens.push(token);
            }
            addTimeToArrayFromToken(token, parsedInput, config);
        }
        else if (config._strict && !parsedInput) {
            getParsingFlags(config).unusedTokens.push(token);
        }
    }

    // add remaining unparsed input length to the string
    getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
    if (string.length > 0) {
        getParsingFlags(config).unusedInput.push(string);
    }

    // clear _12h flag if hour is <= 12
    if (config._a[HOUR] <= 12 &&
        getParsingFlags(config).bigHour === true &&
        config._a[HOUR] > 0) {
        getParsingFlags(config).bigHour = undefined;
    }

    getParsingFlags(config).parsedDateParts = config._a.slice(0);
    getParsingFlags(config).meridiem = config._meridiem;
    // handle meridiem
    config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

    configFromArray(config);
    checkOverflow(config);
}


function meridiemFixWrap (locale, hour, meridiem) {
    var isPm;

    if (meridiem == null) {
        // nothing to do
        return hour;
    }
    if (locale.meridiemHour != null) {
        return locale.meridiemHour(hour, meridiem);
    } else if (locale.isPM != null) {
        // Fallback
        isPm = locale.isPM(meridiem);
        if (isPm && hour < 12) {
            hour += 12;
        }
        if (!isPm && hour === 12) {
            hour = 0;
        }
        return hour;
    } else {
        // this is not supposed to happen
        return hour;
    }
}

// date from string and array of format strings
function configFromStringAndArray(config) {
    var tempConfig,
        bestMoment,

        scoreToBeat,
        i,
        currentScore;

    if (config._f.length === 0) {
        getParsingFlags(config).invalidFormat = true;
        config._d = new Date(NaN);
        return;
    }

    for (i = 0; i < config._f.length; i++) {
        currentScore = 0;
        tempConfig = copyConfig({}, config);
        if (config._useUTC != null) {
            tempConfig._useUTC = config._useUTC;
        }
        tempConfig._f = config._f[i];
        configFromStringAndFormat(tempConfig);

        if (!isValid(tempConfig)) {
            continue;
        }

        // if there is any input that was not parsed add a penalty for that format
        currentScore += getParsingFlags(tempConfig).charsLeftOver;

        //or tokens
        currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

        getParsingFlags(tempConfig).score = currentScore;

        if (scoreToBeat == null || currentScore < scoreToBeat) {
            scoreToBeat = currentScore;
            bestMoment = tempConfig;
        }
    }

    extend(config, bestMoment || tempConfig);
}

function configFromObject(config) {
    if (config._d) {
        return;
    }

    var i = normalizeObjectUnits(config._i);
    config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
        return obj && parseInt(obj, 10);
    });

    configFromArray(config);
}

function createFromConfig (config) {
    var res = new Moment(checkOverflow(prepareConfig(config)));
    if (res._nextDay) {
        // Adding is smart enough around DST
        res.add(1, 'd');
        res._nextDay = undefined;
    }

    return res;
}

function prepareConfig (config) {
    var input = config._i,
        format = config._f;

    config._locale = config._locale || getLocale(config._l);

    if (input === null || (format === undefined && input === '')) {
        return createInvalid({nullInput: true});
    }

    if (typeof input === 'string') {
        config._i = input = config._locale.preparse(input);
    }

    if (isMoment(input)) {
        return new Moment(checkOverflow(input));
    } else if (isDate(input)) {
        config._d = input;
    } else if (isArray(format)) {
        configFromStringAndArray(config);
    } else if (format) {
        configFromStringAndFormat(config);
    }  else {
        configFromInput(config);
    }

    if (!isValid(config)) {
        config._d = null;
    }

    return config;
}

function configFromInput(config) {
    var input = config._i;
    if (isUndefined(input)) {
        config._d = new Date(hooks.now());
    } else if (isDate(input)) {
        config._d = new Date(input.valueOf());
    } else if (typeof input === 'string') {
        configFromString(config);
    } else if (isArray(input)) {
        config._a = map(input.slice(0), function (obj) {
            return parseInt(obj, 10);
        });
        configFromArray(config);
    } else if (isObject(input)) {
        configFromObject(config);
    } else if (isNumber(input)) {
        // from milliseconds
        config._d = new Date(input);
    } else {
        hooks.createFromInputFallback(config);
    }
}

function createLocalOrUTC (input, format, locale, strict, isUTC) {
    var c = {};

    if (locale === true || locale === false) {
        strict = locale;
        locale = undefined;
    }

    if ((isObject(input) && isObjectEmpty(input)) ||
            (isArray(input) && input.length === 0)) {
        input = undefined;
    }
    // object construction must be done this way.
    // https://github.com/moment/moment/issues/1423
    c._isAMomentObject = true;
    c._useUTC = c._isUTC = isUTC;
    c._l = locale;
    c._i = input;
    c._f = format;
    c._strict = strict;

    return createFromConfig(c);
}

function createLocal (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, false);
}

var prototypeMin = deprecate(
    'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other < this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

var prototypeMax = deprecate(
    'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
    function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other > this ? this : other;
        } else {
            return createInvalid();
        }
    }
);

// Pick a moment m from moments so that m[fn](other) is true for all
// other. This relies on the function fn to be transitive.
//
// moments should either be an array of moment objects or an array, whose
// first element is an array of moment objects.
function pickBy(fn, moments) {
    var res, i;
    if (moments.length === 1 && isArray(moments[0])) {
        moments = moments[0];
    }
    if (!moments.length) {
        return createLocal();
    }
    res = moments[0];
    for (i = 1; i < moments.length; ++i) {
        if (!moments[i].isValid() || moments[i][fn](res)) {
            res = moments[i];
        }
    }
    return res;
}

// TODO: Use [].sort instead?
function min () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isBefore', args);
}

function max () {
    var args = [].slice.call(arguments, 0);

    return pickBy('isAfter', args);
}

var now = function () {
    return Date.now ? Date.now() : +(new Date());
};

var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

function isDurationValid(m) {
    for (var key in m) {
        if (!(ordering.indexOf(key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
            return false;
        }
    }

    var unitHasDecimal = false;
    for (var i = 0; i < ordering.length; ++i) {
        if (m[ordering[i]]) {
            if (unitHasDecimal) {
                return false; // only allow non-integers for smallest unit
            }
            if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                unitHasDecimal = true;
            }
        }
    }

    return true;
}

function isValid$1() {
    return this._isValid;
}

function createInvalid$1() {
    return createDuration(NaN);
}

function Duration (duration) {
    var normalizedInput = normalizeObjectUnits(duration),
        years = normalizedInput.year || 0,
        quarters = normalizedInput.quarter || 0,
        months = normalizedInput.month || 0,
        weeks = normalizedInput.week || 0,
        days = normalizedInput.day || 0,
        hours = normalizedInput.hour || 0,
        minutes = normalizedInput.minute || 0,
        seconds = normalizedInput.second || 0,
        milliseconds = normalizedInput.millisecond || 0;

    this._isValid = isDurationValid(normalizedInput);

    // representation for dateAddRemove
    this._milliseconds = +milliseconds +
        seconds * 1e3 + // 1000
        minutes * 6e4 + // 1000 * 60
        hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
    // Because of dateAddRemove treats 24 hours as different from a
    // day when working around DST, we need to store them separately
    this._days = +days +
        weeks * 7;
    // It is impossible translate months into days without knowing
    // which months you are are talking about, so we have to store
    // it separately.
    this._months = +months +
        quarters * 3 +
        years * 12;

    this._data = {};

    this._locale = getLocale();

    this._bubble();
}

function isDuration (obj) {
    return obj instanceof Duration;
}

function absRound (number) {
    if (number < 0) {
        return Math.round(-1 * number) * -1;
    } else {
        return Math.round(number);
    }
}

// FORMATTING

function offset (token, separator) {
    addFormatToken(token, 0, 0, function () {
        var offset = this.utcOffset();
        var sign = '+';
        if (offset < 0) {
            offset = -offset;
            sign = '-';
        }
        return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
    });
}

offset('Z', ':');
offset('ZZ', '');

// PARSING

addRegexToken('Z',  matchShortOffset);
addRegexToken('ZZ', matchShortOffset);
addParseToken(['Z', 'ZZ'], function (input, array, config) {
    config._useUTC = true;
    config._tzm = offsetFromString(matchShortOffset, input);
});

// HELPERS

// timezone chunker
// '+10:00' > ['10',  '00']
// '-1530'  > ['-15', '30']
var chunkOffset = /([\+\-]|\d\d)/gi;

function offsetFromString(matcher, string) {
    var matches = (string || '').match(matcher);

    if (matches === null) {
        return null;
    }

    var chunk   = matches[matches.length - 1] || [];
    var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
    var minutes = +(parts[1] * 60) + toInt(parts[2]);

    return minutes === 0 ?
      0 :
      parts[0] === '+' ? minutes : -minutes;
}

// Return a moment from input, that is local/utc/zone equivalent to model.
function cloneWithOffset(input, model) {
    var res, diff;
    if (model._isUTC) {
        res = model.clone();
        diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
        // Use low-level api, because this fn is low-level api.
        res._d.setTime(res._d.valueOf() + diff);
        hooks.updateOffset(res, false);
        return res;
    } else {
        return createLocal(input).local();
    }
}

function getDateOffset (m) {
    // On Firefox.24 Date#getTimezoneOffset returns a floating point.
    // https://github.com/moment/moment/pull/1871
    return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
}

// HOOKS

// This function will be called whenever a moment is mutated.
// It is intended to keep the offset in sync with the timezone.
hooks.updateOffset = function () {};

// MOMENTS

// keepLocalTime = true means only change the timezone, without
// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
// +0200, so we adjust the time as needed, to be valid.
//
// Keeping the time actually adds/subtracts (one hour)
// from the actual represented time. That is why we call updateOffset
// a second time. In case it wants us to change the offset again
// _changeInProgress == true case, then we have to adjust, because
// there is no such time in the given timezone.
function getSetOffset (input, keepLocalTime, keepMinutes) {
    var offset = this._offset || 0,
        localAdjust;
    if (!this.isValid()) {
        return input != null ? this : NaN;
    }
    if (input != null) {
        if (typeof input === 'string') {
            input = offsetFromString(matchShortOffset, input);
            if (input === null) {
                return this;
            }
        } else if (Math.abs(input) < 16 && !keepMinutes) {
            input = input * 60;
        }
        if (!this._isUTC && keepLocalTime) {
            localAdjust = getDateOffset(this);
        }
        this._offset = input;
        this._isUTC = true;
        if (localAdjust != null) {
            this.add(localAdjust, 'm');
        }
        if (offset !== input) {
            if (!keepLocalTime || this._changeInProgress) {
                addSubtract(this, createDuration(input - offset, 'm'), 1, false);
            } else if (!this._changeInProgress) {
                this._changeInProgress = true;
                hooks.updateOffset(this, true);
                this._changeInProgress = null;
            }
        }
        return this;
    } else {
        return this._isUTC ? offset : getDateOffset(this);
    }
}

function getSetZone (input, keepLocalTime) {
    if (input != null) {
        if (typeof input !== 'string') {
            input = -input;
        }

        this.utcOffset(input, keepLocalTime);

        return this;
    } else {
        return -this.utcOffset();
    }
}

function setOffsetToUTC (keepLocalTime) {
    return this.utcOffset(0, keepLocalTime);
}

function setOffsetToLocal (keepLocalTime) {
    if (this._isUTC) {
        this.utcOffset(0, keepLocalTime);
        this._isUTC = false;

        if (keepLocalTime) {
            this.subtract(getDateOffset(this), 'm');
        }
    }
    return this;
}

function setOffsetToParsedOffset () {
    if (this._tzm != null) {
        this.utcOffset(this._tzm, false, true);
    } else if (typeof this._i === 'string') {
        var tZone = offsetFromString(matchOffset, this._i);
        if (tZone != null) {
            this.utcOffset(tZone);
        }
        else {
            this.utcOffset(0, true);
        }
    }
    return this;
}

function hasAlignedHourOffset (input) {
    if (!this.isValid()) {
        return false;
    }
    input = input ? createLocal(input).utcOffset() : 0;

    return (this.utcOffset() - input) % 60 === 0;
}

function isDaylightSavingTime () {
    return (
        this.utcOffset() > this.clone().month(0).utcOffset() ||
        this.utcOffset() > this.clone().month(5).utcOffset()
    );
}

function isDaylightSavingTimeShifted () {
    if (!isUndefined(this._isDSTShifted)) {
        return this._isDSTShifted;
    }

    var c = {};

    copyConfig(c, this);
    c = prepareConfig(c);

    if (c._a) {
        var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
        this._isDSTShifted = this.isValid() &&
            compareArrays(c._a, other.toArray()) > 0;
    } else {
        this._isDSTShifted = false;
    }

    return this._isDSTShifted;
}

function isLocal () {
    return this.isValid() ? !this._isUTC : false;
}

function isUtcOffset () {
    return this.isValid() ? this._isUTC : false;
}

function isUtc () {
    return this.isValid() ? this._isUTC && this._offset === 0 : false;
}

// ASP.NET json date format regex
var aspNetRegex = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

// from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
// and further modified to allow for strings containing both week and day
var isoRegex = /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;

function createDuration (input, key) {
    var duration = input,
        // matching against regexp is expensive, do it on demand
        match = null,
        sign,
        ret,
        diffRes;

    if (isDuration(input)) {
        duration = {
            ms : input._milliseconds,
            d  : input._days,
            M  : input._months
        };
    } else if (isNumber(input)) {
        duration = {};
        if (key) {
            duration[key] = input;
        } else {
            duration.milliseconds = input;
        }
    } else if (!!(match = aspNetRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : 1;
        duration = {
            y  : 0,
            d  : toInt(match[DATE])                         * sign,
            h  : toInt(match[HOUR])                         * sign,
            m  : toInt(match[MINUTE])                       * sign,
            s  : toInt(match[SECOND])                       * sign,
            ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
        };
    } else if (!!(match = isoRegex.exec(input))) {
        sign = (match[1] === '-') ? -1 : 1;
        duration = {
            y : parseIso(match[2], sign),
            M : parseIso(match[3], sign),
            w : parseIso(match[4], sign),
            d : parseIso(match[5], sign),
            h : parseIso(match[6], sign),
            m : parseIso(match[7], sign),
            s : parseIso(match[8], sign)
        };
    } else if (duration == null) {// checks for null or undefined
        duration = {};
    } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
        diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

        duration = {};
        duration.ms = diffRes.milliseconds;
        duration.M = diffRes.months;
    }

    ret = new Duration(duration);

    if (isDuration(input) && hasOwnProp(input, '_locale')) {
        ret._locale = input._locale;
    }

    return ret;
}

createDuration.fn = Duration.prototype;
createDuration.invalid = createInvalid$1;

function parseIso (inp, sign) {
    // We'd normally use ~~inp for this, but unfortunately it also
    // converts floats to ints.
    // inp may be undefined, so careful calling replace on it.
    var res = inp && parseFloat(inp.replace(',', '.'));
    // apply sign while we're at it
    return (isNaN(res) ? 0 : res) * sign;
}

function positiveMomentsDifference(base, other) {
    var res = {milliseconds: 0, months: 0};

    res.months = other.month() - base.month() +
        (other.year() - base.year()) * 12;
    if (base.clone().add(res.months, 'M').isAfter(other)) {
        --res.months;
    }

    res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

    return res;
}

function momentsDifference(base, other) {
    var res;
    if (!(base.isValid() && other.isValid())) {
        return {milliseconds: 0, months: 0};
    }

    other = cloneWithOffset(other, base);
    if (base.isBefore(other)) {
        res = positiveMomentsDifference(base, other);
    } else {
        res = positiveMomentsDifference(other, base);
        res.milliseconds = -res.milliseconds;
        res.months = -res.months;
    }

    return res;
}

// TODO: remove 'name' arg after deprecation is removed
function createAdder(direction, name) {
    return function (val, period) {
        var dur, tmp;
        //invert the arguments, but complain about it
        if (period !== null && !isNaN(+period)) {
            deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
            'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
            tmp = val; val = period; period = tmp;
        }

        val = typeof val === 'string' ? +val : val;
        dur = createDuration(val, period);
        addSubtract(this, dur, direction);
        return this;
    };
}

function addSubtract (mom, duration, isAdding, updateOffset) {
    var milliseconds = duration._milliseconds,
        days = absRound(duration._days),
        months = absRound(duration._months);

    if (!mom.isValid()) {
        // No op
        return;
    }

    updateOffset = updateOffset == null ? true : updateOffset;

    if (milliseconds) {
        mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
    }
    if (days) {
        set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
    }
    if (months) {
        setMonth(mom, get(mom, 'Month') + months * isAdding);
    }
    if (updateOffset) {
        hooks.updateOffset(mom, days || months);
    }
}

var add      = createAdder(1, 'add');
var subtract = createAdder(-1, 'subtract');

function getCalendarFormat(myMoment, now) {
    var diff = myMoment.diff(now, 'days', true);
    return diff < -6 ? 'sameElse' :
            diff < -1 ? 'lastWeek' :
            diff < 0 ? 'lastDay' :
            diff < 1 ? 'sameDay' :
            diff < 2 ? 'nextDay' :
            diff < 7 ? 'nextWeek' : 'sameElse';
}

function calendar$1 (time, formats) {
    // We want to compare the start of today, vs this.
    // Getting start-of-today depends on whether we're local/utc/offset or not.
    var now = time || createLocal(),
        sod = cloneWithOffset(now, this).startOf('day'),
        format = hooks.calendarFormat(this, sod) || 'sameElse';

    var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

    return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
}

function clone () {
    return new Moment(this);
}

function isAfter (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() > localInput.valueOf();
    } else {
        return localInput.valueOf() < this.clone().startOf(units).valueOf();
    }
}

function isBefore (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() < localInput.valueOf();
    } else {
        return this.clone().endOf(units).valueOf() < localInput.valueOf();
    }
}

function isBetween (from, to, units, inclusivity) {
    inclusivity = inclusivity || '()';
    return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) &&
        (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
}

function isSame (input, units) {
    var localInput = isMoment(input) ? input : createLocal(input),
        inputMs;
    if (!(this.isValid() && localInput.isValid())) {
        return false;
    }
    units = normalizeUnits(units || 'millisecond');
    if (units === 'millisecond') {
        return this.valueOf() === localInput.valueOf();
    } else {
        inputMs = localInput.valueOf();
        return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
    }
}

function isSameOrAfter (input, units) {
    return this.isSame(input, units) || this.isAfter(input,units);
}

function isSameOrBefore (input, units) {
    return this.isSame(input, units) || this.isBefore(input,units);
}

function diff (input, units, asFloat) {
    var that,
        zoneDelta,
        delta, output;

    if (!this.isValid()) {
        return NaN;
    }

    that = cloneWithOffset(input, this);

    if (!that.isValid()) {
        return NaN;
    }

    zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

    units = normalizeUnits(units);

    if (units === 'year' || units === 'month' || units === 'quarter') {
        output = monthDiff(this, that);
        if (units === 'quarter') {
            output = output / 3;
        } else if (units === 'year') {
            output = output / 12;
        }
    } else {
        delta = this - that;
        output = units === 'second' ? delta / 1e3 : // 1000
            units === 'minute' ? delta / 6e4 : // 1000 * 60
            units === 'hour' ? delta / 36e5 : // 1000 * 60 * 60
            units === 'day' ? (delta - zoneDelta) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
            units === 'week' ? (delta - zoneDelta) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
            delta;
    }
    return asFloat ? output : absFloor(output);
}

function monthDiff (a, b) {
    // difference in months
    var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
        // b is in (anchor - 1 month, anchor + 1 month)
        anchor = a.clone().add(wholeMonthDiff, 'months'),
        anchor2, adjust;

    if (b - anchor < 0) {
        anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor - anchor2);
    } else {
        anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
        // linear across the month
        adjust = (b - anchor) / (anchor2 - anchor);
    }

    //check for negative zero, return zero if negative zero
    return -(wholeMonthDiff + adjust) || 0;
}

hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

function toString () {
    return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
}

function toISOString() {
    if (!this.isValid()) {
        return null;
    }
    var m = this.clone().utc();
    if (m.year() < 0 || m.year() > 9999) {
        return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    }
    if (isFunction(Date.prototype.toISOString)) {
        // native implementation is ~50x faster, use it when we can
        return this.toDate().toISOString();
    }
    return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
}

/**
 * Return a human readable representation of a moment that can
 * also be evaluated to get a new moment which is the same
 *
 * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
 */
function inspect () {
    if (!this.isValid()) {
        return 'moment.invalid(/* ' + this._i + ' */)';
    }
    var func = 'moment';
    var zone = '';
    if (!this.isLocal()) {
        func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
        zone = 'Z';
    }
    var prefix = '[' + func + '("]';
    var year = (0 <= this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
    var datetime = '-MM-DD[T]HH:mm:ss.SSS';
    var suffix = zone + '[")]';

    return this.format(prefix + year + datetime + suffix);
}

function format (inputString) {
    if (!inputString) {
        inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
    }
    var output = formatMoment(this, inputString);
    return this.localeData().postformat(output);
}

function from (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function fromNow (withoutSuffix) {
    return this.from(createLocal(), withoutSuffix);
}

function to (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

function toNow (withoutSuffix) {
    return this.to(createLocal(), withoutSuffix);
}

// If passed a locale key, it will set the locale for this
// instance.  Otherwise, it will return the locale configuration
// variables for this instance.
function locale (key) {
    var newLocaleData;

    if (key === undefined) {
        return this._locale._abbr;
    } else {
        newLocaleData = getLocale(key);
        if (newLocaleData != null) {
            this._locale = newLocaleData;
        }
        return this;
    }
}

var lang = deprecate(
    'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
    function (key) {
        if (key === undefined) {
            return this.localeData();
        } else {
            return this.locale(key);
        }
    }
);

function localeData () {
    return this._locale;
}

function startOf (units) {
    units = normalizeUnits(units);
    // the following switch intentionally omits break keywords
    // to utilize falling through the cases.
    switch (units) {
        case 'year':
            this.month(0);
            /* falls through */
        case 'quarter':
        case 'month':
            this.date(1);
            /* falls through */
        case 'week':
        case 'isoWeek':
        case 'day':
        case 'date':
            this.hours(0);
            /* falls through */
        case 'hour':
            this.minutes(0);
            /* falls through */
        case 'minute':
            this.seconds(0);
            /* falls through */
        case 'second':
            this.milliseconds(0);
    }

    // weeks are a special case
    if (units === 'week') {
        this.weekday(0);
    }
    if (units === 'isoWeek') {
        this.isoWeekday(1);
    }

    // quarters are also special
    if (units === 'quarter') {
        this.month(Math.floor(this.month() / 3) * 3);
    }

    return this;
}

function endOf (units) {
    units = normalizeUnits(units);
    if (units === undefined || units === 'millisecond') {
        return this;
    }

    // 'date' is an alias for 'day', so it should be considered as such.
    if (units === 'date') {
        units = 'day';
    }

    return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
}

function valueOf () {
    return this._d.valueOf() - ((this._offset || 0) * 60000);
}

function unix () {
    return Math.floor(this.valueOf() / 1000);
}

function toDate () {
    return new Date(this.valueOf());
}

function toArray () {
    var m = this;
    return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
}

function toObject () {
    var m = this;
    return {
        years: m.year(),
        months: m.month(),
        date: m.date(),
        hours: m.hours(),
        minutes: m.minutes(),
        seconds: m.seconds(),
        milliseconds: m.milliseconds()
    };
}

function toJSON () {
    // new Date(NaN).toJSON() === null
    return this.isValid() ? this.toISOString() : null;
}

function isValid$2 () {
    return isValid(this);
}

function parsingFlags () {
    return extend({}, getParsingFlags(this));
}

function invalidAt () {
    return getParsingFlags(this).overflow;
}

function creationData() {
    return {
        input: this._i,
        format: this._f,
        locale: this._locale,
        isUTC: this._isUTC,
        strict: this._strict
    };
}

// FORMATTING

addFormatToken(0, ['gg', 2], 0, function () {
    return this.weekYear() % 100;
});

addFormatToken(0, ['GG', 2], 0, function () {
    return this.isoWeekYear() % 100;
});

function addWeekYearFormatToken (token, getter) {
    addFormatToken(0, [token, token.length], 0, getter);
}

addWeekYearFormatToken('gggg',     'weekYear');
addWeekYearFormatToken('ggggg',    'weekYear');
addWeekYearFormatToken('GGGG',  'isoWeekYear');
addWeekYearFormatToken('GGGGG', 'isoWeekYear');

// ALIASES

addUnitAlias('weekYear', 'gg');
addUnitAlias('isoWeekYear', 'GG');

// PRIORITY

addUnitPriority('weekYear', 1);
addUnitPriority('isoWeekYear', 1);


// PARSING

addRegexToken('G',      matchSigned);
addRegexToken('g',      matchSigned);
addRegexToken('GG',     match1to2, match2);
addRegexToken('gg',     match1to2, match2);
addRegexToken('GGGG',   match1to4, match4);
addRegexToken('gggg',   match1to4, match4);
addRegexToken('GGGGG',  match1to6, match6);
addRegexToken('ggggg',  match1to6, match6);

addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
    week[token.substr(0, 2)] = toInt(input);
});

addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
    week[token] = hooks.parseTwoDigitYear(input);
});

// MOMENTS

function getSetWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input,
            this.week(),
            this.weekday(),
            this.localeData()._week.dow,
            this.localeData()._week.doy);
}

function getSetISOWeekYear (input) {
    return getSetWeekYearHelper.call(this,
            input, this.isoWeek(), this.isoWeekday(), 1, 4);
}

function getISOWeeksInYear () {
    return weeksInYear(this.year(), 1, 4);
}

function getWeeksInYear () {
    var weekInfo = this.localeData()._week;
    return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
}

function getSetWeekYearHelper(input, week, weekday, dow, doy) {
    var weeksTarget;
    if (input == null) {
        return weekOfYear(this, dow, doy).year;
    } else {
        weeksTarget = weeksInYear(input, dow, doy);
        if (week > weeksTarget) {
            week = weeksTarget;
        }
        return setWeekAll.call(this, input, week, weekday, dow, doy);
    }
}

function setWeekAll(weekYear, week, weekday, dow, doy) {
    var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
        date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

    this.year(date.getUTCFullYear());
    this.month(date.getUTCMonth());
    this.date(date.getUTCDate());
    return this;
}

// FORMATTING

addFormatToken('Q', 0, 'Qo', 'quarter');

// ALIASES

addUnitAlias('quarter', 'Q');

// PRIORITY

addUnitPriority('quarter', 7);

// PARSING

addRegexToken('Q', match1);
addParseToken('Q', function (input, array) {
    array[MONTH] = (toInt(input) - 1) * 3;
});

// MOMENTS

function getSetQuarter (input) {
    return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
}

// FORMATTING

addFormatToken('D', ['DD', 2], 'Do', 'date');

// ALIASES

addUnitAlias('date', 'D');

// PRIOROITY
addUnitPriority('date', 9);

// PARSING

addRegexToken('D',  match1to2);
addRegexToken('DD', match1to2, match2);
addRegexToken('Do', function (isStrict, locale) {
    // TODO: Remove "ordinalParse" fallback in next major release.
    return isStrict ?
      (locale._dayOfMonthOrdinalParse || locale._ordinalParse) :
      locale._dayOfMonthOrdinalParseLenient;
});

addParseToken(['D', 'DD'], DATE);
addParseToken('Do', function (input, array) {
    array[DATE] = toInt(input.match(match1to2)[0], 10);
});

// MOMENTS

var getSetDayOfMonth = makeGetSet('Date', true);

// FORMATTING

addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

// ALIASES

addUnitAlias('dayOfYear', 'DDD');

// PRIORITY
addUnitPriority('dayOfYear', 4);

// PARSING

addRegexToken('DDD',  match1to3);
addRegexToken('DDDD', match3);
addParseToken(['DDD', 'DDDD'], function (input, array, config) {
    config._dayOfYear = toInt(input);
});

// HELPERS

// MOMENTS

function getSetDayOfYear (input) {
    var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
    return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
}

// FORMATTING

addFormatToken('m', ['mm', 2], 0, 'minute');

// ALIASES

addUnitAlias('minute', 'm');

// PRIORITY

addUnitPriority('minute', 14);

// PARSING

addRegexToken('m',  match1to2);
addRegexToken('mm', match1to2, match2);
addParseToken(['m', 'mm'], MINUTE);

// MOMENTS

var getSetMinute = makeGetSet('Minutes', false);

// FORMATTING

addFormatToken('s', ['ss', 2], 0, 'second');

// ALIASES

addUnitAlias('second', 's');

// PRIORITY

addUnitPriority('second', 15);

// PARSING

addRegexToken('s',  match1to2);
addRegexToken('ss', match1to2, match2);
addParseToken(['s', 'ss'], SECOND);

// MOMENTS

var getSetSecond = makeGetSet('Seconds', false);

// FORMATTING

addFormatToken('S', 0, 0, function () {
    return ~~(this.millisecond() / 100);
});

addFormatToken(0, ['SS', 2], 0, function () {
    return ~~(this.millisecond() / 10);
});

addFormatToken(0, ['SSS', 3], 0, 'millisecond');
addFormatToken(0, ['SSSS', 4], 0, function () {
    return this.millisecond() * 10;
});
addFormatToken(0, ['SSSSS', 5], 0, function () {
    return this.millisecond() * 100;
});
addFormatToken(0, ['SSSSSS', 6], 0, function () {
    return this.millisecond() * 1000;
});
addFormatToken(0, ['SSSSSSS', 7], 0, function () {
    return this.millisecond() * 10000;
});
addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
    return this.millisecond() * 100000;
});
addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
    return this.millisecond() * 1000000;
});


// ALIASES

addUnitAlias('millisecond', 'ms');

// PRIORITY

addUnitPriority('millisecond', 16);

// PARSING

addRegexToken('S',    match1to3, match1);
addRegexToken('SS',   match1to3, match2);
addRegexToken('SSS',  match1to3, match3);

var token;
for (token = 'SSSS'; token.length <= 9; token += 'S') {
    addRegexToken(token, matchUnsigned);
}

function parseMs(input, array) {
    array[MILLISECOND] = toInt(('0.' + input) * 1000);
}

for (token = 'S'; token.length <= 9; token += 'S') {
    addParseToken(token, parseMs);
}
// MOMENTS

var getSetMillisecond = makeGetSet('Milliseconds', false);

// FORMATTING

addFormatToken('z',  0, 0, 'zoneAbbr');
addFormatToken('zz', 0, 0, 'zoneName');

// MOMENTS

function getZoneAbbr () {
    return this._isUTC ? 'UTC' : '';
}

function getZoneName () {
    return this._isUTC ? 'Coordinated Universal Time' : '';
}

var proto = Moment.prototype;

proto.add               = add;
proto.calendar          = calendar$1;
proto.clone             = clone;
proto.diff              = diff;
proto.endOf             = endOf;
proto.format            = format;
proto.from              = from;
proto.fromNow           = fromNow;
proto.to                = to;
proto.toNow             = toNow;
proto.get               = stringGet;
proto.invalidAt         = invalidAt;
proto.isAfter           = isAfter;
proto.isBefore          = isBefore;
proto.isBetween         = isBetween;
proto.isSame            = isSame;
proto.isSameOrAfter     = isSameOrAfter;
proto.isSameOrBefore    = isSameOrBefore;
proto.isValid           = isValid$2;
proto.lang              = lang;
proto.locale            = locale;
proto.localeData        = localeData;
proto.max               = prototypeMax;
proto.min               = prototypeMin;
proto.parsingFlags      = parsingFlags;
proto.set               = stringSet;
proto.startOf           = startOf;
proto.subtract          = subtract;
proto.toArray           = toArray;
proto.toObject          = toObject;
proto.toDate            = toDate;
proto.toISOString       = toISOString;
proto.inspect           = inspect;
proto.toJSON            = toJSON;
proto.toString          = toString;
proto.unix              = unix;
proto.valueOf           = valueOf;
proto.creationData      = creationData;

// Year
proto.year       = getSetYear;
proto.isLeapYear = getIsLeapYear;

// Week Year
proto.weekYear    = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;

// Quarter
proto.quarter = proto.quarters = getSetQuarter;

// Month
proto.month       = getSetMonth;
proto.daysInMonth = getDaysInMonth;

// Week
proto.week           = proto.weeks        = getSetWeek;
proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
proto.weeksInYear    = getWeeksInYear;
proto.isoWeeksInYear = getISOWeeksInYear;

// Day
proto.date       = getSetDayOfMonth;
proto.day        = proto.days             = getSetDayOfWeek;
proto.weekday    = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear  = getSetDayOfYear;

// Hour
proto.hour = proto.hours = getSetHour;

// Minute
proto.minute = proto.minutes = getSetMinute;

// Second
proto.second = proto.seconds = getSetSecond;

// Millisecond
proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
proto.utcOffset            = getSetOffset;
proto.utc                  = setOffsetToUTC;
proto.local                = setOffsetToLocal;
proto.parseZone            = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST                = isDaylightSavingTime;
proto.isLocal              = isLocal;
proto.isUtcOffset          = isUtcOffset;
proto.isUtc                = isUtc;
proto.isUTC                = isUtc;

// Timezone
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;

// Deprecations
proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

function createUnix (input) {
    return createLocal(input * 1000);
}

function createInZone () {
    return createLocal.apply(null, arguments).parseZone();
}

function preParsePostFormat (string) {
    return string;
}

var proto$1 = Locale.prototype;

proto$1.calendar        = calendar;
proto$1.longDateFormat  = longDateFormat;
proto$1.invalidDate     = invalidDate;
proto$1.ordinal         = ordinal;
proto$1.preparse        = preParsePostFormat;
proto$1.postformat      = preParsePostFormat;
proto$1.relativeTime    = relativeTime;
proto$1.pastFuture      = pastFuture;
proto$1.set             = set;

// Month
proto$1.months            =        localeMonths;
proto$1.monthsShort       =        localeMonthsShort;
proto$1.monthsParse       =        localeMonthsParse;
proto$1.monthsRegex       = monthsRegex;
proto$1.monthsShortRegex  = monthsShortRegex;

// Week
proto$1.week = localeWeek;
proto$1.firstDayOfYear = localeFirstDayOfYear;
proto$1.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
proto$1.weekdays       =        localeWeekdays;
proto$1.weekdaysMin    =        localeWeekdaysMin;
proto$1.weekdaysShort  =        localeWeekdaysShort;
proto$1.weekdaysParse  =        localeWeekdaysParse;

proto$1.weekdaysRegex       =        weekdaysRegex;
proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

// Hours
proto$1.isPM = localeIsPM;
proto$1.meridiem = localeMeridiem;

function get$1 (format, index, field, setter) {
    var locale = getLocale();
    var utc = createUTC().set(setter, index);
    return locale[field](utc, format);
}

function listMonthsImpl (format, index, field) {
    if (isNumber(format)) {
        index = format;
        format = undefined;
    }

    format = format || '';

    if (index != null) {
        return get$1(format, index, field, 'month');
    }

    var i;
    var out = [];
    for (i = 0; i < 12; i++) {
        out[i] = get$1(format, i, field, 'month');
    }
    return out;
}

// ()
// (5)
// (fmt, 5)
// (fmt)
// (true)
// (true, 5)
// (true, fmt, 5)
// (true, fmt)
function listWeekdaysImpl (localeSorted, format, index, field) {
    if (typeof localeSorted === 'boolean') {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    } else {
        format = localeSorted;
        index = format;
        localeSorted = false;

        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';
    }

    var locale = getLocale(),
        shift = localeSorted ? locale._week.dow : 0;

    if (index != null) {
        return get$1(format, (index + shift) % 7, field, 'day');
    }

    var i;
    var out = [];
    for (i = 0; i < 7; i++) {
        out[i] = get$1(format, (i + shift) % 7, field, 'day');
    }
    return out;
}

function listMonths (format, index) {
    return listMonthsImpl(format, index, 'months');
}

function listMonthsShort (format, index) {
    return listMonthsImpl(format, index, 'monthsShort');
}

function listWeekdays (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
}

function listWeekdaysShort (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
}

function listWeekdaysMin (localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
}

getSetGlobalLocale('en', {
    dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
    ordinal : function (number) {
        var b = number % 10,
            output = (toInt(number % 100 / 10) === 1) ? 'th' :
            (b === 1) ? 'st' :
            (b === 2) ? 'nd' :
            (b === 3) ? 'rd' : 'th';
        return number + output;
    }
});

// Side effect imports
hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

var mathAbs = Math.abs;

function abs () {
    var data           = this._data;

    this._milliseconds = mathAbs(this._milliseconds);
    this._days         = mathAbs(this._days);
    this._months       = mathAbs(this._months);

    data.milliseconds  = mathAbs(data.milliseconds);
    data.seconds       = mathAbs(data.seconds);
    data.minutes       = mathAbs(data.minutes);
    data.hours         = mathAbs(data.hours);
    data.months        = mathAbs(data.months);
    data.years         = mathAbs(data.years);

    return this;
}

function addSubtract$1 (duration, input, value, direction) {
    var other = createDuration(input, value);

    duration._milliseconds += direction * other._milliseconds;
    duration._days         += direction * other._days;
    duration._months       += direction * other._months;

    return duration._bubble();
}

// supports only 2.0-style add(1, 's') or add(duration)
function add$1 (input, value) {
    return addSubtract$1(this, input, value, 1);
}

// supports only 2.0-style subtract(1, 's') or subtract(duration)
function subtract$1 (input, value) {
    return addSubtract$1(this, input, value, -1);
}

function absCeil (number) {
    if (number < 0) {
        return Math.floor(number);
    } else {
        return Math.ceil(number);
    }
}

function bubble () {
    var milliseconds = this._milliseconds;
    var days         = this._days;
    var months       = this._months;
    var data         = this._data;
    var seconds, minutes, hours, years, monthsFromDays;

    // if we have a mix of positive and negative values, bubble down first
    // check: https://github.com/moment/moment/issues/2166
    if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
            (milliseconds <= 0 && days <= 0 && months <= 0))) {
        milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
        days = 0;
        months = 0;
    }

    // The following code bubbles up values, see the tests for
    // examples of what that means.
    data.milliseconds = milliseconds % 1000;

    seconds           = absFloor(milliseconds / 1000);
    data.seconds      = seconds % 60;

    minutes           = absFloor(seconds / 60);
    data.minutes      = minutes % 60;

    hours             = absFloor(minutes / 60);
    data.hours        = hours % 24;

    days += absFloor(hours / 24);

    // convert days to months
    monthsFromDays = absFloor(daysToMonths(days));
    months += monthsFromDays;
    days -= absCeil(monthsToDays(monthsFromDays));

    // 12 months -> 1 year
    years = absFloor(months / 12);
    months %= 12;

    data.days   = days;
    data.months = months;
    data.years  = years;

    return this;
}

function daysToMonths (days) {
    // 400 years have 146097 days (taking into account leap year rules)
    // 400 years have 12 months === 4800
    return days * 4800 / 146097;
}

function monthsToDays (months) {
    // the reverse of daysToMonths
    return months * 146097 / 4800;
}

function as (units) {
    if (!this.isValid()) {
        return NaN;
    }
    var days;
    var months;
    var milliseconds = this._milliseconds;

    units = normalizeUnits(units);

    if (units === 'month' || units === 'year') {
        days   = this._days   + milliseconds / 864e5;
        months = this._months + daysToMonths(days);
        return units === 'month' ? months : months / 12;
    } else {
        // handle milliseconds separately because of floating point math errors (issue #1867)
        days = this._days + Math.round(monthsToDays(this._months));
        switch (units) {
            case 'week'   : return days / 7     + milliseconds / 6048e5;
            case 'day'    : return days         + milliseconds / 864e5;
            case 'hour'   : return days * 24    + milliseconds / 36e5;
            case 'minute' : return days * 1440  + milliseconds / 6e4;
            case 'second' : return days * 86400 + milliseconds / 1000;
            // Math.floor prevents floating point math errors here
            case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
            default: throw new Error('Unknown unit ' + units);
        }
    }
}

// TODO: Use this.as('ms')?
function valueOf$1 () {
    if (!this.isValid()) {
        return NaN;
    }
    return (
        this._milliseconds +
        this._days * 864e5 +
        (this._months % 12) * 2592e6 +
        toInt(this._months / 12) * 31536e6
    );
}

function makeAs (alias) {
    return function () {
        return this.as(alias);
    };
}

var asMilliseconds = makeAs('ms');
var asSeconds      = makeAs('s');
var asMinutes      = makeAs('m');
var asHours        = makeAs('h');
var asDays         = makeAs('d');
var asWeeks        = makeAs('w');
var asMonths       = makeAs('M');
var asYears        = makeAs('y');

function get$2 (units) {
    units = normalizeUnits(units);
    return this.isValid() ? this[units + 's']() : NaN;
}

function makeGetter(name) {
    return function () {
        return this.isValid() ? this._data[name] : NaN;
    };
}

var milliseconds = makeGetter('milliseconds');
var seconds      = makeGetter('seconds');
var minutes      = makeGetter('minutes');
var hours        = makeGetter('hours');
var days         = makeGetter('days');
var months       = makeGetter('months');
var years        = makeGetter('years');

function weeks () {
    return absFloor(this.days() / 7);
}

var round = Math.round;
var thresholds = {
    ss: 44,         // a few seconds to seconds
    s : 45,         // seconds to minute
    m : 45,         // minutes to hour
    h : 22,         // hours to day
    d : 26,         // days to month
    M : 11          // months to year
};

// helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
    return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
}

function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
    var duration = createDuration(posNegDuration).abs();
    var seconds  = round(duration.as('s'));
    var minutes  = round(duration.as('m'));
    var hours    = round(duration.as('h'));
    var days     = round(duration.as('d'));
    var months   = round(duration.as('M'));
    var years    = round(duration.as('y'));

    var a = seconds <= thresholds.ss && ['s', seconds]  ||
            seconds < thresholds.s   && ['ss', seconds] ||
            minutes <= 1             && ['m']           ||
            minutes < thresholds.m   && ['mm', minutes] ||
            hours   <= 1             && ['h']           ||
            hours   < thresholds.h   && ['hh', hours]   ||
            days    <= 1             && ['d']           ||
            days    < thresholds.d   && ['dd', days]    ||
            months  <= 1             && ['M']           ||
            months  < thresholds.M   && ['MM', months]  ||
            years   <= 1             && ['y']           || ['yy', years];

    a[2] = withoutSuffix;
    a[3] = +posNegDuration > 0;
    a[4] = locale;
    return substituteTimeAgo.apply(null, a);
}

// This function allows you to set the rounding function for relative time strings
function getSetRelativeTimeRounding (roundingFunction) {
    if (roundingFunction === undefined) {
        return round;
    }
    if (typeof(roundingFunction) === 'function') {
        round = roundingFunction;
        return true;
    }
    return false;
}

// This function allows you to set a threshold for relative time strings
function getSetRelativeTimeThreshold (threshold, limit) {
    if (thresholds[threshold] === undefined) {
        return false;
    }
    if (limit === undefined) {
        return thresholds[threshold];
    }
    thresholds[threshold] = limit;
    if (threshold === 's') {
        thresholds.ss = limit - 1;
    }
    return true;
}

function humanize (withSuffix) {
    if (!this.isValid()) {
        return this.localeData().invalidDate();
    }

    var locale = this.localeData();
    var output = relativeTime$1(this, !withSuffix, locale);

    if (withSuffix) {
        output = locale.pastFuture(+this, output);
    }

    return locale.postformat(output);
}

var abs$1 = Math.abs;

function toISOString$1() {
    // for ISO strings we do not use the normal bubbling rules:
    //  * milliseconds bubble up until they become hours
    //  * days do not bubble at all
    //  * months bubble up until they become years
    // This is because there is no context-free conversion between hours and days
    // (think of clock changes)
    // and also not between days and months (28-31 days per month)
    if (!this.isValid()) {
        return this.localeData().invalidDate();
    }

    var seconds = abs$1(this._milliseconds) / 1000;
    var days         = abs$1(this._days);
    var months       = abs$1(this._months);
    var minutes, hours, years;

    // 3600 seconds -> 60 minutes -> 1 hour
    minutes           = absFloor(seconds / 60);
    hours             = absFloor(minutes / 60);
    seconds %= 60;
    minutes %= 60;

    // 12 months -> 1 year
    years  = absFloor(months / 12);
    months %= 12;


    // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
    var Y = years;
    var M = months;
    var D = days;
    var h = hours;
    var m = minutes;
    var s = seconds;
    var total = this.asSeconds();

    if (!total) {
        // this is the same as C#'s (Noda) and python (isodate)...
        // but not other JS (goog.date)
        return 'P0D';
    }

    return (total < 0 ? '-' : '') +
        'P' +
        (Y ? Y + 'Y' : '') +
        (M ? M + 'M' : '') +
        (D ? D + 'D' : '') +
        ((h || m || s) ? 'T' : '') +
        (h ? h + 'H' : '') +
        (m ? m + 'M' : '') +
        (s ? s + 'S' : '');
}

var proto$2 = Duration.prototype;

proto$2.isValid        = isValid$1;
proto$2.abs            = abs;
proto$2.add            = add$1;
proto$2.subtract       = subtract$1;
proto$2.as             = as;
proto$2.asMilliseconds = asMilliseconds;
proto$2.asSeconds      = asSeconds;
proto$2.asMinutes      = asMinutes;
proto$2.asHours        = asHours;
proto$2.asDays         = asDays;
proto$2.asWeeks        = asWeeks;
proto$2.asMonths       = asMonths;
proto$2.asYears        = asYears;
proto$2.valueOf        = valueOf$1;
proto$2._bubble        = bubble;
proto$2.get            = get$2;
proto$2.milliseconds   = milliseconds;
proto$2.seconds        = seconds;
proto$2.minutes        = minutes;
proto$2.hours          = hours;
proto$2.days           = days;
proto$2.weeks          = weeks;
proto$2.months         = months;
proto$2.years          = years;
proto$2.humanize       = humanize;
proto$2.toISOString    = toISOString$1;
proto$2.toString       = toISOString$1;
proto$2.toJSON         = toISOString$1;
proto$2.locale         = locale;
proto$2.localeData     = localeData;

// Deprecations
proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
proto$2.lang = lang;

// Side effect imports

// FORMATTING

addFormatToken('X', 0, 0, 'unix');
addFormatToken('x', 0, 0, 'valueOf');

// PARSING

addRegexToken('x', matchSigned);
addRegexToken('X', matchTimestamp);
addParseToken('X', function (input, array, config) {
    config._d = new Date(parseFloat(input, 10) * 1000);
});
addParseToken('x', function (input, array, config) {
    config._d = new Date(toInt(input));
});

// Side effect imports


hooks.version = '2.18.1';

setHookCallback(createLocal);

hooks.fn                    = proto;
hooks.min                   = min;
hooks.max                   = max;
hooks.now                   = now;
hooks.utc                   = createUTC;
hooks.unix                  = createUnix;
hooks.months                = listMonths;
hooks.isDate                = isDate;
hooks.locale                = getSetGlobalLocale;
hooks.invalid               = createInvalid;
hooks.duration              = createDuration;
hooks.isMoment              = isMoment;
hooks.weekdays              = listWeekdays;
hooks.parseZone             = createInZone;
hooks.localeData            = getLocale;
hooks.isDuration            = isDuration;
hooks.monthsShort           = listMonthsShort;
hooks.weekdaysMin           = listWeekdaysMin;
hooks.defineLocale          = defineLocale;
hooks.updateLocale          = updateLocale;
hooks.locales               = listLocales;
hooks.weekdaysShort         = listWeekdaysShort;
hooks.normalizeUnits        = normalizeUnits;
hooks.relativeTimeRounding = getSetRelativeTimeRounding;
hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
hooks.calendarFormat        = getCalendarFormat;
hooks.prototype             = proto;

return hooks;

})));

},{}],87:[function(require,module,exports){
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/

'use strict';
/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};

},{}]},{},[1]);
