# 5.0.1

- FEATURE: Text fields in forms can now have barcode scanning enabled for mobile application
- FEATURE: User assignments now carry a flag denoting whether the location is missing GPS coordinates in order to trigger the users ability to enter them from the mobile app
- BUGFIX: Fixes an issue with mobile app open alert count metric
- BUGFIX: Fixes an issue with alert retrieval for reporting users from mobile devices when they have an active connection



