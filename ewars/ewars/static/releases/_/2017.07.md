# v2017.07

## v2017.07.2

- Fixes an issue with sorting of columns in reporting and submissions components with ordering on non-numeric columns
- Changes to settings table
- Added new `conf` table for account specific settings management
- Added new `distributions` table in prep for new distribution list settings
- More prep for new settings component

## v2017.07.1

- Adds ability to order items in logic editor arbitrarily
- Fixes an issue where search was not working in Indicator Manager
- Cleans up some issues in inbox component and adds pagination
- Adds back in activity feed item for report submissions
- New logic items in logic editor are now added to the top of list instead of bottom for easier editing
- Template PDFs no longer have underscores in file name
- Added description field to columns displayed in preset editor
- Fixes an issue where future dates were showing in documents browser
- Adds a {today} field to templates
- Moved submitted_date column in reporting
- New beta report manager in admin for easier managing of submitted forms
- Fixes an issue with deletion of messages in inbox
