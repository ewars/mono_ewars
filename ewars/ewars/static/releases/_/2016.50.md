# v2016.51

## v2016.51.3

- Fixed an issue with map display in documents

## v2016.51.2

- Fixed an issue where verification outcome was not editable once monitor was selected
- Refactored System -> Alerts indicator settings/Specification
- Fixed inconsistencies in flow of alerts
- Fixed date formatting in DataTable for alerts component

## v2016.51.1

- #239 Fixes an issue where Geographic admins could not open alerts
- #238 Fixes variable label text colouring in admin for complex data source definitions
- Fixes an issue in developer tools with LESS compilation, LESS compilation is now integral part of build system
