# 5.0.3

## General Updates

- FEATURE: Assignments can now be assigned based on a group or locations within a parent
- CHANGE: Assignment editor screen revised to make editing multiple assignments easier as well as remove confusion around saves
- CHANGE: Update to alert tabs/screens layout to make it more intuitive
- FEATURE: Forms now support conditional settings for matrix, row and row cell fields (mobile support coming)
- BUGFIX: Fixed an issue with long lists of assignments not being scrollable in assignment editor
- FEATURE: New location GPS coord collection field type
- FEATURE: New Mapping widget for templates
- BUGFIX: Fixes an issue in Admin -> Submissions with editing select fields
- BUGFIX: Fixes an issue with non-numeric fields in matrix rows not propagating their change events properly

## Technical Changes

- Updated overdue record API to handle new assignment types
- Updated upcoming records API to handle new assignment types
- Updated mobile assignments sync api to translate new assignment format into format supported on mobile app
- Added new `ewars.error(<message>)` util to show red error growl-like notification when errors occurA
- Added `definition` and `type` columns to assignments table
- Added `mapping` table in preparation for new mapping component
- Refactored portions of User management to ES6 classes from React.createClass style syntax

## Coming Soon

- FEATURE: New pdf generation engine supporting ES6 language-level syntaxes
