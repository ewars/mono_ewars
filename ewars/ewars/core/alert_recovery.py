import datetime

# from .users import get_notifiable_users
from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.core import notifications, user_lookup

from psycopg2.extensions import AsIs

from ewars.utils import date_utils
from ewars.models import Alert
# from .activity import add_alert_event
# from .emails import send_alert_email

import tornado
import tornado.ioloop


async def evaluate_recovery():
    """ Iterate throught the alarms in the systems
    and check them for recoverable alerts

    Returns:

    """

    alarms = None
    accounts = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, tki FROM _iw.accounts
            WHERE status = 'ACTIVE';
        """)
        accounts = await cur.fetchall()

    for account in accounts:
        alarms = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, ad_interval, ad_period
                FROM %s.alarms_V2
                WHERE status = 'ACTIVE' 
                AND ad_enabled IS TRUE;
            """, (
                AsIs(account.get("tki")),
            ))
            alarms = await cur.fetchall()

        for alarm in alarms:
            if alarm.get("ad_period", None) is not None \
                    and alarm.get("ad_interval", None) is not None:
                await recover_alerts(alarm.get("uuid"),
                                     alarm.get("ad_period"),
                                     alarm.get("ad_interval"),
                                     user=dict(
                                         aid=account.get("id"),
                                         tki=account.get("tki")
                                     ))

    return True


# TODO: Rework for new system
async def recover_alerts(alarm_uuid, recovery_interval, recovery_period, user=None):
    alarm = None

    # TODO: Needs to be reworked to use new user/tki system
    alerts = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*,
                al.name AS alarm_name,
                l.name AS location_name
            FROM %s.alerts AS a
                LEFT JOIN %s.alarms_V2 AS al ON al.uuid = a.alarm_id
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.state = 'OPEN'
                AND a.stage = 'VERIFICATION'
                AND a.stage_state = 'PENDING'
                AND a.alarm_id = %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_uuid,
        ))
        alerts = await cur.fetchall()

    # What date before today should the alert be auto-recovered
    threshold_delta = date_utils.get_delta(recovery_interval, recovery_period)
    today = datetime.date.today()

    for alert in alerts:
        past_due = alert.get('created') + threshold_delta
        past_due = past_due.date()
        if today > past_due:
            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.alerts
                    SET state = 'AUTODISCARDED',
                        stage = 'VERIFICATION',
                        stage_state = 'COMPLETED',
                        outcome = 'DISCARDED'
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    alert.get("uuid"),
                ))

            await Alert.add_event(alert.get("uuid"), 'AUTOCLOSED', 'fa-trash', user=user)

            alarm_name = alert.get("alarm_name")
            if isinstance(alarm_name, (dict,)):
                alarm_name = alarm_name.get("en", None)

            notification_data = dict(
                alarm_name=alarm_name,
                location_name=alert['location_name'].get("en", None),
                alert_date=date_utils.display(alert.get("trigger_end"), alert.get("interval"))
            )

            notifiers = await Alert.get_notifiable_users(alert.get('uuid'), user=user)

            loop = tornado.ioloop.IOLoop.current()
            loop.spawn_callback(
                notifications.send_bulk,
                'ALERT_AUTOCLOSED',
                notifiers,
                notification_data,
                user=user

            )
