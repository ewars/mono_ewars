import datetime
import json
import uuid

# from ewars.core.registry import registry

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


async def get_account_by_domain(host):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, default_language, status, domain, location_id, iso2, description, account_logo, account_flag, tki FROM
            _iw.accounts
            WHERE domain = %s
            AND status = 'ACTIVE'
        """, (
            host,
        ))
        result = await cur.fetchone()

    if result is None:
        return dict(
            name="Global"
        )
    else:
        return result

async def get_account_by_host(host):
    """ Retrieve an account based on a host name

    Args:
        host:

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, default_language, status, domain, location_id, iso2, description, account_logo, account_flag, tki FROM
            _iw.accounts
            WHERE domain = %s
            AND status = 'ACTIVE'
        """, (
            host,
        ))
        result = await cur.fetchone()

    if result is None:
        return dict(
            name="Global"
        )
    else:
        return result


async def account_action(action, account_id, data, user=None):
    result = None

    if action == "CREATE":
        result = await create_account(data, user=user)
    elif action == "AS_OPTIONS":
        result = await get_accounts_as_options(data, user=user)
    elif action == "STATS":
        result = await get_account_stats(user=user)
    elif action == "UNASSIGNED":
        result = await get_unassigned_locations(data.get("filter"), user=None)
    elif action == "UPDATE":
        result = await update_account(account_id, data, user=None)

    return result


async def update_dashboards(data, user=None):
    """ Update the dashboards configured for an account

    Args:
        acc_id: The account to update
        data: The data to update with
        user: The calling user

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.accounts
            SET dashboards = %s
            WHERE id = %s
        """, (
            json.dumps(data),
            user.get("aid"),
        ))


async def create_account(data, user=None):
    """com.ewars.account.create

    Create a new account within the instance

    Args:
        data: The account data to create
        user: The calling user

    Returns:
        The newly created account

    """
    result = None
    validated = True
    errors = dict()

    new_uuid = str(uuid.uuid4())
    new_uuid = new_uuid.split("-")
    new_uuid = "_%s" % new_uuid[:-1]

    if validated:
        async with get_db_cur() as cur:
            await cur.execute("""
                    INSERT INTO _iw.accounts
                        (name, location_id, logo, icon_flag, styles, default_language, status, domain, is_root, created, created_by, last_modified, flag_code, instance_id, description, account_logo, account_flag, tki)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
               """, (
                data.get("name"),
                data.get("location_id"),
                data.get("logo"),
                data.get("icon_flag"),
                json.dumps(data.get("styles", {})),
                data.get("default_language"),
                data.get("status"),
                data.get("domain"),
                False,
                datetime.datetime.now(),
                user['id'],
                datetime.datetime.now(),
                data.get("flag_code"),
                '14e76d3b-da24-4b6f-a8be-0dab254d98b0',
                data.get("description", "No Description"),
                data.get("account_logo", None),
                data.get("account_flag", None),
                new_uuid,
            ))
            result = await cur.fetchone()

        # Create bot user
        if result is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO _iw.users
                        (group_id, account_id, uuid, status, email, password, name, created, created_by, registered, profile, language, api_token)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                    10,
                    result.get("id"),
                    str(uuid.uuid4()),
                    'ACTIVE',
                    'bot@ewars.ws',
                    '1111',
                    "%s Bot" % (result.get("name")),
                    datetime.datetime.now(),
                    1,
                    datetime.datetime.now(),
                    json.dumps(dict(bio="Bot account")),
                    'en',
                    str(uuid.uuid4()),
                ))

    return result


async def get_accounts_as_options(option_set, user=None):
    """Retrieve option set of active accounts

    Retrieves the list of active accounts as an options set

    Args:
        option_set: Unused
        user: The calling user

    Returns:
        A list of [[id, name]] options for accounts

    """
    options = []
    raw_options = []

    if option_set == "forms":
        async with get_db_cur() as cur:
            await cur.execute("SELECT id, name FROM %s.forms WHERE status = 'ACTIVE'" % (
                AsIs(user.get("tki")),
            ))
            raw_options = await cur.fetchall()

        for option in raw_options:
            options.append([option['id'], option['name']])
    elif option_set == "forms_restricted":
        async with get_db_cur() as cur:
            await cur.execute(
                "SELECT id, name FROM %s.forms WHERE status = 'ACTIVE' AND time_interval IS NOT NULL AND single_report_context = TRUE" % (
                    AsIs(user.get("tki")),
                ))
            raw_options = await cur.fetchall()

        for option in raw_options:
            options.append([option['id'], option['name']])

    return options


async def get_account_stats(user=None):
    """Retrieve stats about a specific account

    Args:
        user: The calling user

    Returns:
        Returns a dict of basic stats for the account in question
    """
    account_id = user['aid']

    data = dict(
        #: The number of reports
        reports=0,
        #: Number of assignments
        assignments=0,
        #: Number of locations
        locations=0,
        #: Number of users
        users=0,
        #: Number of organizations active in the account
        partners=0
    )

    async with get_db_cur() as cur:
        # Get report count
        await cur.execute("""
            SELECT COUNT(*) AS total_reports
                FROM %s.collections AS C
                    LEFT JOIN %s.locations AS l ON l.uuid = C.location_id
                WHERE l.lineage @> %s
                    AND C.status = 'SUBMITTED';
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [user.get("clid")],
        ))
        r_count = await cur.fetchone()
        data['reports'] = r_count.get('total_reports')

        # Get assignment count
        await cur.execute("""
            SELECT COUNT(*) AS total_assignments
                FROM %s.assignments AS a
                WHERE a.status = 'ACTIVE';
        """, (
            AsIs(user.get("tki")),
        ))
        r_count = await cur.fetchone()
        data['assignments'] = r_count.get('total_assignments')

        # Get locations, this equals the assignable locations within the system
        forms = []
        await cur.execute("""
            SELECT id, site_type_id
                FROM %s.forms
                WHERE status = 'ACTIVE'
                    AND ftype = 'PRIMARY';
        """, (
            AsIs(user.get("tki")),
        ))
        forms = await cur.fetchall()

        location_types = []
        for form in forms:
            if form['site_type_id'] is not None:
                if int(form['site_type_id']) not in location_types:
                    location_types.append(int(form['site_type_id']))

        if len(location_types) > 0:
            await cur.execute("""
                SELECT COUNT(*) AS total_locations
                    FROM %s.locations
                    WHERE site_type_id IN %s
                        AND status = 'ACTIVE'
                        AND lineage::TEXT[] @> %s::TEXT[]
            """, (
                AsIs(user.get("tki")),
                tuple(location_types),
                [user['account']['location_id']],
            ))
            r_count = await cur.fetchone()
            data['locations'] = r_count.get('total_locations')
        else:
            data['location'] = 0

        await cur.execute("""
            SELECT COUNT(*) AS total_users
                FROM _iw.sso
                WHERE aid = %s
        """, (
            account_id,
        ))
        r_count = await cur.fetchone()
        data['users'] = r_count.get('total_users')

        await cur.execute("""
            SELECT COUNT(DISTINCT org_id) AS total
              FROM _iw.sso
              WHERE org_id IS NOT NULL
                AND aid = %s
              GROUP BY org_id;
        """, (
            account_id,
        ))
        items = await cur.fetchall()
        data['partners'] = sum([x['total'] for x in items])

    return data


async def get_unassigned_locations(filters, user=None):
    """Find locations which do not have users assigned to them

    Args:
        filters: Filter the list of locations based on form
        user: The calling user

    Returns:
        Returns a list of locations which do not have users assigned to them
    """
    matrix = {}

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, location_type
            FROM %s.forms
            WHERE status = 'ACTIVE'
            AND single_report_context = TRUE
        """ % (
            AsIs(user.get("tki")),
        ))
        forms = await cur.fetchall()

    for form in forms:
        matrix[form.get("id")] = dict(
            form_name=form.get("name"),
            no_assign=[]
        )

        locations = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT, name
                FROM %s.locations
                WHERE lineage @> '{%s}'
                AND site_type_id = '%s'
                AND status = 'ACTIVE'
            """ % (
                AsIs(user.get("tki")),
                user.get("clid"),
                form.get("location_type")[0],
            ))
            locations = await cur.fetchall()

        for location in locations:
            has_assign = False
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT COUNT(*) AS total_assigns
                    FROM %s.assignments
                    WHERE definition->>'location_uuid' = %s
                    AND definition->>'form_id' = %s::TEXT
                    AND status = 'ACTIVE'
                    """, (
                    AsIs(user.get("tki")),
                    location.get("uuid"),
                    form.get('id'),
                ))

                assigns = await cur.fetchone()
                if assigns['total_assigns'] > 0:
                    has_assign = True

            if has_assign == False:
                matrix[form.get("id")]['no_assign'].append([location.get("uuid"), location.get("name")])

    return matrix


async def update_account(account_id, data, user=None):
    """Update an account within the system

    Args:
        account_id:
        data:
        user:

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.accounts
                SET name = %s,
                    location_id = %s,
                    default_language = %s,
                    iso2 = %s,
                    iso3 = %s,
                    status = %s,
                    domain = %s,
                    last_modified = NOW(),
                    flag_code = %s,
                    description = %s,
                    account_logo = %s,
                    account_flag = %s
                WHERE id = %s
        """, (
            data.get("name"),
            data.get("location_id"),
            data.get("default_language"),
            data.get("iso2", None),
            data.get("iso3", None),
            data.get("status"),
            data.get("domain", None),
            data.get("flag_code"),
            data.get("description", None),
            data.get("account_logo", None),
            data.get("account_flag", None),
            account_id,
        ))

        await cur.execute("""
            SELECT * FROM _iw.accounts WHERE id = %s;
        """, (
            account_id,
        ))
        result = await cur.fetchone()

    return result


async def delete_account(account_id, user=None):
    """ Delete and account from the system

    Args:
        account_id:
        user:

    Returns:

    """
    # We need to delete a lot of stuff before we can delete the actual account

    account = None
    alerts = []
    forms = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.accounts
            WHERE id = %s
        """, (
            account_id,
        ))
        account = await cur.fetchone()

        await cur.execute("""
            DROP SCHEMA %s CASCADE;
        """, (
            account.get("tki"),
        ))

        await cur.execute("""
            DELETE FROM _iw.users_accounts WHERE account_id = %s;
        """, (
            account.get("id"),
        ))

    return None
