import datetime
import uuid
import json

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.core.authentication import generate_password
from ewars.core import notifications
from ewars.core.tasks import create_task
# from ewars.core.task_types.registration_request import REQUEST_HTML, REQUEST_SUBJECT
from ewars.content import emails

import tornado.ioloop

DEFAULT_NOTIFICATIONS = {
    "ALERT_TRIGGERED": "EMAIL",
    "ALERT_CLOSED": "EMAIL",
    "ALERT_REOPENED": "EMAIL",
    "ALERT_COMMENT": "EMAIL",
    "ALERT_AUTOCLOSED": "EMAIL",
    "ALERT_OTHER": "NOTIFY",
    "SUCCESS_SUBMIT": "NOTIFY",
    "AMENDMENT_REJECTED": "EMAIL",
    "AMENDMENT_APPROVED": "EMAIL",
    "RETRACT_APPROVED": "EMAIL",
    "RETRACT_REJECTED": "EMAIL",
    "OVERDUE_REPORT": "NOTIFY",
    "REPORT_COMMENT": "NOTIFY",
    "SUBMISSION_OTHER": "NOTIFY",
    "WEEKLY_DIGEST": "EMAIL",
    "DOCUMENT_AVAILABLE": "NOTIFY",
    "OTHER_DIGEST": "IGNORE",
    "FORM_REVISION": "NOTIFY",
    "FORM_NEW": "NOTIFY",
    "FORM_OTHER": "IGNORE",
    "NEW_INDICATOR": "IGNORE",
    "INDICATOR_OTHER": "IGNORE",
    "NEW_TASK": "EMAIL",
    "NEW_LOCATION_REPORTING": "IGNORE",
    "CLOSED_LOC_REPORTING": "IGNORE",
    "LOC_DELETED": "IGNORE",
    "OTHER_LOCATION": "IGNORE"
}


async def delete_invite(invite_uuid):
    """ Delete an invite from the system

    Args:
        invite_uuid (str): The UUID of the invite to delete

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            DELETE FROM _iw.invites WHERE uuid = %s;
        """, (
            invite_uuid,
        ))

    return True


async def send_welcome_existing(user_id, tki=None):
    """ Send the welcome email for an existing user being granted access

    Args:
        user_id (int): THe id of the user being added
        tki (str): The TKI of the account

    Returns:

    """
    user = None
    account = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.users WHERE id = %s;
        """, (
            AsIs(tki),
            user_id,
        ))
        user = await cur.fetchone()

        await cur.execute("""
            SELECT domain FROM _iw.accounts WHERE id = %s;
        """, (
            user.get("aid"),
        ))
        account = await cur.fetchone()

    cur_loop = tornado.ioloop.IOLoop.current()
    cur_loop.spawn_callback(
        notifications.send,
        'WELCOME',
        user,
        dict(
            account_name=user.get('account_name'),
            name=user.get('name'),
            email=user.get('email'),
            account_domain=account.get('domain')
        ),
        account=user.get("account_name", None),
        user=user
    )


async def send_welcome_invite_create(user_id, raw_password, tki=None):
    """ Send the welcome email for an invited user

    Args:
        user_id (int): The id of the user to send the email to
        tki (str): The TKI of the account they were added to

    Returns:

    """
    user = None
    account = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.users
            WHERE id = %s;
        """, (
            AsIs(tki),
            user_id,
        ))
        user = await cur.fetchone()

        await cur.execute("""
            SELECT domain FROM _iw.accounts WHERE id = %s;
        """, (
            user.get("aid"),
        ))
        account = await cur.fetchone()


    if user is not None:
        cur_loop = tornado.ioloop.IOLoop.current()
        cur_loop.spawn_callback(
            notifications.send,
            'WELCOME',
            user.get('id'),
            dict(
                account_name=user.get('account_name'),
                name=user.get('name'),
                password=raw_password,
                email=user.get('email'),
                account_domain=account.get('domain')
            ),
            account=user.get("account_name", None),
            user=user
        )


async def add_activity_new_user(user_id, attachments, tki=None):
    """ Add an activity feed item for a new user joining

    Args:
        user_id (int): The id the user that was added
        attachments (list): A list of attachments for the activity feed item
        tki (str): The token for the account to add to

    Returns:
        (bool): Whether the operation compelted

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO %s.activity_feed
            (event_type, triggered_by, icon, attachments)
            VALUES (%s, %s, %s, %s);
        """, (
            AsIs(tki),
            "NEW_USER",
            user_id,
            "fa-user",
            json.dumps(attachments),
        ))

    return True


async def query_organizations(param):
    """ Public querying of organizations

    Args:
        param: An optional search param

    Returns:

    """
    results = []

    if param is None or param == "":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, name, acronym
                FROM _iw.organizations
                WHERE status = 'ACTIVE'
                ORDER BY name->>'en'
            """)
            results = await cur.fetchall()
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, name, acronym
                FROM _iw.organizations
                WHERE name->>'en' ILIKE %s
                AND status = 'ACTIVE'
                ORDER BY name->>'en'
            """, (
                AsIs("'%" + param + "%'"),
            ))
            results = await cur.fetchall()

    return results


async def query_accounts(param):
    """ Public querying of accounts

    Args:
        param: An optional search param

    Returns:

    """
    results = []

    if param is None or param == "":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, domain
                FROM _iw.accounts
                WHERE status = 'ACTIVE'
                ORDER BY name;
            """)
            results = await cur.fetchall()
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, domain
                FROM _iw.accounts
                WHERE status = 'ACTIVE'
                AND name ILIKE %s
                ORDER BY name
            """, (
                AsIs("'%" + param + "%'"),
            ))
            results = await cur.fetchall()

    return results


async def get_token_by_id(account_id):
    """ Get account token by account id

    Args:
        account_id:

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT tki FROM _iw.accounts
            WHERE id = %s;
        """, (
            account_id,
        ))
        result = await cur.fetchone()

    if result is None:
        return None
    else:
        return result.get("tki", None)


async def get_token_by_domain(domain):
    """ Get account token by domain

    Args:
        domain:

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT tki FROM _iw.accounts
            WHERE domain = %s;
        """, (
            domain,
        ))
        result = await cur.fetchone()

    if result is None:
        return None
    else:
        return result.get("tki", None)


async def has_sso_user(email):
    """ Get an SSO user record

    Args:
        email: The email to use to look up the user

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, email, status
            FROM _iw.users
            WHERE email = %s;
        """, (
            email,
        ))
        result = await cur.fetchone()

    return result


async def has_account_user(email, token):
    """ Get an account specific user record

    Args:
        email: The email of the user to look up
        token: The account token

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, email, status
            FROM %s.users
            WHERE email = %s;
        """, (
            AsIs(token),
            email,
        ))
        result = await cur.fetchone()

    return result


async def check_email_registered(email_address, domain):
    """ Check an email address to see if it

    Args:
        email_address:

    Returns:

    """
    result = False
    total_users = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) total FROM _iw.users WHERE email = %s AND status != 'DELETED'
        """, (
            email_address,
        ))
        total_users = await cur.fetchone()['total']

    if total_users > 0:
        result = True
    else:
        result = False

    return dict(
        result=result
    )


async def create_sso_user(email, password, name, org_id, account_id, status, org_name=None):
    result = None

    enc_password = generate_password(password)

    profile = dict()
    if org_name is not None:
        profile["organization_name"] = org_name

    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO _iw.users
            (uuid, status, group_id, account_id, email, password, name, created, created_by, registered, profile, org_id, language, api_token, user_type, notifications, accounts, system)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
        """, (
            str(uuid.uuid4()),
            status,
            99,
            account_id,
            email,
            enc_password,
            name,
            datetime.datetime.now(),
            3,
            datetime.datetime.now(),
            json.dumps(profile),
            org_id,
            'en',
            str(uuid.uuid4()),
            'USER',
            json.dumps(DEFAULT_NOTIFICATIONS),
            [account_id],
            False,
        ))
        result = await cur.fetchone()

    return result


async def create_acc_context_user(tki, user_id, account_id, status, role, location, created_by):
    """ Create a record for a user in an account context

    Args:
        tki (str): The token of the account context
        user_id (int): The user id to add
        account_id (int): The account id
        status (str): The users initial status
        role (str): The users initial role
        created_by (int): Who created this user

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO %s.accounts
            (user_id, aid, status, role, created_by, location_id)
            VALUES (%s, %s, %s, %s, %s, %s);
        """, (
            AsIs(tki),
            user_id,
            account_id,
            status,
            role,
            created_by,
            location,
        ))

        # Append account id to the users root level account
        await cur.execute("""
            UPDATE _iw.users 
            SET accounts = accounts || %s 
            WHERE id = %s
            AND NOT (%s = ANY(accounts));
        """, (
            [account_id],
            user_id,
            account_id,
        ))

    return result


async def register(email, password, name, org_id, account_id):
    """ Process a registration request

    Args:
        email (str): The registering users email address
        password (str): The registering users password
        name (str): The name of the new user
        org_id (str): The organization id for the user
        account_id (int): The account id that the user is registering for

    Returns:

    """
    result = None
    account = None

    password = generate_password(password)

    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO _iw.users
            (uuid, status, group_id, account_id, email, password, name, created, created_by, registered, profile, org_id, language, api_token, user_type, notifications)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
        """, (
            str(uuid.uuid4()),
            'PENDING_VERIFICATION',
            99,
            account_id,
            email,
            password,
            name,
            datetime.datetime.now(),
            3,
            datetime.datetime.now(),
            '{}',
            org_id,
            'en',
            str(uuid.uuid4()),
            'USER',
            json.dumps(DEFAULT_NOTIFICATIONS),
        ))
        result = await cur.fetchone()

        await cur.execute("""
            SELECT name FROM _iw.accounts
            WHERE id = %s;
        """, (
            AsIs(account_id),
        ))
        account = await cur.fetchone()

        await cur.execute("""
            INSERT INTO _iw.users_accounts (user_id, account_id, role, definition, status)
            VALUES (%s, %s, %s, %s, %s);
        """, (
            result.get('id'),
            account_id,
            "USER",
            json.dumps({}),
            'PENDING',
        ))

    # Create verification email
    loop = tornado.ioloop.IOLoop.instance()
    loop.spawn_callback(
        notifications.send,
        'EMAIL_VERIFICATION',
        result,
        data=dict(
            verification_code=result.get('api_token')
        ),
        account=account.get("name", None),
        user=dict()
    )

    return True


async def get_account_admins(acc_token):
    """ Get admins for the account in question

    Args:
        acc_token (str): The token of the account to get from

    Returns:

    """
    results = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT *
            FROM %s.users
            WHERE role = 'ACCOUNT_ADMIN'
            AND status = 'ACTIVE';
        """, (
            AsIs(acc_token),
        ))
        results = await cur.fetchall()

    return results


async def create_approval_task(user_id, acc_token):
    """ Create the approval task for a new user

    Args:
        user_id (int): The id of the user who needs approval
        acc_token (int): The TKI token for the account in question

    Returns:

    """
    sso_user = None
    account = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.users
            WHERE id = %s;
        """, (
            user_id,
        ))
        sso_user = await cur.fetchone()

        await cur.execute("""
            SELECT name FROM _iw.accounts
            WHERE tki = %s;
        """, (
            AsIs(acc_token),
        ))
        account = await cur.fetchone()

    task_data = dict(
        user_name=sso_user.get("name"),
        email=sso_user.get("email"),
        org_id=sso_user.get("org_id"),
        user_id=sso_user.get("id")
    )

    async with get_db_cur() as cur:
        await cur.execute("""
         INSERT INTO %s.tasks_v2
         (
          task_type,
          assigned_user_id,
          assigned_user_types,
          data,
          location_id,
          state,
          priority,
          created_by
         )
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
        """, (
            AsIs(acc_token),
            "REGISTRATION_REQUEST",
            None,
            ["ACCOUNT_ADMIN"],
            json.dumps(task_data),
            None,
            "OPEN",
            "LOW",
            user_id,
        ))

    admins = await get_account_admins(acc_token)

    cur_loop = tornado.ioloop.IOLoop.current()

    cur_loop.spawn_callback(
        notifications.send,
        'REGISTRATION_REQUEST',
        admins,
        task_data,
        user=dict(
            tki=acc_token
        ),
        account=account.get('name', None),
        system=True
    )


async def add_account(user_id, account_id):
    """ Add an account to a users list of accounts

    Args:
        user_id (int): The id of hte user to add
        account_id (int): The id of the account to add to

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execue("""
            UPDATE _iw.users
            SET accounts || %s
            WHERE id = %s
            AND NOT (%s = ANY(accounts));
        """, (
            [account_id],
            user_id,
            account_id,
        ))
