import uuid
import datetime
import json

# from ewars.core.tasks import RegistrationRequest
from ewars.core.serializers.json_encoder import JSONEncoder
from ewars.core.identity import authentication
from ewars.core.notifications import send
from .dashboards import get_default_dashboards, get_default_dashboard
from ewars.utils.six import string_types
from ewars.utils import date_utils
from ewars.core.locations import utils
from ewars.content import emails

from ewars.db import get_db_cur

from ewars.db import get_db_cursor
from psycopg2.extensions import AsIs

from ewars.constants import CONSTANTS

DEFAULT_NOTIFICATIONS = {
    "ALERT_TRIGGERED": "EMAIL",
    "ALERT_CLOSED": "EMAIL",
    "ALERT_REOPENED": "EMAIL",
    "ALERT_COMMENT": "EMAIL",
    "ALERT_AUTOCLOSED": "EMAIL",
    "ALERT_OTHER": "NOTIFY",

    "SUCCESS_SUBMIT": "NOTIFY",
    "AMENDMENT_REJECTED": "EMAIL",
    "AMENDMENT_APPROVED": "EMAIL",
    "RETRACT_APPROVED": "EMAIL",
    "RETRACT_REJECTED": "EMAIL",
    "OVERDUE_REPORT": "NOTIFY",
    "REPORT_COMMENT": "NOTIFY",
    "SUBMISSION_OTHER": "NOTIFY",

    "WEEKLY_DIGEST": "EMAIL",
    "DOCUMENT_AVAILABLE": "NOTIFY",
    "OTHER_DIGEST": "IGNORE",

    "FORM_REVISION": "NOTIFY",
    "FORM_NEW": "NOTIFY",
    "FORM_OTHER": "IGNORE",

    "NEW_INDICATOR": "IGNORE",
    "INDICATOR_OTHER": "IGNORE",

    "NEW_TASK": "EMAIL",
    "NEW_LOCATION_REPORTING": "IGNORE",
    "CLOSED_LOC_REPORTING": "IGNORE",
    "LOC_DELETED": "IGNORE",
    "OTHER_LOCATION": "IGNORE"
}


async def revoke(user_id, user=None):
    """ Revoke a users access to an account

    Args:
        user_id: The id of the user
        user: THe user requesting the revoke

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE %s.accounts
            SET status = 'DELETED'
            WHERE user_id = %s;
        """, (
            AsIs(user.get("tki")),
            user_id,
        ))

    return True


async def check_email(email, user=None):
    # check if there's a user in the account already
    user = None
    add_type = "NEW"

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, email, name
            FROM %s.users
            WHERE email = %s;
        """, (
            AsIs(user.get("tki")),
            email,
        ))
        user = await cur.fetchone()

    # there's no user in the account with this email
    if user is None:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, email
                FROM _iw.users
                WHERE email = %s;
            """, (
                email,
            ))
            user = await cur.fetchone()

        if user is not None:
            ADD_TYPE = "REGISTER"
    else:
        add_type = "EXISTS"

    return dict(
        result=add_type,
        user=user
    )





async def update_user(user_id, data, user=None):
    """
    Update a users user record, you can not update a users password through this API, you need to use the seconday
    user password update API
    :param user_id: {int} Thd id of the user
    :param data: {dict} The data to update
    :param user: {dict} The calling uyser
    :return:
    """
    cap_user = None
    end_user = None

    async with get_db_cur() as cur:
        await cur.execute("""
         SELECT * FROM _iw.users
         WHERE id = %s;
        """, (
            user_id,
        ))
        cap_user = await cur.fetchone()

    profile = cap_user.get("profile", dict())
    if "bio" in data.keys():
        profile['bio'] = data.get("bio", None)

    if "occupation" in data.keys():
        profile['occupation'] = data.get("occupation", None)

    if "profile" in data.keys():
        profile = data.get("profile", dict())

    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.users
            SET name = %s,
              email = %s,
              phone = %s,
              org_id = %s,
              profile = %s
            WHERE id = %s;
        """, (
            data.get("name"),
            data.get("email"),
            data.get("phone"),
            data.get("org_id"),
            json.dumps(profile),
            user_id,
        ))

        await cur.execute("""
            UPDATE %s.accounts
            SET role = %s,
              location_id = %s,
              status = %s
            WHERE user_id = %s;
        """, (
            AsIs(user.get("tki")),
            data.get("role"),
            data.get("location_id", None),
            data.get("status"),
            user_id,
        ))

        await cur.execute("""
            SELECT * FROM %s.users WHERE id = %s;
        """, (
            AsIs(user.get("tki")),
            user_id,
        ))
        end_user = await cur.fetchone()

    return end_user


async def update_password(user_id, new_password, user=None):
    """  Update a users password
    Args:
        user_id: The id of the user to update
        new_password: The new plaintext password to update for them

    Returns:

    """
    password_salt = authentication.generate_random_salt()
    password = authentication.generate_password(new_password, password_salt=password_salt)

    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE _iw.users
                SET password = %s
                WHERE id = %s
        """, (
            password,
            user_id,
        ))


def get_full_user(user_id):
    """
    Retrieve a full users account
    :param user_id: {int} The ID of the user to retrieve
    :return:
    """
    result = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM _iw.users WHERE id = %s
        """, (
            user_id,
        ))
        result = cur.fetchone()

    return result


async def create_user(data, user=None):
    """ Create a new user in the system

    Args:
        data (obj:`str`): The data for the user
            email (str): Email address of the user to add
            name (str): The name of the user to add
            password (str): The desired password for the user
            phone (str): The users phoen number
            occupation (str): The user occupation
            role (str): The target role for the user in the account
            status (str): The users access status for the account
            location (str optional): If a REGIONAL_ADMIN, the location they're sandboxed to
            system (bool): Whether the user is account controlled
        user (obj:`str`):  The calling user

    Returns:
        Success message

    Raises:
        USER_SENT_INVITE: The email address belongs to a user that was sent an invite
        USER_EXISTS_IS_ACTIVE: The email address belongs to a user that exists and is active
        USER_EXISTS_IS_INACTIVE: The email address belongs to a user that exists but is set to inactive
        USER_EXISTS_SSO: A user within EWARS already exists with this user; prompt to invite
        USER_EXISTS_IS_REVOKED: The email address belongs to a user that exists but whose access is currently revoked from the account

    ToDo:
        * Send a notification to the user with their password informing they've been added

    """

    invite = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.invites
            WHERE email = %s;
        """, (
            AsIs(user.get("tki")),
            data.get("email"),
        ))
        invite = await cur.fetchone()

    # There's a user with this email whose been invited already
    if invite is not None:
        return dict(
            err=True,
            code="USER_SENT_INVITE"
        )

    # Check if there's an existing user already
    acc_user = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, email, name, status
            FROM %s.users
            WHERE email = %s;
        """, (
            AsIs(user.get("tki")),
            data.get("email"),
        ))
        acc_user = await cur.fetchone()

    # Check the status of the user
    if acc_user is not None:
        # User exists and is active
        if acc_user.get("status") == "ACTIVE":
            return dict(
                err=True,
                code="USER_EXISTS_IS_ACTIVE"
            )

        # User exists but is active; prompt to reactive
        if acc_user.get("status") == "INACTIVE":
            return dict(
                err=True,
                code="USER_EXISTS_IS_INACTIVE"
            )

        if acc_user.get("status") == "DELETED":
            return dict(
                err=True,
                code="USER_EXISTS_IS_REVOKED"
            )
    else:
        # No user, check if there's an root user
        root_user = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, email
                FROM _iw.users
                WHERE email = %s;
            """, (
                data.get("email"),
            ))
            root_user = await cur.fetchone()

        # A root user with this email already exists; send them an invite
        if root_user is not None:
            return dict(
                err=True,
                code="USER_EXISTS_AT_ROOT"
            )
        else:

            # No user in the system, lets create one
            password_salt = authentication.generate_random_salt()
            password = authentication.generate_password(data.get("password"), password_salt=password_salt)

            result = None

            # Insert user record
            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO _iw.users
                    (uuid, status, group_id, account_id, email, password, name, phone, created, created_by, registered, profile, org_id, language, api_token, user_type,
                    location_id, notifications, timezone, date_format, time_format, pronoun, accessibility, email_format, desk_not_status, email_status, accounts, system)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;

                """, (
                    str(uuid.uuid4()),
                    data.get("status", "INACTIVE"),
                    4,
                    data.get("aid"),
                    data.get("email"),
                    password,
                    data.get("name"),
                    data.get("phone"),
                    datetime.datetime.now(),
                    user.get("id"),
                    datetime.datetime.now(),
                    json.dumps(dict(
                        bio=data.get("bio", None),
                        occupation=data.get("occupation", None)
                    )),
                    data.get("org_id", None),
                    'en',
                    str(uuid.uuid4()),
                    data.get("role", "USER"),
                    data.get("location", None),
                    json.dumps(DEFAULT_NOTIFICATIONS),
                    'DEFAULT',
                    'DEFAULT',
                    'DEFAULT',
                    'DEFAULT',
                    'DEFAULT',
                    'DEFAULT',
                    'DISABLED',
                    'DEFAULT',
                    [user.get("aid")],
                    data.get("system", False),
                ))
                new_user = await cur.fetchone()

                # INsert account record
                await cur.execute("""
                    INSERT INTO %s.accounts
                    (user_id, aid, status, role, location_id, created_by)
                    VALUES (%s, %s, %s, %s, %s, %s);
                """, (
                    AsIs(user.get("tki")),
                    new_user.get("id"),
                    user.get("aid"),
                    data.get('status'),
                    data.get("role"),
                    data.get("location"),
                    user.get("id"),
                ))

                await cur.execute("""
                    SELECT * FROM %s.users WHERE id = %s;
                """, (
                    AsIs(user.get("tki")),
                    new_user.get("id"),
                ))
                result = await cur.fetchone()

            # Send notification to new users
            cur_loop = tornado.ioloop.IOLoop.current()
            cur_loop.spawn_callback(
                notifications.send,
                'WELCOME',
                new_user.get("id"),
                dict(
                    email=result.get("email"),
                    name=result.get("name"),
                    account_name=result.get("account_name"),
                    account_domain=user.get("account_domain", "ewars.ws"),
                    password=data.get("password"),
                    adder_name=user.get("name"),
                    adder_email=user.get("email")
                ),
                user=user
            )

            # Create activity feed item
            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO %s.activity_feed
                    (event_type, triggered_by, created, icon, attachments)
                    VALUES (%s, %s, %s, %s, %s);
                """, (
                    AsIs(user.get("tki")),
                    "NEW_USER",
                    new_user.get("id"),
                    datetime.datetime.now(),
                    "fa-clipboard",
                    json.dumps([["USER", new_user.get("id")]])
                ))

                # TODO: Add an activity feed item for the user joingin

    return result


def register_user(instance, data):
    """
    routing for registering a user within the system
    :param data:
    :return:
    """

    password_salt = authentication.generate_random_salt()
    password = authentication.generate_password(data.get("password"), password_salt=password_salt)
    user = None
    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            INSERT INTO _iw.users
                (uuid, status, group_id, account_id, email, password, name, created, created_by, registered, profile, org_id, bounced, language, api_token, location_id, lab_id, user_type)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *
        """, (
            str(uuid.uuid4()),
            'PENDING',
            6,
            data.get("account_id", None),
            data.get("email"),
            password,
            data.get("name"),
            datetime.datetime.now(),
            1,
            datetime.datetime.now(),
            '{}',
            data.get("org_id", None),
            False,
            "en",
            str(uuid.uuid4()),
            data.get("location_id", None),
            data.get("lab_id", None),
            data.get("user_type"),
        ))
        user = cur.fetchone()

        if data.get("account_id", None) is not None:
            cur.execute("""
                INSERT INTO _iw.users_accounts (user_id, account_id, role, definition)
                VALUES (%s, %s, %s, %s);
            """, (
                user['id'],
                data.get("account_id"),
                data.get("user_type"),
                json.dumps(dict()),
            ))

    # Send registration confirmation email
    # email_notification.send_email(user, "REGISTRATION_CONFIRMATION",
    #                               data=dict(user=user))
    temp_user = {
        "account_id": data.get("account_id"),
        "id": 1
    }
    # task = RegistrationRequest(user.get("id"), temp_user)
    # task.process()

    return None


def get_own_assignments(user=None):
    results = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.*, l.name AS location_name, f.name AS form_name
            FROM %s.assignments AS a
                LEFT JOIN %s.locations AS l on l.uuid::TEXT = a.location_id::TEXT
                LEFT JOIN %s.forms AS f ON f.id::TEXT = a.form_id::TEXT
            WHERE a.user_id = %s
                AND a.status != 'DELETED'
                AND a.status != 'INACTIVE'
                AND f.status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user.get("id"),
        ))
        results = cur.fetchall()

    return results


def get_assignments(user_id, user=None):
    """
    Get a users active and pending assignments
    :param user_id: {int} The id of the user
    :param user: {dict} The calling user
    :return:
    """
    results = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.*, l.name AS location_name, f.name AS form_name
            FROM %s.assignments AS a
                LEFT JOIN %s.locations AS l on l.uuid::TEXT = a.location_id::TEXT
                LEFT JOIN %s.forms AS f ON f.id::TEXT = a.form_id::TEXT
            WHERE a.user_id = %s
                AND a.status != 'DELETED'
                AND a.status != 'INACTIVE'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user_id,
        ))
        results = cur.fetchall()

    return results


def get_tokens(host):
    """ Get an hosts tokens from the accounts and instances tables
    :param host: {str} The domain the application is being called on
    :return:
    """
    account = None

    host_copy = None
    if ":" in host:
        host_copy = host.split(":")[0]
    else:
        host_copy = host

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM _iw.accounts WHERE domain = %s
        """, (
            host_copy,
        ))
        account = cur.fetchone()

    return account


def get_dummy_user(account_id):
    """Create a dummy user for accessing some data
    :param account_id: {int} The account id to associate the user to
    """
    user = dict(
        id=None,
        name="None",
        account_id=account_id
    )

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT tki FROM _iw.accounts WHERE id = %s;
        """, (
            account_id,
        ))
        user['tki'] = cur.fetchone()['tki']

    return user



def get_user(user=None):
    """ Retrieve details about the calling user

    Args:
        user: The calling user

    Returns:

    """
    result = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM _iw.users
            WHERE id = %s;
        """, (
            user.get("id"),
        ))
        result = cur.fetchone()

    return result


def get_user_by_id(user_id):
    """ Get a user by their id

    Args:
        user_id: The id of the user to look for

    Returns:
        The user record in question
    """
    result = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM _iw.users
            WHERE id = %s;
        """, (
            user_id,
        ))
        result = cur.fetchone()

    return result


async def get_self(user=None):
    """ Let a user get their own record

    Args:
        user: The calling using

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.users WHERE id = %s;
        """, (
            user.get('id'),
        ))
        result = await cur.fetchone()

    return result


async def grant_access(user_id, data, user=None):
    """ Grant an existing wider EWARS system user access to a specific account

    Args:
        user_id: The id of the user to grant access to
        data: The description of their role in the new system
        user: The calling user

    Returns:
        The new localized record for the user
    """
    result = None
    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO %s.accounts
            (user_id, aid, status, role, location_id, created_by)
            VALUES (%s, %s, %s, %s, %s, %s);
        """, (
            AsIs(user.get("tki")),
            user_id,
            user.get("aid"),
            data.get('status'),
            data.get("role"),
            data.get("location_id"),
            user.get("id"),
        ))

        await cur.execute("""
            SELECT * FROM %s.users
            WHERE id = %s;
        """, (
            AsIs(user.get("tki")),
            user_id,
        ))
        result = await cur.fetchone()

    # TODO: Send an email to the user notifying them they've been granted access
    # TODO: Create an activity feed item for the user joining

    return result


async def reinstate(user_email, user=None):
    """ Reinstance a deleted user within an account

    Args:
        user_email (str): The email of the user to reinstate
        user (obj:`str`): The calling user

    Returns:
        resp (bool): Whether or not an error occurred

    """
    # Get the users id
    acc_user = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name
            FROM %s.users
            WHERE email = %s;
        """, (
            AsIs(user.get("tki")),
            user_email,
        ))
        acc_user = await cur.fetchone()

        if acc_user is not None:
            await cur.execute("""
                UPDATE %s.accounts
                SET status = 'ACTIVE'
                WHERE user_id = %s;
            """, (
                AsIs(user.get("tki")),
                acc_user.get("id"),
            ))

    return dict(
        err=False
    )
