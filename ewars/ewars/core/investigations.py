from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

from ewars.utils import date_utils
from .emails import send_alert_email
from .activity import add_alert_activity
from .users import get_notifiable_users


def get_alert_investigation_reports(alert_uuid, user=None):
    """Retrieve reports associated with an alert as investigative
    """
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT c.*, f.name AS form_name, u.name AS user_name, u.email AS user_email
            FROM %s.collections AS c
                LEFT JOIN %s.forms AS f ON f.id = c.form_id
                LEFT JOIN _iw.users AS u ON u.id = c.created_by
            WHERE c.alert_id = %s
                AND c.status = 'SUBMITTED'

        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alert_uuid,
        ))
        reports = cur.fetchall()

    return reports


def handle_investigation_report_submission(report_uuid, alert_id, user=None):
    """Perform some updates and notifications triggered from an investigation report
    being submitted to the alert
    :param report_uuid: {str} The UUID of the submitted report
    :param alert_id: {str} THe UUID of the alert
    :param user: {dict} THe calling user
    """

    report = None
    alert = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT c.form_id, f.name AS form_name, u.name AS user_name
                FROM %s.collections AS c
                LEFT JOIN %s.forms AS f ON f.id = c.form_id
                LEFT JOIN _iw.users AS u ON u.id = c.created_by
            WHERE c.uuid = %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            report_uuid,
        ))
        report = cur.fetchone()

        cur.execute("""
            SELECT a.trigger_end,
                al.name AS alarm_name,
                al.date_spec_interval_type,
                l.name AS location_name
            FROM %s.alerts AS a
                LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.uuid = %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alert_id,
        ))
        alert = cur.fetchone()

    add_alert_activity(alert_id, "REPORT_SUBMISSION", "fa-clipboard", user.get("id"), None, user=user)

    notifiers = get_notifiable_users(alert_id, user=user)

    notification_data = dict(
        alarm_name=alert['alarm_name'].get("en", None),
        location_name=alert['location_name'].get("en", None),
        alert_date=date_utils.display(alert.get("trigger_end"), alert.get("date_spec_interval_type")),
        form_name=report['form_name'].get("en", None),
        user_name=report.get("user_name"),
        alert_uuid=alert_id
    )

    for notification in notifiers:
        send_alert_email(notification, "REPORT_SUBMISSION", notification_data)
