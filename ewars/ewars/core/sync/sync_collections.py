from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

def get_sync_package(user=None):
    results = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT * FROM %s.collections WHERE status = 'SUBMITTED';
        """, (
            AsIs(user.get("tki")),
        ))
        results = cur.fetchall()

    return results