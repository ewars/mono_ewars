import json
import uuid
import gzip
import shutil
import os
import datetime
import lzma

from ewars.db import get_db_cursor
from ewars.conf import settings
from ewars.utils._os import get_install_root

from ewars.core.serializers.json_encoder import JSONEncoder

from psycopg2.extensions import AsIs

TABLES = [
    "locations",
    "location_types",
    "forms",
    "form_versions",
    "indicators",
    "indicator_groups",
    "alarms",
    "alerts",
    "organizations",
    "conf"
]


def _sync_user(after_date, user=None):
    results = dict()
    storage_path = os.path.join(get_install_root(), "static/_sync")

    with get_db_cursor() as cur:
        for table in TABLES:
            cur.execute("""
                    SELECT * FROM %s.%s;
                """, (
                AsIs(user.get("tki")),
                AsIs(table),
            ))
            results[table] = cur.fetchall()

    file_uuid = str(uuid.uuid4())
    raw_file_path = "%s/%s.txt" % (storage_path, file_uuid,)

    with open(raw_file_path, "a") as f:
        f.write("#METADATA#" + os.linesep)
        f.write(json.dumps(METADATA, cls=JSONEncoder) + os.linesep)

        # Collect normal table data
        for sec, items in results.items():
            f.write("#%s#" % (sec.upper()) + os.linesep)
            f.writelines([json.dumps(x, cls=JSONEncoder) + os.linesep for x in items])
        f.write("#COLLECTIONS#" + os.linesep)

    # Collect reports
    with get_db_cursor(name="E_AUU") as cur:
        cur.execute("""
                SELECT uuid, location_id, data_date, data, form_id
                FROM %s.collections
                WHERE status = 'SUBMITTED';
            """, (
            AsIs(user.get("tki")),
        ))

        with open(raw_file_path, "a") as f:
            f.writelines(
                [json.dumps(report, cls=JSONEncoder) + os.linesep for report in ResultIter(cur, arraysize=250)])

    # compress the file
    with open("%s/%s.txt" % (storage_path, file_uuid,), 'rb') as f_in, gzip.open(
                    "%s/%s.txt.gz" % (storage_path, file_uuid,), 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)

    os.remove("%s/%s.txt" % (storage_path, file_uuid))

    return dict(
        result=True,
        id="http://127.0.0.1:9000/sync_pack/%s" % (file_uuid,)
    )


def _sync_admin(after_date, user=None):
    storage_path = os.path.join(get_install_root(), "static/_sync")

    file_uuid = str(uuid.uuid4())
    raw_file_path = "%s/%s.txt" % (storage_path, file_uuid,)

    with open(raw_file_path, "a") as f:
        f.write("#METADATA#" + os.linesep)
        f.write(json.dumps(METADATA, cls=JSONEncoder) + os.linesep)

        with get_db_cursor() as cur:
            # Forms
            cur.execute("""
                SELECT f.*, fv.definition
                FROM %s.forms AS f 
                 LEFT JOIN %s.form_versions AS fv on fv.uuid = f.version_id
                WHERE d.status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            forms = cur.fetchall()

            f.write("#FORMS#" + os.linesep)
            f.writelines([json.dumps(x, cls=JSONEncoder) + os.linesep for x in forms])

            # Locations
            cur.execute("""
                SELECT l.uuid, l.status, l.name, l.site_type_id, l.lineage, l.parent_id, sti.name
                FROM %s.locations AS l 
                  LEFT JOIN %s.location_types AS sti ON sti.id = l.site_type_id;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            locations = cur.fetchall()

            f.write("#LOCATIONS#")
            f.writelines([json.dumps(x, cls=JSONEncoder) + os.linesep for x in locations])

            # Users
            cur.execute("""
                SELECT id, name, email, role
                FROM %s.users;
            """, (
                AsIs(user.get("tki")),
            ))
            users = cur.fetchall()

            f.write("#USERS#")
            f.writelines([json.dumps(x, cls=JSONEncoder) + os.linesep for x in users])


    f.write("#COLLECTIONS#" + os.linesep)

    # Collect reports
    with get_db_cursor(name="E_AUU") as cur:
        cur.execute("""
                SELECT uuid, location_id, data_date, data, form_id
                FROM %s.collections
                WHERE status = 'SUBMITTED';
            """, (
            AsIs(user.get("tki")),
        ))

        with open(raw_file_path, "a") as f:
            f.writelines([json.dumps(report, cls=JSONEncoder) + os.linesep for report in ResultIter(cur, arraysize=250)])

    # compress the file
    with open("%s/%s.txt" % (storage_path, file_uuid,), 'rb') as f_in, gzip.open(
                    "%s/%s.txt.gz" % (storage_path, file_uuid,), 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)

    os.remove("%s/%s.txt" % (storage_path, file_uuid))

    return dict(
        result=True,
        id="http://127.0.0.1:9000/sync_pack/%s" % (file_uuid,)
    )


def _sync_geo(after_date, user=None):
    pass


def _sync_custom(after_date, user=None):
    pass


METADATA = dict(
    DB_VERSION="0.1",
    as_of=datetime.datetime.utcnow()
)


def ResultIter(cursor, arraysize=1000):
    while True:
        results = cursor.fetchmany(arraysize)

        if not results:
            break

        for result in results:
            yield result


def get_package(by_date=None, file_name=None, user=None):
    """ Get a complete initial syncable package

    Args:
        user:

    Returns:

    """
    if user.get("role") == "ACCOUNT_ADMIN":
        return _sync_admin(None, user=user)
