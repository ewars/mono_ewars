import json
import datetime
import collections

from jose import jwt, JWTError

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.models import Device, Submission

JWT_KEY = "cormorant"


async def _get_assignments(user=None):
    """
    Retrieve a users assignments
    :param user: {dict} The calling user
    :return:
    """
    assignments = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*, l.name AS location_name, l.uuid AS location_uuid, f.name AS form_name, f.id AS form_id
            FROM %s.assignments AS a
                LEFT JOIN %s.locations AS l ON l.uuid::TEXT = a.location_id
                LEFT JOIN %s.forms AS f ON f.id = a.form_id
            WHERE a.user_id = %s
                AND a.status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user['id'],
        ))
        assignments = await cur.fetchall()


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


async def _sync_reports(report_data, user=None):
    """
    Evaluate and insert reports from the sync manifest
    :param report_data: {list} A list of report dicts
    :param user: {dict} The calling user
    :return:
    """
    # Sort the reports so they're submitted in order from earliest to latest
    reports_sorted = sorted(report_data, key=lambda k: k['data_date'])

    # Iterate over the reports and evaluate whether they should be inserted or whether they're in
    # conflict, etc...
    results = []
    for report in report_data:
        # Check if the same uuid exists as submitted
        is_submitted = False
        result = None

        existing_reports = None

        if report.get("data_date", "").startswith("0000"):
            # This is a bad date, not sure what these are from; need to do some digging
            report['status'] = 'SUBMITTED'
        else:
            form = None

            async with get_db_cur() as cur:
                await cur.execute("""
                     SELECT id, features
                     FROM %s.forms
                     WHERE id = %s
                """, (
                    AsIs(user.get("tki")),
                    report.get("form_id"),
                ))
                form = await cur.fetchone()

                await cur.execute("""
                    SELECT uuid::TEXT, status, form_id, form_version_id::TEXT
                    FROM %s.collections
                    WHERE location_id::TEXT = %s
                        AND form_id = %s
                        AND data_date = %s
                        AND (status = 'SUBMITTED' OR status = 'PENDING_AMENDMENT')
                """, (
                    AsIs(user.get("tki")),
                    report.get("location_id"),
                    report.get("form_id"),
                    report.get("data_date"),
                ))
                existing_reports = await cur.fetchall()


            persist = True

            has_single_context = False
            if "INTERVAL_REPORTING" in form.get("features"):
                has_single_context = True

            if has_single_context:
                if len(existing_reports) > 0:
                    print('CONFLICT')
                    persist = False
                    report['status'] = "CONFLICT"

            if persist:
                flattened = flatten(report.get('data'), sep=".")
                if "data_date" in flattened:
                    del flattened['data_date']

                if "location_id" in flattened:
                    del flattened['location_id']

                report['data'] = flattened
                result = await Submission.submit(
                    report.get("form_id"),
                    report.get("form_version_id"),
                    report,
                    source="ANDROID",
                    user=user
                )
                report['status'] = 'SUBMITTED'


    sync_result = dict((report['uuid'], report['status']) for report in report_data)

    return sync_result


async def _sync_assignments(assign_data, user=None):
    """
    Sync a users assignments
    :param assign_data: {list} A list of assignments
    :param user: {dict} The calling user
    :return:
    """
    sync_results = dict()

    assignments = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.*,
                l.name AS location_name,
                l.uuid::TEXT AS location_uuid,
                f.name AS form_name,
                f.id AS form_id,
                f.version_id,
                f.block_future_dates,
                f.time_interval AS form_interval,
                f.location_aware,
                f.single_report_context,
                f.features,
                fv.definition AS form_definition
            FROM %s.assignments AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                LEFT JOIN %s.forms AS f ON f.id = a.form_id
                LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
            WHERE a.user_id = %s
                AND a.status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user['id'],
        ))
        assignments = await cur.fetchall()

    for assign in assignments:
        item = dict(
            uuid=assign['uuid'],
            location_id=assign['location_uuid'],
            location_name=assign['location_name'],
            start_date=assign.get("start_date"),
            end_date=assign.get("end_date")
        )

        existing_reports = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT data_date
                FROM %s.collections
                WHERE form_id = %s
                    AND location_id = %s
                    AND status = 'SUBMITTED'
            """, (
                AsIs(user.get("tki")),
                assign['form_id'],
                assign['location_uuid'],
            ))
            existing_reports = await cur.fetchall()

        item['existing'] = existing_reports

        location_aware = False
        block_future_dates = False
        single_report_context = False
        interval = "DAY"

        if "INTERVAL_REPORTING" in assign.get("features"):
            block_future_dates = True
            single_report_context = True
            interval = assign['features']['INTERVAL_REPORTING'].get("interval")

        if "LOCATION_REPORTING" in assign.get("features"):
            location_aware = True

        try:
            sync_results[assign['form_id']]['assignments'].append(item)
        except KeyError as e:
            sync_results[assign['form_id']] = dict(
                type="ASSIGNMENT",
                form_name=assign['form_name'],
                definition=assign['form_definition'],
                version_id=assign['version_id'],
                form_id=assign['form_id'],
                location_aware=location_aware,
                interval=interval,
                single_report_context=single_report_context,
                block_future_dates=block_future_dates,
                assignments=[item]
            )

    results = [value for key, value in sync_results.items()]
    return results


async def _sync_taxonomies(tax_data, user=None):
    sync_results = dict()

    taxonomies = []

    return sync_results


async def _sync_forms(form_data, user=None):
    sync_results = dict()

    return sync_results


async def _sync_user(data, user=None, device_id=None):
    """
    Sync a users profile to their phone
    :param data: {data} The user profile
    :param user: {dict} The calling user
    :return:
    """
    user_data = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT u.id, u.location_id::TEXT, u.name, u.email, u.aid, o.name AS organization_name, a.name AS account_name, u.role
            FROM %s.users AS u
                LEFT JOIN _iw.accounts AS a ON a.id = u.aid
                LEFT JOIN %s.organizations AS o ON o.uuid = u.org_id
            WHERE u.id = %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user.get('id'),
        ))
        user_data = await cur.fetchone()

    # Get SMS gateway information
    gateway = None

    if device_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT gate_id FROM %s.devices
                WHERE device_id = %s;
            """, (
                AsIs(user.get("tki")),
                device_id,
            ))
            device = await cur.fetchone()

            if device is not None:
                if device.get("gate_id", None) is not None:
                    await cur.execute("SELECT * FROM %s.sms_gateways WHERE id = %s AND status = 'ACTIVE'", (
                        AsIs(user.get("tki")),
                        device.get("gate_id"),
                    ))
                    gateway = await cur.fetchone()

    if gateway is not None:
        gateway = gateway.get("send_number", None)

    data = dict(
        id=user_data['id'],
        name=user_data['name'],
        account_name=user_data['account_name'],
        email=user_data['email'],
        organization_name=user_data['organization_name'],
        gateway=gateway,
        role=user_data.get("role"),
        location_id=user_data.get("location_id", None)
    )

    return data


async def _sync_labs(data, user=None):
    """
    Sync laboratories that the user has access to
    :param data: {list} The list of laboratories
    :param user: {dict} The calling user
    :return: {list} A list of laboratories to sync down to the phone
    """
    results = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM _iw.laboratories;
        """)
        results = await cur.fetchall()

    return results


async def update_device_info(*args, **kwargs):
    """ Store or update information about a device connected to the system

    Args:
        *args:
        **kwargs:

    Returns:

    """
    cur_device = None
    user = kwargs.get("user")

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.devices
            WHERE device_id = %s
        """, (
            AsIs(user.get("tki")),
            kwargs.get("device_id", None),
        ))
        cur_device = await cur.fetchone()

    if cur_device is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.devices
                SET android_version = %s,
                app_version = %s,
                app_version_name = %s,
                device_lat = %s,
                device_lng = %s,
                device_name = %s,
                device_number = %s,
                last_seen = %s,
                user_id = %s
                WHERE device_id = %s;
            """, (
                AsIs(user.get("tki")),
                kwargs.get("android_version", None),
                kwargs.get("version_code", None),
                kwargs.get("version_name", None),
                kwargs.get("lat", None),
                kwargs.get("lng", None),
                kwargs.get("device", None),
                kwargs.get("phone", None),
                datetime.datetime.now(),
                user.get("id", None),
                kwargs.get("device_id"),
            ))

    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.devices (
                  android_version,
                  app_version,
                  app_version_name,
                  device_id,
                  device_lat,
                  device_lng,
                  device_name,
                  device_number,
                  device_type,
                  last_seen,
                  status,
                  user_id
                  )
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                kwargs.get("android_version", None),
                kwargs.get("version_code", None),
                kwargs.get("version_name", None),
                kwargs.get("device_id", None),
                kwargs.get("lat", None),
                kwargs.get("lng", None),
                kwargs.get("device", None),
                kwargs.get("phone", None),
                "ANDROID",
                datetime.datetime.now(),
                'ACTIVE',
                user.get("id"),
            ))


async def perform_sync(action, data, user=None, device_id=None):
    """
    Perform a syncing action for an android phone V2
    :param action: {str} The syncing action to undertake
    :param data: {dict|list} The data to sync (optional)
    :param user: {dict} The calling user
    :return: Any data which needs to be returned to the phone
    """
    results = None

    if action == "SYNC_REPORTS":
        results = await _sync_reports(data, user=user)
    elif action == "SYNC_ASSIGNMENTS":
        results = await _sync_assignments(data, user=user)
    elif action == "SYNC_FORMS":
        results = await _sync_forms(data, user=user)
    elif action == "SYNC_TAXONOMIES":
        results = []
    elif action == "SYNC_USER":
        results = await _sync_user(data, user=user, device_id=device_id)
    elif action == "SYNC_LABS":
        results = await _sync_labs(data, user=user)
    else:
        results = dict()

    return results
