import json

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

from ewars.core import notifications
from ewars.content import emails

import tornado
import tornado.ioloop


async def get_invitations(user=None):
    """ Retrieve invitatinos for this account
    
    Args:
        user: The calling user

    Returns:

    """
    results = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * 
            FROM _iw.invites
            WHERE aid = %s;
        """, (
            user.get("aid"),
        ))
        results = await cur.fetchall()

    return results


async def resend_invite(invite_id, user=None):
    """ Resend an invite to a user
    
    Args:
        invite_id: The UUID of the invite
        user: The calling user

    Returns:

    """
    invite = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT i.*, a.name AS account_name
            FROM _iw.invites AS i
              LEFT JOIN _iw.accounts AS a ON a.id = i.aid
            WHERE i.uuid = %s;
        """, (
            invite_id,
        ))
        invite = await cur.fetchone()

    cur_loop = tornado.ioloop.IOLoop.current()
    cur_loop.spawn_callback(
        notifications.send,
        'INVITE',
        invite.get('email'),
        dict(
            token=invite.get('uuid'),
            account_name=invite.get('account_name'),
            email=invite.get('email')
        ),
        account=user.get('account_name', None),
        user=user
    )

    return dict(
        err=False
    )


async def get_invite(invite_uuid):
    """ Retrieve an invite form the system

    Args:
        invite_uuid (str): The uuid of the invite to retrieve

    Returns:

    """
    invite = None

    async with get_db_cur() as cur:
        await cur.execute("""
                SELECT i.*,
                    a.tki,
                    a.name AS account_name,
                    a.domain,
                    u.name,
                    u.email AS root_email
                FROM _iw.invites AS i
                  LEFT JOIN _iw.accounts AS a ON a.id = i.aid
                  LEFT JOIN _iw.users AS u ON u.id = i.user_id
                WHERE i.uuid = %s;
            """, (
            invite_uuid,
        ))
        invite = await cur.fetchone()

        if invite is None:
            return None

        if invite['details'].get("location", None) is not None:
            await cur.execute("""
                    SELECT name
                    FROM %s.locations
                    WHERE uuid = %s;
                """, (
                AsIs(invite.get("tki")),
                invite.get("details", {}).get("location", None),
            ))
            invite['location'] = await cur.fetchone()

    return invite


async def invite_user(data, user=None):
    """ INvite a user to EWARS

    Args:
        data (obj): The definition for the invite
        user (obj): The user sending the invite

    Returns:

    """
    acc_user = None

    # Check if the user is already in the system
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.users
            WHERE email = %s;
        """, (
            AsIs(user.get("tki")),
            data.get("email"),
        ))
        acc_user = await cur.fetchone()

    # This user already exists
    if acc_user is not None:
        # User is active, nothing to do
        if acc_user.get("status") == "ACTIVE":
            return dict(
                err=True,
                code="USER_EXISTS_IS_ACTIVE"
            )

        # User is inactive, prompt admin to reinstate from
        # user edtor
        if acc_user.get("status") == "INACTIVE":
            return dict(
                err=True,
                code="USER_EXISTS_IS_INACTIVE"
            )

        # The user exists, but their access was revoked at some point,
        # provide UI to reinstate it
        if acc_user.get("status") == "DELETED":
            return dict(
                err=True,
                code="USER_EXISTS_IS_REVOKED"
            )
    else:
        # See if this user has a root user
        root_user = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM _iw.users
                WHERE email = %s;
            """, (
                data.get("email"),
            ))
            root_user = await cur.fetchone()

        # Check if its an account-controlled user,
        # if it is we can't let them invite i
        if root_user is not None:
            if root_user.get("system", False) == True:
                return dict(
                    err=True,
                    code="USER_IS_SYSTEM"
                )

        # Check if there's a pending invite for this
        # email address already
        pending = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM _iw.invites
                WHERE email = %s
                AND aid = %s;
            """, (
                data.get("email"),
                user.get("aid"),
            ))
            pending = await cur.fetchone()

        if pending is not None:
            return dict(
                err=True,
                code="ACTIVE_INVITE_EXISTS"
            )

        result = None
        user_id = None
        if root_user is not None:
            user_id = root_user.get("id")

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO _iw.invites
                (user_id, email, details, aid)
                VALUES (%s, %s, %s, %s) RETURNING *;
            """, (
                user_id,
                data.get("email"),
                json.dumps(dict(
                    role=data.get("role"),
                    status=data.get("status"),
                    location=data.get("location")
                )),
                user.get("aid"),
            ))
            result = await cur.fetchone()

        # Send a notification to the user
        cur_loop = tornado.ioloop.IOLoop.current()
        cur_loop.spawn_callback(
            notifications.send,
            'INVITE',
            data.get('email'),
            dict(
                token=str(result.get('uuid')),
                account_name=user.get('account_name'),
                email=data.get('email')
            ),
            account=user.get('account_name', None),
            user=user
        )
        return dict(
            err=False
        )
