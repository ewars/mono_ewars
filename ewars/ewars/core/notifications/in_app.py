import datetime
import json

from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

from ewars.utils import template

def send(recipient, subject, content, data, unread=True, user=None):
    """ Add a notification to users inbox

    Args:
        recipient: The recipient of the notification
        subject: THe subject of the notification
        content: THe content of the notification
        data: The data for the notification template
        unread: Whether or not the notification should be inserted as unread

    Returns:

    """

    result = template.render_string(content, data=data)

    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            INSERT INTO %s.messages
            (lineage, recipient, sender, subject, sent, read, state, thread, message_type)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
        """, (
            AsIs(user.get("tki")),
            [],
            recipient.get('id'),
            3,
            subject,
            datetime.datetime.now(),
            not unread,
            'ON',
            json.dumps(dict(content=result)),
            "NOTIFICATION",
        ))

