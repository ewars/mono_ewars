import asyncio

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.conf import settings
from ewars.utils.six import string_types
from ewars.core.notifications import email, in_app
from ewars.constants import EMAIL_SUBJECTS, EMAILS_HTML, EMAILS_PLAIN, SYSTEM_EMAILS

from tornado import gen
from tornado.ioloop import IOLoop

async def send_bulk(notification_type, target_users, data, user=None, account=None, system=False, subject=None):
    if settings.DEBUG:
        await send(notification_type, target_users[0], data, account=account, user=user, subject=subject)
    else:
        for target in target_users:
            await send(notification_type, target, data, account=account, user=user, subject=subject)

    return True


async def send(notification_type, target_user, data, user=None, system=False, account=None, subject=None):
    """ Send a notification within the system

    Args:
        notification_type: The notification type attempting to be sent
        target_user: The user(s) being sent to
        data: Any data to be populated into the template
        user: The thread user

    Returns:

    """

    user_settings = None

    user_id = None
    user_email = None

    if isinstance(target_user, dict):
        user_id = target_user.get("id")
    elif isinstance(target_user, (string_types,)):
        user_email = target_user
    else:
        user_id = target_user

    if subject is None:
        subject = EMAIL_SUBJECTS.get(notification_type, None)
    html = EMAILS_HTML.get(notification_type, None)
    plain = EMAILS_PLAIN.get(notification_type, '')

    user_settings = None
    if user_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT
                    id,
                    email,
                    name,
                    timezone,
                    date_format,
                    time_format,
                    email_format,
                    desk_not_status,
                    email_status,
                    notifications,
                    tki
                FROM %s.users
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                user_id,
            ))
            user_settings = await cur.fetchone()


    if user_settings is None and user_email is not None:
        user_settings = dict(
            id=None,
            email=user_email,
            email_status="DEFAULT",
            name="",
            notifications=dict(),
            email_format="DEFAULT"
        )

    is_blocked = False
    if user_settings.get("email", "").endswith("@ewars.ws") and settings.DEBUG == False:
        if user_settings.get("email") != "support@ewars.ws":
            # We don't send to @ewars.ws users as none of them exist
            is_blocked = True

    if user_settings.get("email_status", "DISABLED") == "DISABLED" and settings.DEBUG == False:
        is_blocked = True

    if account is not None:
        subject = "%s (%s)" % (subject, account)

    # Figure out how to send this notifications
    # Get the notification setting
    setting = user_settings.get('notifications', {}).get(notification_type, 'SYSTEM')

    if setting == "IGNORE":
        # Don't do anything
        pass
    elif setting == "NOTIFY":
        # Create an in-app notification
        in_app.send(user_settings, subject, html, data, unread=False, user=user)
    elif setting == "EMAIL":
        # Check if the user has disabled email
        if is_blocked != True:
            if user_settings.get("email_status", "DEFAULT") != "DISABLED":
                # Check if they prefer plaintext
                if user_settings.get("email_format", "DEFAULT") != "PLAINTEXT":
                    email.send(user_settings, subject, html, data, account=account, format="HTML")
                else:
                    email.send(user_settings, subject, plain, data, account=account, format="PLAINTEXT")

        in_app.send(user_settings, subject, html, data, unread=True, user=user)
    elif setting == "SYSTEM":
        if is_blocked != True:
            email.send(user_settings, subject, html, data, account=account, format="HTML")

    return True


def handle_ses_topic(data):
    """ Handles Amazon SNS confgured topicx for bounced emails and complaints
    disabling the user getting email notifications permanently moving forward.

    Args:
        data:

    Returns:

    """
    not_type = data.get("notificationType", None)

    if not_type == "Bounce":
        bouncers = data['bounce'].get("bouncedRecipients", [])

        for bounced_user in bouncers:
            with get_db_cursor(commit=True) as cur:
                cur.execute("""
                    UPDATE _iw.users
                    SET email_status = 'DISABLED'
                    WHERE email = %s;
                """, (
                    bounced_user.get("emailAddress"),
                ))

    if not_type == "Complaint":
        complaints = data['complaint'].get("complainedRecipients", [])

        for complaint in complaints:
            with get_db_cursor(commit=True) as cur:
                cur.execute("""
                    UPDATE _iw.users
                    SET email_status = 'DISABLED'
                    WHERE email = %s
                """, (
                    complaint.get("emailAddress"),
                ))

    return True
