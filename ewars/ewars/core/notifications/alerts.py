
REOPEN_HTML = """
 <p><strong>{{data.user_name}}</strong> has <strong>re-opened</strong> the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Reason:</strong><br/>
            {{data.reason}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
"""

REOPEN_PLAIN = """
{{data.user_name}} has re-opened the following alert:

Alarm: {{data.alarm_name}}
Location: {{data.location_name}}
Alert Date: {{data.alert_date}}

Reason:
{{data.reason}}

You can view this alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
"""

COMMENT_HTML = """
     <p><strong>{{data.commenter}}</strong> has <strong>commented</strong> on an alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Comment:</strong><br/>
            {{data.content}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
"""

COMMENT_PLAIN = """

"""

DISCARD_HTML = """
<p><strong>{{data.user_name}}</strong> has <strong>discarded</strong> the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Reason:</strong><br/>
            {{data.content}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
"""

DISCARD_PLAIN = """
{{datauser_name}} has discarded the following alert:

Alarm: {{data.alarm_name}}
Location: {{data.location_name}}
Alert Date: {{data.alert_date}}

Reason:
{{data.content}}

You can review the alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
"""

OUTCOME_HTML = """
<p><strong>{{data.user_name}}</strong> has completed <strong>Outcome</strong> for the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Outcome: </strong> {{data.outcome}}<br />
            <strong>Comments:</strong><br/>
            {{data.comments}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
"""

OUTCOME_PLAIN = """
{{data.user_name}} has completed Outcome for the following alert:

Alarm: {{data.alarm_name}}
Location: {{data.location_name}}
Alert Date: {{data.alert_date}}

Outcome: {{data.outcome}}
Comments:
{{data.comments}}

You can review the alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
"""

VERIFICATION_HTML = """
<p><strong>{{data.user_name}}</strong> has completed <Strong>Verification</strong> for the following alert:.</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Triage:</strong><br/>
            {{data.content}}
            <br/>
            <strong>Verification Outcome:</strong> {{data.outcome}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
"""

VERIFICATION_PLAIN = """
{{data.user_name}} has completed verification for the following alert:

Alarm: {{data.alarm_name}}
Location: {{data.location_name}}
Alert Date: {{data.alert_date}}

Triage:
{{data.content}}

Verification Outcome: {{data.outcome}}

You can review the alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
"""

RISK_ASSESS_HTML = """
<p><strong>{{data.user_name}}</strong> has completed <strong>Risk Assessment</strong> on the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Hazard Assessment:</strong><br/>
            {{data.hazard_assessment}}<br/>
            <Strong>Exposure Assessment:</strong><br/>
            {{data.exposure_assessment}}<br/>
            <strong>Context Assessment</strong><br/>
            {{data.context_assessment}}

        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
"""

RISK_ASSESS_PLAIN = """
{{data.user_name}} has completed Risk Assessment on the following alert:

Alarm: {{data.alarm_name}}
Location: {{data.location_name}}
Alert Date: {{data.alert_date}}

Hazard Assessment:
{{data.hazard_assessment}}
Exposure Assessment:
{{data.exposure_assessment}}
Context Assessment
{{data.context_assessment}}

You can review ths alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
"""

RISK_CHAR_HTML = """
<p><strong>{{data.user_name}}</strong> has completed <strong>Risk Characterisation</strong> on the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Risk:</strong> {{data.risk}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
"""

RISK_CHAR_PLAIN = """
{{data.user_name}} has completed Risk Characterisation on the following alert:

Alarm: {{data.alarm_name}}
Location: {{data.location_name}}
Alert Date: {{data.alert_date}}

Risk: {{data.risk}}

You can review this alert here http://ewars.ws/alert#?uuid={{data.alert_uuid}}
"""