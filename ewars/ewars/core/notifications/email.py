import boto.ses
from jinja2 import Environment, PackageLoader
import datetime

from ewars.utils import template

from ewars.conf import settings

env = Environment(loader=PackageLoader('ewars', 'template/emails'))


def send(recipient, subject, content, data, account=None, format="HTML"):
    """ Send an email notification

    Args:
        recipient: The recipient of the email
        subject: The subject of the email
        content: THe content of the email (html|text)
        data: Data to be populated into the email
        format: The format of the email (HTML|PLAINTEXT)

    Returns:

    """

    cur_date = datetime.datetime.utcnow()
    cur_date = cur_date.strftime("%A, %d %B %Y %H:%M%p")

    email_tmpl = ""
    if format == "HTML":
        email_tmpl = template.render_html(content, data=data,
                                          recipient=recipient,
                                          account_name=account,
                                          notification_time=cur_date,
                                          subject=subject)
    elif format == "PLAINTEXT":
        email_tmpl = template.render_plain(content,
                                           data=data,
                                           recipient=recipient,
                                           account_name=account,
                                           notification_time=cur_date,
                                           subject=subject)

    if format == "HTML":
        _send_html(recipient, subject, email_tmpl)
    else:
        _send_plain(recipient, subject, email_tmpl)

    return True


def _send_plain(recip, subject, content):
    """ Send a plaintext email

    Args:
        recip:
        subject:
        content:

    Returns:

    """
    conn = boto.ses.connect_to_region(
        settings.AWS_REGION,
        aws_access_key_id=settings.AWS_ACCESS_KEY,
        aws_secret_access_key=settings.AWS_SECRET_KEY
    )

    if settings.DEBUG:
        print("""
            Sent %s to %s
        """ % (
            subject,
            recip.get('email'),
        ))
        recip['email'] = "support@ewars.ws"
        # return

    try:
        conn.send_email(
            "support@ewars.ws",
            subject,
            None,
            [recip.get("email")],
            format="text",
            reply_addresses=["support@ewars.ws"],
            return_path="support@ewars.ws",
            text_body=content
        )
    except Exception as e:
        # ignore SAX problems on OSX
        print("SAX ERROR")
        pass

    conn.close()


def _send_html(recipient, subject, content):
    """ Send an HTML email

    Args:
        recip: The recipient
        subject:  The subject of the email
        content: The contents of the email

    Returns:

    """
    conn = boto.ses.connect_to_region(
        settings.AWS_REGION,
        aws_access_key_id=settings.AWS_ACCESS_KEY,
        aws_secret_access_key=settings.AWS_SECRET_KEY
    )

    if settings.DEBUG:
        print("""
            Sent %s to %s
        """ % (
            subject,
            recipient.get('email'),
        ))
        recipient['email'] = "support@ewars.ws"
        # return

    try:
        conn.send_email(
            "support@ewars.ws",
            subject,
            None,
            [recipient.get("email")],
            format="html",
            reply_addresses=["support@ewars.ws"],
            return_path="support@ewars.ws",
            html_body=content
        )
    except Exception as e:
        # ignore SAX problems on OSX
        print("SAX ERROR")
        pass

    conn.close()
