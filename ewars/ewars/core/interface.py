import pickle
import json
import datetime
import uuid

from tornado.web import RequestHandler

from base64 import b64decode

from ewars.conf import settings
from ewars.core import clients, account as accounts
from ewars.core import identity
from ewars.models import User
from ewars.db import get_db_cur

from concurrent.futures import ThreadPoolExecutor

from psycopg2.extensions import AsIs

MAX_WORKERS = 10


class BaseRequestHandler(RequestHandler):
    executor = ThreadPoolExecutor()

    SUPPORT_METHODS = ("CONNECT", "HEAD", "POST", "DELETE", "PATCH", "OPTIONS", "PUT")

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers",
                        "x-requested-with,Access-Control-Request-Method,Content-Type,Access-Control-Request-Headers,x-forwarded-for,content-type,accept")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Allow", "POST, GET, OPTIONS")

    def options(self):
        headers = self.request.headers.get('Access-Control-Request-Headers')
        if headers:
            self.set_header('Access-Control-Allow-Headers', headers)

        self.set_header('Access-Control-Allow-Credentials', 'true')

        self.set_status(204)
        self.finish()

    async def prepare(self):
        self._user_accounts = None
        session_token = self.get_secure_cookie('ewars_v4')
        if session_token is None:
            self.current_user = None
        else:
            user = await identity.get_session(session_token.decode('utf-8'))

            if user is not None:
                self.current_user = user.get('data', None)

        if self.current_user is not None:
            if len(self.current_user.get('accounts', [])) > 0:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT id, name, domain, tki
                        FROM _iw.accounts
                        WHERE id = ANY(%s::INT[])
                        AND id != %s;
                    """, (
                        self.current_user.get('accounts', []),
                        self.current_user.get('aid'),
                    ))
                    accounts = await cur.fetchall()
                    self._user_accounts = [dict(x) for x in accounts]

                    if settings.DEBUG:
                        for x in self._user_accounts:
                            x['domain'] = "%s:9000" % (x.get('domain'))



    #
    #
    # async def get_current_user(self):
    #     session_token = self.get_secure_cookie("ewars_v4")
    #     if session_token is None:
    #         return None
    #
    #     user = await identity.get_session(session_token.decode('utf-8'))
    #
    #     result = None
    #     if user is not None:
    #         result = user.get("data", None)
    #
    #     return result

    # @tornado.gen.coroutine
    # def render_client(self, client, public=False):
    #     result = yield self.renderer(client, public=public)
    #     self.render("template.html", **result)

    async def render_client(self, client, public=False):

        if self.current_user is None and public is False:
            self.redirect("/login")
            return "NO_AUTH"
        else:

            # Get the account from host
            account_conf = await identity.get_account_by_host(self.request.host)
            if account_conf is None:
                account_conf = dict(
                    name='Global',
                    id=None,
                    uuid=None,
                    account_flag='DEFAULT'
                )

            ewars_client = clients.get_client("ewars")

            ewars_version = "%s.js.gz" % (ewars_client.get("version"),)
            if settings.DEBUG:
                ewars_version = "dev.js"
            ewars_file = 'ewars-%s' % (ewars_version,)

            client_version = "%s.js.gz" % (client.get("version"),)
            if settings.DEBUG:
                client_version = "dev.js"
            client_file = '%s-%s' % (client.get("token"), client_version)

            # Load clients to populate menu
            if public is False:
                apps = clients.load_clients(self.current_user.get("role"))
            else:
                apps = []

            app_cat = {}
            for app in apps:
                if app.get("enabled", False) \
                        and app.get("category", None) is not None:
                    if "accounts" in app.keys():
                        if self.current_user.get("aid") in app.get("accounts", []):
                            try:
                                app_cat[app.get("category", None)].append(app)
                            except KeyError:
                                app_cat[app.get("category", None)] = [app]
                    else:
                        try:
                            app_cat[app.get("category", None)].append(app)
                        except KeyError:
                            app_cat[app.get("category", None)] = [app]

            configs = []
            if client.get("config", None) is not None and self.current_user is not None:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT key, value FROM %s.conf
                        WHERE grp IN %s;
                    """, (
                        AsIs(self.current_user.get("tki")),
                        tuple(client.get("config")),
                    ))
                    configs = await cur.fetchall()

            if client.get('config', None) is not None and self.current_user is None:
                # Need to figure out account from domain
                host_name = self.request.host_name
                if settings.DEBUG and host_name in ("localhost", "127.0.0.1"):
                    configs = [
                        dict(key="APP_NAME", value="EWARS")
                    ]
                else:
                    if account_conf is not None:
                        if account_conf.get('name') != 'Global':
                            async with get_db_cur() as cur:
                                await cur.execute("""
                                SELECT key, value FROM %s.conf
                                WHERE grp IN %s;
                            """, (
                                    AsIs(account_conf.get("tki")),
                                    tuple(client.get("config")),
                                ))
                                configs = await cur.fetchall()

                pass

            configs = dict((x.get("key"), x.get("value")) for x in configs)

            theme = "flex"
            if client.get("route").startswith("/admin"):
                theme = "admin"

            unactioned_tasks = 0
            inbox_count = 0
            if not public and self.current_user.get("user_type", None) != "SUPER_ADMIN":
                unactioned_tasks = await User.get_unactioned_tasks(
                    self.current_user.get('id'),
                    user=self.current_user
                )
                inbox_count = unactioned_tasks
            else:
                inbox_count = None

            for key, value in account_conf.items():
                if isinstance(value, datetime.datetime):
                    account_conf[key] = value.strftime('%Y-%m-%d')

                if isinstance(value, uuid.UUID):
                    account_conf[key] = str(value)

            if self.current_user is not None:
                for key, value in self.current_user.items():
                    if isinstance(value, datetime.datetime):
                        self.current_user[key] = value.strftime('%Y-%m-%d')

                    if isinstance(value, uuid.UUID):
                        self.current_user[key] = str(value)

            return dict(
                debug=settings.DEBUG,
                theme=theme,
                account=account_conf,
                accounts=self._user_accounts,
                inst=dict(
                    name="EWARS"
                ),
                instance=dict(
                    name="EWARS"
                ),
                invitation=None,
                configs=configs,
                version=settings.VERSION,
                user=self.current_user,
                inbox_count=0,
                task_count=unactioned_tasks,
                auth=self.current_user,
                ewars_client=ewars_client,
                ewars_file=ewars_file,
                client=client,
                app_cat=app_cat,
                client_file=client_file)
