import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


async def query_collection(form_id, location_id, start_date, end_date, offset, user=None):
    results = []

    if start_date is None:
        start_date = "1990-01-01"

    if end_date is None:
        end_date = datetime.date.today()

    if offset is None:
        offset = 0

    location_filter = ""
    if user.get("role") == "REGIONAL_ADMIN":
        location_filter = "AND l.lineage::TEXT[] @> '{%s}'::TEXT[]" % (user.get("location_id"),)
    if location_id is not None:
        location_filter = "AND c.location_id = '%s'" % (location_id,)

    start_filter = ""
    if start_date is not None:
        start_filter = "AND c.data_date >= '%s'" % (start_date,)

    end_filter = ""
    if end_filter is not None:
        end_filter = "AND c.data_date <= '%s'" % (end_date,)

    if user.get("role") == "ACCOUNT_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT c.uuid,
                    c.form_id,
                    c.form_version_id,
                    c.data_date,
                    c.location_id,
                    c.data,
                    c.submitted,
                    c.status,
                    u.name AS user_name,
                    u.email AS user_email,
                    c.submitted_date,
                    l.name AS location_name
                FROM %s.collections AS c
                  LEFT JOIN %s.users AS u ON u.id = c.created_by 
                  LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                WHERE c.form_id = %s
                  %s
                  %s
                  AND c.status = 'SUBMITTED'
                  %s
                ORDER BY c.submitted DESC
                LIMIT 20
                OFFSET %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
                AsIs(start_filter),
                AsIs(end_filter),
                AsIs(location_filter),
                offset,
            ))
            results = await cur.fetchall()
    else:  # Regional administrator collection query, filtered by locations under their purview
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT c.uuid,
                    c.form_id,
                    c.form_version_id,
                    c.data_date,
                    c.location_id,
                    c.data,
                    c.submitted,
                    c.status,
                    u.name AS user_name,
                    u.email AS user_email,
                    c.submitted_date,
                    l.name AS location_name
                FROM %s.collections AS c
                  LEFT JOIN %s.users AS u ON u.id = c.created_by 
                  LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                WHERE c.form_id = %s
                  %s 
                  %s 
                  AND c.status = 'SUBMITTED'
                  %s
                ORDER BY c.submitted DESC
                LIMIT 20
                OFFSET %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
                AsIs(start_filter),
                AsIs(end_filter),
                AsIs(location_filter),
                offset,
            ))
            results = await cur.fetchall()

    return results
