import json

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

async def recv_geom_updates(data, user=None):
    async with get_db_cur() as cur:
        for key, value in data.items():
            new_geom = dict(
                type="Point",
                coordinates=[
                    float(value.get("longitude")),
                    float(value.get("latitude"))
                ]
            )

            await cur.execute("""
                UPDATE %s.locations
                SET geojson = %s 
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                json.dumps(new_geom),
                key,
            ))

    return dict(
        success=True
    )



async def query_locations(parent_id, user=None):
    results = []

    if user.get("role") == "REGIONAL_ADMIN":
        if parent_id is None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, name, site_type_id
                    FROM %s.locations 
                    WHERE uuid = %s;
                """, (
                    AsIs(user.get("tki")),
                    user.get("location_id"),
                ))
                results = [await cur.fetchone()]

            return results

    async with get_db_cur() as cur:
        if parent_id is not None:
            await cur.execute("""
                SELECT uuid, name, site_type_id
                FROM %s.locations 
                WHERE parent_id = %s 
                AND status = 'ACTIVE'
                ORDER BY name->>'en' ASC;
            """, (
                AsIs(user.get("tki")),
                parent_id,
            ))
            results = await cur.fetchall()
        else:
            await cur.execute("""
                SELECT uuid, name, site_type_id
                FROM %s.locations
                WHERE parent_id IS NULL 
                AND status = 'ACTIVE'
                ORDER BY name->>'en' ASC;
            """, (
                AsIs(user.get("tki")),
            ))
            results = await cur.fetchall()

        for result in results:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.locations
                WHERE parent_id = %s
                AND status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                result.get("uuid"),
            ))
            res = await cur.fetchone()
            result['children'] = res.get('total')

    return results


async def get_location(uuid, user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid, name, site_type_id
            FROM %s.locations
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            uuid,
        ))
        result = await cur.fetchone()

    return result


async def search_location(text, user=None):
    result = None

    return result


async def update_geom(data, user=None):
    print(data)
    result = None
    # {"type": "Point", "coordinates": [99.95973446210974, 51.518431853333254]}
    new_geom = dict(
        type="Point",
        coordinates=[
            data.get("longitude"),
            data.get("latitude")
        ]
    )

    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE %s.locations
            SET geojson = %s 
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            json.dumps(new_geom),
            data.get("uuid"),
        ))

    return result
