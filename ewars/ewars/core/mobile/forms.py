from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.models import Submission


async def submit_form(form_id, location_id, data_date, data, user=None):
    form = None
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT version_id, features
            FROM %s.forms 
            WHERE id = %s;
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = await cur.fetchone()

    has_interval = True if "LOCATION_REPORTING" in form['features'].keys() else False
    has_location = True if "INTERVAL_REPORTING" in form['features'].keys() else False

    # Check that it has a report date
    if has_interval:
        if data_date in (None, ""):
            return dict(
                err=True,
                code="DATA_DATE_MISSING"
            )

    # Check that it has a location
    if has_location:
        if location_id in (None, ""):
            return dict(
                err=True,
                code="LOCATION_MISSING"
            )

    if has_interval and has_location:
        # Check for duplicates
        exists = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS t
                FROM %s.collections 
                WHERE form_id = %s 
                  AND data_date = %s 
                  AND status = 'SUBMITTED'
                  AND location_id = %s;
            """, (
                AsIs(user.get("tki")),
                form_id,
                data_date,
                location_id,
            ))
            res = await cur.fetchone()
            exists = res.get('t')

        if exists > 0:
            return dict(
                err=True,
                code="DUPLICATE_REPORT"
            )

    actual_data = data
    if data.get('data', None) is not None \
            and data.get('data_date', None) is not None \
            and data.get('location_id', None) is not None:
        actual_data = data.get('data')

    submission = dict(
        data=actual_data,
        location_id=location_id,
        data_date=data_date,
        created_by=user.get("id")
    )

    result = await Submission.submit(
        form_id,
        form.get("version_id"),
        submission,
        source="ANDROID",
        user=user
    )

    return result


async def get_form(form_id, user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT 
                f.id,
                f.name,
                f.features,
                fv.definition,
                f.version_id
            FROM %s.forms AS f 
              LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
            WHERE f.id = %s;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
        ))
        result = await cur.fetchone()

    return result


async def get_form_version(version_id, user=None):
    result = None

    return result


async def get_forms(user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT 
              f.id, 
              f.name, 
              f.features,
              f.version_id,
              fv.definition
            FROM %s.forms AS f 
              LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
            WHERE f.status = 'ACTIVE'
            ORDER BY f.name->>'en' ASC;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchall()

    return result
