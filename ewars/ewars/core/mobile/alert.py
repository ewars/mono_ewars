import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


async def get_report(report_uuid, user=None):
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT 
                c.*,
                f.name AS form_name,
                f.id AS form_id,
                f.features,
                v.definition AS form_definition,
                l.name AS location_name,
                u.name AS user_name,
                u.email AS user_email
            FROM %s.collections AS c 
              LEFT JOIN %s.forms AS f ON f.id = c.form_id
              LEFT JOIN %s.form_versions AS v ON v.uuid = c.form_version_id
              LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
              LEFT JOIN %s.users AS u ON u.id = c.created_by
            WHERE c.uuid = %s;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            report_uuid,
        ))
        result = await cur.fetchone()

    if result is None:
        return dict(
            err=True,
            code="NO_REPORT"
        )

    return result


async def get_activity(alert_id, offset, user=None):
    """ Get activity for an alert
    
    Args:
        alert_id: 
        offset: 
        user: 

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT e.*, u.name, u.email
            FROM %s.alert_events AS e 
              LEFT JOIN %s.users AS u ON u.id = e.user_id
            WHERE e.alert_id = %s 
            ORDER BY action_date DESC 
            LIMIT 15
            OFFSET %s;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alert_id,
            offset,
        ))
        result = await cur.fetchall()

    return result


async def get_stage(alert_id, stage, user=None):
    result = None

    return result


async def get_users(alert_id, user=None):
    """ Get users belonging to an alert
    
    Args:
        alert_id: 
        user: 

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT
                u.id,
                u.name,
                u.email,
                u.org_id,
                u.role
            FROM %s.users AS u 
              LEFT JOIN %s.alert_users AS au ON au.user_id = u.id
            WHERE au.alert_id = %s 
              AND u.status = 'ACTIVE'
            ORDER BY u.name;
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alert_id,
        ))
        result = await cur.fetchall()

    return result

async def add_comment(alert_id, data, user=None):
    alert = None
    async with get_db_cur() as cur:
        await cur.execute("""
                INSERT INTO %s.alert_events (action_date, action_type, icon, node_type, user_id, content, alert_id)
                VALUES (%s, %s, %s, %s, %s, %s, %s);
            """, (
            AsIs(user.get("tki")),
            datetime.datetime.now(),
            'COMMENT',
            'fa-comment',
            'COMMENT',
            data.get('uid'),
            data.get("comment"),
            alert_id,
        ))

    return True

