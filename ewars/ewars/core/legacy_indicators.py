from ewars.constants import CONSTANTS

ROOT_DEFAULTS = [
    dict(id=None, status="ACTIVE", name=dict(en="System [legacy]"), context=CONSTANTS.SYSTEM, itype=CONSTANTS.SYSTEM)
]

SYSTEM_FOLDERS = [
    dict(id=None, status="ACTIVE", name=dict(en="Alarms"), context=CONSTANTS.ALARMS, itype=CONSTANTS.SYSTEM),
    dict(id=None, status="ACTIVE", name=dict(en="Forms"), context=CONSTANTS.FORMS, itype=CONSTANTS.SYSTEM),
    dict(id=None, status="ACTIVE", name=dict(en="Metrics"), context=CONSTANTS.METRICS, itype=CONSTANTS.SYSTEM),
    dict(id=None, status="ACTIVE", name=dict(en="Organizations"), context=CONSTANTS.ORGANIZATIONS, itype=CONSTANTS.SYSTEM),
]

METRICS_FOLDERS = [
    dict(id=None, status="ACTIVE", name=dict(en="Reporting"), context=CONSTANTS.REPORTING_METRICS, itype=CONSTANTS.SYSTEM),
    dict(id=None, status="ACTIVE", name=dict(en="Users"), context=CONSTANTS.USERS_METRICS, itype=CONSTANTS.SYSTEM),
    dict(id=None, status='ACTIVE', name=dict(en="Alerts"), context=CONSTANTS.ALERT_METRICS, itype=CONSTANTS.SYSTEM),
]

REPORTING_INDICATORS = [
    dict(id=None, status="ACTIVE", name=dict(en="Completeness Overall"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM,
         definition=dict(type="COMPLETENESS")),
    dict(id=None, status="ACTIVE", name=dict(en="Timeliness Overall"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM, definition=dict(type="TIMELINESS")),
    dict(id=None, status="ACTIVE", name=dict(en="Report Submissions Overall"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM, definition=dict(type="SUBMISSIONS"))
]

USERS_INDICATORS = [
    dict(id=None, status="ACTIVE", name=dict(en="Organizations Total"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM),
    dict(id=None, status="ACTIVE", name=dict(en="User"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM, definition=dict(type="USERS")),
    dict(id=None, status="ACTIVE", name=dict(en="Registrations"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM, definition=dict(type="REGISTRATIONS"))
]

FORM_INDICATORS = [

]

OVERALL_ALERT_METRICS = [
    dict(id=None, status='ACTIVE', name=dict(en="Alerts Raised (All)"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM, definition=dict(type=CONSTANTS.ALERTS_TRIGGERED)),
    dict(id=None, status='ACTIVE', name=dict(en="Active Alerts (All)"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM, definition=dict(type=CONSTANTS.ALERTS_ACTIVE)),
    dict(id=None, status='ACTIVE', name=dict(en="Inactive Alerts (All)"), context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM, definition=dict(type=CONSTANTS.ALERTS_INACTIVE))
]
