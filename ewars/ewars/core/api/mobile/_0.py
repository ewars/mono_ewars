from ewars.core.api._base import BaseAPI

from ewars.core.mobile import metrics, alerts, forms, alert, collections, locations, \
    tasks, user, device
from ewars.models import Alert

async def recv_ping(user=None):
    return dict(
        pong="OK"
    )



class API(BaseAPI):
    """ Base API for mobile, v0 """
    _methods = {

        # General API Items
        "com.ewars.metrics": metrics.get_metrics,

        ##########
        # Alerts #
        ##########
        "com.ewars.alert": alerts.get_alert,
        "com.ewars.alerts.open": alerts.get_open_alerts,
        "com.ewars.alerts.monitored": alerts.get_monitored_alerts,
        "com.ewars.alert.stage": Alert.get_alert_action,
        "com.ewars.alert.activity": alert.get_activity,
        "com.ewars.alert.action": Alert.submit_action,
        "com.ewars.alert.report": alert.get_report,
        "com.ewars.alert.users": alert.get_users,
        "com.ewars.alert.comment": alert.add_comment,

        "com.ewars.collections": collections.query_collection,
        # "com.ewars.request_assign": tasks.request_assign,

        #########
        # Forms #
        #########

        "com.ewars.form": forms.get_form,
        "com.ewars.forms": forms.get_forms,
        "com.ewars.form.version": forms.get_form_version,
        "com.ewars.form.submit": forms.submit_form,

        #############
        # Locations #
        #############

        "com.ewars.locations": locations.query_locations,
        "com.ewars.location": locations.get_location,

        #########
        # Tasks #
        #########

        "com.ewars.tasks": tasks.get_tasks,
        "com.ewars.task.action": tasks.action_task,


        "com.ewars.assignments": user._get_assignments,
        "com.ewars.user": user.get_user,
        "com.ewars.location.update_geom": locations.update_geom,
        "com.ewars.recv_geom_updates": locations.recv_geom_updates,

        # This is for post-updgrade to retrieve data for the user
        "com.ewars.upgrade": None,

        "com.ewars.device": device.update_device,

        "com.ewars.sync": device.sync_device,

        "com.ewars.password.update": user.update_password,

        "com.ewars.ping": recv_ping
    }
