#from .desktop._0 import API as _desktop_0
from .mobile._0 import API as _mobile_0
from .web._0 import API as _web_0

_DESKTOP_VERS = {
}

_MOBILE_VERS = {
    "0": _mobile_0
}

_WEB_VERS = {
    "0": _web_0
}

_CLI_VERS = {
    "0": None
}

_CONTEXTS = {
    "DESKTOP": _DESKTOP_VERS,
    "MOBILE": _MOBILE_VERS,
    "CLI": _CLI_VERS,
    "WEB": _WEB_VERS
}


def get_api(context, version):
    """ Instantiate an API object to represent the request

    Args:
        version: The version of the API wanted

    Returns:
        A class representing the API as static methods and a router
    """
    _api = None
    VERS = _CONTEXTS.get(context)
    if version in VERS.keys():
        _api = VERS.get(version)

    if _api is not None:
        return _api()

    return None
