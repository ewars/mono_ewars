from ewars.core.api._base import BaseAPI

# from ewars.core.identity import user as user_fn
# from ewars.core import metrics
# from ewars.core.notifications.activity import management as act_api
# from ewars.core.querying import query_resource, get_resource
# from ewars.core.identity import assignment, registration, invitations
# from ewars.conf import management as conf_actions
# from ewars.core import inbox, tasks, export, indicators, alerts, forms, locations, \
#     dashboards, templates, plots, mapping, devices, articles, sites, notebooks, \
#     documents, importer, workflows, huds, datasets
# from ewars.core.identity import account, organization
# from ewars.core.analysis import auditing
# from ewars.core import admin
# from ewars.core import plot

from ewars.core import registration, querying, invitations, metrics, identity, export, \
    importer, tasks
from ewars.conf import management as conf
from ewars.analysis import auditing
from ewars import plot
from ewars.core import location_merge

from ewars.models import User, Form, Location, LocationType, ReportingPeriod, Template, \
    Notebook, Plot, Alarm, Map, Device, Submission, Dashboard, Alert, Role, \
    Organization, Assignment, Notification, Indicator, IndicatorGroup, \
    Document, Draft, Differential, Task, Account, Activity, DocumentTemplate, Apec, Outbreak


class API(BaseAPI):
    _methods = {

        # System
        'com.ewars.grant': User.grant_access,
        'com.ewars.activity': Activity.get_activity_stream,

        # Metrics
        "com.ewars.metrics.overall": metrics.get_overall_metrics,
        "com.ewars.metric": metrics.get_metric,

        # Querying
        "com.ewars.query": querying.query_resource,
        "com.ewars.resource": querying.get_resource,

        # Analysis
        "com.ewars.plot": plot.plot,

        ############
        # Analysis #
        ############

        "com.ewars.performance": auditing.performance,

        #########
        # Forms #
        #########
        'com.ewars.forms': Form.query,
        "com.ewars.form": Form.get_form,
        "com.ewars.forms.active": Form.get_active_forms,
        "com.ewars.forms.accessible": Form.accessible_forms,
        "com.ewars.form.definition": Form.get_definition,
        "com.ewars.form.create": Form.create,
        "com.ewars.form.update": Form.update,
        "com.ewars.form.delete": Form.delete,
        "com.ewars.form.duplicate": Form.duplicate,
        "com.ewars.form.current": Form.get_current_definition,
        "com.ewars.form.export": Form.export,
        "com.ewars.form.import": Form.import_form,
        "com.ewars.form.fields": Form.get_fields,
        "com.ewars.form.dimensions": Form.get_dimensions,
        "com.ewars.form.measures": Form.get_measures,
        "com.ewars.form.field": Form.get_field,

        ########
        # User #
        ########
        "com.ewars.user.get": User.get_self,
        "com.ewars.user.profile": User.get_profile,
        'com.ewars.user.profile.activity': User.get_profile_activity,
        "com.ewars.user.patch": User.update_profile,
        "com.ewars.user.assignments": User.assignments,
        "com.ewars.user.dashboards": User.get_dashboards,
        "com.ewars.user.dashboard": Dashboard.get,
        "com.ewars.user.documents": User.get_documents,
        "com.ewars.user.tasks": User.get_tasks,
        "com.ewars.user.notifications": None,
        "com.ewars.user.missing": None,
        "com.ewars.user.upcoming": User.get_upcoming_submissions,
        "com.ewars.user.overdue": User.get_overdue_submissions,
        "com.ewars.user.alerts": None,
        "com.ewars.user.revoke": User.revoke_access,
        "com.ewars.user.reinstate": User.reinstate_access,
        "com.ewars.user.register": None,
        "com.ewars.user.update": User.update,
        "com.ewars.user.create": User.create_system,
        "com.ewars.user.activity": User.get_activity,
        "com.ewars.user.password.update": User.update_password,
        "com.ewars.user.grant": User.grant_access,

        "com.ewars.user.create.check_email": identity.check_email,

        "com.ewars.assignments.user": User.get_assignments,
        "com.ewars.assignment.create": User.add_assignment,
        "com.ewars.assignment.update": Assignment.update,
        "com.ewars.assignment.delete": Assignment.delete,

        "com.ewars.user.assignment.request": Assignment.request,

        ##############
        # Dashboards #
        ##############
        "com.ewars.dashboard.create": Dashboard.create,
        "com.ewars.dashboard.update": Dashboard.update,
        "com.ewars.dashboard.delete": Dashboard.delete,

        #############
        # Locations #
        #############
        "com.ewars.location": Location.get_by_id,
        "com.ewars.location.update": Location.update,
        "com.ewars.location.create": Location.create,
        "com.ewars.location.delete": Location.delete,
        "com.ewars.location.merge": location_merge.merge_locations,
        "com.ewars.location.merge.preview": location_merge.get_merge_preview,
        "com.ewars.location.tree": location_merge.get_location_tree,
        "com.ewars.location.children.enable": Location.enable_children,
        "com.ewars.location.children.disable": Location.disable_children,
        "com.ewars.location.profile": Location.get_profile,
        "com.ewars.location.profile.assignments": Location.get_assignments,
        "com.ewars.location.profile.periods": Location.get_reporting_periods,
        "com.ewars.location.profile.users": Location.get_users,
        'com.ewars.location.geometry': Location.get_geometry,
        "com.ewars.location.assignments": Location.get_assignments,

        "com.ewars.locations.types": LocationType.get_all,
        "com.ewars.locations.type.delete": LocationType.delete,
        "com.ewars.locations.type.create": LocationType.create,
        "com.ewars.locations.type.update": LocationType.update,
        'com.ewars.locations.geometry': Location.get_geoms_for_spec,
	"com.ewars.locations.search": Location.search_locations,

        "com.ewars.location.groups": Location.get_groups,

        ##############################
        # Location Reporting Periods #
        ##############################
        "com.ewars.location.periods": Location.get_reporting_periods,
        'com.ewars.location.period.create': ReportingPeriod.add_reporting_period,
        "com.ewars.location.period.update": ReportingPeriod.update_reporting_period,
        "com.ewars.location.period.delete": ReportingPeriod.delete_reporting_period,

        #############
        # Templates #
        #############
        'com.ewars.template.create': Template.create,
        'com.ewars.template.update': Template.update,
        'com.ewars.template.delete': Template.delete,
        'com.ewars.template.duplicate': Template.duplicate,
        'com.ewars.template.cache.clear': Template.clear_cache,

        #############
        # Documents #
        #############
        "com.ewars.documents": None,
        "com.ewars.document": None,
        "com.ewars.document.share": Document.share,
        "com.ewars.document.pdf": None,
        "com.ewars.document.comment": None,
        "com.ewars.document.data": Document.export_data,

        #########
        # Tasks #
        #########
        "com.ewars.tasks": User.get_tasks,
        'com.ewars.task.action': tasks.action_task,

        'com.ewars.registration.approve': None,
        'com.ewars.registration.reject': None,
        'com.ewars.assignment.approve': None,
        'com.ewars.assignment.reject': None,
        'com.ewars.amendment.approve': None,
        'com.ewars.amendment.reject': None,
        'com.ewars.retraction.approve': None,
        'com.ewars.retractions.reject': None,

        #################
        # Notifications #
        #################
        "com.ewars.notifications": User.get_messages,
        "com.ewars.notification.get": Notification.get,
        "com.ewars.notification.delete": Notification.delete,
        "com.ewars.notification.read": Notification.set_read,
        "com.ewars.notifications.clear": User.clear_notifications,
        "com.ewars.notification.reply": None,

        ##############
        # Indicators #
        ##############
        "com.ewars.indicators": Indicator.query,
        "com.sonoma.indicators": Indicator.get_all_inds,
        "com.ewars.indicator.create": Indicator.create,
        "com.ewars.indicator.delete": Indicator.delete,
        "com.ewars.indicator.update": Indicator.update,
        "com.ewars.indicator.inbound": Indicator.inbound,
        "com.ewars.indicator_group.create": IndicatorGroup.create,
        "com.ewars.indicator_group.update": IndicatorGroup.update,
        "com.ewars.indicator_group.delete": IndicatorGroup.delete,

        #########
        # Plots #
        #########
        "com.ewars.plots": Plot.query,
        "com.ewars.plot.create": Plot.create,
        "com.ewars.plot.update": Plot.update,
        "com.ewars.plot.delete": Plot.delete,
        "com.ewars.plot.share": None,
        "com.ewars.plot.pdf": None,
        "com.ewars.plot.comment": None,

        #############
        # Notebooks #
        #############
        "com.ewars.notebooks": User.get_notebooks,
        "com.ewars.notebooks.shared": Notebook.shared,
        "com.ewars.notebook.create": Notebook.create,
        "com.ewars.notebook.update": Notebook.update,
        "com.ewars.notebook.delete": Notebook.delete,
        'com.ewars.notebooks': Notebook.get,
        "com.ewars.notebook.pdf": None,
        "com.ewars.notebook.share": None,
        "com.ewars.notebook.comment": None,

        ########
        # Maps #
        ########
        "com.ewars.maps": Map.get,
        "com.ewars.maps.shared": Map.get_shared,
        "com.ewars.map.create": Map.create,
        "com.ewars.map.update": Map.update,
        "com.ewars.map.delete": Map.delete,
        "com.ewars.map.share": None,
        "com.ewars.map.pdf": None,
        "com.ewars.map.comment": None,

        "com.ewars.mapping.layer_options": Map.get_map_base_options,
        "com.ewars.mapping.form_points": Map.get_form_points,
        "com.ewars.mapping.loc_geoms": Map.get_geometries,
        "com.ewars.mapping.create": Map.create_map,
        "com.ewars.mapping.update": Map.update_map,
        "com.ewars.mapping.delete": Map.delete_map,
        "com.ewars.mapping.query": Map.get_maps,

        ###########
        # Devices #
        ###########
        "com.ewars.device.create": None,
        "com.ewars.device.delete": Device.delete,
        "com.ewars.device.update": Device.update,
        "com.ewars.device.block": None,
        "com.ewars.device.sms.send": None,
        "com.ewars.device.wap.send": None,

        ##########
        # Export #
        ##########
        "com.ewars.export": export.export_data,

        ##########
        # Alarms #
        ##########
        "com.sonoma.alarms": Alarm.get_all_alarms,
        'com.ewars.alarms': Alarm.get_alarms,
        "com.ewars.alarm.create": Alarm.create,
        "com.ewars.alarm.update": Alarm.update,
        "com.ewars.alarm.delete": Alarm.delete,
        "com.ewars.alarm.inbound": None,
        "com.ewars.alarm.evaluate_historic": Alarm.evaluate_historic,
        "com.ewars.alarm.evaluate_historic_silent": Alarm.evaluate_historic_silent,

        ##########
        # Alerts #
        ##########
        "com.ewars.alerts.query": Alert.retrieve_alerts,
        "com.ewars.alerts.query.legacy": Alert.query,
        "com.ewars.alert": Alert.get,
        "com.ewars.alert.activity": Alert.get_activity,
        "com.ewars.alert.collections": None,
        "com.ewars.alert.comment": Alert.comment,
        "com.ewars.alert.related": Alert.get_related,
        "com.ewars.alert.users": Alert.get_stakeholders,
        "com.ewars.alert.reopen": Alert.reopen,
        "com.ewars.alert.reevaluate": Alert.reevaluate,
        "com.ewars.alert.action.get": Alert.get_action,
        "com.ewars.alert.action.update": Alert.update_action,
        "com.ewars.alert.action.submit": Alert.submit_action,
        "com.ewars.alert.records": Alert.get_records,

        ###############
        # Collections #
        ###############
        "com.ewars.collections.get": Submission.query,
        "com.ewars.collections.recent": Form.recent_submissions,
        "com.ewars.collection.evaluate": Submission.re_evaluate,
        "com.ewars.collection.get": None,
        "com.ewars.collection.amend": Submission.amend_report,
        "com.ewars.collection.retract": Submission.retract,
        "com.ewars.collection.update": Submission.update,
        "com.ewars.collection.submit": Submission.submit,
        "com.ewars.collection.draft.update": Draft.create,
        "com.ewars.collection.draft.create": Draft.create,
        'com.ewars.collection.draft.delete': Draft.delete,

        # Collection Relations/Profiling
        "com.ewars.collection.history": Submission.get_submission_history,
        "com.ewars.collection.alerts": Submission.get_related_alerts,
        "com.ewars.collection.amendments": Submission.get_amendments,
        "com.ewars.collection.comments": Submission.comments,
        "com.ewars.collection.comment": Submission.comment,
        "com.ewars.collection.differential": Differential.get_differential,

        #################
        # Organizations #
        #################
        "com.ewars.organization.create": Organization.create_organization,
        "com.ewars.organization.update": Organization.update_organization,
        "com.ewars.organization.delete": Organization.delete_organization,
        "com.ewars.organization.remove": Organization.remove,
        "com.ewars.organization.add": Organization.add_organization,

        ########
        # Conf #
        ########
        "com.ewars.conf": conf.get_settings,
        "com.ewars.conf.update": conf.update_settings,

        ################
        # Options sets #
        ################
        "com.ewars.options": Form.get_fields_as_options,

        #################
        # Collaboration #
        #################

        "com.ewars.profile": User.get_profile,
        "com.ewars.profile.activity": User.get_user_activity,
        "com.ewars.profile.assignments": User.get_assignments,

        "com.ewars.account.dashboards.update": Account.update_dashboards,

        ##########
        # Import #
        ##########

        "com.ewars.import.create": importer.create_project,
        "com.ewars.import.status": importer.check_progress,
        "com.ewars.import.update": importer.update_project,
        "com.ewars.import.delete": importer.delete_project,
        "com.ewars.import.validate": importer.validate_project,
        "com.ewars.import.update_prop": importer.update_prop,
        "com.ewars.import.run": importer.run_import,
        "com.ewars.import.delete_row": importer.remove_row,
        'com.ewars.import.importables': importer.get_importables,
        'com.ewars.import.chunks': importer.get_chunks,

        ###################
        # Admin Functions #
        ###################

        "com.ewars.admin.account.create": Account.create,
        # "com.ewars.admin.account.update": Account.update,
        # "com.ewars.admin.account.delete": Account.delete,

        ###########
        # Invites #
        ###########
        "com.ewars.invite": invitations.invite_user,
        "com.ewars.invites": invitations.get_invitations,
        "com.ewars.invite.resend": invitations.resend_invite,
        "com.ewars.invite.rescind": None,

        #########
        # Roles #
        #########
        "com.ewars.roles": None,
        "com.ewars.role.create": None,
        "com.ewars.role.update": None,
        "com.ewars.role.delete": None,
        "com.ewars.role": None,
        "com.ewars.system.permissions": None,

        ################
        # Task actions #
        ################
        'com.ewars.user.registration.approve': None,
        'com.ewars.user.registration.reject': None,
        'com.ewars.user.assignment.approve': None,
        'com.ewars.user.assignment.reject': None,
        'com.ewars.submission.amendment.approve': None,
        'com.ewars.submission.amendment.reject': None,
        'com.ewars.submission.retraction.approve': None,
        'com.ewars.submission.retraction.reject': None,
        'com.ewars.location.request.approve': None,
        'com.ewars.location.request.reject': None,
        'com.ewars.organization.request.approve': None,
        'com.ewars.organization.request.reject': None,

        ##########
        # Tables #
        ##########
        'com.ewars.table.locations': Location.get_table_locations,

        'com.ewars.admin.documents': Document.query,

        ###########
        # Account #
        ###########
        'com.ewars.account.usage': Account.usage,

        'com.ewars.v5.document': DocumentTemplate.get_template,
        'com.ewars.v5.documents': DocumentTemplate.query_templates,
        'com.ewars.v5.document.create': DocumentTemplate.create_template,
        'com.ewars.v5.document.delete': DocumentTemplate.delete_template,
        'com.ewars.v5.document.update': DocumentTemplate.update_template,

        'com.sonoma.apec.specification': Apec.get_metrics,

        #############
        # Outbreaks #
        #############
        "com.sonoma.outbreak": Outbreak.get_by_id,
        "com.sonoma.outbreaks.active": Outbreak.get_active_outbreaks,
        "com.sonoma.outbreak.create": Outbreak.create,
        "com.sonoma.outbreak.delete": Outbreak.delete,
        "com.sonoma.outbreak.update": Outbreak.update

    }
