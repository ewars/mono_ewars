import datetime
import json
import uuid

from ewars.core.serializers.json_encoder import JSONEncoder

import tornado

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from tornado import ioloop, gen


@gen.coroutine
def _submit_activity(report, form, user=None, token=None):
    """ Create an activity feed item for the report submission

    Args:
        report:
        form:
        user:
        token:

    Returns:

    """
    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            INSERT INTO %s.activity_feed
            (event_type, triggered_by, created, icon, attachments)
            VALUES (%s, %s, %s, %s, %s);
        """, (
            AsIs(user.get("tki")),
            "REPORT_SUBMISSION",
            user.get("id"),
            datetime.datetime.now(),
            "fa-clipboard",
            json.dumps([["REPORT", report.get("uuid")]])
        ))


