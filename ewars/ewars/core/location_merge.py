import json
import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


def get_location_types(user=None):
    """ Retrieve a list of location types for this account

    Args:
        user: The calling user

    Returns:
        A list of location types
    """
    results = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.location_types;
        """, (
            AsIs(user.get("tki")),
        ))
        results = cur.fetchall()

    return results



async def get_location_tree(location_id, user=None):
    """ Get a nested tree of a location

    Args:
        location_id:

    Returns:

    """
    locations = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid::TEXT, name, lineage::TEXT[], parent_id::TEXT FROM %s.locations
            WHERE lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            [location_id],
        ))
        locations = await cur.fetchall()

    if len(locations) > 1:
        pre_dict = dict((x.get("uuid"), x) for x in locations)

        for loc_uuid, loc in pre_dict.items():
            if loc.get("parent_id", None) is not None:
                if loc.get("parent_id") in pre_dict.keys():
                    try:
                        pre_dict[loc.get("parent_id")]['children'].append(loc)
                    except KeyError:
                        pre_dict[loc.get("parent_id")]['children'] = [loc]

        result = pre_dict[location_id]

    else:
        result = locations[0]

    return [result]


async def get_merge_preview(target_id, source_id, user=None):
    """ Creates a merge preview between two locations

    Args:
        target_id: The location to merge into
        source_id: The location being merged
        user: The calling user

    Returns:

    """
    if target_id == source_id:
        return dict(
            error=True,
            error_code="You can not merge a location into itself"
        )

    target_tree = await get_location_tree(target_id, user=user)
    target_tree = target_tree[0]
    source_tree = await get_location_tree(source_id, user=user)
    source_tree = source_tree[0]

    if source_tree.get("parent_id", None) is None:
        return dict(
            error=True,
            error_code="You can not merge the root location"
        )

    # Check if they're trying to merge the location into a location under itself
    if source_tree.get("uuid") in target_tree.get("lineage"):
        return dict(
            error=True,
            error_code="You can not merge a location under itself"
        )

    if target_tree.get("children", None) is None:
        target_tree['children'] = []

    for child in source_tree.get("children", []):
        child['actionType'] = "MIGRATE"
        child['parent_id'] = target_tree.get("uuid")
        target_tree['children'].append(child)

    return [target_tree]


async def merge_locations(target_id, source_id, user=None):
    """ Perform a merge between two locations

    Args:
        source_id: The UUID of the location to merge
        target_id: The UUID of the location to merge into

    Returns:

    """

    if target_id == source_id:
        return dict(
            error=True,
            error_code="You can not merge a location into itself"
        )

    target_locations = None
    target_location = None
    source_locations = None
    source_location = None

    result = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid::TEXT, name, lineage::TEXT[], parent_id::TEXT FROM %s.locations
            WHERE lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            [target_id],
        ))
        target_locations = await cur.fetchall()

        await cur.execute("""
            SELECT uuid::TEXT, name, lineage::TEXT[], parent_id::TEXT FROM %s.locations
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            target_id,
        ))
        target_location = await cur.fetchone()

        await cur.execute("""
            SELECT uuid::TEXT, name, lineage::TEXT[], parent_id::TEXT FROM %s.locations
            WHERE lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            [source_id],
        ))
        source_locations = await cur.fetchall()

        await cur.execute("""
            SELECT uuid::TEXT, name, lineage::TEXT[], parent_id::TEXT FROM %s.locations
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            source_id,
        ))
        source_location = await cur.fetchone()

    if source_location.get("parent_id", None) is None:
        return dict(
            error=True,
            error_code="You can not merge the root location"
        )

    # Check if they're trying to merge the location into a location under itself
    if source_location.get("uuid") in target_location.get("lineage"):
        return dict(
            error=True,
            error_code="You can not merge a location under itself"
        )

    # Merge the two lists (excluding the source location)
    result = target_locations + [x for x in source_locations if x.get("uuid") != source_id]

    # Map children which were originally direct children of
    # source to be direct children of target
    for item in result:
        if item.get("parent_id") == source_id:
            item['parent_id'] = target_id

    # Build lookup dict
    loc_dict = dict((x.get("uuid"), x) for x in result)

    # Sort the locations in the into a list
    locations = sorted(result, key=lambda k: len(k.get("lineage")))

    # Remap all the locations so that they have the correct lineage
    for location in locations:
        # Ignore root-level
        if location.get("uuid") != target_id:
            parent = loc_dict.get(location.get("parent_id"))
            if parent is not None:
                location['lineage'] = parent.get("lineage") + [location.get("uuid")]

    # Save the changes to all the locations
    async with get_db_cur() as cur:
        for location in locations:
            await cur.execute("""
                UPDATE %s.locations
                SET parent_id = %s,
                  lineage = %s
                  WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                location.get("parent_id"),
                location.get("lineage"),
                location.get("uuid"),
            ))

    # Now we need to remap data for the root-level
    # [x] Alerts
    # [x] Form Submissions
    # Reporting Periods
    # [x] Assignments
    # Delete old geometry
    # [x] Migrate tasks
    # [x] Migrate notifications
    # [x] GEO_ADMIN assignments
    # [x] Alarms
    # [x] Activity
    # [x] Tasks

    # Update tertiary data
    async with get_db_cur() as cur:
        await cur.execute("""
            UPDATE %s.alerts SET location_id = %s WHERE location_id = %s;
            UPDATE %s.collections SET location_id = %s WHERE location_id = %s;
            UPDATE %s.assignments SET location_id = %s WHERE location_id = %s;
            UPDATE %s.tasks_v2 SET location_id = %s WHERE location_id = %s;
            UPDATE %s.accounts SET location_id = %s WHERE location_id = %s;
            UPDATE %s.alarms SET location_spec_uuid = %s WHERE location_spec_uuid = %s;
        """, (
            AsIs(user.get("tki")), target_id, source_id,
            AsIs(user.get("tki")), target_id, source_id,
            AsIs(user.get("tki")), target_id, source_id,
            AsIs(user.get("tki")), target_id, source_id,
            AsIs(user.get("tki")), target_id, source_id,
            AsIs(user.get("tki")), target_id, source_id,
        ))

    source_reporting = []
    target_reporting = []
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.location_reporting
            WHERE location_id = %s;
        """, (
            AsIs(user.get("tki")),
            source_id,
        ))
        source_reporting = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.location_reporting
            WHERE location_id = %s
        """, (
            AsIs(user.get("tki")),
            target_id,
        ))
        target_reporting = await cur.fetchall()

    reporting_migrate = []
    reporting_delete = []

    for item in source_reporting:
        target_has = False
        for tgt_rpt in target_reporting:
            if item.get("location_id") == tgt_rpt.get("location_id") and \
                            item.get("form_id") == tgt_rpt.get("form_id"):
                target_has = True

        if target_has:
            reporting_delete.append(item)
        else:
            reporting_migrate.append(item)

    # Store the reporting periods which should be migrated
    async with get_db_cur() as cur:
        for rp in reporting_migrate:
            await cur.execute("""
                UPDATE %s.location_reporting
                SET location_id = %s
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                target_id,
                rp.get("id"),
            ))

        for rp_del in reporting_delete:
            await cur.execute("""
                DELETE FROM %s.location_reporting
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                rp_del.get("id"),
            ))

        # Delete old geometry
        await cur.execute("""
            DELETE FROM %s.location_gis
            WHERE location_id = %s;
        """, (
            AsIs(user.get("tki")),
            source_id,
        ))

        # Finally delete the old location
        await cur.execute("""
            DELETE FROM %s.locations
            WHERE uuid = %s;
        """, (
            AsIs(user.get("tki")),
            source_id,
        ))

    return dict(
        success=True
    )


async def get_groups(user=None):
    result = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT groups 
            FROM %s.locations
            WHERE groups IS NOT NULL 
              AND array_length(groups, 1) > 0;
        """, (
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchall()

    groups = []
    for location in result:
        groups = groups + location.get("groups", [])

    unique = []
    for item in groups:
        if item not in unique:
            unique.append(item)

    return unique
