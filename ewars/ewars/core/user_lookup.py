from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from psycopg2.extensions import AsIs


async def get_instance_admins(user=None):
    """ Get all high-level administrators which have control over an account
    :param user:
    :return:
    """
    if user is None:
        return []

    admins = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, email FROM %s.users
            WHERE role IN %s
                AND status = 'ACTIVE';
        """, (
            AsIs(user.get("tki")),
            ("SUPER_ADMIN", "GLOBAL_ADMIN",),
        ))
        admins = await cur.fetchall()

    return admins


async def get_regional_admins(lineage, user=None):
    """ Get all REGIONAL_ADMINS within a lineage
    :param lineage: {list} A list of location ids which make up a lineage
    :param user: {dict} The calling user
    :return: List of user records
    """
    admins = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, email FROM %s.users
                WHERE role = 'REGIONAL_ADMIN'
                AND status = 'ACTIVE'
                AND aid = %s
                AND location_id::TEXT IN %s

        """, (
            AsIs(user.get("tki")),
            user.get("aid"),
            tuple(lineage),
        ))
        admins = await cur.fetchall()

    return admins


async def get_account_admins(user=None):
    """
    Get all ACCOUNT_ADMINS for a give account
    :param user:
    :return:
    """
    users = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, name, email FROM %s.users
            WHERE role = 'ACCOUNT_ADMIN'
                AND status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
        ))
        users = await cur.fetchall()

    return users


async def get_account_admins_by_tki(tki=None):
    async with get_db_cur() as cur:
        await cur.execute('''
            SELECT id, name, email, bounced
            FROM %s.users 
            WHERE role = 'ACCOUNT_ADMIN'
              AND status = 'ACTIVE';
        ''', (
            AsIs(tki),
        ))

        results = await cur.fetchall()

    return results
