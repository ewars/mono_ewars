import datetime
import json
import asyncio

from ewars.db import get_db_cur
from ewars.core.serializers import JSONEncoder
from ewars.constants import CONSTANTS

from psycopg2.extensions import AsIs

from ewars.models import Location
from ewars.utils import date_utils
from ewars.core import notifications, user_lookup


class AssignmentTask:
    __slots__ = ['id', 'user', 'task', 'action_taken']

    def __init__(self, id, user, task):
        self.id = id
        self.user = user
        self.action_taken = None
        self.task = task

    async def _approve(self, data):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.assignments
                SET status = 'ACTIVE'
                WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                self.task['data']['assignment_id'],
            ))

        loop = asyncio.get_event_loop()
        loop.create_task(
            notifications.send(
                'ASSIGNMENT_APPROVED',
                self.task['data']['user_id'],
                data=dict(
                    form_name=self.task['data']['form_name']['en'],
                    location_name=self.task['data']['location_name'],
                    location_name_long=self.task['data']['location_full_name'],
                    approver=self.user['name'],
                    approver_email=self.user['email']
                ),
                account=self.user.get("account_name", None),
                user=self.user
            )
        )

        await self._close()

    async def _reject(self, data):
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.assignments WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                self.task['data']['assignment_id'],
            ))


        loop = asyncio.get_event_loop()
        loop.create_task(
            notifications.send(
                'ASSIGNMENT_REJECTED',
                self.task['data']['user_id'],
                data=dict(
                    form_name=self.task['data']['form_name']['en'],
                    location_name=self.task['data']['location_name'],
                    location_name_long=self.task['data']['location_full_name'],
                    approver=self.user['name'],
                    approver_email=self.user['email'],
                    reason=data.get('reason')
                ),
                account=self.user.get("account_name"),
                user=self.user
            )
        )

        await self._close()

    async def action(self, action, data):
        self.action_taken = action
        if action == CONSTANTS.APPROVE:
            await self._approve(data)
        elif action == CONSTANTS.REJECT:
            await self._reject(data)

        return True

    async def _close(self):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.tasks_v2
                    SET state = 'CLOSED',
                        action_taken = %s,
                        actioned_by = %s,
                        actioned_date = %s
                    WHERE id = %s;
            """, (
                AsIs(self.user.get("tki")),
                self.action_taken,
                self.user['id'],
                datetime.datetime.now(),
                self.id,
            ))


class AssignmentRequest:
    def __init__(self, user_id, definition, user):
        self.user = user
        self.target_user_id = user_id
        self.target_definition = definition

    async def process(self):
        location = None
        form = None
        user = None
        existing = None
        result = None

        is_location_based = False

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, name, features
                FROM %s.forms
                WHERE id = %s
            """, (
                AsIs(self.user.get("tki")),
                self.target_definition.get("form_id"),
            ))
            form = await cur.fetchone()

            if "LOCATION_REPORTING" in form['features'].keys():
                is_location_based = True
                await cur.execute("""
                    SELECT uuid, name, lineage FROM %s.locations WHERE uuid = %s
                """, (
                    AsIs(self.user.get("tki")),
                    self.target_definition.get("location_id"),
                ))
                location = await cur.fetchone()

            await cur.execute("""
                SELECT *
                FROM %s.assignments
                WHERE user_id = %s
                    AND status != 'DELETED'
                    AND status != 'INACTIVE'
            """, (
                AsIs(self.user.get("tki")),
                self.target_user_id,
            ))
            existing = await cur.fetchall()

        # Check if the user already has an assignment for this definition
        for exists in existing:
            isSameLocation = False
            isSameForm = False
            isPending = False
            isActive = False

            if str(exists.get("form_id", None)) == str(self.target_definition.get("form_id")):
                isSameForm = True

            # Support forms which do not have locations associated
            if is_location_based:
                if str(exists.get("location_id", None)) == str(self.target_definition.get("location_uuid")):
                    isSameLocation = True

            if exists['status'] == CONSTANTS.PENDING:
                isPending = True
            elif exists.get("status") == CONSTANTS.ACTIVE:
                isActive = True

            # Check if there's already an assignment
            if is_location_based:
                if isSameLocation and isSameForm and isActive:
                    result = {
                        "result": False,
                        "message": dict(
                            en="You already have the requested assignment."
                        )
                    }
                else:
                    result = None
            else:
                if isSameForm and isActive:
                    result = dict(
                        result=False,
                        message="You already have the request assignment"
                    )
                else:
                    result = None

            # Check if there's a pending assignment already awaiting approval
            if is_location_based:
                if isSameLocation and isSameForm and isPending:
                    result = dict(
                        result=False,
                        message=dict(
                            en="An assignment is awaiting approval which matches this assignment request, please contact an administrator to check the status of the request."
                        )
                    )
                else:
                    result = None
            else:
                if isSameForm and isPending:
                    result = dict(
                        result=False,
                        message="An assignment is awaiting approval which matches this assignment request, please constact and administrator to check the status of the request."
                    )
                else:
                    result = None

        if result is not None:
            return result

        # We're OK, proceed with setting up the assignment
        # If this is an admin requesting the assignment, create it immediately
        new_assign = None
        if self.user.get('role') == 'ACCOUNT_ADMIN':
            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO %s.assignments (user_id, created, created_by, last_modified, status, location_id, form_id, start_date, end_date)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
                """, (
                    AsIs(self.user.get("tki")),
                    self.target_user_id,
                    datetime.datetime.now(),
                    self.user['id'],
                    datetime.datetime.now(),
                    'ACTIVE',
                    self.target_definition.get("location_id", None),
                    self.target_definition.get("form_id", None),
                    datetime.datetime.utcnow(),
                    datetime.datetime.utcnow(),
                ))
                new_assign = await cur.fetchone()

            return {
                "result": True
            }
        else:
            # Need to create the assignment
            target_user = None

            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO %s.assignments (user_id, created, created_by, last_modified, status, location_id, form_id, start_date, end_date)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
                """, (
                    AsIs(self.user.get("tki")),
                    self.target_user_id,
                    datetime.datetime.utcnow(),
                    self.user['id'],
                    datetime.datetime.utcnow(),
                    'PENDING',
                    self.target_definition.get("location_id"),
                    self.target_definition.get("form_id", None),
                    datetime.datetime.utcnow(),
                    datetime.datetime.utcnow(),
                ))
                new_assign = await cur.fetchone()

                await cur.execute("""
                    SELECT id, name, email FROM %s.users WHERE id = %s
                """, (
                    AsIs(self.user.get("tki")),
                    self.target_user_id,
                ))
                target_user = await cur.fetchone()

            location_long_name = "Not applicable"
            location_name = "Not applicable"
            location_id = None
            location_type = None
            if location:
                location_id = location.get("uuid")
                location_name = location['name'].get("en")
                location_long_name = await Location.get_full_name(location.get("uuid"), user=self.user)
                location_type = await Location.get_type_name(location.get("uuid"), user=self.user)

            task_data = dict(
                form_name=form['name'],
                form_id=form.get("id"),
                form_interval=form.get("time_interval"),
                location_uuid=location_id,
                location_name=location_name,
                location_full_name=location_long_name,
                location_type_name=location_type,
                user_name=target_user.get("name"),
                user_email=target_user.get("email"),
                user_id=self.target_user_id,
                assignment_id=new_assign.get("uuid")
            )

            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO %s.tasks_v2 (task_type, assigned_user_id, assigned_user_types, data, location_id, state, priority, created_by)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                    AsIs(self.user.get("tki")),
                    'ASSIGNMENT_REQUEST',
                    None,
                    [CONSTANTS.ACCOUNT_ADMIN, CONSTANTS.REGIONAL_ADMIN],
                    json.dumps(task_data, cls=JSONEncoder),
                    self.target_definition.get("location_id", None),
                    'OPEN',
                    'LOW',
                    self.user['id'],
                ))

            # Send out notifications to stakeholders
            recipients = []
            if location:
                recipients = await user_lookup.get_regional_admins(location.get("lineage"), user=self.user)

            admins = await user_lookup.get_account_admins(user=self.user)
            recipients = recipients + admins

            form_interval = "DAY"
            if "INTERVAL_REPORTING" in form['features'].keys():
                form_interval = form['features']['INTERVAL_REPORTING'].get("interval", "DAY")

            data = dict(
                form_name=form['name']['en'],
                requestor=target_user['name'],
                requestor_email=target_user['email'],
                location_name=location_name,
                location_name_long=location_long_name
            )

            loop = asyncio.get_event_loop()
            loop.create_task(
                notifications.send_bulk(
                    'ASSIGNMENT_REQUEST',
                    recipients,
                    data,
                    user=self.user
                )
            )

            return {
                "result": True
            }
