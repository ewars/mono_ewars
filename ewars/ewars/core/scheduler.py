import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.core.alert_recovery import evaluate_recovery
from ewars.utils import isoweek

#: Is this start-up?
is_startup = True

TIMED_JOBS = {
    # "WEEKLY_DIGEST": dict(
    #         fn=run_digest,
    #         last_run=None,
    #         day_of_week=6,
    #         hour_of_day=23,
    #         is_running=False
    # )
}

INTERVAL_JOBS = {
    "ALERTS": dict(
        last_run=datetime.datetime.utcnow(),
        is_running=False,
        fn=evaluate_recovery,
        interval=99999999999
    )
}


async def scheduler(jobs):
    """ Checks the scheduled jobs periodicially
    and runs them under certain criteria

    Returns:

    """
    now = datetime.datetime.utcnow()

    if "TIMED" not in jobs:
        jobs["TIMED"] = TIMED_JOBS
    if "INTERVAL" not in jobs:
        jobs['INTERVAL'] = INTERVAL_JOBS

    # Iterate through the interval-based jobs and run them
    for job_name, job in jobs.get("INTERVAL").items():
        if job.get("is_running", False) is False:
            if int(_get_time_diff(job.get("last_run"), now)) > job.get("interval"):
                job['is_running'] = True
                result = await job.get("fn")()
                job['last_run'] = datetime.datetime.utcnow()
                job['is_running'] = False

    # Handle evented jobs like weekly digest
    # last run needs to be persisted for theses
    # jobs so that they're not run more than once
    global is_startup
    for tj_name, tj_method in jobs.get("TIMED").items():
        if tj_method.get("is_running", False) is False:
            # Retrieve any stored last run for the job
            if is_startup is True or tj_method.get("last_run", None) is None:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT last_run
                        FROM ewars.jobs
                        WHERE name = %s
                    """, (
                        tj_name,
                    ))
                    tj = await cur.fetchone()
                    tj_method['last_run'] = tj.get('last_run', datetime.datetime.utcnow())

            if tj_method.get("last_run", None) is None:
                tj_method['last_run'] = datetime.datetime.utcnow()

            next_run = _get_next_run(tj_method)

            if now > next_run:
                # next run is after now
                tj_method['is_running'] = True
                tj_method.get("fn")()
                tj_method['last_run'] = datetime.datetime.utcnow()
                async with get_db_cur() as cur:
                    await cur.execute("""
                        UPDATE ewars.jobs
                        SET last_run = %s
                        WHERE name = %s
                    """, (
                        datetime.datetime.now(),
                        tj_name,
                    ))
                tj_method['is_running'] = False

    is_startup = False


def _get_time_diff(start, end):
    """ Get the time in seconds between two datetimes

    Args:
        start: The lower date tiem
        end:  The higher date time

    Returns:
        int seconds between the the two dates
    """
    return (end - start).seconds


def _get_next_run(method):
    """ Based on the job configuration, figure out when the next job should run

    Args:
        method: A dict containing the information about the run

    Returns:
        A datetime for the next possible run
    """
    this_week = isoweek.Week.withdate(method.get("last_run"))
    due_date = this_week.day(method.get("day_of_week"))

    return datetime.datetime.combine(due_date, datetime.time(hour=method.get("hour_of_day")))
