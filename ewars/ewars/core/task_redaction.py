import json
import datetime
import asyncio

from psycopg2.extensions import AsIs

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.core import user_lookup
from ewars.constants import CONSTANTS

from ewars.core import notifications

import tornado
import tornado.ioloop


class RedactionTask:
    """ Represents a Redaction task within the system
    """

    def __init__(self, id, user, task):
        """ Initialize the class instance
        :param id: {int} THe id of the task
        :param user:
        :return:
        """
        self.user = user
        self.id = id
        self.task = task
        self.action_taken = None

    async def action(self, action, data):
        self.action_taken = action
        if action == "REJECT":
            await self._reject(data)
        elif action == "APPROVE":
            await self._approve(data)

    async def _reject(self, data):
        async with get_db_cur() as cur:
            # Set report back to SUBMITTED
            await cur.execute("""
                UPDATE %s.collections
                SET status = 'SUBMITTED'
                WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                self.task['data']['report_uuid'],
            ))

            # Remove retraction request
            await cur.execute("""
                UPDATE %s.retractions
                SET status = 'REJECTED'
                WHERE report_id = %s
            """, (
                AsIs(self.user.get("tki")),
                self.task['data']['report_uuid'],
            ))

        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send,
            "RETRACTION_REJECTED",
            self.task['data']['user_id'],
            data=dict(
                form_name=self.task['data']['form_name']['en'],
                report_date=self.task['data']['report_date'],
                location_name=self.task['data']['location_name']['en'],
                approver=self.user['name'],
                approver_email=self.user['email'],
                reason=data.get("reason")
            ),
            account=self.user.get("account_name", None),
            user=self.user
        )

        await self._close()

    async def _close(self):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.tasks_v2
                    SET state = 'CLOSED',
                        action_taken = %s,
                        actioned_by = %s,
                        actioned_date = %s
                    WHERE id = %s;
            """, (
                AsIs(self.user.get("tki")),
                self.action_taken,
                self.user['id'],
                datetime.datetime.now(),
                self.id,
            ))

    async def _approve(self, data):
        # Mark the report as deleted
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.collections WHERE uuid = %s;
            """, (
                AsIs(self.user.get("tki")),
                self.task['data']['report_uuid'],
            ))

            await cur.execute("""
                UPDATE %s.retractions
                SET status = 'COMMITTED'
                WHERE report_id = %s
            """, (
                AsIs(self.user.get("tki")),
                self.task['data']['report_uuid'],
            ))

        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send,
            "RETRACTION_APPROVED",
            self.task['data']['user_id'],
            data=dict(
                form_name=self.task['data']['form_name']['en'],
                report_date=self.task['data']['report_date'],
                location_name=self.task['data']['location_name']['en'],
                approver=self.user['name'],
                approver_email=self.user['email']
            ),
            account=self.user.get("account_name", None),
            user=self.user
        )

        await self._close()


class RedactionRequest:
    """ Represents an amendment within the system """

    def __init__(self, report_uuid, user, reason):
        self.report_uuid = report_uuid
        self.user = user
        self.reason = reason

    async def process(self):
        can_delete = False
        if self.user.get('role') == 'ACCOUNT_ADMIN':
            can_delete = True

        if can_delete:
            await self._delete_report()
        else:
            await self._init()

        return True

    async def _init(self):
        """ Initializes the task and inserts it into the database
        :return:
        """

        self.report = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT c.uuid, c.data_date, c.form_id, c.location_id, f.name AS form_name, f.time_interval AS form_interval, l.name AS location_name, l.lineage AS lineage
                FROM %s.collections AS c
                    LEFT JOIN %s.forms AS f ON f.id = c.form_id
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                WHERE c.uuid = %s;
            """, (
                AsIs(self.user.get("tki")),
                AsIs(self.user.get("tki")),
                AsIs(self.user.get("tki")),
                self.report_uuid,
            ))
            self.report = await cur.fetchone()

        # Create task in database
        self.task_data = {
            "form_name": self.report.get("form_name", None),
            "report_uuid": self.report_uuid,
            "user_name": self.user['name'],
            "user_email": self.user['email'],
            "user_id": self.user['id'],
            "location_name": self.report.get("location_name"),
            "report_date": self.report.get("data_date").strftime("%Y-%m-%d"),
            "reason": self.reason,
            "form_interval": self.report.get("form_interval")
        }

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.tasks_v2 (task_type, assigned_user_id, assigned_user_types, data, location_id, state, priority, created_by)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
            """, (
                AsIs(self.user.get("tki")),
                'REDACTION_REQUEST',
                None,
                [CONSTANTS.ACCOUNT_ADMIN, CONSTANTS.REGIONAL_ADMIN],
                json.dumps(self.task_data),
                self.report.get("location_id"),
                'OPEN',
                'LOW',
                self.user['id'],
            ))

            await cur.execute("""
                UPDATE %s.collections
                SET status = 'PENDING_RETRACTION'
                WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                self.report.get("uuid"),
            ))

        regional_admins = await user_lookup.get_regional_admins(self.report.get('lineage'), user=self.user)
        admins = await user_lookup.get_account_admins(user=self.user)

        recipients = regional_admins + admins

        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send_bulk,
            'RETRACTION_REQUEST',
            recipients,
            dict(
                form_name=self.report.get('form_name', {}).get('en', None),
                deleter=self.user.get('name'),
                deleter_email=self.user.get('email'),
                report_date=self.report.get('data_date'),
                location_name=self.report.get('location_name', {}).get('en', 'Unknown'),
                reason=self.reason
            ),
            user=self.user
        )

        return True

    async def _delete_report(self):
        """
        Delete the report  system
        :return:
        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.collections
                WHERE uuid = %s
            """, (
                AsIs(self.user.get("tki")),
                self.report_uuid,
            ))
