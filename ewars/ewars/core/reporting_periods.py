import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.utils import date_utils

from psycopg2.extensions import AsIs


def is_active(location, site_type_id, location_id):
    is_active = False

    if location.get("site_type_id", None) != int(site_type_id):
        return False

    if location.get("location_status", None) == "ACTIVE":
        is_active = True
    else:
        is_active = False

    if location_id not in location.get("lineage", []):
        return False

    return is_active


def has_reporting(location):
    """ Does this location have reporting

    Args:
        location:

    Returns:

    """
    return location.get("id", None) != None



def extrapolate_reporting_periods(form_id, start_date, end_date, location_id):
    form = None
    tki = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT id, features
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(tki),
            form_id,
        ))
        form = cur.fetchone()

    interval = None
    site_type_id = None
    has_location = False
    has_interval = False
    has_overdue = False
    overdue = None
    overdue_interval = None

    if "INTERVAL_REPORTING" in form.get('features').keys():
        has_interval = True
        interval = form['features']['INTERVAL_REPORTING']['interval']

    if "LOCATION_REPORTING" in form.get("features").keys():
        has_location = True
        site_type_id = form['features']['LOCATION_REPORTING']['site_type_id']

    reporting_periods = None

    with get_db_cursor() as cur:
        cur.execute("""
                    SELECT rp.*, l.lineage
                    FROM %s.location_reporting AS rp
                        LEFT JOIN %s.locations AS l ON l.uuid = rp.location_id
                    WHERE l.lineage::TEXT[] @> %s::TEXT[]
                    AND rp.status = 'ACTIVE'
                    AND rp.form_id = %s
                    ORDER BY array_length(l.lineage, 1)

                """, (
            AsIs(tki),
            AsIs(tki),
            [location_id],
            form_id,
        ))
        reporting_periods = cur.fetchall()

    if reporting_periods is None:
        return []

    start_date = date_utils.parse_date(start_date)
    start_date = date_utils.get_end_of_current_interval(start_date, interval)

    if end_date is None:
        end_date = datetime.date.today()
    else:
        end_date = date_utils.parse_date(end_date)

    end_date = date_utils.get_end_of_current_interval(end_date, interval)

    uuids = []

    expected_series = date_utils.get_date_range(start_date, end_date, interval)
    exp_dict = dict((x, dict(expected=0, on_time=0)) for x in expected_series)

    pre_dict = dict(
        (x.get("location_id"), dict(expected=0, found=0, lineage=x.get("lineage"))) for x in reporting_periods)
    for rp in reporting_periods:
        total_locations = 0
        with get_db_cursor() as cur:
            cur.execute("""
                        SELECT COUNT(*) AS total
                        FROM %s.locations
                        WHERE lineage::TEXT[] @> %s::TEXT[]
                        AND status = 'ACTIVE'
                        AND site_type_id = %s
                    """, (
                AsIs(tki),
                [rp.get("location_id")],
                site_type_id,
            ))
            total_locations = cur.fetchone()['total']

        rp_start = rp.get("start_date")
        rp_end = rp.get("end_date", datetime.date.today())
        if rp_end is None:
            rp_end = end_date

        if rp_end < end_date:
            rp_end = end_date

        if start_date > rp_start:
            rp_start = start_date

        pre_dict[rp.get("location_id")]['start_date'] = rp_start
        pre_dict[rp.get("location_id")]['end_date'] = rp_end
        pre_dict[rp.get("location_id")]['locations'] = total_locations
        pre_dict[rp.get("location_id")]['intervals'] = date_utils.get_intervals_between(rp_start, rp_end, interval)

        loc_uuids = []

        with get_db_cursor() as cur:
            cur.execute("""
                        SELECT uuid
                        FROM %s.locations
                        WHERE lineage::TEXT[] @> %s::TEXT[]
                        AND status = 'ACTIVE'
                        AND site_type_id = %s
                    """, (
                AsIs(tki),
                [rp.get("location_id")],
                site_type_id,
            ))
            loc_uuids = cur.fetchall()

        pre_dict[rp.get("location_id")]['uuids'] = [x.get("uuid") for x in loc_uuids]
        uuids = uuids + [x.get("uuid") for x in loc_uuids]

    for rp in reporting_periods:
        uuids = pre_dict[rp.get("location_id")].get("uuids", [])
        for loc_uuid, item in pre_dict.items():
            if loc_uuid != rp.get("location_id"):
                # not the same reporting period
                if rp.get("location_id") in item.get("lineage"):
                    uuids = list(set(uuids) - set(item.get("uuids")))

        pre_dict[rp.get("location_id")]['uuids'] = uuids
        pre_dict[rp.get("location_id")]['locations'] = len(uuids)

    return pre_dict






