import json
import asyncio
import datetime

from ewars.core import notifications
from ewars.constants import CONSTANTS
from ewars.db import get_db_cur
from ewars.core import user_lookup
from ewars.core.serializers import JSONEncoder
from ewars.models.organization import Organization

import tornado.ioloop

from psycopg2.extensions import AsIs


class RegistrationTask:
    def __init__(self, id, user, task):
        self.id = id
        self.user = user
        self.task = None
        self.action_taken = None
        self.task = task

    async def action(self, action, data):
        self.action_taken = action
        if action == "APPROVE":
            result = await self._approve(data)
        elif action == "REJECT":
            result = await self._reject(data)

        return True

    async def _approve(self, data):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.accounts
                SET status = 'ACTIVE',
                  role = %s,
                  location_id = %s
                WHERE user_id = %s;
            """, (
                AsIs(self.user.get("tki")),
                data.get("role"),
                data.get("location_id", None),
                data.get("user_id"),
            ))

            create_new = False
            if data.get("org_id", None) == "NEW":
                create_new = True
            if data.get("org_id", None) is None and data.get("organization_name", None) is not None:
                create_new = True

            if create_new:
                # we're making a new organization
                new_org = await Organization.create_organization(dict(
                    name=dict(en=data.get("organization_name", "Unnamed organization")),
                    status="ACTIVE",
                    acronym="",
                    site="site",
                    id=self.user.get("id")
                ),
                    user=self.user)

                await cur.execute("""
                    UPDATE _iw.users
                    SET org_id = %s 
                    WHERE id = %s;
                """, (
                    new_org.get("uuid"),
                    data.get("user_id"),
                ))

        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send,
            'REGISTRATION_APPROVED',
            int(data.get('user_id')),
            dict(
                name=data.get('user_name')
            ),
            account=self.user.get("account_name", None),
            user=self.user
        )

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.activity_feed
                (event_type, triggered_by, icon, attachments)
                VALUES ('NEW_USER', %s, 'fa-user', %s)
            """, (
                AsIs(self.user.get("tki")),
                data.get("user_id"),
                json.dumps([["USER", data.get("user_id")]]),
            ))

        await self._close()

        return True

    async def _reject(self, data):
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.accounts
                WHERE user_id = %s;
            """, (
                AsIs(self.user.get("tki")),
                data.get("user_id"),
            ))

        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send,
            'REGISTRATION_REJECTED',
            self.task.get('data', {}).get('email', None),
            dict(
                name=data.get('user_name'),
                reason=data.get('reason')
            ),
            account=self.user.get('account_name', None),
            user=self.user
        )

        await self._close()

        return True

    async def _close(self):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.tasks_v2
                    SET state = 'CLOSED',
                        action_taken = %s,
                        actioned_by = %s,
                        actioned_date = %s
                    WHERE id = %s;
            """, (
                AsIs(self.user.get("tki")),
                self.action_taken,
                self.user['id'],
                datetime.datetime.now(),
                self.id,
            ))


class RegistrationRequest:
    def __init__(self, user, tki, account):
        self.user = user
        self.tki = tki
        self.account = account

    async def process(self):
        user = None
        account = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM _iw.users
                WHERE id = %s
            """, (
                self.user.get("id"),
            ))
            user = await cur.fetchone()

        task_data = dict(
            user_name=user.get("name", None),
            email=user.get("email", None),
            org_id=user.get("org_id", None),
            user_id=user.get('id', None),
            organization_name=user.get("profile", {}).get("organization_name", None)
        )

        async with get_db_cur() as cur:
            await cur.execute("""
                    INSERT INTO %s.tasks_v2 (task_type, assigned_user_id, assigned_user_types, data, location_id, state, priority, created_by)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
                """, (
                AsIs(self.tki),
                'REGISTRATION_REQUEST',
                None,
                [CONSTANTS.ACCOUNT_ADMIN],
                json.dumps(task_data, cls=JSONEncoder),
                None,
                'OPEN',
                'LOW',
                user.get("id"),
            ))

        # Send out notifications to stakeholders
        admins = await user_lookup.get_account_admins(user=dict(tki=self.tki))

        loop = tornado.ioloop.IOLoop.current()
        loop.spawn_callback(
            notifications.send_bulk,
            'REGISTRATION_REQUEST',
            admins,
            task_data,
            account=self.account,
            user=dict(
                tki=self.tki
            )
        )
