from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.constants import CONSTANTS
from ewars.core.task_amendment import AmendmentTask, AmendmentRequest
from ewars.core.task_redaction import RedactionTask, RedactionRequest
from ewars.core.task_assignment import AssignmentTask, AssignmentRequest
from ewars.core.task_registration import RegistrationTask, RegistrationRequest


class Task:
    """ Represents a task within the system
    """

    @classmethod
    async def get(cls, id, user):
        task = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.tasks_v2 WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                id,
            ))
            task = await cur.fetchone()

        if task.get("task_type") == "REDACTION_REQUEST":
            return RedactionTask(id, user, task)
        elif task.get("task_type") == "AMENDMENT_REQUEST":
            return AmendmentTask(id, user, task)
        elif task.get('task_type') == "ASSIGNMENT_REQUEST":
            return AssignmentTask(id, user, task)
        elif task.get("task_type") == "REGISTRATION_REQUEST":
            return RegistrationTask(id, user, task)


async def action_task(task_id, action, opt_data, user=None):
    """ Perform an action against a task

    Args:
        task_id: The id of the task to action
        action: The action to take
        opt_data: Any optional data associated with the task
        user: The calling user

    Returns:

    """
    result = None

    task = await Task.get(task_id, user)

    if task is not None:
        result = await task.action(action, opt_data)

    return result


async def create_task(task_type, *args, **kwargs):
    """ Generate a task of a given type

    Args:
        task_type: THe type of task to generate
        **args: Args to pass on to the task
        **kwargs: KWArgs to pass on to the task

    Returns:

    """
    task = None
    if task_type == "AMEND":
        task = AmendmentRequest(*args)
    elif task_type == "RETRACT":
        task = RedactionRequest(*args, **kwargs)
    elif task_type == "REGISTRATION":
        task = RegistrationRequest(*args, **kwargs)
    elif task_type == "ASSIGNMENT":
        task = AssignmentRequest(*args, **kwargs)

    result = await task.process()
    return result
