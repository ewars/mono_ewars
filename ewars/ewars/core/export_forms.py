from collections import OrderedDict
import timeit
import tempfile
import codecs
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool
from functools import partial
import time
import json
import uuid
import csv
import os
from io import StringIO, BytesIO
from zipfile import ZipFile, ZIP_DEFLATED
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

from ewars.db import get_db_cur
from ewars.db import get_db_cursor
from psycopg2.extensions import AsIs
from ewars.conf import settings
from ewars.utils.six import string_types
from ewars.utils.tmp_files import write_tmp_file
from ewars.utils import date_utils

class UnicodeWriter:
    def __init__(self, f, dialect=csv.excel, encoding="utf-8-sig", **kwds):
        self.queue = StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow(row)
        data = self.queue.getvalue()
        data = self.encoder.encode(data)
        print(data)
        self.stream.write(data)
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

csv.register_dialect(
    'ewars_dialect',
    delimiter=',',
    quotechar='"',
    doublequote=True,
    skipinitialspace=True,
    lineterminator='\r\n',
    quoting=csv.QUOTE_ALL
)

def _get_label(col):
    """Get the human-readable label for a field

    Args:
        col: The field/col in question

    Returns:
        The string label for the column
    """
    if isinstance(col['label'], string_types):
        return col.get("label")
    else:
        return col['label'].get("en")


def _extrapolate_columns(definition, path, key_path=None):
    columns = []

    raw_columns = []
    for key, col in definition.items():
        col['name'] = key
        raw_columns.append(col)

    raw_columns = sorted(raw_columns, key=lambda k: int(k.get("order")))

    # for key, col in definition.items():
    for col in raw_columns:
        if col.get('redacted', False) == False:
            if col.get("fields", False):
                sub_path = None
                sub_key_path = None
                if path is not None:
                    sub_path = "%s %s" % (path, _get_label(col))
                else:
                    sub_path = _get_label(col)

                if key_path is not None:
                    sub_key_path = "%s.%s" % (key_path, col.get("name"))
                else:
                    sub_key_path = col.get("name")

                sub_fields = _extrapolate_columns(col.get("fields"), sub_path, key_path=sub_key_path)
                columns = columns + sub_fields
            else:
                item_path = None
                real_path = None
                if path is not None:
                    item_path = "%s %s" % (path, _get_label(col))
                else:
                    item_path = _get_label(col)

                if key_path is not None:
                    real_path = "%s.%s" % (key_path, col.get("name"))
                else:
                    real_path = col.get("name")
                columns.append([real_path, item_path])

    return columns


def _get_value(path, obj):
    if "." in path:
        leafs = path.split(".")

        cur_node = obj
        for leaf in leafs:
            if leaf in cur_node:
                cur_node = cur_node.get(leaf, None)
            else:
                return None

        return cur_node
    else:
        return obj.get(path, None)


def _extrapolate_row(row, key_paths):
    """
    Extrapolate row data based on definitino
    :param row:
    :param definition:
    :param path:
    :return:
    """
    row_data = dict()

    for path in key_paths:
        row_data[path[0]] = _get_value(path[0], row)

    return row_data


def ResultIter(cursor, arraysize=1000):
    while True:
        results = cursor.fetchmany(arraysize)

        if not results:
            break


        for result in results:
            yield result

def ResultPoolIter(cursor, arraysize=1000):
    while True:
        results = cursor.fetchmany(arraysize)

        if not results:
            break

        yield results
        #
        # for result in results:
        #     yield result


def _build_location_name(locations, location):
    pass

def fix_array(node):
    if isinstance(node, list):
        return '"%s"' % (','.join(node))
    elif isinstance(node, type(None)):
        return ''
    else:
        return node

def extrap(pre_columns, total_columns, loc_dict, loc_empty, node):
    pre = [node.get(col, '') for col in pre_columns]

    locs = [x for x in loc_empty]
    if node.get("location_id", None) is not None:
        if loc_dict.get(node.get("location_id", None), None) is not None:
            locs = loc_dict.get(node.get("location_id", None), [])

    r_data = json.loads(node.get('data', '{}'))
    post = [r_data.get(col, '') for col in total_columns]

    return list(pre + locs + post)

def export_forms(form_id, location_uuid, start_date, end_date, user=None):
    """
    Export data from the system for a specific form
    :param form_id: {int} The id of the form
    :param location_uuid: {str} The UUID of the location to export for
    :param start_date: {str} The start date to start the export from
    :param end_date: {str} The date to export up to
    :param options: {dict} additional options
    :param user: {dict} The user requesting the export
    :return:
    """
    # Get the data where the location is under the specified location
    form_versions = []
    with get_db_cursor() as cur:
        cur.execute('''
            SELECT * FROM %s.form_versions
            WHERE form_id = %s;
        ''', (
            AsIs(user.get('tki')),
            form_id,
        ))
        form_versions = cur.fetchall()

    form_versions = dict((str(x.get('uuid')), x) for x in form_versions)
    col_extraps = dict(
        (x.get('uuid'), _extrapolate_columns(x.get('definition', {}), None)) for x in form_versions.values())

    total_columns = []

    for key, value in col_extraps.items():
        for col in value:
            if col[0] not in total_columns:
                total_columns.append(col[0])

    data_rows = []

    location_types = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT
                l.uuid::TEXT AS location_uuid,
                l.name->>'en' AS location_name,
                l.pcode,
                sti.name->>'en' AS loc_type,
                sti.id AS sti_id,
                l.lineage,
                l.groups
            FROM %s.locations AS l
              LEFT JOIN %s.location_types AS sti ON sti.id = l.site_type_id;
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
        ))
        locations = cur.fetchall()
        locations = dict((str(x.get('location_uuid')), x) for x in locations)

        cur.execute("""
            SELECT id AS user_id, email AS submitter_email, name AS submitter_name
            FROM %s.users;
        """, (
            AsIs(user.get('tki')),
        ))
        users = cur.fetchall()
        users = dict((str(x.get('user_id')), x) for x in users)

        # Get the location types for the account
        cur.execute('''
            SELECT id, name->>'en' AS name
            FROM %s.location_types
            ORDER BY id;
        ''', (
            AsIs(user.get('tki')),
        ))
        location_types = cur.fetchall()

    loc_dict = dict()

    type_len = len(location_types)
    for lid, location in locations.items():
        lineage = location.get('lineage', [])
        names = dict((x.get('sti_id'), x.get('location_name')) for _, x in locations.items() if x.get('location_uuid') in lineage)
        names_sorted = [names.get(x.get('id'), '') for x in location_types]

        loc_dict[location.get('location_uuid')] = names_sorted


    pre_columns = ["location_uuid", "location_name", "loc_type", "pcode", "location_groups", "data_date", "isoweek", "isoyear", "iso", "submitted_date",
                   "submitter_name", "submitter_email"]
    loc_columns = [x.get('name') for x in location_types]
    end_columns = pre_columns + loc_columns + total_columns

    loc_cols = dict()

    loc_empty = ["" for x in range(0, len(loc_columns))]
    func = partial(extrap, pre_columns, total_columns, loc_dict, loc_empty)

    completed = [end_columns]

    f = StringIO()

    file_name = "%s_sep_%s_tmp.csv" % (str(uuid.uuid4()), str(time.time()).replace('.', ''))
    write_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT
                    d.location_id::TEXT,
                    to_char(d.data_date, 'YYYY-MM-DD') AS data_date,
                    to_char(d.data_date::TIMESTAMP, 'IW') as isoweek,
                    to_char(date_trunc('week', data_date) + '6 days'::INTERVAL, '"W"IW (' || to_char(data_date - '6 days'::INTERVAL, 'DD') || '-DD Mon IYYY)') AS week,
                    (SELECT to_char(d.data_date::TIMESTAMP, 'IYYY')) AS isoyear,
                    d.created_by,
                    to_char(d.submitted_date, 'YYYY-MM-DD') AS submitted_date,
                    d.data
                FROM %s.collections AS d
                    LEFT JOIN %s.locations AS l ON l.uuid = d.location_id
                WHERE d.form_id = %s
                    AND d.data_date >= '%s'
                    AND d.data_date <= '%s'
                    AND (l.lineage @> ARRAY['%s']::TEXT[] OR l.lineage IS NULL)
                    AND d.status = 'SUBMITTED'
            ) TO STDOUT WITH (FORMAT CSV, HEADER TRUE, FORCE_QUOTE *, DELIMITER ',')
        """ % (
            user.get('tki'),
            user.get('tki'),
            form_id,
            start_date,
            end_date,
            location_uuid,
        )

        cur.copy_expert(sql, f)

    f.seek(0)

    end_file_name = "%s_sep_%s.csv" % (str(uuid.uuid4()), str(time.time()).replace('.', ''))
    end_zip_file_name = "%s_%s.zip" % (str(uuid.uuid4()), str(time.time()).replace('.', ''))
    end_zip_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, end_zip_file_name)

    string_buffer = StringIO()
    tmp_name = "/tmp/%s.csv" % (str(uuid.uuid4()))
    #fp = open(tmp_name, "w+", encoding="utf-8")
    fp = codecs.open(tmp_name, "w+", "utf-8-sig")
    #writer = UnicodeWriter(fp, quoting=csv.QUOTE_ALL)

    # Reset seek after last iterator
    f.seek(0)
    # Write to string buffer
    writer = csv.writer(fp, dialect=csv.excel, quoting=csv.QUOTE_ALL)
    # Read CSV from buffer
    fileDialect = csv.Sniffer().sniff(f.read(1024), delimiters=",")
    f.seek(0)
    myReader = csv.DictReader(f, dialect=fileDialect)

    # Read in fiels from data buffer and write to string_buffer as CSV
    #d_writer.writerows([end_columns])
    writer.writerow(end_columns)
    for idx, x in enumerate(myReader):
        x = dict(x)
        x.update(locations.get(x.get('location_id'), {}))
        x.update(users.get(x.get('created_by'), {}))
        writer.writerow(func(x))

    fp.close()
    with ZipFile(end_zip_file_path, 'w', ZIP_DEFLATED) as zip_file:
        zip_file.write(tmp_name, end_file_name)
        #zip_file.writestr(end_file_name, string_buffer.getvalue())


    #try:
        #os.remove(tmp_name)
    #except OSError:
        #pass

    return dict(fn=end_zip_file_name)
