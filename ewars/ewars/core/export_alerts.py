import uuid
from io import StringIO, BytesIO
import csv
import os
import zipfile
import time

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.conf import settings
from ewars.utils import tmp_files, date_utils

csv.register_dialect(
    'ewars_dialect',
    delimiter=',',
    quotechar='"',
    doublequote=True,
    skipinitialspace=True,
    lineterminator='\r\n',
    quoting=csv.QUOTE_MINIMAL
)


ALERT_HEADERS = [
    "uuid",
    "alarm_name",
    "location",
    "location_pcode",
    "state",
    "stage",
    "stage_state",
    "risk",
    "outcome",
    "triggered",
    "alert_period"
]

ALERT_ACTION_HEADERS = [
    'alert_id',
    'type',
    'status',
    'data',
    'last_modified',
    'created',
    'submitted_date'
]

ALERT_EVENT_HEADERS = [
    'action_date',
    'action_type',
    'node_type',
    'content',
    'alert_id'
]

async def export_alerts(alarm_id, location_id, start_date, end_date, user=None):
    """ Export alerts within the system

    Args:
        definition:
        user:

    Returns:

    """
    start_date = date_utils.parse_date(start_date)
    end_date = date_utils.parse_date(end_date)

    results = []

    if alarm_id == 'ALL':
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, l.pcode AS location_pcode, al.name AS alarm_name
                FROM %s.alerts AS a 
                  LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                  LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE l.lineage::TEXT[] @> %s::TEXT[]
                AND a.trigger_end >= %s 
                AND a.trigger_start <= %s;
           """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                [location_id],
                start_date,
                end_date,
            ))
            results = await cur.fetchall()
    else:

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, l.name AS location_name, l.pcode AS location_pcode, al.name AS alarm_name
                FROM %s.alerts AS a
                 LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                 LEFT JOIN %s.alarms_v2 AS al ON al.uuid = a.alarm_id
                WHERE a.alarm_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.trigger_end >= %s
                AND a.trigger_start <= %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                alarm_id,
                [location_id],
                start_date,
                end_date,
            ))
            results = await cur.fetchall()

    if len(results) <= 0:
        return dict(
            err=True,
            code="NO_DATA"
        )


    rows = [ALERT_HEADERS]

    for alert in results:
        rows.append([
            alert.get("uuid"),
            alert.get("alarm_name"),
            alert['location_name']['en'],
            alert.get("location_pcode"),
            alert.get("state"),
            alert.get("stage"),
            alert.get("stage_state"),
            alert.get("risk"),
            alert.get("outcome"),
            alert.get("created").strftime("%Y-%m-%d"),
            alert.get("trigger_end")
        ])

    # TODO: Alert events
    # TODO: Alert actions

    alert_uuids = [x.get("uuid") for x in results]

    events = []
    actions = []
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.alert_events
            WHERE alert_id = ANY(%s);
        """, (
            AsIs(user.get("tki")),
            alert_uuids,
        ))
        events = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.alert_actions
            WHERE alert_id = ANY(%s);
        """, (
            AsIs(user.get("tki")),
            alert_uuids,
        ))
        actions = await cur.fetchall()

    event_rows = [ALERT_EVENT_HEADERS]
    for event in events:
        event_rows.append([event.get(h) for h in ALERT_EVENT_HEADERS])

    event_file = StringIO()
    d_writer = csv.writer(event_file, dialect="ewars_dialect")
    d_writer.writerows(event_rows)

    action_rows = [ALERT_ACTION_HEADERS]
    for action in actions:
        action_rows.append([action.get(h) for h in ALERT_ACTION_HEADERS])

    actions_file = StringIO()
    a_writer = csv.writer(actions_file, dialect="ewars_dialect")
    a_writer.writerows(action_rows)

    alerts_file = StringIO()
    al_writer = csv.writer(alerts_file, dialect="ewars_dialect")
    al_writer.writerows(rows)

    file_name = "export - %s - %s - %s" % (
        user.get("name"),
        start_date.strftime("%Y-%m-%d"),
        end_date.strftime("%Y-%m-%d"),
    )

    outfile = BytesIO()
    with zipfile.ZipFile(outfile, mode="w", compression=zipfile.ZIP_DEFLATED) as zf:
        zf.writestr("alerts.csv", str(alerts_file.getvalue()))
        zf.writestr("alert_actions.csv", str(actions_file.getvalue()))
        zf.writestr("alert_events.csv", str(event_file.getvalue()))

    out_file_name = "%s_sep_%s_%s.zip" % (str(uuid.uuid4()), file_name, str(time.time()).replace(".", ""))
    write_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, out_file_name)

    with open(write_path, "wb") as f:
        f.write(outfile.getvalue())


    return dict(fn=out_file_name)
