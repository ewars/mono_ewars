import sys
import os
import base64
from openpyxl.writer.excel import save_virtual_workbook

import boto3


from openpyxl import Workbook

from ewars.conf import settings

async def export_document_data(instance_name, raws, sheets, user=None):
    wb = Workbook()

    ws = wb.active
    ws.title = "Single Values"

    for row in raws:
        ws.append(row)

    for sheet in sheets:
        new_sheet = wb.create_sheet(title=sheet[0])

        for row in sheet[1]:
            new_sheet.append(row)

    instance_name = "%s.xlsx" % instance_name
    save_name = base64.urlsafe_b64encode(instance_name.encode("utf-8"))

    wb.save(
        filename=os.path.join(settings.FILE_UPLOAD_TEMP_DIR, save_name.decode("utf-8") + ".xlsx")
    )

    return dict(
        n=save_name.decode("utf-8")
    )