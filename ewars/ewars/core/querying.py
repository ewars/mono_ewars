import json

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from ewars.utils.validation import validate_uuid
from ewars.models import User

RESOURCES = {
    "user": ("_schema.users", False, 'id'),
    "account": ("_iw.accounts", False, 'id'),
    "organization": ("_schema.orgs_full", False, 'uuid'),
    "laboratory": ("_iw.laboratories", True, 'uuid'),
    "form": ("_schema.forms", False, 'id'),
    "location": ("_schema.locations", False, 'uuid'),
    "report": ("_schema.collections", False, 'uuid'),
    "collection": ("_schema.collections", False, 'uuid'),
    "location_type": ("_schema.location_types", False, 'id'),
    "alert": ("_schema.alerts", False, 'uuid'),
    "alarm": ("_schema.alarms_V2", True, 'uuid'),
    "indicator": ("_schema.indicators", False, 'uuid'),
    "indicator_group": ("_schema.indicator_groups", False, 'id'),
    "notebook": ("_schema.analysis_notebooks", False, 'uuid'),
    "assignment": ("_schema.assignments", False, 'uuid'),
    "version": ("_schema.form_versions", False, 'uuid'),
    "template": ("_schema.templates", True, 'uuid'),
    "device": ("_schema.devices", False, 'uuid'),
    "gateway": ("_schema.sms_gateways", False, 'uuid'),
    "reporting_period": ("_schema.location_reporting", False, 'uuid'),
    "alert_action": ("_schema.alert_actions", False, 'id'),
    "layout": ("_schema.layouts", False, "uuid"),
    "retraction": ("_schema.retractions", False, "uuid"),
    "amendment": ("_schema.amendments", False, "uuid"),
    "sso": ("_iw.sso", False, "id"),
    "alerts_full": ("_schema.alerts_full", False, "uuid"),
    "alert_full": ("_schema.alerts_full", False, "uuid"),
    "import_data": ("_schema.import_project_data", False, 'uuid'),
    "import_project": ("_schema.import_projects", False, 'uuid'),
    "role": ("_schema.roles", False, "uuid"),
    "assignment_full": ("_schema.v_assignments", False, "uuid"),
    "v_location": ("_schema.v_location_profile", False, "uuid"),
    "v_issue": ("_iw.v_issues", False, "uuid"),
    "c_project": ("_iw.projects", False, "uuid"),
    "c_issue_comment": ("_iw.v_issues_comments", False, "uuid"),
    "c_doc_set": ("_iw.doc_set", False, "uuid"),
    "c_documentation": ("_iw.documentation", False, "uuid"),
    "c_proposal": ("_iw.proposals", False, "uuid"),
    "c_language": ("_iw.languages", False, "id"),
    "c_translation": ("_iw.translations", False, "uuid"),
    "c_user": ("_iw.users", False, "id"),
    "c_release": ("_iw.releases", False, "uuid"),
    "hud": ("_schema.huds", False, "uuid"),
    "event_log": ("_schema.event_log", False, "uuid"),
    "outbreak": ("_schema.outbreaks", False, "uuid")
}

SINGLE_RESOURCES = {
    "alarm": ("_schema.alarms_V2", "uuid"),
    "user": ("_schema.users", "id",),
    "account": ("_iw.accounts", "id",),
    "organization": ("_schema.orgs_full", "uuid",),
    "laboratory": ("_iw.laboratory", "uuid",),
    "form": ("_schema.forms", "id",),
    "version": ("_schema.form_versions", "uuid",),
    "location": ("_schema.locations", "uuid",),
    "report": ("_schema.collections", "uuid",),
    "collection": ("_schema.collections", "uuid",),
    "assignment": ("_schema.assignments", "uuid",),
    "indicator": ("_schema.indicators", "uuid",),
    "indicator_group": ("_schema.indicator_groups", "id",),
    "template": ("_schema.templates", "uuid",),
    "alert": ("_schema.alerts", "uuid",),
    "device": ("_schema.devices", "id"),
    "location_type": ("_schema.location_types", "id"),
    "layout": ("_schema.layouts", "uuid"),
    "sso": ("_iw.sso", "id"),
    "alert_full": ("_schema.alerts_full", "uuid"),
    "import_project": ("_schema.import_projects", "uuid"),
    "c_project": ("_iw.projects", "uuid"),
    "c_user": ("_iw.users", "id"),
    "c_issue": ("_iw.issues", "uuid"),
    "c_release": ("_iw.releases", "uuid"),
    "hud": ("_schema.huds", "uuid"),
    "outbreak": ("_schema.outbreaks", "uuid"),
    "role": ("_schema.roles", "uuid"),
}

from ewars.constants import CONSTANTS

from psycopg2.extensions import AsIs


def get_filter_sql(flter):
    filter_sql = []
    for key, filt in flter.items():
        el_type = filt.get("type", None)
        if el_type is not None:
            del filt['type']
        comp_key = None
        value = None

        for t_key, t_value in filt.items():
            comp_key = t_key
            value = t_value

        if el_type == "date" and "T" in value:
            value = value.split("T")[0]

        print(comp_key, value)
        property = key
        if "." in key:
            property = "af.%s->>'%s'" % (key.split(".")[0], key.split(".")[1])

        if comp_key.upper() == CONSTANTS.IN:
            filter_sql.append("af.%s IN ('%s')" % (
                property,
                "','".join([str(x) for x in value]),
            ))
        elif comp_key.upper() == CONSTANTS.NIN:
            filter_sql.append("af.%s NOT IN ('%s')" % (
                property,
                "','".join(value),
            ))
        elif value == CONSTANTS.NULL:
            filter_sql.append("af.%s IS NULL" % (property,))
        elif value == CONSTANTS.NOT_NULL:
            filter_sql.append("af.%s IS NOT NULL" % (property,))
        elif comp_key.upper() == CONSTANTS.HAS:
            filter_sql.append("af.%s::TEXT[] @> '{%s}'::TEXT[]" % (property, value,))
        elif comp_key.upper() == "UNDER":
            filter_sql.append("lineage::TEXT[] @> '{%s}'::TEXT[]" % (value,))
        elif comp_key.upper() == CONSTANTS.CONTAINS:
            filter_sql.append("af.%s ILIKE '%%%s%%'" % (property, value.replace("'", "''"),))
        elif comp_key.upper() == CONSTANTS.LIKE:
            filter_sql.append("af.%s ILIKE '%%%s%%'" % (property, value.replace("'", "''"),))
        elif comp_key.upper() == "UNDER":
            # Used for finding locations under a given parent
            filter_sql.append("af.%s::TEXT[] @> '{%s}'::TEXT[]" % (property, value))
        else:
            if el_type is not None:
                if el_type == "date":
                    filter_sql.append("af.%s::DATE %s '%s'" % (
                        property,
                        getattr(CONSTANTS, comp_key.upper()),
                        value
                    ))
                else:
                    filter_sql.append("af.%s %s '%s'" % (
                        property,
                        getattr(CONSTANTS, comp_key.upper()),
                        value
                    ))
            else:
                filter_sql.append("af.%s %s '%s'" % (
                    property,
                    getattr(CONSTANTS, comp_key.upper()),
                    value
                ))

    sql = " AND ".join(filter_sql)
    sql = " AND " + sql
    return sql


def get_order_sql(order):
    sql = ""
    if order is not None:
        orderings = []
        for key, dir in order.items():
            direction = dir
            ord_type = None
            if ":" in dir:
                direction, ord_type = dir.split(":")

            if ord_type is None:
                if "." in key:
                    orderings.append("af.%s->>'%s' %s" % (key.split(".")[0], key.split(".")[1], direction,))
                else:
                    orderings.append("af.%s %s" % (key, direction))
            else:
                if ord_type == "number":
                    orderings.append("af.%s::NUMERIC %s" % (key, direction))
                elif ord_type in ("text", "select", "textarea"):
                    orderings.append("af.%s::TEXT %s" % (key, direction))
                elif ord_type == "DATE":
                    orderings.append("af.%s::DATE %s" % (key, direction))
                else:
                    orderings.append("af.%s::TEXT %s" % (key, direction))

        if len(orderings) > 0:
            order_sql = ", ".join(orderings)
            sql += " ORDER BY " + order_sql

    return sql


async def query_alerts(select, flter, order, limit, offset, joins, count=None, user=None):
    results = []
    total_count = 0

    if user.get("role") == "USER":
        async with get_db_cur() as cur:
            assignments = await User.assignments(user=user)
            loc_ids = []
            for assign in assignments:
                for loc in assign.get("locations", []):
                    loc_ids.append(str(loc.get("location_id")))

            await cur.execute("""
                SELECT
                  af.* ,
                  (SELECT ARRAY(SELECT name->>'en' FROM %s.locations AS lt WHERE lt.uuid::TEXT = ANY(af.lineage::TEXT[]))) AS location_full_name
                FROM %s.alerts_full AS af
                  LEFT JOIN %s.collections AS c ON c.uuid = af.triggering_report_id
                WHERE af.location_id::TEXT = ANY(%s)
                  %s
                  %s
                  LIMIT %s
                  OFFSET %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                loc_ids,
                AsIs(get_filter_sql(flter)),
                AsIs(get_order_sql(order)),
                limit,
                offset,
            ))
            results = await cur.fetchall()

            await cur.execute("""
                SELECT af.uuid
                FROM %s.alerts_full  AS af
                  LEFT JOIN %s.collections AS c ON c.uuid = af.triggering_report_id
                WHERE af.location_id::TEXT = ANY(%s)
                  %s
                  %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                loc_ids,
                AsIs(get_filter_sql(flter)),
                AsIs(get_order_sql(order)),
            ))
            total_count = len(await cur.fetchall())


    elif user.get("role") == "REGIONAL_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT af.*,
                    (SELECT ARRAY(SELECT name->>'en' FROM %s.locations AS lt WHERE lt.uuid::TEXT = ANY(af.lineage::TEXT[]))) AS location_full_name
                FROM %s.alerts_full AS af
                WHERE af.lineage::TEXT[] @> %s::TEXT[]
                %s
                %s
                LIMIT %s
                OFFSET %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                [user.get("location_id")],
                AsIs(get_filter_sql(flter)),
                AsIs(get_order_sql(order)),
                limit,
                offset,
            ))
            results = await cur.fetchall()

            await cur.execute("""
                SELECT af.uuid
                FROM %s.alerts_full AS af
                WHERE af.lineage::TEXT[] @> %s::TEXT[]
                %s
                %s
            """, (
                AsIs(user.get("tki")),
                [user.get("location_id")],
                AsIs(get_filter_sql(flter)),
                AsIs(get_order_sql(order)),
            ))
            total_count = len(await cur.fetchall())
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT af.*,
                (SELECT ARRAY(SELECT name->>'en' FROM %s.locations AS lt WHERE lt.uuid::TEXT = ANY(af.lineage::TEXT[]))) AS location_full_name
                FROM %s.alerts_full AS af
                WHERE af.uuid IS NOT NULL
                %s
                %s
                LIMIT %s
                OFFSET %s
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(get_filter_sql(flter)),
                AsIs(get_order_sql(order)),
                limit,
                offset,
            ))
            results = await cur.fetchall()

            await cur.execute("""
                SELECT af.uuid
                FROM %s.alerts_full AS af
                WHERE af.uuid IS NOT NULL
                %s
                %s
            """, (
                AsIs(user.get("tki")),
                AsIs(get_filter_sql(flter)),
                AsIs(get_order_sql(order)),
            ))
            total_count = len(await cur.fetchall())

    if count is True:
        return dict(
            results=results,
            count=total_count
        )
    else:
        return results


async def query_resource(resource, select, flter, order, limit, offset, joins, count=None, user=None):
    if resource == "alerts_full":
        return await query_alerts(select, flter, order, limit, offset, joins, count=count, user=user)

    results = []
    total_count = None

    resource_table = RESOURCES[resource][0]

    special_selects = []
    if select is not None:
        for index, item in enumerate(select):
            if "@" in item:
                special_selects.append(item)
                del select[index]

    if "_schema" in resource_table:
        resource_table = resource_table.replace("_schema", user.get("tki"))

    sql = "SELECT "

    sql_count = "SELECT COUNT(*) AS count "

    # map in select
    selects = []
    isAll = False

    if select is not None:
        selects = selects + select
    else:
        isAll = True
        sql = sql + " * "

    if len(selects) > 0 and isAll:
        sql = sql + ", " + ",".join(selects)

    if len(selects) > 0 and isAll is False:
        sql = sql + ",".join(selects)

    # Map in table to query
    sql = sql + " FROM %s " % (resource_table,)
    sql_count += " FROM %s " % (resource_table,)

    flter_sql = []
    for key, filt in flter.items():
        if len(filt.keys()) <= 0:
            continue

        el_type = filt.get("type", None)
        if el_type is not None:
            del filt['type']
        comp_key, value = filt.popitem()

        if el_type == "LOCGROUPS":
            # We query this a little differently because it's an array
            if comp_key == "EQ":
                pass
            elif comp_key == "IN":
                pass
            elif comp_key == "NIN":
                pass
            else:
                pass
        else:
            property = key
            if "." in key:
                property = "%s->>'%s'" % (key.split(".")[0], key.split(".")[1])

            if comp_key.upper() == CONSTANTS.IN:
                flter_sql.append("%s IN ('%s')" % (
                    property,
                    "','".join([str(x) for x in value]),
                ))
            elif comp_key.upper() == CONSTANTS.NIN:
                flter_sql.append("%s NOT IN ('%s')" % (
                    property,
                    "','".join(value),
                ))
            elif value == CONSTANTS.NULL:
                flter_sql.append("%s IS NULL" % (property,))
            elif value == CONSTANTS.NOT_NULL:
                flter_sql.append("%s IS NOT NULL" % (property,))
            elif comp_key.upper() == CONSTANTS.HAS:
                flter_sql.append("%s::TEXT[] @> '{%s}'::TEXT[]" % (property, value,))
            elif comp_key.upper() == "UNDER":
                flter_sql.append("lineage::TEXT[] @> '{%s}'::TEXT[]" % (value,))
            elif comp_key.upper() == CONSTANTS.LIKE:
                flter_sql.append("%s ILIKE '%%%s%%'" % (property, value.replace("'", "''"),))
            elif comp_key.upper() == CONSTANTS.CONTAINS:
                flter_sql.append("%s ILIKE '%%%s%%'" % (property, value.replace("'", "''"),))
            elif comp_key.upper() == "UNDER":
                # Used for finding locations under a given parent
                flter_sql.append("%s::TEXT[] @> '{%s}'::TEXT[]" % (property, value))
            else:
                flter_sql.append("%s %s '%s'" % (
                    property,
                    getattr(CONSTANTS, comp_key.upper()),
                    value
                ))

    if len(flter_sql) > 0:
        ft_sql = " AND ".join(flter_sql)

        sql += "WHERE " + ft_sql
        sql_count += "WHERE " + ft_sql

    if order is not None:
        orderings = []
        for key, dir in order.items():
            direction = dir
            ord_type = None
            if ":" in dir:
                direction, ord_type = dir.split(":")

            if ord_type is None:
                if "." in key:
                    orderings.append("%s->>'%s' %s" % (key.split(".")[0], key.split(".")[1], direction,))
                else:
                    orderings.append("%s %s" % (key, direction))
            else:
                if ord_type == "number":
                    orderings.append("%s::NUMERIC %s" % (key, direction))
                elif ord_type in ("text", "select", "textarea"):
                    orderings.append("%s::TEXT %s" % (key, direction))
                elif ord_type == "DATE":
                    orderings.append("%s::DATE %s" % (key, direction))
                else:
                    orderings.append("%s::TEXT %s" % (key, direction))

        if len(orderings) > 0:
            order_sql = ", ".join(orderings)
            sql += " ORDER BY " + order_sql

    if limit is not None:
        sql += " LIMIT %s" % (limit,)
    if offset is not None:
        sql += " OFFSET %s" % (offset,)

    async with get_db_cur() as cur:
        await cur.execute(sql)
        results = await cur.fetchall()
        await cur.execute(sql_count)
        total_count = await cur.fetchone()
        total_count = total_count.get('count')

    if joins is not None:
        for join in joins:
            for result in results:
                join_result = None
                definition = join.split(":")
                query_table = RESOURCES[definition[0]][0]

                select = "*"
                try:
                    if definition[3]:
                        select = definition[3].split(",")
                except IndexError:
                    pass

                if "_schema" in query_table:
                    query_table = query_table.replace("_schema", user.get("tki"))

                perform_query = True
                if definition[2] == "uuid":
                    if not validate_uuid(result[definition[1]]):
                        perform_query = False

                if perform_query:
                    async with get_db_cur() as cur:
                        await cur.execute("""
                            SELECT %s
                                FROM %s
                                WHERE %s = %s
                        """, (
                            AsIs(", ".join(select)),
                            AsIs(query_table),
                            AsIs(definition[2]),
                            result[definition[1]],
                        ))
                        join_result = await cur.fetchone()

                    result[definition[0]] = join_result
                else:
                    result[definition[0]] = None

    if len(special_selects):
        if "@children" in special_selects and resource == "location":
            async with get_db_cur() as cur:
                for item in results:
                    await cur.execute("""
                        SELECT COUNT(*) AS children
                        FROM %s.locations
                        WHERE parent_id = %s
                            AND status != 'DELETED'
                    """, (
                        AsIs(user.get("tki")),
                        item.get("uuid"),
                    ))
                    res = await cur.fetchone()
                    item['children'] = res.get('children')

        if '@lineage' in special_selects and resource == 'location':
            async with get_db_cur() as cur:
                for item in results:
                    await cur.execute("""
                        SELECT (
                            SELECT array(
                                SELECT name->>'en' FROM %s.locations AS t
                                WHERE t.uuid::TEXT = ANY(l.lineage::TEXT[])
                                ORDER BY array_length(t.lineage, 1)
                            )
                        ) AS names
                        FROM %s.locations AS l
                        WHERE l.uuid = %s;
                    """, (
                        AsIs(user.get("tki")),
                        AsIs(user.get('tki')),
                        item.get("uuid"),
                    ))
                    res = await cur.fetchone()
                    item['@lineage'] = res.get("names", [])

    # Handle the funcky storage of the ds_indicator in alarms
    if resource == 'alarm':
        for result in results:
            if '}' in result.get('ds_indicator', ''):
                result['ds_indicator'] = json.loads(result.get('ds_indicator'))

    if count is True:
        return dict(
            results=results,
            count=total_count
        )
    else:
        return results


async def get_resource(resource, id, select, joins, user=None):
    result = None

    resource_table = SINGLE_RESOURCES[resource][0]

    if "schema" in resource_table:
        resource_table = resource_table.replace("_schema", user.get("tki"))

    sql = "SELECT "

    # map in select
    selects = []
    isAll = False

    if select is not None:
        selects = selects + select
        if "account_id" in selects:
            selects = [x for x in selects if x != "account_id"]
    else:
        isAll = True
        sql = sql + " * "

    if len(selects) > 0 and isAll:
        sql = sql + ", " + ",".join(selects)

    if len(selects) > 0 and isAll is False:
        sql = sql + ",".join(selects)

    # Map in table to query
    sql = sql + " FROM %s " % (resource_table,)

    sql = sql + " WHERE %s = '%s'" % (SINGLE_RESOURCES[resource][1], id,)

    async with get_db_cur() as cur:
        await cur.execute(sql)
        result = await cur.fetchone()

    if result is None:
        return None

    # Need to get GeoJSON for locations if we're querying for locations
    should_get_geom = False
    if resource == "location":
        if select is None:
            should_get_geom = True
        else:
            if "geometry" in select:
                select.append('geojson')
                should_get_geom = True

    if should_get_geom:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT ST_AsGeoJSON(default_center) AS default_center
                FROM %s.locations
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                result.get("uuid"),
            ))
            res = await cur.fetchone()
            result['default_center'] = res.get('default_center')

    if joins is not None:
        for join in joins:
            join_result = None
            definition = join.split(":")
            resource = SINGLE_RESOURCES[definition[0]]
            query_table = resource[0]

            if "schema" in query_table:
                query_table = query_table.replace("_schema", user.get("tki"))

            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT *
                        FROM %s
                        WHERE %s = %s
                """, (
                    AsIs(query_table),
                    AsIs(definition[2]),
                    result[definition[1]],
                ))
                join_result = await cur.fetchone()

            result[definition[0]] = join_result

    # Handle the funky storage of the indicator definition in alarms
    if result is not None and resource == 'alarm':
        if '{' in result.get('ds_indicator', ''):
            result['ds_indicator'] = json.loads(result.get('ds_indicator'))

    return result
