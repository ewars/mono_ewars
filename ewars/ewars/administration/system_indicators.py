GROUPS = [
    [1, dict(en="System"), None]
]

INDICATORS = [
    [
        "ffec4451-b00f-466e-9935-2d9c060576f1",
        {"en": "Users"},
        "USERS",
        [
            ["status", ["ACTIVE", "INACTIVE", "PENDING_APPROVAL"]],
            ["organization_id", {"resource": "organization"}],
            ["type", ["USER", "REGIONAL_ADMIN", "ACCOUNT_ADMIN"]]
        ],
        "users"
    ],
    [
        "a18b5f1f-4775-4bc9-89cf-b0d83abab45a",
        {"en": "Alerts"},
        "ALERTS",
        [
            ["state", ["OPEN", "CLOSED", "DISCARDED"]],
            ["stage_state", ["PENDING", "ACTIVE", "COMPLETED"]],
            ["stage", ["RISK", "RISK_CHAR", "RISK_ASSESS", "OUTCOME"]],
            ["risk", ["LOW", "MODERATE", "HIGH", "SEVERE"]],
            ["alarm_id", {"resource": "alarm"}],
            ["organization_id", {"resource": "organization"}]
        ],
        "alerts"
    ],
    [
        "470cc1fc-cfb3-49d9-8ac8-b1788b9c28f6",
        {"en": "Locations"},
        "LOCATIONS",
        [
            ["status",["ACTIVE","DISABLED"]],
            ["form_id",{"resource":"form"}]
        ],
        "locations"
    ],
    [
        "e51f4ca7-1678-44e3-b5cb-64f84555696f",
        {"en": "Form Submissions"},
        "FORM_SUBMISSIONS",
        [
            ["form_id",{"resource":"form"}],
            ["metric",["SUBMITTED","LATE","EXPECTED","MISSING"]],
            ["organization_id",{"resource":"organization"}],
            ["submitter_type",["USER","ACCOUNT_ADMIN","REGIONAL_ADMIN"]],
            ["source",["ALL","MOBILE","DESKTOP","WEB","NODE","SMS"]]
        ],
        "form_submissions"
    ],
    [
        "59deb036-9857-401d-bec6-86b991071912",
        {"en": "Assignments"},
        "ASSIGNMENTS",
        [
            ["form_id",{"resource":"form"}],
            ["status",["ACTIVE","ELAPSED","INACTIVE"]]
        ],
        "assignments"
    ],
    [
        "ddfea0c3-a047-450f-9681-d5a992f2cdee",
        {"en": "Devices"},
        "DEVICES",
        [
            ["type",["ANDROID","IOS","DESKTOP","NODE"]],
            ["version",""],
            ["organization_id",{"resource":"organization"}],
            ["status",["ANY","SYNCED","UNSYNCED"]]
        ],
        "devices"
    ],
    [
        "b59c5be2-5e7a-4e74-9319-c2ce7d6adbf2",
        {"en": "Tasks"},
        "TASKS",
        [
            ["type",["REGISTRATION","AMENDMENT","DISCARD"]],
            ["status",["ANY","PENDING","APPROVED","REJECTED"]]
        ],
        "tasks"
    ],
    [
        "08f844e7-62d2-4c8d-81dd-41b10808783c",
        {"en": "Forms"},
        "FORMS",
        [
            ["status",["ACTIVE","DRAFT"]],
            ["type",["PRIMARY"]],
            ["features","FEATURES"]
        ],
        "forms"
    ]
]
