import datetime

from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

__ALL__ = ["add_assignment", "delete_assignment", "update_assignment"]


def add_assignment(user_id, data, user=None):
    """ Add a new assignment to a user

    Args:
        user_id: THe id of the user to add to
        data: The data for the assignment
        user: THe calling user

    Returns:

    """

    result = None


    # Check if an assignment already exists
    count = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.assignments
            WHERE form_id = %s
                AND location_id = %s
                AND user_id = %s
        """, (
            AsIs(user.get("tki")),
            data.get("form_id"),
            data.get("location_uuid", None),
            data.get("user_id"),
        ))
        count = cur.fetchone()['total']

    if count > 0:
        return dict(
            err=True,
            error="DUPLICATE_ASSIGNMENT"
        )


    with get_db_cursor(commit=True) as cur:
        cur.execute("""
         INSERT INTO %s.assignments
         (user_id, created, created_by, last_modified, status, location_id, form_id, start_date, end_date)
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
        """, (
            AsIs(user.get("tki")),
            data.get("user_id"),
            datetime.datetime.now(),
            user.get("id"),
            datetime.datetime.now(),
            data.get('status'),
            data.get("location_uuid", None),
            data.get("form_id", None),
            data.get("start_date", None),
            data.get("end_date", None),
        ))
        result = cur.fetchone()


    return result


def delete_assignment(assign_id, user=None):
    """ Delete an assignment from the system

    Args:
        assign_id: THe assignment to remove
        user: The calling user

    Returns:

    """
    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            DELETE FROM %s.assignments
            WHERE uuid =%s
        """, (
            AsIs(user.get("tki")),
            assign_id,
        ))

    return True


def update_assignment(assign_id, data, user=None):
    """ Update an assignmnet

    Args:
        assign_id: The id of the assignment
        data: Teh data to update the assignment with
        user: The calling user

    Returns:

    """
    result = None

    with get_db_cursor(commit=True) as cur:
        cur.execute("""
            UPDATE %s.assignments
            SET status = %s,
                location_id = %s,
                form_id = %s,
                start_date = %s,
                end_date = %s
            WHERE uuid = %s
        """, (
            AsIs(user.get("tki")),
            data.get("status"),
            data.get("location_id", None),
            data.get("form_id", None),
            data.get('start_date', None),
            data.get("end_date", None),
            assign_id,
        ))

    return result


def query_assignments(user_id, user=None):
    """ Retrieve a users list of assignments

    Args:
        user_id: The id of the user
        user: The calling user

    Returns:

    """
    results = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.*, f.name AS form_name, l.name AS location_name
             FROM %s.assignments AS a
             LEFT JOIN %s.locations AS l on l.uuid = a.location_id
             LEFT JOIN %s.forms AS f ON f.id = a.form_id
             WHERE a.status IN ('ACTIVE', 'INACTIVE')
                AND a.user_id = %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            user_id,
        ))
        results = cur.fetchall()

    return results