from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

from ewars.utils import date_utils


def get_total_alerts(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    user = kwargs.get("user")

    results = []

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.*, l.name
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE l.lineage::TEXT[] @> %s::TEXT[]
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
                AND a.state != 'DELETED'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [target_location['uuid']],
            lookup_start,
            end_date,
        ))
        alerts = cur.fetchall()

    pre_data = dict((key, 0) for key in time_series)

    for alert in alerts:
        try:
            pre_data[alert['trigger_end']] += 1
        except KeyError:
            pre_data[alert['trigger_end']] = 1

    results = [[key, value] for key, value in pre_data.items()]

    return results


def get_active_alerts(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    user = kwargs.get("user")

    results = []

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.*, l.name
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE l.lineage::TEXT[] @> %s::TEXT[]
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
                AND a.state = 'OPEN'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [target_location['uuid']],
            lookup_start,
            end_date,
        ))
        alerts = cur.fetchall()

    pre_data = dict((key, 0) for key in time_series)

    for alert in alerts:
        try:
            pre_data[alert['trigger_end']] += 1
        except KeyError:
            pre_data[alert['trigger_end']] = 1

    results = [[key, value] for key, value in pre_data.items()]

    return results


def get_inactive_alerts(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    user = kwargs.get("user")

    results = []

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)

    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.*, l.name
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE l.lineage::TEXT[] @> %s::TEXT[]
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
                AND a.state = 'CLOSED'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [target_location['uuid']],
            lookup_start,
            end_date,
        ))
        alerts = cur.fetchall()

    pre_data = dict((key, 0) for key in time_series)

    for alert in alerts:
        try:
            pre_data[alert['trigger_end']] += 1
        except KeyError:
            pre_data[alert['trigger_end']] = 1

    results = [[key, value] for key, value in pre_data.items()]

    return results
