import datetime

from ewars.db import get_db_cursor
from ewars.constants import CONSTANTS

from psycopg2.extensions import AsIs

from ewars.utils import date_utils


def get_completeness(*args, **kwargs):
    """
    Returs the number of reports submitted in a period
    :param location_uuid:
    :param form_id:
    :param start_date:
    :param end_date:
    :param interval:
    :param user:
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    form_id = kwargs.get("form_id")
    user = kwargs.get("user")

    results = []

    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT *
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = cur.fetchone()

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    form_time_series = date_utils.get_date_range(start_date, end_date, form['time_interval'])

    active_locations = 0
    with get_db_cursor() as cur:
        cur.execute("""
            SELECT COUNT(*) AS ct
            FROM %s.locations
            WHERE lineage::TEXT[] @> %s::TEXT[]
                AND status = 'ACTIVE'
                AND site_type_id = %s
        """, (
            AsIs(user.get("tki")),
            [target_location['uuid']],
            form['location_type'][0],
        ))
        active_locations = cur.fetchone()['ct']

    # Get the start date of the first interval
    lookup_start = date_utils.get_start_of_interval(form_time_series[0], form['time_interval'])
    pre_data = dict((item.strftime("%Y-%m-%d"), dict(expected=active_locations, submitted=0)) for item in form_time_series)

    reports = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT r.uuid, r.data_date, (SELECT 1) AS ct
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
            WHERE r.status = 'SUBMITTED'
                AND r.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND data_date >= %s
                AND data_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location['uuid']],
            lookup_start,
            form_time_series[-1],
        ))
        reports = cur.fetchall()



    # For each report, map its data out to the correct pre_data category
    for report in reports:
        ddate = date_utils.parse_date(report['data_date'])
        string_date = ddate.strftime("%Y-%m-%d")

        if string_date in pre_data.keys():
            pre_data[string_date]['submitted'] += 1

    # Iterate through and adjust based on whether
    # the reporting date is over
    for key, data in pre_data.items():
        ddate = date_utils.parse_date(key)

        # if we're IN the ddate interval now, leave it in, but if it's past the current period take it out
        # so that it doesn't skew the timeliness
        if ddate > datetime.date.today():
            del pre_data[key]

        if target_interval == form['time_interval']:
            if ddate > end_date and key in pre_data.keys():
                del pre_data[key]

    results = []

    if target_interval == form['time_interval']:
        for key, data in pre_data.items():
            value = 0
            try:
                value = float(data.get("submitted")) / data.get("expected")
            except ZeroDivisionError:
                pass
            results.append((key, value,))
    elif target_interval == CONSTANTS.YEAR:
        coarse = dict((item.strftime("%Y-%m-%d"), dict(expected=0, submitted=0)) for item in time_series)

        for key, data in pre_data.items():
            source_date = date_utils.parse_date(key)
            # Need to check if this is a partial year (i.e. it's the current year and it's not over yet)
            is_partial_year = False

            if date_utils.is_in(datetime.date.today(), "DAY", source_date, CONSTANTS.YEAR):
                is_partial_year = True

            # Find which target this belongs to
            target = None
            for target_date in time_series:
                if date_utils.is_in(source_date, form['time_interval'], target_date, target_interval):
                    target = target_date

            if target is not None:
                string_target = target.strftime("%Y-%m-%d")
                coarse[string_target]['expected'] += data.get("expected")
                coarse[string_target]['submitted'] += data.get("submitted")

        for key, data in coarse.items():
            value = 0
            try:
                value = float(data.get("submitted")) / float(data.get('expected'))
            except ZeroDivisionError:
                pass

            results.append((key, value))
    else:
        for key, data in pre_data.items():
            value = 0
            try:
                value = float(data.get("submitted")) / data.get("expected")
            except ZeroDivisionError:
                pass
            results.append((key, value))


    return results
