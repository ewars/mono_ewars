from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs

from ewars.constants import CONSTANTS
from ewars.utils import date_utils


def get_active_alerts(*args, **kwargs):
    """
    Get a series of active of alert counts for a specific alarm and location
    :param target_location: {uuid} THe UUID of the location to get data fro
    :param target_interval: {str} The target time interval the data should be formatted to
    :param start_date: {str} THe start date for the intervals
    :param end_date: {str} The end date of the data requested
    :param reduction: {str} An aggregate function to be applied to the data
    :param form_id: {int} The id of the alarm to query against
    :param user: {dict} The calling user
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    alarm_id = kwargs.get("alarm_id")
    user = kwargs.get("user")

    results = []

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)
    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND a.state = 'OPEN'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            lookup_start,
            end_date,
            [target_location.get("uuid")],
        ))
        alerts = cur.fetchall()

    pre_data = dict((key, 0) for key in time_series)

    for alert in alerts:
        try:
            pre_data[alert['trigger_end']] += 1
        except KeyError:
            pre_data[alert['trigger_end']] = 1

    results = [[key, value] for key, value in pre_data.items()]

    return results


def get_inactive_alerts(*args, **kwargs):
    """
    Get a sereis of inactive of alert countrs for a specific alarm and location
    :param target_location: {uuid} THe UUID of the location to get data fro
    :param target_interval: {str} The target time interval the data should be formatted to
    :param start_date: {str} THe start date for the intervals
    :param end_date: {str} The end date of the data requested
    :param reduction: {str} An aggregate function to be applied to the data
    :param form_id: {int} The id of the alarm to query against
    :param user: {dict} The calling user
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    alarm_id = kwargs.get("alarm_id")
    user = kwargs.get("user")

    results = []

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)
    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND a.state = 'CLOSED'
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            lookup_start,
            end_date,
            [target_location.get("uuid")],
        ))
        alerts = cur.fetchall()

    pre_data = dict((key, 0) for key in time_series)

    for alert in alerts:
        try:
            pre_data[alert['trigger_end']] += 1
        except KeyError:
            pre_data[alert['trigger_end']] = 1

    results = [[key, value] for key, value in pre_data.items()]

    return results


def get_all_alerts(*args, **kwargs):
    """
    Get a sereis of inactive of alert countrs for a specific alarm and location
    :param target_location: {uuid} THe UUID of the location to get data fro
    :param target_interval: {str} The target time interval the data should be formatted to
    :param start_date: {str} THe start date for the intervals
    :param end_date: {str} The end date of the data requested
    :param reduction: {str} An aggregate function to be applied to the data
    :param form_id: {int} The id of the alarm to query against
    :param user: {dict} The calling user
    :return:
    """
    target_location = kwargs.get("target_location")
    target_interval = kwargs.get("target_interval")
    start_date = kwargs.get("start_date")
    end_date = kwargs.get("end_date")
    alarm_id = kwargs.get("alarm_id")
    user = kwargs.get("user")

    results = []

    time_series = date_utils.get_date_range(start_date, end_date, target_interval)

    lookup_start = date_utils.get_start_of_interval(start_date, target_interval)
    alerts = []

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT a.trigger_end
            FROM %s.alerts AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
            WHERE a.alarm_id = %s
                AND a.trigger_end >= %s
                AND a.trigger_end <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            alarm_id,
            lookup_start,
            end_date,
            [target_location.get("uuid")],
        ))
        alerts = cur.fetchall()

    pre_data = dict((key, 0) for key in time_series)

    for alert in alerts:
        try:
            pre_data[alert['trigger_end']] += 1
        except KeyError:
            pre_data[alert['trigger_end']] = 1

    results = [[key, value] for key, value in pre_data.items()]

    return results
