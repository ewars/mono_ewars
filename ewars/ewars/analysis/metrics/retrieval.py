import datetime

from ewars.db import get_db_cursor
from ewars.db import get_db_cur

from ewars.core import auditing
from ewars.core.analysis import immediate

from psycopg2.extensions import AsIs


async def get_forms(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total FROM %s.forms
            WHERE status  = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            user.get("aid"),
        ))
        result = await cur.fetchone()['total']

    return result


async def get_submissions(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.collections AS c
            LEFT JOIN %s.forms AS f ON f.id = c.form_id
            WHERE c.status = 'SUBMITTED'
            AND f.status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_assignments(user=None):
    result = None

    if user.get("role") == "USER":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.assignments
                WHERE user_id = %s
                AND status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
                user.get('id'),
            ))
            result = await cur.fetchone()
            result = result.get('total')

    elif user.get("role") == "ACCOUNT_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.assignments AS a
                LEFT JOIN %s.forms AS f ON f.id = a.form_id
                WHERE a.status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))
            result = await cur.fetchone()
            result = result.get('total')

    elif user.get("role") == "REGIONAL_ADMIN":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.assignments AS a
                LEFT JOIN %s.locations AS l ON l.uuid = a.location_id
                LEFT JOIN _iw.users AS u ON a.user_id = u.id
                WHERE a.status = 'ACTIVE'
                AND l.lineage::TEXT[] @> %s::TEXT[]
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                [user.get("location_id")],
            ))
            result = await cur.fetchone()
            result = result.get('total')

    else:
        result = 0

    return result


async def get_reporting_locations(user=None):
    """ Get the number of reporting locations for a given user

    Args:
        user:

    Returns:

    """
    result = 0

    if user.get("user_type", None) == "ACCOUNT_ADMIN":
        result = immediate.run(
            target_interval="DAY",
            target_location=user['account'].get("location_id"),
            indicator=dict(
                uuid='08f844e7-62d2-4c8d-81dd-41b10808783c',
                metric="REPORTING_LOCATIONS"
            ),
            options=dict(
                start_date=datetime.date.today(),
                end_date=datetime.date.today(),
                reduction="MAX"
            ),
            user=user
        )
        result = result.get('data', 0)

    elif user.get("user_type") == "USER":
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT COUNT(*) AS total
                FROM %s.assignments
                WHERE user_id = %s
                AND status = 'ACTIVE'
                AND location_id IS NOT NULL
            """, (
                AsIs(user.get("tki")),
                user.get('id'),
            ))
            result = await cur.fetchone()['total']
    elif user.get("user_type") == "REGIONAL_ADMIN":
        result = immediate.run(
            target_interval="DAY",
            target_location=user.get("location_id"),
            indicator=dict(
                uuid='08f844e7-62d2-4c8d-81dd-41b10808783c',
                metric="REPORTING_LOCATIONS"
            ),
            options=dict(
                start_date=datetime.date.today(),
                end_date=datetime.date.today(),
                reduction="MAX"
            ),
            user=user
        )
        result = result.get("data", 0)

    return result


async def get_users(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.users
            WHERE aid = %s
            AND status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            user.get("aid"),
        ))
        result = await cur.fetchone()['total']

    return result


async def get_locations(user=None):
    location = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT location_id
            FROM _iw.accounts
            WHERE id = %s
        """, (
            user.get("aid"),
        ))
        location = await cur.fetchone()['location_id']

    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.locations AS l
                LEFT JOIN _iw.accounts AS a ON a.location_id = l.uuid
            WHERE l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            [location],
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_open_alerts(user=None):
    result = 0
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.alerts AS a
                LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
            WHERE a.state= 'OPEN'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_all_alerts(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.alerts AS a
                LEFT JOIN %s.alarms AS al on al.uuid = a.alarm_id
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_alerts_closed(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.alerts AS a
                LEFT JOIN %s.alarms AS al ON al.uuid = a.alarm_id
            WHERE a.state = 'CLOSED'
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_devices(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.devices
        """, (
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_partners(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(DISTINCT(org_id)) AS total
            FROM %s.users
            WHERE aid = %s
        """, (
            AsIs(user.get("tki")),
            user.get("aid"),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_tasks_open(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.tasks_v2
            WHERE state = 'OPEN'
        """, (
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_tasks(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.tasks_v2
        """, (
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_completeness(user=None):
    return 0
    # location = None
    #
    # with get_db_cursor() as cur:
    #     cur.execute("""
    #         SELECT location_id
    #         FROM _iw.accounts
    #         WHERE id = %s
    #     """, (
    #         user.get("aid"),
    #     ))
    #     location = cur.fetchone()['location_id']
    #
    # today = datetime.date.today()
    # today = today.strftime("%Y-%m-%d")
    # result = auditing.get_completeness_location(location, "2011-01-01", today, user=user)
    # return result


async def get_timeliness(user=None):
    return 0
    # location = None
    #
    # with get_db_cursor() as cur:
    #     cur.execute("""
    #             SELECT location_id
    #             FROM _iw.accounts
    #             WHERE id = %s
    #         """, (
    #         user.get("aid"),
    #     ))
    #     location = cur.fetchone()['location_id']
    #
    # today = datetime.date.today()
    # today = today.strftime("%Y-%m-%d")
    # result = auditing.get_timeliness_location(location, "2011-01-01", today, user=user)
    # return result


async def get_documents(user=None):
    return 0


async def get_alarms(user=None):
    result = 0

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT COUNT(*) AS total
            FROM %s.alarms
            AND status IS TRUE
        """, (
            AsIs(user.get("tki")),
        ))
        result = await cur.fetchone()
        result = result.get('total')

    return result


async def get_organizations(user=None):
    """ Get the number of distinct organizations
    within an account

    Args:
        user:

    Returns:

    """
    result = None

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT DISTINCT(org_id)
            FROM %s.users
            WHERE aid = %s
            AND status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            user.get("aid"),
        ))
        result = await cur.fetchall()

    result = len(result)

    return result
