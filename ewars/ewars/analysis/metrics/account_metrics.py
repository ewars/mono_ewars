from ewars.db import get_db_cursor

from psycopg2.extensions import AsIs


def get_account_summary_metrics(user=None):
    """Retrieve stats about a users account

    Args:
        user: The calling user

    Returns:
        A dict containing scalars for various metrics
    """
    account_id = user['aid']

    data = dict(
            #: The number of reports
            reports=0,
            #: Number of assignments
            assignments=0,
            #: Number of locations
            locations=0,
            #: Number of users
            users=0,
            #: Number of organizations active in the account
            partners=0
    )

    with get_db_cursor() as cur:
        # Get report count
        cur.execute("""
            SELECT COUNT(*) AS total_reports
                FROM %s.collections AS c
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                WHERE l.lineage @> %s
                    AND c.status = 'SUBMITTED';
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            [user['account']['location_id']],
        ))
        data['reports'] = cur.fetchone()['total_reports']

        # Get assignment count
        cur.execute("""
            SELECT COUNT(*) AS total_assignments
                FROM %s.assignments AS a
                    LEFT JOIN _iw.users AS u ON u.id = a.user_id
                WHERE a.status = 'ACTIVE'
        """, (
            AsIs(user.get("tki")),
            account_id,
        ))
        data['assignments'] = cur.fetchone()['total_assignments']

        # Get locations, this equals the assignable locations within the system
        forms = []
        with get_db_cursor() as cur:
            cur.execute("""
                SELECT id, site_type_id
                    FROM %s.forms
                    WHERE status = 'ACTIVE'
                        AND ftype = 'PRIMARY'
            """, (
                AsIs(user.get("tki")),
                [account_id],
            ))
            forms = cur.fetchall()

            location_types = []
            for form in forms:
                if form['site_type_id'] is not None:
                    if int(form['site_type_id']) not in location_types:
                        location_types.append(int(form['site_type_id']))

            if len(location_types) > 0:
                cur.execute("""
                    SELECT COUNT(*) AS total_locations
                        FROM %s.locations
                        WHERE site_type_id IN %s
                            AND status = 'ACTIVE'
                            AND lineage::TEXT[] @> %s::TEXT[]
                """, (
                    AsIs(user.get("tki")),
                    tuple(location_types),
                    [user['account']['location_id']],
                ))
                data['locations'] = cur.fetchone()['total_locations']
            else:
                data['location'] = 0

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT COUNT(*) AS total_users
                    FROM _iw.users
                    WHERE account_id = %s
            """, (
                account_id,
            ))
            data['users'] = cur.fetchone()['total_users']

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT COUNT(DISTINCT org_id) AS total
                  FROM _iw.users
                  WHERE org_id IS NOT NULL
                    AND account_id = %s
                  GROUP BY org_id;
            """, (
                account_id,
            ))
            items = cur.fetchall()
            data['partners'] = sum([x['total'] for x in items])

    return data
