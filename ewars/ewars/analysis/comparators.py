#cython: boundscheck=False
#cython: wraparound=False

def compare(comparison, x, y):
    x = float(x)
    y = float(y)
    if comparison == "GT":
        return x > y
    elif comparison == "GTE":
        return x >= y
    elif comparison == "LT":
        return x < y
    elif comparison == "LTE":
        return x <= y
    elif comparison == "EQ":
        return x == y
    elif comparison == "NEQ":
        return x != y


