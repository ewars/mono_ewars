import ast
import operator as op
import re
import time
import json
from statistics import median, mean
from functools import lru_cache
from multiprocessing.pool import ThreadPool

from ewars.analysis import immediate, aggregate, groups
from ewars.models import Location
from ewars.utils.six import string_types

from ewars.db import get_db_cursor
from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
             ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor,
             ast.USub: op.neg}

async def analysis_parser(data, user=None):
    result = None

    query_type = data.get("type", "SERIES")

    if query_type == "SERIES":
        result = await _handle_series(data, user=user)

    elif query_type == "GENERATOR":

        gen_location_parent = data['generator']['location_parent']
        gen_location_type = data['generator']['location_type']
        gen_location_status = data['generator']['location_status']

        locations = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT, name, pcode FROM %s.locations
                WHERE status = %s
                    AND site_type_id = %s
                    AND lineage::TEXT[] @> %s::TEXT[]
            """, (
                AsIs(user.get("tki")),
                gen_location_status,
                gen_location_type,
                [gen_location_parent],
            ))
            locations = await cur.fetchall()

        target_interval = data.get("target_interval")
        target_indicator = data.get("target_indicator")
        options = data.get("options")

        if options is None:
            options = dict()

        if "reduction" in data:
            options['reduction'] = data.get("reduction", None)
            options['geometry'] = data.get("geometry", False)
            options['parent'] = data.get("parent_label", False)
            options['fill_missing'] = data.get("fill_missing", False)

        result = []
        for target_location in locations:
            sub_result = await immediate.run(
                indicator=target_indicator,
                target_interval=target_interval,
                target_location=target_location['uuid'],
                options=options,
                user=user
            )
            result.append(sub_result)

    elif query_type == "TABLE":
        result = await _handleTableQuery(data, user=user)

    elif query_type == "SERIES_COMPLEX":
        if isinstance(data.get("location"), (string_types,)):
            result = await _series_complex(data, user=user)
        elif isinstance(data.get("location"), (dict,)):
            result = await _series_complex_generator(data, user=user)

    elif query_type == "SLICE_COMPLEX":
        result = await _handle_slice_complex(data, user=user)

    elif query_type == "SLICE":
        result = await _handle_slice(data, user=user)


    elif query_type == "THRESHOLD":
        options = dict(
            start_date=data.get("start_date"),
            end_date=data.get("end_date")
        )

        resultant = await immediate.run(
            indicator=data.get("indicator"),
            target_interval=data.get("interval"),
            target_location=data.get("location"),
            options=options,
            user=user
        )

        result = await _calculate_threshold(resultant, data, user=user)

    return result

async def _handle_series(data, user=None):
    """ Generate a time series data collection
    
    Args:
        data: 
        user: 

    Returns:

    """
    result = None

    location = data.get("location", None)
    indicator = data.get("indicator", None)
    start_date = data.get("start_date", None)
    end_date = data.get("end_date", None)
    fill_missing = data.get("fill_missing", False)
    interval = data.get("interval", None)


    # Do some validation
    if location is None:
        return dict(
            err=True,
            code="LOCATION_MISSING"
        )

    if indicator is None:
        return dict(
            err=True,
            code="INDICATOR_MISSING"
        )

    if start_date is None:
        return dict(
            err=True,
            code="NO_START_DATE"
        )

    if end_date is None:
        return dict(
            err=True,
            code="NO_END_DATE"
        )

    if interval is None:
        return dict(
            err=True,
            code="NO_INTERVAL"
        )

    # Build base query
    query = dict(
        indicator=indicator,
        target_location=location,
        target_interval=interval,
        options=dict(
            start_date=start_date,
            end_date=end_date,
            fill_missing=True
        ),
        user=user
    )

    if isinstance(location, (string_types,)):
        result = await immediate.run(**query)

    if isinstance(location, (list,)):
        result = []
        for target_loc in data.get("target_location"):
            query['target_location'] = target_loc
            sub_res = await immediate.run(**query)
            result.append(sub_res)

    if isinstance(location, (dict,)):
        # Check if it's a group or not
        is_group = location.get("groups", None) is not None

        # If this is a group we need handle this separates
        if is_group:
            sub_results = []

            # If its a output Aggregate merge the series together as a sum
            if location.get("agg", None) == "AGGREGATE":
                locations = await groups.get_grouped_locations(location.get("groups"), user=user)

                series = []
                for loc in locations:
                    query['target_location'] = loc.get("uuid")
                    res = await immediate.run(**query)
                    series.append(res)

                series = aggregate.merge_series([x.get('data') for x in series])
                result = dict(
                    data=series,
                    indicator=indicator,
                    location=location
                )
            # If it's a INDIVIDUAL output, format as a generator
            else:
                locations = await groups.get_grouped_locations(location.get("groups"), collapse=False, user=user)

                series = []
                for loc in locations:
                    query['target_location'] = loc.get("uuid")
                    sub_res = await immediate.run(**query)
                    series.append(sub_res)

                result = dict(
                    data=series
                )

        else:
            result = []
            locations = None
            async with get_db_cur() as cur:
                await cur.execute("""
                        SELECT uuid::TEXT, name, pcode
                        FROM %s.locations
                        WHERE status = %s
                            AND site_type_id = %s
                            AND lineage::TEXT[] @> %s::TEXT[]
                    """, (
                    AsIs(user.get("tki")),
                    location.get("status"),
                    location.get("site_type_id"),
                    [location.get("parent_id")],
                ))
                locations = await cur.fetchall()

            series = []
            for location in locations:
                query['target_location'] = location.get("uuid")
                sub_res = await immediate.run(**query)
                series.append(sub_res)

            result = dict(
                data=series
            )

    return result

def _is_valid_calc(val):
    if val in ('', None):
        return 0

    return val

def _parse_formula_static(formula, data):
    """ Handle parsing a set of scalar values in a formula
    
    Args:
        formula: The formula to parse

    Returns:

    """
    resultants = 0
    var_names = data.keys()

    data_dict = {}
    for var_name, sii in data.items():
        data_series = sii.get("data")

        if isinstance(data_series, (list,)):
            for dp in data_series:
                value = dp[1]
                if value == "" or value is None:
                    value = 0
                try:
                    data_dict[var_name] += float(value)
                except KeyError:
                    data_dict[var_name] = float(value)
        else:
            data_dict[var_name] = float(data_series)

    compiled_formula = formula

    for key, val in data_dict.items():
        real_val = val

        if real_val == 0.0:
            real_val = 0

        reg = '\\b' + key + '\\b'
        compiled_formula = re.sub(reg, str(real_val), compiled_formula, flags=re.IGNORECASE)

    # Default any variables which were not found to 0
    for var_name in var_names:
        reg = '\\b' + var_name + '\\b'
        compiled_formula = re.sub(reg, str(0), compiled_formula, flags=re.IGNORECASE)

    return eval_expr(compiled_formula)


async def _handle_slice_complex(data, user=None):
    """ Querying for data for a complex slice
    
    Args:
        data: 
        user: 

    Returns:

    """
    result = 0

    location = data.get("location", None)
    series = data.get("series", None)
    formula = data.get("formula", None)

    query = dict(
        target_interval=data.get("interval", "DAY"),
        target_location=data.get("location", None),
        indicator=data.get("indicator"),
        options=dict(
            start_date=data.get("start_date", None),
            end_date=data.get("end_date", None),
            reduction=data.get("reduction", "SUM"),
            geometry=data.get("geometry", False)
        ),
        user=user
    )

    if isinstance(location, (string_types,)):
        sub_results = []

        raw_series = dict()
        tmp_loc = None
        for sii in series:
            query['indicator'] = sii.get("indicator", None)
            reduction = sii.get("reduction", "SUM")
            if reduction == "NONE":
                reduction = "SUM"

            query['options']['reduction'] = reduction
            query['options']['geometry'] = data.get("geometry", False)

            tmp_data = await immediate.run(**query)
            tmp_loc = tmp_data.get("location", None)
            raw_series[sii.get("variable_name")] = tmp_data


        result = dict(
            location=tmp_loc,
            data=_parse_formula_static(formula, raw_series)
        )

    if isinstance(location, (dict,)):
        is_group = location.get("groups", None) is not None
        output_series = []

        if is_group:  # Handle location group calculation
            result = []

            if location.get("agg", "AGGREGATE") == "AGGREGATE":
                locations = await groups.get_grouped_locations(location.get("groups"), user=user)

                pre_calc = []

                if len(locations) <= 0:
                    return dict(
                        data=None,
                        location=dict(
                            name=location.get("groups")
                        )
                    )

                for loc in locations:
                    sub_series = dict()
                    for sii in series:
                        reduction = sii.get("reduction", "SUM")
                        if reduction in ("NONE", "", "null", None):
                            reduction = "SUM"

                        query['options']['reduction'] = reduction
                        query['options']['geometry'] = data.get("geometry", False)

                        query['indicator'] = sii.get("indicator", None)
                        query['target_location'] = loc.get("uuid")
                        sub_series[sii.get("variable_name")] = await immediate.run(**query)

                    pre_calc.append((sub_series, loc))

                sub_calc = dict()

                for sii in series:
                    sub_reduction = sii.get("reduction", "SUM")
                    if sub_reduction in ("NONE", "", "null", None):
                        sub_reduction = "SUM"

                    var_name = sii.get("variable_name")
                    if sub_reduction == "SUM":
                        for (sub_s, loc) in pre_calc:
                            try:
                                sub_calc[var_name] += sub_s[var_name]['data'] or 0.0
                            except KeyError:
                                sub_calc[var_name] = sub_s[var_name]['data'] or 0.0
                    elif sub_reduction in ("AVG", "MED", "MID"):
                        sub_calc[var_name] = mean([x.get(var_name, {}).get("data", 0.0) for x, y in pre_calc])

                for key, value in sub_calc.items():
                    if value is None:
                        value = 0

                    if isinstance(value, (string_types,)):
                        try:
                            value = float(value)
                        except ValueError:
                            value = 0


                sub_data = dict((key, dict(data=value)) for key, value in sub_calc.items())
                calc_result = _parse_formula_static(formula, sub_data)

                result = dict(
                    data=calc_result,
                    location=dict(
                        name=location.get("groups")
                    )
                )

            else:
                locations = await groups.get_grouped_locations(location.get("groups"), collapse=False, user=user)

                pre_calc = []
                for loc in locations:
                    sub_series = dict()
                    for sii in series:
                        query['indicator'] = sii.get("indicator", None)
                        query['target_location'] = loc.get("uuid")
                        sub_series[sii.get("variable_name")] = await immediate.run(**query)

                    pre_calc.append((sub_series, loc))

                for (series, loc) in pre_calc:
                    result.append(dict(
                        data=_parse_formula_static(formula, series),
                        location=loc
                    ))

        else:  # Handle a generator
            # Generator
            result = []
            locations = None
            loc_status = location.get('status', 'ACTIVE')
            if loc_status == 'ANY':
                async with get_db_cur() as cur:
                    await cur.execute("""
                                    SELECT uuid::TEXT, name
                                    FROM %s.locations
                                    WHERE (status = 'ACTIVE' OR status = 'INACTIVE')
                                        AND site_type_id = %s
                                        AND lineage::TEXT[] @> %s::TEXT[]
                                """, (
                        AsIs(user.get("tki")),
                        location.get("site_type_id"),
                        [location.get("parent_id")],
                    ))
                    locations = await cur.fetchall()
            else:
                async with get_db_cur() as cur:
                    await cur.execute("""
                            SELECT uuid::TEXT, name
                            FROM %s.locations
                            WHERE status = %s
                                AND site_type_id = %s
                                AND lineage::TEXT[] @> %s::TEXT[]
                        """, (
                        AsIs(user.get("tki")),
                        location.get("status"),
                        location.get("site_type_id"),
                        [location.get("parent_id")],
                    ))
                    locations = await cur.fetchall()

            pre_calc = []
            for loc in locations:
                sub_series = dict()
                tmp_result = None
                for sii in data.get("series", []):
                    query['options']['reduction'] = sii.get('reduction', "SUM")
                    query['indicator'] = sii.get("indicator", None)
                    query['target_location'] = loc.get('uuid')
                    tmp_result = await immediate.run(**query)
                    sub_series[sii.get("variable_name")] = tmp_result

                pre_calc.append((sub_series, tmp_result.get("location")))

            for (series, loc) in pre_calc:
                result.append(dict(
                    data=_parse_formula_static(formula, series),
                    location=loc
                ))

    # Loading root-level geometry for mapping
    if data.get("geometry", False) is True:
        root_geom = None
        centroid = None
        # Get root-level geometry for the account

        root_loc = None
        root_id = None

        if isinstance(location, (string_types,)):
            root_id = result['location']['lineage'][0]

        if isinstance(location, (dict,)):
            if "parent_id" in location.keys():
                root_id = location.get("parent_id", None)
            else:
                root_id = result[0]['location']['lineage'][0]

        if data.get("centroid", False) is True:
            centroid = await Location.centroid(root_id, tki=user.get("tki"))

        root_geom = await Location.geometry(root_id, tki=user.get("tki"))

        result = dict(
            d=result,
            g=root_geom,
            c=centroid
        )

    return result


async def _handle_slice(data, user=None):
    """ Handle a slice query
    
    Args:
        data: 
        user: 

    Returns:

    """
    result = 0

    location = data.get("location", None)

    query = dict(
        target_interval=data.get("interval", "DAY"),
        target_location=data.get("location", None),
        indicator=data.get("indicator"),
        options=dict(
            start_date=data.get("start_date", None),
            end_date=data.get("end_date", None),
            reduction=data.get("reduction", None),
            geometry=data.get("geometry", False)
        ),
        user=user
    )

    # Straight Shot query
    if isinstance(location, (string_types,)):
        raw_series = await immediate.run(**query)

        if data.get("meta", True) == True:
            result = dict(
                data=raw_series.get('data'),
                location=raw_series.get("location"),
                indicator=raw_series.get("indicator")
            )
        else:
            result = dict(data=raw_series.get("data"))

    elif isinstance(location, (dict,)):
        is_group = location.get("groups", None) is not None

        if is_group:
            result = []

            if location.get("agg", "AGGREGATE") == "AGGREGATE":
                locations = await groups.get_grouped_locations(location.get("groups", None), user=user)

                for loc in locations:
                    query['target_location'] = loc.get("uuid")
                    sub_res = await immediate.run(**query)
                    result.append(sub_res)

                for res in result:
                    if res.get("data", None) is not None:
                        if data.get("reduction", "SUM") == "SUM":
                            if res.get('data', []) == []:
                                res['data'] = 0

                result = dict(
                    data=aggregate.merge_statics(result),
                    location=dict(
                        name=", ".join(location.get("groups", None))
                    )
                )
            else:
                locations = await groups.get_grouped_locations(location.get("groups", None), collapse=False, user=user)

                for loc in locations:
                    query['target_location'] = loc.get("uuid")
                    sub_res = await immediate.run(**query)
                    result.append(sub_res)

                for res in result:
                    if res.get("data", None) is not None:
                        if data.get("reduction", "SUM") == "SUM":
                            if res.get('data', []) == []:
                                res['data'] = 0


        else:
            locations = None

            # Set up any filtering
            status_filter = ""
            if location.get('status') != "ANY":
                status_filter = "AND status = '%s'" % (location.get("status"))

            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid::TEXT, name 
                    FROM %s.locations 
                    WHERE site_type_id = %s 
                      AND lineage::TEXT[] @> %s::TEXT[]
                      %s;
                """, (
                    AsIs(user.get("tki")),
                    location.get("site_type_id"),
                    [location.get("parent_id", None)],
                    AsIs(status_filter),
                ))
                locations = await cur.fetchall()

            result = []

            for loc in locations:
                query['target_location'] = loc.get("uuid")
                sub_res = await immediate.run(**query)
                result.append(sub_res)

            for res in result:
                if res.get("data", None) is not None:
                    if data.get("reduction", "SUM") == "SUM":
                        if res.get('data', []) == []:
                            res['data'] = 0

    # Loading root-level geometry for mapping
    if data.get("geometry", False) is True:
        root_geom = None
        centroid = None
        # Get root-level geometry for the account

        root_loc = None
        root_id = None

        if isinstance(location, (string_types,)):
            root_id = result['location']['lineage'][0]

        if isinstance(location, (dict,)):
            if "parent_id" in location.keys():
                root_id = location.get("parent_id", None)
            else:
                root_id = result[0]['location']['lineage'][0]

        if data.get("centroid", False) is True:
            centroid = await Location.centroid(root_id, tki=user.get("tki"))

        root_geom = await Location.geometry(root_id, tki=user.get("tki"))

        result = dict(
            d=result,
            g=root_geom,
            c=centroid
        )

    return result





async def _handleTableQuery(query, user=None):
    """ Handles processing a complex table query
    :param query: {dict} THe definition of the table
    :param user: {dict} The calling user
    :return:
    """

    results = {}

    locations = []

    location = query.get("location")

    if isinstance(location, (string_types,)):
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT, name, lineage, pcode
                FROM %s.locations
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                query.get("location"),
            ))
            locations = await cur.fetchall()
    elif isinstance(location, (dict,)):
        is_group = location.get("groups", None) is not None

        if is_group:
            locations = await groups.get_grouped_locations(location.get("groups", []), collapse=False, user=user)
        else:
            loc_status = query.get("location", {}).get("status", "ACTIVE")
            if loc_status == "ANY":
                loc_status = "ACTIVE"

            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT l.uuid::TEXT, l.name, l.lineage, p.name->>'en' AS parent_name, pcode
                    FROM %s.locations AS l 
                      LEFT JOIN %s.locations AS p ON p.uuid::TEXT = l.parent_id::TEXT
                    WHERE l.site_type_id = %s
                        AND l.lineage::TEXT[] @> %s::TEXT[]
                        AND l.status = %s
                    ORDER BY l.name->>'en' ASC
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    query['location']['site_type_id'],
                    [query['location']['parent_id']],
                    loc_status,
                ))
                locations = await cur.fetchall()

    for loc in locations:
        row = dict(
            location=loc,
            data={}
        )
        for col in query.get("columns"):
            st = time.time()
            # BUild the queries for each item
            col['location'] = loc.get("uuid")
            data = await analysis_parser(col, user=user)

            if data is not None:
                if isinstance(data, dict):
                    row['data'][col.get("uuid")] = data.get("data")
                else:
                    row['data'][col.get('uuid')] = data
            else:
                row['data'][col.get("uuid")] = 0

        results[loc.get("uuid")] = row

    return results



async def _series_complex(data, user=None):
    series = data.get("series")
    raw_series = dict()
    end_data = {}

    options = dict()
    options['start_date'] = data.get("start_date")
    options['end_date'] = data.get("end_date")
    options['fill_missing'] = True

    var_names = [x.get("variable_name") for x in series]

    for sii in data.get("series", []):
        target_indicator = sii.get("indicator")
        target_location = data.get("location")
        target_interval = data.get("interval", "NONE")
        raw_series[sii.get("variable_name")] = await immediate.run(
            indicator=target_indicator,
            target_interval=target_interval,
            target_location=target_location,
            options=options,
            user=user
        )

    formula = data.get("formula")

    resultants = []

    for var_name, sii in raw_series.items():
        data_series = sii.get("data")

        for dp in data_series:
            string_date = dp[0]
            if string_date not in end_data.keys():
                end_data[string_date] = dict()

            value = 0
            try:
                value = float(dp[1])
            except:
                pass

            end_data[string_date][var_name] = value

    for data_date, vars in end_data.items():
        compiled_formula = formula

        for key, value in vars.items():
            real_val = 0
            if value is not None:
                real_val = value

            reg = '\\b' + key + '\\b'
            compiled_formula = re.sub(reg, str(real_val), compiled_formula, flags=re.IGNORECASE)

        # Default any variables which were not found to 0
        for var_name in var_names:
            reg = '\\b' + var_name + '\\b'
            compiled_formula = re.sub(reg, str(0), compiled_formula, flags=re.IGNORECASE)

        resultants.append([data_date, eval_expr(compiled_formula)])

    data['data'] = resultants

    return data


async def _series_complex_generator(data, user=None):
    data['data'] = []
    result = []
    locations = []

    location = data.get("location", None)

    query = dict(
        target_location=data.get("location", None),
        target_interval=data.get("interval", None),
        indicator=data.get("indicator", None),
        options=dict(
            start_date=data.get("start_date", None),
            end_date=data.get("end_date", None),
            geometry=data.get("geometry", False),
            fill_missing=data.get("fill_missing", False),
            centroid=data.get("centroid", False)
        ),
        user=user
    )

    is_group = location.get("groups", None) is not None
    formula = data.get("formula")

    if is_group:
        locations = None
        if location.get("agg", "AGGREGATE") == "AGGREGATE":
            locations = await groups.get_grouped_locations(location.get("groups"), user=user)

            series = data.get("series")
            var_names = [x.get("variable_name") for x in series]

            pre_merge = []

            for loc in locations:
                raw_series = dict()
                end_data = dict()

                for sii in series:
                    query['target_location'] = loc.get("uuid")
                    query['indicator'] = sii.get("indicator")
                    raw_series[sii.get("variable_name")] = await immediate.run(**query)

                resultants = []

                data_series = None
                for var_name, sii in raw_series.items():
                    data_series = sii.get("data")

                for dp in data_series:
                    string_date = dp[0]
                    if string_date not in end_data.keys():
                        end_data[string_date] = dict()

                    value = 0
                    try:
                        value = float(dp[1])
                    except ValueError:
                        pass

                    end_data[string_date][var_name] = value

                for data_date, vars in end_data.items():
                    compiled_formula = formula

                    for key, value in vars.items():
                        real_val = 0
                        if value is not None:
                            real_val = value

                        reg = '\\b' + key + '\\b'
                        compiled_formula = re.sub(reg, str(real_val), compiled_formula, flags=re.IGNORECASE)

                    # Default any variables which were not found to 0
                    for var_name in var_names:
                        reg = '\\b' + var_name + '\\b'
                        compiled_formula = re.sub(reg, str(0), compiled_formula, flags=re.IGNORECASE)

                    resultants.append([data_date, eval_expr(compiled_formula)])

                pre_merge.append(resultants)

            agged = aggregate.merge_series(pre_merge)

            result = dict(
                data=agged,
                location=dict(
                    name=", ".join(location.get("groups"))
                ),
                indicator=dict(
                    name="Complex",
                    formula=formula
                )
            )


        else:
            locations = await groups.get_grouped_locations(location.get("groups"), collapse=False, user=user)

            series = data.get("series")
            var_names = [x.get("variable_name") for x in series]

            pre_merge = []

            for loc in locations:
                raw_series = dict()
                end_data = dict()

                for sii in series:
                    query['target_location'] = loc.get("uuid")
                    query['indicator'] = sii.get("indicator")
                    raw_series[sii.get("variable_name")] = await immediate.run(**query)

                resultants = []

                data_series = None
                for var_name, sii in raw_series.items():
                    data_series = sii.get("data")

                for dp in data_series:
                    string_date = dp[0]
                    if string_date not in end_data.keys():
                        end_data[string_date] = dict()

                    value = 0
                    try:
                        value = float(dp[1])
                    except ValueError:
                        pass

                    end_data[string_date][var_name] = value

                for data_date, vars in end_data.items():
                    compiled_formula = formula

                    for key, value in vars.items():
                        real_val = 0
                        if value is not None:
                            real_val = value

                        reg = '\\b' + key + '\\b'
                        compiled_formula = re.sub(reg, str(real_val), compiled_formula, flags=re.IGNORECASE)

                    # Default any variables which were not found to 0
                    for var_name in var_names:
                        reg = '\\b' + var_name + '\\b'
                        compiled_formula = re.sub(reg, str(0), compiled_formula, flags=re.IGNORECASE)

                    resultants.append([data_date, eval_expr(compiled_formula)])

                pre_merge.append(dict(
                    data=resultants,
                    location=loc,
                    indicator=dict(
                        name="Complex",
                        formula=formula
                    )
                ))

            result = dict(
                data=pre_merge
            )

    else:
        locations = None

        status_filter = ""
        if location.get("status", "ANY") != "ANY":
            status_filter = "AND status = '%s'" % (location.get("status"),)

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT, name, lineage
                FROM %s.locations 
                WHERE site_type_id = %s 
                  AND lineage::TEXT[] @> %s::TEXT[]
                  %s;
            """, (
                AsIs(user.get("tki")),
                location.get("site_type_id"),
                [location.get("parent_id", None)],
                AsIs(status_filter),
            ))
            locations = await cur.fetchall()

        series = data.get("series")

        var_names = [x.get("variable_name") for x in series]

        # Get data for each variable for each location
        for loc in locations:
            raw_series = dict()
            end_data = dict()

            for sii in data.get("series", []):
                query['indicator'] = sii.get("indicator")
                query['target_location'] = loc.get("uuid")
                raw_series[sii.get("variable_name")] = await immediate.run(**query)

            resultants = []

            for var_name, sii in raw_series.items():
                data_series = sii.get("data")

                for dp in data_series:
                    string_date = dp[0]
                    if string_date not in end_data.keys():
                        end_data[string_date] = dict()

                    value = 0
                    try:
                        value = float(dp[1])
                    except ValueError:
                        pass

                    end_data[string_date][var_name] = value

            for data_date, vars in end_data.items():
                compiled_formula = formula

                for key, value in vars.items():
                    real_val = 0
                    if value is not None:
                        real_val = value

                    reg = '\\b' + key + '\\b'
                    compiled_formula = re.sub(reg, str(real_val), compiled_formula, flags=re.IGNORECASE)

                # Default any variables which were not found to 0
                for var_name in var_names:
                    reg = '\\b' + var_name + '\\b'
                    compiled_formula = re.sub(reg, str(0), compiled_formula, flags=re.IGNORECASE)

                resultants.append([data_date, eval_expr(compiled_formula)])

            result.append(dict(
                data=resultants,
                location=loc,
                indicator=None
            ))

        result = dict(
            data=result
        )

    return result

#
# def eval_(node):
#     if isinstance(node, ast.Num):  # <number>
#         return node.n
#     elif isinstance(node, ast.BinOp):  # <left> <operator> <right>
#         return operators[type(node.op)](eval_(node.left), eval_(node.right))
#     elif isinstance(node, ast.UnaryOp):  # <operator> <operand> e.g., -1
#         return operators[type(node.op)](eval_(node.operand))
#     elif isinstance(node, ast.IfExp):
#         return operators[type(node.op)](eval_(node.left), eval_(node.right))
#     elif isinstance(node, ast.If):
#         return operators[type(node.op)](eval_(node.))
#     else:
#         raise TypeError(node)


def eval_expr(expr):
    result = 0
    try:
        result = eval(compile(ast.parse('(%s)' % expr, mode='eval'), '', mode='eval'))
    except Exception as e:
        print(e)
        result = 0

    return result


async def _calculate_threshold(resultant, data, user=None):
    return []
