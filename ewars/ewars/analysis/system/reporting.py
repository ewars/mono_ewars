import time
import functools

from ewars.db import get_db_cur, get_db_cursor

from ewars.constants import CONSTANTS

from ewars.analysis.aggregate import aggregate
from ewars.utils import date_utils
from ewars.analysis.auditing import get_timeliness
from ewars.models import Location
from ewars.analysis.system import completeness

from psycopg2.extensions import AsIs

import asyncio
import functools
from collections import OrderedDict


def async_lru(size=100):
    cache = OrderedDict()

    def decorator(fn):
        @functools.wraps(fn)
        async def memoizer(*args, **kwargs):
            key = str((args, kwargs))
            try:
                cache[key] = cache.pop(key)
            except KeyError:
                if len(cache) >= size:
                    cache.popitem(last=False)
                cache[key] = await fn(*args, **kwargs)
            return cache[key]

        return memoizer

    return decorator


def ResultIter(cursor, arraysize=1000):
    while True:
        results = cursor.fetchmany(arraysize)

        if not results:
            break

        for result in results:
            yield result


async def _get_site_types(form_id, tki=None):
    results = []
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT site_types
            FROM %s.v_sti_collections
            WHERE form_id = %s;
        """, (
            AsIs(tki),
            form_id,
        ))
        results = await cur.fetchone()

    if results is None:
        return []
    else:
        return results.get("site_types", [])


def ResultIter(cursor, arraysize=1000):
    while True:
        results = cursor.fetchmany(arraysize)

        if not results:
            break

        for result in results:
            yield result


async def get_report_submissions(form_id, target_interval, start_date, end_date, target_location, source=None,
                                 tki=None):
    """ Retrieve report submissions from the system for analysis

    Args:
        form_id:
        target_interval:
        start_date:
        end_date:
        target_location:
        source:
        user:

    Returns:

    """
    reports = []

    form_param = "WHERE r.form_id = %s"
    form_value = form_id
    form = None
    site_type_id = None
    site_types = None

    if form_id:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, features
                FROM %s.forms 
                WHERE id = %s;
            """, (
                AsIs(tki),
                form_id,
            ))
            form = await cur.fetchone()

    site_types = await _get_site_types(form_id, tki=tki)

    site_types = [str(x) for x in site_types if x is not None]

    site_type_id = form.get("features", {}).get("LOCATION_REPORTING", {}).get("site_type_id", None)

    params = []

    selector = ""
    joiner = ""

    if target_interval == 'WEEK':
        selector = "(date_trunc('week', d) + '6 days'::INTERVAL)::DATE as data_date,"
        joiner = "BETWEEN date_trunc('week', d) AND (date_trunc('week', d) + '6 days'::INTERVAL)::DATE"
    elif target_interval == 'DAY':
        selector = "d::DATE as data_date,"
        joiner = "= d::DATE"
    elif target_interval == 'MONTH':
        selector = "SELECT (date_trunc('month', d::DATE) + '1 month'::INTERVAL - '1 day'::INTERVAL)::DATE"
        joiner = "BETWEEN d::DATE AND (date_trunc('month', d::DATE) + '1 month'::INTERVAL - '1 day'::INTERVAL)::DATE"
    elif target_interval == 'YEAR':
        selector = "(date_trunc('month', d::DATE) + '1 year'::INTERVAL - '1 day'::INTERVAL)::DATE"
        joiner = "BETWEEN d::DATE AND (date_trunc('month', d::DATE) + '1 year'::INTERVAL - '1 day'::INTERVAL)::DATE"

    sql = """
        SELECT
          {SELECTOR}
          COALESCE(SUM(c.count), 0) AS counter
        FROM %s.collection_counts AS c
          JOIN generate_series('{DATE_START}'::TIMESTAMP, '{DATE_END}', '1 {INTERVAL}') AS d ON c.data_date {CORRELATE}
          LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
        WHERE {PARAM}
          AND l.lineage::TEXT[] @> %s::TEXT[]
        GROUP BY d;
    """

    sql = sql.replace('{SELECTOR}', selector)
    sql = sql.replace('{CORRELATE}', joiner)

    # sql = """
    #     SELECT
    #           d::DATE AS data_date,
    #           COALESCE(SUM(cc.count), 0) AS counter
    #     FROM generate_series('{DATE_START}' :: TIMESTAMP, '{DATE_END}', '1 {INTERVAL}') AS d
    #       LEFT JOIN (
    #           SELECT c.data_date, c.count
    #           FROM %s.collection_counts AS c
    #             LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
    #           WHERE {PARAM}
    #               AND l.lineage::TEXT[] @> %s::TEXT[]
    #      ) AS cc ON cc.data_date BETWEEN date_trunc('{INTERVAL}', d) AND d::DATE
    #     GROUP BY d;
    # """

    if form_id is not None:
        params.append("c.form_id = %s" % (form_id))
        params.append("l.site_type_id IN (%s)" % (",".join(site_types)))

    if source is not None:
        if source == "MOBILE":
            source = "ANDROID"
        params.append("c.source = '%s'" % (source))
        # Need to change how we query in this situation
        sql = """
                SELECT
                    d::DATE AS data_date,
                    COALESCE(COUNT(cc.uuid), 0) AS counter
                FROM generate_series('{DATE_START}'::TIMESTAMP, '{DATE_END}', '1 {INTERVAL}') AS d 
                  LEFT JOIN (
                    SELECT c.data_date, c.uuid
                      FROM %s.collections AS c 
                        LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                      WHERE {PARAM}
                        AND (c.status = 'SUBMITTED' OR c.status = 'PENDING_AMENDMENT')
                        AND l.lineage::TEXT[] @> %s::TEXT[]
                  ) AS cc on cc.data_date BETWEEN date_trunc('{INTERVAL}', d) AND d::DATE
                GROUP BY d;
            """

    if len(params) > 0:
        params = " AND ".join(params)

    sql = sql.replace('{DATE_END}', end_date.strftime('%Y-%m-%d'))
    sql = sql.replace('{DATE_START}', start_date.strftime('%Y-%m-%d'))
    sql = sql.replace('{INTERVAL}', target_interval)

    # sql = """
    #     SELECT r.data_date, COUNT(r.uuid) AS counter
    #         FROM %s.collections AS r
    #         LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
    #         WHERE {PARAM}
    #             AND r.data_date >= %s
    #             AND r.data_date <= %s
    #             AND (r.status = 'SUBMITTED' OR r.status = 'PENDING_AMENDMENT')
    #             AND l.lineage::TEXT[] @> %s::TEXT[]
    #         GROUP BY r.data_date;
    # """

    tuples = (
        AsIs(tki),
        AsIs(tki),
        [target_location.get("uuid")],
    )

    if len(params) > 0:
        sql = sql.replace("{PARAM}", params)
    else:
        sql = sql.replace("{PARAM}", "")

    reports = []
    with get_db_cursor() as cur:
        cur.execute(sql, tuples)
        reports = cur.fetchall()

    reports = [[x.get("data_date"), x.get("counter")] for x in reports]
    return sorted(reports, key=lambda k: k[0])


async def get_report_timeliness(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    # Returns the form [date, {late: ..., submission_date: [], on_time: 0, missing: 0, expected: 0, submitted: 0}]
    # Need to parse that into a calculation
    data = await get_timeliness(target_location.get("uuid"), form_id, start_date, end_date, user=user)

    # Turn the string dates back into proper dates
    for item in data:
        item[0] = date_utils.parse_date(item[0])

    # We can't use the general aggregate function for this as the result is a complex
    # so we have to do our own aggregation
    # using the form of a tuple (date, on_time, received)

    # Create a range set based on the target interval
    ranger = date_utils.get_range_set(start_date, end_date, target_interval, default_value=[0, 0])

    for span in ranger:
        for item in data:
            if item[0] >= span.get("x") and item[0] <= span.get("y"):
                try:
                    span['z'][0] += item[1].get("on_time", 0)
                    span['z'][1] += item[1].get("expected", 0)
                except ValueError:
                    span['z'][0] = item[1].get("on_time", 0)
                    span['z'][0] = item[1].get("expected", 0)

    series = [[x.get("y"), (x['z'][0] / x['z'][1])] for x in ranger]

    return series


async def get_report_completeness(indicator, target_interval, start_date, end_date, target_location, options,
                                  user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    data = await completeness.get_completeness(
        target_location.get("uuid"),
        start_date,
        end_date,
        form_id=form_id,
        user=user)

    # convert string formatted date to date object
    for item in data:
        item[0] = date_utils.parse_date(item[0])

    ranger = date_utils.get_range_set(start_date, end_date, target_interval, default_value=[0, 0])

    for span in ranger:
        for item in data:
            if item[0] >= span.get("x") and item[0] <= span.get("y"):
                try:
                    span['z'][0] += item[1].get("submitted")
                    span['z'][1] += item[1].get("expected")
                except ValueError:
                    span['z'][0] = item[1].get("submitted")
                    span['z'][1] = item[1].get("expected")

    series = [[x.get('y'), (span['z'][0] / span['z'][1])] for x in ranger]

    return series


async def get_reports_expected(form_id, target_interval, start_date, end_date, target_location, options, user=None):
    """ Calculate completeness for a given location
 
     Args:
         location_id:
         start_date:
         end_date:
         form_id:
         reduction:
         user:
 
     Returns:
 
     """
    results = None
    forms = None
    interval = "DAY"

    if form_id is not None:
        async with get_db_cur() as cur:
            await cur.execute("""
                        SELECT id, features
                        FROM %s.forms
                        WHERE id = %s
                    """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            res = await cur.fetchone()
            forms = [res]
    else:
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, features
                FROM %s.forms
                WHERE status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
            ))
            forms = await cur.fetchall()

    timeline = date_utils.get_date_range(
        start_date,
        end_date,
        "DAY"
    )

    # Root timeline
    pre_dict = dict((x, dict(e=0, s=0)) for x in timeline)

    loc_sets = []

    for form in forms:
        site_type_id = None
        sub_interval = None

        # Get the interval for the reporting
        if 'INTERVAL_REPORTING' in form.get('features').keys():
            sub_interval = form['features']["INTERVAL_REPORTING"].get("interval", None)
            if sub_interval is not None:
                interval = sub_interval

        # Get the location information for the reporting
        if "LOCATION_REPORTING" in form.get('features').keys():
            site_type_id = form['features']['LOCATION_REPORTING'].get("site_type_id", None)

        # Check if there's an interval, if not, this isn't a report that has interval-based reporting
        if sub_interval is not None:
            sub_time = date_utils.get_date_range(
                start_date,
                end_date,
                sub_interval
            )
            # date: (loc_uuids, expected, submitted)
            sub_dict = dict((x, [[], 0, 0]) for x in sub_time)

            rps = await Location.get_rps_locale(
                target_location.get("uuid"),
                site_type_id,
                form.get('id'),
                user=user
            )

            for rp in rps:
                for ts, dt in sub_dict.items():
                    if rp.get('start_date') <= ts <= rp.get("end_date"):
                        dt[0].append(rp.get("uuid"))

            for x, dt in sub_dict.items():
                if x in pre_dict.keys():
                    pre_dict[x]['e'] += len(list(set(dt[0])))

            loc_sets.append(sub_dict)

    raw_data = [[x, y.get("e", 0)] for x, y in pre_dict.items()]
    result = aggregate(target_interval, start_date, end_date, raw_data)

    return result


async def get_report_source_import(indicator, target_interval, start_date, end_date, target_location, options,
                                   user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT c.data_date
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.source = 'IMPORT'
                AND c.form_id = %s
                AND c.data_date >= %s
                AND c.data_date <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            start_date,
            end_date,
            [target_location.get("uuid")],
        ))
        reports = await cur.fetchall()

    series = [[x.get("data_date"), 1] for x in reports]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


async def get_report_source_mobile(indicator, target_interval, start_date, end_date, target_location, options,
                                   user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT c.data_date
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.source IN ('ANDROID','IOS')
                AND c.form_id = %s
                AND c.data_date >= %s
                AND c.data_date <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            start_date,
            end_date,
            [target_location.get("uuid")],
        ))
        reports = await cur.fetchall()

    series = [[x.get("data_date"), 1] for x in reports]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


async def get_report_source_web(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT c.data_date
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.source = 'SYSTEM'
                AND c.form_id = %s
                AND c.data_date >= %s
                AND c.data_date <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            start_date,
            end_date,
            [target_location.get("uuid")],
        ))
        reports = await cur.fetchall()

    series = [[x.get("data_date"), 1] for x in reports]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


async def get_reports_late(form_id, target_interval, start_date, end_date, target_location, options, user=None):
    if form_id is None:
        return []

    form = None
    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, features
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        form = await cur.fetchone()

        await cur.execute("""
            SELECT c.data_date, c.submitted_date::DATE
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND c.data_date >= %s
                AND c.data_date <= %s
                AND (c.status = 'SUBMITTED' OR c.status = 'PENDING_AMENDMENT')
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        reports = await cur.fetchall()

    # iterate through the reports and create a resultant series
    series = []

    features = form.get("features")
    overdue_threshold = features['OVERDUE']['interval']
    overdue_interval = features['OVERDUE']['threshold']
    late_delta = date_utils.get_delta(overdue_interval, overdue_threshold)

    for report in reports:
        if report.get("submitted_date") > report.get("data_date") + late_delta:
            series.append([report.get("data_date"), 1])

    results = aggregate(target_interval, start_date, end_date, series)

    return results


async def get_reports_timely(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    form = None
    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, time_interval, overdue_threshold, overdue_interval
            FROM %s.forms
            WHERE id = %s
        """, (
            AsIs(user.get('tki')),
            form_id,
        ))
        form = await cur.fetchone()

        await cur.execute("""
            SELECT c.data_date, c.submitted_date
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.form_id = %s
                AND (c.status = 'SUBMITTED' OR c.status = 'PENDING_AMENDMENT')
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND c.data_date >= %s
                AND c.data_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        reports = await cur.fetchall()

    # iterate through the reports and create a resultant series
    series = []

    late_delta = date_utils.get_delta(form.get("overdue_threshold"), form.get("overdue_interval"))

    for report in reports:
        if report.get("submitted_date") <= report.get("data_date") + late_delta:
            series.append([report.get("data_date"), 1])

    results = aggregate(target_interval, start_date, end_date, series)

    return results


async def get_reports_submitted_by_admin(indicator, target_interval, start_date, end_date, target_location, options,
                                         user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT c.data_date
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                LEFT JOIN _iw.users AS u ON u.id = c.created_by
            WHERE c.form_id = %s
                AND c.data_date >= %s
                AND c.data_date <= %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND u.user_type IN ('SUPER_ADMIN','GLOBAL_ADMIN','ACCOUNT_ADMIN','INSTANCE_ADMIN')
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            start_date,
            end_date,
            [target_location.get("uuid")],
        ))
        reports = await cur.fetchall()

    series = [[x.get("data_date"), 1] for x in reports]
    results = aggregate(target_interval, start_date, end_date, series)

    return results


async def get_report_source_other(indicator, target_interval, start_date, end_date, target_location, options,
                                  user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT c.data_date
            FROM %s.collections AS c
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.form_id = %s
                AND (c.status = 'SUBMITTED' OR c.status = 'PENDING_AMENDMENT')
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND c.data_date <= %s
                AND c.data_date >= %s
                AND c.source NOT IN %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
            ("SYSTEM", "MOBILE"),
        ))
        reports = await cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("data_date"), 1] for x in reports])


async def get_report_retractions(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    retractions = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT r.retracted_date
            FROM %s.retractions AS r
                LEFT JOIN %s.collections AS c ON c.uuid = r.report_id
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.form_id = %s
                AND (c.status = 'SUBMITTED' OR c.status = 'PENDING_AMENDMENT')
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND r.retracted_date >= %s
                AND r.retracted_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        retractions = await cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("retraction_date"), 1] for x in retractions])


async def get_report_amendments(indicator, target_interval, start_date, end_date, target_location, options, user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    amendments = []

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT a.amended_date
            FROM %s.amendments AS a
                LEFT JOIN %s.collections AS c ON c.uuid = a.report_id
                LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
            WHERE c.form_id = %s
                AND l.lineage::TEXT[] @> %s::TEXT[]
                AND a.amended_date >= %s
                AND a.amended_date <= %s
        """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            [target_location.get("uuid")],
            start_date,
            end_date,
        ))
        amendments = await cur.fetchall()

    return aggregate(target_interval, start_date, end_date, [[x.get("amendment_date"), 1] for x in amendments])


async def get_reports_submitted_by_user(indicator, target_interval, start_date, end_date, target_location, options,
                                        user=None):
    form_id = indicator.get("form_id", None)

    if form_id is None:
        return []

    reports = []

    async with get_db_cur() as cur:
        await cur.execute("""
                SELECT c.data_date
                FROM %s.collections AS c
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                    LEFT JOIN _iw.users AS u ON u.id = c.created_by
                WHERE c.form_id = %s
                    AND (c.status = 'SUBMITTED' OR c.status = 'PENDING_AMENDNMENT')
                    AND c.data_date >= %s
                    AND c.data_date <= %s
                    AND l.lineage::TEXT[] @> %s::TEXT[]
                    AND u.user_type = 'USER'
            """, (
            AsIs(user.get("tki")),
            AsIs(user.get("tki")),
            form_id,
            start_date,
            end_date,
            [target_location.get("uuid")],
        ))
        reports = await cur.fetchall()

    series = [[x.get("data_date"), 1] for x in reports]
    results = aggregate(target_interval, start_date, end_date, series)

    return results
