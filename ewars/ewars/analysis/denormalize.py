from ewars.db import get_db_cursor

def _evaluate(data, circuit):
    """ Evaluate a circuit against a report

    Args:
        report:
        circuit:

    Returns:

    """
    reduction = circuit.get("r")

    values = []

    print(circuit)

    for n in circuit.get("n"):
        if isinstance(n, (string_types,)):
            if ":" in n:
                print(n)
                field = n.split(":")[0]
                cmp = n.split(":")[1].upper()
                val = n.split(":")[2]

                alias = field.replace("data.", "")
                actual_value = data.get(alias, None)

                if actual_value is not None:
                    if cmp == "EQ":
                        if val == actual_value:
                            values.append(1)
                    elif cmp == "NEQ":
                        if val != actual_value:
                            values.append(1)
                    elif cmp == "GTE":
                        if actual_value >= val:
                            values.append(1)
                    elif cmp == "GT":
                        if actual_value > val:
                            values.append(1)
                    elif cmp == "LT":
                        if actual_value < val:
                            values.append(1)
                    elif cmp == "LTE":
                        if actual_value <= val:
                            values.append(1)


            else:
                field = n.replace("data.", "")
                values.append(data.get(field, None))
        else:
            # This is a complex, we need to evaluate to a binary value
            c_values = []
            for item in n[1:]:
                if isinstance(item, (string_types,)):
                    setter = item.split(":")
                    field = setter[0]
                    cmp = setter[1].upper()
                    val = setter[2]

                    alias = field.replace("data.", "")
                    actual_value = data.get(alias, None)

                    res = None
                    if actual_value is not None:
                        if cmp == "EQ":
                            res = val == actual_value
                        elif cmp == "NEQ":
                            res = val != actual_value
                        elif cmp == "GTE":
                            res = float(val) >= float(actual_value)
                        elif cmp == "GT":
                            res = float(val) > float(actual_value)
                        elif cmp == "LT":
                            res = float(val) < float(actual_value)
                        elif cmp == "LTE":
                            res = float(val) <= float(actual_value)

                    c_values.append(res)

            if n[0] == "ALL":
                if all(x == True for x in c_values):
                    values.append(1)
            else:
                if True in c_values:
                    values.append(1)

    values = [float(x) for x in values if isOk(x)]

    if reduction == "SUM":
        return sum(values)


def isOk(value):
    result = True

    if value is None:
        result = False

    if value == "":
        result = False

    if value == "null":
        result = False

    return result

def denormalize(report, user=None):
    """ Denormalize a submitted forms data into the denormalization table

    Args:
        report: The submission to denorm
        user: The calling user

    Returns:

    """
    form = None

    with get_db_cursor() as cur:
        cur.execute("""
            SELECT uuid, etl
            FROM ewars.form_versions
            WHERE uuid = %s
        """, (
            report.get("version_id"),
        ))
        form = cur.fetchone()

    etl = form.get("etl")

    submissions = []
    insertions = []

    report_date = report.get('data')
    for ind, logic in etl.items():
        submissions.append((report.get("data_date"), report.get("location_id"), None))

        result = _evaluate(report_date, logic)
        insertions.append(tuple([
            ind,
            report.get("location_id", None),
            result,
            report.get("data_date").strftime("%Y-%m-%d"),
            report.get("uuid", None),
        ]))

    if len(insertions) > 0:
        with get_db_cursor(commit=True) as cur:
            args_str = b",".join(cur.mogrify(b"%s, %s, %s, %s, %s)", x) for x in insertions)
            cur.execute(b"INSERT INTO ewars.denorm VALUES " + args_str)



