import datetime
import calendar
import time

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

METHODS = {

}

# Simple directory of tables to set up for the remote
LOADERS = dict(
    forms="SELECT * FROM %s.forms",
    form_versions="SELECT * FROM %s.form_versions;",
    location_types="SELECT * FROM %s.location_types;",
    indicators="SELECT * FROM %s.indicators;",
    indicator_groups="SELECT * FROM %s.indicator_groups;",
    alarms="SELECT * FROM %s.alarms_v2;",
    workflows="SELECT * FROM %s.workflows",
    notebooks="SELECT * FROM %s.notebooks",
    tasks="SELECT * FROM %s.tasks_v2 WHERE state = 'OPEN'",
    huds="SELECT * FROM %s.huds;",
    maps="SELECT * FROM %s.maps;",
    devices="SELECT * FROM %s.devices;",
    assignments="SELECT * FROM %s.assignments;",
    layouts="SELECT * FROM %s.layouts;",
    organizations="SELECT * FROM %s.orgs_full",
    channels="SELECT * FROM %s.channels"
)


async def get_base(data, user=None):
    """ Get the base setup data for remotely connected client
    application

    Args:
        data: dictionary of data to send down to the client
        user: The calling user

    Returns:

    """
    payload = dict()
    permissions = []
    user_details = None
    locations = []
    perm_manifest = {}

    async with get_db_cur() as cur:
        for key, query in LOADERS.items():
            await cur.execute(
                query,
                (
                    AsIs(user.get("tki")),
                )
            )
            items = await cur.fetchall()
            payload[key] = [dict(x) for x in items]

        await cur.execute("""
            SELECT l.uuid, l.name, l.site_type_id, l.lineage, l.parent_id, l.status, l.groups,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.location_reporting AS t WHERE t.location_id = l.uuid) AS reporting
            FROM %s.locations AS l;
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
        ))
        locations = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.users
            WHERE id = %s;
        """, (
            AsIs(user.get("tki")),
            user.get('id'),
        ))
        user_details = await cur.fetchone()

        await cur.execute("""
            SELECT * FROM %s.assignments
            WHERE user_id = %s;
        """, (
            AsIs(user.get('tki')),
            user.get('id'),
        ))
        permissions = await cur.fetchall()

        for perm in permissions:
            is_interval = False
            is_location = False
            for form in payload['forms']:
                if form.get('id') == perm.get('form_id'):
                    if form.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
                        is_interval = True

                    if 'LOCATION_REPORTING' in form.get('features', {}).keys():
                        is_location = True

            topic_key = str(perm.get('form_id'))
            if is_location:
                topic_key += '_' + str(perm.get('location_id'))

            if is_interval and is_location:
                await cur.execute("""
                    SELECT DISTINCT(data_date) AS dt
                    FROM %s.collections
                    WHERE form_id = %s 
                        AND status != 'DRAFT'
                        AND status != 'DELETED'
                        AND location_id = %s;
                """, (
                    AsIs(user.get('tki')),
                    perm.get('form_id'),
                    perm.get('location_id'),
                ))
                topic_manifest = await cur.fetchall()
                topic_manifest = [x.get('dt') for x in topic_manifest]

                perm_manifest[topic_key] = topic_manifest

            elif is_interval and is_location is False:
                await cur.execute("""
                    SELECT DISTINCT(data_date) AS dt
                    FROM %s.collections
                    WHERE form_id = %s 
                    AND status != 'DRAFT'
                    AND status != 'DELETED';
                """, (
                    AsIs(user.get('tki')),
                    perm.get('form_id'),
                ))
                topic_manifest = await cur.fetchall()
                topic_manifest = [x.get('dt') for x in topic_manifest]

                perm_manifest[topic_key] = topic_manifest

        await cur.execute("""
            SELECT u.*,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.assignments AS t WHERE t.user_id = u.id AND t.status = 'ACTIVE') AS assignments
            FROM %s.users AS u;
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
        ))
        users = await cur.fetchall()

        await cur.execute("""
            SELECT l.uuid, l.name, l.site_type_id, l.lineage, l.parent_id, l.groups, l.status,
            (SELECT json_agg(row_to_json(t.*)) FROM %s.location_reporting AS t WHERE t.location_id = l.uuid AND t.status = 'ACTIVE') AS reporting
            FROM %s.locations AS l;
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
        ))
        locations = await cur.fetchall()

        payload['users'] = users
        payload['locations'] = locations

    manifests = []
    for key, val in perm_manifest.items():
        manifests.append(dict(
            id=key,
            records=val,
            ts_ms=int(time.time())
        ))

    return dict(
        user=user_details,
        permissions=permissions,
        locations=locations,
        data=payload,
        manifests=manifests,
        schemas=dict(),
        ts_ms=int(time.time())
    )


async def recv_ask(data, user=None):
    """ Receive a payload from a remote device

    This constitutes first contact between a remote client and the main servers
    for establishing what data should be transferred between them

    *R* = remote client
    *E* = EWARS (this application)

    __REQUEST__
    dts: *R* most recent ts
    rts: *R* most recently received event from *E*
    uid: user id of *R*
    did: device id of *R*
    v: *R* current version of their client
    reps: <arr> of items that the remote wants a canonical version for (in case of errors)
    peers: Events held by *R* for peers
        [(uid, did, ts),...]

    __RESPONSE__
    events: <arr> of events since __rts__
    reps: <arr> array of items requested for canonical version
    d_wants: ts that *E* wants from *R* (all events where ts > d_wants)
    p_wants: <arr> of specs for data that *E* wants from *R* for it's peers
        [(uid, did, ts),...]


    Args:
        data:
        user:

    Returns:

    """

    # TODO: Get most recent stable release version of remote client type
    # TODO: Figure out what *E* wants from *R* for the peer data that *R* holds
    # TODO: Retrieve canonical versions for any reps items sent from *R*

    return_payload = dict(
        events=None,
        wants=None,
        p_wants=None,
        v=None
    )

    # Figure out what *E* wants from *D*
    max_result = None
    wants = 0
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT MAX(ts) as recent
            FROM %s.event_log 
            WHERE origin_uid = %s 
              AND origin_did = %s;
        """, (
            AsIs(user.get('tki')),
            data.get('uid'),
            data.get('did'),
        ))
        max_result = await cur.fetchone()
        max_result = max_result.get('recent', None)

    if max_result is not None:
        wants = max_result

    # Get list of events to send to *R*
    events = []
    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT * FROM %s.event_log
            WHERE ts > %s
              AND origin_uid != %s 
              AND origin_did != %s;
        """, (
            AsIs(user.get('tki')),
            data.get('rts', 0),
            data.get('uid'),
            data.get('did'),
        ))
        events = await cur.fetchall()

    # *R* receives all events us no matter what the users role is
    # it's the client applications job to filter the data to applications
    # within their own system based on metadata and their own assignments.
    # This ensures that data passed on from *R* to it's peers is complete

    # Potential for this to be a bit of a fire hose

    client_version = (0, 0, 1)

    peer_data = []
    for peer in data.get('peers', []):
        # Get latest ts we hold for the peer
        max_peer_ts = None
        async with get_db_cur() as cur:
            cur.execute("""
                SELECT MAX(ts) AS record
                FROM %s.event_log
                WHERE origin_uid = %s 
                  AND origin_did = %s;
            """, (
                AsIs(user.get('tki')),
                peer[0],
                peer[1],
            ))
            max_peer_ts = await cur.fetchone()
            max_peer_ts = max_peer_ts.get('record', 0)

        if peer[2] > max_peer_ts:
            peer_data.append((peer[0], peer[1], max_peer_ts))

    return dict(
        events=events,
        wants=wants,
        peers=peer_data,
        v=client_version
    )


async def recv_push(data, user=None):
    """ Called from remote after successful canon_payload call, contains
    events for peers and remote client

    *R* = remote client
    *E* = EWARS (this application)

    __PAYLOAD__
    events: <arr> of *R*'s events
    p_events: <obj>[(uid,did)]arr contains results of p_wants from canon_payload
        for *R*'s peers


    Args:
        data:
        user:

    Returns:

    """

    # TODO: Process events from *R* into system
    # TODO: Process events in d_wants that *R* sent over wire

    pass


async def get_rebase(resource, spec, user=None):
    """ Remote has requested the canonical set of data for a given resource
    specification

    Used for instance, if a user gets a new assignment, and they don't have the
    back data for the location/form_id. Instead of pushing down the stream, the
    user requests the data from remote as a bundle

    Args:
        resource: The resource type we're trying to get
        spec: A spec filtering the data to be pushed down
        user: The calling user

    Returns:

    """
    pass


async def get_topic_updates(data, user=None):
    """ Given a topic identifier and a timestamp, get any updates from the topic tables
    with a higher ts_ms than the provided ts_ms and push down to the user

    Args:
        data:
        user:

    Returns:

    """

    ts_ms = data.get('ts_ms', None)
    topic = data.get('topic', None)

    return dict(
        data=[],
        ts_ms=int(time.time())
    )

async def get_form_base(payload, user=None):
    form_id = payload.get('form_id', None)
    results = []
    form = None

    ts_ms = str(time.time())

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT id, features FROM %s.forms
            WHERE id = %s;
        """, (
            AsIs(user.get('tki')),
            form_id,
        ))
        form = await cur.fetchone()

    records_manifest = dict()

    async with get_db_cur() as cur:
        await cur.execute("""
                SELECT * FROM %s.collections
                WHERE form_id = %s
                AND status != 'DRAFT'
                ORDER BY submitted DESC
                LIMIT 10000;
            """, (
            AsIs(user.get("tki")),
            form_id,
        ))
        results = await cur.fetchall()

        if form.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
            await cur.execute("""
                    SELECT 
                      location_id::TEXT,
                      data_date
                    FROM %s.collections
                    WHERE form_id = %s 
                    AND status != 'DRAFT'
                    ORDER BY data_date DESC;
                """, (
                AsIs(user.get('tki')),
                form_id,
            ))
            topic_manifest = await cur.fetchall()

            for item in topic_manifest:
                try:
                    records_manifest[item.get("location_id")].append(item.get('data_date'))
                except KeyError:
                    records_manifest[item.get("location_id")] = [item.get("data_date")]

    return dict(
        ts_ms=ts_ms,
        data=[dict(x) for x in results],
        manifest=records_manifest
    )


async def get_topic_base(topic, user=None):
    """ Get the baseline for a set of data

    Args:
        form_id:
        locationd_id:
        user:

    Returns:

    """
    topic = topic.get('topic', None)

    if topic is None:
        return dict(
            err=True,
            err_code="NO_TOPIC_SPEC"
        )

    results = []

    location_id = None
    form_id = None
    alarm_id = None

    is_records = False
    is_alerts = False
    is_schema = False

    records_manifest = None

    ts_ms = int(time.time())

    if "RECORDS_" in topic:
        is_records = True
        parsed = topic.replace("RECORDS_", "")
        location_id = None
        if '_' in parsed:
            form_id, location_id = parsed.split('_')
        else:
            form_id = parsed

        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id, features
                FROM %s.forms
                WHERE id = %s;
            """, (
                AsIs(user.get('tki')),
                form_id,
            ))
            form = await cur.fetchone()

            await cur.execute("""
                SELECT * FROM %s.collections
                WHERE form_id = %s
                AND location_id = %s
                AND status != 'DRAFT'
                ORDER BY submitted DESC
                LIMIT 10000;
            """, (
                AsIs(user.get("tki")),
                form_id,
                location_id,
            ))
            results = await cur.fetchall()

            if form.get('features', {}).get('INTERVAL_REPORTING', None) is not None:
                await cur.execute("""
                    SELECT DISTINCT(data_date) as dt
                    FROM %s.collections
                    WHERE form_id = %s 
                    AND location_id = %s 
                    AND status != 'DRAFT'
                    ORDER BY data_date DESC;
                """, (
                    AsIs(user.get('tki')),
                    form_id,
                    location_id,
                ))
                topic_manifest = await cur.fetchall()

                records_manifest = dict(
                    lid=location_id,
                    dt=[x.get('dt') for x in topic_manifest]
                )

    if "ALERTS_" in topic:
        is_alerts = True
        parsed = topic.replace("ALERTS_", "")

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*, 
                   (SELECT json_agg(row_to_json(t.*)) FROM %s.alert_events AS t WHERE t.alert_id = a.uuid) AS events,
                   (SELECT json_agg(row_to_json(b.*)) FROM %s.alert_actions AS b WHERE b.alert_id = a.uuid) AS actions
                   FROM %s.alerts AS a 
                   WHERE a.alarm_id = %s;
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                parsed,
            ))
            results = await cur.fetchall()

    return dict(
        ts_ms=ts_ms,
        data=[dict(x) for x in results],
        manifest=records_manifest
    )
