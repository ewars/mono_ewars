import brotli
import uuid
import json
import os
from io import StringIO, BytesIO
import socket
import gzip

from ewars.db import get_db_cur, get_db_cursor
from ewars.core.serializers import JSONEncoder
from ewars.conf import settings

from ewars.models import EventLog

from psycopg2.extensions import AsIs


async def _get_seed_form_user(form_id, user=None):
    """ Get seed package for a reporting user

    Args:
        form_id:
        user:

    Returns:

    """
    data = StringIO()
    seed_id = None

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    WITH assignments_ AS (
                        SELECT location_id
                        FROM %s.assignments
                        WHERE form_id = %s
                            AND user_id = %s
                            AND status != 'DELETED'
                    )
                    SELECT 
                        uuid,
                        data_date,
                        location_id,
                        form_version_id,
                        data,
                        submitted_date,
                        created_by
                    FROM %s.collections
                    WHERE form_id = %s
                        AND location_id = ANY(SELECT location_id FROM assignments_)
                        AND status = 'SUBMITTED'
                ) as t
            ) TO STDOUT
        """ % (
            AsIs(user.get('tki')),
            form_id,
            user.get('id'),
            user.get('tki'),
            form_id,
        )
        cur.copy_expert(sql, data)

    data.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(data.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, end_uri)


async def _get_seed_form_geo_admin(form_id, user=None):
    """ Get seed for a geo admin

    Args:
        form_id:
        user:

    Returns:

    """
    data = StringIO()
    seed_id = None

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t)
                FROM (
                    WITH locations_ AS (
                        SELECT uuid::TEXT
                        FROM %s.locations 
                        WHERE lineage::TEXT @> ARRAY['%s']::TEXT[]
                        AND status = 'ACTIVE'
                    )
                    SELECT 
                        uuid,
                        data_date,
                        location_id,
                        form_version_id,
                        data,
                        submitted_date,
                        created_by
                    FROM %s.collections
                    WHERE form_id = %s
                    AND location_id = ANY(SELECT uuid FROM locations_)
                    AND status = 'SUBMITTED'
                ) as t
            ) TO STDOUT
        """ % (
            user.get('tki'),
            user.get('location_id'),
            user.get('tki'),
            form_id,
        )
        cur.copy_expert(sql, data)

    data.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(data.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, end_uri)


async def _get_seed_form(form_id, user=None):
    data = StringIO()

    seed_id = None

    with get_db_cursor() as cur:
        sql = """
            COPY (
                SELECT row_to_json(t) 
                FROM (
                SELECT
                    uuid,
                    data_date,
                    location_id,
                    form_version_id,
                    data,
                    submitted_date,
                    created_by
                FROM %s.collections
                WHERE form_id = %s
                AND status = 'SUBMITTED'
                LIMIT 20000
                ) AS t
            ) TO STDOUT
        """ % (
            user.get('tki'),
            form_id,
        )
        cur.copy_expert(sql, data)

    data.seek(0)

    file_name = '%s.json.gz' % (str(uuid.uuid4()))
    end_file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, file_name)
    inner_file = '%s.json' % (str(uuid.uuid4()))

    with gzip.open(end_file_path, 'wb') as zip_file:
        zip_file.write(data.getvalue().encode('utf8'))

    end_uri = 'http://127.0.0.1:9000/download/%s' % (file_name)

    return (inner_file, end_uri)


async def _get_omnibus(user=None):
    result = dict(
        locations=None,
        location_types=None,
        forms=None,
        indicators=None,
        indicator_groups=None,
        notebooks=None,
        workflows=None,
        users=None,
        assignments=None,
        tasks=None,
        organizations=None,
        alarms=None,
        conf=None,
        ds=None,
        ts=0
    )

    async with get_db_cur() as cur:
        await cur.execute("""
            SELECT uuid, name, parent_id, site_type_id
            FROM %s.locations
            WHERE status != 'DELETED';
        """, (
            AsIs(user.get('tki')),
        ))
        result['locations'] = await cur.fetchall()

        await cur.execute("""
            SELECT id, name 
            FROM %s.location_types;
        """, (
            AsIs(user.get('tki')),
        ))
        result['location_types'] = await cur.fetchall()

        await cur.execute("""
            SELECT f.id, f.name, f.features, f.status, v.definition, v.etl
            FROM %s.forms AS f 
              LEFT JOIN %s.form_versions AS v ON v.uuid = f.version_id
            WHERE f.status != 'DELETED';
        """, (
            AsIs(user.get('tki')),
            AsIs(user.get('tki')),
        ))
        result['forms'] = await cur.fetchall()

        await cur.execute("""
            SELECT uuid, name, group_id, status, itype, definition
            FROM %s.indicators 
            WHERE status != 'DELETED';
        """, (
            AsIs(user.get('tki')),
        ))
        result['indicators'] = await cur.fetchall()

        await cur.execute("""
            SELECT id, name
            FROM %s.indicator_groups;
        """, (
            AsIs(user.get('tki')),
        ))
        result['indicator_groups'] = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.notebooks
            WHERE shared IS TRUE OR created_by = %s;
        """, (
            AsIs(user.get('tki')),
            user.get('id'),
        ))
        result['notebooks'] = await cur.fetchall()

        await cur.execute("""
            SELECT *
            FROM %s.workflows;
        """, (
            AsIs(user.get('tki')),
        ))
        result['workflows'] = await cur.fetchall()

        await cur.execute("""
            SELECT id, name, email, role, org_id, password
            FROM %s.users;
        """, (
            AsIs(user.get('tki')),
        ))
        result['users'] = await cur.fetchall()

        await cur.execute("""
            SELECT * 
            FROM %s.assignments;
        """, (
            AsIs(user.get('tki')),
        ))
        result['assignments'] = await cur.fetchall()

        await cur.execute("""
            SELECT * 
            FROM %s.alarms_v2;
        """, (
            AsIs(user.get('tki')),
        ))
        result['alarms'] = await cur.fetchall()

        await cur.execute("""
            SELECT * 
            FROM %s.data_sets;
        """, (
            AsIs(user.get('tki')),
        ))
        result['ds'] = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.conf;
        """, (
            AsIs(user.get('tki')),
        ))
        result['conf'] = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.organizations;
        """, (
            AsIs(user.get('tki')),
        ))
        result['organizations'] = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.tasks_v2;
        """, (
            AsIs(user.get('tki')),
        ))
        result['tasks'] = await cur.fetchall()

        await cur.execute("""
            SELECT * FROM %s.alerts;
        """, (
            AsIs(user.get('tki')),
        ))
        result['alerts'] = await cur.fetchall()

        await cur.execute("""
            SELECT MAX(ts) AS counter
            FROM %s.event_log
            WHERE origin_uid != %s 
              AND origin_uid != %s;
        """, (
            AsIs(user.get('tki')),
            user.get('id'),
            user.get('did'),
        ))
        res = await cur.fetchone()
        if res is None:
            result['ts'] = 0
        else:
            result['ts'] = res.get('counter', 0)


    return result


async def get_seed(seed, seed_id, user=None):
    """ Get the manifest for a seed package

    Args:
        seed: The seed package name (omnibus, record, record_ds)
        seed_id: The id (optional) for the package
        user: The calling user

    Returns:

    """
    result = None

    if seed == 'FORM':
        if user.get('role') == 'USER':
            result = await _get_seed_form_user(seed_id, user=user)
        elif user.get('role') == 'REGIONAL_ADMIN':
            result = await _get_seed_form_geo_admin(seed_id, user=user)
        else:
            result = await _get_seed_form(seed_id, user=user)
    elif seed == 'OMNIBUS':
        result = await _get_omnibus(user=user)
    elif seed == 'DS':
        result = None

    return result
