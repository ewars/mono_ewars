import datetime
from unittest import TestCase
from ewars.utils import date_utils


class TestDisplay(TestCase):
    def test_display_day(self):
        self.assertEqual(
            "2016-01-01",
            date_utils.display("2016-01-01", "DAY")
        )

    def test_display_year(self):
        self.assertEqual(
            "2016",
            date_utils.display("2016-01-01", "YEAR")
        )

    def test_display_month(self):
        self.assertEqual(
            "2016-01",
            date_utils.display("2016-01-01", "MONTH")
        )

    def test_display_week(self):
        self.assertEqual(
            "W53 2015 (Dec 28-Jan 03)",
            date_utils.display("2016-01-01", "WEEK")
        )


class TestParseDate(TestCase):
    def test_iso(self):
        self.assertEqual(
            datetime.date(2016, 1, 1),
            date_utils.parse_date("2016-01-01")
        )

    def test_date(self):
        self.assertEqual(
            datetime.date(2016, 1, 1),
            date_utils.parse_date(datetime.date(2016, 1, 1))
        )
