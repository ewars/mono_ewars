from collections import namedtuple

TABLES = {
    "collection_counts": {
        "t": "collection_counts"
    },
    "locations": {
        "t": "locations",
        "joins": {
            "location_types": ("site_type_id", "uuid"),
            "collections": ("lineage", "location_id"),
            "alerts": ("uuid", "location_id"),
            "users": ("uuid", "location_id")
        }
    },
    "locations_tot": {
        "t": "locations",
        "joins": {
            "location_types": ("site_type_id", "uuid"),
            "collections": ("uuid", "location_id"),
            "alerts": ("uuid", "location_id"),
            "users": ("uuid", "location_id")
        }
    },
    "collections": {
        "t": "collections",
        "joins": {
            "locations": ("location_id", "uuid"),
            "users": ("created_by", "id"),
            "forms": ("form_id", "id")
        }
    },
    "alerts": {
        "t": "alerts",
        "joins": {
            "locations": ("location_id", "uuid"),
            "alarms": ("alarm_id", "uuid")
        }
    },
    "alarms": {
        "t": "alarms_V2",
        "joins": {
            "alerts": ("uuid", "alarm_id")
        }
    },
    "tasks": {
        "t": "tasks_v2",
        "joins": {
            "user": ("user_id", "id")
        }
    },
    "devices": {
        "t": "devices",
        "joins": {
            "user": ("user_id", "id")
        }
    },
    "location_types": {
        "t": "location_types",
        "joins": {
            "locations": ("id", "site_type_id")
        }
    },
    "organizations": {
        "t": "organizations",
        "joins": {
            "users": ("uuid", "org_id")
        }
    },
    "users": {
        "t": "users",
        "joins": {
            "collections": ("id", "created_by"),
            "organizations": ("org_id", "uuid"),
            "tasks": ("id", "user_id")
        }
    },
    "forms": {
        "t": "forms",
        "joins": {
            "collections": ("id", "form_id")
        }
    }
}


MAPPINGS = dict(
    RECORDS=dict(
        t="collection_counts",
        select="SUM(count) AS _{CODE}",
        name="Number of Records",
        mapping=dict(
            LOCATION=("location_id", "uuid"),
            LOCATION_TYPE=("locations", "uuid", "location_id", "site_type_id", "id"),
            LOCATION_STATUS=("locations", "uuid", "location_id", "status", "status"),
            RECORD_DATE=("data_date", "BETWEEN"),
            FORM=("form_id", "id"),
        )
    ),
    ALERTS=dict(
        t="alerts",
        select="COUNT({ALIAS}.*) AS _{CODE}",
        name="Number of Alerts",
        mapping=dict(
            ALARM=("ALARMS", "alarm_id", "uuid"),
            LOCATION=("LOCATIONS", "location_id", "uuid"),
            LOCATION_TYPE=("locations", "location_id", "uuid", "{ALIAS}.site_type_id", "{ALIAS}.type"),
            LOCATION_STATUS=("LOCATIONS", "location_id", "uuid", "status", "{ALIAS}.status")
        )
    ),
    LOCATIONS=dict(
        t="locations",
        select="COUNT({ALIAS}.*) AS _{CODE}",
        name="Number of Locations",
        mapping=dict(
            LOCATION=("locations", "uuid", "uuid", "LEFT JOIN"),
            COLLECTIONS=("collections", "uuid", "location_id", "LEFT JOIN"),
            ASSIGNMENTS=("uuid", "location_id"),
            LOCATIONS=("uuid", "uuid"),
            LOCATION_STATUS=("status", "status")
        )
    ),
    ORGANIZATIONS=dict(
        t='organizations',
        name="Number of Organizations",
        select="COUNT({ALIAS}.*) AS _{CODE}",
        mapping=dict(

        )
    ),
    ALARMS=dict(
        t="alarms_v2",
        name="Number of Alarms",
        select="COUNT(c.*) AS _{CODE}"
    ),
    DEVICES=dict(
        t="devices",
        name="Number of Devices",
        select="(COUNT"
    ),
    LOCATION_TYPES=dict(
        t="location_types",
        name="Number of Location Types",
        mapping=dict(
            LOCATION=None,
            RECORD_DATE=None
        )
    ),
    FIELD=dict(
        t="COLLECTIONS",
        select="SUM(COALESCE(CAST({ALIAS}.data ->> '{FIELD}' AS NUMERIC), 0)) AS _{CODE}",
        name="Field",
        mapping=dict(
            LOCATION=("location_id", "uuid"),
            FORM=("id", "form_id"),
            USER_TYPE=("created_by", "id"),
            RECORD_DATE=("data_date", ""),
            LOCATION_STATUS=("locations", "uuid", "location_id", "status", "status"),
            LOCATION_TYPE=("locations", "uuid", "location_id", "site_type_id", "id")
        )
    )
)

FMAPPINGS = dict(
    FORM=dict(
        t="forms",
        filter="id",
        mapping=dict(
            collections=("id", "form_id"),
            collection_counts=("id", "form_id")
        ),
        select="{ALIAS}.id AS _{CODE}"
    ),
    LOCATION_STATUS=dict(
        t=None,
        vals=["ACTIVE", "Inactive", "NULL"],
        mapping=dict(
            FORM="CROSS",
            LOCATION=("status", "status")
        ),
        join_type="CROSS JOIN",
        custom="SELECT status FROM unnest(ARRAY['ACTIVE', 'INACTIVE', NULL]) AS {ALIAS}",
        select="{ALIAS}.status AS _{CODE}",
        group="{ALIAS}.status"
    ),
    FIELD=dict(
        t="collections",
        filter=None,
        mapping=dict(
        ),
        select="{JOIN} {ALIAS}.data->>'{FIELD}' AS _{CODE}"
    ),
    LOCATION_TYPE=dict(
        t="location_types",
        mapping=dict(
            FORM="CROSS",
            LOCATION="CROSS",
            RECORD_DATE="CROSS"
        ),
        join_type="CROSS JOIN",
        select="{ALIAS}.id AS _{CODE}",
        group="{ALIAS}.id",
        filter="site_type_id"
    ),
    RECORD_DATE=dict(
        t="DATE",
        mapping=dict(),
        join_type="CROSS JOIN",
        select="date_part('epoch', {ALIAS}) AS _{CODE}",
        group="date_part('epoch', {ALIAS})"
    ),
    LOCATION=dict(
        t="locations",
        mapping=dict(
            FORM="CROSS",
            LOCATION="CROSS",
            RECORD_DATE="CROSS"
        ),
        join_type="CROSS JOIN",
        select="{ALIAS}.uuid AS _{CODE}",
        group="{ALIAS}.uuid",
        filter="site_type_id"
    )
)



PRESETS = {
    "RECORDS": {
        "filters": None,
        "select": "SUM(count) AS _{CODE}",
        "primary": "collection_counts",
        "name": "Number of Records"
    },
    "USERS": {
        "join": "LEFT JOIN {SCHEMA}.users AS u",
        "select": "COUNT({ALIAS}.*} AS _{CODE}",
        "primary": "users",
        "name": "Number of Users"
    },
    "ORGANIZATIONS": {
        "join": "LEFT JOIN {SCHEMA}.organizations AS o",
        "select": "COUNT({ALIAS}.*} AS _{CODE}",
        "primary": "organizations",
        "name": "Number of Organizations"
    },
    "ALARMS": {
        "filters": None,
        "select": "COUNT({ALIAS}.*} AS _{CODE}",
        "primary": "alarms",
        "name": "Number of Alarms"
    },
    "FORM": {
        "prop": "id",
        "primary": "forms",
        "name": "Form"
    },
    "FIELD": {
        "primary": "collections",
        "select": "SUM(COALESCE(CAST({ALIAS}.data ->> '{FIELD}' AS NUMERIC), 0)) AS _{CODE}",
        "name": "Field"
    },
    "FORM_STATUS": {
        "prop": "status",
        "primary": "forms",
        "name": "Form Status"
    },
    "ALERT_DATE": {
        "prop": "trigger_end",
        "primary": "alerts",
        "name": "Alert Date"
    },
    "LOCATION": {
        "filter": "l.uuid",
        "sub": "()",
        "prop": "uuid",
        "join": ("{SCHEMA}.locations", "l", "l.uuid = {ALIAS}.location_id",),
        "dim_join": """
            LEFT JOIN (
            SELECT
              lll.uuid,
              lll.site_type_id,
              ARRAY(SELECT {ALIAS}{ALIAS}.uuid FROM {TKI}.locations AS {ALIAS}{ALIAS} WHERE {ALIAS}{ALIAS}.lineage @> ARRAY[lll.uuid::TEXT]) AS childs
            FROM _4134c0e5ce1b.locations AS lll
            ) AS {ALIAS} ON c.{REMOTE} = ANY({ALIAS}.childs)
        """,
        "primary": "locations",
        "name": "Location"
    },
    "LOCATIONS": {
        "filter": None,
        "select": "COALESCE(SUM(CASE WHEN c.uuid IS NOT NULL THEN 1 ELSE 0 END), 0) AS _{CODE}",
        "primary": "locations_tot",
        "name": "Number of Locations"
    },
    "LOCATION_STATUS": {
        "filter": "status",
        "prop": "status",
        "primary": "locations_tot",
        "name": "Location Status"
    },
    "LOCATION_TYPE": {
        "filter": "site_type_id",
        "prop": "site_type_id",
        "primary": "locations_tot",
        "name": "Location Type"
    },
    "LOCATION_GEOM": {
        "filter": "l.geometry_type",
        "prop": "l.geometry_type",
        "table": ("{SCHEMA}.locations", "l", "l.uuid = {ALIAS}.location_id"),
        "primary": "locations",
        "name": "Location Geometry"
    },
    "LOCATION_GROUP": {
        "filter": "l.groups",
        "prop": "l.groups",
        "table": ("{SCHEMA}.locations", "l", "l.uuid = {ALIAS}.location_id"),
        "primary": "locations",
        "name": "Location Group"
    },
    "RECORD_DATE": {
        "filter": "data_date",
        "prop": "data_date",
        "primary": "collections",
        "type": "PERIOD",
        "name": "Record Date"
    },
    "DATE": {
        "filter": "c.data_date",
        "prop": "c.data_date",
        "table": ("{SCHEMA}.collections", "c"),
        "primary": "collections",
        "type": "PERIOD",
        "name": "Date"
    },
    "DATE_SUB": {
        "filter": "c.submitted_date",
        "prop": "c.submitted_date",
        "table": ("{SCHEMA}.collections", "c"),
        "primary": "collections",
        "name": "Record Submission Date"
    },
    "USER_TYPE": {
        "filter": "role",
        "prop": "role",
        "primary": "users",
        "name": "User Type"
    },
    "USER": {
        "filter": "id",
        "prop": "id",
        "primary": "users",
        "name": "User"
    },
    "ORGANIZATION": {
        "filter": "uuid",
        "prop": "uuid",
        "table": ("{SCHEMA}.organizations", "o", "o.uuid = {ALIAS}.org_id"),
        "primary": "organizations",
        "name": "Organization"
    },
    "SOURCE_TYPE": {
        "filter": "c.source",
        "prop": "c.source",
        "table": ("{SCHEMA}.collections", "c"),
        "primary": "collections",
        "name": "Record Source Type"
    },
    "RECORD_STATUS": {
        "filter": "c.status",
        "prop": "c.status",
        "table": ("{SCHEMA}.collections", "c"),
        "primary": "collections",
        "name": "Record Status"
    },
    "ALARM": {
        "prop": "uuid",
        "filter": "uuid",
        "select": "COALESCE(SUM(CASE WHEN c.status = 'ACTIVE' THEN 1 ELSE 0 END), 0) AS _{CODE}",
        "primary": "alarms",
        "name": "Alarm"
    },
    "ALERT_STATE": {
        "filter": "state",
        "prop": "state",
        "primary": "alerts",
        "name": "Alert State"
    },
    "ALERT_STAGE": {
        "filter": "stage",
        "prop": "stage",
        "primary": "alerts",
        "name": "Alert Stage"
    },
    "DEVICE_TYPE": {
        "filter": "d.type",
        "prop": "d.type",
        "table": ("{SCHEMA}.devices", "d"),
        "primary": "devices",
        "name": "Device Types"
    },
    "TASK_STATUS": {
        "filter": "t.status",
        "table": ("{SCHEMA}.tasks_v2", "t"),
        "primary": "tasks",
        "name": "Task Status"
    },
    "TASK_TYPE": {
        "filter": "t.type",
        "table": ("{SCHEMA}.tasks_v2", "t"),
        "primary": "tasks",
        "name": "Task Type"
    },
    "ASSIGNMENT_STATUS": {
        "filter": "ass.status",
        "table": ("{SCHEMA}.assignments", "ass"),
        "primary": "assignments",
        "name": "Assignment Status"
    },
    "LOC_REPORTING_STATUS": {
        "filter": "lr.status",
        "table": ("{SCHEMA}.location_reporting", "lr"),
        "primary": "reporting",
        "name": "Location Reporting Status"
    },

    "EXPECTED": {},
    "ON_TIME": {},
    "LATE_RECORDS": {},
    "COMPLETENESS": {},
    "TIMELINESS": {},
    "LOCATION_TYPES": {
        "filter": None,
        "select": "COALESCE(SUM(CASE WHEN c.status = 'ACTIVE' THEN 1 ELSE 0 END), 0) AS _{CODE}",
        "primary": "location_types",
        "name": "Number of Location Types"
    },
    "ALERTS": {
        "primary": "alerts",
        "prop": "uuid",
        "select": "COALESCE(SUM(CASE WHEN c.state IS NOT NULL THEN 1 ELSE 0 END), 0) AS _{CODE}",
        "name": "Number of Alerts"
    },
    "FORMS": {
        "name": "Number of Forms"
    },
    "DEVICES": {
        "primary": "devices",
        "name": "Number of Devices"
    },
    "TASKS": {
        "name": "Number of Tasks"
    },
    "ASSIGNMENTS": {},
    "SOURCE": {
        "prop": "source",
        "primary": "collections",
        "filter": "source",
        "name": "Source"
    }
}
