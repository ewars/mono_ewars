from ewars.db import get_db_cur
from ewars.db import get_db_cursor

from ewars.plot.completeness import get_completeness
from ewars.plot.query_builder import QueryBuilder


async def _process_measure(results, measure, dimensions, filters, user=None):
    """ Process a measure

    Args:
        measure:
        dimensions:
        user:

    Returns:

    """

    query = QueryBuilder(user, filters)

    if measure[2] == "COMPLETENESS":
        result = await get_completeness(measure, dimensions, filters, user=user)
        return result

    if measure[2] == "TIMELINESS":
        result = await get_completeness(measure, dimensions, filters, user=user)
        return result

    for dim in dimensions:
        await query.add_dimension(dim)

    query.set_measure(measure, filters.get(measure[0], []))
    sql = query.getQuery()
    sql = sql.replace("{TKI}", user.get("tki"))

    result = None
    with get_db_cursor() as cur:
        cur.execute(sql)
        if len(dimensions) > 0:
            result = cur.fetchall()
            result = list(result)
        else:
            result = cur.fetchone()
            code = "_%s" % (measure[0].replace("-", "_"))
            results[code] = result.get(code)

    return result
