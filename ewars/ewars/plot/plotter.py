import uuid

from psycopg2.extensions import AsIs

from ewars.db import get_db_cur
from ewars.db import get_db_cursor
from ewars.models import Form, Location

from ewars.plot.plot_query_builder import QueryBuilder


async def plot(definition, axis, options, user=None):
    """ Produces a data set for visualization on the frontend, produce as matrixs

       [
           [Dim1,       Dim2,  Dim3,   M1,  M2],
           [2017-01-01, Alpha, Male,   100, 200],
           [2017-01-01, Alpha, Female, 100, 200]
       ]

       dimensions are used to group the data
       measures are actual numerical measurements

       There are additional dimensions which are handled:
           M C (Complex) - Indicates that the resulting measure should be the result of a formula
           D GEOM        - Produces geometry for the locations (if any) in the data set, used for maps/chloropleths

       A measure can come from a number of places
           I (Indicator)  - A pre-calculated indicator; forces use of data_date for the indicator data
           F (Form Field) - Sources data from a field within a specific form, can use alternate date source from within field
           DS (Data Source) - Sources data from a data source column, can use a alternate date source from other column in data source
           C (Complex) - The source is complex and values are derived from a formula correlating multiple other sources

       Args:
           dimensions: Dimensions for the data set
           measures: Measurements for the data set
           user: The calling user

       Returns:

       """

    # Root-level dimensions
    dimensions = [x for x in definition if x.get('t') == 'D' and x.get('p') is None]

    # Root-level measures
    measures = [x for x in definition if x.get('t') == 'M' and x.get('p') is None]

    # Root-level calculations
    calculations = [x for x in definition if x.get('t') == 'C' and x.get('p', None) is None]

    # Measures/dimensions which need to be loaded for details
    details = [x for x in definition if x.get('t') in ('M', 'D', 'C') and x.get('_t') == 'D']

    # Measures/dimensions which need to be loaded for sizing nodes
    sizers = [x for x in definition if x.get('t') in ('M', 'C', 'D') and x.get('_t') == 'S']

    # Measures/dimensions which need to be loaded for colouring data
    colourers = [x for x in definition if x.get('t') in ('M', 'C', 'D') and x.get('_t') == 'C']

    # Filters to be applied to things
    filterables = [x for x in definition if x.get('t') in ('C', 'M', 'D') and x.get('_t', None) == 'F']
    filter_set = dict()
    for x in filterables:
        try:
            filter_set[x.get('p')].append(x)
        except KeyError:
            filter_set[x.get('p')] = [x]

    qb = QueryBuilder(filter_set)

    for dim in dimensions:
        qb.add_dimension(dim)

    specials = []

    for meas in measures:
        if meas.get('n') in ('COMPLETENESS', 'TIMELINESS', 'RECORDS_LATE', 'RECORDS_TIMELY', 'RECORDS_EXPECTED'):
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT features FROM %s.forms WHERE id = %s;
                """, (
                    AsIs(user.get('tki')),
                    meas.get('fid'),
                ))
                result = await cur.fetchone()
                meas['features'] = result.get('features')

        if meas.get('n') == 'RECORDS' and meas.get('fid', None) is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT features FROM %s.forms WHERE id = %s;
                """, (
                    AsIs(user.get('tki')),
                    meas.get('fid'),
                ))
                result = await cur.fetchone()
                meas['features'] = result.get('features')

        qb.add_measure(meas)

    for detail in details:
        if detail.get('t') == 'M':
            qb.add_measure(detail)
        else:
            qb.add_property(detail)

    for sizer in sizers:
        if sizer.get('t') == 'M':
            qb.add_measure(sizer)
        else:
            qb.add_property(sizer)

    for colour in colourers:
        if colour.get('t') == 'M':
            qb.add_measure(colour)
        else:
            qb.add_property(colour)

    for calc in calculations:
        variables = [x for x in definition if x.get('_t', None) == 'CX' and x.get('p', None) == calc.get('_')]
        var_ids = [x.get('_') for x in variables]
        filterables = [x for x in filterables if x.get('p') in var_ids]
        qb.add_calculation(calc, variables, filterables)

    sql = qb.collect()

    sql = sql.replace('__SCHEMA__', user.get('tki'))

    pre_data = None
    with get_db_cursor() as cur:
        cur.execute(sql)
        pre_data = cur.fetchall()

    result_aliases = [(x.result_alias, x.id) for x in qb.dimensions]
    result_m_aliases = [(x.result_alias, x.id) for x in qb.measures]
    result_c_aliases = [(x.result_alias, x.id) for x in qb.calculations]

    result_aliases = result_aliases + result_m_aliases + result_c_aliases

    manifest = dict((x[1], index) for index, x in enumerate(result_aliases))

    parsed_results = []

    # Parse out the data into a list of lists
    results = []
    for item in pre_data:
        results.append([item.get(alias[0]) for alias in result_aliases])

    index = dict((x[1], idx) for idx, x in enumerate(result_aliases))

    metadata = dict()

    meas_indices = [index.get(x.id) for x in qb.measures]
    meas_indices = meas_indices + [index.get(x.id) for x in qb.calculations]

    # Filter out rows with None for all measures

    for dim in qb.dimensions:
        idx = index.get(dim.id)
        sub_meta = dict()
        value_set = set([x[idx] for x in results])
        sub_meta['values'] = sorted(value_set)
        sub_meta['min'] = min(value_set)
        sub_meta['max'] = max(value_set)
        metadata[dim.id] = sub_meta

    lookup = dict()
    for dim in qb.dimensions:
        if dim.name == 'LOCATION':
            values = metadata.get(dim.id, {}).get('values', [])
            locations = []
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid::TEXT, name->>'en' AS name
                    FROM %s.locations 
                    WHERE uuid::TEXT = ANY(%s);
                """, (
                    AsIs(user.get('tki')),
                    values,
                ))
                locations = await cur.fetchall()

            locations = dict((x.get('uuid'), x) for x in locations)

            if options is not None:
                if options.get('loadGeom', False) is True:
                    for loc_uid, loc in locations.items():
                        loc['geometry'] = await Location.geometry(loc.get('uuid'), tki=user.get('tki'))

            metadata[dim.id]['dict'] = locations

        elif dim.name == 'LOCATION_TYPE':
            values = metadata.get(dim.id, {}).get('values', [])
            results = []
            async with get_db_cur() as cur:
                await cur.exccute("""
                    SELECT id, name
                    FROM %s.location_types
                    WHERE id = ANY(%s);
                """, (
                    AsIs(user.get('tki')),
                    values,
                ))
                results = await cur.fetchall()

            results = dict((x.get('id'), x.get('name')) for x in results)
            metadata[dim.id]['dict'] = results

        elif 'FIELD.' in dim.name:
            # We're dealing with a field
            field = await Form.get_field(dim.name, user=user)
            if field.get('type', None) == 'select':
                metadata[dim.id]['dict'] = dict((x[0], y[1]) for x, y in field.get('options', []))

        elif dim.name == 'ALARM':
            values = metadata.get(dim.id, {}).get('values', [])
            results = []
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid::TEXT, name
                    FROM %s.alarms_v2
                    WHERE uuid::TEXT = ANY(%s);
                """, (
                    AsIs(user.get('tki')),
                    values,
                ))
                results = await cur.fetchall()

            results = dict((x.get('uuid'), x.get('name')) for x in results)
            metadata[dim.id]['dict'] = results

    results = [x for x in results if any([x[i] is not None for i in meas_indices])]

    return dict(
        d=results,
        m=metadata,
        i=index
    )
