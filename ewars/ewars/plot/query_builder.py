import datetime
from collections import OrderedDict, namedtuple

from ewars.plot.constants import ALIASES
from ewars.plot.presets import FMAPPINGS, MAPPINGS
from ewars.models import Form
from ewars.plot.date_parser import _process_date
from ewars.plot.filter_parser import _parse_filter

class _QueryBuilder(object):
    def __init__(self, user):
        self.user = user

        self.joins = []
        self.selects = []
        self.groups = []

        self.dimensions = 0
        self._dimensions = OrderedDict()

        self.counter = 0
        self._aliasPrefix = ""
        self.aliases = dict()

    def _get_alias(self):
        alias = ALIASES[self.counter]
        self.counter += 1

        if self.counter >= len(ALIASES):
            self._aliasPrefix = self._aliasPrefix + "_"

        return "%s%s" % (self._aliasPrefix, alias)

    def add_dimension(self, dimension):
        pass

    def set_measure(self, measure):
        pass

    def add_filter(self, filtr):
        pass

    def get_query(self):
        pass


class QueryBuilder(object):
    def __init__(self, user, filters):
        self.user = user
        self.context = None
        self.select = None

        self.joins = []
        self.selects = []
        self.groups = []

        self.dimensions = 0
        # Contains PJOIN = list of ON criteria
        self._dimensions = OrderedDict()
        self._filters = filters
        """
        
            key: TABLE|TYPE
            dict(
                dim=code,
                sql_join="FROM {TKI}.something",
                sql_crit=["prop = value"],
                type="CROSS",
                alias="a",
                filters=[]
        """

        self.counter = 0
        self._aliasPrefix = ""
        self.aliases = dict()

    def _getAlias(self):
        alias = ALIASES[self.counter]
        self.counter += 1

        if self.counter >= len(ALIASES):
            self._aliasPrefix = self._aliasPrefix + "_"

        return "%s%s" % (self._aliasPrefix, alias)

    async def add_dimension(self, dimension):
        dim_conf = None

        inner_alias = self._getAlias()
        outer_alias = self._getAlias()

        select = None
        group = None
        join_type = "LEFT JOIN"
        inner_joins = []

        meta = dict()

        block_filtering = False
        wrapped = True

        # get the basis for the query, the group and select for the dimension
        if "FIELD." in dimension[2]:
            dim_conf = FMAPPINGS.get("FIELD")
            field = await Form.get_field(dimension[2].replace("FIELD.", ""), user=self.user)

            alias = inner_alias

            select = "{ALIAS}.data->>'{FIELD}' AS _{CODE}".format(**dict(
                ALIAS=outer_alias,
                FIELD=".".join(dimension[2].split(".")[2:]),
                CODE=dimension[0].replace("-", "_")
            ))

            print("TEST")
        else:
            dim_conf = FMAPPINGS.get(dimension[2])
            # Process the select
            select = dim_conf.get("select")
            select = select.format(**dict(
                ALIAS=outer_alias,
                CODE=dimension[0].replace("-", "_")
            ))
            group = dim_conf.get("group").format(**dict(
                ALIAS=outer_alias
            ))
            # Process the group by

        join_type = dim_conf.get("join_type", "LEFT JOIN")

        query = None
        if dim_conf.get("custom", None) is not None:
            query = dim_conf.get("custom").format(**dict(
                ALIAS=inner_alias
            ))
        elif dimension[3].get("type", None) == "DATE":
            wrapped = False
            date_filters = self._filters.get(dimension[0], None)
            dim_def = dimension[3]
            period = ["{N_Y_S}", "{NOW}"]

            interval = dim_def.get("interval", "DAY")

            if date_filters is not None:
                block_filtering = True
                for df in date_filters:
                    period = df[3].get("period", ["{NOW}-90D", "{NOW}"])

            end_date = _process_date(period[1], None, None)
            start_date = _process_date(period[0], None, end_date)

            meta = dict(
                start_date=start_date,
                end_date=end_date,
                interval=interval
            )

            query = """ generate_series('{DATE_START}'::TIMESTAMP, '{DATE_END}', '1 {INTERVAL}')"""
            query = query.format(**dict(
                DATE_START=start_date,
                DATE_END=end_date,
                INTERVAL=interval
            ))

        else:
            query = "SELECT * FROM {{TKI}}.{TABLE} AS {ALIAS}".format(**dict(
                TABLE=dim_conf.get("t"),
                ALIAS=inner_alias
            ))

        filters = []
        if block_filtering is False:
            wrapped = True
            for filt in self._filters.get(dimension[0]):
                filt_conf = filt[3]
                filt_def = None
                join_alias = None

                token = None

                # Check if the filter is a field value or preset
                if "FIELD." in filt[2]:
                    filt_def = FMAPPINGS.get("FIELD")
                    token = "{ALIAS}.data->>'{FIELD}'".format(**dict(
                        ALIAS=inner_alias,
                        FIELD=".".join(filt[2].split(".")[2:]),
                    ))
                else:
                    filt_def = FMAPPINGS.get(filt[2])
                    token = "{ALIAS}.{PROP}".format(**dict(
                        ALIAS=inner_alias,
                        PROP=filt_def.get("filter")
                    ))

                # If the filter isn't based on the same table as the measure, we need to JOIN
                # in order to filter
                if filt_def.get("t") != dim_conf.get("t"):
                    if dim_conf.get("t") in filt_def.get("mapping", {}).keys():
                        # Get the definition for the mapping
                        join_def = filt_def.get("mapping", {}).get(dim_conf.get("t"))

                        join_alias = ALIASES[self.counter]
                        self.counter += 1
                        join_sql = "LEFT JOIN {{TKI}}.{TABLE} AS {JOIN_ALIAS} ON {JOIN_ALIAS}.{LOCAL_KEY} = {ALIAS}.{REMOTE_KEY} ".format(
                            **dict(
                                TABLE=filt_def.get("t"),
                                JOIN_ALIAS=join_alias,
                                ALIAS=alias,
                                LOCAL_KEY=join_def[0],
                                REMOTE_KEY=join_def[1]
                            ))
                        inner_joins.append(join_sql)

                        token = token.format(ALIAS=join_alias)
                else:
                    token = token.format(ALIAS=alias)

                # For each of the filter specs evaluate the filter type
                for item in filt_conf:
                    comparator = None
                    splat = item.split(":")

                    if splat[0] == "":
                        comparator = "EQ"
                    else:
                        comparator = splat[0]

                    val = splat[1]

                    filters.append(_parse_filter(comparator, token, val))

        """
            At the end of this we should have SQL which looks like this
            
            FROM (
                SELECT a.*
                FROM {TKI}.location_types AS a
                WHERE a.id IN ARRAY[1,2]
            ) AS b 
            
            Once getQuery is called we compute the relations between the dimensions
            and cache them unless a new dimension is added
        """

        sql_filter = ""
        if len(filters) > 0:
            sql_filter = " WHERE "
            sql_filter += " AND ".join(filters)

        join_type = "FROM"
        if self.dimensions > 0:
            join_type = "CROSS JOIN"

        if wrapped:
            sql = """
                {JOIN} (
                    {QUERY}
                    {JOINS}
                    {FILTERS}
                ) AS {ALIAS}
            """.format(**dict(
                JOIN=join_type,
                QUERY=query,
                JOINS=" ".join(inner_joins),
                FILTERS=sql_filter,
                ALIAS=outer_alias
            ))
        else:
            sql = """
                {JOIN} {QUERY} AS {ALIAS}
            """.format(**dict(
                JOIN=join_type,
                QUERY=query,
                ALIAS=outer_alias
            ))

        _dimension = dict(
            dim=dimension[0],
            alias=outer_alias,
            conf=dimension[3],
            dim_type=dimension[2],
            sql=sql,
            select=select,
            joins=[],
            grouping=group,
            meta=meta
        )

        self._dimensions[outer_alias] = _dimension
        self.dimensions += 1

    def set_measure(self, measure, m_filters):
        self.measure = measure
        # We're doing a straight query for data, potentially with filters
        definition = None
        mapping_def = None
        outer_alias = None

        select = None
        context = None
        joins = []
        outer_joins = []
        inner_joins = []
        filters = []
        query = ""

        inner_selects = []

        joined = dict()

        outer_alias = self._getAlias()
        inner_alias = self._getAlias()

        if "FIELD." in measure[2]:
            # This is a field
            definition = MAPPINGS.get("FIELD")
            select = "SUM(COALESCE(CAST({ALIAS}.data ->> '{FIELD}' AS NUMERIC), 0)) AS _{CODE}".format(**dict(
                ALIAS=outer_alias,
                FIELD=".".join(measure[2].split(".")[2:]),
                CODE=measure[0].replace("-", "_")
            ))

            context = "{{TKI}}.collections AS {ALIAS}".format(**dict(
                ALIAS=inner_alias
            ))

        else:
            definition = MAPPINGS.get(measure[2])
            if definition.get('select', None) is not None:
                select = definition.get('select').format(**dict(
                    ALIAS=outer_alias,
                    CODE=measure[0].replace("-", "_")
                ))
            else:
                select = "COUNT({ALIAS}.*) AS _{CODE}".format(**dict(
                    ALIAS=outer_alias,
                    CODE=measure[0].replace("-", "_")
                ))

            context = "{{TKI}}.{TABLE} AS {ALIAS}".format(**dict(
                TABLE=definition.get("t"),
                ALIAS=inner_alias
            ))

        inner_selects.append("{ALIAS}.*".replace("{ALIAS}", inner_alias))

        def_mappings = definition.get("mapping", {})

        for alias, dim in self._dimensions.items():
            dim_conf = None
            if "FIELD." in dim.get("dim_type"):
                dim_conf = FMAPPINGS.get("FIELD")
            else:
                dim_conf = FMAPPINGS.get(dim.get("dim_type"))

            # Do we need additional data to be able to join
            # to the dimension?
            if dim.get("dim_type") in def_mappings.keys():
                # We have a mapping for it
                join_def = def_mappings.get(dim.get("dim_type"))
                if len(join_def) == 2:
                    # We can join directly the dimension
                    dim_def = dim.get("conf")
                    dim_meta = dim.get("meta")

                    if dim_def.get("type") == "DATE":
                        # We're dealing with joining on a date type field
                        joiner = """
                            {LOCAL_ALIAS}.{LOCAL_PROP} BETWEEN date_trunc('{INTERVAL}', {REMOTE_ALIAS}) AND {REMOTE_ALIAS}
                        """

                        joiner = joiner.format(**dict(
                            LOCAL_ALIAS=outer_alias,
                            LOCAL_PROP=join_def[0],
                            INTERVAL=dim_meta.get("interval").lower(),
                            REMOTE_ALIAS=dim.get("alias")
                        ))
                        outer_joins.append(joiner)
                    else:
                        outer_joins.append("{OUTER_ALIAS}.{INNER_PROP} = {DIM_ALIAS}.{DIM_PROP}".format(**dict(
                            OUTER_ALIAS=outer_alias,
                            INNER_PROP=join_def[0],
                            DIM_ALIAS=alias,
                            DIM_PROP=join_def[1]
                        )))
                elif len(join_def) == 5:
                    # We need to use a joined table to
                    # join to the dim table
                    join_alias = None
                    if join_def[0] not in joined.keys():
                        join_alias = self._getAlias()
                        joined[join_def[0]] = join_alias
                        inner_join = "LEFT JOIN {{TKI}}.{TABLE} AS {J_ALIAS} ON {J_ALIAS}.{J_PROP} = {ALIAS}.{PROP}".format(
                            **dict(
                                TABLE=join_def[0],
                                J_ALIAS=join_alias,
                                J_PROP=join_def[1],
                                ALIAS=inner_alias,
                                PROP=join_def[2]
                            ))
                        inner_joins.append(inner_join)


                    else:
                        join_alias = joined[join_def[0]]

                    inner_selects.append("{ALIAS}.{J_PROP}".format(**dict(
                        ALIAS=join_alias,
                        J_PROP=join_def[1]
                    )))

                    inner_selects.append("{ALIAS}.{J_PROP}".format(**dict(
                        ALIAS=join_alias,
                        J_PROP=join_def[3]
                    )))

                    outer_join = "{J_ALIAS}.{J_PROP} = {D_ALIAS}.{D_PROP}".format(**dict(
                        J_ALIAS=outer_alias,
                        J_PROP=join_def[3],
                        D_ALIAS=dim.get("alias"),
                        D_PROP=join_def[4]
                    ))

                    outer_joins.append(outer_join)

        # If there are filters for the measure itself we need to format as a sub-query, filters on the
        # outer scope won't apply correctly.
        if len(m_filters) > 0:
            for filt in m_filters:
                filt_conf = filt[3]
                filt_def = None
                join_alias = None

                token = None

                # Check if the filter is a field value or preset
                if "FIELD." in filt[2]:
                    filt_def = FMAPPINGS.get("FIELD")
                    token = "{{ALIAS}}.data->>'{FIELD}'".format(**dict(
                        FIELD=".".join(filt[2].split(".")[2:]),
                    ))
                else:
                    filt_def = FMAPPINGS.get(filt[2])
                    token = "{{ALIAS}}.{PROP}".format(**dict(
                        PROP=filt_def.get("filter")
                    ))

                # If the filter isn't based on the same table as the measure, we need to JOIN
                # in order to filter
                if filt_def.get("t") != definition.get("t"):
                    if filt_def.get("t") in joined.keys():
                        # We've already joined the table, so we can use the existing alias
                        j_alias = joined.get(filt_def.get("t"))
                        token = token.replace("{ALIAS}", j_alias)
                    else:
                        if definition.get("t") in filt_def.get("mapping", {}).keys():
                            # Get the definition for the mapping
                            join_def = filt_def.get("mapping", {}).get(definition.get("t"))

                            join_alias = ALIASES[self.counter]
                            self.counter += 1
                            join_sql = "LEFT JOIN {{TKI}}.{TABLE} AS {JOIN_ALIAS} ON {JOIN_ALIAS}.{LOCAL_KEY} = {ALIAS}.{REMOTE_KEY} ".format(
                                **dict(
                                    TABLE=filt_def.get("t"),
                                    JOIN_ALIAS=join_alias,
                                    ALIAS=outer_alias,
                                    LOCAL_KEY=join_def[0],
                                    REMOTE_KEY=join_def[1]
                                ))
                            joins.append(join_sql)

                            token = token.format(ALIAS=join_alias)
                else:
                    token = token.format(ALIAS=outer_alias)

                # For each of the filter specs evaluate the filter type
                for item in filt_conf:
                    comparator = None
                    splat = item.split(":")

                    if splat[0] == "":
                        comparator = "EQ"
                    else:
                        comparator = splat[0]

                    val = splat[1]

                    filters.append(_parse_filter(comparator, token, val))

        outer_sql = ""
        sql_join = " FROM "
        if len(outer_joins) > 0:
            outer_sql = " ON "
            outer_sql += " AND ".join(outer_joins)
            sql_join = " LEFT JOIN "

        sql_inner_select = ", ".join(inner_selects)

        sql = """
            {SQL_JOIN} (SELECT {INNER_SELECTS} FROM {CONTEXT} {JOINS} {FILTERS}) AS {ALIAS} {OUTER_JOINS}
        """.format(**dict(
            SQL_JOIN=sql_join,
            CONTEXT=context,
            JOINS="\n".join(inner_joins),
            FILTERS="",
            ALIAS=outer_alias,
            OUTER_JOINS=outer_sql,
            INNER_ALIAS=inner_alias,
            INNER_SELECTS=sql_inner_select
        ))

        # No measures attached already
        self.context = sql
        self.select = select

    def getQuery(self):
        # Direct query, no dimensions
        sql = None
        if self.dimensions <= 0:
            self.counter += 1
            sql = """
                SELECT 
                  {SELECTS}
                  {QUERY}
            """.format(**dict(
                SELECTS=self.select,
                QUERY=self.context
            ))
        else:
            # We need to add dimensions to the system first

            selects = ", ".join([x.get("select") for x in self._dimensions.values()])
            dimensions = " ".join([x.get("sql") for x in self._dimensions.values()])
            groupings = ", ".join([x.get("grouping") for x in self._dimensions.values()])

            selects = selects + "," + self.select
            dimensions = dimensions + " " + self.context

            sql = """
                SELECT {SELECTS}
                {DIMENSIONS}
                GROUP BY {GROUPINGS}
            """.format(**dict(
                SELECTS=selects,
                DIMENSIONS=dimensions,
                GROUPINGS=groupings
            ))

        return sql