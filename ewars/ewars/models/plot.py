import json
import uuid
import datetime

from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

class Plot:

    @staticmethod
    async def update(plot_id, data, user=None):
        """ Update a plot within the system

        Args:
            plot_id: The id of the plot to update
            data: The data associated with the plot
            user: The calling user

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.plots
                SET title = %s,
                    description = %s,
                    definition = %s,
                    last_modified = %s,
                    shared = %s
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                data.get("title"),
                data.get('description'),
                json.dumps(data.get("definition")),
                datetime.datetime.now(),
                data.get("shared"),
                plot_id,
            ))

            await cur.execute("""
                SELECT * FROM %s.plots WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                plot_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create(data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.plots
                (title, description, created_by, created, last_modified, definition, shared)
                VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING *
            """, (
                AsIs(user.get("tki")),
                data.get("title"),
                data.get("description"),
                user.get("id"),
                datetime.datetime.now(),
                datetime.datetime.now(),
                json.dumps(data.get("definition")),
                data.get("shared"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(plot_id, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.plots
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                plot_id,
            ))

        return True

    @staticmethod
    async def query(query_type, user=None):
        results = []

        if query_type == "USER":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.plots
                    WHERE created_by = %s
                """, (
                    AsIs(user.get("tki")),
                    user.get("id"),
                ))
                result = await cur.fetchall()
        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT p.*, u.name, u.email
                    FROM %s.plots AS p
                        LEFT JOIN %s.users AS u ON u.id = p.created_by
                    WHERE p.shared IS TRUE
                    AND p.creacted_by != %s
                    ORDER BY p.title ASC;
                """, (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    user.get("id"),
                ))
                result = await cur.fetchall()

        return results
