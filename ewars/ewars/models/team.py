from ewars.db import get_db_cur
from psycopg2.extensions import AsIs

class Team:

    @staticmethod
    async def get_teams(user=None):
        return []

    @staticmethod
    async def get_team(team_id, user=None):
        return {}

    @staticmethod
    async def get_team_backlog(team_id, user=None):
        return []

    @staticmethod
    async def get_team_users(team_id, user=None):
        return []

    @staticmethod
    async def get_team_files(team_id, user=None):
        return []

    @staticmethod
    async def search_team_backlog(team_id, term, user=None):
        return []

    @staticmethod
    async def create(data, user=None):
        return {}

    @staticmethod
    async def update(team_id, data, user=None):
        return {}

    @staticmethod
    async def delete(team_id, user=None):
        return True

    @staticmethod
    async def add_user(team_id, user_id, user=None):
        return {}

    @staticmethod
    async def submit_message(team_id, message, user=None):
        return {}

    @staticmethod
    async def update_message(team_id, message_id, updated, user=None):
        return {}

    @staticmethod
    async def delete_message(team_id, message_id, user=None):
        return {}

    @staticmethod
    async def remove_user(team_id, user_id, user=None):
        return {}