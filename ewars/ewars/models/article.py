import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Article:
    @staticmethod
    async def create(data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.feed_items
                (
                  title,
                  sef,
                  feeds,
                  content,
                  content_type,
                  keys,
                  status,
                  created_by,
                  created,
                  last_modified,
                  publish_date,
                  access
                )
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("title"),
                create_sef(data.get("title"), data.get("publish_date")),
                data.get("feeds"),
                data.get("content"),
                "MARKDOWN",
                [],
                data.get("status"),
                user.get("id"),
                datetime.datetime.now(),
                datetime.datetime.now(),
                data.get("publish_date", datetime.datetime.now()),
                data.get("access", []),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def update(id, data, user=None):
        """ UPdate and article in the system

        Args:
            id: The id of the article
            data: The data representing the article
            user: The calling user

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.feed_items
                SET title = %s,
                  sef = %s,
                  feeds = %s,
                  content = %s,
                  keys = %s,
                  status = %s,
                  last_modified = NOW(),
                  publish_date = %s,
                  access = %s
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("title"),
                create_sef(data.get("title"), data.get("publish_date")),
                data.get("feeds", []),
                data.get("content"),
                data.get("keys", []),
                data.get("status", "DRAFT"),
                data.get("publish_date", datetime.datetime.now()),
                data.get("access", []),
                id,
            ))

        return True

    @staticmethod
    async def delete(id, user=None):
        """ Delete an article from the system

        Args:
            id:
            user:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.articles WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                id,
            ))

        return True
