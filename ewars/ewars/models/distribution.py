import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

class Distribution:

    @staticmethod
    def delete_list(id, user=None):
        """ Delete a distribution list

        Args:
            id:
            user:

        Returns:

        """
        result = None

        with get_db_cursor(commit=True) as cur:
            cur.execute("""
                DELETE FROM %s.distributions WHERE id = %s;
            """, (
                AsIs(user.get('tki')),
                id,
            ))

        return result

    @staticmethod
    def get_list(id, user=None):
        """ Get a distribution list

        Args:
            id:
            user:

        Returns:

        """
        result = None

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT *
                FROM %s.distributions
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                id,
            ))
            result = cur.fetchone()

        return result

    @staticmethod
    def create_list(data, user=None):
        """ Create a new distribution list

        Args:
            data:
            user:

        Returns:

        """
        result = None

        with get_db_cursor(commit=True) as cur:
            cur.execute("""
                INSERT INTO %s.distributions
                (name, data, created, last_modified, created_by)
                VALUES (%s, %s, NOW(), NOW(), %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                json.dumps(data.get('data')),
                user.get("id"),
            ))
            result = cur.fetchone()

        return result

    @staticmethod
    def update_list(id, data, user=None):
        """ Update a distribution list

        Args:
            id:
            data:
            user:

        Returns:

        """
        result = None

        with get_db_cursor(commit=True) as cur:
            cur.execute("""
                UPDATE %s.distributions
                SET name = %s,
                data = %s,
                last_modified = NOW()
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                json.dumps(data.get('data')),
                id,
            ))

            cur.execute("""SELECT * FROM %s.distributions WHERE id = %s""", (
                AsIs(user.get("tki")),
                id,
            ))
            result = cur.fetchone()

        return result