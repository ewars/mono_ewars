import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Hud:
    @staticmethod
    async def query(limit, offset, sorts, filters, user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT h.*,
                        u.name AS creator_name,
                        um.name AS modifier_name
                FROM %s.huds AS h 
                  LEFT JOIN %s.users AS u ON u.id = h.created_by
                  LEFT JOIN %s.users AS um ON um.id = h.lm_id
                LIMIT %s 
                OFFSET %s
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                limit,
                offset,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def get(hud_id, user=None):
        """ Retrieve a single HUD by it's UUID
        
        Args:
            hud_id: THe UUID of the HUD
            user: The calling user

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.huds WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                hud_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def update(hud_id, data, user=None):
        """ Update a HUD definition
        
        Args:
            hud_id: The UUID of the hud to update
            data: THe data to update into the HUD
            user: THe calling user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.huds
                    SET name = %s,
                    settings = %s,
                    status = %s,
                    description = %s,
                    definition = %s,
                    widgets = %s,
                    last_modified = %s,
                    lm_id = %s,
                    permissions = %s 
                  WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                json.dumps(data.get('settings', {})),
                data.get('status'),
                data.get("description"),
                json.dumps(data.get('definition', [])),
                json.dumps(data.get("widgets", {})),
                datetime.datetime.utcnow(),
                user.get('id'),
                json.dumps(data.get("permissions", {})),
                hud_id,
            ))

            await cur.execute("""
                SELECT * FROM %s.huds WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                hud_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(hud_id, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.huds WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                hud_id,
            ))

        return True

    @staticmethod
    async def create(data, user=None):
        """ Store the hud in the database
        
        Args:
            data: The data representing the HUD
            user: The calling user

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.huds
                (name, status, settings, description, definition, widgets, created, created_by, last_modified, lm_id, permissions)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("name", "Unnamed HUD"),
                data.get("status", "DRAFT"),
                json.dumps(data.get("settings", {})),
                data.get('description', "No description provided"),
                json.dumps(data.get('definition', [])),
                json.dumps(data.get("widgets", {})),
                datetime.datetime.utcnow(),
                user.get('id'),
                datetime.datetime.utcnow(),
                user.get("id"),
                json.dumps(data.get("permissions", {})),
            ))
            result = await cur.fetchone()

        return result
