import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Site:
    @staticmethod
    async def delete():
        return None

    @staticmethod
    async def delete_page():
        return None

    @staticmethod
    async def update_page(page_id, data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.site_pages
                    SET title = %s,
                    sef = %s,
                    parent_id = %s,
                    definition = %s,
                    access = %s,
                    last_modified = NOW(),
                    page_type = %s
                    WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("title"),
                data.get("sef"),
                data.get("parent_id", None),
                json.dumps(data.get("definition", dict())),
                data.get("access", "PUBLIC"),
                data.get("page_type"),
                page_id,
            ))

            await cur.execute("""
                SELECT * FROM %s.site_pages
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                page_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create_page(data, user=None):
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.site_pages
                (site_id, title, sef, parent_id, definition, status, access, last_modified, created_by, page_type)
                VALUES (%s, %s, %s, %s, %s, %s, %s, NOW(), %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("site_id"),
                data.get("title"),
                data.get("sef"),
                data.get("parent_id", None),
                json.dumps(data.get("definition")),
                data.get("status", "INACTIVE"),
                data.get("access", "PUBLIC"),
                user.get("id"),
                data.get("page_type", "MARKDOWN"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    def get(site_id, user=None):
        result = None

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT * FROM %s.sites
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                site_id,
            ))
            result = cur.fetchone()

        return result

    @staticmethod
    def get_site_menu(site_id, user=None):
        result = None

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT *
                FROM %s.site_pages
                WHERE site_id = %s;
            """, (
                AsIs(user.get("tki")),
                site_id,
            ))
            result = cur.fetchall()

        return result

    @staticmethod
    def get_site_page(site_id, page_sef, user=None):
        result = None

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT *
                FROM %s.site_pages
                WHERE site_id = %s
                AND sef = %s
            """, (
                AsIs(user.get("tki")),
                site_id,
                page_sef,
            ))
            result = cur.fetchone()

        return result

    @staticmethod
    def get_page(route, user=None):
        pass

    @staticmethod
    def get_site_root(site_id, user=None):
        result = None

        with get_db_cursor() as cur:
            cur.execute("""
                SELECT * FROM %s.site_pages
                WHERE parent_id IS NULL
                AND site_id = %s
                LIMIT 1
            """, (
                AsIs(user.get("tki")),
                site_id,
            ))
            result = cur.fetchone()

        return result

    @staticmethod
    async def update(site_id, data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.sites
                    SET title = %s,
                    status = %s,
                    access = %s,
                    last_modified = NOW(),
                    theme = %s,
                    hub_indexed = %s,
                    goog = %s,
                    description = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                data.get("title"),
                data.get("status"),
                data.get("access"),
                data.get("theme", "DEFAULT"),
                data.get("hub_indexed", "NO"),
                data.get("goog", None),
                data.get("description", None),
                site_id,
            ))

            await cur.execute("""
                SELECT * FROM %s.sites
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                site_id
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create(data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.sites
                (
                    title,
                    status,
                    access,
                    created_by,
                    created,
                    last_modified,
                    theme,
                    hub_indexed,
                    goog,
                    description
                )
                VALUES (%s, %s, %s, %s, NOW(), NOW(), %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("title"),
                data.get("status", "INACTIVE"),
                data.get("access", "PRIVATE"),
                user.get("id"),
                data.get("theme", "DEFAULT"),
                data.get("hub_indexed", "NO"),
                data.get("goog", None),
                data.get("description", None),
            ))
            result = await cur.fetchone()

        return result
