import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class Workflow:
    @staticmethod
    async def create(data, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.workflows
                       (name, status, created_by, created, last_modified, last_modified_by, definition, object_type, object_event)
                VALUES (%s,   %s,     %s,         %s,      %s,             %s,              %s,          %s,         %s);
            """, (
                AsIs(user.get("tki")),
                data.get("name", "NEW_WORKFLOW"),
                data.get('status', 'INACTIVE'),
                user.get("id"),
                datetime.datetime.utcnow(),
                datetime.datetime.utcnow(),
                user.get("id"),
                json.dumps(data.get('definition', [])),
                "ALERT",
                "TRIGGERED"
            ))

        return True

    @staticmethod
    async def update(id, data, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                 UPDATE %s.workflows
                 SET name = %s,
                  definition = %s
                  WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("name", "NEW_NAME"),
                json.dumps(data.get('definition', {})),
                id,
            ))

        return True

    @staticmethod
    async def delete(id, user=None):
        pass

    @staticmethod
    async def get_processes(id, user=None):
        pass
