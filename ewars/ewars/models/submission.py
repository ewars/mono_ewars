import json
import uuid
import datetime
import asyncio

from ewars.db import get_db_cur
from ewars.core.serializers import JSONEncoder
from ewars.constants import CONSTANTS
from ewars.core import tasks

from psycopg2.extensions import AsIs
import tornado
import tornado.ioloop

from .location import Location
from .differential import Differential
from .activity import Activity
from .alarm import Alarm
from .task import Task
from .notification import Notification
from .event_log import EventLog

COMMENT_SUBJECT = "[EWARS] A comment was made on a form submission"

COMMENT_HTML = """
<p><strong>{user_name}</strong> commented on the following report</p>

<table>
    <tr>
        <th>Form</th>
        <td>{form_name}</td>
    </tr>
    <tr>
        <th>Location</th>
        <td>{location_full_name}</td>
    </tr>
    <tr>
        <th>Date</th>
        <td>{data_date}</td>
    </tr>
</table>

<h2>Comment</h2>

{comment}

<p>You can view this report and it's comments <a href="http://ewars.ws/reporting#/{uuid}">here</a></p>

"""

COMPARATORS = {
    "eq": "=",
    "neq": "!=",
    "gt": ">",
    "gte": ">=",
    "lt": "<",
    "lte": "<=",
    "in": None,
    "CONTAINS": None,
    "STARTS_WITH": None,
    "ENDS_WITH": None
}

CAST_TYPES = dict(
    number="NUMERIC",
    date="TEXT",
    select="TEXT",
    text="TEXT"
)

__ALL__ = ["query_submissions"]


class Submission:
    @staticmethod
    async def get_related_alerts(collection_id, user=None):
        """ Get any alerts that this report is associated with

        Args:
            collection_id: The UUID of the collection form
            user: THe calling user

        Returns:
            A list of alerts relating to this form submission
        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT * FROM %s.alerts_full
                 WHERE triggering_report_id = %s;
            """, (
                AsIs(user.get("tki")),
                collection_id,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def comments(collection_id, offset=0, timestamp=None, user=None):
        """ Retrieve comments for a given report

        Args:
            collection_id: The collection uuid to get comments for
            offset: A strict offset from the oldest comment to retrieve
            timestamp: A strict timestamp to get all comments since the timestamp
            user: The calling user

        Returns:
            List of comments
        """
        results = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.v_collection_comments
                WHERE collection_id = %s
                ORDER BY created DESC;
            """, (
                AsIs(user.get("tki")),
                collection_id,
            ))
            results = await cur.fetchall()

        for item in results:
            item['day'] = item.get("created", datetime.datetime.utcnow()).strftime("%Y-%m-%d")
            item['time'] = item.get("created", datetime.datetime.utcnow()).strftime("%H:%M %p")

        return results

    @staticmethod
    async def comment(collection_id, content, user=None):
        """ Comment on a report

        Args:
            collection_id: The UUID of the report
            content: The content to append
            user: The commenting user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.collection_comments
                (collection_id, created, user_id, content)
                VALUES (%s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                collection_id,
                datetime.datetime.utcnow(),
                user.get("id"),
                content,
            ))

        return True

    @staticmethod
    async def get_submission_history(collection_id, user=None):
        results = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT history
                FROM %s.collections
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                collection_id,
            ))
            results = await cur.fetchone()

        return results

    @staticmethod
    async def query(form_id, filters, ordering, limit, offset, unused, count=None, user=None):
        inner_selects = [
            "r.*",
            "l.name AS location_name",
            "l.lineage",
	    "l.groups",
            "u.group_id AS user_group_id",
            "u.account_id AS user_account_id",
            "u.name AS creator_name",
            "sti.name->>'en' AS sti_name"
            # "(SELECT ARRAY(SELECT lt.name->>'en' FROM %s.locations AS lt WHERE lt.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name"
        ]

        sql = """
        SELECT * FROM (
            SELECT {INNER_SELECTS}
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
                LEFT JOIN _iw.users AS u ON u.id = r.created_by
                LEFT JOIN %s.location_types AS sti ON sti.id = l.site_type_id
            ) AS ss
        {WHERES}
        {ORDERINGS}
        LIMIT %s
        OFFSET %s
        """

        wheres = [
            'ss.form_id = %s',
            "ss.status = 'SUBMITTED'"
        ]

        # location_where = None
        # if user.get('role') == 'USER':
        #     assignments = []
        #     async with get_db_cur() as cur:
        #         await cur.execute("""
        #             SELECT location_id
        #             FROM %s.assignments
        #             WHERE form_id = %s
        #               AND user_id = %s
        #               AND status = ANY(ARRAY['ACTIVE', 'INACTIVE']);
        #         """, (
        #             AsIs(user.get('tki')),
        #             form_id,
        #             user.get('id'),
        #         ))
        #         assignments = await cur.fetchall()
        #
        #     if "location_id" not in filters.keys():
        #         wheres.append("ss.location_id::TEXT = ANY(ARRAY['%s']) " % (
        #             "','".join(str(x.get('location_id')) for x in assignments),
        #         ))

        orderings = []

        # Build count query
        sql_count = """
            SELECT COUNT(*) AS total_reports FROM (
            SELECT {INNER_SELECTS}
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
                LEFT JOIN _iw.users AS u ON u.id = r.created_by
                LEFT JOIN %s.location_types AS sti ON sti.id = l.site_type_id
            ) AS ss
        {WHERES}
        """

        for key, filter in filters.items():
            el_type = filter.get("type", None)
            if el_type is not None:
                del filter['type']
            else:
                el_type = "TEXT"

            comp_key, value = filter.popitem()

            if el_type == "user":
                if comp_key == "eq":
                    wheres.append("ss.%s IN (%s)" % (
                        key,
                        ",".join([str(x) for x in value])
                    ))
                elif comp_key == "neq":
                    wheres.append("ss.%s NOT IN (%s)" % (
                        key,
                        ",".join([str(x) for x in value])
                    ))
            elif key == "data":
                _ql = "TRIM(CAST(ss.data AS TEXT)) ILIKE '%%%s%%'" % (
                    value.strip(),
                )
                _ql = _ql.replace("%", "%%")
                wheres.append(_ql)
            else:

                if comp_key == "in":
                    wheres.append("ss.%s::TEXT IN ('%s')" % (
                        key,
                        "','".join(value),
                    ))
                elif comp_key.upper() == "UNDER":
                    wheres.append("ss.lineage::TEXT[] @> '{%s}'::TEXT[]" % (
                        value,
                    ))
                else:
                    if key.startswith("data."):
                        # implement a cast for specific types, if the type is passed
                        if el_type is not None:
                            cast_type = CAST_TYPES.get(el_type)
                            if cast_type == "NUMERIC":
                                # COALESCE(CAST(c.data->>'{FIELD}' AS NUMERIC), 0) AS {INNER_ALIAS},
                                wheres.append("COALESCE(CAST(ss.data->>'%s' AS NUMERIC), 0) %s %s" % (
                                    key.replace("data.", ""),
                                    COMPARATORS[comp_key],
                                    value,
                                ))
                            else:
                                if comp_key.upper() == "CONTAINS":
                                    _ql = "TRIM(CAST(ss.data->>'%s' AS %s)) ILIKE '%%%s%%'" % (
                                        key.replace('data.', ''),
                                        cast_type,
                                        value.strip(),
                                    )
                                    _ql = _ql.replace("%", "%%")
                                    wheres.append(_ql)

                                else:
                                    wheres.append("TRIM(CAST(ss.data->>'%s' AS %s)) %s '%s'" % (
                                        key.replace('data.', ''),
                                        cast_type,
                                        COMPARATORS[comp_key],
                                        value.strip(),
                                    ))
                        else:
                            wheres.append("TRIM(ss.data->>'%s') %s '%s'" % (
                                key.replace("data.", ""),
                                COMPARATORS[comp_key],
                                str(value).strip(),
                            ))
                    else:
                        if el_type == "date":
                            wheres.append("ss.%s::DATE %s '%s'::DATE" % (
                                key,
                                COMPARATORS[comp_key],
                                value
                            ))
                        else:
                            wheres.append("ss.%s %s '%s'" % (
                                key,
                                COMPARATORS[comp_key],
                                value
                            ))

        ft_sql = " AND ".join(wheres)
        # Add to main sql
        # sql += ft_sql
        # Add to coutn query

        orderings = []
        for key, dir in ordering.items():
            direction = dir
            field_type = None
            if ":" in dir:
                direction, field_type = dir.split(":")

            if key.startswith("data."):
                # This is a nested JSON attributed, we need
                # to do some conversion to the key to make
                # it work, these are slow queries
                # so need to look up a way to make it run a little faster
                cleaned = key.replace("data.", "")
                new_key = "data->>'%s'" % (cleaned,)
                if field_type == "number":
                    orderings.append("ss.orderer::NUMERIC %s" % (direction))
                elif field_type in ("text", "select", "textarea"):
                    orderings.append("ss.orderer::TEXT %s" % (direction))
                elif field_type == "DATE":
                    orderings.append("ss.orderer::DATE %s" % (direction))
                else:
                    orderings.append("ss.orderer::TEXT %s" % (direction))
                inner_selects.append("r.%s AS orderer" % (new_key))
            elif key == "creator_name":
                # We're ordering on a join
                orderings.append("ss.creator_name %s" % (direction))
            elif key == "location_name":
                orderings.append("ss.location_name->>'en' %s" % (direction))
            else:
                orderings.append("ss.%s %s" % (key, direction))

        if len(orderings) > 0:
            sql = sql.replace("{ORDERINGS}", "ORDER BY %s" % (", ".join(orderings)))
        else:
            sql = sql.replace("{ORDERINGS}", "")

        sql = sql.replace("{INNER_SELECTS}", ", ".join(inner_selects))
        sql_count = sql_count.replace("{INNER_SELECTS}", ", ".join(inner_selects))
        sql = sql.replace("{WHERES}", "WHERE %s" % (" AND ".join(wheres)))
        sql_count = sql_count.replace("{WHERES}", "WHERE %s" % (" AND ".join(wheres)))

        items = []
        count = None

        async with get_db_cur() as cur:
            try:
                await cur.execute(sql, (
                    AsIs(user.get("tki")),
                    # AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    form_id,
                    limit,
                    offset,
                ))
            except Exception as e:
                print(e)

            items = await cur.fetchall()

            await cur.execute(sql_count, (
                AsIs(user.get("tki")),
                # AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            count = await cur.fetchone()
            count = count.get('total_reports')

        for item in items:
            if item.get("location_id", None) is not None:
                item['location_full'] = await Location.get_full_name(item.get("location_id"), user=user)

        return dict(
            results=items,
            count=count
        )

    @staticmethod
    async def amend_report(report_uuid, data, reason, user=None, application=None):
        """ Trigger an amendment of a specific report

        Args:
            report_uuid: The UUID of the report
            updated_data: The updated data to amend into the report
            user: The calling (amending) user

        Returns:

        """
        if user.get("role") in ("SUPER_ADMIN", "GLOBAL_ADMIN", "ACCOUNT_ADMIN"):
            result = await Submission._perform_amend(report_uuid, data, reason, user=user, application=application)
        else:

            form = None
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT f.features
                    FROM %s.collections AS c
                      LEFT JOIN %s.forms AS f ON f.id = c.form_id
                    WHERE c.uuid = %s;
                """, (
                    AsIs(user.get('tki')),
                    AsIs(user.get('tki')),
                    report_uuid,
                ))
                form = await cur.fetchone()

            if form is not None:
                amend_approval = form.get('features', {}).get('AMENDMENTS', {}).get('approval', True)

            if amend_approval is True:
                result = await Submission._request_amend(report_uuid, data, reason, user=user, application=application)
            else:
                result = await Submission._perform_amend(report_uuid, data, reason, user=user, application=application)

        return True

    @staticmethod
    async def _perform_amend(report_uuid, data, reason, user=None, application=None):
        """ Perform an immediate amendment to a report

        Args:
            report_uuid: The uuid of the report
            reason: The reason for the amendment
            user: The calling user

        Returns:

        """
        original_report = None
        new_report = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.collections
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                report_uuid,
            ))
            original_report = await cur.fetchone()

        changes = Differential.get_diff(original_report, data)

        hist = original_report.get("history", [])

        hist.append(dict(
            event_type="AMENDED",
            created=datetime.datetime.now(),
            user_id=user.get("id"),
            name=user.get("name")
        ))

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.collections
                SET data = %s,
                    last_modified = %s,
                    data_date = %s,
                    location_id = %s,
                    history = %s
                WHERE uuid = %s
                RETURNING *;
            """, (
                AsIs(user.get("tki")),
                json.dumps(data.get('data')),
                datetime.datetime.now(),
                data.get("data_date"),
                data.get("location_id"),
                json.dumps(hist, cls=JSONEncoder),
                report_uuid,
            ))
            new_report = await cur.fetchone()

            await cur.execute("""
                INSERT INTO %s.amendments
                (report_id, original, amended, amended_date, amendment_reason, status, form_id, amended_by)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
            """, (
                AsIs(user.get("tki")),
                new_report.get("uuid"),
                json.dumps(original_report, cls=JSONEncoder),
                json.dumps(data, cls=JSONEncoder),
                datetime.datetime.now(),
                reason,
                "OK",
                new_report.get("form_id"),
                user.get("id"),
            ))

        return new_report

    @staticmethod
    async def _request_amend(report_uuid, data, reason, user=None, application=None):
        """ Request that a report be amended

        Args:
            report_uuid:
            reason:
            user:

        Returns:

        """
        # TODO: Create the amendment request
        result = await tasks.create_task("AMEND", report_uuid, user, data, reason, user=user)

        return result

    @staticmethod
    async def get_amendments(collection_id, user=None):
        """ Get amendments associated with a report

        Args:
            collection_id: the UUID of the form submission
            user: The calling user

        Returns:

        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.v_amendments WHERE report_id = %s;
            """, (
                AsIs(user.get("tki")),
                collection_id,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def submit(form_id, version_id, data, source='SYSTEM', user=None):
        """ Wraps the submission logic with a catch and dumps the report data
        to a db table for recovery

        Args:
            form_id:
            version_id:
            data:
            source:
            user:

        Returns:

        """

        result = None
        try:
            result = await Submission._submit(form_id, version_id, data, source=source, user=user)
        except Exception as e:
            print(e)
            async with get_db_cur() as cur:
                await cur.execute("""
                 INSERT INTO %s.submit_recover (data, exc)
                 VALUES (%s, %s);
                """, (
                    AsIs(user.get('tki')),
                    json.dumps(dict(
                        fid=form_id,
                        vid=version_id,
                        data=data,
                        source=source,
                        uid=user.get('id')
                    ), cls=JSONEncoder),
                    str(e),
                ))

        # REMOVE: This is here as a temporary stop-gap until workflows are in place
        # bangladesh; generate a contact tracing form submission for amendment
        if form_id == 11 and user.get('tki', None) == '_21bce401c88d':
            new_report = dict()
            old_data = data.get('data', {})
            new_data = dict(
                case_id=old_data.get('case_id', None),
                surname=old_data.get('name_patient', None),
                name_father=old_data.get('name_father', None),
                family_card=old_data.get('family_card', None),
                phone=old_data.get('phone', None),
                age=old_data.get('age', None),
                date_birth=old_data.get('date_birth', None),
                sex=old_data.get('sex', None),
                nationality=old_data.get('nationality', None),
                occupation=old_data.get('occupation', None),
                name_school=old_data.get('name_school', None),
                name_mahji=old_data.get('name_mahji', None),
                phone_mahji=old_data.get('phone_mahji', None),
                mosque_name=old_data.get('mosque_name', None),
                imam_name=old_data.get('imam_name', None),
                block=old_data.get('block', None),
                camp_site=old_data.get('camp_site', None),
                house_no=old_data.get('house_number', None),
                previous_health_facility=old_data.get('previous_health_facility', None),
                found_case='NO',
                date_visit_previous_health_facility=old_data.get('date_visit_previous_health_facility', None)
            )

            zone_pcode = old_data.get('zone_select', None)

            if zone_pcode is not None:
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT name->>'en' AS name
                        FROM %s.locations
                        WHERE uuid = %s;
                    """, (
                        AsIs(user.get('tki')),
                        data.get('location_id'),
                    ))
                    location = await cur.fetchone()
                    if location is not None:
                        new_data['name_dtc'] = location.get('name', 'No location found')

                    await cur.execute("""
                        SELECT uuid::TEXT as id
                        FROM %s.locations
                        WHERE pcode = %s;
                    """, (
                        AsIs(user.get('tki')),
                        zone_pcode,
                    ))
                    end_location = await cur.fetchone()
                    new_report['location_id'] = end_location.get('id', None)

                new_report['data_date'] = data.get('data_date', None)
                new_report['created_by'] = user.get('id')
                new_report['form_id'] = 12
                new_report['form_version_id'] = data.get('form_version_id', None)
                new_report['data'] = new_data

                sec_result = await Submission._submit(12, '85e26753-5174-4218-9bf0-699cb60b3363', new_report,
                                                      source=source,
                                                      user=user)

        return result

    @staticmethod
    async def _submit(form_id, version_id, data, source="SYSTEM", user=None):
        """ Submit a report in to the system

        Args:
            form_id: The form id the submission correlates to
            version_id: The version of the form to use for the submission
            data: The data for the report
            user:

        Returns:

        """
        result = None

        form = None
        form_version = None

        now = datetime.datetime.utcnow()

        exists = None

        isDuplicate = False

        if data.get("uuid", None) not in ("None", None):
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.collections
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    data.get("uuid"),
                ))
                exists = await cur.fetchone()

        # Check for duplicate report
        dupes = []
        if data.get("location_id", None) is not None:
            loc_id = data.get("location_id")
            if loc_id.lower() == "null":
                data['location_id'] = None

            if loc_id == '':
                data['location_id'] = None

        if data.get("location_id", None) is not None:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid, location_id, data_date
                    FROM %s.collections
                    WHERE form_id = %s
                        AND data_date = %s
                        AND location_id = %s
                        AND status = 'SUBMITTED'
                """, (
                    AsIs(user.get("tki")),
                    data.get("form_id"),
                    data.get('data_date'),
                    data.get("location_id"),
                ))
                dupes = await cur.fetchall()

        isMultiple = False
        if len(dupes) > 0:
            isMultiple = True

        # Get the form information
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.forms
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

            # If the version_id is None, get the latest version id
            if version_id is None:
                version_id = form.get("version_id")

        if "INTERVAL_REPORTING" in form.get("features", dict()):
            if isMultiple:
                return dict(
                    error="A report already exists for this location and time",
                    result=False
                )

        history = [dict(created=now, event_type="SUBMITTED")]

        if exists is None:
            new_uuid = str(uuid.uuid4())
            no_form = new_uuid.split("-")[-1]
            eid = "-".join([no_form[i:i + 4] for i in range(0, len(no_form), 4)])
            eid = "0B-%s" % (eid,)

            if form.get("eid_prefix", None) not in ("", None):
                eid = "%s-%s" % (
                    form.get("eid_prefix"),
                    eid,
                )

            eid = eid.upper()

            async with get_db_cur() as cur:
                await cur.execute("""
                    INSERT INTO %s.collections
                    (
                        uuid,
                        submitted_date,
                        form_id,
                        created_by,
                        created,
                        last_modified,
                        status,
                        data,
                        form_version_id,
                        data_date,
                        location_id,
                        history,
                        revisions,
                        source,
                        eid
                    )
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
                """, (
                    AsIs(user.get("tki")),
                    new_uuid,
                    now,
                    form_id,
                    user.get("id"),
                    now,
                    now,
                    'SUBMITTED',
                    json.dumps(data.get('data', dict())),
                    version_id,
                    data.get("data_date", None),
                    data.get("location_id", None),
                    json.dumps(history, cls=JSONEncoder),
                    json.dumps([], cls=JSONEncoder),
                    source,
                    eid,
                ))
                report = await cur.fetchone()
                report = dict(report)

        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.collections
                    SET data_date = %s,
                        location_id = %s,
                        submitted_date = %s,
                        last_modified = %s,
                        data = %s,
                        status = 'SUBMITTED',
                        source = %s
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    data.get('data_date'),
                    data.get("location_id"),
                    datetime.datetime.utcnow(),
                    datetime.datetime.utcnow(),
                    json.dumps(data.get('data')),
                    source,
                    data.get("uuid"),
                ))

                await cur.execute("""
                    SELECT *
                    FROM %s.collections
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    data.get("uuid"),
                ))
                report = await cur.fetchone()
                report = dict(report)

        cur_loop = asyncio.get_event_loop()
        cur_loop.create_task(Alarm.evaluate_submission(report, user=user))
        cur_loop.create_task(_submit_activity(report, form, user=user, token=user.get('tki')))

        return dict(
            result=True,
            report=report
        )

    @staticmethod
    async def re_evaluate(collection_id, user=None):
        # Re-evaluates an alert against an alarm
        tornado.ioloop.IOLoop.current().spawn_callback(Alarm.evaluate_submission,
                                                       {"uuid": collection_id, "status": "SUBMITTED"}, user=user)

        return True

    @staticmethod
    async def update(id, data, user=None):
        """ Update a report

        Args:
            id:
            data:
            user:

        Returns:

        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.collections
                    SET data_date = %s,
                      location_id = %s,
                      data = %s,
                      last_modified = NOW()
                    WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("data_date"),
                data.get("location_id"),
                json.dumps(data.get('data')),
                id,
            ))

            await cur.execute("""
                SELECT data_date, location_id, data
                FROM %s.collections
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def patch_collection(report_uuid, prop, value, user=None):
        """ Perform a single propert update to a specific form.

        Args:
            report_uuid: THe UUID of the report to update
            prop: The property to update
            value: The value to update it with
            user: The calling user

        Returns:

        """
        if prop.startswith("data."):
            # We're updating a data property
            real_prop = ".".join(prop.split(".")[1:])
            data = None

            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT data
                    FROM %s.collections
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    report_uuid,
                ))
                data = await cur.fetchone()['data']

            data[real_prop] = value

            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.collections
                    SET data = %s,
                        last_modified = %s
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    json.dumps(data),
                    datetime.datetime.now(),
                    report_uuid,
                ))
        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    UPDATE %s.collections
                    SET %s = %s,
                        last_modified = %s
                    WHERE uuid = %s
                """, (
                    AsIs(user.get("tki")),
                    AsIs(prop),
                    value,
                    datetime.datetime.now(),
                    report_uuid,
                ))

        # ret = await EventLog.add_event(
        #     'RES_CHG',
        #     user.get('id'),
        #     user.get('did', None),
        #     dict(
        #         res='RECORD',
        #         id=report_uuid,
        #         d=[prop, value]
        #     ),
        #     user=user
        # )

        return True

    @staticmethod
    async def retract(report_uuid, retraction_reason, user=None):
        """ Perform a retraction of a report

        Args:
            report_uuid: The UUID of the collected form
            retraction_reason: The reason for the retraction
            user: THe calling user

        Returns:

        """
        result = None
        if user.get('role') in ('ACCOUNT_ADMIN', 'REGIONAL_ADMIN'):
            result = await Submission._perform_retraction(report_uuid, retraction_reason, user=user)
        else:
            result = await tasks.create_task('RETRACT', report_uuid, user, retraction_reason)

        return result

    @staticmethod
    async def search(form_id, param, filters, ordering, limit, offset, count=None, user=None):
        inner_selects = [
            "r.*",
            "l.name AS location_name",
            "l.lineage",
            "u.group_id AS user_group_id",
            "u.account_id AS user_account_id",
            "u.name AS creator_name",
            "sti.name->>'en' AS sti_name"
            # "(SELECT ARRAY(SELECT lt.name->>'en' FROM %s.locations AS lt WHERE lt.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name"
        ]

        sql = """
        SELECT * FROM (
            SELECT {INNER_SELECTS}
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
                LEFT JOIN _iw.users AS u ON u.id = r.created_by
                LEFT JOIN %s.location_types AS sti ON sti.id = l.site_type_id
            ) AS ss
        {WHERES}
        {ORDERINGS}
        LIMIT %s
        OFFSET %s
        """

        wheres = [
            'ss.form_id = %s',
            "ss.status = 'SUBMITTED'"
        ]

        # location_where = None
        # if user.get('role') == 'USER':
        #     assignments = []
        #     async with get_db_cur() as cur:
        #         await cur.execute("""
        #             SELECT location_id
        #             FROM %s.assignments
        #             WHERE form_id = %s
        #               AND user_id = %s
        #               AND status = ANY(ARRAY['ACTIVE', 'INACTIVE']);
        #         """, (
        #             AsIs(user.get('tki')),
        #             form_id,
        #             user.get('id'),
        #         ))
        #         assignments = await cur.fetchall()
        #
        #     if "location_id" not in filters.keys():
        #         wheres.append("ss.location_id::TEXT = ANY(ARRAY['%s']) " % (
        #             "','".join(str(x.get('location_id')) for x in assignments),
        #         ))

        orderings = []

        # Build count query
        sql_count = """
            SELECT COUNT(*) AS total_reports FROM (
            SELECT {INNER_SELECTS}
            FROM %s.collections AS r
                LEFT JOIN %s.locations AS l ON l.uuid = r.location_id
                LEFT JOIN _iw.users AS u ON u.id = r.created_by
                LEFT JOIN %s.location_types AS sti ON sti.id = l.site_type_id
            ) AS ss
        {WHERES}
        """

        for key, filter in filters.items():
            el_type = filter.get("type", None)
            if el_type is not None:
                del filter['type']
            else:
                el_type = "TEXT"

            comp_key, value = filter.popitem()

            if el_type == "user":
                if comp_key == "eq":
                    wheres.append("ss.%s IN (%s)" % (
                        key,
                        ",".join([str(x) for x in value])
                    ))
                elif comp_key == "neq":
                    wheres.append("ss.%s NOT IN (%s)" % (
                        key,
                        ",".join([str(x) for x in value])
                    ))
            else:

                if comp_key == "in":
                    wheres.append("ss.%s::TEXT IN ('%s')" % (
                        key,
                        "','".join(value),
                    ))
                elif comp_key.upper() == "UNDER":
                    wheres.append("ss.lineage::TEXT[] @> '{%s}'::TEXT[]" % (
                        value,
                    ))
                else:
                    if key.startswith("data."):
                        # implement a cast for specific types, if the type is passed
                        if el_type is not None:
                            cast_type = CAST_TYPES.get(el_type)
                            if cast_type == "NUMERIC":
                                # COALESCE(CAST(c.data->>'{FIELD}' AS NUMERIC), 0) AS {INNER_ALIAS},
                                wheres.append("COALESCE(CAST(ss.data->>'%s' AS NUMERIC), 0) %s %s" % (
                                    key.replace("data.", ""),
                                    COMPARATORS[comp_key],
                                    value,
                                ))
                            else:
                                if comp_key.upper() == "CONTAINS":
                                    _ql = "TRIM(CAST(ss.data->>'%s' AS %s)) ILIKE '%%%s%%'" % (
                                        key.replace('data.', ''),
                                        cast_type,
                                        value.strip(),
                                    )
                                    _ql = _ql.replace("%", "%%")
                                    wheres.append(_ql)

                                else:
                                    wheres.append("TRIM(CAST(ss.data->>'%s' AS %s)) %s '%s'" % (
                                        key.replace('data.', ''),
                                        cast_type,
                                        COMPARATORS[comp_key],
                                        value.strip(),
                                    ))
                        else:
                            wheres.append("TRIM(ss.data->>'%s') %s '%s'" % (
                                key.replace("data.", ""),
                                COMPARATORS[comp_key],
                                str(value).strip(),
                            ))
                    else:
                        if el_type == "date":
                            wheres.append("ss.%s::DATE %s '%s'::DATE" % (
                                key,
                                COMPARATORS[comp_key],
                                value
                            ))
                        else:
                            wheres.append("ss.%s %s '%s'" % (
                                key,
                                COMPARATORS[comp_key],
                                value
                            ))

        ft_sql = " AND ".join(wheres)
        # Add to main sql
        # sql += ft_sql
        # Add to coutn query

        orderings = []
        for key, dir in ordering.items():
            direction = dir
            field_type = None
            if ":" in dir:
                direction, field_type = dir.split(":")

            if key.startswith("data."):
                # This is a nested JSON attributed, we need
                # to do some conversion to the key to make
                # it work, these are slow queries
                # so need to look up a way to make it run a little faster
                cleaned = key.replace("data.", "")
                new_key = "data->>'%s'" % (cleaned,)
                if field_type == "number":
                    orderings.append("ss.orderer::NUMERIC %s" % (direction))
                elif field_type in ("text", "select", "textarea"):
                    orderings.append("ss.orderer::TEXT %s" % (direction))
                elif field_type == "DATE":
                    orderings.append("ss.orderer::DATE %s" % (direction))
                else:
                    orderings.append("ss.orderer::TEXT %s" % (direction))
                inner_selects.append("r.%s AS orderer" % (new_key))
            elif key == "creator_name":
                # We're ordering on a join
                orderings.append("ss.creator_name %s" % (direction))
            elif key == "location_name":
                orderings.append("ss.location_name->>'en' %s" % (direction))
            else:
                orderings.append("ss.%s %s" % (key, direction))

        if len(orderings) > 0:
            sql = sql.replace("{ORDERINGS}", "ORDER BY %s" % (", ".join(orderings)))
        else:
            sql = sql.replace("{ORDERINGS}", "")

        sql = sql.replace("{INNER_SELECTS}", ", ".join(inner_selects))
        sql_count = sql_count.replace("{INNER_SELECTS}", ", ".join(inner_selects))
        sql = sql.replace("{WHERES}", "WHERE %s" % (" AND ".join(wheres)))
        sql = sql + " AND data::TEXT ILIKE '__%s__'" % (param,)
        sql = sql.replace("__", "%%")
        sql_count = sql_count.replace("{WHERES}", "WHERE %s" % (" AND ".join(wheres)))
        sql_count = sql_count + " AND data::TEXT ILIKE '__%s__'" % (param,)
        sql_count = sql_count.replace("__", "%%")

        items = []
        count = None

        async with get_db_cur() as cur:
            try:
                await cur.execute(sql, (
                    AsIs(user.get("tki")),
                    # AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    form_id,
                    limit,
                    offset,
                ))
            except Exception as e:
                print(e)

            items = await cur.fetchall()

            await cur.execute(sql_count, (
                AsIs(user.get("tki")),
                # AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            count = await cur.fetchone()
            count = count.get('total_reports')

        for item in items:
            if item.get("location_id", None) is not None:
                item['location_full'] = await Location.get_full_name(item.get("location_id"), user=user)

        return dict(
            results=items,
            count=count
        )

    @staticmethod
    async def _perform_retraction(report_uuid, reason, user=None):
        """ Immediately retract a collected item from the system

        Args:
            report_uuid: The uuid of the collected item
            reason: THe reason for the retraction
            user: The calling user

        Returns:

        """

        alerts = []
        record = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, form_id, location_id
                FROM %s.collections
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                report_uuid,
            ))
            record = await cur.fetchone()

            await cur.execute("""
                DELETE FROM %s.collections WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                report_uuid,
            ))

            await cur.execute("""
                SELECT uuid
                FROM %s.alerts
                WHERE triggering_report_id = %s
            """, (
                AsIs(user.get("tki")),
                report_uuid,
            ))
            alerts = await cur.fetchall()

        form_id = record.get("form_id")

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.retractions
                (report_id, retracted_by, retracted_date, retraction_reason, status, form_id)
                VALUES (%s, %s, %s, %s, %s, %s);
            """, (
                AsIs(user.get("tki")),
                report_uuid,
                user.get("id"),
                datetime.datetime.now(),
                reason,
                "OK",
                form_id,
            ))

            for alert in alerts:
                await cur.execute("""
                        DELETE FROM %s.alert_events WHERE alert_id = %s;
                        DELETE FROM %s.alert_actions WHERE alert_id = %s;
                        DELETE FROM %s.alert_users WHERE alert_id = %s;
                        DELETE FROM %s.alerts WHERE uuid = %s;
                    """, (
                    AsIs(user.get("tki")),
                    alert.get("uuid"),
                    AsIs(user.get("tki")),
                    alert.get("uuid"),
                    AsIs(user.get("tki")),
                    alert.get("uuid"),
                    AsIs(user.get("tki")),
                    alert.get("uuid"),
                ))

        return True


async def _submit_activity(report, form, user=None, token=None):
    """ Asynchronously insert an item into the activity feed

    Args:
        report:
        form:
        user:
        token:

    Returns:

    """
    async with get_db_cur() as cur:
        await cur.execute("""
            INSERT INTO %s.activity_feed
            (event_type, triggered_by, created, icon, attachments)
            VALUES (%s, %s, %s, %s, %s);
        """, (
            AsIs(user.get("tki")),
            "REPORT_SUBMISSION",
            user.get("id"),
            datetime.datetime.now(),
            "fa-clipboard",
            json.dumps([["REPORT", report.get("uuid")]], cls=JSONEncoder)
        ))
