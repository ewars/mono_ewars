import datetime
import json

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs


class LocationType:
    @staticmethod
    async def get_all(user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * 
                FROM %s.location_types
                ORDER BY name->>'en';
            """, (
                AsIs(user.get("tki")),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def update(id, data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.location_types
                SET name = %s,
                    description = %s,
                    status = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                json.dumps(data.get("name")),
                data.get('description'),
                data.get('status'),
                id,
            ))

            await cur.execute("""
                SELECT * FROM %s.location_types WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def create(data, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.location_types
                (name, description, status, created, created_by)
                VALUES (%s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                json.dumps(data.get("name")),
                data.get("description", ""),
                data.get('status'),
                datetime.datetime.now(),
                user.get("id"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(id, user=None):
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.locations
                SET site_type_id = 1
                WHERE site_type_id = %s
            """, (
                AsIs(user.get("tki")),
                id,
            ))

            await cur.execute("""
                DELETE FROM %s.location_types WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                id,
            ))

        return True


