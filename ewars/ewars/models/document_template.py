import json
import datetime
import os

from ewars.db import get_db_cur
from ewars.core.serializers import JSONEncoder
from ewars.utils import date_utils, form_utils
from ewars.utils.isoweek import Week

from psycopg2.extensions import AsIs

from tornado.template import Template as TornTemplate


class DocumentTemplate:
    @staticmethod
    async def query_templates(data, user=None):

        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT * FROM %s.document_templates
                 ORDER BY name;
            """, (
                AsIs(user.get('tki')),
            ))
            results = await cur.fetchall()

        return dict(
            d=list(results)
        )

    @staticmethod
    async def get_template(template_id, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.document_templates
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                template_id,
            ))
            result = await cur.fetchone()

        return dict(result)

    @staticmethod
    async def update_template(template_id, data, user=None):
        """ Update a template within the system

        Args:
            template_id:
            template_data:
            token:

        Returns:

        """

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.templates
                SET name = %s,
                  instance_name = %s,
                  status = %s,
                  permissions = %s,
                  generation = %s,
                  theme = %s,
                  pages = %s,
                  data = %s,
                  modified_by = %s
                WHERE uuid = %s;
                  
            """, (
                AsIs(user.get('tki')),
                data.get('name'),
                data.get('instance_name'),
                data.get('status'),
                data.get('permissions'),
                data.get('generation'),
                data.get('theme'),
                data.get('pages'),
                data.get('data'),
                user.get('id'),
                template_id,
            ))

        return dict(success=True)

    @staticmethod
    async def delete_template(template_id, user=None):
        """ Delete a template from the system

        Args:
            template_id:
            user:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.document_templates
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                template_id,
            ))

        return dict(success=True)

    @staticmethod
    async def create_template(data, user=None):
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.document_templates (name, instance_name, status, permissions, generation, theme, pages, data, created_by, modified_by)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get('tki')),
                data.get('name'),
                data.get('instance_name'),
                data.get('status', 'DRAFT'),
                data.get('permissions', '{}'),
                data.get('generation', '{}'),
                data.get('theme', 'DEFAULT'),
                data.get('pages', '[]'),
                data.get('data', '{}'),
                user.get('id'),
                user.get('id'),
            ))
            result = await cur.fetchone()


        return dict(result)



