import json
import uuid
import datetime

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

from ewars.constants import CONSTANTS
from ewars.core import legacy_indicators
from ewars.constants import system_indicators as ind_defs


class Indicator:
    @staticmethod
    async def inbound(ind_id, user=None):
        """ Find forms which are configured to push data to this indicator

        Args:
            ind_id: The UUID of the indicator
            user: The calling user

        Returns:
            A list of forms which contribute to this indicators data
        """
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.id, f.name
                FROM %s.forms AS f
                LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE fv.etl ? %s
                    AND f.status = 'ACTIVE'
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                ind_id,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def query(parent, user=None):
        """Query against the indicators within the system

        Args:
            parentId: The parentId to query under
            user: The calling user

        Returns:
            A list of folders and indicators to render
        """
        results = []

        if parent is None:
            # Getting groups and root-level indicators
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT id, name, (SELECT 'FOLDER') AS context, (SELECT 'ACTIVE') AS status
                        FROM %s.indicator_groups
                        WHERE parent_id IS NULL
                        ORDER BY name->>'en' ASC
                """, (
                    AsIs(user.get("tki")),
                ))
                results = await cur.fetchall()


        else:
            folders = []
            indicators = []
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT id, name, (SELECT 'FOLDER') AS context, (SELECT 'ACTIVE') AS status
                        FROM %s.indicator_groups
                        WHERE parent_id = %s
                        ORDER BY name->>'en' ASC
                """, (
                    AsIs(user.get("tki")),
                    parent,
                ))
                folders = await cur.fetchall()

                await cur.execute("""
                    SELECT uuid, name, (SELECT 'INDICATOR') AS context, itype, status, definition
                        FROM %s.indicators
                        WHERE group_id = %s
                        ORDER BY name->>'en' ASC
                """, (
                    AsIs(user.get("tki")),
                    parent,
                ))
                indicators = await cur.fetchall()

            if folders is None:
                folders = []

            if indicators is None:
                indicators = []

            results = folders + indicators

        return results

    @staticmethod
    async def create(data, user=None):
        """Create a new indicator

        Args:
            data: The new indicators settings
            user: The calling user

        Returns:
            The newly created indicator
        """
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.indicators
                    (group_id, created_by, name, status, protected, created, last_modified, ranked, colour, itype, definition, description, guidance)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("group_id", None),
                user.get('id'),
                json.dumps(data.get("name", {})),
                data.get("status"),
                data.get("protected", False),
                datetime.datetime.now(),
                datetime.datetime.now(),
                data.get("ranked", False),
                data.get("colour", None),
                data.get("itype", "DEFAULT"),
                json.dumps(data.get("definition", {})),
                json.dumps(data.get("description", {})),
                json.dumps(data.get("guidance", {})),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(indicator_id, user=None):
        """Delete an indicator from the system

        Args:
            indicator_id: The UUID of the indicator to delete
            user: The calling user

        Returns:
            True|False whether the operation was successhul
        """

        await Indicator._delete_indicator(indicator_id, user=user)

        return True

    @staticmethod
    async def update(indicator_id, data, user=None):
        """Update an existing indicator

        Args:
            indicator_id: The UUID of the indicator
            data: The data to update
            user: The calling user

        Returns:
            The updated indicator
        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.indicators
                SET group_id = %s,
                    name = %s,
                    status = %s,
                    last_modified = %s,
                    ranked = %s,
                    colour = %s,
                    itype = %s,
                    definition = %s,
                    description = %s,
                    guidance = %s
                WHERE uuid = %s
                RETURNING *
            """, (
                AsIs(user.get("tki")),
                data.get("group_id"),
                json.dumps(data.get("name")),
                data.get("status"),
                datetime.datetime.now(),
                data.get("ranked", False),
                data.get("colour", None),
                data.get("itype", "DEFAULT"),
                json.dumps(data.get("definition", {})),
                json.dumps(data.get("description", {})),
                json.dumps(data.get("guidance", {})),
                indicator_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def _delete_indicator(ind_id, user=None):
        """ Remove an indicator from the system as a result of the deletion
        of an indicator gropu

        Args:
            ind_id: The UUID of the indicator to remove
            user: The calling user

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.indicators WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                ind_id,
            ))

        return True

    @staticmethod
    async def get_all_inds(user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid, name, itype
                FROM %s.indicators
                WHERE status = 'ACTIVE';
            """, (
                AsIs(user.get("tki")),
            ))
            results = await cur.fetchall()

        return results
