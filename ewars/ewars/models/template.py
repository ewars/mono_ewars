import json
import datetime
import os

from ewars.db import get_db_cur
from ewars.core.serializers import JSONEncoder
from ewars.utils import date_utils, form_utils
from ewars.utils.isoweek import Week

from psycopg2.extensions import AsIs

from tornado.template import Template as TornTemplate


class Template:
    @staticmethod
    async def update(template_id, data, user=None):
        """ Update a template within the system

        Args:
            template_id:
            template_data:
            token:

        Returns:

        """

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.templates
                SET template_name = %s,
                    instance_name = %s,
                    description = %s,
                    content = %s,
                    status = %s,
                    generation = %s,
                    shareable = %s,
                    access = %s,
                    data = %s,
                    template_type = %s,
                    orientation = %s
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                json.dumps(data.get("template_name")),
                json.dumps(data.get("instance_name")),
                json.dumps(data.get("description")),
                data.get("content"),
                data.get("status"),
                json.dumps(data.get("generation")),
                data.get("shareable"),
                data.get("access"),
                json.dumps(data.get('data'), cls=JSONEncoder),
                data.get("template_type", "GENERATED"),
                data.get("orientation", "PORTRAIT"),
                template_id,
            ))

    @staticmethod
    async def create(data, user=None):
        """ Create a new template within the system

        Args:
            template_data:
            token:

        Returns:

        """
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
            INSERT INTO %s.templates
            (created_by, template_name, instance_name, data, description, created, content, status, generation, shareable, access, template_type, orientation)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                user.get("id"),
                json.dumps(data.get("template_name")),
                json.dumps(data.get("instance_name")),
                json.dumps(data.get('data')),
                json.dumps(data.get("description", {})),
                datetime.datetime.now(),
                data.get('content'),
                data.get("status", "DRAFT"),
                json.dumps(data.get("generation")),
                data.get('shareable'),
                data.get("access"),
                data.get("template_type"),
                data.get("orientation", "PORTRAIT"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(template_id, user=None):
        """ Delete a template from the system

        Args:
            template_id:
            token:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.templates WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                template_id
            ))

    @staticmethod
    async def duplicate(template_id, user=None):
        """ Duplicate a template within the system

        Args:
            template_id:
            token:

        Returns:

        """
        template = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.templates WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                template_id,
            ))
            template = await cur.fetchone()

        result = await Template.create(template, user=user)

        return result

    @staticmethod
    async def clear_cache(template_id, user=None):
        """ Clear a templates PDF download cache
        
        Args:
            template_id: 
            user: 

        Returns:

        """
        items = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT filename FROM %s.template_cache
                WHERE template_id = %s;
            """, (
                AsIs(user.get("tki")),
                template_id,
            ))
            items = await cur.fetchall()

        for item in items:
            try:
                os.remove(item.get("filename"))
            except OSError:
                pass

        async with get_db_cur() as cur:
            await cur.execute("""
                DELETE FROM %s.template_cache 
                WHERE template_id = %s;
            """, (
                AsIs(user.get("tki")),
                template_id,
            ))

        return True

    @staticmethod
    async def find_template(template_id):
        """ Search through the system for a template

        Args:
            template_id: THe UUID of the template

        Returns:

        """

        instances = ["ewars"]

        template = None

        for instance in instances:
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT * FROM %s.templates WHERE uuid = %s
                """, (
                    AsIs(instance),
                    template_id,
                ))
                result = await cur.fetchone()

                if result is not None:
                    template = (instance, result)

        return template

    @staticmethod
    def format_document_title(instance_name, definition=None, report_date=None, template_type=None, report_uuid=None, location=None):
        result = ""

        if isinstance(instance_name, (dict,)):
            result = instance_name.get("en")
        else:
            result = instance_name

        result = instance_name.get("en")

        nodes = [
            "year",
            "report_date",
            "report_start_date",
            "report_end_date",
            "month",
            "year",
            "day",
            "location",
            "day_week",
            "week_no",
            "week",
            "location_name",
            "name",
            "today"
        ]

        r_date = date_utils.parse_date(report_date)

        start_date = date_utils.get_start_of_interval(r_date, definition['generation']['interval'])
        week_no = Week.withdate(r_date).week
        week = Week.withdate(r_date).day(6)

        location_name = "No associated location"
        if location is not None:
            location_name = location.get("name")['en']

        interval = None
        if template_type == "GENERATED":
            interval = definition['generation'].get("interval", "DAY")

        today = datetime.datetime.utcnow()

        result = result.replace("{year}", str(r_date.year))
        result = result.replace("{report_date}", date_utils.display(r_date, interval))
        result = result.replace("{report_start_date}", start_date.strftime("%Y-%m-%d"))
        result = result.replace("{report_end_date}", r_date.strftime("%Y-%m-%d"))
        result = result.replace("{month}", str(r_date.month))
        result = result.replace("{day}", str(r_date.day))
        result = result.replace("{location}", location_name)
        result = result.replace("{day_week}", r_date.strftime("dddd"))
        result = result.replace("{week_no}", str(week_no))
        result = result.replace("{week}", str(week_no))
        result = result.replace("{location_name}", location_name)
        result = result.replace("{today}", today.strftime("%H:%M %A, %d %B %Y"))

        # result = result.replace("Raw Value", "")

        return result


    @staticmethod
    async def format_template(content, definition=None, report_date=None, location=None, template_type=None, report_uuid=None):
        """ Format a template for rendering

        Args:
            content: The content of the template
            definition: The definition for the template
            report_date: The report date for the template
            location: The location of the report

        Returns:

        """
        result = content

        collected = None

        if template_type == "PER_REPORT":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT c.uuid,
                        c.data_date,
                        c.data,
                        f.name AS form_name,
                        v.definition AS definition
                    FROM %s.collections AS c
                        LEFT JOIN %s.forms AS f on f.id = c.form_id
                        LEFT JOIN %s.form_versions AS v ON v.uuid = c.form_version_id
                    WHERE c.uuid = %s
                """, (
                    report_uuid,
                ))
                collected = await cur.fetchone()

        def get_field(field_path):
            return collected['data'].get(field_path, "Not found")

        def get_field_display(field_path):
            field = form_utils.get_field_definition(collected.get("definition"), field_path)

            value = collected['data'].get(field_path, None)

            if value is None:
                return "Not found"

            if field is None:
                return "Not found"

            disp_value = "Not found"
            if field.get("type") == "select":
                for opt in field.get("options"):
                    if opt[0] == value:
                        disp_value = opt[1]
            elif field.get("type") == "date":
                date_utils.display(value, field.get("date_type", "DAY"))

            return disp_value

        nodes = [
            "year",
            "report_date",
            "report_start_date",
            "report_end_date",
            "month",
            "year",
            "day",
            "location",
            "day_week",
            "week_no",
            "week",
            "location_name",
            "name",
            "today"
        ]

        t = TornTemplate(result)

        r_date = date_utils.parse_date(report_date)

        start_date = date_utils.get_start_of_interval(r_date, definition['generation']['interval'])
        week_no = Week.withdate(r_date).week
        week = Week.withdate(r_date).day(6)

        location_name = "No associated location"
        if location is not None:
            location_name = location.get("name")['en']

        interval = None
        if template_type == "GENERATED":
            interval = definition['generation'].get("interval", "DAY")

        today = datetime.datetime.utcnow()
        result = t.generate(
            year=str(r_date.year),
            report_date=date_utils.display(r_date, interval),
            report_start_date=start_date.strftime('%Y-%m-%d'),
            report_end_date= r_date.strftime("%Y-%m-%d"),
            month=str(r_date.month),
            day=str(r_date.day),
            location=location_name,
            day_week=r_date.strftime("dddd"),
            week_no=str(week_no),
            week=str(week_no),
            get_field=get_field,
            get_field_display=get_field_display,
            location_name=location_name,
            today=today.strftime("%H:%M %A, %d %B %Y"),
            name=""
        )

        result = result.decode("utf-8")
        result = result.replace("{year}", str(r_date.year))
        result = result.replace("{report_date}", date_utils.display(r_date, interval))
        result = result.replace("{report_start_date}", start_date.strftime("%Y-%m-%d"))
        result = result.replace("{report_end_date}", r_date.strftime("%Y-%m-%d"))
        result = result.replace("{month}", str(r_date.month))
        result = result.replace("{day}", str(r_date.day))
        result = result.replace("{location}", location_name)
        result = result.replace("{day_week}", r_date.strftime("dddd"))
        result = result.replace("{week_no}", str(week_no))
        result = result.replace("{week}", str(week_no))
        result = result.replace("{location_name}", location_name)
        result = result.replace("{today}", "%s UTC" % (today.strftime("%H:%M %A, %d %B %Y")))

        # result = result.replace("Raw Value", "")

        return result

