import json
import uuid
import datetime

from ewars.utils.six import string_types

from ewars.core.form_definition import _extrapolate_grid, _generate_table, _flat_def, _flatten_definition
from ewars.core.serializers import JSONEncoder

from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

GRID_TYPES = ["matrix", "row", "text", "select", "numeric", "date", 'location', 'number', 'user']
DIM_TYPES = ["text", "select", "date", "location"]
IGNORED_FIELD_TYPES = ["header", "matrix", "row", "display"]
MEAS_TYPES = ["number", "numeric"]

GET_ACCESSIBLE_FORMS = """
    SELECT id, uuid, name, status
    FROM %s.forms
    WHERE status = ANY('{ACTIVE, ARCHIVED}');
"""


class Form:

    @staticmethod
    async def accessible_forms(user=None):
        results = []
        async with get_db_cur() as cur:
            await cur.execute(GET_ACCESSIBLE_FORMS, (AsIs(user.get("tki")),))
            results = await cur.fetchall()

        return results


    @staticmethod
    async def get_unique_fields(form_id, user=None):
        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.id, f.features, fv.definition
                FROM %s.forms AS f
                  LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE f.id = %s;
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

        unique_fields = []

        if form.get("features", {}).get("LOCATION_REPORTING", None) is not None:
            unique_fields.append("location_id")

        if form.get("features", {}).get("INTERVAL_REPORTING", None) is not None:
            unique_fields.append("data_date")

        for key, field in form.get("definition", {}):
            if field.get("unique", False) is True:
                unique_fields.append(key)

            if field.get("fields", None) is not None:
                for r_key, r_field in field.get("fields", {}):
                    if r_field.get("unique", False) is True:
                        unique_fields.append("%s.%s" % (key, r_key,))

                    if r_field.get("fields", None) is not None:
                        for c_key, c_field in r_field.get("fields", {}):
                            if c_field.get("unique", False) is True:
                                unique_fields.append("%s.%s.%s", (key, r_key, c_key,))

        return unique_fields

    @staticmethod
    async def get_form(form_id, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.*,
                    fv.definition
                FROM %s.forms AS f
                  LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id;
            """)
            result = await cur.fetchone()

        return result

    @staticmethod
    async def get_active_forms(fields, user=None):
        results = []

        selects = "*"
        if fields is not None:
            selects = ", ".join(fields)

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT %s
                FROM %s.forms
                WHERE status = ANY('{ACTIVE, ARCHIVED}');
            """, (
                AsIs(selects),
                AsIs(user.get("tki")),
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def query(limit, offset, sorters, filters, user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.id, f.name, f.status, f.created, f.last_modified,
                        u.name AS creator_name
                FROM %s.forms AS f
                  LEFT JOIN %s.users AS u ON u.id = f.created_by
                LIMIT %s
                OFFSET %s
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                limit,
                offset,
            ))
            results = await cur.fetchall()

        return results

    @staticmethod
    async def create(data, user=None):
        """ Create a new form

        Args:
            data:
            user:

        Returns:

        """
        result = None
        version_data = data.get('version', {})

        new_version = str(uuid.uuid4())
        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.forms
                (uuid, name, status, created, last_modified, created_by, version_id, guidance, description, features, eid_prefix, exportable)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                str(uuid.uuid4()),
                json.dumps(data.get("name")),
                data.get("status"),
                datetime.datetime.utcnow(),
                datetime.datetime.utcnow(),
                user.get('id'),
                None,
                json.dumps(data.get("guidance", {})),
                json.dumps(data.get("description", {})),
                json.dumps(data.get('features', {})),
                data.get("eid_prefix", None),
                data.get("exportable", True),
            ))
            result = await cur.fetchone()

            await cur.execute("""
                INSERT INTO %s.form_versions
                (uuid, form_id, version, definition, etl)
                VALUES (%s, %s, %s, %s, %s) RETURNING *;
            """, (
                AsIs(user.get("tki")),
                new_version,
                result.get("id"),
                1,
                json.dumps(version_data.get('definition', {})),
                json.dumps(version_data.get("etl", {})),
            ))
            result['version'] = await cur.fetchone()

            await cur.execute("""
                UPDATE %s.forms
                SET version_id = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                new_version,
                result.get("id"),
            ))

            result['version_id'] = new_version

        return result

    @staticmethod
    async def get_definition(form_id, user=None):
        forms = None
        grid = {}
        features = {}
        definition = {}
        result = None

        versions = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.forms WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

            await cur.execute("""
                SELECT uuid, definition
                FROM %s.form_versions
                WHERE form_id = %s
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            versions = await cur.fetchall()

            await cur.execute("""
                SELECT fv.definition
                FROM %s.form_versions AS fv
                    LEFT JOIN %s.forms AS f ON f.version_id = fv.uuid
                WHERE f.id = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            definition = await cur.fetchone()
            definition = definition.get('definition')

        for version in versions:
            if isinstance(version.get("definition"), (string_types,)):
                if "{" in version.get("definition"):
                    version['definition'] = json.loads(version.get("definition"))
                else:
                    version['definition'] = {}

        interval = "DAY"
        interval_feature = form.get("features", dict()).get("INTERVAL_REPORTING", None)
        if interval_feature is not None:
            interval = interval_feature.get("interval", "DAY")

        has_location = False
        location_feature = form.get("features", {}).get("LOCATION_REPORTING", None)
        if location_feature is not None:
            has_location = True

        grid = _extrapolate_grid(versions)
        grid['submitted_date'] = dict(
            root=True,
            order=-4,
            type="date",
            name="submitted_date",
            label=dict(
                en="Submitted Date"
            ),
            filterKey="submitted_date"
        )
        grid['data_date'] = dict(
            type="date",
            root=True,
            name="data_date",
            order=-3,
            label=dict(
                en="Report Date"
            ),
            date_type=interval,
            filterKey="data_date"
        )

        if has_location == True:
            grid['location_name'] = dict(
                root=True,
                order=-2,
                width=250,
                type="location",
                name="location_id",
                label=dict(
                    en="Location"
                ),
                filterKey="location_id"
            )

            grid['location_full'] = dict(
                root=True,
                order=-1.3,
                width=250,
                type='location',
                name='location_full',
                label=dict(
                    en="Location Path"
                ),
                filterKey="location_id",
                sortKey="location_full_name"
            )

            grid['sti_name'] = dict(
                root=True,
                order=-1.2,
                width=250,
                type="select",
                name="sti_name",
                label=dict(
                    en="Location Type"
                ),
                optionsSource=dict(
                    resource="location_type",
                    valSource="id",
                    labelSource="name",
                    query=dict()
                ),
                filterKey="site_type_id",
                sortKey="sti_name"
            )

            grid['groups'] = dict(
                root=True,
                order=-1.1,
                width=250,
                type="text",
                name="groups",
                label=dict(
                    en="Location Group(s)"
                ),
                filterKey="groups",
                sortKey="groups"
            )

        grid["creator_name"] = dict(
            root=True,
            order=-1,
            width=250,
            type="user",
            filterKey="created_by",
            name="creator_name",
            label=dict(
                en="Created By"
            )
        )

        grid['created'] = dict(
            root=True,
            order=999,
            type="date",
            filterKey="created",
            label=dict(
                en="Created"
            )
        )

        grid['eid'] = dict(
            root=True,
            order=1000,
            type="text",
            filterKey="eid",
            label="EID"
        )

        return dict(
            interval=interval,
            grid=_generate_table(grid),
            definition=definition,
            form_id=form_id,
        )

    @staticmethod
    async def get_fields(form_id, user=None):
        """ get the available fields in a form

        Args:
            form_id:
            user:

        Returns:

        """
        results = []
        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT fv.definition
                 FROM %s.forms AS f
                  LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE f.id = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

        definition = form.get('definition', {})

        for key, value in definition.items():
            if value.get("type") not in IGNORED_FIELD_TYPES:
                results.append([value.get("order", 0), key, value.get("label"), value.get("type"), value])
            else:
                # recursion
                if value.get("fields", None) is not None:
                    parent_order = value.get("order", 0)
                    sub_results = _flatten_definition(
                        value.get("fields", {}),
                        key,
                        parent_order,
                        value.get("label", {}).get("en", "Unlabelled")
                    )

                    results = results + sub_results

        return results

    @staticmethod
    async def get_field_fv(fv_id, field_id, user=None):
        form = None
        results = []

        target = field_id.replace('data.', '')

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT definition
                FROM %s.form_versions
                WHERE uuid = %s;
            """, (
                AsIs(user.get('tki')),
                fv_id,
            ))
            form = await cur.fetchone()

        definition = form.get('definition', {})

        for key, value in definition.items():
            if value.get("type") not in IGNORED_FIELD_TYPES:
                results.append([value.get("order", 0), key, value.get("label"), value.get("type"), value])
            else:
                # recursion
                if value.get("fields", None) is not None:
                    parent_order = value.get("order", 0)
                    sub_results = _flatten_definition(
                        value.get("fields", {}),
                        key,
                        parent_order,
                        value.get("label")
                    )

                    results = results + sub_results

        result = None
        for item in results:
            if item[1] == target:
                result = item

        return result

    @staticmethod
    async def get_field(field_id, user=None):
        form_id, _, field_key = field_id.partition(".")

        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT name
                FROM %s.forms
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

        fields = await Form.get_fields(form_id, user=user)

        result = None
        for item in fields:
            if item[1] == field_key:
                result = item

        if result is not None:
            result[2] = "%s \\ %s" % (form.get("name", {}).get("en", "No Form"), result[2])
        return result

    @staticmethod
    async def get_dimensions(form_id, user=None):
        results = []
        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT fv.definition
                 FROM %s.forms AS f
                  LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE f.id = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

        definition = form.get('definition', {})

        for key, value in definition.items():
            label = value.get('label', None)
            if isinstance(label, (dict,)):
                label = label.get('en')

            if label is None:
                label = "Unlabelled"

            if value.get("type") not in IGNORED_FIELD_TYPES:
                results.append([
                    form_id,
                    value.get("order", 0),
                    key,
                    value.get("type"),
                    label,
                    value
                ])
            else:
                # recursion
                if value.get("fields", None) is not None:
                    parent_order = value.get("order", 0)
                    sub_results = _flat_def(
                        value.get("fields", {}),
                        key,
                        parent_order,
                        label,
                        form_id
                    )

                    results = results + sub_results

        # Filter to dimensionable types
        end_results = []
        for item in results:
            if item[3] in DIM_TYPES:
                end_results.append(item)

        return end_results

    @staticmethod
    async def get_measures(form_id, user=None):
        """ Get the measures available for a given form

        Args:
            form_id:
            user:

        Returns:

        """
        results = []
        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                 SELECT fv.definition
                 FROM %s.forms AS f
                  LEFT JOIN %s.form_versions AS fv ON fv.uuid = f.version_id
                WHERE f.id = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

        definition = form.get('definition', {})

        for key, value in definition.items():
            label = value.get('label', None)
            if isinstance(label, (dict,)):
                label = label.get('en', None)

            if label is None:
                label = "Unlabelled"

            if value.get("type") not in IGNORED_FIELD_TYPES:
                results.append([
                    form_id,
                    value.get("order", 0),
                    key,
                    value.get("type"),
                    label,
                    value
                ])
            else:
                # recursion
                if value.get("fields", None) is not None:
                    parent_order = value.get("order", 0)
                    sub_results = _flat_def(
                        value.get("fields", {}),
                        key,
                        parent_order,
                        label,
                        form_id
                    )

                    results = results + sub_results

        # Filter to dimensionable types
        end_results = []
        for item in results:
            if item[3] in MEAS_TYPES:
                end_results.append(item)

        return end_results

    @staticmethod
    async def duplicate(form_id, user=None):
        form = None
        version = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.forms
                WHERE id = %s;
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

            await cur.execute("""
                SELECT * FROM %s.form_versions
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                form.get("version_id", None),
            ))
            version = await cur.fetchone()

        del form['id']
        del form['version_id']
        del form['uuid']
        form['status'] = "DRAFT"
        form['created_by'] = user.get("id")

        if isinstance(form.get("name", dict()), (dict,)):
            for key, value in form['name'].items():
                form['name'][key] = form['name'][key] + " copy"
        else:
            form['name'] = form['name'] + " copy"

        form['version'] = {}
        form['version']['definition'] = version.get("definition", {})
        form['version']['etl'] = version.get("etl", {})
        await Form.create(form, user=user)

        return True

    @staticmethod
    async def delete(form_id, user=None):
        """ Delete a form from the system

        Args:
            form_id:
            user:

        Returns:

        """
        alerts = []
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.uuid
                FROM %s.alerts AS a
                  LEFT JOIN %s.collections AS c ON c.uuid = a.triggering_report_id
                WHERE c.form_id = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            alerts = await cur.fetchall()

        alert_uuids = [x.get("uuid") for x in alerts]

        async with get_db_cur() as cur:
            # Remove submissions from any alerts
            if len(alert_uuids) > 0:
                await cur.execute("""
                     UPDATE %s.alerts
                     SET triggering_report_id = NULL
                     WHERE uuid IN %s;
                """, (
                    AsIs(user.get("tki")),
                    tuple(alert_uuids),
                ))

            # Delete stuff
            await cur.execute("""
                DELETE FROM %s.assignments WHERE form_id = %s;
                DELETE FROM %s.amendments WHERE form_id = %s;
                DELETE FROM %s.retractions WHERE form_id = %s;
                DELETE FROM %s.collections WHERE form_id = %s;
                ALTER TABLE %s.form_versions DISABLE TRIGGER ALL;
                ALTER TABLE %s.forms DISABLE TRIGGER ALL;
                DELETE FROM %s.form_versions WHERE form_id = %s;
                DELETE FROM %s.forms WHERE id = %s;
                ALTER TABLE %s.form_versions ENABLE TRIGGER ALL;
                ALTER TABLE %s.forms ENABLE TRIGGER ALL;
            """, (
                AsIs(user.get("tki")), form_id,
                AsIs(user.get("tki")), form_id,
                AsIs(user.get("tki")), form_id,
                AsIs(user.get("tki")), form_id,
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")), form_id,
                AsIs(user.get("tki")), form_id,
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
            ))

        return True

    @staticmethod
    async def update(form_id, data, user=None):
        """ Update a form and it's definition

        Args:
            form_id:
            data:
            user:

        Returns:

        """
        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.forms
                    SET name = %s,
                    description = %s,
                    status = %s,
                    last_modified = %s,
                    features = %s,
                    eid_prefix = %s,
                    exportable = %s
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                json.dumps(data.get("name"), cls=JSONEncoder),
                json.dumps(data.get("description")),
                data.get("status"),
                datetime.datetime.now(),
                json.dumps(data.get("features", {})),
                data.get("eid_prefix", None),
                data.get("exportable", True),
                form_id,
            ))

            version = data.get("version", dict())

            await cur.execute("""
                UPDATE %s.form_versions
                SET definition = %s,
                 etl = %s
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                json.dumps(data['version']['definition']),
                json.dumps(data['version']['etl']),
                data.get("version_id"),
            ))

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM %s.forms
                WHERE id = %s
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            result = await cur.fetchone()

            await cur.execute("""
                SELECT *
                FROM %s.form_versions
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                data.get("version_id")
            ))
            result['version'] = await cur.fetchone()

        return result

    @staticmethod
    async def get_current_definition(form_id, user=None):
        result = None
        async with get_db_cur() as cur:
            await cur.execute("""
                    SELECT f.*, v.definition
                    FROM %s.forms AS f
                    LEFT JOIN %s.form_versions AS v ON v.uuid = f.version_id
                    WHERE f.id = %s
                """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def export(form_id, user=None):
        result = dict()

        form = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT f.*, v.*
                FROM %s.forms AS f
                  LEFT JOINT %s.form_versions AS v ON v.uuid = f.version_id
                WHERE f.id = %s;
            """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                form_id,
            ))
            form = await cur.fetchone()

        pass

    @staticmethod
    async def import_form(data, user=None):
        pass

    @staticmethod
    async def recent_submissions(offset, user=None):
        reports = []

        if user.get("role") == "ACCOUNT_ADMIN":
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT c.uuid,
                        c.submitted_date,
                        c.data_date,
                        c.created,
                        u.name as user_name,
                        l.name AS location_name,
                        f.name AS form_name,
                        f.time_interval AS time_interval
                    FROM %s.collections AS c
                    LEFT JOIN _iw.users AS u ON u.id = c.created_by
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                    LEFT JOIN %s.forms AS f ON f.id = c.form_id
                    WHERE c.status = 'SUBMITTED'
                    ORDER BY c.created DESC
                    LIMIT 10
                    OFFSET %s
                """ % (
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    AsIs(user.get("tki")),
                    offset,
                ))
                reports = await cur.fetchall()

        return reports

    @staticmethod
    async def get_fields_as_options(form_id, user=None):
        results = []

        unsorted = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT fv.definition
                from %s.form_versions as fv
                WHERE fv.form_id = %s;
            """, (
                AsIs(user.get("tki")),
                form_id,
            ))
            unsorted = await cur.fetchone()

        unsorted = [x for x in unsorted]

        options = []

        def _convert(data):
            for key, value in data.items():
                options.append([key, value.get("label")])

                if value.get("fields", None):
                    pass

        return results
