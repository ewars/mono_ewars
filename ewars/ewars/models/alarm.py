import uuid
import json
import datetime

from ewars.db import get_db_cur
from ewars.constants import CONSTANTS
from ewars.utils.six import string_types
from ewars.utils import date_utils
from ewars.analysis import immediate, comparators
from ewars.core import notifications

from .location_group import LocationGroup
from .alert import Alert
from .notification import Notification

from psycopg2.extensions import AsIs

import tornado
import tornado.ioloop


class Alarm:
    @staticmethod
    async def get_alarms(limit, offset, sorts, filters, user=None):
        results = []
        total = 0

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT a.*,
                  u.name AS created_name,
                  um.name AS modified_name
                FROM %s.alarms_v2 AS a
                  LEFT JOIN %s.users AS u ON u.id = a.created_by
                  LEFT JOIN %s.users AS um ON um.id = a.modified_by
                ORDER BY a.name;
            """, (
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
                AsIs(user.get('tki')),
            ))
            results = await cur.fetchall()

            total_sql = cur.query.decode('utf').split('FROM')[1]
            total_sql = total_sql.split('ORDER BY')[0]
            total_sql = """ SELECT COUNT(*) AS total FROM %s """ % (total_sql,)
            await cur.execute(total_sql)
            total = await cur.fetchone()
            total = total.get('total', 0)

        return dict(
            total=total,
            results=results
        )

    @staticmethod
    async def create(data, user=None):
        """Create a new alarm

        Args:
            data: The data for the alarm
            user: The calling user

        Returns:
            The newly create alarm
        """
        result = None

        ds_indicator = ''
        if data.get("ds_indicator", None) is not None:
            ds_indicator = data.get("ds_indicator", '')
            if isinstance(ds_indicator, dict):
                ds_indicator = json.dumps(ds_indicator)

        crit_indicator = data.get("crit_indicator", None)
        if crit_indicator is not None:
            if isinstance(crit_indicator, (dict,)):
                crit_indicator = json.dumps(crit_indicator)
        else:
            crit_indicator = ""

        async with get_db_cur() as cur:
            await cur.execute("""
                INSERT INTO %s.alarms_v2 (
                  name,
                  description,
                  status,
                  created,
                  last_modified,
                  created_by,
                  modified_by,
                  loc_restrict,
                  loc_id,
                  loc_groups,
                  sti_restrict,
                  sti,
                  ds_interval_type,
                  ds_interval,
                  ds_type,
                  ds_indicator,
                  ds_agg_lower,
                  ds_aggregate,
                  ds_modifier,
                  crit_comparator,
                  crit_source,
                  crit_sd_spec,
                  crit_sd_intervals,
                  crit_ed_spec,
                  crit_ed_intervals,
                  crit_indicator,
                  crit_formula,
                  crit_series,
                  crit_reduction,
                  crit_floor,
                  crit_modifier,
                  crit_value,
                  ad_enabled,
                  ad_interval,
                  ad_period,
                  inv_enabled,
                  inv_form_ids,
                  eid_prefix,
                  monitor_type,
                  sti_agg_rollup
                ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                RETURNING *;
            """, (
                AsIs(user.get("tki")),
                data.get("name"),
                data.get('description'),
                data.get('status'),
                datetime.datetime.utcnow(),
                datetime.datetime.utcnow(),
                user.get("id"),
                user.get("id"),
                data.get("loc_restrict", False),
                data.get("loc_id", None),
                data.get("loc_groups", None),
                data.get("sti_restrict", False),
                data.get("sti", None),
                data.get('ds_interval_type', "WEEK"),
                data.get("ds_interval", 0),
                data.get("ds_type", "INDICATOR"),
                ds_indicator,
                data.get("ds_agg_lower", False),
                data.get("ds_aggregate", "SUM"),
                data.get("ds_modifier", None),
                data.get("crit_comparator", "EQ"),
                data.get("crit_source", "INDICATOR"),
                data.get("crit_sd_spec", None),
                data.get("crit_sd_intervals", None),
                data.get('crit_ed_spec', None),
                data.get('crit_ed_intervals', None),
                crit_indicator,
                data.get("crit_formula", None),
                json.dumps(data.get("crit_series", [])),
                data.get('crit_reduction', "SUM"),
                data.get("crit_floor", None),
                data.get("crit_modifier", None),
                data.get("crit_value", None),
                data.get("ad_enabled", False),
                data.get('ad_interval', None),
                data.get("ad_period", None),
                data.get("inv_enabled", False),
                data.get("inv_form_ids", None),
                data.get("eid_prefix", None),
                data.get('monitor_type', 'AGGREGATE'),
                data.get("sti_agg_rollup", False),
            ))
            result = await cur.fetchone()

        if result is not None:
            if '}' in result.get('ds_indicator', ''):
                result['ds_indicator'] = json.loads(result.get('ds_indicator'))

        return result

    @staticmethod
    async def delete(alarm_uuid, user=None):
        """Delete an alarm from the system

        Args:
            alarm_uuid: The uuid of the alarm to delete
            user: THe calling user

        Returns:

        """
        alerts = []
        async with get_db_cur() as cur:
            # Get uuid of alerts associated with alarm
            await cur.execute("""
                SELECT uuid FROM %s.alerts
                WHERE alarm_id = %s;
            """, (
                AsIs(user.get("tki")),
                alarm_uuid,
            ))
            alerts = await cur.fetchall()

            # map out uuids
            alert_uuids = [x.get("uuid") for x in alerts]

            if len(alert_uuids) > 0:
                # Delete all alert user associations
                await cur.execute("""
                    DELETE FROM %s.alert_users
                    WHERE alert_id IN %s;
                """, (
                    AsIs(user.get("tki")),
                    tuple(alert_uuids),
                ))

                # Delete all alert action associations
                await cur.execute("""
                    DELETE FROM %s.alert_actions
                    WHERE alert_id IN %s;
                """, (
                    AsIs(user.get("tki")),
                    tuple(alert_uuids),
                ))

                # Delete all alert event items
                await cur.execute("""
                    DELETE FROM %s.alert_events
                    WHERE alert_id IN %s;
                """, (
                    AsIs(user.get("tki")),
                    tuple(alert_uuids),
                ))

                # Delete all alets
                await cur.execute("""
                    DELETE FROM %s.alerts
                    WHERE alarm_id = %s;
                """, (
                    AsIs(user.get("tki")),
                    alarm_uuid,
                ))

            # Delete the alarm
            await cur.execute("""
                DELETE FROM %s.alarms_v2 WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alarm_uuid,
            ))

        return True

    @staticmethod
    async def update(alarm_uuid, data, user=None):
        """Update an alarms settings in the database

        Args:
            alarm_uuid: The uuid of the alarm
            data: The data to update the alarm with
            user: The calling user

        Returns:
            The updated alarm
        """

        indicator_id = None
        indicator_definition = None
        if isinstance(data.get("indicator_id", None), (dict,)):
            indicator_definition = data.get("indicator_id")
            if data.get("indicator_definition", None) is not None:
                indicator_definition = data.get("indicator_definition")
        elif isinstance(data.get("indicator_definition", None), (dict,)):
            indicator_definition = data.get("indicator_definition")
        else:
            indicator_id = data.get("indicator_id")

        ds_indicator = None
        if data.get("ds_indicator", None) is not None:
            ds_indicator = data.get("ds_indicator", None)
            if isinstance(ds_indicator, dict):
                ds_indicator = json.dumps(ds_indicator)

        crit_indicator = data.get("crit_indicator", None)
        if crit_indicator is not None:
            if isinstance(crit_indicator, (dict,)):
                crit_indicator = json.dumps(crit_indicator)

        async with get_db_cur() as cur:
            await cur.execute("""
                UPDATE %s.alarms_v2
                    SET name = %s,
                      description = %s,
                      status = %s,
                      last_modified = %s,
                      modified_by = %s,
                      loc_restrict = %s,
                      loc_id = %s,
                      loc_groups = %s,
                      sti_restrict = %s,
                      sti = %s,
                      ds_interval_type = %s,
                      ds_interval = %s,
                      ds_type = %s,
                      ds_indicator = %s,
                      ds_agg_lower = %s,
                      ds_aggregate = %s,
                      ds_modifier = %s,
                      crit_comparator = %s,
                      crit_source = %s,
                      crit_sd_spec = %s,
                      crit_sd_intervals = %s,
                      crit_ed_spec = %s,
                      crit_ed_intervals = %s,
                      crit_indicator = %s,
                      crit_formula = %s,
                      crit_series = %s,
                      crit_reduction = %s,
                      crit_floor = %s,
                      crit_modifier = %s,
                      crit_value = %s,
                      ad_enabled = %s,
                      ad_interval = %s,
                      ad_period = %s,
                      inv_enabled = %s,
                      inv_form_ids = %s,
                      eid_prefix = %s,
                      monitor_type = %s,
                      sti_agg_rollup = %s
                WHERE uuid = %s;
            """, (
                AsIs(user.get("tki")),
                data.get("name", "Unnamed Alarm"),
                data.get("description", "No description provided"),
                data.get('status', "INACTIVE"),
                datetime.datetime.utcnow(),
                user.get("id"),
                data.get("loc_restrict", False),
                data.get("loc_id", None),
                data.get("loc_groups", None),
                data.get("sti_restrict", False),
                data.get("sti", None),
                data.get("ds_interval_type", None),
                data.get("ds_interval", None),
                data.get("ds_type", None),
                ds_indicator,
                data.get("ds_agg_lower", None),
                data.get('ds_aggregate', None),
                data.get("ds_modifier", None),
                data.get("crit_comparator", None),
                data.get("crit_source", None),
                data.get("crit_sd_spec", None),
                data.get('crit_sd_intervals', None),
                data.get("crit_ed_spec", None),
                data.get("crit_ed_intervals", None),
                crit_indicator,
                data.get("crit_formula", None),
                json.dumps(data.get("crit_series", [])),
                data.get("crit_reduction", None),
                data.get("crit_floor", None),
                data.get('crit_modifier', None),
                data.get('crit_value', None),
                data.get("ad_enabled", False),
                data.get('ad_interval', None),
                data.get('ad_period', None),
                data.get("inv_enabled", False),
                data.get("inv_form_ids", None),
                data.get("eid_prefix", None),
                data.get('monitor_type', 'AGGREGATE'),
                data.get("sti_agg_rollup", False),
                alarm_uuid,
            ))

            await cur.execute('''
             SELECT * FROM %s.alarms_V2 WHERE uuid = %s;
            ''', (
                AsIs(user.get('tki')),
                alarm_uuid,
            ))
            result = await cur.fetchone()

        if result is not None:
            if '{' in result.get('ds_indicator', ''):
                result['ds_indicator'] = json.loads(result.get('ds_indicator'))

        return result

    @staticmethod
    async def evaluate_submission(report, user=None):
        """ Evaluate a report against alarms which it might be able to trigger
        
        Args:
            report: The submission to evaluate
            user: 

        Returns:

        """
        if report.get('status') == CONSTANTS.SUBMITTED:
            alarms = []
            async with get_db_cur() as cur:
                await cur.execute("""
                    SELECT uuid
                    FROM %s.alarms_V2
                    WHERE status = 'ACTIVE';
                """, (
                    AsIs(user.get("tki")),
                ))
                alarms = await cur.fetchall()

            for alarm in alarms:
                try:
                    await Alarm._evaluate_alarm(
                        alarm.get("uuid"),
                        report.get("uuid"),
                        token=user.get("tki")
                    )
                except Exception as e:
                    print(str(e))
                    pass

    @staticmethod
    async def _hard_evaluate(alarm_uuid, alert_uuid, report_uuid, token=None):
        report = None
        alarm = None
        form = None
        form_logic = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alarms_V2 WHERE uuid = %s
            """, (
                AsIs(token),
                alarm_uuid,
            ))
            alarm = await cur.fetchone()

            await cur.execute("""
                SELECT c.*, l.lineage, l.site_type_id
                  FROM %s.collections AS c
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                  WHERE c.uuid = %s;
            """, (
                AsIs(token),
                AsIs(token),
                report_uuid,
            ))
            report = await cur.fetchone()

            if report is not None:
                await cur.execute("""
                    SELECT id, name, time_interval, version_id
                    FROM %s.forms
                    WHERE id = %s
                """, (
                    AsIs(token),
                    report.get("form_id"),
                ))
                form = await cur.fetchone()

                await cur.execute("""
                    SELECT etl FROM %s.form_versions
                    WHERE uuid = %s
                """, (
                    AsIs(token),
                    form.get("version_id"),
                ))
                form_logic = await cur.fetchone()

        if report is None:
            return None

        # Does this report have an effect on the alarm?
        has_effect = False

        # Need to filter to figure out if this report should actually be run for the given
        # Need to know if this report affects either the source indicator for the alarm,
        # or the comparison/comparator indication for the alarm

        # Indicators which the report outputs
        output_indicators = []

        etl = form_logic.get("etl")
        if etl is None:
            etl = dict()
        output_indicators = list(set(etl.keys()))

        # Need to add in system indicators to the output_indicators array
        output_indicators.append(dict(
            uuid='e51f4ca7-1678-44e3-b5cb-64f84555696f',
            form_id=form.get("id")
        ))

        alarm_indicators = []
        is_complex = False
        ds_formula = None
        ds_sources = None

        # Primary alarm indicator
        ds_indicator = alarm.get("ds_indicator", None)
        ds_ind_uuid = alarm.get("ds_indicator", None)
        if "{" in ds_indicator:
            ds_indicator = json.loads(ds_indicator)
            if 'ds_formula' in ds_indicator.keys():
                ds_sources = ds_indicator.get('ds_sources')
                ds_formula = ds_indicator.get('ds_formula')
                is_complex = True
            else:
                alarm_indicators.append(ds_indicator)
        else:
            alarm_indicators.append(ds_ind_uuid)

        crit_indicator = None
        crit_indicator_uuid = None
        if alarm.get("crit_source", None) == "DATA":
            crit_indicator = alarm.get("crit_indicator", None)
            if "uuid" in crit_indicator:
                crit_indicator = json.loads(crit_indicator)
                crit_indicator_uuid = crit_indicator.get("uuid")

        for a_ind in alarm_indicators:
            for o_ind in output_indicators:
                if isinstance(a_ind, (string_types,)) and isinstance(o_ind, (string_types,)):
                    if o_ind == a_ind:
                        has_effect = True
                elif isinstance(a_ind, (dict,)) and isinstance(o_ind, (dict,)):
                    if a_ind.get("uuid", None) == o_ind.get("uuid", None):
                        if a_ind.get("uuid", None) is not None and o_ind.get("uuid", None) is not None:
                            # Check that they have matching form id so we don't allow other forms to trigger alerts
                            if a_ind.get("form_id", "NO_A") == o_ind.get("form_id", "NO_B"):
                                has_effect = True

        if has_effect == False:
            return None

        start_date = None
        end_date = None
        locations = []

        # Figure out the date range for querying for source data
        if alarm.get("ds_interval_type") == "TRIGGER_PERIOD" or alarm.get("date_spec") == "TRIGGER":
            end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get("ds_interval"))
            start_date = date_utils.get_start_of_interval(end_date, alarm.get("ds_interval"))
        elif alarm.get("ds_interval_type") == "RANGE":
            end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get('ds_interval'))
            # Get the delta to apply to the end_date
            interval_delta = date_utils.get_delta(alarm.get("ds_sd_intervals"), alarm.get("ds_interval"))
            start_date = end_date - interval_delta
            # Get the start date (start of the week) for the start date interval
            start_date = date_utils.get_start_of_interval(start_date, alarm.get("ds_interval"))

        ds_agg_lower = alarm.get("ds_agg_lower", False)
        sti = None
        loc_id = report.get("location_id")
        loc_groups = None

        # Check if the location is restricted for raising this alert
        if alarm.get("restrict_loc", False) is True:
            if alarm.get("loc_spec") == "SPECIFIC":
                loc_id = alarm.get("loc_id", None)
                if loc_id not in report.get("lineage", []):
                    return None
            elif alarm.get("loc_spec") == "GROUPS":
                loc_groups = alarm.get("loc_groups")
                is_ok = False

                loc_groups_full = await LocationGroup.get_locations_in_group(loc_groups, user=dict(
                    tki=token
                ))

                for loc in loc_groups_full:
                    if loc.get("uuid") in report.get("lineage"):
                        is_ok = True

                if is_ok != True:
                    return None

        # Check if this is restricted to a specific location type
        if alarm.get('sti_restrict', False) is True:
            sti = alarm.get("sti")
            if report.get("site_type_id") != int(sti):
                return None

        # get the data
        ds_location = report.get("location_id", None)

        constrain_record = alarm.get('monitor_type', 'AGGREGATE')
        if constrain_record == 'RECORD':
            constrain_record = report_uuid
            loc_id = report.get('location_id')
        else:
            constrain_record = False

        ds_result = None
        if alarm.get("ds_type") == "INDICATOR":
            results = await immediate.run(
                target_interval=alarm.get("ds_interval", "DAY"),
                target_location=str(loc_id),
                indicator=ds_indicator,
                options=dict(
                    start_date=start_date,
                    end_date=end_date,
                    # constrain=agg_lower,
                    constrain_report=constrain_record,
                    roll_up=ds_agg_lower,
                    reduction=alarm.get("ds_aggregate", "SUM")
                ),
                user=dict(
                    tki=token,
                    group_id=1
                )
            )
            ds_result = results.get("data", None)

        if ds_result is not None:
            ds_result = float(ds_result)

        # Apply the modifier if one has been specified
        if alarm.get('ds_modifier') is not None:
            ds_result = ds_result * float(alarm.get("ds_modifier", 1))

        # Set up the value to compare to
        comparator = alarm.get("crit_comparator")
        modifier = alarm.get("crit_modifier", 1)
        floor = alarm.get('crit_floor', None)

        result = None
        if alarm.get("crit_source") == "VALUE":
            # Create a lambda function for the comparator
            crit_value = alarm.get("crit_value", 0)
            a = None
            if comparator == "GT":
                a = lambda x: True if x > crit_value else False
            elif comparator == "GTE":
                a = lambda x: True if x >= crit_value else False
            elif comparator == "EQ":
                a = lambda x: True if x == crit_value else False
            elif alarm.get("comparator") == "LT":
                a = lambda x: True if x < crit_value else False
            elif alarm.get("comparator") == "LTE":
                a = lambda x: True if x <= crit_value else False
            elif alarm.get("comparator") == "NEQ":
                a = lambda x: True if x != crit_value else False

            # Iterate through the locations and apply the lambda to the values
            # to figure out if the value should trigger the alarm
            result = a(ds_result)

        elif alarm.get("crit_source") == "DATA":
            comparator_data = await Alarm._get_alarm_comparator(alarm,
                                                                report,
                                                                str(report.get("location_id")),
                                                                user=dict(tki=token))

            if comparator_data is not None:
                # Iterate through the location results and do the comparison
                crit_result = comparator_data

                if alarm.get("crit_modifier", None) is not None:
                    crit_result = crit_result * float(alarm.get("crit_modifier"))

                    if alarm.get('crit_floor', None) is not None:
                        if crit_result < float(alarm.get('crit_floor')):
                            crit_result = float(alarm.get("crit_floor"))

                    if ds_result is None:
                        result = False
                    else:
                        result = comparators.compare(comparator, ds_result, crit_result)
            else:
                result = False

        # Set up a dummy user
        account = None
        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT id FROM _iw.accounts WHERE tki = %s;
            """, (
                token,
            ))
            account = await cur.fetchone()

        dummy_user = dict(
            tki=token,
            aid=account.get("id")
        )

        if result is True:
            pass
        else:
            async with get_db_cur() as cur:
                await cur.execute("""
                    DELETE FROM %s.alert_actions WHERE alert_id = %s;
                    DELETE FROM %s.alert_events WHERE alert_id = %s;
                    DELETE FROM %s.alerts WHERE uuid = %s;
                """, (
                    AsIs(token),
                    alert_uuid,
                    AsIs(token),
                    alert_uuid,
                    AsIs(token),
                    alert_uuid,
                ))

        return True

    @staticmethod
    async def _evaluate_alarm(alarm_uuid, report_uuid, silent=False, token=None):
        ''' Evaluate a submission against a specific alarm
        
        Args:
            alarm_id: The alarm to evaluate against
            report_id: The submission to evaluate against
            token: The account token this exists within
            silent: If set to silent, notifications will not be raised for the alerts

        Returns:

        '''
        report = None
        alarm = None
        form = None
        form_logic = None
        account = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alarms_V2 WHERE uuid = %s
            """, (
                AsIs(token),
                alarm_uuid,
            ))
            alarm = await cur.fetchone()

            await cur.execute("""
                SELECT id, name 
                FROM _iw.accounts 
                WHERE tki = %s;
            """, (
                token,
            ))
            account = await cur.fetchone()

            await cur.execute("""
                SELECT c.*, l.lineage, l.site_type_id, l.lineage
                  FROM %s.collections AS c
                    LEFT JOIN %s.locations AS l ON l.uuid = c.location_id
                  WHERE c.uuid = %s;
            """, (
                AsIs(token),
                AsIs(token),
                report_uuid,
            ))
            report = await cur.fetchone()

            if report is not None:
                await cur.execute("""
                    SELECT id, name, time_interval, version_id
                    FROM %s.forms
                    WHERE id = %s
                """, (
                    AsIs(token),
                    report.get("form_id"),
                ))
                form = await cur.fetchone()

                await cur.execute("""
                    SELECT etl FROM %s.form_versions
                    WHERE uuid = %s
                """, (
                    AsIs(token),
                    form.get("version_id"),
                ))
                form_logic = await cur.fetchone()

        if report is None:
            return None

        # Does this report have an effect on the alarm?
        has_effect = False

        # Need to filter to figure out if this report should actually be run for the given
        # Need to know if this report affects either the source indicator for the alarm,
        # or the comparison/comparator indication for the alarm

        # Indicators which the report outputs
        output_indicators = []

        etl = form_logic.get("etl")
        if etl is None:
            etl = dict()
        output_indicators = list(set(etl.keys()))

        # Need to add in system indicators to the output_indicators array
        output_indicators.append(dict(
            uuid='e51f4ca7-1678-44e3-b5cb-64f84555696f',
            form_id=form.get("id")
        ))

        alarm_indicators = []
        is_complex = False
        ds_formula = None
        ds_sources = None

        # Primary alarm indicator
        ds_indicator = alarm.get("ds_indicator", None)
        ds_ind_uuid = alarm.get("ds_indicator", None)
        if "{" in ds_indicator:
            ds_indicator = json.loads(ds_indicator)
            if 'ds_formula' in ds_indicator.keys():
                ds_sources = ds_indicator.get('ds_sources')
                ds_formula = ds_indicator.get('ds_formula')
                is_complex = True
            else:
                alarm_indicators.append(ds_indicator)
        else:
            alarm_indicators.append(ds_ind_uuid)

        crit_indicator = None
        crit_indicator_uuid = None
        if alarm.get("crit_source", None) == "DATA":
            crit_indicator = alarm.get("crit_indicator", None)
            if "uuid" in crit_indicator:
                crit_indicator = json.loads(crit_indicator)
                crit_indicator_uuid = crit_indicator.get("uuid")

        for a_ind in alarm_indicators:
            for o_ind in output_indicators:
                if isinstance(a_ind, (string_types,)) and isinstance(o_ind, (string_types,)):
                    if o_ind == a_ind:
                        has_effect = True
                elif isinstance(a_ind, (dict,)) and isinstance(o_ind, (dict,)):
                    if a_ind.get("uuid", None) == o_ind.get("uuid", None):
                        if a_ind.get("uuid", None) is not None and o_ind.get("uuid", None) is not None:
                            # Check that they have matching form id so we don't allow other forms to trigger alerts
                            if a_ind.get("form_id", "NO_A") == o_ind.get("form_id", "NO_B"):
                                has_effect = True

        if has_effect == False:
            return None

        start_date = None
        end_date = None
        locations = []

        # Figure out the date range for querying for source data
        if alarm.get("ds_interval_type") == "TRIGGER_PERIOD" or alarm.get("date_spec") == "TRIGGER":
            end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get("ds_interval"))
            start_date = date_utils.get_start_of_interval(end_date, alarm.get("ds_interval"))
        elif alarm.get("ds_interval_type") == "RANGE":
            end_date = date_utils.get_end_of_current_interval(report.get("data_date"), alarm.get('ds_interval'))
            # Get the delta to apply to the end_date
            interval_delta = date_utils.get_delta(alarm.get("ds_sd_intervals"), alarm.get("ds_interval"))
            start_date = end_date - interval_delta
            # Get the start date (start of the week) for the start date interval
            start_date = date_utils.get_start_of_interval(start_date, alarm.get("ds_interval"))

        ds_agg_lower = alarm.get("ds_agg_lower", False)
        sti = None
        loc_id = report.get("location_id")
        loc_groups = None

        # Check if the location is restricted for raising this alert
        if alarm.get("restrict_loc", False) is True:
            if alarm.get("loc_spec") == "SPECIFIC":
                loc_id = alarm.get("loc_id", None)
                if loc_id not in report.get("lineage", []):
                    return None
            elif alarm.get("loc_spec") == "GROUPS":
                loc_groups = alarm.get("loc_groups")
                is_ok = False

                loc_groups_full = await LocationGroup.get_locations_in_group(loc_groups, user=dict(
                    tki=token
                ))

                for loc in loc_groups_full:
                    if loc.get("uuid") in report.get("lineage"):
                        is_ok = True

                if is_ok != True:
                    return None

        # get the data
        ds_location = report.get("location_id", None)
        # Check if this is restricted to a specific location type
        if alarm.get('sti_restrict', False) is True and ds_location is not None:
            sti = alarm.get("sti")
            if alarm.get("sti_agg_rollup", False) is True:
                # Get the first parent of this location which
                # conforms to the specified location type
                location_id = None
                ds_agg_lower = True
                async with get_db_cur() as cur:
                    await cur.execute("""
                        SELECT l.uuid, lv.uuid AS p_id, lv.name
                        FROM %s.locations AS l 
                          LEFT JOIN %s.locations AS lv ON l.uuid::TEXT = ANY(l.lineage::TEXT[]) AND lv.site_type_id = %s
                        WHERE l.uuid = %s;
                    """, (
                        AsIs(token),
                        AsIs(token),
                        alarm.get("sti", None),
                        report.get('location_id'),
                    ))
                    location_id = await cur.fetchone()

                if location_id is not None:
                    location_id = location_id.get("p_id")
                    loc_id = location_id
            else:
                if report.get("site_type_id") != int(sti):
                    return None

        constrain_record = alarm.get('monitor_type', 'AGGREGATE')
        if constrain_record == 'RECORD':
            constrain_record = report_uuid
            loc_id = report.get('location_id')
        else:
            constrain_record = False

        ds_result = None
        if alarm.get("ds_type") == "INDICATOR":
            results = await immediate.run(
                target_interval=alarm.get("ds_interval", "DAY"),
                target_location=str(loc_id),
                indicator=ds_indicator,
                options=dict(
                    start_date=start_date,
                    end_date=end_date,
                    # constrain=agg_lower,
                    constrain_report=constrain_record,
                    roll_up=ds_agg_lower,
                    reduction=alarm.get("ds_aggregate", "SUM")
                ),
                user=dict(
                    tki=token,
                    group_id=1
                )
            )
            ds_result = results.get("data", None)

        if ds_result is not None:
            ds_result = float(ds_result)

        # Apply the modifier if one has been specified
        if alarm.get('ds_modifier') is not None:
            ds_result = ds_result * float(alarm.get("ds_modifier", 1))

        # Set up the value to compare to
        comparator = alarm.get("crit_comparator")
        modifier = alarm.get("crit_modifier", 1)
        floor = alarm.get('crit_floor', None)

        result = None
        if alarm.get("crit_source") == "VALUE":
            # Create a lambda function for the comparator
            crit_value = alarm.get("crit_value", 0)
            a = None
            if comparator == "GT":
                a = lambda x: True if x > crit_value else False
            elif comparator == "GTE":
                a = lambda x: True if x >= crit_value else False
            elif comparator == "EQ":
                a = lambda x: True if x == crit_value else False
            elif alarm.get("comparator") == "LT":
                a = lambda x: True if x < crit_value else False
            elif alarm.get("comparator") == "LTE":
                a = lambda x: True if x <= crit_value else False
            elif alarm.get("comparator") == "NEQ":
                a = lambda x: True if x != crit_value else False

            # Iterate through the locations and apply the lambda to the values
            # to figure out if the value should trigger the alarm
            result = a(ds_result)

        elif alarm.get("crit_source") == "DATA":
            comparator_data = await Alarm._get_alarm_comparator(alarm,
                                                                report,
                                                                str(report.get("location_id")),
                                                                user=dict(tki=token))

            if comparator_data is not None:
                # Iterate through the location results and do the comparison
                crit_result = comparator_data

                if alarm.get("crit_modifier", None) is not None:
                    crit_result = crit_result * float(alarm.get("crit_modifier"))

                    if alarm.get('crit_floor', None) is not None:
                        if crit_result < float(alarm.get('crit_floor')):
                            crit_result = float(alarm.get("crit_floor"))

                    if ds_result is None:
                        result = False
                    else:
                        result = comparators.compare(comparator, ds_result, crit_result)
            else:
                result = False

        dummy_user = dict(
            tki=token,
            aid=account.get("id"),
            account_name=account.get('name')
        )

        if result is True:
            # Raise the alert
            await Alarm._raise_alert(
                alarm,
                report.get("location_id"),
                report,
                end_date,
                form,
                allow_duplicate=constrain_record,
                user=dummy_user,
                silent=silent
            )

        return True

    @staticmethod
    async def _evaluate_historic(aid, uuids, tki=None):
        for ruuid in uuids:
            await Alarm._evaluate_alarm(
                aid,
                ruuid,
                token=tki
            )

    @staticmethod
    async def _evaluate_historic_silent(aid, uuids, tki=None):
        for ruuid in uuids:
            await Alarm._evaluate_alarm(
                aid,
                ruuid,
                token=tki,
                silent=True
            )

    @staticmethod
    async def evaluate_historic(alarm_uuid, user=None):
        report_uuids = None

        today = datetime.datetime.utcnow()
        historic = today - datetime.timedelta(days=10)

        async with get_db_cur() as cur:
            await cur.execute('''
                SELECT uuid::TEXT
                FROM %s.collections 
                WHERE data_date >= %s;
            ''', (
                AsIs(user.get('tki')),
                historic,
            ))
            report_uuids = await cur.fetchall()

        report_uuids = [x.get('uuid') for x in report_uuids]

        loop = tornado.ioloop.IOLoop.current()
        loop.add_callback(Alarm._evaluate_historic, alarm_uuid, report_uuids, tki=user.get('tki'))

        return True

    @staticmethod
    async def evaluate_historic_silent(alarm_uuid, user=None):
        """ Evaluate an alarm silently; do not send out notifications
        if an alert is found

        Args:
            alarm_uuid: THe UUID of the alarm to evaluate
            user: The calling user

        Returns:

        """

        # Route is only available to account admins
        if user.get('role') not in ('ACCOUNT_ADMIN',):
            return False

        report_uuids = None

        today = datetime.datetime.utcnow()
        historic = today - datetime.timedelta(days=10)


        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT uuid::TEXT
                FROM %s.collections
                WHERE data_date >= %s
                AND status = 'SUBMITTED';
            """, (
                AsIs(user.get('tki')),
                historic,
            ))
            report_uuids = await cur.fetchall()

        report_uuids = [x.get('uuid') for x in report_uuids]

        loop = tornado.ioloop.IOLoop.current()
        loop.add_callback(Alarm._evaluate_historic_silent, alarm_uuid, report_uuids, tki=user.get('tki'))

        return True

    @staticmethod
    async def _get_alarm_comparator(alarm, report, location, user=None):
        ''' Get the comparator for an alarm evaluate
        
        Args:
            alarm: 
            report: 
            location_id: 
            user: 

        Returns:

        '''
        reduction = alarm.get("crit_reduction", "SUM")

        start_date = report.get("data_date")
        end_date = report.get("data_date")

        if alarm.get("crit_sd_spec") == "SPECIFIC":
            start_date = date_utils.parse_date(alarm.get("start_date"))
        elif alarm.get("crit_sd_spec") == "INTERVALS_BACK":
            start_date = start_date - date_utils.get_delta(
                alarm.get("crit_sd_intervals"),
                alarm.get("ds_interval")
            )

        if alarm.get("crit_ed_spec") == "SPECIFIC":
            end_date = date_utils.parse_date(alarm.get("end_date"))
        elif alarm.get("crit_ed_spec") == "INTERVALS_BACK":
            end_date = end_date - date_utils.get_delta(
                alarm.get("crit_ed_intervals"),
                alarm.get("ds_interval")
            )

        indicator = alarm.get("crit_indicator", None)
        if indicator is not None:
            if "uuid" in indicator:
                indicator = json.loads(indicator)

        if indicator is None:
            return None

        data = await immediate.run(
            target_interval=alarm.get("ds_interval"),
            target_location=location,
            indicator=indicator,
            options=dict(
                start_date=start_date,
                end_date=end_date,
                roll_up=alarm.get("ds_agg_lower", False)
            ),
            user=user
        )

        expected_intervals = date_utils.get_date_range(start_date, end_date, alarm.get("ds_interval"))
        if len(data.get("data")) < len(expected_intervals):
            # There's not enough data to do the calculation, return None
            return None

        agg_result = 0

        data = [[x[0], x[1]] for x in data.get("data") if x[1] is not None]

        if len(data) > 0:
            if reduction == "SUM":
                agg_result = sum([x[1] for x in data])
            elif reduction == "AVG":
                agg_result = sum([x[1] for x in data]) / len(data)
            elif reduction == "MIN":
                min = data[0][1]
                for x in data:
                    if x[1] > min:
                        min = x[1]
                agg_result = min
            elif reduction == "MAX":
                max = data[0][1]
                for x in data:
                    if x[1] > max:
                        max = x[1]
                agg_result = max
            elif reduction == "COUNT":
                agg_result = len(data)
            elif reduction == "DISTINCT":
                items = [x[1] for x in data]
                agg_result = len(set(items))

        return agg_result

    @staticmethod
    async def _raise_alert(alarm, location, report, alert_date, form, allow_duplicate=False, user=None, silent=False):
        """ Raise an alert (or update an existing alert) for the specific alarm and location
            
            Args:
                alarm: THe alarm to raise for
                location: THe location to raise for
                report: The triggering report
                alert_date: The date of the alert
                form: The form that triggered the alert
                user: The user who triggered the alert
                
            Returns:

            """
        # First check if there's already an alert for this location and alarm
        alert = None

        async with get_db_cur() as cur:
            await cur.execute("""
                    SELECT * FROM %s.alerts
                    WHERE location_id = %s
                        AND alarm_id = %s
                        AND trigger_end = %s;
                """, (
                AsIs(user.get("tki")),
                location,
                alarm.get("uuid"),
                alert_date,
            ))
            alert = await cur.fetchone()

        if alert is None or allow_duplicate is not None:
            # If there's no alert or the alert is inactive
            new_uuid = str(uuid.uuid4())
            tmp_eid = new_uuid.split("-")[-1]
            eid = "-".join([tmp_eid[i:i + 4] for i in range(0, len(tmp_eid), 4)])
            eid = "0A-%s" % (eid,)

            if alarm.get("eid_prefix", None) not in ("", None):
                eid = "%s-%s" % (
                    alarm.get("eid_prefix"),
                    eid,
                )

            eid = eid.upper()

            async with get_db_cur() as cur:
                await cur.execute("""
                        INSERT INTO %s.alerts (uuid, alarm_id, created, state, indicator_id, location_id, trigger_start, trigger_end, source, triggering_report_id, indicator_definition, stage, stage_state, eid)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING *;
                    """, (
                    AsIs(user.get("tki")),
                    new_uuid,
                    alarm.get("uuid"),
                    datetime.datetime.now(),
                    'OPEN',
                    alarm.get("indicator_id", None),
                    location,
                    alert_date,
                    alert_date,
                    "SYSTEM",
                    report.get("uuid"),
                    json.dumps(alarm.get("indicator_definition", "{}")),
                    'VERIFICATION',
                    'PENDING',
                    eid,
                ))
                alert = await cur.fetchone()

            await Alert.add_event(alert.get("uuid"), "TRIGGERED", "fa-exclamation-circle", user=user)
            if not silent:
                await Alarm._raise_primary_actions(alarm, alert, report, form, user=user)
        else:
            # An active alert already exists for this context,
            # just send updates if notify email or sms is enabled
            pass

    @staticmethod
    async def _raise_primary_actions(alarm, alert, report, form, user=None):
        """ Raise primary actions, these are used when a new alert has been created
        
        @params
            :param alarm: {dict} The alarm which raised the alert
            :param alert: {dict} The newly created alert
            :param report: {report} The report which was submitted that triggered the alert
            :param user: {dict} Dummy user used for querying
        :return:
        """
        submitter = None
        alert_location = None

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT *
                FROM _iw.users
                WHERE id = %s
            """, (
                report.get("created_by"),
            ))
            submitter = await cur.fetchone()

            await cur.execute("""
                SELECT uuid, name, lineage
                FROM %s.locations
                WHERE uuid = %s
            """, (
                AsIs(user.get("tki")),
                alert.get("location_id"),
            ))
            alert_location = await cur.fetchone()

            await cur.execute("""
                        SELECT u.id,
                            u.name,
                            u.email,
                            u.role,
                            u.profile,
                            o.name AS organization_name,
                            l.name AS location_name,
                            lt.name AS location_type
                        FROM %s.users AS u
                            LEFT JOIN %s.locations AS l ON l.uuid = u.location_id
                            LEFT JOIN _iw.organizations AS o ON o.uuid = u.org_id
                            LEFT JOIN %s.location_types AS lt ON lt.id = l.site_type_id
                            LEFT JOIN _iw.laboratories AS lab ON lab.uuid = u.lab_id
                            LEFT JOIN %s.assignments AS ass ON ass.user_id = u.id
                        WHERE u.status = 'ACTIVE'
                            AND 
                              (
                                (u.role = 'REGIONAL_ADMIN' AND u.location_id IN %s) 
                                OR (u.role = 'ACCOUNT_ADMIN') 
                                OR (u.role = 'USER' AND ass.location_id = %s AND ass.status = 'ACTIVE' AND ass.form_id = %s AND ass.user_id != %s)
                                );
                    """, (
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                AsIs(user.get("tki")),
                tuple(alert_location.get("lineage")),
                alert.get("location_id"),
                report.get("form_id"),
                submitter.get("id"),
            ))
            notifiers = await cur.fetchall()

        concrete = dict((x.get("id"), x) for x in notifiers)
        notifiers = list(concrete.values())

        # Add regional admins to the list of admins
        # Map in users with assignments at this location
        cur_loop = tornado.ioloop.IOLoop.current()

        cur_loop.spawn_callback(
            notifications.send_bulk,
            'ALERT',
            notifiers,
            dict(
                eid=alert.get("eid", None),
                form_name=form['name'].get("en", None),
                submitter=submitter.get("name"),
                alarm_name=alarm.get("name", "Unnamed Alarm"),
                alert_uuid=alert.get("uuid"),
                alert_location=alert_location['name'].get("en"),
                trigger_period=date_utils.display(alert.get("trigger_end"), alarm.get("date_spec_interval"))
            ),
            account=user.get('account_name', None),
            user=user,
            subject="[EWARS] New %s alert" % (alarm.get("name", "Unnamed Alarm"))
        )

        cur_loop.spawn_callback(
            notifications.send,
            'ALERT_USER',
            submitter,
            dict(
                form_name=form['name'].get("en", None),
                eid=alert.get("eid", None),
                submitter=submitter.get("name"),
                alarm_name=alarm.get("name", "Unnamed Alarm"),
                alert_uuid=alert.get("uuid"),
                alert_location=alert_location['name'].get("en"),
                trigger_period=date_utils.display(alert.get("trigger_end"), alarm.get("date_spec_interval")),
                report_date=date_utils.display(report.get("data_date"), form.get("time_interval"))
            ),
            user=user,
            account=user.get('account_name', None),
            subject="[EWARS] New %s alert" % (alarm.get("name", "Unnamed Alarm"))
        )

    @staticmethod
    async def get_all_alarms(user=None):
        results = []

        async with get_db_cur() as cur:
            await cur.execute("""
                SELECT * FROM %s.alarms_v2;
            """, (
                AsIs(user.get("tki")),
            ))
            results = await cur.fetchall()

        return results
