from ewars.db import get_db_cur

from psycopg2.extensions import AsIs

SQL_CREATE_GROUP = """
    INSERT INTO %s.form_groups
    (name, forms, created_by, modified_by)
    VALUES (%s, %s, %s, %s) RETURNING *;
"""

SQL_DELETE_GROUP = """
    DELETE FROM %s.form_groups
    WHERE uuid = %s;
"""

SQL_UPDATE_GROUP = """
    UPDATE %s.form_groups
    SET name = %s,
        forms = %s,
        modified_by = %s,
        modified = NOW()
    WHERE uuid = %s
    RETURNING *;
"""

SQL_GET_GROUPS = """
    SELECT *
    FROM %s.form_groups
"""

SQL_GET_GROUP_FORMS = """
    SELECT id, uuid, name, description
    FROM %s.forms
    WHERE uuid = ANY(%s)
    AND status = 'ACTIVE';
"""

SQL_GET_GROUP_BY_ID = """
    SELECT * FROM %s.form_groups
    WHERE uuid = %s;
"""

SCHEMA = {
    "name": {
        "type": "dict",
        "schema": {
            "en": {"type": "string", "required": False},
            "fr": {"type": "string", "required": False},
            "ar": {"type": "string", "required": False}
        }
    },
    "forms": {"type": "string", "schema": {"type": "string", "required": True}}
}

class FormGroup(object):
    @staticmethod
    async def create(data, user=None):
        result = None
        async with get_db_cur(commit=True) as cur:
            await cur.execute(SQL_CREATE_GROUP, (
                AsIs(user.get("tki")),
                json.dumps(data.get("name", {})),
                data.get("forms", []),
                user.get("uuid"),
                user.get("uuid"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def delete(gid, user=None):
        async with get_db_cur(commit=True) as cur:
            await cur.execute(SQL_DELETE_GROUP, (
                AsIs(TKI),
                gid,
            ))

        return True

    @staticmethod
    async def update(data, user=None):
        result = None
        async with get_db_cur(commit=True) as cur:
            await cur.execute(SQL_UPDATE_GROUP, (
                AsIs(user.get("tki")),
                json.dumps(data.get("name", {})),
                data.get("forms", []),
                user.get("uuid"),
            ))
            result = await cur.fetchone()

        return result

    @staticmethod
    async def get_forms(gid, user=None):
        group = None
        forms = []

        async with get_db_cur() as cur:
            await cur.execute(SQL_GET_GROUP, (
                AsIs(user.get("tki")),
                gid,
            ))
            group = await cur.fetchone()

            if group is not None:
                if len(group.get("forms", [])) > 0:
                    await cur.execute(SQL_GET_GROUP_FORMS, (
                        AsIs(TKI),
                        group.get("forms"),
                    ))
                    forms = await cur.fetchall()

        return forms

    @staticmethod
    async def get_by_id(gid, user=None):
        result = None

        async with get_db_cur() as cur:
            await cur.execute(SQL_GET_GROUP_BY_ID, (
                AsIs(user.get("tki")),
                gid,
            ))
            result = await cur.fetchone()

        return result






