from __future__ import unicode_literals


def gettext_noop(s):
    return s

###############
# Application #
###############

# Application Name
NAME = "EWARS"
# Core
DEBUG = False

SSL_PRIVATE_KEY = None
SSL_CERT_FILE = None

PEER_ID = 1
DEVICE_ID = 'GLOBAL_EWARS'

##########
# Server #
##########

#: The root non-account affiliated entry point; supercedes global
ROOT_DOMAIN = "127.0.0.1:9000"


#############
# Date/Time #
#############

TIME_ZONE = 'America/Chicago'

USE_TZ = False

############
# Language #
############

LANGUAGE_CODE = 'en-us'

# Avialable languages
LANGUAGES = [
    # ('af', gettext_noop('Afrikaans')),
    # ('ar', gettext_noop('Arabic')),
    # ('ast', gettext_noop('Asturian')),
    # ('az', gettext_noop('Azerbaijani')),
    # ('bg', gettext_noop('Bulgarian')),
    # ('be', gettext_noop('Belarusian')),
    # ('bn', gettext_noop('Bengali')),
    # ('br', gettext_noop('Breton')),
    # ('bs', gettext_noop('Bosnian')),
    # ('ca', gettext_noop('Catalan')),
    # ('cs', gettext_noop('Czech')),
    # ('cy', gettext_noop('Welsh')),
    # ('da', gettext_noop('Danish')),
    # ('de', gettext_noop('German')),
    # ('dsb', gettext_noop('Lower Sorbian')),
    # ('el', gettext_noop('Greek')),
    ('en', gettext_noop('English')),
    # ('en-au', gettext_noop('Australian English')),
    # ('en-gb', gettext_noop('British English')),
    # ('eo', gettext_noop('Esperanto')),
    # ('es', gettext_noop('Spanish')),
    # ('es-ar', gettext_noop('Argentinian Spanish')),
    # ('es-co', gettext_noop('Colombian Spanish')),
    # ('es-mx', gettext_noop('Mexican Spanish')),
    # ('es-ni', gettext_noop('Nicaraguan Spanish')),
    # ('es-ve', gettext_noop('Venezuelan Spanish')),
    # ('et', gettext_noop('Estonian')),
    # ('eu', gettext_noop('Basque')),
    # ('fa', gettext_noop('Persian')),
    # ('fi', gettext_noop('Finnish')),
    ('fr', gettext_noop('French')),
    # ('fy', gettext_noop('Frisian')),
    # ('ga', gettext_noop('Irish')),
    # ('gd', gettext_noop('Scottish Gaelic')),
    # ('gl', gettext_noop('Galician')),
    # ('he', gettext_noop('Hebrew')),
    # ('hi', gettext_noop('Hindi')),
    # ('hr', gettext_noop('Croatian')),
    # ('hsb', gettext_noop('Upper Sorbian')),
    # ('hu', gettext_noop('Hungarian')),
    # ('ia', gettext_noop('Interlingua')),
    # ('id', gettext_noop('Indonesian')),
    # ('io', gettext_noop('Ido')),
    # ('is', gettext_noop('Icelandic')),
    # ('it', gettext_noop('Italian')),
    # ('ja', gettext_noop('Japanese')),
    # ('ka', gettext_noop('Georgian')),
    # ('kk', gettext_noop('Kazakh')),
    # ('km', gettext_noop('Khmer')),
    # ('kn', gettext_noop('Kannada')),
    # ('ko', gettext_noop('Korean')),
    # ('lb', gettext_noop('Luxembourgish')),
    # ('lt', gettext_noop('Lithuanian')),
    # ('lv', gettext_noop('Latvian')),
    # ('mk', gettext_noop('Macedonian')),
    # ('ml', gettext_noop('Malayalam')),
    # ('mn', gettext_noop('Mongolian')),
    # ('mr', gettext_noop('Marathi')),
    # ('my', gettext_noop('Burmese')),
    # ('ne', gettext_noop('Nepali')),
    # ('nl', gettext_noop('Dutch')),
    # ('nn', gettext_noop('Norwegian Nynorsk')),
    # ('os', gettext_noop('Ossetic')),
    # ('pa', gettext_noop('Punjabi')),
    # ('pl', gettext_noop('Polish')),
    # ('pt', gettext_noop('Portuguese')),
    # ('pt-br', gettext_noop('Brazilian Portuguese')),
    # ('ro', gettext_noop('Romanian')),
    # ('ru', gettext_noop('Russian')),
    # ('sk', gettext_noop('Slovak')),
    # ('sl', gettext_noop('Slovenian')),
    # ('sq', gettext_noop('Albanian')),
    # ('sr', gettext_noop('Serbian')),
    # ('sr-latn', gettext_noop('Serbian Latin')),
    # ('sv', gettext_noop('Swedish')),
    # ('sw', gettext_noop('Swahili')),
    # ('ta', gettext_noop('Tamil')),
    # ('te', gettext_noop('Telugu')),
    # ('th', gettext_noop('Thai')),
    # ('tr', gettext_noop('Turkish')),
    # ('tt', gettext_noop('Tatar')),
    # ('udm', gettext_noop('Udmurt')),
    # ('uk', gettext_noop('Ukrainian')),
    # ('ur', gettext_noop('Urdu')),
    # ('vi', gettext_noop('Vietnamese')),
    # ('zh-hans', gettext_noop('Simplified Chinese')),
    # ('zh-hant', gettext_noop('Traditional Chinese')),
]

LANGUAGES_BIDI = ["he", "ar", "fa", "ur"]

############
# Database #
############

DB_BACKEND = None
DB_NAME = None
DB_HOST = None
DB_USER = None
DB_PASSWORD = None
DB_PORT = 5432

DB_READ_HOST = None

#########
# Email #
#########

EMAIL_BACKEND = 'ewars.core.mail.backends.ses'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25

EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USER_TLS = False
EMAIL_USE_SSL = False
EMAIL_SSL_CERTFILE = None
EMAIL_SSL_KEYFILE = None
EMAIL_TIMEOUT = None



DEFAULT_FROM_EMAIL = "support@ewars.ws"
EMAIL_SUBJECT_PREFIX = "[EWARS] "

##################
# Authentication #
##################
# Password signing key
SECRET_KEY = None
# JSON Web Token signing key
JWT_KEY = None

###########
# Storage #
###########

FILE_UPLOAD_TEMP_DIR = "/tmp"

STORAGE_DRIVER = "ewars.core.files.storage.S3"

# S3 Credentials
AWS_REGION = None
AWS_ACCESS_KEY = None
AWS_SECRET_KEY = None

# Local file Storage
STORAGE_PATH = ""

##################
# AUTHENTICATION #
##################

LOGIN_URL = '/login/'

# The number of days a password reset link is valid for
PASSWORD_RESET_TIMEOUT_DAYS = 3
PASSWORD_STRENGTH_REGEX = ""

###########
# LOGGING #
###########

# The callable to use to configure logging
LOGGING_CONFIG = 'logging.config.dictConfig'

# Custom logging configuration.
LOGGING = {}

# Default exception reporter filter class used in case none has been
# specifically assigned to the HttpRequest instance.
DEFAULT_EXCEPTION_REPORTER_FILTER = 'ewars.views.debug.SafeExceptionReporterFilter'

#################
# WKHTMLTOX #
#############
WKHTML_BIN = None

################
# EWARS System #
################
VERSION = "5.0.22"

# Client versions support
MIN_ANDROID_VERSION = "33.0.0"
MIN_IOS_VERSION = "1.0.0"
MIN_DESKTOP_VERSION = "1.0.0"
