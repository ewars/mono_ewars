DEFAULTS = [
    "REGIONAL_ADMIN_OVERVIEW",
    "REGIONAL_ADMIN_SURVEILLANCE",
    "REGIONAL_ADMIN_ALERTS"
]

REGIONAL_ADMIN_OVERVIEW = dict(
        name="Overview",
        icon="fa-dashboard",
        definition=[
            [
                dict(type="METRICS_OVERALL", title="Summary Statistics"),
                dict(type="DOCUMENTS", title="Available Documents")
            ],
            [
                dict(type="ACTIVITY", title="Activity")
            ],
            [
                dict(type="METRICS",
                     metrics_items=["FORM_SUBMISSIONS", "ASSIGNMENTS", "REPORTING_LOCATIONS", "PARTNERS"],
                     title="Summary Statistics")
            ]
        ]
)

REGIONAL_ADMIN_SURVEILLANCE = dict(
        name="Surveillance",
        color="#febf32",
        icon="fa-line-chart",
        definition=[
            [
                dict(type="RECENT_REPORTS", title="Recently Submitted Reports")
            ]
        ]
)

REGIONAL_ADMIN_ALERTS = dict(
        name="Alerts",
        metric="ACTIVE_ALERTS",
        color="#f18b23",
        icon="fa-bell-o",
        definition=[
            [
                dict(type="ALERTS", title="Alerts")
            ]
        ]
)
