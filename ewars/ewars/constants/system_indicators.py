from ewars.constants import CONSTANTS

__ALL__ = ["FORM_INDICATORS", "ALARM_INDICATORS"]

FORM_INDICATORS = [
    # Non-Interval Indicators
    ## Total number of submissions, form period based, based off of submitted_date
    dict(
        id=None,
        status="ACTIVE",
        interval=False,
        name=dict(en="Report Submissions Accumulated over time"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.SUBMITTED)
    ),
    ## Interval-based, shows the number of reports AVAILABLE for a given report period based on data_date
    dict(
        id=None,
        status="ACTIVE",
        interval=False,
        name=dict(en="Report Submissions"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.SUBMITTED_PERIOD)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=False,
        name=dict(en="Active Reporting Locations Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.REPORTING_LOCATIONS)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=False,
        name=dict(en="Assignments Created Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.ASSIGNMENTS)
    ),
    dict(
        id=None,
        interval=False,
        status="ACTIVE",
        name=dict(en="Assignments total change over time"),
        context=CONSTANTS.INDICATOR, itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.ASSIGNMENTS_ACCUMULATED)
    ),
    # INTERVAL BASED
    dict(
        id=None,
        status="ACTIVE",
        interval=True,
        name=dict(en="Completeness Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.COMPLETENESS)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=True,
        name=dict(en="Timeliness Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.TIMELINESS)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=True,
        name=dict(en="Total Missing Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.MISSING)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=True,
        name=dict(en="Expected Reports Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.EXPECTED)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=True,
        name=dict(en="Reports On Time Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.REPORTS_ON_TIME)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=True,
        name=dict(en="Reports Late Per Period"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.REPORTS_LATE)
    )
]

ALARM_INDICATORS = [
    dict(
        id=None,
        status="ACTIVE",
        name=dict(en="Alerts Raised"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.ALARM_ALERTS_RAISED)
    ),
    dict(
        id=None,
        status="ACTIVE",
        interval=False,
        name=dict(en="Inactive Alerts"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.ALARM_ALERTS_INACTIVE)
    ),
    dict(
        id=None,
        status="ACTIVE",
        name=dict(en="Active Alerts"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type=CONSTANTS.ALARM_ALERTS_ACTIVE)
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Open Alerts"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_OPEN")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Closed Alerts"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_CLOSED")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Alerts under Verification"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_VERIFICATION")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Alerts under Risk Assessment"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_RISK")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Alerts under Investigation"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_INVESTIGATION")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Alerts closed at Verification"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_CLOSED_AT_VERIFICATION")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Alerts closed at Risk Assessement"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_CLOSED_AT_RISK")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Alerts closed at Investigation"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_CLOSED_AT_INVESTIGATION")
    ),
    dict(
        status="ACTIVE",
        name=dict(en="Alerts Auto-Closed"),
        context=CONSTANTS.INDICATOR,
        itype=CONSTANTS.CUSTOM,
        definition=dict(type="ALARM_ALERTS_AUTO_CLOSED")
    )
]
