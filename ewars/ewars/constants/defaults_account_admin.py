DEFAULTS = [
    "ACCOUNT_ADMIN_OVERVIEW",
    "ACCOUNT_ADMIN_SURVEILLANCE",
    "ACCOUNT_ADMIN_ALERTS"
]

ACCOUNT_ADMIN_OVERVIEW = dict(
    name="Overview",
    icon="fa-dashboard",
    definition=[
        [
            dict(type="METRICS_OVERALL", title="Summary Statistics"),
            dict(type="DOCUMENTS", title="Available Documents")
        ],
        [
            dict(type="ACTIVITY", title="Activity")
        ],
        [
            dict(type="METRICS", metrics_items=["FORM_SUBMISSIONS", "ASSIGNMENTS", "REPORTING_LOCATIONS", "PARTNERS"], title="Summary Statistics")
        ]
    ]
)

ACCOUNT_ADMIN_SURVEILLANCE = dict(
    name="Surveillance",
    color="#febf32",
    icon="fa-line-chart",
    definition=[
        [
            dict(type="RECENT_REPORTS", title="Recently Submitted Reports")
        ]
    ]
)

ACCOUNT_ADMIN_ALERTS = dict(
    name="Alerts",
    metric="ACTIVE_ALERTS",
    color="#f18b23",
    icon="fa-bell-o",
    definition=[
        [
            dict(type="ALERTS", title="Alerts")
        ]
    ]
)