EMAILS_HTML = dict(
    DOCUMENT_SHARE="""
        <p>{{data.sender_name}} ({{data.sender_email}}) has shared a <strong>{{data.template_name}}</strong> report with you through EWARS.</p>

        <p>You can view the report by clicking the link below:</p>

        <a href="{{data.uri}}" target="_blank">{{data.report_title}}</a>
    """,
    ALERT_DISCARDED="""
        <p><strong>{{data.user_name}}</strong> has <strong>discarded</strong> the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Reason:</strong><br/>
            {{data.content}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    ALERT_VERIFICATION="""
        <p><strong>{{data.user_name}}</strong> has completed <Strong>Verification</strong> for the following alert:.</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Triage:</strong><br/>
            {{data.content}}
            <br/>
            <strong>Verification Outcome:</strong> {{data.outcome}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    ALERT_RISK_CHAR="""
        <p><strong>{{data.user_name}}</strong> has completed <strong>Risk Characterisation</strong> on the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Risk:</strong> {{data.risk}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    ALERT_RISK_ASSESS="""
        <p><strong>{{data.user_name}}</strong> has completed <strong>Risk Assessment</strong> on the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Hazard Assessment:</strong><br/>
            {{data.hazard_assessment}}<br/>
            <Strong>Exposure Assessment:</strong><br/>
            {{data.exposure_assessment}}<br/>
            <strong>Context Assessment</strong><br/>
            {{data.context_assessment}}

        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    ALERT_REOPENED="""
        <p><strong>{{data.user_name}}</strong> has <strong>re-opened</strong> the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Reason:</strong><br/>
            {{data.reason}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    ALERT_OUTCOME="""
       <p><strong>{{data.user_name}}</strong> has completed <strong>Outcome</strong> for the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
            <br />
            <strong>Outcome: </strong> {{data.outcome}}<br />
            <strong>Comments:</strong><br/>
            {{data.comments}}
        </p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    ALERT_AUTOCLOSED="""
        <p>The following alert has been auto-discarded:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
        </p>
        <p>You can re-open this alert from the alert dashboard.</p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    ALERT_REPORT_SUBMISSION="""
        <p><strong>{{data.user_name}}</strong> has submitted a <strong>{{data.form_name}}</strong> report for the following alert:</p>
        <p>
            <strong>Alarm: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br/>
            <strong>Alert Date: </strong> {{data.alert_date}}<br />
        </p>
        <p>You can re-open this alert from the alert dashboard.</p>
        <p>You can review the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a>.</p>
    """,
    REGISTRATION_REQUEST="""
        <p><strong>{{data.user_name}} [{{data.email}}]</strong> has recently requested a user account in EWARS which requires your approval:</p>

        <p>Please review this request from "Tasks" in your dashboard and take an appropriate action.</p>
    """,
    REGISTRATION_APPROVED="""
        <p class="lead">{{recipient.name}}, thank you for registering an account with EWARS.</p>

        <p>Your registration has been approved by an administrator and you can now log in to the EWARS application
            <a href="http://ewars.ws/login">here</a>
        </p>
    """,
    REGISTRATION_REJECTED="""
        <p class="lead">Thank you for registering an account with EWARS.</p>

        <p>Unfortunately an administrator has
            <strong>rejected</strong> your registration, you will not be able to access the EWARS system with the account you registered with.
        </p>

        <p>If you feel that this action was made in error, please contact support via email at <a
                href="mailto:support@ewars.ws">support@ewars.ws</a> and provide the email address and account you were attempting to register with.
        </p>

        <p>An account registration can be rejected for a number of reasons, please follow up with support for more information. The admistrator who rejected your request gave the following reason:</p>

            <strong>Reason</strong>

        <p>{{data.reason}}</p>
    """,
    RETRACTION_REQUEST="""
        <p><strong>{{data.deleter}} [{{data.deleter_email}}]</strong> has requested a report deletion:</p>

        <p>
            <strong>Report Type: </strong> {{data.form_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br />
            <strong>Report Date: </strong> {{data.report_date}}<br />
            <strong>Reason: </strong> {{data.reason}}
        </p>

        <p>Please review this request from "Tasks" in your dashboard and take an appropriate action.</p>
    """,
    RETRACTION_APPROVED="""
        <p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>approved</strong> your request to delete the following report:</p>

        <p>
            <strong>Report Type: </strong> {{data.form_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br />
            <strong>Report Date: </strong> {{data.report_date}}
        </p>

        <p>You do not need to take any other action as the report is now removed from the system.</p>
    """,
    RETRACTION_REJECTED="""
        <p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>rejected</strong> your request to delete the following report:</p>

        <p>
            <strong>Report Type: </strong> {{data.form_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}<br />
            <strong>Report Date: </strong> {{data.report_date}}
        </p>

        <p>They gave the following reason for the rejection:</p>

        <p>{{data.reason}}</p>
    """,
    AMENDMENT_REQUEST="""
   <p><strong>{{data.requestor}} [{{data.requestor_email}}]</strong> has requested an amendment to the following report:</p>

<p>
    <strong>Report Type: </strong> {{data.form_name}}<br/>
    <strong>Location: </strong> {{data.location_name}}<br />
    <strong>Report Date: </strong> {{data.report_date}}<br />
    <br />
    <strong>Reason: </strong> {{data.reason}}
</p>

<p>Please review this request from "Tasks" in your dashboard and take an appropriate action.</p> 
    """,
    AMENDMENT_APPROVED="""
   <p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>approved</strong> your request to amend the following report:</p>

<p>
    <strong>Report Type: </strong> {{data.form_name}}<br/>
    <strong>Location: </strong> {{data.location_name}}<br />
    <strong>Report Date: </strong> {{data.report_date}}
</p>

<p>You do not need to take any other action as the report is now updated in the system.</p> 
    
    """,
    AMENDMENT_REJECTED="""
   <p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>rejected</strong> your request to amend the following report:</p>

<p>
    <strong>Report Type: </strong> {{data.form_name}}<br/>
    <strong>Location: </strong> {{data.location_name}}<br />
    <strong>Report Date: </strong> {{data.report_date}}
</p>

<p>They gave the following reason for the rejection:</p>

<p>{{data.reason}}</p> 
    """,
    INVITATION="""
    
    """,
    ASSIGNMENT_REQUEST="""
        <p><strong>{{data.requestor}} [{{data.requestor_email}}]</strong> has requested a new assignment with the following details:</p>

        <p>
            <strong>Report Type: </strong> {{data.form_name}}<br/>
            <strong>Location: </strong> {{data.location_name_long}}
        </p>

        <p>Please review this request from "Tasks" in your dashboard and take an appropriate action.</p>
    """,
    ASSIGNMENT_APPROVED="""
        <p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>approved</strong> the following assignment request.</p>

        <p>
            <strong>Report Type: </strong> {{data.form_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}
        </p>

        <p>You can now submit reports for the assignment from your account.</p>
    """,
    ASSIGNMENT_REJECTED="""
        <p><strong>{{data.approver}} [{{data.approver_email}}]</strong> has <strong>rejected</strong> the following assignment request:</p>

        <p>
            <strong>Report Type: </strong> {{data.form_name}}<br/>
            <strong>Location: </strong> {{data.location_name}}
        </p>

        <p>They gave the following reason for the rejection:</p>

        <p>{{data.reason}}</p>
    """,
    EMAIL_VERIFICATION="""
        <p>Thank you for submitting a registration for EWARS. Before we can proceed with your registration, please click the link below to verify ownership of this email address</p>

        <a href="http://ewars.ws/register/verify/{{data.verification_code}}_{{data.tki}}">Verify your email address</a>
    """,
    INVITE="""
        <p>You've been invited to join the {{data.account_name}} EWARS account</p>

        <p>Click here <a href="http://ewars.ws/invitation/{{data.token}}">http://ewars.ws/invitation/{{data.token}}</a> to join the account.</p>
    """,
    WELCOME="""
        <p>Welcome to EWARS {{data.name}}, an account administrator has created an account for you in {{data.account_name}} at http://{{data.account_domain}}.</p>

        <p>Below you will find your login details, you can change your password at any time by logging in and visiting the profile page within EWARS.</p>

        <ul>
            <li><strong>Username</strong></li>
            <li>{{data.email}}</li>
            <li><strong>Password</strong></li>
            <li>{{data.password}}</li>
        </ul>

        <p>You can log into your account here <a href="http://{{data.account_domain}}/login">http://{{data.account_domain}}/login</a></p>

        <p>You can learn more about EWARS here <a href="http://ewars-project.org">http://ewars-project.org</a> or view EWARS guidance here <a href="http://ewars-project.org/guidance">http://ewars-project.org/guidance</a></p>
    """,
    WELCOME_INVITE_EXISTING="""
        <p>You've completed your invite and can now access the {{data.account_name}} account in EWARS.</p>

        <p>You can log into your account here <a href="http://{{data.account_domain}}/login">http://{{data.account_domain}}/login</a></p>

        <p>You can learn more about EWARS here <a href="http://ewars-project.org">http://ewars-project.org</a> or view EWARS guidance here <a href="http://ewars-project.org/guidance">http://ewars-project.org/guidance</a></p>
    """,
    WELCOME_INVITE="""
        <p>Welcome to EWARS {{data.name}}, you've completed your invite and can now access the {{data.account_name}} EWARS.</p>

        <p>For your records, your user details are below.</p>

        <ul>
            <li><strong>Username</strong></li>
            <li>{{data.email}}</li>
            <li><strong>Password</strong></li>
            <li>{{data.password}}</li>
        </ul>

        <p>You can log into your account here <a href="http://{{data.account_domain}}/login">http://{{data.account_domain}}/login</a></p>

        <p>You can learn more about EWARS here <a href="http://ewars-project.org">http://ewars-project.org</a> or view EWARS guidance here <a href="http://ewars-project.org/guidance">http://ewars-project.org/guidance</a></p>
    """,
    SHARE="""
        <p>{{data.sender_name}} ({{data.sender_email}}) has shared a document with you.</p>

        <p>You can view the document by clicking the link below:</p>

        <a href="{{data.uri}}" target="_blank">{{data.report_title}}</a>
    """,
    ALERT="""
        <p>A <strong>{{data.form_name}}</strong> has triggered the following alert:</p>
        <p>
            <strong>Name: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.alert_location}}<br />
            <strong>Date: </strong> {{data.trigger_period}}<br />
            <strong>EID: </strong> {{data.eid}}
        </p>
        <p>You can view more information and take actions on the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a></p>
    """,
    ALERT_USER="""
        <p>A <strong>{{data.form_name}}</strong> that you recently submitted has triggered the following alert:</p>
        <p>
            <strong>Name: </strong> {{data.alarm_name}}<br/>
            <strong>Location: </strong> {{data.alert_location}}<br />
            <strong>Date: </strong> {{data.alert_date}}<br />
            <strong>EID: </strong> {{data.eid}}
        
        </p>
        <p>You can view more information and take actions on the alert <a href="http://ewars.ws/alert#?uuid={{data.alert_uuid}}">here</a></p>
    """,
    RECOVERY="""
        <p>You recently request recovery of your password for your {{data.get("email") }} account in EWARS</p>

        <p>Please click the link below to recover your account and set a new password.</p>

        <a href="http://ewars.ws/authentication/reset/{{data.get("token")}}">Reset Password</a>
    """

)
