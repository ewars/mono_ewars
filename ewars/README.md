## EWARS Application

The fastest way to get up and running with EWARS development is to use IntelliJ IDEA (Community or Pro editions)

1. Install Python 3.5+
2. Create python virtual environment `pyvenv venv`
3. Install dependencies from `src/ewars/requirements.txt`

## Repository Structure

```
    /ewars - Python application built on top of tornado
    /rust - Python rust library
    /README.md - This readme
    /run.sh - A quick running script that will let you run the application without installing it
```

## Running the application

You can install the application on your local system by running

`pip3 install develop`

within the `/src/ewars` directory

## Notes / Known Issues

On Ubuntu and some other Linux distributions there's an issue around fonts when using document generation for reports.

You can install the necessary fonts on Ubuntu using the commands below.

```
sudo apt-get install ttf-mscorefonts-installer
sudo fc-cache -f -v
```
