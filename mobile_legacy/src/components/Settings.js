import React, {Component} from "react";
import {
    View,
    Text,
    ScrollView,
    TouchableHighlight,
    TextInput,
    Button,
    ToastAndroid,
    Picker,
    Modal
} from "react-native";

import {
    ActionButton,
    LineSpacer
} from "./common";

import config from './lib/lang';


import styles from "./STYLES";
import Storage from "./lib/storage";


class Settings extends Component {
    static navigationOptions = () => {
        return {
            title: global._("SETTINGS")
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            phone: "",
            language: "en"
        }
    }

    componentWillMount() {
        Storage.getSettings()
            .then((data) => {
                this.setState({
                    ...data
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    componentWillReceiveProps() {
        Storage.getSettings()
            .then((data) => {
                this.setState({
                    ...data
                })
            })
    }

    _onFieldChange = (text) => {
        this.setState({phone: text});
    };

    _store = () => {
        let data = {
            phone: this.state.phone || "",
            language: this.state.language || "en"
        };

        Storage.saveSettings(data)
            .then(() => {
                config(data.language);
                // Trigger a re-render to show updated language
                this.setState({
                    ...this.state
                });
                ToastAndroid.show(global._("SETTINGS_UPDATED"), 3000);
            })
            .catch((err) => {
                ToastAndroid.show(global._("AN_ERROR_OCCURRED"), 3000);
            })
    };

    _pass = () => {

    };

    render() {
        return (
            <ScrollView>
                <View style={styles.CARD}>

                    <View style={styles.FIELD}>
                        <Text style={styles.LABEL}>{global._("MOBILE_NO")}</Text>
                        <TextInput
                            value={this.state.phone}
                            onChangeText={this._onFieldChange}
                            editable={true}/>
                    </View>

                    <View style={styles.FIELD}>
                        <Text style={styles.LABEL}>{global._("LANGUAGE")}</Text>

                        <Picker
                            selectedValue={this.state.language || 'en'}
                            style={{height: 50, width: 100}}
                            onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                            <Picker.Item label="English" value="en"/>
                            <Picker.Item label="French" value="fr"/>
                            <Picker.Item label="Arabic" value="ar"/>
                            <Picker.Item label="Portuguese" value="pt"/>
                            <Picker.Item label="Development" value="dev"/>
                        </Picker>
                    </View>

                    <LineSpacer/>

                    <ActionButton
                        title={global._("SAVE_CHANGES")}
                        onPress={this._store}/>
                </View>
            </ScrollView>
        )
    }
}

export default Settings;
