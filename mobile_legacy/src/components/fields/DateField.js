import React, {Component} from "react";
import {
    View,
    Text,
    DatePickerAndroid,
    Modal,
    Picker,
    TouchableHighlight,
    Image,
    StyleSheet,
    Button
} from "react-native";
import moment from "moment";

import {ActionButton} from "../common";

import Utils from "../utils";

class WeekPicker extends Component {
    static defaultProps = {
        offsetAvailability: false
    };

    constructor(props) {
        super(props)

        this.state = {
            year: moment.utc().year(),
            manualSet: false
        }
    }

    componentWillMount() {
    }

    componentWillReceiveProps(nextProps) {
    }

    _onChange = (value) => {
        this.props.onChange(value);
    };

    _onYearChange = (value) => {
        this.setState({
            year: value,
            manualSet: true
        })
    };

    render() {

        let weeksInYear = moment.utc().isoWeekYear(this.state.year).isoWeeksInYear();
        let weeks = [];

        let today = moment.utc();
        let comparator = moment.utc();
        if (this.props.offsetAvailability) comparator.add(2, 'days');


        for (let i = 0; i <= weeksInYear; i++) {
            let weekEnd = moment.utc().isoWeekYear(this.state.year).isoWeek(i + 1).isoWeekday(7);
            if (weekEnd.isBefore(comparator, "d") && weekEnd.isoWeekYear() == this.state.year) {
                weeks.push(weekEnd);
            }
        }

        weeks.reverse();

        let items = [
            ""
        ];

        weeks.forEach(item => {
            items.push(item);
        })

        let week = "";
        if (this.props.value) {
            week = moment.utc(this.props.value).format("YYYY-MM-DD");
        }

        return (
            <View style={styles.PICKER_WEEK}>
                <View style={[styles.PICKER_MAN, {width: 50}]}>
                    <YearPicker
                        enabled={this.props.enabled}
                        value={this.state.year}
                        onChange={this._onYearChange}/>
                </View>
                <View style={styles.PICKER_MAN}>
                    <Picker
                        enabled={this.props.enabled}
                        onValueChange={this._onChange}
                        selectedValue={week}>
                        {items.map(item => {
                            if (item != "") {
                                return (
                                    <Picker.Item
                                        value={item.format("YYYY-MM-DD")}
                                        label={item.format("[W]W")}
                                        key={item.format("[W]W-GGGG")}/>
                                )
                            } else {
                                return (
                                    <Picker.Item
                                        value={""}
                                        label="Week"
                                        key="None"/>
                                )
                            }
                        })}
                    </Picker>
                </View>
            </View>
        )
    }
}

class MonthPicker extends Component {
    render() {
        return (
            <View>
                <Text>Year</Text>
                <Text>Month</Text>
            </View>
        )
    }
}

class YearPicker extends Component {
    _onChange = (value) => {
        this.props.onChange(value);
    };

    render() {
        let yearStart = 2016;

        let years = [];
        while (yearStart <= moment.utc().year()) {
            years.push(yearStart);
            yearStart++;
        }

        years.push("");
        years.reverse();


        let value = this.props.value || "";

        return (
            <View>
                <Picker
                    enabled={this.props.enabled}
                    selectedValue={value}
                    onValueChange={this._onChange}>
                    {years.map(year => {
                        if (year == "") {
                            return (
                                <Picker.Item
                                    key="NONE"
                                    label="Year"
                                    value={""}/>
                            )
                        } else {
                            return (
                                <Picker.Item
                                    key={year}
                                    label={String(year)}
                                    value={year}/>
                            )
                        }
                    })}
                </Picker>
            </View>
        )
    }
}

class DateField extends Component {
    static defaultProps = {
        enabled: true,
        offsetAvailability: false
    };

    constructor(props) {
        super(props);

        this.state = {
            modal: false
        }
    }

    _show = async() => {
        let value;
        if (this.props.value) value = this.props.value;
        if (!this.props.value) value = Utils.getFieldValue(this.props.sourceData, this.props.name) || this.props.value;

        if (!value) value = moment.utc().toDate();
        if (value) value = moment.utc(value).toDate();

        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
                date: value,
                mode: "spinner",
                maxDate: moment.utc().toDate()
            });
            if (action != DatePickerAndroid.dismissedAction) {
                let newDate = moment.utc([String(year), String(month), String(day), 0, 0, 1]);
                this.props.onChange(this.props.name, newDate.format("YYYY-MM-DD"));
            }
        } catch (err) {
            console.log(err)
        }
    };

    _showPicker = () => {
        this.setState({
            modal: true
        })

    };

    _closeModal = () => {
        this.setState({
            modal: false
        })
    };

    _onChange = (value) => {
        this.props.onChange(this.props.name, value);
    };

    _clearDate = () => {
        this.props.onChange(this.props.name, null);
    };

    render() {
        let value = this.props.value || null;
        if (!value) value = Utils.getFieldValue(this.props.sourceData, this.props.name) || this.props.value;

        let handler = this._show;

        if (this.props.data.date_type == "YEAR") {
            return (
                <View>
                    <View>
                        <YearPicker
                            enabled={this.props.enabled}
                            value={value}
                            title="x"
                            onChange={this._onChange}/>
                    </View>
                    {value && this.props.enabled ?
                        <View style={styles.BTN_WRAPPER}>
                            <TouchableHighlight style={styles.CLEAR_BUTTON} onPress={this._clearDate}>
                                <Image style={styles.CLEAR_BUTTON_IMG} source={require("../../img/ic_ewars_close.png")}/>
                            </TouchableHighlight>
                        </View>
                        : null}
                </View>
            );
        }

        if (this.props.data.date_type == "WEEK") {
            return (
                <View style={styles.DATE_WRAPPER}>
                    <View style={styles.CONTROL_WRAPPER}>
                        <WeekPicker
                            enabled={this.props.enabled}
                            onChange={this._onChange}
                            offsetAvailability={this.props.offsetAvailability}
                            value={value}/>
                    </View>
                    {value && this.props.enabled ?
                    <View style={styles.BTN_WRAPPER}>
                        <TouchableHighlight style={styles.CLEAR_BUTTON} onPress={this._clearDate}>
                            <Image style={styles.CLEAR_BUTTON_IMG} source={require("../../img/ic_ewars_close.png")}/>
                        </TouchableHighlight>
                    </View>
                        : null}
                </View>
            )
        }
        let label = global._("SELECT_DATE");
        if (value) label = Utils.DATE(value, this.props.data.date_type || "DAY");

        return (
            <View style={styles.DATE_WRAPPER}>
                <View style={{flex: 4}}>
                    <Text onPress={handler} style={styles.HANDLE}>{label}</Text>
                    {value && this.props.enabled ?
                        <View style={styles.BTN_WRAPPER}>
                            <TouchableHighlight style={styles.CLEAR_BUTTON} onPress={this._clearDate}>
                                <Image style={styles.CLEAR_BUTTON_IMG} source={require("../../img/ic_ewars_close.png")}/>
                            </TouchableHighlight>
                        </View>
                        : null}

                    <Modal
                        onRequestClose={() => {}}
                        transparent={true}
                        visible={this.state.modal}>
                        <View style={styles.MODAL_OUTER}>
                            <View style={styles.MODAL_INNER}>
                                <ActionButton
                                    title="Close"
                                    onPress={this._closeModal}/>
                                <View style={styles.MODAL_BODY}>

                                </View>
                                <View style={styles.MODAL_FOOTER}>
                                    <Button style={{marginRight: 8}} title="Select" onPress={() => {}}/>
                                </View>
                            </View>
                        </View>

                    </Modal>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    MODAL_OUTER: {
        backgroundColor: "rgba(255, 255, 255, 0.9)",
        flex: 1
    },
    MODAL_INNER: {
        flexDirection: "column",
        margin: 20,
        borderRadius: 3,
        flex: 1,
        backgroundColor: "#F2F2F2"
    },
    MODAL_HEADER: {
        flexDirection: "column",
        backgroundColor: "#333333",
        alignItems: "flex-end",
        height: 25
    },
    MODAL_FOOTER: {
        flexDirection: "row",
        alignItems: "flex-end",
        height: 60,
        padding: 8
    },
    MODAL_CLOSE: {
        height: 20,
        width: 20,
        marginTop: 3,
        marginRight: 8
    },
    MODAL_BODY: {
        flex: 2
    },
    PICKER_MAN: {
        flex: 1
    },
    PICKER_WEEK: {
        flexDirection: "row",
        justifyContent: "flex-start"
    },
    HANDLE: {
        borderBottomWidth: 1,
        borderColor: "rgb(209, 206, 209)",
        paddingTop: 8,
        paddingRight: 5,
        paddingBottom: 8,
        paddingLeft: 14,
        fontSize: 16,
        color: "#000000"
    },
    DATE_WRAPPER: {
        flexDirection: "row"
    },
    CONTROL_WRAPPER: {
        flex: 2
    },
    BTN_WRAPPER: {
        flex: 1,
        maxWidth: 50
    },
    CLEAR_BUTTON: {
        paddingTop: 8,
        paddingBottom: 8,
        justifyContent: "center",
        backgroundColor: "#CCC",
        alignItems: "center",
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3
    },
    CLEAR_BUTTON_IMG: {
        height: 20,
        width: 20,
        tintColor: "white"
    }
})

export default DateField;