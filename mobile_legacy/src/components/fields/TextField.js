import React, {Component} from "react";
import {
    View,
    TextInput,
    Text,
    Button,
    Modal, PermissionsAndroid, ToastAndroid
} from "react-native";

import Utils from "../utils";
import BarcodeScanner from '../controls/c.barcode';

class TextField extends Component {
    static defaultProps = {
        enabled: true,
        focused: false
    };

    constructor(props) {
        super(props);

        this.state = {
            showBarCode: false
        }
    }

    _close = () => {
    };


    onChange = (value) => {
        this.props.onChange(this.props.name, value);
    };

    _barcode = () => {
        this.setState({
            showBarCode: true
        })
    };

    _onChange = (prop, value) => {
        this.setState({
            showBarCode: false
        }, () => {
            this.props.onChange(this.props.name, value);
        })
    };

    componentDidMount() {
        console.log("HERE")
    }


    componentDidUpdate = (prevProps) => {
        if (this.props.focused && !prevProps.focused) {
            this._el.focus();
        }
    };

    async checkCameraPermissions() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: global._("PERMISSION_CAMERA_TITLE"),
                    message: global._("PERMISSION_CAMERA_MESSAGE")
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED || granted === true) {
                return true;
            } else {
                ToastAndroid.show(global._("ERR_CAMERA_PERMISSION"), ToastAndroid.SHORT);
                return false;
            }


        } catch (err) {
            ToastAndroid.show(global._("ERR_CAMERA_PERMISSION"), ToastAndroid.SHORT);
            return false;
        }
    }

    _nextField = () => {
        this.props.onNextField(this.props.name);
    };

    render() {
        let value = Utils.getFieldValue(this.props.sourceData, this.props.data.name);

        return (
            <View>
                <Modal
                    visible={this.state.showBarCode}
                    onRequestClose={() => {
                    }}>
                    <BarcodeScanner
                        onClose={() => {
                            this.setState({
                                showBarCode: false
                            })
                        }}
                        onChange={this._onChange}/>
                </Modal>
                <TextInput
                    editable={this.props.enabled}
                    keyboardType="default"
                    ref={(el) => this._el = el}
                    returnKeyType="next"
                    blurOnSubmit={true}
                    onSubmitEditing={this._nextField}
                    onChangeText={this.onChange}
                    value={value}/>
                {this.props.data.barcode ?
                    <Button
                        title={global._("BUTTON_SCAN_BARCODE")}
                        onPress={() => {
                            this.checkCameraPermissions()
                                .then(res => {
                                    if (res === true) {
                                        this.setState({
                                            showBarCode: true
                                        })
                                    } else {
                                        ToastAndroid.show(global._("ERR_CAMERA_PERMISSION"), ToastAndroid.SHORT);
                                    }
                                })
                                .catch(err => {
                                    ToastAndroid.show(global._("ERR_CAMERA"), ToastAndroid.SHORT);
                                })
                        }}/>
                    : null}
            </View>
        )
    }
}

export default TextField;
