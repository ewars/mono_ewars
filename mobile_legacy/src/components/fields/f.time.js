import React, {Component} from "react";
import {
    View,
    Text,
    DatePickerAndroid,
    Modal,
    Picker,
    TouchableHighlight,
    Image,
    StyleSheet,
    Button
} from "react-native";
import moment from "moment";

import {ActionButton} from "../common";

import Utils from "../utils";

const range = function(size, startAt) {
    let items = [];

    for (let i = startAt; i <= size; i++) {
        items.push(i);
    }

    return items;
}

const _format = function(val) {
    if (val < 10) return `0${val}`;
    return String(val);
}

const styles = StyleSheet.create({
    HBOX: {
        flexDirection: "row",
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    PICKER_STYLE: {
        flex: 1
    }
})

class TimeField extends Component {
    static defaultProps = {
        enabled: true,
        offsetAvailability: false
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    _hourChange = (e) => {
        let value = Utils.getFieldValue(this.props.sourceData, this.props.data.name);
        let result = value || "00:00";
        let splat = result.split(":");
        splat[0] = _format(e);
        this.props.onChange(this.props.data.name, splat.join(":"));
    };

    _minsChange = (e) => {
        let value = Utils.getFieldValue(this.props.sourceData, this.props.data.name);
        let result = value || "00:00";
        let splat = result.split(":");
        splat[1] = _format(e);
        this.props.onChange(this.props.data.name, splat.join(":"));
    };

    render() {
        let value = Utils.getFieldValue(this.props.sourceData, this.props.data.name);
        let label = value || "00:00";

        let view = (
            <View stye={styles.HBOX}>
                <Text style={styles.HANDLE} enabled={false}>{label}</Text>
            </View>
        )

        let hour = 0, mins = 0;
        if (value) {
            let splat = value.split(":");
            hour = parseInt(splat[0]);
            mins = parseInt(splat[1]);
        }

        if (this.props.enabled) {
            view = (
                <View syle={styles.HBOX}>
                    <View>
                        <Picker style={styles.PICKER_STYLE} mode="dropdown" selectedValue={hour} onValueChange={this._hourChange}>
                            {range(24, 0).map(item => {
                                return <Picker.Item
                                    key={"HOUR_" + item}
                                label={_format(item)} value={item}/>
                            })}
                        </Picker>
                    </View>
                    <View>
                        <Picker style={styles.PICKER_STYLE} mode="dropdown" selectedValue={mins} onValueChange={this._minsChange}>
                            {range(60, 0).map(item => {
                                return <Picker.Item
                                    key={"MIN_" + item}
                                label={_format(item)} value={item}/>
                            })}
                        </Picker>
                    </View>
                </View>
            )
        }

        return (
            <View>
                {view}
            </View>
        )
    }
}

export default TimeField;
