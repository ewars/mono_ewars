import React, {Component} from "react";

import {
    View,
    ScrollView,
    Picker,
    Button,
    Modal,
    TouchableHighlight,
    Text,
    Image,
    ToolbarAndroid
} from "react-native";
import Icon from "react-native-fontawesome-pro";

import Utils from "../utils";
import API from "../api/api";
import styles from "../STYLES";


class LocationItem extends Component {
    _onClick = () => {
        this.props.onAction(this.props.data);
    };

    render() {
        let showArrow = true;
        let label = Utils.i18n(this.props.data.name);

        if (this.props.data.site_type_id === this.props.target_type) showArrow = false;
        return (
            <TouchableHighlight onPress={this._onClick}>
                <View style={styles.LIST_ITEM}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text style={styles.LIST_ITEM_TEXT}>{label}</Text>
                    </View>
                    {showArrow ?
                        <View style={styles.LIST_ITEM_RIGHT}>
                            <Icon name="chevron-right" type="light"/>
                        </View>
                        : null}
                </View>
            </TouchableHighlight>
        )
    }
}

class LocationList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: [],
            current: null,
            trail: [],
            _isLoaded: false
        }
    }

    componentWillMount() {
        API.getLocations([this.props.parent_id])
            .then((resp) => {
                this.setState({
                    locations: resp.data,
                    _isLoaded: true,
                    loading: false
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    _update = (data) => {
        API.getLocations([data.uuid])
            .then(resp => {
                let trail = this.state.trail;
                if (this.state.current) {
                    trail.push(this.state.current);
                }
                this.setState({
                    current: data,
                    trail: trail,
                    locations: resp.data
                })
            })
            .catch(err => {
                console.log(err);
            })
    };

    _action = (data) => {
        if (data.site_type_id === this.props.target_type) {
            this.props.select(data);
        } else {
            this._update(data);
        }
    };

    _back = () => {
        if (this.state.current) {
            let trail = this.state.trail,
                getUUID,
                current = this.state.current || null;

            if (trail.length > 0) {
                let last = trail.pop();
                getUUID = last.uuid;
                current = last;
            } else {
                getUUID = null;
                current = null;
            }

            API.getLocations([getUUID || null])
                .then(res => {
                    this.setState({
                        current: current,
                        trail: trail,
                        locations: res.data
                    })
                })
                .catch(err => {
                    console.log(err);
                })
        }
    };

    _onClose = () => {
        this.props.onClose();
    };

    onActionSelected = (data) => {
        if (data == 0) {
            this.props.onClose();
        }

        if (data == 1) {
            this._back();
        }
    };

    render() {
        if (!this.state._isLoaded) {
            return <Text style={styles.PLACEHOLDER}>{global._("LOADING")}</Text>;
        }
        let label = "Locations";
        if (this.state.current) label = this.state.current.name.en || this.state.current.name;

        return (
            <View style={{flexDirection: "column", flex: 1}}>
                <View style={{flex: 1, height: 40, maxHeight: 30, flexDirection: "row"}}>
                    {this.state.current ?
                        <View style={styles.LOCATION_ICON_LEFT}>
                            <TouchableHighlight onPress={this._back}>
                                <Icon
                                    containerStyle={{
                                        marginLeft: 8,
                                        marginTop: 4
                                    }}
                                    name="chevron-left" type="light"/>
                            </TouchableHighlight>
                        </View>
                        : null}
                    <View style={styles.LOCATION_TEXT}>
                        <Text style={{marginTop: 3, marginLeft: 8}}>Locations</Text>
                    </View>
                    <View style={styles.LOCATION_ICON_RIGHT}>
                        <TouchableHighlight onPress={this._onClose}>
                            <Icon
                                containerStyle={{
                                    marginRight: 8,
                                    marginTop: 4
                                }}
                                name="times" type="light"/>
                        </TouchableHighlight>
                    </View>

                </View>
                <View style={styles.SCROLL_WRAPPER}>
                    <ScrollView style={styles.SCROLLVIEW}>
                        {this.state.locations.map((item) => {
                            return <LocationItem
                                onAction={this._action}
                                key={item.uuid}
                                target_type={this.props.target_type}
                                data={item}/>;
                        })}
                    </ScrollView>
                </View>
            </View>
        )
    }
}

class Location extends Component {
    static defaultProps = {
        enabled: true
    };

    constructor(props) {
        super(props);

        this.state = {
            locations: {},
            searchText: "",
            shown: false,
            location: null
        }
    }

    componentWillMount() {
        if (!this.state.location && this.props.value) {
            this._init(this.props.value);
        }
    }

    componentWillUnmount() {
        this.state.location = null;
    }

    _init = (uuid) => {
        API.getLocation([uuid])
            .then((resp) => {
                this.setState({
                    location: resp.data
                })
                this.props.onChange(resp.data);
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _show = () => {
        if (!this.props.enabled) return;
        this.setState({
            shown: true
        })
    };

    _clearSearch = () => {
        this.setState({
            searchText: ""
        })
    };

    _closeModal = () => {
        this.setState({
            shown: false,
            searchText: ""
        })
    };

    _select = (data) => {
        this.setState({
            location: data,
            shown: false,
            locations: {},
            searchText: ""
        })
        this.props.onChange(data);
    };

    render() {

        let selectedName = global._("NONE_SELECTED");
        if (this.state.location) selectedName = Utils.i18n(this.state.location.name);

        return (
            <View>
                <TouchableHighlight onPress={this._show}>
                    <View style={styles.FIELD}>
                        <View>
                            <Text style={styles.LABEL}>{global._("LOCATION")}*</Text>
                        </View>
                        <View style={styles.LOCATION_FIELD}>
                            <Text style={styles.LOCATION_FIELD_TEXT}>{selectedName}</Text>
                        </View>
                    </View>
                </TouchableHighlight>
                <Modal
                    onRequestClose={this._closeModal}
                    visible={this.state.shown}>
                    <LocationList
                        onClose={this._closeModal}
                        select={this._select}
                        target_type={this.props.site_type_id}/>
                </Modal>
            </View>
        )
    }
}

export default Location;