import React, {Component} from 'react';
import {
    View,
    Button,
    Text,
    ScrollView,
    TextInput,
    StyleSheet,
    ToastAndroid,
    PermissionsAndroid
} from "react-native";
import Utils from "../utils";

const styles = StyleSheet.create({
    BUTTON: {
        marginBottom: 8,
    },
    PADDER: {
        paddingLeft: 8,
        paddingRight: 8,
        marginTop: 8,
        marginBottom: 8
    },
    HEADER: {
        fontWeight: "bold",
        marginBottom: 8
    },
    FIELD: {
        backgroundColor: "white",
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 8,
        paddingBottom: 8
    },
    BUTTON_RED: {
        marginBottom: 8,
        backgroundColor: "rgb(190, 97, 107)"
    },
    BUTTON_ORANGE: {
        marginBottom: 8,
        backgroundColor: "rgb(207, 135, 114)"
    },
    BUTTON_GREEN: {
        marginBottom: 8,
        backgroundColor: "rgb(164, 189, 142)"
    }
})

class LatLngField extends React.Component {
    constructor(props) {
        super(props);
    }

    async _getLocation() {
        try {
            const granted = await PermissionsAndroid.requestMultiple(
                [
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
                ],
                {
                    title: global._("PERMISSION_GPS_TITLE"),
                    message: global._("PERMISSION_GPS_MESSAGE")
                },
                {
                    timeout: 60000,
                    enableHighAccuracy: true
                }
            )

            if (granted['android.permission.ACCESS_COARSE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED
                && granted['android.permission.ACCESS_FINE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED) {
                return true;
            } else {
                ToastAndroid.show(global._("ERR_GPS_PERMISSIONS"), ToastAndroid.SHORT);
                return false;
            }


        } catch (err) {
            ToastAndroid.show(global._("ERR_GPS_PERMISSIONS"), ToastAndroid.SHORT);
            return false;
        }
    };

    _getLocale = (self) => {
        ToastAndroid.show(global._("GETTING_LOCATION"), ToastAndroid.LONG);

        navigator.geolocation.getCurrentPosition(
            (res) => {
                let result = [String(res.coords.latitude), String(res.coords.longitude)];
                self.props.onChange(self.props.name, result);
            },
            (res) => {
                ToastAndroid.show(global._("ERR_LOCATION_RETRIEVAL"), ToastAndroid.SHORT);
            },
            {
                enableHighAccuracy: true
            }
        )
    };

    render() {
        let value = Utils.getFieldValue(
            this.props.sourceData,
            this.props.name
        );
        value = value || [];

        return (
            <View style={styles.PADDER}>

                <View style={styles.FIELD}>
                    <Text>{global._("LATITUDE")}</Text>
                    <TextInput
                        keyboardType={"numeric"}
                        editable={true}
                        onChangeText={(val) => {
                            value[0] = String(val);
                            this.props.onChange(this.props.name, value);
                        }}
                        value={value[0] || ""}/>
                </View>

                <View style={styles.FIELD}>
                    <Text>{global._("LONGITUDE")}</Text>
                    <TextInput
                        onChangeText={(val) => {
                            value[1] = String(val);
                            this.props.onChange(this.props.name, value);
                        }}
                        keyboardType={"numeric"}
                        editable={true}
                        value={value[1] || ""}/>
                </View>
                <View style={styles.FIELD}>
                    <Button
                        style={styles.BUTTON_GREEN}
                        title={global._("BUTTON_USE_LOCATION")}
                        onPress={() => {
                            this._getLocation()
                                .then(data => {
                                    if (data === true) {
                                        this._getLocale(this);
                                    } else {
                                        ToastAndroid.show(global._("ERR_GPS_PERMISSIONS"), ToastAndroid.SHORT);
                                    }
                                })
                                .catch(err => {
                                    ToastAndroid.show(global._("ERR_GPS_PERMISSIONS"), ToastAndroid.SHORT);
                                    console.log(err)
                                })
                        }}/>
                </View>
            </View>
        )
    }
}

export default LatLngField;