import React, {Component} from "react";
import {
    View,
    Text,
    ScrollView,
    AsyncStorage,
    StyleSheet,
    TouchableHighlight
} from "react-native";

import Utils from "./utils";
import Storage from "./lib/storage";
import styles from "./STYLES";

const style = StyleSheet.create({
    ROW: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2"
    }
})

const ERRORS = {
    DUPLICATE_REPORT: global._("ERR_DUPLICATE_REPORT"),
    LOCATION_REQUIRED: global._("ERR_LOCATION_REQUIRED"),
    DATA_DATE_REQUIRED: global._("ERR_DATA_DATE_REQUIRED"),
    FORM_CHANGE: global._("ERR_FORM_CHANGE")
}

class Item extends Component {
    _select = () => {
        this.props.select(this.props.data);
    };

    render() {
        let errorString = ERRORS[this.props.data._error.code];

        let formName = Utils.i18n(this.props.assigns[this.props.data.form_id].form_name);
        let interval = this.props.assigns[this.props.data.form_id].features.INTERVAL_REPORTING;
        let reportDate;
        if (interval) {
            reportDate = Utils.DATE(this.props.data.data_date, interval.interval);
        }
        let location_name;
        if (this.props.assigns[this.props.data.form_id].features.LOCATION_REPORTING) {
            let locations = this.props.assigns[this.props.data.form_id].locations;
            locations.forEach((location) => {
                if (location.location_id == this.props.data.location_id) location_name = Utils.i18n(location.location_name);
            })
        }

        return (
            <TouchableHighlight onPress={this._select}>
                <View style={style.ROW}>
                    <Text>{formName}</Text>
                    {location_name ?
                        <Text>{location_name}</Text>
                        : null}
                    {this.props.data.data_date ?
                        <Text>{reportDate}</Text>
                        : null}
                    <Text style={styles.ERROR}>
                        {errorString}
                    </Text>
                </View>
            </TouchableHighlight>
        )
    }
}

class Problems extends Component {
    static navigationOptions = () => {
        return {
            title: global._("PROBLEM_RECORDS")
        }
    };

    static defaultProps = {
        form: null
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            assignments: null
        }
    }

    componentWillMount() {
        this._getDrafts();
    }

    componentWillReceiveProps() {
        this._getDrafts();
    }

    _getDrafts = () => {
        Storage.getProblemReports(this.props.form ? (this.props.form.id || null) : null)
            .then((data) => {
                this.setState({data: data})
            })
            .catch((err) => {
                console.log(err);
            })

        Storage.getAssignments()
            .then((data) => {
                let _data = {};
                data.forEach((item) => {
                    _data[item.form_id] = item;
                })
                this.setState({assignments: _data})
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _showReport = (_data) => {
        Storage.getAssignment(_data.form_id)
            .then((assignment) => {
                this.props.navigation.navigate("FORM", {
                    data: {
                        uuid: _data.uuid,
                        data: _data.data,
                        data_date: _data.data_date,
                        location_id: _data.location_id,
                        form_id: _data.form_id,
                        status: "DRAFT"
                    },
                    form: {
                        id: _data.form_id,
                        name: assignment.form_name,
                        definition: assignment.definition,
                        features: assignment.features,
                        version_id: assignment.version_id || null
                    },
                    assignment: assignment
                })
            })
            .catch((err) => {
                console.log(err);
            })
    };

    render() {
        if (!this.state.assignments) {
            return (
                <View style={styles.WRAPPER}>
                    <Text style={styles.PLACEHOLDER}>{global._("LOADING")}</Text>
                </View>
            )
        }
        return (
            <View>
                <ScrollView>
                    {this.state.data.map((item, idx) => {
                        return <Item
                            assigns={this.state.assignments}
                            select={this._showReport}
                            key={idx}
                            data={item}/>
                    })}
                </ScrollView>
            </View>
        )
    }
}

export default Problems;