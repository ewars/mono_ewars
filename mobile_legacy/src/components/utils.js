import Moment from "moment";

let LISTENERS = global.LISTENERS = {};

class utils {
    static i18n(data) {
        if (data.en) return data.en;
        if (data.en == "") return data.en;
        return data;
    }

    static getFieldValue = (data, path) => {
        if (!data) return null;
        return data[path] || null;
    }

    static getField = (source, srcPath) => {
        let item = source;

        let path = srcPath.split(".");

        path.forEach(seg => {
            if (item[seg]) {
                if (item[seg].fields) {
                    item = item[seg].fields;
                } else {
                    item = item[seg];
                }
            }
        })

        return item;
    }

    static isSet(value) {
        if (value == undefined) return false;
        if (value == null) return false;
        if (value == "") return false;
        return true;
    }

    static _getRuleResult = (rule, data) => {
        let sourceValue = data[rule[0]] || null;
        if (sourceValue == null || sourceValue == undefined) return false;

        let result;
        switch (rule[1]) {
            case "eq":
                if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                    if (sourceValue.indexOf(rule[2]) >= 0) {
                        result = true;
                    } else {
                        result = false;
                    }
                } else {
                    result = sourceValue == rule[2];
                }
                return result;
            case "ne":
            case "neq":
                if (Object.prototype.toString.call(sourceValue) == "[object Array]") {
                    if (sourceValue.indexOf(rule[2]) < 0) {
                        result = true;
                    } else {
                        result = false;
                    }
                } else {
                    result = sourceValue != rule[2];
                }
                return result;
            case "gt":
                return parseFloat(sourceValue) > parseFloat(rule[2]);
            case "lt":
                return parseFloat(sourceValue) < parseFloat(rule[2]);
            case 'gte':
                return parseFloat(sourceValue) >= parseFloat(rule[2]);
            case 'lte':
                return parseFloat(sourceValue) <= parseFloat(rule[2]);
            default:
                return false;
        }
    }

    static getVisibleFields = (definition, data) => {
        let fields = [];

        let cFieldResults = {};

        definition.forEach((node, index) => {
            if (node.name.indexOf(".") >= 0) {
                let splitName = node.name.split(".");
                // Row
                if (splitName.length == 2) {
                    // Row field
                    if (cFieldResults[splitName[0]]) {
                        // This fields parent is displayed
                        if (utils.shouldBeVisible(data, node)) {
                            fields.push(node);
                            cFieldResults[node.name] = true;
                        } else {
                            cFieldResults[node.name] = false;
                        }
                    } else {
                        cFieldResults[node.name] = false;
                    }
                } else if (splitName.length == 3) {
                    // Cell field
                    // Matrix is visible
                    if (cFieldResults[splitName[0]]) {
                        // Row is visible
                        let rowName = `${splitName[0]}.${splitName[1]}`;
                        if (cFieldResults[rowName]) {
                            // cell is visible
                            if (utils.shouldBeVisible(data, node)) {
                                fields.push(node);
                                cFieldResults[node.name] = true;
                            } else {
                                cFieldResults[node.name] = false;
                            }
                        } else {
                            cFieldResults[node.name] = false;
                        }
                    } else {
                        cFieldResults[node.name] = false;
                    }
                }
            } else {
                if (utils.shouldBeVisible(data, node)) {
                    fields.push(node);
                    cFieldResults[node.name] = true;
                } else {
                    cFieldResults[node.name] = false;
                }
            }
        })

        return fields;
    }

    static shouldBeVisible = (data, field) => {
        let result = false;

        if (!field.conditions) return true;
        if (!field.conditions.rules) return true;

        // TODO: Check that parent fields are visible


        if (["ANY", "any"].indexOf(field.conditions.application) >= 0) {
            field.conditions.rules.forEach(rule => {
                let tmpResult = utils._getRuleResult(rule, data);

                if (result != true && tmpResult == true) result = true;
            })
        } else {
            let ruleCount = field.conditions.rules.length;
            let rulePassCount = 0;

            field.conditions.rules.forEach(rule => {
                let ruleResult = utils._getRuleResult(rule, data);
                if (ruleResult) rulePassCount++;
            })

            if (rulePassCount == ruleCount) result = true;
        }

        return result;
    }

    static unflatten = (data) => {
        let obj = {};


        for (let i in data) {
            let val = data[i];

            let path = i.split('.');

            let cv = obj;

            path.forEach(p => {
                if (cv[p]) {
                    cv = cv[p];
                } else {
                    if (p == path[path.length - 1]) {
                        cv[p] = val;
                    } else {
                        cv[p] = {};
                        cv = cv[p];
                    }
                }
            })
        }

        return obj;
    }

    static uuid = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    static DATE = (value, format) => {
        if (!value) return "Unknown";
        let defFormat = 'YYYY-MM-DD';
        if (format == "WEEK") {
            let startDate = Moment.utc(value).startOf('isoweek').format("DD");
            let endDate = Moment.utc(value).endOf("isoweek").format("DD");
            let stringDate = Moment.utc(value).format("[W]W GGGG | [START - END] MMMM");
            stringDate = stringDate.replace("START", startDate).replace("END", endDate);
            return stringDate;
        }

        return Moment.utc(value).format(defFormat);
    }

    static isFuture = (strDate) => {
        return !moment.utc(strDate).isBefore(moment.utc(), "d");
    }

    static listen = (signalName, callback) => {
        if (LISTENERS[signalName]) {
            LISTENERS[signalName].push(callback);
        } else {
            LISTENERS[signalName] = [callback];
        }
    }

    static unListen = (signalName, callback) => {
        LISTENERS[signalName].splice(LISTENERS[signalName].indexOf(callback), 1);
    }

    static dump = () => {
        console.log(LISTENERS);
    }

    static emit = (signalName, data) => {
        if (LISTENERS[signalName]) {
            LISTENERS[signalName].forEach(cb => {
                cb(data);
            })
        }

    };

    static deepCompare = (objOne, objTwo) => {
        let i, l, leftChain, rightChain;

        function compare2Objects(x, y) {
            var p;

            // Quick checking of one object being a subset of another.
            // todo: cache the structure of arguments[0] for performance
            for (p in y) {
                if (y.hasOwnProperty(p) != x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] != typeof x[p]) {
                    return false;
                }
            }

            for (p in x) {
                if (y.hasOwnProperty(p) != x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] != typeof x[p]) {
                    return false;
                }

                switch (typeof (x[p])) {
                    case 'object':
                    case 'function':

                        leftChain.push(x);
                        rightChain.push(y);

                        if (!compare2Objects(x[p], y[p])) {
                            return false;
                        }

                        leftChain.pop();
                        rightChain.pop();
                        break;

                    default:
                        if (x[p] != y[p]) {
                            return false;
                        }
                        break;
                }
            }

            return true;
        }

        if (arguments.length < 1) {
            return true; //Die silently? Don't know how to handle such case, please help...
            // throw "Need two or more arguments to compare";
        }


        leftChain = []; //Todo: this can be cached
        rightChain = [];

        if (!compare2Objects(objOne, objTwo)) {
            return false;
        }

        return true;
    }
}

export default utils;