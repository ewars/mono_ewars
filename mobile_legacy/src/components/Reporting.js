import React, {Component} from "react";
import {
    View,
    Text,
    ScrollView,
    AsyncStorage,
    StyleSheet,
    TouchableHighlight,
    NetInfo,
    ToastAndroid,
    Navigator,
    Image,
    Alert,
    Modal
} from "react-native";

import {StackNavigator, createStackNavigator} from 'react-navigation';

import DialogAndroid from "react-native-dialogs";

import Utils from "./utils";
import API from "./api";

import Assignments from "./reporting/Assignments";
import Assignment from "./reporting/Assignment";
import Main from "./reporting/Main";

import About from "./About";
import Profile from "./Profile";

import Form from "./Form";
import ReportBrowser from "./reporting/ReportBrowser";
import Queue from "./Queue";
import Sent from "./Sent";
import Alerts from "./reporting/Alerts";
import AlertView from "./reporting/Alert";
import Verification from "./alerts/Verification";
import RiskAssessment from "./alerts/RiskAssessment";
import RiskCharacterization from "./alerts/RiskCharacterization";
import Outcome from "./alerts/Outcome";
import Activity from "./alerts/Activity";
import AlertUsers from "./alerts/AlertUsers";
import Footer from "./reporting/Footer";
import Sync from "./lib/sync";
import Drafts from "./Drafts";
import Problems from "./Problems";
import BarcodeScanner from './controls/c.barcode';
import GeomManager from './reporting/view.geoms';
import GeomSetter from "./reporting/view.geomsetter";

import Settings from "./Settings";

import styles from "./STYLES";

const routes = [
    {title: "EWARS", index: 0, view: "DEFAULT"}
];

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View><Text>Test</Text></View>
        )
    }
}

class NoConnection extends Component {
    render() {
        return (
            <View style={styles.OFFLINE}>
                <Text style={styles.OFFLINE_TEXT}>Account Administrator and Geographic Administrator accounts currently
                    do not support offline operations. Please ensure you have an active data connection before using
                    EWARS mobile.</Text>
            </View>
        )
    }
}

class Reporting extends Component {
    static navigationOptions = {
        title: 'EWARS',
    };

    static defaultProps = {
        isConnected: false
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            sync_result: null
        }
    }

    _renderScene = (route, navigator) => {

        let scene;

        switch (route.view) {
            case "DEFAULT":
                scene = <Main
                    logout={this.props.logout}
                    navigator={navigator}/>;
                break;
            case "ASSIGNMENT":
                scene = <Assignment
                    navigator={navigator}
                    data={route.data}/>;
                break;
            case "ABOUT":
                scene = <About/>;
                break;
            case "PROFILE":
                scene = <Profile/>;
                break;
            case "FORM":
                scene = (
                    <Form
                        form={route.data.form}
                        assignment={route.data.assignment || null}
                        data={route.data.data}
                        navigator={navigator}/>
                );
                break;
            case "SETTINGS":
                scene = <Settings/>;
                break;
            case "BROWSER":
                scene = <ReportBrowser
                    navigator={navigator}
                    data={route.data}/>;
                break;
            case "SENT":
                scene = <Sent
                    navigator={navigator}
                    form={route.data}/>;
                break;
            case "QUEUED":
                scene = <Queue
                    navigator={navigator}
                    form={route.data}/>;
                break;
            case "ALERTS":
                scene = <Alerts navigator={navigator}/>;
                break;
            case "ALERTS_MONITORED":
                scene = <Alerts navigator={navigator} open={false}/>;
                break;
            case "ALERT":
                scene = <AlertView
                    navigator={navigator}
                    alert={route.data[0]}
                    alarm={route.data[1]}/>;
                break;
            case "VERIFICATION":
                scene = <Verification
                    navigator={navigator}
                    alert={route.data.alert}
                    alarm={route.data.alarm}/>;
                break;
            case "RISK_ASSESS":
                scene = <RiskAssessment
                    navigator={navigator}
                    alert={route.data.alert}
                    alarm={route.data.alarm}/>;
                break;
            case "RISK_CHAR":
                scene = <RiskCharacterization
                    navigator={navigator}
                    alert={route.data.alert}
                    alarm={route.data.alarm}/>;
                break;
            case "OUTCOME":
                scene = <Outcome
                    navigator={navigator}
                    alert={route.data.alert}
                    alarm={route.data.alarm}/>;
                break;
            case "ALERT_ACTIVITY":
                scene = <Activity alert={route.data.alert} alarm={route.data.alarm}/>;
                break;
            case "ALERT_USERS":
                scene = <AlertUsers alert={route.data.alert} alarm={route.data.alarm}/>;
                break;
            case "SYNC_RESULT":
                scene = null;
                break;
            case "DRAFTS":
                scene = <Drafts
                    navigator={navigator}
                    form={route.data}/>;
                break;
            case "PROBLEMS":
                scene = <Problems
                    navigator={navigator}
                    form={route.data}/>;
                break;
            default:
                scene = (
                    <Text>Test</Text>
                )
                break;
        }

        return (
            <View style={styles.WRAPPER}>
                <View style={styles.MAIN}>
                    {scene}
                </View>
                <Footer isConnected={this.props.isConnected}/>
            </View>
        )
    };

    _settings = (navigator) => {
        navigator.push({
            index: 1,
            title: "Settings",
            view: "SETTINGS"
        })
    };

    _sync = () => {
        let self = this;
        let syncer = new Sync();

        this._dg = new DialogAndroid();
        this._dg.set({
            title: "Syncing..."
        })
        this._dg.show();

        syncer.prep()
            .then((res) => {
                self._dg.dismiss();
                if (res) {
                    if (res.length > 0) {
                        // Show that errors have occurred
                        Alert.alert(
                            "Problem Reports",
                            "Some reports could not be synced, please see Problematic Reports for more information",
                            [
                                {
                                    text: "Ok", onPress: () => {
                                    }
                                }
                            ]
                        )
                    } else {
                        ToastAndroid.show("Sync complete", ToastAndroid.SHORT);
                    }
                } else {
                    ToastAndroid.show("Sync complete", ToastAndroid.SHORT);
                }
                Utils.emit("RELOAD_ASSIGNMENTS");
            })
            .catch((err) => {
                self._dg.dismiss();
            })
    };

    render() {



        // let nav = <Navigator.NavigationBar
        //     routeMapper={{
        //         LeftButton: this._leftButton,
        //         RightButton: this._rightButton,
        //         Title: this._title
        //     }}
        //     style={styles.NAV_HEADER}
        // />;
        //
        //
        // return (
        //     <Navigator
        //         navigationBar={nav}
        //         renderScene={this._renderScene}
        //         sceneStyle={{paddingTop: 50}}
        //         initialRoute={routes[0]}>
        //
        //     </Navigator>
        // )
    }
}


export default class RootComponent extends Component {
    static defaultProps = {
        connection: true
    };

    constructor(props) {
        super(props);

        this._Root = createStackNavigator({
            HOME: {
                title: "EWARS",
                screen: Main
            },
            ASSIGNMENT: {
                screen: Assignment
            },
            SETTINGS: {
                screen: Settings
            },
            REPORT: {
                screen: Form
            },
            PROFILE: {
                screen: Profile
            },
            ABOUT: {
                screen: About
            },
            SENT: {
                screen: Sent
            },
            QUEUE: {
                screen: Queue
            },
            DRAFTS: {
                screen: Drafts
            },
            PROBLEMS: {
                screen: Problems
            },
            ALERTS: {
                screen: Alerts
            },
            ALERTS_MONITORED: {
                screen: Alerts
            },
            ALERT: {
                screen: AlertView
            },
            BROWSER: {
                screen: ReportBrowser
            },
            FORM: {
                screen: Form
            },
            VERIFICATION: {
                screen: Verification
            },
            RISK_ASSESS: {
                screen: RiskAssessment
            },
            RISK_CHAR: {
                screen: RiskCharacterization
            },
            OUTCOME: {
                screen: Outcome
            },
            ALERT_USERS: {
                screen: AlertUsers
            },
            GEOMS: {
                screen: GeomManager
            },
            GEOM_SETTER: {
                screen: GeomSetter
            },
            ALERT_ACTIVITY: {
                screen: Activity
            }
        }, {
            initialRouteName: "HOME"
        });
    }

    componentWillUnmount() {
    }

    render() {
        if (global.USER.role != "USER") {
            return (
                <View style={{flexDirection: "column", flex: 1}}>
                    <View style={{flex: 2}}>
                        {!this.props.connection ?
                            <NoConnection/>
                            : null}
                        <this._Root/>
                    </View>
                    <View style={{flex: 1, height: 30, maxHeight: 30}}>
                        <Footer/>
                    </View>
                </View>
            )
        }

        return (
            <View style={{flexDirection: "column", flex: 1}}>
                <View style={{flex: 2}}>
                    <this._Root/>
                </View>
                <View style={{flex: 1, height: 30, maxHeight: 30}}>
                    <Footer/>
                </View>
            </View>
        )
    }
}