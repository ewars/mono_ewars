import React, {Component} from 'react';
import {URI} from "./constants";

import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    DrawerLayoutAndroid,
    ToolbarAndroid,
    Settings,
    ListView,
    AsyncStorage,
    Image,
    TextInput,
    Button,
    ActivityIndicator,
    NetInfo,
    Modal,
    ToastAndroid
} from 'react-native';

import DeviceInfo from "react-native-device-info";
import Storage from "./lib/storage";
import LoadingView from "./LoadingView";
import Footer from './reporting/Footer';

import SyncAPI from "./sync_api";
import Sync from "./lib/sync";

const TOOLBAR_ACTIONS = [
    {title: "Settings", show: "always"}
];

const style = StyleSheet.create({
    error: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ff5442",
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 3,
        paddingRight: 3
    },
    errorText: {
        color: "#FFFFFF"
    },
    IMAGE_WRAPPER: {
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: "#FFFFFF",
        paddingTop: 16,
        paddingBottom: 16
    },
    ROW: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    HEADER: {
        fontSize: 16,
        fontWeight: "bold",
        marginTop: 8
    },
    VER_CODE: {
        textAlign: "center",
        marginTop: 8,
        fontSize: 10
    }
})

class LoginView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            error: null,
            progress: false
        }
    }

    _onEmailInputChange = (e) => {
        this.setState({
            email: e,
            error: null
        })
    };

    _onPasswordChange = (value) => {
        this.setState({
            password: value,
            error: null
        })
    };

    _handleResponse = (data) => {
        console.log("ERR", data);
        if (data.err === true) {
            if (data.code === "NO_AUTH") {
                this.setState({
                    error: global._("ERR_LOGIN"),
                    progress: false
                })
            } else {
                this.setState({
                    error: global._("ERR_UNKNOWN"),
                    progress: false
                })
            }
        } else if ((data.code || "") == "MULTI_ACCOUNT") {
            this.setState({
                error: global._("ERR_MULTI_ACCOUNT"),
                progress: false
            })
        } else {
            console.log("AUTH_RESULT");
            console.log(JSON.stringify(data));
            Storage.setToken(data.token)
                .then(() => {
                    global.TOKEN = data.token;
                    let sy = new Sync();
                    return sy.prep();
                })
                .catch(err => {
                    console.log("ERR", err);
                })
                .then((resp) => {
                    this.props.onLoginSuccess(data.token);
                })
                .catch(err => {
                    console.log("ERR", err);
                });
        }
    };

    _critical = (err) => {
        this.setState({
            progress: false,
            error: global._(err || "ERR_UNKNOWN")
        })
    };

    _login = () => {
        if (!global.IS_CONNECTED) {
            ToastAndroid.show("No internet connection available, you must be connected to login to EWARS", ToastAndroid.SHORT);
            return;
        }
        this.setState({progress: true, error: null})
        let self = this;
        NetInfo.isConnected.fetch()
            .then(isConnected => {
                if (!isConnected) {
                    ToastAndroid.show(global._("NO_DATA_CONNECTION"), ToastAndroid.SHORT);
                    self._critical("NO_CONNECTION");
                } else {
                    fetch(URI + "/login", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },
                        body: JSON.stringify({
                            email: this.state.email,
                            password: this.state.password
                        })
                    })
                        .then((resp) => resp.json())
                        .catch(err => {
                            console.log(err)
                        })
                        .then((resJson) => {
                            self._handleResponse(resJson);
                        })
                        .catch((err) => {
                            console.log(err);
                            self._critical();
                        })

                }
            })
            .catch(err => {
                console.log(err);
            })
    };

    _storeToken = (token) => {

    };

    render() {
        let version = DeviceInfo.getVersion();
        return (
            <View style={{flex: 1, backgroundColor: "#F2F2F2"}}>
                <View style={{flex: 2}}>
                <View style={style.IMAGE_WRAPPER}>
                    <View style={style.ROW}>
                        <Image
                            style={{width: 50, height: 50, margin: 10}}
                            source={require("../img/ic_launcher.png")}/>
                    </View>
                    <View style={style.ROW}>
                        <Text style={style.HEADER}>EWARS Mobile</Text>
                    </View>
                </View>
                <View style={{padding: 10}}>
                    {this.state.error ?
                        <View style={style.error}>
                            <Text style={style.errorText}>{this.state.error}</Text>
                        </View>
                        : null}
                    <TextInput
                        placeholder={global._("EMAIL")}
                        keyboardType="email-address"
                        onChangeText={this._onEmailInputChange}
                        value={this.state.email}/>

                    <TextInput
                        placeholder={global._("PASSWORD")}
                        secureTextEntry={true}
                        onChangeText={this._onPasswordChange}
                        value={this.state.password}/>
                </View>
                <View style={{padding: 10, marginTop: 8, justifyContent: "center"}}>
                    <Button
                        title={global._("LOGIN")}
                        onPress={this._login}/>

                    <Text style={style.VER_CODE}>v{version} (api v2.1)</Text>
                </View>
                </View>
                <View>
                    <Footer/>
                </View>
                <Modal
                    onRequestClose={() => {
                        alert("Modal has been closed.")
                    }}
                    visible={this.state.progress}>
                    <LoadingView
                        message={global._("ATTEMPTING_LOGIN")}/>
                </Modal>
            </View>
        )
    }
}

export default LoginView;
