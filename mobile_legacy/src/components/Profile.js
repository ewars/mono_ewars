import React, {Component} from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    AsyncStorage, Modal
} from "react-native";


import {
    ActionButton,
    LineSpacer
} from "./common";

import api from "./api/api";

import TextField from "./fields/TextField";

import styles from "./STYLES";

const STYLE = StyleSheet.create({
    row: {
        flexDirection: "column",
        marginBottom: 8,
        marginRight: 8,
        marginLeft: 8
    },
    label: {
        fontSize: 12,
        color: "#2d7aed",
        fontWeight: "bold"
    },
    value: {
        fontSize: 12,
        paddingLeft: 20
    }
});

const style = StyleSheet.create({
    image: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#FFFFFF",
        paddingBottom: 8,
        paddingTop: 8,
        marginBottom: 8
    },
    ROW: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2"
    },
    BUTTON_BAR: {
        flexDirection: "row",
        height: 48,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2",
        alignItems: "flex-start",
        paddingRight: 8,
        paddingLeft: 8,
        paddingTop: 8
    },
    SYNC: {
        backgroundColor: "#F2F2F2",
        padding: 8,
        borderWidth: 1,
        borderColor: "#333"
    },
    QUEUE_TITLE: {
        paddingLeft: 8,
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 8,
        backgroundColor: "#333",
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderWidth: 1,
        borderColor: "#333",
        marginLeft: 8
    },
    QUEUE_TEXT: {
        color: "#FFFFFF"
    }
})

class Profile extends Component {
    static navigationOptions = () => {
        return {
            title: global._("PROFILE")
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            user: {},
            password: "",
            conf_password: "",
            showPasswordUpdate: false
        }
    }

    componentWillMount() {
        try {
            AsyncStorage.multiGet([
                "@SETTINGS:account_name",
                "@SETTINGS:name",
                "@SETTINGS:email",
                "@SETTINGS:organization_name"
            ]).then((data) => {
                let user = {};
                data.forEach(item => {
                    user[item[0].split(":")[1]] = item[1];
                })

                this.setState({
                    user: user
                })
            });
        } catch (err) {
            console.log(err)
        }
    }

    _updatePassword = () => {
        this.setState({
            showPasswordUpdate: true
        })
    };

    _doPasswordUpdate = () => {
        api.updatePassword([this.state.password, this.state.conf_password], {})
            .then(res => {
                console.log(res);
            })
            .catch(err => {
                console.log(err)
            })

    };

    _cancelPasswordUpdate = () => {
        this.setState({
            showPasswordUpdate: false
        })
    };

    _closeModal = () => {
        this.setState({
            showPasswordUpdate: false
        })
    };

    onPassChange = (text) => {
        this.setState({
            password: text
        })
    };

    onPassConfChange = (text) => {
        this.setState({
            conf_password: text
        })
    };

    render() {

        let passView = (
            <View style={STYLE.row}>
                <ActionButton
                    title={global._("CHANGE_PASSWORD")}
                    onPress={this._updatePassword}/>
            </View>
        )
        return (
            <View style={styles.VIEW_MAIN}>
                <View style={style.image}>
                    <Image source={require("../img/ic_launcher.png")}/>
                </View>

                <View style={STYLE.row}>
                    <Text style={STYLE.label}>{global._("NAME")}</Text>
                    <Text style={STYLE.value}>{this.state.user.name}</Text>
                </View>

                <View style={STYLE.row}>
                    <Text style={STYLE.label}>{global._("EMAIL")}</Text>
                    <Text style={STYLE.value}>{this.state.user.email}</Text>
                </View>

                <View style={STYLE.row}>
                    <Text style={STYLE.label}>{global._("ORGANIZATION")}</Text>
                    <Text style={STYLE.value}>{this.state.user.organization_name}</Text>
                </View>

                <View style={STYLE.row}>
                    <Text style={STYLE.label}>{global._("ACCOUNT")}</Text>
                    <Text style={STYLE.value}>{this.state.user.account_name}</Text>
                </View>


                <Modal
                    transparent={false}
                    style={styles.MODAL_STYLE}
                    animationType="slide"
                    onRequestClose={this._closeModal}
                    visible={this.state.showPasswordUpdate}>
                    <View style={styles.ACTIONS_MODAL}>

                        <View style={STYLE.row}>
                            <Text style={STYLE.label}>{global._("PASSWORD")}*:</Text>
                            <TextInput
                                editable={true}
                                keyboardType="default"
                                onChangeText={this.onPassChange}
                                value={this.state.password}/>
                        </View>

                        <View style={STYLE.row}>
                            <Text style={STYLE.label}>{global._("PASSWORD_CONF")}*:</Text>
                            <TextInput
                                editable={true}
                                keyboardType="default"
                                onChangeText={this.onPassConfChange}
                                value={this.state.conf_password}/>
                        </View>

                        <ActionButton
                            title={global._("UPDATE_PASSWORD")}
                            color="grey"
                            onPress={this._doPasswordUpdate}/>
                        <LineSpacer/>
                        <ActionButton
                            title={global._("CANCEL")}
                            color="red"
                            onPress={this._cancelPasswordUpdate}/>
                    </View>
                </Modal>
            </View>
        )
    }
}

export default Profile;