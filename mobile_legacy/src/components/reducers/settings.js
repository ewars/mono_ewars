import * as types from "../actions/actionTypes";

const initialState = {
    userToken: null
};

export default function settings(state = initialState, action = {}) {
    switch (action.type) {
        case (types.SET_USER_TOKEN):
            let newState = Object.assign({}, state, {userToken: action.data});
            return newState;
        default:
            return state;
    }
}