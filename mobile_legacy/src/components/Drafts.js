import React, {Component} from "react";
import {
    View,
    Text,
    ScrollView,
    AsyncStorage,
    StyleSheet,
    TouchableHighlight,
    Image
} from "react-native";
import DialogAndroid from "react-native-dialogs";

import Utils from "./utils";
import Storage from "./lib/storage";
import styles from "./STYLES";
import API from "./api/api";

const style = StyleSheet.create({
    ROW: {
        flexDirection: "column",
        padding: 8,
        borderBottomWidth: 1,
        borderColor: "#F2F2F2"
    }
})

class Item extends Component {
    _select = () => {
        this.props.select(this.props.data);
    };

    render() {
        return (
            <TouchableHighlight onPress={this._select}>
                <View style={styles.LIST_ITEM}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text>{Utils.i18n(this.props.data.meta.form_name)}</Text>
                        {this.props.data.meta.location_name ?
                            <Text>{Utils.i18n(this.props.data.meta.location_name)}</Text>
                            : null}
                        {this.props.data.data_date ?
                            <Text>{this.props.data.data_date}</Text>
                            : null}
                    </View>
                    <View style={style.LIST_ITEM_RIGHT}>
                        <Image
                            style={styles.LIST_ITEM_CARET}
                            source={require("../img/ic_keyboard_arrow_right_black_24dp.png")}/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class Drafts extends Component {
    static navigationOptions = () => {
        return {
            title: global._("DRAFTS")
        }
    };

    static defaultProps = {
        form_id: null
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            _isLoaded: false
        }

        this._sub = this.props.navigation.addListener(
            'didFocus',
            this._getDrafts
        );
    }

    componentWillMount() {
        this._getDrafts();
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    componentWillReceiveProps() {
        this._getDrafts();
    }

    _getDrafts = (form_id) => {
        const data = this.props.navigation.getParam("data", {});
        Storage.getDrafts(data.form ? (data.form.id || null) : null)
            .then((data) => {
                this.setState({
                    data: data,
                    _isLoaded: true
                })
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _showReport = (_data) => {

        if (global.USER.role == "USER") {
            Storage.getAssignment(_data.form_id)
                .then((assignment) => {
                    this.props.navigation.navigate("FORM", {
                        data: {
                            uuid: _data.uuid,
                            data: _data.data,
                            data_date: _data.data_date,
                            location_id: _data.location_id,
                            form_id: _data.form_id,
                            status: "DRAFT"
                        },
                        form: {
                            id: _data.form_id,
                            name: assignment.form_name,
                            definition: assignment.definition,
                            features: assignment.features,
                            version_id: assignment.version_id || null
                        },
                        assignment: assignment
                    })
                })
                .catch((err) => {
                    console.log(err);
                })
        } else {
            API.getForm([_data.form_id])
                .then((resp) => {
                    this.props.navigation.navigate("FORM", {
                        data: {
                            uuid: _data.uuid,
                            data: _data.data,
                            data_date: _data.data_date,
                            location_id: _data.location_id,
                            form_id: _data.form_id,
                            status: "DRAFT"
                        },
                        form: resp.data,
                        assignment: null
                    })
                })
                .catch((err) => {
                    console.log(err);
                })
        }
    };

    render() {

        if (this.state._isLoaded && this.state.data.length <= 0) {
            return (
                <Text style={styles.PLACEHOLDER}>{global._("NO_DRAFTS_AVAILABLE")}</Text>
            )
        }

        return (
            <View>
                <ScrollView>
                    {this.state.data.map((item, idx) => {
                        return <Item
                            select={this._showReport}
                            key={idx}
                            data={item}/>
                    })}
                </ScrollView>
            </View>
        )
    }
}

export default Drafts;
