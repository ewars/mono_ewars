import React, {Component} from "react";
import {
    View,
    Text,
    ActivityIndicator
} from "react-native";

class Syncing extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <View>
                    <Text>Syncing</Text>
                </View>
                <View>
                    <ActivityIndicator/>
                </View>
            </View>

        )
    }
}

export default Syncing;