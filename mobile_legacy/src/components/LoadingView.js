import React, {Component} from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    Button
} from "react-native";

const style = StyleSheet.create({
    LOAD_VIEW: {
        flex: 1,
        flexDirection: "column"
    },
    IMAGE: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 120
    },
    IMG: {
        marginBottom: 10
    },
    BUTTON_PAD: {
        marginTop: 20
    }
})

class LoadingView extends Component {
    static defaultProps = {
        cancel: () => {}
    };

    constructor(props) {
        super(props);
    }

    render() {
        let message = this.props.message ? this.props.message : global._("LOADING");
        return (
            <View style={style.LOAD_VIEW}>
                <View style={style.IMAGE}>
                    <Image style={style.IMG} source={require("../img/ic_launcher.png")}/>
                    <Text>{message}</Text>
                    {/*<View style={style.BUTTON_PAD}>*/}
                        {/*<Button*/}
                            {/*title="Cancel"*/}
                            {/*color="red"*/}
                            {/*onPress={this.props.cancel}/>*/}
                    {/*</View>*/}
                </View>
            </View>
        )
    }
}

export default LoadingView;