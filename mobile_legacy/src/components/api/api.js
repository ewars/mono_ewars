import BaseAPI from "./_base";

const APIS = {
    // General APIs
    getMetrics: "com.ewars.metrics",

    // Form APIS
    getFormDefinition: "com.ewars.form.definition",
    getForm: "com.ewars.form",
    getForms: "com.ewars.forms",
    submitForm: "com.ewars.form.submit",
    amendSubmission: "com.ewars.form.submission.amend",
    retractSubmissions: "com.ewars.form.submission.retract",

    // assignments
    getAssignment: "com.ewars.user.assignment",

    // Alerts
    getAlarm: "com.ewars.alarm",
    getAlert: "com.ewars.alert",
    getAlerts: "com.ewars.alerts",
    getOpenAlerts: "com.ewars.alerts.open",
    getMonitoredAlerts: "com.ewars.alerts.monitored",
    doAlertAction: "com.ewars.alert.action",
    commentAlert: "com.ewars.alert.comment",
    queryAlertActivity: "com.ewars.alert.activity",
    queryAlertUsers: "com.ewars.alert.users",
    getAlertStage: "com.ewars.alert.stage",

    // Users
    getUser: "com.ewars.user",

    // Assignments
    getAssignments: "com.ewars.assignments",

    // Self
    updateToken: "com.ewars.user.token",
    updateProfile: "com.ewars.user.profile.update",

    // locations
    getLocations: "com.ewars.locations",
    getLocation: "com.ewars.location",

    queryCollection: "com.ewars.collections",

    updateDeviceInfo: "com.ewars.device",
    getTriggeringReport: "com.ewars.alert.report",
    submitLocationUpdate: "com.ewars.location.update_geom",
    pushLocationUpdates: "com.ewars.recv_geom_updates",
    submitAlertComment: "com.ewars.alert.comment",
    updatePassword: "com.ewars.password.update",

    testEndpoint: "com.ewars.ping"

};

let API = {};

for (var m in APIS) {
    let newFunc = function(args, kwargs) {
        let call = APIS[arguments.callee._name];
        return new Promise((resolve, reject) => {
            BaseAPI._call(call, args, kwargs)
                .then((resp) => {
                    resolve(resp);
                })
                .catch((err) => {
                    reject(err);
                })
        })

    };
    newFunc._name = m;
    API[m] = newFunc;
}

export default API;
