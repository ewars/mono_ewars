import React, {Component} from "react";
import {
    View,
    ListView,
    ScrollView,
    Text,
    TouchableHighlight,
    StyleSheet,
    AsyncStorage,
    Modal
} from "react-native";

import Utils from "./utils";
import Form from "./Form";

import styles from "./STYLES";

const STYLES = StyleSheet.create({
    row: {
        flexDirection: "row",
        justifyContent: "center",
        backgroundColor: "#FFFFFF",
        borderBottomWidth: 1,
        borderBottomColor: "#F2F2F2"
    },
    cell: {
        flexDirection: "column",
        padding: 8,
        flex: 4
    },
    cell_int: {
        backgroundColor: "#a9a9a9",
        borderRightWidth: 1,
        borderColor: "#F2F2F2",
        width: 150,
        flex: 1,
        padding: 8,
        alignItems: "center"
    },
    text: {
        flex: 1,
        fontWeight: 'bold'
    },
    text_under: {
        fontSize: 12
    }
});

const INTERVAL = {
    NONE: "Daily",
    DAY: "Daily",
    WEEK: "Weekly",
    MONTH: "Monthly",
    YEAR: "Yearly"
}

class Item extends Component {
    constructor(props) {
        super(props);
    }

    _onPress = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let locations = this.props.data.assignments.map(item => {
            return Utils.i18n(item.location_name);
        });

        locString = locations.join(", ");

        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={STYLES.row}>

                    <View style={STYLES.cell}>
                        <Text style={STYLES.text}>{Utils.i18n(this.props.data.form_name)}</Text>
                        <Text style={STYLES.text_under}>
                            {locString}
                            </Text>
                    </View>
                </View>
            </TouchableHighlight>

        )
    }
}

class DraftItem extends Component {
    constructor(props) {
        super(props);
    }

    _onPress = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let draftDate = "Date unset";
        if (this.props.data.data_date)  draftDate = Utils.DATE(this.props.data.data_date, this.props.data.meta.interval || "DAY");

        let locName = "Location unset";
        if (this.props.data.meta.location_name) locName = Utils.i18n(this.props.data.meta.location_name);

        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={STYLES.row}>

                    <View style={STYLES.cell}>
                        <Text style={STYLES.text}>{Utils.i18n(this.props.data.meta.form_name)}</Text>
                        <Text style={STYLES.text_under}>
                            {locName}&nbsp;|&nbsp;{draftDate}
                        </Text>
                    </View>
                </View>
            </TouchableHighlight>

        )
    }
}

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.HEADER_WRAPPER}>
                <Text style={styles.HEADER_TEXT}>{this.props.label}</Text>
            </View>
        )
    }
}


class Assignments extends Component {
    constructor(props) {
        super(props)

        this.state = {
            view: "LIST",
            assignments: [],
            assignment: null,
            drafts: [],
            modal: false,
            draft: false
        }
    }

    componentWillMount() {
        this._reload();
    }

    _reload = () => {
        try {
            AsyncStorage.multiGet(["@SETTINGS:assignments", "@DATA:drafts"], (err, stores) => {
                let assignments = [];
                let drafts = [];
                stores.forEach((result, i, store) => {
                    let key = result[0].split(":")[1];
                    let val = result[1];

                    if (val) {
                        val = JSON.parse(val);
                    }

                    if (key == "assignments") assignments = val;
                    if (key == "drafts") drafts = val;
                });

                this.setState({
                    assignments: assignments,
                    drafts: drafts || [],
                    data: null,
                    modal: false
                })
            })


        } catch (err) {
            console.log(err)
        }
    };

    _renderRow = (item, sectionId, rowid, highlightRow) => {
        return <Item data={item} onSelect={highlightRow}/>
    };

    _onClickRow = (data) => {
        this.setState({
            data: null,
            assignment: data,
            modal: true
        })
    };

    _cancel = () => {
        this._reload();
    };

    _onSelectDraft = (data) => {
        // Get the assignment
        let assignment;

        this.state.assignments.forEach((item) => {
            if (data.form_id == item.form_id) {
                assignment = item;
            }
        });

        this.setState({
            assignment: assignment,
            modal: true,
            data: data,
            draft: true
        })
    };

    render() {
        let drafts = <Text style={styles.PLACEHOLDER}>No drafts</Text>;

        if (this.state.drafts.length > 0) {
            drafts = this.state.drafts.map((item, index) => {
                return <DraftItem
                            onSelect={this._onSelectDraft}
                            data={item}
                            key={"DRAFT_" + index}/>
            })
        }

        let assignments = <Text style={styles.PLACEHOLDER}>No assignments</Text>;

        if (this.state.assignments.length > 0) {
            assignments = this.state.assignments.map(item => {
                return <Item
                    onSelect={this._onClickRow}
                    data={item}
                    key={item.form_id}/>
            })
        }

        return (
            <View>
                <ScrollView>

                    <Header label="Assignments"/>
                    {assignments}

                    <Header label="Drafts"/>
                    {drafts}
                </ScrollView>
                <Modal
                    onRequestClose={() => {}}
                    visible={this.state.modal}>
                    {this.state.assignment ?
                        <Form
                            draft={this.state.draft}
                            cancel={this._cancel}
                            data={this.state.data || null}
                            assignment={this.state.assignment}/>
                        : null}
                </Modal>
            </View>
        )

    }
}

export default Assignments;
