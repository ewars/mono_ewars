import React, {Component} from "react";

import {
    View,
    Text,
    TextInput,
    TouchableHighlight,
    Image,
    Button
} from "react-native";

import styles from "./STYLES";

import Utils from "./utils";

export class FormLabel extends Component {
    render() {
        return (
            <View style={styles.INPUT_LABEL_WRAPPER}>
                <Text style={styles.INPUT_LABEL}>{this.props.text}</Text>
            </View>
        )
    }
}

export class TextAreaField extends Component {
    _onChange = (text) => {
        this.props.onChange(this.props.name, text);
    };

    render() {
        return (
            <TextInput
                style={styles.TEXT_AREA}
                multiline={true}
                numberOfLines={6}
                editable={!this.props.disabled}
                value={this.props.value}
                onChangeText={this._onChange}/>
        )
    }
}


export class ListItem extends Component {
    constructor(props) {
        super(props)
    }

    _onPress = () => {
        this.props.onAction(this.props.data);
    };

    render() {
        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={styles.LIST_ITEM}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text style={styles.LIST_ITEM_TEXT}>{Utils.i18n(this.props.data.title)}</Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        <Image
                            style={styles.LIST_ITEM_CARET}
                            source={require("../img/ic_keyboard_arrow_right_black_24dp.png")}/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

export class ActionButton extends Component {
    render() {
        return (
            <View style={styles.ACTION_BUTTON_WRAPPER}>
                <Button
                    style={styles.ACTION_BUTTON}
                    color={this.props.color || null}
                    title={this.props.title}
                    onPress={this.props.onPress}/>
            </View>

        )
    }
}

export class LineSpacer extends Component {
    render() {
        return (
            <View style={styles.LINE_SPACER}/>
        )
    }
}

export class Error extends Component {
    render() {
        console.log(this.props.text);
        return (
            <View style={styles.ERROR}>
                <Text style={styles.ERROR_TEXT}>{this.props.text}</Text>
            </View>
        )
    }
}

