import React, {Component} from "react";

import {
    View,
    ScrollView,
    Picker,
    Button,
    Text
} from "react-native";
import DialogAndroid from "react-native-dialogs";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";

import {
    TextAreaField,
    FormLabel,
    Error
} from "../common";

class Outcome extends Component {
    static navigationOptions = {
        title: global._("OUTCOME")
    };

    constructor(props) {
        super(props);

        this.state = {
            comments: "",
            outcome: "DISCARD"
        }
    }

    componentWillMount() {
        this._query();
    }

    componentWillReceiveProps() {
        this._query();
    }

    _query = () => {
        const alert = this.props.navigation.getParam("alert", {});
        API.getAlertStage([alert.uuid, "OUTCOME"])
            .then((resp) => {
                if (resp.data) {
                    this.setState({
                        comments: resp.data.data.comments,
                        outcome: resp.data.data.outcome,
                        data: resp.data
                    })
                }

            })
            .catch((err) => {
                console.log(err);
            })
    };

    _onCommentChange = (prop, value) => {
        this.setState({
            comments: value,
            error: null
        });
    };

    _onOutcomeChange = (value) => {
        this.setState({
            outcome: value,
            error: null
        });
    };

    _submit = () => {
        if (!this.state.outcome || this.state.outcome == "") {
            this.setState({
                error: global._("ERR_OUTCOME")
            })
            return;
        }

        let id;
        if (this.state.data) id = this.state.data.id || null;

        const alert = this.props.navigation.getParam("alert", {});
        API.doAlertAction([
            alert.uuid,
            id,
            {
                alert_id: alert.uuid,
                type: "OUTCOME",
                status: "SUBMITTED",
                data: {
                    comments: this.state.comments,
                    outcome: this.state.outcome
                }


            }
        ])
            .then((resp) => {

                if (["DISCARD", "DISCARDED", "RESPOND"].indexOf(this.state.outcome) >= 0) {
                    global.RELOAD_ALERTS = true;
                    this.props.navigation.navigate("ALERTS");
                } else {
                    global.RELOAD_ALERT = true;
                    this.props.navigation.goBack();
                }
            })
            .catch((err) => {
                console.log(err);
                this.setState({
                    error: global._("ERR_UNKNOWN")
                })
            })

    };

    render() {
        let readOnly = false;

        const alert = this.props.navigation.getParam("alert", {});

        if (alert.stage == "VERIFICATION") {
            return (
                <View style={styles.CARD}>
                    <Text style={styles.PLACEHOLDER}>{global._("PLEASE_VERIFICATION")}</Text>
                </View>
            )
        }

        if (alert.stage == "RISK_ASSESS") {
            return (
                <View style={styles.CARD}>
                    <Text style={styles.PLACEHOLDER}>{global._("PLEASE_RISK_ASSESS")}</Text>
                </View>
            )
        }

        if (alert.stage == "RISK_CHAR") {
            return (
                <View style={styles.CARD}>
                    <Text style={styles.PLACEHOLDER}>{global._("PLEASE_RISK_CHAR")}</Text>
                </View>
            )
        }

        if (this.state.data) {
            if (this.state.data.state == "SUBMITTED") readOnly = true;
        }

        return (
            <ScrollView>
                <View style={styles.CARD}>

                    {this.state.error ?
                        <Error text={this.state.error}/>
                    : null}

                    <FormLabel text="Outcome"/>

                    <Picker
                        enabled={!readOnly}
                        selectedValue={this.state.outcome}
                        onValueChange={this._onOutcomeChange}>
                        <Picker.Item label={global._("NONE_SELECTED")} value=""/>
                        <Picker.Item label={global._("DISCARD")} value="DISCARD"/>
                        <Picker.Item label={global._("MONITOR")} value="MONITOR"/>
                        <Picker.Item label={global._("RESPOND")} value="RESPOND"/>
                    </Picker>

                    <FormLabel text="Comments"/>

                    <TextAreaField
                        disabled={readOnly}
                        name={global._("COMMENTS")}
                        value={this.state.comments}
                        onChange={this._onCommentChange}/>

                    {!readOnly ?
                        <Button
                            title={global._("BUTTON_SUBMIT_OUTCOME")}
                            onPress={this._submit}/>
                        : null}
                </View>
            </ScrollView>
        )
    }
}

export default Outcome;
