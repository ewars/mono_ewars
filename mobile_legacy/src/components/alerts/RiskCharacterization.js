import React, {Component} from "react";

import {
    View,
    ScrollView,
    TextInput,
    Picker,
    Button,
    Text
} from "react-native";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";
import CONSTANTS from "../constants";

import {
    FormLabel,
    Error
} from "../common";

var RISK_CONTENT = {};
RISK_CONTENT[CONSTANTS.LOW] = global._("RISK_LOW_MESSAGE");
RISK_CONTENT[CONSTANTS.MODERATE] = global._("RISK_MODERATE_MESSAGE");
RISK_CONTENT[CONSTANTS.HIGH] = global._("RISK_HIGH_MESSAGE");
RISK_CONTENT[CONSTANTS.SEVERE] = global._("RISK_SEVERE_MESSAGE");

const LIKELIHOOD = global._("LIKELIHOOD");
const CONSEQUENCES = global._("CONSEQUENCES");

var definition = [
    [
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH]
    ]
];

const REMAP = {
    SEVERE: global._("SEVERE_RISK"),
    HIGH: global._("HIGH_RISK"),
    MODERATE: global._("MODERATE_RISK"),
    LOW: global._("LOW_RISK")
}

var RISK_NAMES = {
    LOW: [CONSTANTS.GREEN, global._("LOW_RISK")],
    MODERATE: [CONSTANTS.YELLOW, global._("MODERATE_RISK")],
    HIGH: [CONSTANTS.ORANGE, global._("HIGH_RISK")],
    SEVERE: [CONSTANTS.RED, global._("SEVERE_RISK")]
}

class TextField extends Component {
    _onChange = (text) => {
        this.props.onChange(this.props.name, text);
    };

    render() {
        return (
            <View>
                <TextInput
                    multiline={true}
                    numberOfLines={6}
                    editable={true}
                    value={this.props.value}
                    onChange={this._onChange}/>
            </View>
        )
    }
}

class RiskCharacterization extends Component {
    static navigationOptions = {
        title: global._("RISK_CHAR")
    };

    constructor(props) {
        super(props);

        this.state = {
            likelihood: -1,
            consequences: -1,
            data: null
        }
    }

    componentWillMount() {
        this._query();
    }

    componentWillReceiveProps() {
        this._query();
    }

    _query = () => {
        const alert = this.props.navigation.getParam("alert", {});
        API.getAlertStage([alert.uuid, "RISK_CHAR"])
            .then((resp) => {
                if (resp.data) {
                    this.setState({
                        likelihood: resp.data.data.riskIndex[0],
                        consequences: resp.data.data.riskIndex[1],
                        data: resp.data
                    })
                }
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _onChange = (prop, text) => {
        this.setState({
            [prop]: text,
            error: null
        });
    };

    _onLikeChange = (value) => {
        this.setState({
            likelihood: value,
            error: null
        })
    };

    _onConsChange = (value) => {
        this.setState({
            consequences: value,
            error: null
        })
    };

    _isSet = (val) => {
        if (val === 0) return true;
        if (val == -1) return false;
        if (val == undefined) return false;
        if (val == null) return false;
        if (val == "") return false;
        return true;
    };

    _submit = () => {
        if (!this._isSet(this.state.likelihood)) {
            this.setState({
                error: global._("ERR_LIKELIHOOD")
            })
            return;
        }

        if (!this._isSet(this.state.consequences)) {
            this.setState({
                error: global._("ERR_CONSEQUENCES")
            })
            return;
        }

        let risk = definition[this.state.likelihood][this.state.consequences][1];


        let id;
        if (this.state.data) id = this.state.data.id || null;

        const alert = this.props.navigation.getParam("alert", {});
        API.doAlertAction([
            alert.uuid,
            id,
            {
                alert_id: alert.uuid,
                type: "RISK_CHAR",
                status: "SUBMITTED",
                data: {
                    riskIndex: [this.state.likelihood, this.state.consequences],
                    risk: risk
                }
            }
        ])
            .then((resp) => {

                global.RELOAD_ALERT = true;
                this.props.navigation.goBack();
            })
            .catch((err) => {
                this.setState({
                    errors: [
                        global._("ERR_UNKNOWN")
                    ]
                })
            })

    };

    render() {
        let readOnly = false;

        const alert = this.props.navigation.getParam("alert", {});
        if (alert.stage == "VERIFICATION") {
            return (
                <View style={styles.CARD}>
                    <Text style={styles.PLACEHOLDER}>Please complete verification</Text>
                </View>
            )
        }

        if (alert.stage == "RISK_ASSESS") {
            return (
                <View style={styles.CARD}>
                    <Text style={styles.PLACEHOLDER}>Please complete Risk Assessment</Text>
                </View>
            )
        }


        if (this.state.data) {
            if (this.state.data.status == "SUBMITTED") readOnly = true;
        }

        let risk = "Unknown", riskColor, lContent, cContent, aContent;
        if (this._isSet(this.state.likelihood)) {
            lContent = LIKELIHOOD[this.state.likelihood];
        }

        if (this._isSet(this.state.consequences)) {
            cContent = CONSEQUENCES[this.state.consequences];
            cContent = cContent.join("\n");
        }

        if (this._isSet(this.state.likelihood) && this._isSet(this.state.consequences)) {
            risk = definition[this.state.likelihood][this.state.consequences][1];
            risk = REMAP[risk];
            riskColor = definition[this.state.likelihood][this.state.consequences][0];
            aContent = RISK_CONTENT[risk];
        }


        return (
            <ScrollView>
                <View style={styles.CARD}>

                    {this.state.error ?
                        <Error text={this.state.error}/>
                    : null}

                    <View style={styles.FIELD}>
                        <FormLabel text={global._("LIKELIHOOD_LABEL")}/>

                        <Picker
                            onValueChange={this._onLikeChange}
                            enabled={!readOnly}
                            selectedValue={this.state.likelihood}>
                            <Picker.Item label={global._("NONE_SELECTED")} value={-1}/>
                            <Picker.Item label={global._("ALMOST_CERTAIN")} value={0}/>
                            <Picker.Item label={global._("HIGHLY_LIKELY")} value={1}/>
                            <Picker.Item label={global._("LIKELY")} value={2}/>
                            <Picker.Item label={global._("UNLIKELY")} value={3}/>
                            <Picker.Item label={global._("VERY_UNLIKELY")} value={4}/>
                        </Picker>
                        <Text style={styles.TEXT_BLOCK}>{lContent}</Text>
                    </View>


                    <View style={styles.FIELD}>
                        <FormLabel text={global._("CONSEQUENCES_LABEL")}/>

                        <Picker
                            enabled={!readOnly}
                            onValueChange={this._onConsChange}
                            selectedValue={this.state.consequences}>
                            <Picker.Item label={global._("NONE_SELECTED")} value={-1}/>
                            <Picker.Item label={global._("MAJOR")} value={3}/>
                            <Picker.Item label={global._("SEVERE")} value={4}/>
                            <Picker.Item label={global._("MODERATE")} value={2}/>
                            <Picker.Item label={global._("MINOR")} value={1}/>
                            <Picker.Item label={global._("MINIMAL")} value={0}/>
                        </Picker>
                        <Text style={styles.TEXT_BLOCK}>{cContent}</Text>
                    </View>

                    <Text style={styles.RISK_TITLE}>{global._("RISK")}: {risk}</Text>
                    <Text style={styles.TEXT_BLOCK}>{aContent}</Text>


                    {!readOnly ?
                        <Button
                            title={global._("BUTTON_SUBMIT_CHAR")}
                            onPress={this._submit}/>
                        : null}
                </View>
            </ScrollView>
        )
    }
}

export default RiskCharacterization;
