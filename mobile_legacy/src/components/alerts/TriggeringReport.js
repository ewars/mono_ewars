import React, {Component} from "react";

import {
    View,
    ScrollView,
    ListView,
    Text,
    TextInput,
    TouchableHighlight,
    Image,
    StyleSheet,
    Picker
} from "react-native";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";


class TriggeringReport extends Component {
    static navigationOptions = {
        title: global._("TRIGGERING_RECORD")
    };

    constructor(props) {
        super(props);

        this.state = {
            triage: "",
            outcome: null
        }
    }

    _onChange = (text) => {
        this.setState({triage: text});
    };

    _onOutcomeChange = (value) => {
        this.setState({outcome: value});
    };

    render() {
        return (
            <View>
                <View><Text>Alert Verification</Text></View>

                <View><Text>Triage</Text></View>

                <View>
                    <TextInput
                        multiline={true}
                        numberOfLines={6}
                        editable={true}
                        value={this.state.triage}
                        onChange={this._onChange}/>
                </View>

                <View><Text>Outcome</Text></View>

                <View>
                    <Picker selectedValue={this.state.outcome} onValueChange={this._onOutcomeChange}>
                        <Picker.Item label="Discard" value="DISCARD"/>
                        <Picker.Item label="Monitor" value="MONITOR"/>
                        <Picker.Item label="Start Risk Assessment" value="ADVANCE"/>
                    </Picker>
                </View>
            </View>
        )
    }
}

export default TriggeringReport;