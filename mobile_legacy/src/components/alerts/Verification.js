import React, {Component} from "react";

import {
    View,
    ScrollView,
    Picker,
    Button
} from "react-native";
import DialogAndroid from "react-native-dialogs";

import {
    FormLabel,
    TextAreaField,
    Error
} from "../common";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";


class Verification extends Component {
    static navigationOptions = {
        title: global._("VERIFICATION")
    };

    constructor(props) {
        super(props);

        this.state = {
            triage: "",
            outcome: null,
            data: null
        }
    }

    componentWillMount() {
        const alert = this.props.navigation.getParam("alert", {});
        this._getStage(alert.uuid, "VERIFICATION");
    }

    componentWillReceiveProps(props) {
        const alert = this.props.navigation.getParam("alert", {});
        this._getStage(alert.uuid, "VERIFICATION");
    }

    _getStage = (alert_id, stage) => {
        API.getAlertStage([alert_id, stage])
            .then((resp) => {
                if (resp.data) {
                    this.setState({
                        outcome: resp.data.data.outcome,
                        triage: resp.data.data.content,
                        data: resp.data
                    })
                }
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _onChange = (prop, text) => {
        this.setState({
            [prop]: text,
            error: null
        });
    };

    _onOutcomeChange = (value) => {
        this.setState({
            outcome: value,
            error: null,
            data: {
                status: "DRAFT"
            }
        });
    };

    _submit = () => {
        if (!this.state.outcome || this.state.outcome == "") {
            this.setState({
                error: global._("ERR_VERIFICATION_OUTCOME")
            })
            return;
        }

        if (!this.state.triage || this.state.triage == "") {
            this.setState({
                error: global._("ERR_TRIAGE")
            })
            return;
        }

        let id;
        if (this.state.data) id = this.state.data.id || null;

        const alert = this.props.navigation.getParam("alert", {});
        API.doAlertAction([
            alert.uuid,
            id,
            {
                alert_id: alert.uuid,
                type: "VERIFICATION",
                status: "SUBMITTED",
                data: {
                    outcome: this.state.outcome,
                    content: this.state.triage
                }
            }
        ])
            .then((resp) => {

                if (this.state.outcome == "DISCARD") {
                    global.RELOAD_ALERTS = true;
                    this.props.navigation.navigate("ALERTS");
                } else {

                    global.RELOAD_ALERT = true;
                    this.props.navigation.goBack();
                }

            })
            .catch((err) => {
                this.setState({
                    error: global._("ERR_UNKNOWN")
                })
            })

    };

    render() {
        let readOnly = false;

        if (this.state.data) {
            if (this.state.data.status !== "SUBMITTED" || this.state.outcome == "MONITOR") {
                readOnly = false;
            } else {
                readOnly = true;
            }
        }
        return (
            <ScrollView style={styles.SCROLLVIEW}>
                <View style={styles.CARD}>

                    {this.state.error ?
                        <Error text={this.state.error}/>
                        : null}

                    <FormLabel text={global._("VERIFICATION_COMMENTS")}/>

                    <TextAreaField
                        name="triage"
                        disabled={readOnly}
                        value={this.state.triage}
                        onChange={this._onChange}/>

                    <FormLabel text={global._("VERIFICATION_OUTCOME")}/>

                    <Picker
                        enabled={!readOnly}
                        selectedValue={this.state.outcome}
                        onValueChange={this._onOutcomeChange}>
                        <Picker.Item label={global._("NONE_SELECTED")} value=""/>
                        <Picker.Item label={global._("DISCARD")} value="DISCARD"/>
                        <Picker.Item label={global._("MONITOR")} value="MONITOR"/>
                        <Picker.Item label={global._("START_RISK_ASSESSMENT")} value="ADVANCE"/>
                    </Picker>

                    {!readOnly ?
                        <Button
                            onPress={this._submit}
                            title={global._("BUTTON_SUBMIT_VERIFICATION")}/>
                        : null}
                </View>
            </ScrollView>
        )
    }
}

export default Verification;
