import React, {Component} from "react";

import {
    View,
    ScrollView,
    ListView,
    Text,
    TextInput,
    TouchableHighlight,
    Image,
    StyleSheet,
    Picker,
    Button,
    ToastAndroid
} from "react-native";
import Icon from "react-native-fontawesome-pro";
import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";

const BASE_OFFSET = 15;

const STATES = {
    "VERIFICATION": {icon: "fa-check", color: "green", label: "Verified"},
    "UNVERIFIED": {icon: "fa-exclamation-circle", color: "red", label: "Unverified"}
};

const ACTIONS = {
    VERIFIED: "verified this alert on",
    ASSESSED: "assessed this alert on",
    CHARACTERIZED: "characterized this alert on",
    OUTCOME: "set the outcome for this alert on",
    REOPEN: "reopened this alert on",
    DISCARDED: "discarded this alert on",
    AUTOCLOSED: "discarded this alert on",
    MONITOR: "set this alerts state to Monitor on",
    RESPOND: "set this alerts outcome to Respond on",
    CLOSED: "caused this alert to close on",
    RESPONSE: "set this alerts outcome to Respond on",
    REPORT_SUBMISSION: "submitted an investigation report on"
};

const EVENTS = {
    OPENED: "Alert opened on",
    CLOSED: "Alert was closed on",
    TRIGGERED: "Alert was raised on",
    AUTODISCARDED: "Alert was auto-discarded by the system due to inaction on",
    AUTOCLOSED: "Alert was auto-discarded by the system due to inaction on"
};

class ActivityItem extends Component {
    render() {
        let label;
        if (this.props.data.node_type == "EVENT") {
            label = EVENTS[this.props.data.action_type];
            label += " " + Utils.DATE(this.props.data.action_date, "DAY");
        } else if (this.props.data.node_type == "ACTION") {
            label = this.props.data.name + " ";
            label += ACTIONS[this.props.data.action_type] + " ";
            label += Utils.DATE(this.props.data.action_date, "DAY");
        } else {
            return (
                <View style={styles.LIST_ITEM}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text style={styles.BOLD}>{this.props.data.name} ({this.props.data.email})</Text>
                        <Text style={styles.COMMENT_BUBBLE}>{this.props.data.content}</Text>
                    </View>
                </View>
            )

        }
        return (
            <View style={styles.ACT_ITEM}>
                <Text>{label}</Text>
            </View>
        )
    }
}

class Activity extends Component {
    static navigationOptions = () => {

        return {
            title: global._("ACTIVITY"),
            headerRight: (
                <TouchableHighlight style={styles.NAV_BTN} onPress={() => {
                    Utils.emit("PUSH_MESSAGE")
                }}>
                    <Icon
                        containerStyle={{
                            marginTop: 8,
                            marginRight: 8
                        }}
                        name="paper-plane" type="light" size={30}/>
                </TouchableHighlight>
            )
        }
        title: global._("ACTIVITY")
    };

    constructor(props) {
        super(props);

        const alert = this.props.navigation.getParam("alert", {});
        const alarm = this.props.navigation.getParam("alarm", {});
        this.state = {
            data: [],
            offset: 0,
            alert: alert,
            alarm: alarm,
            comment: ""
        }

        Utils.listen("PUSH_MESSAGE", this._addComment);
    }

    componentWillMount() {
        const alert = this.props.navigation.getParam("alert", {});
        const alarm = this.props.navigation.getParam("alarm", {});
        this._query(alert.uuid, this.state.offset);
    }

    _query = (uuid, offset) => {
        API.queryAlertActivity([uuid, offset])
            .then((resp) => {
                this.setState({
                    data: resp.data,
                    offset: offset
                })
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _addComment = () => {
        const alert = this.props.navigation.getParam("alert", {});
        if (this.state.comment == "") {
            ToastAndroid.show("Please provide a comment", ToastAndroid.show);
            return;
        } else {

            API.submitAlertComment([alert.uuid, {uid: global.USER.id, comment: this.state.comment}])
                .then(res => {
                    this.setState({
                        comment: ""
                    }, () => {
                        this._query(alert.uuid, 0);
                        ToastAndroid.show("Alert comment submitted", ToastAndroid.SHORT);
                    })
                })
                .catch(err => {
                    console.log(err)
                    ToastAndroid.show("There was an error submitting your comment", ToastAndroid.SHORT);
                })
        }

    };

    _onCommentChange = (e) => {
        console.log(e);
        this.setState({
            comment: e
        })
    };

    render() {
        return (
            <View style={{display: "flex", flex: 1, flexDirection: "column"}}>

                    <ScrollView style={{flex: 1}}>
                        {this.state.data.map((item) => {
                            return <ActivityItem
                                key={"EVENT_" + item.id}
                                data={item}/>
                        })}
                    </ScrollView>
                <View style={{height: 100, maxHeight: 100, paddingLeft: 8, paddingRight: 8, paddingTop: 8, paddingBottom: 8, backgroundColor: "#FFFFFF", flex: 1}}>
                    <TextInput
                        multiline={true}
                        onChangeText={this._onCommentChange}
                        value={this.state.comment}/>
                </View>
            </View>
        )
    }
}

export default Activity;