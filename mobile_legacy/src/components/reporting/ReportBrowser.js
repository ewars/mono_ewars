import React, {Component} from "react";

import {
    View,
    ScrollView,
    ListView,
    Text,
    TouchableHighlight,
    Image,
    Modal
} from "react-native";
import Moment from "moment";
import DialogAndroid from "react-native-dialogs";

import styles from "../STYLES";

import Utils from "../utils";

import API from "../api/api";

import {
    ActionButton,
    LineSpacer
} from "../common";

import Location from "../fields/Location";
import DateField from "../fields/DateField";

class Collected extends Component {
    _select = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let locationName = Utils.i18n(this.props.data.location_name || {en: "Unknown"});
        let submittedDate = Moment.utc(this.props.data.submitted).format("MMM Do YYYY, h:mm a [UTC]");

        return (
            <TouchableHighlight onPress={this._select}>
                <View style={styles.LIST_ITEM}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text>{locationName}</Text>
                        <Text>{this.props.data.data_date}</Text>
                        <Text>Submitted: {submittedDate}</Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        <Image
                            style={styles.LIST_ITEM_CARET}
                            source={require("../../img/ic_keyboard_arrow_right_black_24dp.png")}/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class ReportBrowser extends Component {
    static navigationOptions = {
        title: "Records"
    };

    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.uuid !== r2.uuid});
        const data = this.props.navigation.getParam("config", {});

        this.state = {
            config: data,
            collections: [],
            location_id: null,
            start_date: null,
            end_date: null,
            offset: 0,
            ds: ds,
            _isLoaded: false,
            showFilters: false
        }
    }

    componentWillMount() {
        Utils.listen("SHOW_BROWSER_FILTER", this._showBrowserFilter);
        this._query(
            this.state.location_id,
            this.state.start_date,
            this.state.end_date,
            this.state.offset,
            false,
            false
        )
    }

    componentWillUnmount() {
        Utils.unListen("SHOW_BROWSER_FILTER", this._showBrowserFilter);
    }

    _showBrowserFilter = () => {
        this.setState({
            showFilters: true
        })
    };

    _query = (location_id, start_date, end_date, offset, clear, showFilters) => {
        if (showFilters == undefined) showFilters = false;
        const data = this.props.navigation.getParam("config", {});

        API.queryCollection([
            data.id,
            location_id || null,
            start_date || null,
            end_date || null,
            offset || 0
        ])
            .then((resp) => {
                let ds;
                let data;

                if (clear) {
                    ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.uuid !== r2.uuid});
                    ds = ds.cloneWithRows(resp.data);
                    data = resp.data;
                } else {
                    data = this.state.collections.concat(resp.data);
                    ds = this.state.ds.cloneWithRows(data);
                }

                this.setState({
                    collections: data,
                    offset: offset,
                    start_date: start_date,
                    end_date: end_date,
                    location_id: location_id,
                    ds: ds,
                    _isLoaded: true,
                    showFilters: showFilters
                })

                if (clear) {
                    this.refs.scrollView.scrollTo({x: 0, y: 0, animated: false});
                }
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _viewReport = (report) => {
        const data = this.props.navigation.getParam("config", {});
        this.props.navigation.navigate("REPORT", {
            form: data,
            data: report,
            lock: true
        })
    };

    _renderRow = (rowData) => {
        return (
            <Collected
                key={rowData.uuid}
                data={rowData}
                onSelect={this._viewReport}
                form={this.props.data}/>
        )
    };

    _paginate = () => {
        this._query(
            this.state.location_id,
            this.state.start_date,
            this.state.end_date,
            this.state.offset + 20,
            false,
            false
        )
    };

    _closeModal = () => {
        this.setState({showFilters: false});
    };

    _onLocationChange = (prop) => {
        this.setState({
            location_id: prop.uuid
        })
    };

    _onPropChange = (prop, value) => {
        this.setState({
            [prop]: value
        })
    };

    _clear = () => {
        this._query(
            null,
            null,
            null,
            0,
            true,
            false
        )
    };

    _applyFilter = () => {
        this._query(
            this.state.location_id,
            this.state.start_date,
            this.state.end_date,
            0,
            true,
            false
        )
    };

    render() {
        const form = this.props.navigation.getParam("config", {});
        if (this.state._isLoaded) {
            if (this.state.collections.length <= 0) {
                return (
                    <View style={styles.MAIN}>
                        <Text style={styles.PLACEHOLDER}>No Reports Available</Text>
                    </View>
                )
            }
        }

        let target_loc_type;
        if (form.features.LOCATION_REPORTING) {
            target_loc_type = form.features.LOCATION_REPORTING.site_type_id;
        }


        return (
            <View>
                <ListView
                    onEndReached={this._paginate}
                    initialListSize={0}
                    pageSize={1}
                    onEndReachedThreshold={300}
                    ref="scrollView"
                    dataSource={this.state.ds}
                    renderRow={this._renderRow}/>
                <Modal
                    transparent={false}
                    style={styles.MODAL_STYLE}
                    animationType="slide"
                    onRequestClose={this._closeModal}
                    visible={this.state.showFilters}>
                    <View style={styles.ACTIONS_MODAL}>
                        <Location
                            site_type_id={target_loc_type}
                            name="location"
                            value={this.state.location_id}
                            onChange={this._onLocationChange}/>

                        <LineSpacer/>
                        <Text style={styles.ASSIGN_HEADER_TEXT}>Report Date</Text>

                        <View style={styles.FIELD}>
                            <Text>After</Text>
                            <DateField
                                name="start_date"
                                data={{
                                    date_type: "DAY"
                                }}
                                value={this.state.start_date}
                                onChange={this._onPropChange}/>
                        </View>

                        <View style={styles.FIELD}>
                            <Text>Before</Text>
                            <DateField
                                data={{
                                    date_type: "DAY"
                                }}
                                name="end_date"
                                value={this.state.end_date}
                                onChange={this._onPropChange}/>
                        </View>

                        <LineSpacer/>
                        <ActionButton
                            title="Apply"
                            color="green"
                            onPress={this._applyFilter}/>
                        <ActionButton
                            title="Clear"
                            color="orange"
                            onPress={this._clear}/>
                        <ActionButton
                            title="Close"
                            color="grey"
                            onPress={this._closeModal}/>
                    </View>

                </Modal>
            </View>
        )
    }
}

export default ReportBrowser;