import React, {Component} from "react";

import {
    View,
    ScrollView,
    ListView,
    Text,
    TouchableHighlight,
    Image
} from "react-native";
import Icon from "react-native-fontawesome-pro";

import styles from "../STYLES";
import Storage from "../lib/storage";

import Utils from "../utils";


class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.HEADER_WRAPPER}>
                <Text style={styles.HEADER_TEXT}>{this.props.label}</Text>
            </View>
        )
    }
}


class ListItem extends Component {
    constructor(props) {
        super(props)
    }

    _onPress = () => {
        this.props.onAction(this.props.data);
    };

    render() {
        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={styles.LIST_ITEM}>
                    {this.props.icon ?
                    <View style={styles.LISTE_ITEM_LEFT}>
                        <Icon name={this.props.icon} size={15} type="light" containerStyle={{marginTop: 1, marginLeft: 8}}/>
                    </View>
                        : null}
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text style={styles.LIST_ITEM_TEXT}>{Utils.i18n(this.props.data.form_name)}</Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        <Icon name="chevron-right" type="light" size={15}/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class Assignment extends Component {
    static navigationOptions = () => {
        return {
            title: global._("ASSIGNMENT")
        }
    };

    constructor(props) {
        super(props);

        const {navigation} = this.props;
        const data = navigation.getParam('data', {});

        this.state = {
            draftCount: null,
            data: data
        }
    }

    componentWillMount() {
        const {navigation} = this.props;
        const data = navigation.getParam('data', {});
        Storage.getDraftCount(data.id)
            .then((resp) => {
                this.setState({
                    draftCount: resp
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    componentWillReceiveProps(nextProps) {
        const {navigation} = this.props;
        const data = navigation.getParam('data', {});
        Storage.getDraftCount(data.id)
            .then((resp) => {
                this.setState({
                    draftCount: resp
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    _onAction = (data) => {
        const _data = this.props.navigation.getParam('data', {});

        if (data.view == "CREATE") {
            let assignment;
            if (global.USER.role == "USER") assignment = this.props.data;
            this.props.navigation.navigate("FORM", {
                form: _data,
                data: {data: {}, location_id: null, data_date: null, created_by: global.USER.id, status: "DRAFT"},
                assignment: _data
            })
        }

        if (data.view == "BROWSE") {
            this.props.navigation.navigate("BROWSER", {
                config: _data
            })
        }

        if (data.view == "DRAFTS") {
            this.props.navigation.navigate("DRAFTS", {
                data: _data
            })
        }

        if (data.view == "QUEUED") {
            this.props.navigation.navigate("QUEUE", {
                data: _data
            })
        }

        if (data.view === "SENT") {
            this.props.navigation.navigate("SENT", {
                data: _data
            })
        }

        if (data.view === "GUIDANCE") {
            this.props.navigation.navigate("GUIDANCE", {
                data: _data
            })
        }
    };

    render() {
        const {navigation} = this.props;
        const data = navigation.getParam('data', {});

        let draftsLabel = global._("DRAFTS");
        if (this.state.draftCount !== null) {
            draftsLabel += " (" + this.state.draftCount + ")";
        }
        return (
            <ScrollView style={styles.SCROLLVIEW_NOPAD}>
                <View style={styles.PAGE}>
                    <View style={styles.HEADER_WRAPPER}>
                        <Text style={styles.HEADER_TEXT}>{Utils.i18n(data.name)}</Text>
                    </View>


                    <Header label={global._("ACTIONS")}/>
                    <ListItem
                        key="NEW"
                        icon="plus"
                        onAction={this._onAction}
                        data={{form_name: global._("CREATE_NEW"), view: "CREATE"}}/>
                    <ListItem
                        key="DRAFTS"
                        icon="file"
                        onAction={this._onAction}
                        data={{form_name: draftsLabel, view: "DRAFTS"}}/>
                    {global.USER.role != "USER" ?
                        <ListItem
                            key="BROWSE"
                            icon="database"
                            onAction={this._onAction}
                            data={{form_name: global._("BROWSE"), view: "BROWSE"}}/>
                        : null}

                    <Header label="Data"/>
                    {global.USER.role == "USER" ?
                        <ListItem
                            key="QUEUED"
                            icon="cube"
                            onAction={this._onAction}
                            data={{form_name: global._("QUEUED"), view: "QUEUED"}}/>
                        : null}
                    <ListItem
                        key="SENT"
                        icon="share"
                        onAction={this._onAction}
                        data={{form_name: global._("SENT"), view: "SENT"}}/>

                    {/*<Header label="Additional Information"/>*/}
                    {/*<ListItem*/}
                    {/*key="GUIDANCE"*/}
                    {/*data={{form_name: "Guidance"}}/>*/}

                </View>

            </ScrollView>
        )
    }
}

export default Assignment;