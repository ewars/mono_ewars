import React, {Component} from 'react';
import {
    View,
    Text,
    ListView,
    ScrollView,
    TouchableHighlight,
    AsyncStorage,
    Image,
    Modal,
    Button, ToastAndroid, Alert
} from "react-native";
import Icon from "react-native-fontawesome-pro";
import codePush, {InstallMode} from "react-native-code-push";

import styles from "../STYLES";

import Utils from "../utils";
import API from "../api/api";
import Storage from "../lib/storage";

import BarcodeScanner from '../controls/c.barcode';

import Sent from "../Sent";
import DialogAndroid from "react-native-dialogs";
import Sync from "../lib/sync";

class AppInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            label: "Base"
        }

        codePush.getUpdateMetadata()
            .then((metadata) => {
                console.log(metadata)
                // this.setState({label: metadata.label, version: metadata.appVersion, description: metadata.description});
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {
        return (
            <View style={styles.FOOT}>
                <Text style={styles.FOOT_TEXT}>EWARS v33({this.state.label}) EAPI v5.0.0</Text>
            </View>
        )
    }
}

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.HEADER_WRAPPER}>
                {this.props.icon ?
                    <View>
                        <Icon name={this.props.icon} size={12} type="light"
                              containerStyle={{marginRight: 3, paddingTop: 2}}/>
                    </View>
                    : null}
                <View>

                    <Text style={styles.HEADER_TEXT}>{this.props.label}</Text>
                </View>
            </View>
        )
    }
}

class ListItem extends Component {
    constructor(props) {
        super(props)
    }

    _onPress = () => {
        this.props.onAction(this.props.data);
    };

    render() {
        let label = Utils.i18n(this.props.data.name);
        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={styles.LIST_ITEM}>
                    {this.props.data.icon ?
                        <View style={styles.LIST_ITEM_RIGHT}>
                            <Icon name={this.props.data.icon} type="light" size={15}
                                  containerStyle={{marginTop: 1, marginLeft: 8, marginRight: 8}}/>
                        </View>
                        : null}
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text style={styles.LIST_ITEM_TEXT}>{label}</Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        <Icon name="chevron-right" type="light" size={15}/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class FormList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            forms: [],
            _isLoaded: false
        }

        if (global.FORMS) {
            this.state.forms = global.FORMS;
            this.state._isLoaded = true;
        }

        Utils.listen("CONNECTION_CHANGE", this._reload);
    }

    _reload = (state) => {
        if (state) {
            API.getForms()
                .then((resp) => {
                    global.FORMS = resp.data;
                    this.setState({
                        forms: resp.data,
                        _isLoaded: true
                    })
                })
                .catch((err) => {
                    console.log(err);
                })
        }
    }

    componentWillMount() {
        this._reload(global.IS_CONNECTED);
    }

    componentWillUnmount() {
        Utils.unListen("CONNECTION_CHANGE", this._reload);
    }

    _onAction = (data) => {
        this.props.onAction(data);
    };

    render() {
        if (!global.IS_CONNECTED) {
            return (
                <View>
                    <Text style={styles.PLACEHOLDER}>{global._("CONNECT_FORMS")}</Text>
                </View>
            )
        }
        if (!this.state._isLoaded) {
            return (
                <View>
                    <Text style={styles.PLACEHOLDER}>{global._("LOADING_FORMS")}</Text>
                </View>
            )
        }
        return (
            <View>
                {this.state.forms.map((form) => {
                    return (
                        <ListItem
                            name={form.name}
                            key={"FORM" + String(form.id)}
                            onAction={this._onAction}
                            data={form}/>
                    )
                })}
            </View>
        )
    }
}

class AssignmentItem extends Component {
    _onPress = () => {
        let data = {
            name: this.props.data.form_name,
            definition: this.props.data.definition,
            id: this.props.data.form_id,
            features: this.props.data.features,
            locations: this.props.data.locations
        }
        this.props.onAction(data);
    };

    render() {
        let label = Utils.i18n(this.props.data.form_name);
        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={styles.LIST_ITEM}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text style={styles.LIST_ITEM_TEXT}>{label}</Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        <Icon name="chevron-right" type="light"/>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class MissingGeoms extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableHighlight onPress={this.props.onShowMissingGeoms}>
                <View style={styles.LIST_ITEM_GEOM}>
                    <View>
                        <Text style={styles.LIST_ITEM_TEXT_GEOM}>{global._("MISSING_GPS")}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class AssignmentsList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            assignments: [],
            _isLoaded: false
        }

    }

    componentWillUnmount() {
        Utils.unListen("RELOAD_ASSIGNMENTS", this._reload);
    }

    _reload = () => {
        Storage.getAssignments()
            .then((data) => {
                this.setState({
                    assignments: data,
                    _isLoaded: true
                })
            })
            .catch((err) => {
                console.log(err);
            })
    };

    componentWillMount() {
        Storage.getAssignments()
            .then((data) => {
                this.setState({
                    assignments: data,
                    _isLoaded: true
                }, () => {
                    Utils.listen("RELOAD_ASSIGNMENTS", this._reload);
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

    _onAction = (data) => {
        this.props.onAction(data);
    };

    _showGeomManager = () => {
        this.props.onShowGeomManager();
    };

    render() {
        if (!this.state._isLoaded) {
            return (
                <View>
                    <Text>Loading forms...</Text>
                </View>
            )
        }

        let hasMissingGeoms = false,
            missingGeoms = {};

        (this.state.assignments || []).forEach(item => {
            item.locations.forEach(loc => {
                if (loc.no_geom) {
                    if (loc.location_id != null) {
                        hasMissingGeoms = true;
                        missingGeoms[loc.location_id] = loc;
                    }
                }
            })
        });

        let showRequestAssign = false;
        if (global.USER.role === "USER" && global.IS_CONNECTED) showRequestAssign = true;

        return (
            <View>

                {this.state.assignments.map((item) => {
                    return <AssignmentItem
                        key={"FORM_" + item.form_id}
                        name={item.form_name}
                        onAction={this._onAction}
                        data={item}/>
                })}


                {hasMissingGeoms ?
                    <MissingGeoms
                        onShowMissingGeoms={this._showGeomManager}
                        data={missingGeoms}/>
                    : null}
            </View>
        )
    }
}


class AlertWrapper extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: true
        }
    }

    componentWillMount() {
        Utils.dump();
        Utils.listen("CONNECTION_CHANGE", this._connChange);
    }

    componentWillUnmount() {
        Utils.unListen("CONNECTION_CHANGE", this._connChange);
    }

    _connChange = (data) => {
        this.setState({
            show: data
        })
    }

    _onView = (data) => {
        this.props.onView(data);
    };

    render() {
        if (!this.state.show) {
            return null;
        }
        return (
            <View>
                <Header label={global._("ALERT_LOG")} icon="bell"/>
                <ListItem onAction={this._onView} key="ALERTS"
                          data={{icon: "exclamation", name: this.props.alertLabel, view: "ALERTS"}}/>
                <ListItem onAction={this._onView} key="ALERTS_MONITORED"
                          data={{icon: "eye", name: this.props.alertsMonitorLabel, view: "ALERTS_MONITORED"}}/>
            </View>
        )
    }
}

class Main extends Component {
    static navigationOptions = ({navigation}) => {
        const params = navigation.state.params || {};

        return {
            headerTitle: "EWARS",
            headerRight: (
                <TouchableHighlight
                    style={styles.NAV_BTN} onPress={() => {
                    params.sync();
                }}>
                    <Icon
                        containerStyle={{
                            marginTop: 15,
                            marginRight: 8
                        }}
                        name="exchange" size={20} type="light"/>
                    {/*<Image style={styles.NAV_IMAGE} source={require("../../img/ic_ewars_sync.png")}/>*/}
                </TouchableHighlight>
            ),
            headerLeft: (
                <TouchableHighlight style={styles.NAV_BTN_RIGHT} onPress={() => {
                    navigation.navigate("SETTINGS");
                }}>
                    <Icon
                        containerStyle={{
                            marginTop: 15,
                            marginLeft: 8
                        }}
                        name="cog" size={20} type="light"/>
                </TouchableHighlight>
            )
        };
    };

    _isMounted = false;

    constructor(props) {
        super(props);

        this._isMounted = false;

        this.state = {
            assignments: [],
            metrics: {},
            localMetrics: {},
            showSyncModal: false
        };

        this._sub = this.props.navigation.addListener(
            'didFocus',
            this._rerender
        );
    }

    _rerender = () => {
        this.setState({
            ...this.state
        }, () => {
            Utils.emit("RELOAD_ASSIGNMENTS");
            if (global.IS_CONNECTED) {
                this._getMetrics();
            }
        })
    };

    _getMetrics = () => {
        let locData,
            remData;
        Storage.getLocalMetrics()
            .then((data) => {
                locData = data;
                if (global.IS_CONNECTED) {
                    return API.getMetrics()
                } else {
                    this.setState({
                        metrics: {},
                        localMetrics: locData
                    })
                }
            })
            .catch(err => {
                locData = {};
            })
            .then(data => {
                remData = data.data;
                this.setState({
                    metrics: data.data,
                    localMetrics: locData
                })
            })
            .catch(err => {
                remData = {};
            })
    };

    componentWillMount() {
        this.props.navigation.setParams({
            sync: this._sync
        });

        this._getMetrics();
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._sub.remove();
        this._isMounted = false;
    }

    _sync = () => {
        if (!global.IS_CONNECTED) {
            ToastAndroid.show(global._("NO_CONNECTION"), ToastAndroid.SHORT);
            return;
        }
        let self = this;
        this.setState({
            showSyncModal: true
        }, () => {

            let syncer = new Sync();

            syncer.prep()
                .then((res) => {
                    if (res) {
                        if (res.length > 0) {
                            // Show that errors have occurred
                            Alert.alert(
                                global._("PROBLEM_RECORDS"),
                                global._("SYNC_ERROR_RECORDS"),
                                [
                                    {
                                        text: global._("OK"),
                                        onPress: () => {
                                            this.setState({
                                                showSyncModal: false
                                            })
                                        }
                                    }
                                ]
                            )
                        } else {
                            this.setState({
                                showSyncModal: false
                            });

                            ToastAndroid.show(global._("SYNC_COMPLETE"), ToastAndroid.SHORT);
                        }
                    } else {
                        this.setState({
                            showSyncModal: false
                        });
                        ToastAndroid.show(global._("SYNC_COMPLETE"), ToastAndroid.SHORT);
                    }

                    Utils.emit("RELOAD_ASSIGNMENTS");
                })
                .catch((err) => {
                    this.setState({
                        showSyncModal: false
                    })
                    Utils.emit("RELOAD_ASSIGNMENTS");
                })
        })
    };

    _onForm = (data) => {
        this.props.navigation.navigate("ASSIGNMENT", {
            data: data
        })
    };

    _onAssign = (data) => {
        this.props.navigation.navigate("ASSIGNMENT", {
            data: data
        })
    };

    _onView = (data) => {
        if (data.view === "SIGN_OUT") {
            Alert.alert(
                global._("SIGN_OUT"),
                global._("SIGN_OUT_MESSAGE"),
                [
                    {
                        text: global._("CANCEL"),
                        onPress: () => {
                        }
                    },
                    {
                        text: global._("SIGN_OUT"),
                        onPress: () => {
                            Utils.emit("LOGOUT");
                        }
                    }
                ]
            );
            return;
        }
        switch (data.view) {
            case 'ABOUT':
                this.props.navigation.navigate("ABOUT");
                break;
            case "PROFILE":
                this.props.navigation.navigate("PROFILE");
                break;
            case 'SENT':
                this.props.navigation.navigate("SENT");
                break;
            case 'QUEUED':
                this.props.navigation.navigate("QUEUE");
                break;
            case 'DRAFTS':
                this.props.navigation.navigate("DRAFTS");
                break;
            case 'PROBLEMS':
                this.props.navigation.navigate("PROBLEMS");
                break;
            case 'ALERTS':
                this.props.navigation.navigate("ALERTS");
                break;
            case "ALERTS_MONITORED":
                this.props.navigation.navigate("ALERTS_MONITORED", {open: false});
                break;
            default:
                break;
        }
    };

    _showGeoms = () => {
        this.props.navigation.navigate("GEOMS");
    };

    render() {
        let alertLabel = global._("OPEN_ALERTS");
        let alertMonitorLabel = global._("MONITORED_ALERTS");
        if (this.state.metrics) {
            alertLabel += " (" + this.state.metrics.OPEN_ALERTS + ")";
            alertMonitorLabel += " (" + this.state.metrics.MONITORED_ALERTS + ")";
        }

        let view;
        if (global.USER.role === "USER") {
            view = <AssignmentsList onAction={this._onForm} onShowGeomManager={this._showGeoms}/>;
        } else {
            view = <FormList onAction={this._onAssign}/>;
        }

        let queueLabel = global._("QUEUE");
        if (this.state.localMetrics.QUEUE) queueLabel += " (" + this.state.localMetrics.QUEUE + ")";
        let draftsLabel = global._("DRAFTS");
        if (this.state.localMetrics.DRAFTS) draftsLabel += " (" + this.state.localMetrics.DRAFTS + ")";
        let sentLabel = global._("SENT");
        if (this.state.localMetrics.SENT) sentLabel += " (" + this.state.localMetrics.SENT + ")";
        let problemsLabel = global._("PROBLEM_RECORDS");
        if (this.state.localMetrics.PROBLEMS) problemsLabel += " (" + this.state.localMetrics.PROBLEMS + ")";

        let showRequestAssign = false;
        if (global.USER.role === "USER" && global.IS_CONNECTED) showRequestAssign = true;

        return (
            <View style={styles.MAIN}>
                <Modal
                    onRequestClose={() => {
                        console.log("CLOSE");
                    }}
                    transparent={false}
                    visible={this.state.showSyncModal}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <View style={{width: 100, height: 80, justifyContent: "center", alignItems: "center"}}>
                            <Icon name="sync" size={40} type="light"/>
                        </View>
                        <View style={{width: 200, height: 50}}>
                            <Text
                                style={{textAlign: "center"}}>{global._("SYNCING_WAIT")}</Text>
                        </View>

                    </View>
                </Modal>
                <ScrollView>

                    <Header label={global._("FORMS")} icon="clipboard"/>
                    {view}

                    {/*<Header label=""/>*/}
                    {/*<ListItem*/}
                    {/*data={{name: "Request assignment"}}*/}
                    {/*key="REQUEST"/>*/}


                    <Header label={global._("RECORD_MANAGEMENT")} icon="database"/>
                    <ListItem key="DRAFTS" data={{icon: "file", name: draftsLabel, view: "DRAFTS"}}
                              onAction={this._onView}/>
                    <ListItem key="PROBLEMS"
                              data={{name: problemsLabel, view: "PROBLEMS", icon: "exclamation-triangle"}}
                              onAction={this._onView}/>
                    {global.USER.role == "USER" ?
                        <ListItem key="QUEUED" data={{name: queueLabel, view: "QUEUED", icon: "cube"}}
                                  onAction={this._onView}/>
                        : null}
                    <ListItem key="SENT" data={{name: sentLabel, view: "SENT", icon: "share"}} onAction={this._onView}/>

                    <AlertWrapper
                        alertLabel={alertLabel}
                        alertsMonitorLabel={alertMonitorLabel}
                        onView={this._onView}/>


                    {/*<Header label="TASKS"/>*/}
                    {/*<ListItem onAction={this._onView} key="TASKS" data={{form_name: "Tasks", view: "TASKS"}}/>*/}

                    {/*<Header label="NOTIFICATIONS"/>*/}
                    {/*<ListItem onAction={this._onView} key="NOTIFICATIONS"*/}
                    {/*data={{form_name: "Notifications", view: "NOTIFICATIONS"}}/>*/}

                    <Header label={global._("OTHER")}/>
                    <ListItem onAction={this._onView} key="ABOUT"
                              data={{name: global._("ABOUT"), view: "ABOUT", icon: "question-circle"}}/>
                    <ListItem
                        onAction={this._onView}
                        key="PROFILE"
                        data={{icon: "user", name: global._("PROFILE"), view: "PROFILE"}}/>

                    <Header label=""/>
                    <ListItem onAction={this._onView} key="LOGOUT"
                              data={{icon: "sign-out", name: global._("SIGN_OUT"), view: "SIGN_OUT"}}/>


                    <AppInfo/>
                </ScrollView>

            </View>
        )
    }
}


export default Main;