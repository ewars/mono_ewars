import React, {Component} from "react";
import {
    View,
    ListView,
    ScrollView,
    Text,
    TouchableHighlight,
    StyleSheet,
    AsyncStorage,
    Modal
} from "react-native";

import Utils from "../utils";
import Form from "../Form";

const DATA = [
    {name: "TExt", location: "Location name1"},
    {name: "TExt", location: "Location name2"},
    {name: "TExt", location: "Location name3"},
    {name: "TExt", location: "Location name4"},
    {name: "TExt", location: "Location name5"},
    {name: "TExt", location: "Location name6"},
    {name: "TExt", location: "Location name7"},
    {name: "TExt", location: "Location name8"},
    {name: "TExt", location: "Location name9"},
    {name: "TExt", location: "Location name0"},
    {name: "TExt", location: "Location name11"},
    {name: "TExt", location: "Location name22"},
    {name: "TExt", location: "Location name33"},
    {name: "TExt", location: "Location name44"},
    {name: "TExt", location: "Location name55"},
    {name: "TExt", location: "Location name66"},
    {name: "TExt", location: "Location name77"}
];

const STYLES = StyleSheet.create({
    row: {
        flexDirection: "row",
        justifyContent: "center",
        backgroundColor: "#FFFFFF",
        borderBottomWidth: 1,
        borderBottomColor: "#F2F2F2"
    },
    cell: {
        flexDirection: "column",
        padding: 8,
        flex: 4
    },
    cell_int: {
        backgroundColor: "#a9a9a9",
        borderRightWidth: 1,
        borderColor: "#F2F2F2",
        width: 150,
        flex: 1,
        padding: 8,
        alignItems: "center"
    },
    text: {
        flex: 1,
        fontWeight: 'bold'
    },
    text_under: {
        fontSize: 12
    }
});

const INTERVAL = {
    NONE: "Daily",
    DAY: "Daily",
    WEEK: "Weekly",
    MONTH: "Monthly",
    YEAR: "Yearly"
}

class Item extends Component {
    constructor(props) {
        super(props);
    }

    _onPress = () => {
        this.props.onSelect(this.props.data);
    };

    render() {



        let locations = this.props.data.assignments.map(item => {
            return Utils.i18n(item.location_name);
        });

        locString = locations.join(", ");

        return (
            <TouchableHighlight onPress={this._onPress}>
                <View style={STYLES.row}>

                    <View style={STYLES.cell}>
                        <Text style={STYLES.text}>{Utils.i18n(this.props.data.form_name)}</Text>
                        <Text style={STYLES.text_under}>
                            {locString}
                        </Text>
                    </View>
                </View>
            </TouchableHighlight>

        )
    }
}


class Assignments extends Component {
    constructor(props) {
        super(props)

        this.state = {
            view: "LIST",
            assignments: [],
            assignment: null,
            modal: false
        }
    }

    componentWillMount() {
        try {
            AsyncStorage.getItem("@SETTINGS:assignments")
                .then((resp) => {
                    this.setState({
                        assignments: JSON.parse(resp || "[]")
                    })
                })
        } catch (err) {
            console.log(err)
        }
    }


    _onClickRow = (data) => {
        this.props.navigator.push({
            data: data,
            index: 1
        })
    };

    _cancel = () => {
        this.setState({
            assignment: null,
            modal: false
        })
    };

    render() {
        return (
            <View>
                <ScrollView>
                    {this.state.assignments.map(item => {
                        return <Item
                            onSelect={this._onClickRow}
                            data={item}
                            key={item.form_id}/>
                    })}
                </ScrollView>
                <Modal
                    onRequestClose={() => {}}
                    visible={this.state.modal}>
                    {this.state.assignment ?
                        <Form
                            cancel={this._cancel}
                            assignment={this.state.assignment}/>
                        : null}
                </Modal>
            </View>
        )

    }
}

export default Assignments;
