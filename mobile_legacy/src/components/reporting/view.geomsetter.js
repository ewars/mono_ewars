import React, {Component} from 'react';
import {
    View,
    Button,
    Text,
    ScrollView,
    TextInput,
    StyleSheet,
    ToastAndroid,
    PermissionsAndroid
} from "react-native";

import API from '../api/api';

import Storage from '../lib/storage';

const styles = StyleSheet.create({
    BUTTON: {
        marginBottom: 8,
    },
    PADDER: {
        paddingLeft: 8,
        paddingRight: 8,
        marginTop: 8,
        marginBottom: 8
    },
    HEADER: {
        fontWeight: "bold",
        marginBottom: 8
    },
    FIELD: {
        backgroundColor: "white",
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 8,
        paddingBottom: 8
    },
    BUTTON_RED: {
        marginBottom: 8,
        backgroundColor: "rgb(190, 97, 107)"
    },
    BUTTON_ORANGE: {
        marginBottom: 8,
        backgroundColor: "rgb(207, 135, 114)"
    },
    BUTTON_GREEN: {
        marginBottom: 8,
        backgroundColor: "rgb(164, 189, 142)"
    }
})

export default class GeomSetter extends Component {
    static navigationOptions = {
        title: global._("LOCATION_EDITOR")
    };

    constructor(props) {
        super(props);

        const config = this.props.navigation.getParam("data", {});
        this.state = {
            latitude: config.current.latitude || "0",
            longitude: config.current.longitude || "0"
        }
    }

    _cancel = () => {
        this.props.navigation.goBack();
    };

    _submit = () => {
        const config = this.props.navigation.getParam("data", {});

        return Storage.getStoredLocationUpdates()
            .then(data => {
                if (!data) data = {};
                if (data.length != undefined) data = {};
                data[config.location.location_id] = {
                    latitude: this.state.latitude,
                    longitude: this.state.longitude
                }
                return Storage.setStoredLocationUpdates(data);
            })
            .catch(err => {
                console.log(err)
            })
            .then(res => {
                this.props.navigation.goBack();
            })
            .catch(err => {
                console.log(err);
            })
    };

    async _getLocation() {
        try {
            const granted = await PermissionsAndroid.requestMultiple(
                [
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
                ],
                {
                    title: global._("PERMISSION_GPS_TITLE"),
                    message: global._("PERMISSION_GPS_MESSAGE")
                },
                {
                    timeout: 1000,
                    enableHighAccuracy: true
                }
            )

            if (granted['android.permission.ACCESS_COARSE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED
                && granted['android.permission.ACCESS_FINE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED) {
                return true;
            } else {
                ToastAndroid.show(global._("ERR_GPS_PERMISSIONS"), ToastAndroid.SHORT);
                return false;
            }


        } catch (err) {
            ToastAndroid.show(global._("ERR_GPS_PERMISSIONS"), ToastAndroid.SHORT);
            return false;
        }
    };

    _getLocale = (self) => {
        ToastAndroid.show(global._("GETTING_LOCATION"), ToastAndroid.LONG);
        navigator.geolocation.getCurrentPosition(
            (res) => {
                self.setState({
                    latitude: String(res.coords.latitude),
                    longitude: String(res.coords.longitude)
                })
            },
            (res) => {
                ToastAndroid.show(global._("ERR_LOCATION_RETRIEVAL"), ToastAndroid.SHORT);
            },
            {
                enableHighAccuracy: true
            }
        )
    };

    render() {
        const config = this.props.navigation.getParam("data", {});

        return (
            <ScrollView>
                <View style={styles.PADDER}>
                    <Text
                        style={styles.HEADER}>{config.location.location_name.en || config.location.location_name}</Text>

                    <View style={styles.FIELD}>
                        <Text>{global._("LATITUDE")}</Text>
                        <TextInput
                            keyboardType={"numeric"}
                            editable={true}
                            onChangeText={(val) => {
                                this.setState({
                                    latitude: val
                                })
                            }}
                            value={this.state.latitude}/>
                    </View>

                    <View style={styles.FIELD}>
                        <Text>{global._("LONGITUDE")}</Text>
                        <TextInput
                            onChangeText={(val) => {
                                this.setState({
                                    longitude: val
                                })
                            }}
                            keyboardType={"numeric"}
                            editable={true}
                            value={this.state.longitude}/>
                    </View>
                    <View style={styles.FIELD}>
                        <Button
                            style={styles.BUTTON_GREEN}
                            title={global._("BUTTON_USE_LOCATION")}
                            onPress={() => {
                                this._getLocation()
                                    .then(data => {
                                        if (data === true) {
                                            this._getLocale(this);
                                        } else {
                                            ToastAndroid.show("Could not retrieve location", ToastAndroid.SHORT);
                                        }
                                    })
                                    .catch(err => {
                                        console.log(err)
                                    })
                            }}/>
                    </View>
                </View>

                <View style={{marginLeft: 9, marginRight: 9, marginBottom: 9}}>
                    <Button
                        color="rgb(164, 189, 142)"
                        style={styles.BUTTON_ORANGE}
                        title={global._("SAVE_CHANGES")} onPress={this._submit}/>
                </View>
                <View style={{marginLeft: 9, marginRight: 9}}>
                    <Button
                        color="rgb(190, 97, 107)"
                        style={styles.BUTTON_RED}
                        title={global._("CANCEL")} onPress={this._cancel}/>
                </View>


            </ScrollView>
        )
    }
}