import React, {Component} from "react";
import {
    View,
    Button,
    ScrollView,
    TouchableHighlight,
    Text,
    Image
} from "react-native";
import Storage from "../lib/storage";
import styles from '../STYLES';
import Icon from 'react-native-fontawesome-pro';


class ItemLocation extends Component {
    constructor(props) {
        super(props);
    }

    _select = () => {
        this.props.onSelect({
            location: this.props.data,
            current: this.props.updated[this.props.data.location_id] || {}
        });
    };

    render() {
        let updated = this.props.updated[this.props.data.location_id] != null;

        let icon = <Icon name="chevron-right" type="light"/>
        let style = {};
        if (updated) {
            style.textDecorationLine = "underline line-through";
            icon = <Icon name="check" color="green" type="light"/>
        }


        return (
            <TouchableHighlight onPress={this._select}>
                <View style={styles.LIST_ITEM}>
                    <View style={styles.LIST_ITEM_LEFT}>
                        <Text style={style}>
                            {this.props.data.location_name.en || this.props.data.location_name}
                        </Text>
                    </View>
                    <View style={styles.LIST_ITEM_RIGHT}>
                        {icon}
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

export default class GeomManager extends Component {
    static navigationOptions = {
        title: global._("LOCATION_MANAGEMENT")
    };

    constructor(props) {
        super(props);

        this.state = {
            locations: []
        }

        let locations = [];
        let updated = [];

        this._sub = this.props.navigation.addListener(
            'didFocus',
            this._load
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    _load = () => {
        Storage.getAssignments()
            .then((data) => {
                let results = {};
                data.forEach(item => {
                    item.locations.forEach(node => {
                        if (node.location_id != null && node.no_geom) {
                            results[node.location_id] = node;
                        }
                    })
                })

                let missing = [];
                for (let i in results) {
                    missing.push(results[i]);
                }
                locations = missing || [];
                return Storage.getStoredLocationUpdates();
            })
            .catch((err) => {
                console.log(err);
            })
            .then(res => {
                updated = res;
                this.setState({
                    locations: locations,
                    updated: updated
                })
            })
            .catch(err => {
                console.log(err);
            })

    }

    _select = (data) => {
        this.props.navigation.navigate("GEOM_SETTER", {
            data: data
        })
    };

    render() {
        if (!this.state.updated) {
            return (
                <View>
                    <View style={{marginTop: 8}}>
                        <Text style={{textAlign: "center"}}>{global._("LOADING")}</Text>
                    </View>
                </View>
            )
        }
        let items = [];

        console.log(this.state.updated);
        this.state.locations.forEach(item => {
            items.push(
                <ItemLocation
                    key={item.location_id}
                    data={item}
                    updated={this.state.updated}
                    onSelect={this._select}/>
            );
        });


        return (
            <View>
                <ScrollView>
                    {items}
                </ScrollView>
            </View>
        )
    }
}

