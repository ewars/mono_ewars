#ifndef RECORDSELECTDELEGATE_H
#define RECORDSELECTDELEGATE_H

#include <QObject>
#include <QStyledItemDelegate>

class RecordSelectDelegate : public QStyledItemDelegate {
    Q_OBJECT

public:
    RecordSelectDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;


};

#endif // RECORDSELECTDELEGATE_H
