#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->mainToolbar->addAction(tr("New Record"));

    QIcon btnIcon(":/icos/icons/clipboard.svg");
    QPushButton *btn = new QPushButton();
    btn->setIcon(btnIcon);
    ui->mainToolbar->addWidget(btn);

    ui->tabWidget->setElideMode(Qt::ElideRight);
    ui->tabWidget->addTab(new MainOverview, "mainoverview");
    QIcon icon(":/icos/icons/tachometer-alt.svg");
    ui->tabWidget->setTabIcon(0, icon);

    ui->tabWidget->addTab(new ReportingGridView, "reporting_grid");
    QIcon reportingIcon(":/icos/icons/clipboard.svg");
    ui->tabWidget->setTabIcon(0, icon);

    //ui->tabWidget->addTab(new Ui::partspro,"test");
}

MainWindow::~MainWindow()
{
    delete ui;
}
