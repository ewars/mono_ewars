#ifndef REPORTINGGRIDVIEW_H
#define REPORTINGGRIDVIEW_H

#include <QWidget>
#include <QToolBar>
#include <QTableView>
#include <QTableWidget>
#include <QStringList>
#include <QPushButton>
#include <QSqlTableModel>
#include <QMessageBox>

namespace Ui {
class ReportingGridView;
}

class ReportingGridView : public QWidget
{
    Q_OBJECT

public:
    explicit ReportingGridView(QWidget *parent = nullptr);
    ~ReportingGridView();

private:
    Ui::ReportingGridView *ui;
};

#endif // REPORTINGGRIDVIEW_H
