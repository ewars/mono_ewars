#include "mainoverview.h"
#include "ui_mainoverview.h"

MainOverview::MainOverview(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainOverview)
{
    ui->setupUi(this);


    // Home list widget
    QIcon reportingIcon(":/icos/icons/clipboard.svg");
    new QListWidgetItem(reportingIcon, tr("Reporting"), ui->homeListWidget);

    QIcon outbreaksHomeIcon(":/icos/icons/ambulance.svg");
    new QListWidgetItem(outbreaksHomeIcon, tr("Outbreaks"), ui->homeListWidget);

    QIcon alertsIcon(":/icos/icons/bell.svg");
    new QListWidgetItem(alertsIcon, tr("Alerts"), ui->homeListWidget);

    QIcon documentsIcon(":/icos/icons/book.svg");
    new QListWidgetItem(documentsIcon, tr("Documents"), ui->homeListWidget);


    // Admin menu items
    QIcon configIcon(":/icos/icons/cog.svg");
    new QListWidgetItem(configIcon, tr("Config"), ui->adminListWidget);

    QIcon formsIcon(":/icos/icons/clipboard.svg");
    new QListWidgetItem(formsIcon, tr("Forms"), ui->adminListWidget);

    QIcon locationsIcon(":/icos/icon/map.svg");
    new QListWidgetItem(locationsIcon, tr("Locations"), ui->adminListWidget);

    QIcon usersIcon(":/icos/icons/user.svg");
    new QListWidgetItem(usersIcon, tr("Users"), ui->adminListWidget);

    QIcon devicesIcon(":/icos/icons/mobile.svg");
    new QListWidgetItem(devicesIcon, tr("Devices"), ui->adminListWidget);

    QIcon templatesIcon(":/icos/icons/book.svg");
    new QListWidgetItem(templatesIcon, tr("Document Templates"), ui->adminListWidget);

    QIcon importIcon(":/icos/icons/upload.svg");
    new QListWidgetItem(importIcon, tr("Data Import"), ui->adminListWidget);

    QIcon locationTypesIcon(":/icos/icons/map-marker.svg");
    new QListWidgetItem(locationTypesIcon, tr("Location Types"), ui->adminListWidget);

    QIcon organizationsIcon(":/icos/icons/hospital.svg");
    new QListWidgetItem(organizationsIcon, tr("Organizations"), ui->adminListWidget);

    QIcon alarmsIcon(":/icos/icons/bell.svg");
    new QListWidgetItem(alarmsIcon, tr("Alarms"), ui->adminListWidget);

    QIcon integrationsIcon(":/icos/icons/cube.svg");
    new QListWidgetItem(integrationsIcon, tr("Integrations"), ui->adminListWidget);

    QIcon indicatorsIcon(":/icos/icons/code-merge.svg");
    new QListWidgetItem(indicatorsIcon, tr("Indicators"), ui->adminListWidget);

    QIcon dashboardsIcon(":/icos/icons/tachometer-alt.svg");
    new QListWidgetItem(dashboardsIcon, tr("Dashboards"), ui->adminListWidget);

    QIcon rolesIcon(":/icos/icons/users.svg");
    new QListWidgetItem(rolesIcon, tr("Roles"), ui->adminListWidget);

    QIcon teamsIcon(":/icos/icons/users.svg");
    new QListWidgetItem(teamsIcon, tr("Teams"), ui->adminListWidget);

    QIcon outbreaksIcon(":/icos/icons/ambulance.svg");
    new QListWidgetItem(outbreaksIcon, tr("Outbreaks"), ui->adminListWidget);

    QIcon sitesIcon(":/icos/icons/browser.svg");
    new QListWidgetItem(sitesIcon, tr("Sites"), ui->adminListWidget);

    QIcon translationsIcon(":/icos/icons/translation.svg");
    new QListWidgetItem(translationsIcon, tr("Translations"), ui->adminListWidget);

    QIcon apiKeysIcon(":/icos/icons/code.svg");
    new QListWidgetItem(apiKeysIcon, tr("API Keys"), ui->adminListWidget);

    QIcon hubIcon(":/icos/icons/cube.svg");
    new QListWidgetItem(hubIcon, tr("Hub"), ui->adminListWidget);


}

MainOverview::~MainOverview()
{
    delete ui;
}
