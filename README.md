# Early Warning and Response System

EWARS is a suite of applications, components and tools used by the WHO, Ministries of Health and partner organizations to monitor, manage and respond to catastrophic events such as health crises, natural disasters and conflicts. The components which make up the system are primarily used for the monitoring of health and diseases in populations where these topics are a concern, though in some cases it has been used for contexts above and beyond these such as nutrition and facility evaluation.

The system is predominantly used by field-level staff to submit and manage records pertaining to aggregated cases and deaths for verifiable and non-verifiable diseases. In addition to this, it is used for case/event-based surveillance of highly volatile diseases which have a high rate of spread to “head off” and control outbreaks. The generality of the systems design means that it can potentially be used for a variety of contexts and scenarios beyond the promoted health context.

The application has been in operation since 2014 after an initial pilot was deployed to South Sudan by JDU Sofware & Consulting, not long after this point, the project was transitioned to a direct consultancy between Jeff Uren (CD JDU S&C) and the WHO. Jeff Uren has been responsible for the design, development, infrastructure management, support, training and day to day management of the system since this period.

For information about contributing to the project, please see the CONTRIBUTING and CODE_OF_CONDUCT documents in the repository root.
