use serde_json as json;

use actix::prelude::*;

use crate::db::db::DbExecutor;

static SQL_CACHE_LOCATIONS: &'static str = "./import_queue.toml";

pub struct ImportJob {
    pub form_id: Uuid,
    pub data: String,
    pub options: Value,
}

impl actix::Message for ImportJob {
    type: Result = Result<(), ()>;
}

pub struct ImportService {
    queue: VecDeque<ImportJob>,
    hb: Instant,
    processing: bool,
}

impl ImportJob {
    pub fn new() -> Self {
        Self {
            queue: VecDeque::new(),
            hb: Instant::now(),
            processing: false,
        }
    }

    pub fn process_job(&self, job: &ImportJob) -> Result<(), ()> {
        Ok(())
    }

    fn hb(&mut self, ctx: &mut Context<Self>) {
        println!("Heartbeat Run");
        ctx.run_later(Duration::new(10, 0), |act, ctx| {
            if Instant::now().duration_since(act.hb) > Duration::new(20, 0) {
                if !act.processing {
                    act.processing = true;
                    eprintln!("Heartbeat called");
                    let mut total = act.queue.len();
                    if total > 10 {
                        total = 10
                    }
                    let drained = act.queue.drain(0..total).collect::<VecDeque<_>>();

                    for item in drained.iter() {
                        act.process_job(item);
                    }
                    act.hb = Instant::now();
                    act.processing = false;
                }
            }
            act.hb(ctx);
        });
    }
}

impl Actor for ImportService {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        println!("Import service started");
        self.hb(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        println!("Stopping import service");
        Running::Stop
    }


}

impl Supervised for ImportService {
    fn restarting(&mut self, _: &mut Self::Context) {
        println!("Restarting import service");
    }
}

impl Drop for ImportService {
    fn drop(&mut self) {
        if self.queue.len() > 0 {
            let data: Vec<ImportJob> = self.queue.draing(0..).collect::<Vec>ImportJob>>();
            let json_str: String = json::to_string(&data).unwrap();
            fs::write("./.cache/imports.json", json_str).expect("Could not write to cache file");
    }
}


impl Handler<ImportJob> for ImportService {
    type Result = Result<(), ()>;


    fn handle(&mut self, msg: ImportJob, ctx: &mut Context<Self>) -> Self::Result {
        eprintln!("{:?}", msg);
        self.queue.push_back(msg.clone());
        Ok(())
    }
}
