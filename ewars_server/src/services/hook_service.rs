use std::collections::VecDeque;

use actix::prelude::*;
use serde_json as json;
use serde_json::Value;
use postgres::{Connection, TlsMode};
use uuid::Uuid;

use crate::settings::Settings;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Hook {
    pub res_type: String,
    pub hook_id: Uuid,
    pub res_id: Uuid,
}

impl Message for Hook {
    type Result = Result<(), ()>;
}

pub struct HookService {
    pub db: Connection,
    pub queue: VecDeque<Hook>,
}

impl HookService {
    pub fn new() -> Self {
        let conf = Settings::new().unwrap();
        let conn = Connection::connect(conf.database.db_url, TlsMode::None).unwrap();
        HookService {
            db: conn,
            queue: VecDeque::new(),
        }
    }
}

impl Actor for HookService {
    type Context = SyncContext<Self>;
}

impl Handler<Hook> for HookService {
    type Result = Result<(), ()>;

    fn handle(&mut self, msg: Hook, ctx: &mut Self::Context) -> Self::Result {
        self.queue.push_back(msg.clone());

        Ok(())
    }
}
