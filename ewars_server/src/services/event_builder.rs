/**
 * Event Builder actor/service
 * This service is tasked with generating event log items from updates occurring elsewhere in the
 * system as well as maintaining a cache of the data for querying by remote users
 * This is a critical system and should not be altered without giving some serious thought to the
 * changes that you are making, it operates on a diff scenario where the difference between the
 * original resource and the updated resource is calculated and propagated into the event log.
 * External devices rely on this worker being as complete and correct as possible in order to
 * ensure that data synced between devices is current and corret.
 */


use std::time::{Duration, Instant};
use std::collections::{VecDeque};
use std::fs;

use serde_json as json;
use serde_json::Value;
use actix::prelude::*;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::{Connection, TlsMode};

use crate::handlers::api_handler::{PoolConnection};
use crate::settings::Settings;

use crate::db::db::DbExecutor;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum EventType {
    Create,
    Delete,
    Update,
    Rollback,
    Rebuild,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum ResourceType {
    Record,
    Assignment,
    Alert,
    Alarm,
    Location,
    LocationType,
    Indicator,
    IndicatorGroup,
    Task,
    Dashboard,
    Resource,
    Document,
    User,
    Account,
    Device,
    Interval,
    Role,
    Team,
    TeamMessage,
    Invite,
    Draft,
    Conf,
    Organization,
    ReportingPeriod,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Job {
    pub resource_id: Uuid,
    pub resource_type: ResourceType,
    pub event_type: EventType,
    pub ts_ms: DateTime<Utc>,
    pub completed: bool,
    pub event_id: Option<Uuid>,
    pub dependencies: Option<Vec<Uuid>>, // ITems which must be processed first before this one
}

impl Message for Job {
    type Result = Result<(), ()>;
}

pub struct EventBuilder {
    queue: VecDeque<Job>,
    hb: Instant,
    processing: bool,
    db: Connection,
}

impl EventBuilder {
    pub fn new() -> Self {
        let conf = Settings::new().unwrap();
        let conn = Connection::connect(conf.database.db_url, TlsMode::None).unwrap();
        Self {
            queue: VecDeque::new(),
            hb: Instant::now(),
            processing: false,
            db: conn,
        }
    }

    pub fn hb(&self, ctx: &mut Context<Self>) {
        eprintln!("EventBuilder: Heartbeat");
        ctx.run_later(Duration::new(10, 0), |act, ctx| {
            if Instant::now().duration_since(act.hb) > Duration::new(20, 0) {
                // TODO: Flush the queue to disk so that we have a persistent that we can revover to in
                // the event of a failure
                act.hb = Instant::now();
            }
            act.processing = false;
        });
    }
}

impl Actor for EventBuilder {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        eprintln!("EventBuilder: Service started");
        // TODO: Check for and load up the cache file in case there are jobs that were incomplete
        // from the previous run
        self.hb(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        eprintln!("EventBuilder: Service stopping");
        Running::Stop
    }
}

impl Supervised for EventBuilder {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("EventBuilder: Restarting service");
    }
}

impl Drop for EventBuilder {
    fn drop(&mut self) {
        if self.queue.len() > 0 {
            let data: Vec<Job> = self.queue.drain(0..).collect::<Vec<Job>>();
            let json_str: String = json::to_string(&data).unwrap();
            fs::write("./.cache/jobs.json", json_str).expect("Could not write to cache file");
        }
    }
}

impl Handler<Job> for EventBuilder {
    type Result = Result<(), ()>;

    fn handle(&mut self, job: Job, ctx: &mut Context<Self>) -> Self::Result {
        eprintln!("{:?}", job);
        self.queue.push_back(job.clone());
        Ok(())
    }
}

// Handle a builder job to make sure that the event log is populated properly
pub fn handle_job(conn: &PoolConnection, job: &Job) -> Result<(), ()> {

    // Check the event type that we'll be processing
    match job.event_type {
        EventType::Delete => {

        },
        EventType::Create => {

        },
        EventType::Update => {

        },
        EventType::Rollback => {

        },
        EventType::Rebuild => {

        },
        _ => {}
    }

    Ok(())
}

// Handle a change to a record
fn handle_record_change(conn: &PoolConnection, job: &Job) -> Result<(), ()> {
    unimplemented!()
}
