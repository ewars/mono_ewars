use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

use crate::models::{UserSession};
use crate::handlers::api_handler::{PoolConnection};

static SQL_INSERT_EVENT: &'static str = r#"
    INSERT INTO {SCHEMA}.event_log
    (ref_type, ref_id, ref_ts, command, data, ts_ms, applied, source_version, device_id)
    VALUES ($1, $2, extract(epoch FROM NOW()) * 1000, $3, $4, extract(epoch FROM NOW()) * 1000, TRUE, 0, 'GLOBAL') RETURNING uuid;
"#;

#[derive(Debug, Serialize)]
pub struct EventNotification {
    pub uuid: Uuid,
    pub tki: String,
    pub ref_type: String,
}


// Create an event in the main event log for this account and trigger a NOTIFY to let clients know new data is available
pub fn create_event(conn: PoolConnection, ref_type: String, ref_id: String, command: String, data: Option<Value>, tki: String) -> Result<(), ()> {
    let result: Option<Uuid> = match &conn.query(&SQL_INSERT_EVENT.replace("{SCHEMA}", &tki), &[
        &ref_type,
        &ref_id,
        &command,
        &data,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(row.get("uuid"))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if result.is_None() {
        Err(())
    }

    // Create the postgre notification event
    let out_event = EventNotification {
        uuid: result.unwrap(),
        tki: tki.clone(),
        ref_type: ref_type.clone(),
    };
    let event_output: &'static str = json::to_str(&out_event).unwrap();
    &conn.execute(&format!("NOTIFY sonoma, '{}';", event_output), &[])?;

    Ok(())
}
