use std::collections::HashMap;

use std::io;
use std::io::Error as IoError;

use serde_json::Value;
use chrono::prelude::*;
use chrono::Duration;

lazy_static! {
    static ref MONTH_DAYS: HashMap<u32, u32> = {
        let mut m = HashMap::new();
        m.insert(1, 31); // Jan
        m.insert(2, 28); // Feb; leap years has 29
        m.insert(3, 31); // Mar
        m.insert(4, 30); // Apr
        m.insert(5, 31); // May
        m.insert(6, 30); // Jun
        m.insert(7, 31); // Jul
        m.insert(8, 31); // Aug
        m.insert(9, 30); // Sep
        m.insert(10, 31); // Oct
        m.insert(11, 30); // Nov
        m.insert(12, 31); // Dec
        m
    };
}

// Given a start date, end date and interval, figure out the reporting dates within that range
pub fn get_reporting_intervals(start_date: &NaiveDate, end_date: &NaiveDate, interval: &String) -> Result<Vec<NaiveDate>, IoError> {
    let full_dates: Vec<NaiveDate> = Vec::new();
    let r_end_date = end_date.clone();
    let mut resultant_dates: Vec<NaiveDate> = Vec::new();

    let mut current = start_date.clone();
    let mut done: bool = false;

    if interval == "DAY" {
        // Every day is a reporting period
        let mut done = false;
        let mut current = start_date.clone();

        while !done {
            resultant_dates.push(current.clone());
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }

    }

    if interval == "WEEK" {
        // Figure out which days in the series are ends of ISOWeeksS
        let mut done = false;
        let mut current = start_date.clone();

        while !done {
            if current.weekday() == Weekday::Sun {
                resultant_dates.push(current.clone());
            }
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }
    }

    if interval == "MONTH" {
        // Figure out which days in the series are ends of months
        let mut done = false;
        let mut current = start_date.clone();

        while !done {
            // get the end of the month
            let year = current.year();
            let month = current.month();
            let mut max_days: u32 = MONTH_DAYS.get(&month).unwrap().clone();
            // If this is february we need to check if it's a leap year,
            // as Feb has 29 days in leap years and 28 for all others
            if month == 2 {
                if is_leap_year(&year) {
                    max_days = 29;
                }
            }

            let month_end_utc: Date<Utc> = Utc.ymd(year, month, max_days).and_hms(9, 10, 11).date();
            let month_end: NaiveDate = month_end_utc.naive_utc();
            if current == month_end {
                resultant_dates.push(current.clone());
            }
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }
    }

    if interval == "YEAR" {
        // Figure out which days in the series are ends of the year
        let mut done = false;
        let mut current = start_date.clone();

        while !done {
            let year = current.year();
            let year_end: Date<Utc> = Utc.ymd(year, 12, 31).and_hms(9, 10, 11).date();
            if year_end.naive_utc() == current {
                resultant_dates.push(current.clone());
            }
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }
    }

    Ok(resultant_dates)
}

// Check if the year is a leap year
pub fn is_leap_year(year: &i32) -> bool {
    let result: bool = false;

    let remainder: i32 = year % 4;

    // if not evenly divisible by 4, not a leap year
    if remainder > 0 {
        return false;
    } else {
        // Could be a leap year
        // if it's not evenly divisible by 100, it's a leap year
        let hun_remainder: i32 = year % 100;
        if hun_remainder > 0 {
            return true;
        } else {
            // If it's evenly divislbe by 400 it's a leap year
            let hun4_remainder: i32 = year % 400;
            if hun4_remainder == 0 {
                return true;
            } else {
                return false;
            }
        }
    }
}

// Get the next ISO8601 week end from the current date
pub fn get_next_week_end() -> NaiveDate {
    let mut done = false;
    let mut current: DateTime<Utc> = Utc::now();

    while !done {
        if current.weekday() == Weekday::Sun {
            done = true;
        } else {
            current = current + Duration::days(1);
        }
    }

    current.naive_utc().date()
}

// Get the next day end from the today (i.e. returns today)
pub fn get_next_day_end() -> NaiveDate {
    let today: DateTime<Utc> = Utc::now();

    today.naive_utc().date()
}


// Get the next month end from todays date
pub fn get_next_month_end() -> NaiveDate {
    let today: DateTime<Utc> = Utc::now();
    let year = today.year();
    let month = today.month();
    let max_days: u32 = MONTH_DAYS.get(&month).unwrap().clone();
    let month_end_utc: Date<Utc> = Utc.ymd(year, month, max_days).and_hms(9, 10, 11).date();

    month_end_utc.naive_utc()
}


// Get the next year end
pub fn get_next_year_end() -> NaiveDate {
    let today: DateTime<Utc> = Utc::now();

    let year = today.year();
    
    let year_end: Date<Utc> = Utc.ymd(year, 12, 31).and_hms(9, 10, 11).date();

    year_end.naive_utc()

}
