use md5;
use rand;

use rand::Rng;

// Verify a users password, compare the stored password
// with the provided password + salt
pub fn verify_pass(plaintext: &String, encoded: &String) -> Result<(), ()> {
    let splitted: Vec<&str> = encoded.split(":").collect();

    let pass: String = splitted[0].to_string();
    let salt: String = splitted[1].to_string();
    let digi_string: String = format!("{}{}", plaintext, salt);
    let m = md5::compute(&digi_string);
    let digest: String = format!("{:x}", m);

    if &pass == &digest {
        Ok(())
    } else {
        Err(())
    }
}

// Create the storable password
pub fn generate_password(plaintext: &String) -> Result<String, ()> {
    use rand::Rng;
    let mut coded_pass: String = String::new();
    let mut salt: String = rand::thread_rng().gen_iter::<char>().take(16).collect();

    let combined: String = format!("{}{}", plaintext, salt);

    let m = md5::compute(&combined);
    let digest: String = format!("{:x}", m);

    coded_pass = format!("{}:{}", digest, salt.to_string());

    Ok(coded_pass)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_verify() {
        let enc_pass = "87a588b4ccf07edc6a8e94a1786ba305:VSSMnLkSwModCZFWcKmlSUKHUQFftpIe".to_string();
        let raw_pass = "ewars123".to_string();

        match verify_pass(&raw_pass, &enc_pass) {
            Ok(_) => {
                assert_eq!(true, true);
            },
            Err(_) => {
                assert_eq!(true, false);
            }
        }
    }
}
