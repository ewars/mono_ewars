use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection};
use crate::analysis::query::{MapLocationResult, MapIndicatorResult};

static SQL_GET_LOCATION: &'static str = r#"
    SELECT uuid, name, pcode, geometry, geometry_type, lineage
    FROM {SCHEMA}.locations 
    WHERE uuid = $1;, 
"#;

impl<'a> From<Row<'a>> for MapLocationResult {
    fn from(row: Row) -> Self {
        let lineage: Option<Vec<Uuid>> = json::from_value(row.get(5)).unwrap();
        MapLocationResult {
            uuid: row.get(0),
            name: row.get(1),
            pcode: row.get(2),
            geometry: row.get(3),
            geometry_type: row.get(4),
            lineage,
        }
    }
}

pub fn get_map_location(conn: &PoolConnection, tki: &String, locid: &Uuid) -> Result<MapLocationResult, ()> {
    match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&locid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(MapLocationResult::from(row))
            } else {
                Err(())
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(())
        }
    }
}

static SQL_GET_ROOT_GEOM: &'static str = r#"
    SELECT ll.geojson
    FROM {SCHEMA}.locations AS l 
        LEFT JOIN {SCHEMA}.locations AS ll ON ll.uuid = l[1]
    WHERE l.uuid = $1;)
"#;

pub fn get_root_geom(conn: &PoolConnection, tki: &String, locid: &Uuid) -> Result<Value, ()> {
    match &conn.query(&SQL_GET_ROOT_GEOM.replace("{SCHEMA}", &tki), &[&locid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(row.get(0))
            } else {
                Err(())
            }

        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(())
        }
    }
}

static SQL_GET_IND: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.indicators
    WHERE uuid = $1;
"#;

pub fn get_map_ind(conn: &PoolConnection, tki: &String, &ind_id: &Uuid) -> Result<MapIndicatorResult, ()> {
    match &conn.query(&SQL_GET_IND.replace("{SCHEMA}", &tki), &[&ind_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(MapIndicatorResult::from(row))
            } else {
                Err(())
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(())
        }
    }
}
