use std::collections::HashMap;

use chrono::prelude::*;

use crate::api::locations::{DataResult};
use crate::api::locations::{TimelyDataResult};

pub fn reduce_completeness(data: &Vec<(NaiveDate, DataResult)>, end_date: &NaiveDate) -> Result<HashMap<NaiveDate, f64>, ()> {
    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    let mut expected_records: f64 = 0.0;
    let mut submitted_records: f64 = 0.0;

    for item in data.iter() {
        expected_records += item.1.e as f64;
        submitted_records += item.1.s as f64;
    }

    let mut calc: f64 = 0.0;
    if submitted_records > 0.0 {
        calc = submitted_records / expected_records;
    }

    result.insert(end_date.clone(), calc.clone());

    Ok(result)
}

pub fn reduce_series_completeness(data: &Vec<(NaiveDate, DataResult)>) -> Result<HashMap<NaiveDate, f64>, ()> {
    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    for item in data.iter() {
        let expected: f64 = item.1.e.clone() as f64;
        let submitted: f64 = item.1.s.clone() as f64;

        let mut calc: f64 = 0.0;
        if submitted > 0.0 {
            calc = submitted / expected;
        }

        result.insert(item.0.clone(), calc.clone());

    }

    Ok(result)
}

pub fn reduce_timeliness(data: &Vec<(NaiveDate, TimelyDataResult)>, end_date: &NaiveDate) -> Result<HashMap<NaiveDate, f64>, ()> {
    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    let mut on_time: f64 = 0.0;
    let mut expected: f64 = 0.0;

    for item in data.iter() {
        expected += item.1.e as f64;
        on_time += item.1.o as f64;
    }

    let mut calc: f64 = 0.0;
    if on_time > 0.0 {
        calc = on_time / expected;
    }

    result.insert(end_date.clone(), calc.clone());

    Ok(result)
}

pub fn reduce_series_timeliness(data: &Vec<(NaiveDate, TimelyDataResult)>) -> Result<HashMap<NaiveDate, f64>, ()> {
    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    for item in data.iter() {
        let expected: f64 = item.1.e.clone() as f64;
        let on_time: f64 = item.1.o.clone() as f64;

        let mut calc: f64 = 0.0;
        if on_time > 0.0 {
            calc = on_time / expected;
        }

        result.insert(item.0.clone(), calc.clone());
    }

    Ok(result)
}
