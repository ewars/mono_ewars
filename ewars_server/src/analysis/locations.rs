use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::analysis::structs::{AnalysisQuery, IndicatorDefinition, LocationDefinition, Indicator};
use crate::utils::{date_utils};

pub type QueryResult = HashMap<NaiveDate, f64>;

pub fn query_locations(conn: &PoolConnection, locid: &Uuid, tki: &String, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let ind_def: Indicator = match query.indicator.clone().unwrap() {
        IndicatorDefinition::SystemIndicator(c) => c,
        _ => {
            return Err(json::to_value("NON_SYS_IND").unwrap());
        }
    };
    let metric: String = ind_def.metric.clone().unwrap_or("SUBMITTED".to_string());

    let mut wheres: Vec<String> = Vec::new();

    let result: Result<QueryResult, Value> = match metric.as_ref() {
        "REPORTING_LOCATIONS" => get_reporting_locations(&conn, &locid, &tki, &ind_def, &query),
        _ => {
            Err(json::to_value("UNK_SUB_DIM").unwrap())
        }
    };
    eprintln!("{:?}", result);

    result
}

#[derive(Debug, Clone)]
pub struct Location {
    pub uuid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: NaiveDate,
}

impl<'a> From<Row<'a>> for Location {
    fn from(row: Row) -> Self {
        Location {
            uuid: row.get(0),
            start_date: row.get(1),
            end_date: row.get(2),
        }
    }
}

impl Location {
    pub fn is_reporting(&self, dt: &NaiveDate) -> bool {
        return (self.start_date <= dt.clone() && self.end_date >= dt.clone());
    }
}

static SQL_GET_LOCATIONS: &'static str = r#"
    SELECT l.uuid,
        rp.start_date,
        COALESCE(rp.end_date, NOW()::DATE) AS end_date
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.reporting AS rp ON rp.lid = l.uuid
        LEFT JOIN {SCHEMA}.forms AS f ON l.lti::TEXT = jsonb_extract_path_text(f.definition, '__lid__', 'lti')
    WHERE rp.fid = $1
        AND l.lineage @> $2::UUID[]
        AND l.status = 'ACTIVE'
        AND rp.status = 'ACTIVE';
"#;

static SQL_GET_FORM_INTERVAL: &'static str = r#"
    SELECT jsonb_extract_path_text(definition, '__dd__', 'interval')
    FROM {SCHEMA}.forms
    WHERE uuid = $1;
"#;

fn get_reporting_locations(conn: &PoolConnection, locid: &Uuid, tki: &String, ind: &Indicator, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let form_id: Uuid = match ind.form_id {
        Some(res) => res,
        None => {
            return Err(json::to_value("ERR_NO_FORM_ID").unwrap());
        }
    };

    let status: String = ind.status.clone().unwrap_or("ACTIVE".to_string());

    let start_date: NaiveDate = NaiveDate::parse_from_str(&query.start_date.clone().unwrap_or("2017-01-01".to_string()), "%Y-%m-%d").unwrap();
    let end_date: NaiveDate = NaiveDate::parse_from_str(&query.end_date.clone().unwrap_or("2018-12-31".to_string()), "%Y-%m-%d").unwrap();

    // TODO: Get form interval
    let interval: Option<String> = match conn.query(&SQL_GET_FORM_INTERVAL.replace("{SCHEMA}", &tki), &[&form_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(row.get(0))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    let end_interval: String = interval.unwrap_or("DAY".to_string());

    let reporting_dates: Vec<NaiveDate> = date_utils::get_reporting_intervals(&start_date, &end_date, &end_interval).unwrap();

    let mut data: HashMap<NaiveDate, f64> = HashMap::new();

    for dt in &reporting_dates {
        data.insert(dt.clone(), 0.0);
    }

    let lid: Vec<Uuid> = vec![locid.clone()];
    let locations: Vec<Location> = match conn.query(&SQL_GET_LOCATIONS.replace("{SCHEMA}", &tki), &[&form_id, &lid]) {
        Ok(rows) => {
            rows.iter().map(|x| Location::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            return Err(json::to_value("ERR_RETRIEVE_LOCATIONS").unwrap());
        }
    };

    for loc in &locations {
        for dt in &reporting_dates {
            if loc.is_reporting(&dt) == true {
                data.entry(dt.clone())
                    .and_modify(|x| *x += 1.0)
                    .or_insert(1.0);
            }
        }
    }

    Ok(data)
}
