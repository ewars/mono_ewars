use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection};
use crate::analysis::structs::{AnalysisQuery};

#[derive(Debug, Deserialize, Clone)]
pub struct DataPoint {
    pub data: HashMap<String, Option<FieldValue>>,
    pub lid: Option<Uuid>,
    pub dd: Option<NaiveDate>,
    pub fid: Uuid,
}

impl<'a> From<Row<'a>> for DataPoint {
    fn from(row: Row) -> Self {
        let data: HashMap<String, Option<FieldValue>> = json::from_value(row.get("data")).unwrap();

        DataPoint {
            data,
            lid: row.get("lid"),
            dd: row.get("dd"),
            fid: row.get("fid"),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(untagged)]
pub enum FieldValue {
    ArrayValue(Vec<String>),
    TextValue(String),
    FloatValue(f64),
    IntegerValue(i64),
    DateValue(NaiveDate),
}

#[derive(Debug, Deserialize, Clone)]
#[serde(untagged)]
pub enum IndicatorDefinition {
    BasicSet(String),
    ComplexSet(Vec<String>),
}

#[derive(Debug, Deserialize, Clone)]
pub struct ExtrapDefinition {
    pub r: Option<String>,
    pub n: Option<Vec<IndicatorDefinition>>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct UsableForm {
    pub uuid: Uuid,
    pub ind_etl: ExtrapDefinition,
}

impl<'a> From<Row<'a>> for UsableForm {
    fn from(row: Row) -> Self {
        let ind_etl: ExtrapDefinition = json::from_value(row.get("etl_def")).unwrap();
        UsableForm {
            uuid: row.get("uuid"),
            ind_etl,
        }
    }
}

static SQL_GET_SOURCES: &'static str = r#"
    SELECT r.uuid, 
        CAST(r.etl->>'{NODE}' AS JSONB) AS etl_def
    FROM {SCHEMA}.forms AS r
    WHERE r.status = ANY(ARRAY['ACTIVE', 'ARCHIVED'])
    AND r.etl->'{NODE}' IS NOT NULL;
"#;

static SQL_GET_FORM_FIELDS: &'static str = r#"
    SELECT 
        json_build_object({NODES}) as fields
    FROM {SCHEMA}.forms AS r
        WHERE r.uuid = $1;
"#;

static SQL_GET_POINTS: &'static str = r#"
    SELECT
        json_build_object({NODES}) AS data,
        CAST(r.data->>'__lid__' AS UUID) AS lid,
        CAST(r.data->>'__dd__' AS DATE) AS dd,
        r.fid
    FROM {SCHEMA}.records AS r
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = r.data->>'__lid__'
    WHERE r.fid = ANY($1)
        AND l.lineage::UUID[] @> $2::UUID[]
        AND l.status = 'ACTIVE'
        AND CAST(r.data->>'__dd__' AS DATE) >= $3
        AND CAST(r.data->>'__dd__' AS DATE) <= $4;
"#;

// Retrieves a dataset and turns it into a single value time series.
pub fn get_data_series(conn: &PoolConnection, ind: &Uuid, loc: &Uuid, tki: &String, query: &AnalysisQuery, start_date: &NaiveDate, end_date: &NaiveDate) -> Result<HashMap<NaiveDate, f64>, Value> {
    let mut results: Vec<DataPoint> = Vec::new();

    let sql = SQL_GET_SOURCES.replace("{NODE}", &ind.to_string());
    let forms: Vec<UsableForm> = match conn.query(&sql.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| UsableForm::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    if forms.len() <= 0 {
        return Err(json::to_value("ERR_NO_IND_MAPPINGS").unwrap());
    }

    let mut fields: Vec<String> = Vec::new();
    let mut form_ids: Vec<Uuid> = Vec::new();

    for form in &forms {
        let n = form.ind_etl.n.clone();
        form_ids.push(form.uuid.clone());
        match n {
            Some(res) => {
                for r in res {
                    match r {
                        IndicatorDefinition::BasicSet(c) => {
                            let item: String = c.to_string();
                            if item.contains(":") {
                                fields.push(item.split(":").collect::<Vec<&str>>()[0].to_string());
                            } else {
                                fields.push(item.clone());
                            }
                        }, 
                        IndicatorDefinition::ComplexSet(c) => {
                            for t in &c[1..] {
                                let b: String = t.to_string();
                                if b.contains(":") {
                                    fields.push(b.split(":").collect::<Vec<&str>>()[0].to_string());
                                } else {
                                    fields.push(b.clone());
                                }
                            }
                        },
                        _ => {}
                    }
                }
            },
            None => {}
        }
    }

    form_ids.dedup();
    fields.dedup();

    let mut selects: Vec<String> = Vec::new();

    for field in &fields {
        selects.push(format!("'{}'", field.clone()));
        selects.push(format!("r.data->'{}'", &field.replace("data.", "")));
    }


    let data_sql = SQL_GET_POINTS.replace("{NODES}", &selects.join(","));

    let lid: Vec<Uuid> = vec![loc.clone()];
    let records: Vec<DataPoint> = match conn.query(&data_sql.replace("{SCHEMA}", &tki), &[&form_ids, &lid, &start_date, &end_date]) {
        Ok(rows) => {
            rows.iter().map(|x| DataPoint::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut data: HashMap<NaiveDate, Vec<DataPoint>> = HashMap::new();

    for dp in &records {
        data.entry(dp.dd.clone().unwrap())
            .and_modify(|x| {x.push(dp.clone())})
            .or_insert(vec![dp.clone()]);
    }

    let mut end_data: HashMap<NaiveDate, f64> = HashMap::new();

    let default_value: f64 = 0.0;
    for (key, val) in data {
        let mut value: f64 = 0.0;
        end_data.insert(key.clone(), value);

        let mut top_values: Vec<f64> = Vec::new();

        for dp in &val {
            for form in &forms {
                let n = form.ind_etl.n.clone();
                let r = form.ind_etl.r.clone();

                let mut values: Vec<f64> = Vec::new();

                match n {
                    Some(res) => {
                        for r in res {
                            match r {
                                IndicatorDefinition::BasicSet(c) => {
                                    if c.contains(":") {
                                        let field_list: Vec<&str>  = c.split(":").collect();
                                        let field_name: String = field_list[0].to_string();
                                        let cmp: String = field_list[1].to_string();
                                        let cmp_val: String = field_list[2].to_string();
                                    } else {
                                        match dp.data.get(&c).unwrap() {
                                            Some(nn) => {
                                                match nn {
                                                    FieldValue::FloatValue(c) => {
                                                        values.push(c.clone());
                                                    },
                                                    _ => {
                                                        eprintln!("{:?}", c);
                                                    }
                                                }
                                            },
                                            None => {
                                                values.push(0.0);
                                            }
                                        }
                                    }
                                },
                                IndicatorDefinition::ComplexSet(c) => {
                                    eprintln!("{:?}", c);
                                },
                                _ => {}
                            }
                        }
                    },
                    None => {}
                }

                let agg_type: String = r.unwrap_or("SUM".to_string());

                match agg_type.as_ref() {
                    "SUM" => {
                        let result: f64 = values.iter().sum();
                        top_values.push(result);
                    },
                    _ => {
                        let result: f64 = values.iter().sum();
                        top_values.push(result);
                    }
                }

            }
        }

        let resultant: f64 = top_values.iter().sum();
        end_data.insert(key.clone(), resultant);
    }


    Ok(end_data)
}
