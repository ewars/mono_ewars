use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::analysis::structs::{AnalysisQuery, IndicatorDefinition, LocationDefinition, Indicator};
use crate::api::locations::{calc_completeness, DataResult};
use crate::api::locations::{calc_timeliness, TimelyDataResult};
use crate::analysis::utils;

pub type QueryResult = HashMap<NaiveDate, f64>;

pub fn query_submissions(conn: &PoolConnection, locid: &Uuid, tki: &String, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let ind_def: Indicator = match query.indicator.clone().unwrap() {
        IndicatorDefinition::SystemIndicator(c) => c,
        _ => {
            return Err(json::to_value("NON_SYS_IND").unwrap());
        }
    };
    let metric: String = ind_def.metric.clone().unwrap_or("SUBMITTED".to_string());

    let mut wheres: Vec<String> = Vec::new();

    let result: Result<QueryResult, Value> = match metric.as_ref() {
        "COMPLETENESS" => get_submissions(&conn, &locid, &tki, &ind_def, &query),
        "TIMELINESS" => get_timeliness(&conn, &locid, &tki, &ind_def, &query),
        "SUBMISSIONS" => get_submissions(&conn, &locid, &tki, &ind_def, &query),
        "SUBMITTED" => get_submissions(&conn, &locid, &tki, &ind_def, &query),
        "MISSING" => get_missing(&conn, &locid, &tki, &ind_def, &query),
        "LATE" => get_late(&conn, &locid, &tki, &ind_def, &query),
        _ => {
            Err(json::to_value("UNK_SUB_DIM").unwrap())
        }
    };
    eprintln!("{:?}", result);

    result
}

fn get_submissions(conn: &PoolConnection, locid: &Uuid, tki: &String, ind: &Indicator, query: &AnalysisQuery) -> Result<QueryResult, Value> {

    let form_id: Uuid = match ind.form_id {
        Some(res) => res,
        None => {
            return Err(json::to_value("ERR_NO_FORM").unwrap());
        }
    };
    let start_date = NaiveDate::parse_from_str("2016-01-01", "%Y-%m-%d").unwrap();
    let end_date = NaiveDate::parse_from_str("2018-12-31", "%Y-%m-%d").unwrap();

    let data: Vec<(NaiveDate, DataResult)> = match calc_completeness(&conn, &tki, &locid, &form_id, &start_date, &end_date) {
        Ok(res) => res,
        Err(err) => {
            return Err(json::to_value("CALC_ERR").unwrap());
        }
    };

    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    for item in &data {
        let r: f64 = item.1.s as f64;
        result.entry(item.0.clone())
            .and_modify(|x| *x += r)
            .or_insert(r);
    }

    Ok(result)
} 

fn get_expected(conn: &PoolConnection, locid: &Uuid, tki: &String, ind: &Indicator, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let form_id: Uuid = match ind.form_id {
        Some(res) => res,
        None => {
            return Err(json::to_value("ERR_NO_FORM").unwrap());
        }
    };
    let start_date = NaiveDate::parse_from_str("2016-01-01", "%Y-%m-%d").unwrap();
    let end_date = NaiveDate::parse_from_str("2018-12-31", "%Y-%m-%d").unwrap();

    let data: Vec<(NaiveDate, DataResult)> = match calc_completeness(&conn, &tki, &locid, &form_id, &start_date, &end_date) {
        Ok(res) => res,
        Err(err) => {
            return Err(json::to_value("CALC_ERR").unwrap());
        }
    };

    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    for item in &data {
        let r: f64 = item.1.e as f64;
        result.entry(item.0.clone())
            .and_modify(|x| *x += r)
            .or_insert(r);
    }

    Ok(result)
}

fn get_timeliness(conn: &PoolConnection, locid: &Uuid, tki: &String, ind: &Indicator, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let query_type = query.query_type.clone();
    let form_id: Uuid = match ind.form_id {
        Some(res) => res,
        None => {
            return Err(json::to_value("ERR_NO_FORM").unwrap());
        }
    };
    let start_date = NaiveDate::parse_from_str("2016-01-01", "%Y-%m-%d").unwrap();
    let end_date = NaiveDate::parse_from_str("2018-12-31", "%Y-%m-%d").unwrap();

    let data: Vec<(NaiveDate, TimelyDataResult)> = match calc_timeliness(&conn, &tki, &locid, &form_id, &start_date, &end_date) {
        Ok(res) => res,
        Err(err) => {
            return Err(json::to_value("CALC_ERR").unwrap());
        }
    };

    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    // Because completeness is a bit janky, it depends on the type of query being executed
    // to determine how it gets returned from this function.
    // If the query is going to reduce the value further up we do it here now as it's a calculated
    // value and there's now ay to return a summable collection here.
    match query_type.as_ref() {
        "SLICE" => {
            result = utils::reduce_timeliness(&data, &end_date).unwrap();
        },
        "SLICE_COMPLEX" => {
            result = utils::reduce_timeliness(&data, &end_date).unwrap();
        },
        "SERIES" => {
            result = utils::reduce_series_timeliness(&data).unwrap();
        },
        "SERIES_COMPLEX" => {
            result = utils::reduce_series_timeliness(&data).unwrap();
        },
        "MAP_SLICE" => {
            result = utils::reduce_timeliness(&data, &end_date).unwrap();
        },
        "MAP_SLICE_COMPLEX" => {
            result = utils::reduce_timeliness(&data, &end_date).unwrap();
        }
        _ => {}
    }

    Ok(result)
}


fn get_completeness(conn: &PoolConnection, locid: &Uuid, tki: &String, ind: &Indicator, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let query_type = query.query_type.clone();
    let form_id: Uuid = match ind.form_id {
        Some(res) => res,
        None => {
            return Err(json::to_value("ERR_NO_FORM").unwrap());
        }
    };
    let start_date = NaiveDate::parse_from_str("2016-01-01", "%Y-%m-%d").unwrap();
    let end_date = NaiveDate::parse_from_str("2018-12-31", "%Y-%m-%d").unwrap();

    let data: Vec<(NaiveDate, DataResult)> = match calc_completeness(&conn, &tki, &locid, &form_id, &start_date, &end_date) {
        Ok(res) => res,
        Err(err) => {
            return Err(json::to_value("CALC_ERR").unwrap());
        }
    };

    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    // Because completeness is a bit janky, it depends on the type of query being executed
    // to determine how it gets returned from this function.
    // If the query is going to reduce the value further up we do it here now as it's a calculated
    // value and there's now ay to return a summable collection here.
    match query_type.as_ref() {
        "SLICE" => {
            result = utils::reduce_completeness(&data, &end_date).unwrap();
        },
        "SLICE_COMPLEX" => {
            result = utils::reduce_completeness(&data, &end_date).unwrap();
        },
        "SERIES" => {
            result = utils::reduce_series_completeness(&data).unwrap();
        },
        "SERIES_COMPLEX" => {
            result = utils::reduce_series_completeness(&data).unwrap();
        },
        "MAP_SLICE" => {
            result = utils::reduce_completeness(&data, &end_date).unwrap();
        },
        "MAP_SLICE_COMPLEX" => {
            result = utils::reduce_completeness(&data, &end_date).unwrap();
        }
        _ => {}
    }

    Ok(result)
}

// Calculate the number
fn get_missing(conn: &PoolConnection, locid: &Uuid, tki: &String, ind: &Indicator, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let form_id: Uuid = match ind.form_id {
        Some(res) => res,
        None => {
            return Err(json::to_value("ERR_NO_FORM").unwrap());
        }
    };
    let start_date = NaiveDate::parse_from_str("2016-01-01", "%Y-%m-%d").unwrap();
    let end_date = NaiveDate::parse_from_str("2018-12-31", "%Y-%m-%d").unwrap();

    let data: Vec<(NaiveDate, DataResult)> = match calc_completeness(&conn, &tki, &locid, &form_id, &start_date, &end_date) {
        Ok(res) => res,
        Err(err) => {
            return Err(json::to_value("CALC_ERR").unwrap());
        }
    };

    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    for item in &data {
        let expected: f64 = item.1.e.clone() as f64;
        let submitted: f64 = item.1.s.clone() as f64;

        let mut calc: f64 = 0.0;
        if expected > 0.0 {
            calc = expected - submitted;
        }

        result.insert(item.0.clone(), calc.clone());
    }

    Ok(result)
}

// Calculate the number of records late per interval
fn get_late(conn: &PoolConnection, locid: &Uuid, tki: &String, ind: &Indicator, query: &AnalysisQuery) -> Result<QueryResult, Value> {
    let query_type = query.query_type.clone();
    let form_id: Uuid = match ind.form_id {
        Some(res) => res,
        None => {
            return Err(json::to_value("ERR_NO_FORM").unwrap());
        }
    };
    let start_date = NaiveDate::parse_from_str("2016-01-01", "%Y-%m-%d").unwrap();
    let end_date = NaiveDate::parse_from_str("2018-12-31", "%Y-%m-%d").unwrap();

    let data: Vec<(NaiveDate, TimelyDataResult)> = match calc_timeliness(&conn, &tki, &locid, &form_id, &start_date, &end_date) {
        Ok(res) => res,
        Err(err) => {
            return Err(json::to_value("CALC_ERR").unwrap());
        }
    };

    let mut result: HashMap<NaiveDate, f64> = HashMap::new();

    for item in &data {
        let expected: f64 = item.1.e.clone() as f64;
        let on_time: f64 = item.1.o.clone() as f64;

        let mut calc: f64 = 0.0;
        if expected > 0.0 {
            calc = expected - on_time;
        }

        result.insert(item.0.clone(), calc.clone());
    }

    Ok(result)
}

