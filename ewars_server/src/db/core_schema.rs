static SQL_CORE_SCHEMA: &'static str = r#"
    CREATE TABLE core.accounts (
        uuid UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
        id SERIAL NOT NULL,
        name TEXT NOT NULL,
        description TEXT NULL,
        language TEXT NOT NULL DEFAULT 'en',
        status TEXT NOT NULL DEFAULT 'INACTIVE',
        domain TEXT NOT NULL,
        theme JSONB NOT NULL DEFAULT '{}',
        tki TEXT NOT NULL,
        config JSONB NOT NULL DEFAULT '{}',
        created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
        created_by UUID NULL,
        modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
        created_by UUID NULL
    );


    CREATE TABLE sessions (
    
    );

    CREATE TABLE users (

    );

    CREATE TABLE translations (

    );
"#;
