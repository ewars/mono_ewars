use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::api::utils::*;

static SQL_GET_FORMS_COUNT: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.forms
    WHERE status = ANY(ARRAY['ACTIVE', 'ARCHIVED']);
"#;

static SQL_GET_RECORD_COUNT: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.records
    WHERE status = 'SUBMITTED';
"#;

static SQL_GET_USER_COUNT: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.users
    WHERE status = 'ACTIVE';
"#;

static SQL_GET_LOCATION_COUNT: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.locations
    WHERE status = 'ACTIVE';
"#;

static SQL_GET_ALERTS_OPEN: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.alerts
    WHERE status = 'OPEN';
"#;

static SQL_GET_ALERTS_CLOSED: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.alerts
    WHERE status = 'CLOSED';
"#;

static SQL_GET_ALERTS: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.alerts;
"#;

static SQL_GET_DEVICES: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.devices;
"#;

static SQL_GET_PARTNERS: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.organizations;
"#;

static SQL_GET_TASKS_OPEN: &'static str = r#"
    SELECT COUNT(uui) FROM {SCHEMA}.tasks WHERE status = 'OPEN';
"#;

static SQL_GET_ALARMS: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.alarms WHERE status = 'ACTIVE';
"#;

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT COUNT(uuid) FROM {SCHEMA}.assignments WHERE status = 'ACTIVE';
"#;

pub fn get_user_metric(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let metric: String = json::from_value(msg.args.clone()).unwrap();

    let mut result: i64 = match metric.as_ref() {
        "FORMS" => get_count_result!(conn, SQL_GET_FORMS_COUNT, tki),
        "FORM_SUBMISSIONS" => get_count_result!(conn, SQL_GET_RECORD_COUNT, tki),
        "SUBMISSIONS" => get_count_result!(conn, SQL_GET_RECORD_COUNT, tki),
        "USERS" => get_count_result!(conn, SQL_GET_USER_COUNT, tki),
        "LOCATIONS" => get_count_result!(conn, SQL_GET_LOCATION_COUNT, tki),
        "ALERTS_OPEN" => get_count_result!(conn, SQL_GET_ALERTS_OPEN, tki),
        "ALERTS_CLOSED" => get_count_result!(conn, SQL_GET_ALERTS_CLOSED, tki),
        "ALERTS_TOTAL" => get_count_result!(conn, SQL_GET_ALERTS, tki),
        "DEVICES" => get_count_result!(conn, SQL_GET_DEVICES, tki),
        "PARTNERS" => get_count_result!(conn, SQL_GET_PARTNERS, tki),
        "TASKS_OPEN" => get_count_result!(conn, SQL_GET_TASKS_OPEN, tki),
        "ALARMS" => get_count_result!(conn, SQL_GET_ALARMS, tki),
        "ORGANIZATIONS" => get_count_result!(conn, SQL_GET_PARTNERS, tki),
        "ASSIGNMENTS" => get_count_result!(conn, SQL_GET_ASSIGNMENTS, tki),
        _ => 0
    };


    Ok(json::to_value(result).unwrap())
}


