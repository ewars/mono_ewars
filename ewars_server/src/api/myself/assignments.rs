use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::myself::RawAssignment;

static GET_ROOT_ASSIGNMENTS: &'static str = r#"
    SELECT 
        a.uuid,
        a.lid,
        a.fid,
        a.assign_type,
        a.assign_group,
        f.features,
        l.name AS location_name,
        f.name AS form_name,
        l.status AS location_status,
        f.status AS form_status,
        a.status AS assign_status,
        rp.status AS reporting_status,
        rp.start_date,
        rp.end_date
    FROM {SCHEMA}.assignments AS a
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = a.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = a.lid
        LEFT JOIN {SCHEMA}.reporting AS rp ON rp.lid = a.uuid AND rp.fid = a.fid
    WHERE a.uid = $1
        AND f.status = ANY(ARRAY['ACTIVE', 'ARCHIVED']);
"#;

static SQL_GET_UNDERS: &'static str = r#"
    SELECT l.uuid
    FROM {SCHEMA}.locations AS l
    WHERE l.lineage @> ARRAY[$1]::UUID[]
        AND l.lti = $2;
"#;

static SQL_GET_GROUP_LOCATIONS: &'static str = r#"
    SELECT l.uuid
    FROM {SCHEMA}.locations AS l
    WHERE l.groups @> ARRAY[$1]::TEXT[]
        AND l.lti = $2;
"#;

// Derive the assignmens that a user has
pub fn get_derived_assignments(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let assignments: Vec<RawAssignment> = match &conn.query(&GET_ROOT_ASSIGNMENTS.replace("{SCHEMA}", &tki), &[&msg.user.uuid]) {
        Ok(rows) => {
            rows.iter().map(|x| RawAssignment::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut end_assigns: Vec<RawAssignment> = Vec::new();

    for assign in assignments {
        if assign.has_location() {        
            match assign.assign_type.as_ref() {
                "UNDER" => {
                    if let Some(feat) = assign.features.get("LOCATION_REPORTING") {
                        let lti: Uuid = feat.lti.unwrap();
                        // Get all locations under the parent of this type
                        let locations: Vec<Uuid> = match &conn.query(&SQL_GET_UNDERS.replace("{SCHEMA}", &tki), &[&vec![assign.lid], &lti]) {
                            Ok(rows) => {
                                rows.iter().map(|x| x.get("uuid")).collect()
                            },
                            Err(err) => {
                                eprintln!("{:?}", err);
                                Vec::new()
                            }
                        };

                        for loc in locations {
                            let mut new_assign = assign.clone();
                            new_assign.assign_type = "DEFAULT".to_string();
                            new_assign.lid = Some(loc);
                            end_assigns.push(assign.to_owned());
                        }
                    }
                },
                "GROUP" => {
                    if let Some(feat) = assign.features.get("LOCATION_REPORTING") {
                        let lti: Uuid = feat.lti.unwrap();
                        
                        let group: String = assign.assign_group.clone().unwrap();
                        let locations: Vec<Uuid> = match &conn.query(&SQL_GET_GROUP_LOCATIONS.replace("{SCHEMA}", &tki), &[&vec![&group], &lti]) {
                            Ok(rows) => {
                                rows.iter().map(|x| x.get("uuid")).collect()
                            },
                            Err(err) => {
                                eprintln!("{:?}", err);
                                Vec::new()
                            }
                        };
                        
                        for loc in locations {
                            let mut new_assign = assign.clone();
                            new_assign.lid = Some(loc.clone());
                            new_assign.assign_type = "DEFAULT".to_string();
                            new_assign.assign_group = Option::None;
                            end_assigns.push(new_assign.to_owned());
                        }
                    }
                },
                _ => {
                    end_assigns.push(assign.clone());
                }
            }
        } else {
            // This assignment is probably for a normal form with no location/interval reporting
            end_assigns.push(assign.clone());
        }
    }

    Ok(json::to_value(end_assigns).unwrap())
}
