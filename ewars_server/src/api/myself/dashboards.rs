use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::dashboards::{DEFAULT_DASHBOARDS, DefaultDashboard};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Dashboard {
    pub uuid: Option<Uuid>,
    pub name: String,
    pub definition: Option<Value>,
    pub color: Option<String>,
    pub order: Option<i32>,
}

#[derive(Debug, Serialize, Clone)]
pub struct DashboardsResult {
    pub dashboards: Vec<Dashboard>,
    pub initial: Option<Dashboard>,
}


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DashConfig {
    pub name: String,
    pub uuid: Uuid,
    pub order: Option<i32>,
}

static SQL_GET_DASH_CONFIG: &'static str = r#"
    SELECT value
    FROM {SCHEMA}.conf
    WHERE key = 'DASHBOARDS';
"#;

static SQL_GET_DASHBOARD: &'static str = r#"
    SELECT uuid, name, layout AS definition 
    FROM {SCHEMA}.resources
    WHERE uuid = $1;
"#; 

pub fn get_dashboards(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let result: Value;
    

    let dashes: HashMap<String, Vec<DashConfig>> = match conn.query(&SQL_GET_DASH_CONFIG.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                eprintln!("{:?}", row);

                let data: HashMap<String, Vec<DashConfig>> = json::from_value(row.get(0)).unwrap();
                data
            } else {
                HashMap::new()
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            HashMap::new()
        }
    };

    let empty_vec: Vec<DashConfig> = Vec::new();
    let user_dashes: Vec<DashConfig> = dashes.get(&msg.user.role).unwrap_or(&empty_vec).to_vec();

    if user_dashes.len() <= 0 {
        // Return default dashboards as this user type doesn't have any dashboards configured
       let dashboards: Vec<DefaultDashboard> = DEFAULT_DASHBOARDS.get(&msg.user.role[..]).unwrap().to_vec();

       let mut dash_digis: Vec<Dashboard> = Vec::new();
       let mut default_dash: Option<Dashboard> = Option::None;

       for dash in &dashboards {
           let color = match dash.color {
               Some(res) => Some(res.to_string()),
               None => Option::None
           };

           if dash.default == true {
               default_dash = Some(Dashboard {
                    uuid: Option::None,
                    name: dash.name.to_string(),
                    definition: Some(json::to_value(dash.definition.clone()).unwrap()),
                    color: color.clone(),
                    order: Some(dash.order.clone()),
               })
           }

           dash_digis.push(Dashboard {
                uuid: Option::None,
                name: dash.name.to_string(),
                color,
                definition: Option::None,
                order: Some(dash.order),
           });
       }

       result = json::to_value(DashboardsResult {
           dashboards: dash_digis,
           initial: default_dash,
       }).unwrap();

       // Prematurely exit returning dashboards
       return Ok(result);
    }

    let mut dash_digis: Vec<Dashboard> = Vec::new();
    let mut default_dash: Option<Dashboard> = Option::None;
    for dash in &user_dashes {
        let dash_config: Option<Dashboard> = match conn.query(&SQL_GET_DASHBOARD.replace("{SCHEMA}", &tki), &[&dash.uuid]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    
                    Some(Dashboard {
                        uuid: Some(dash.uuid.clone()),
                        name: row.get("name"),
                        definition: row.get("definition"),
                        color: Option::None,
                        order: Some(dash.order.unwrap_or(0)),
                    })
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        };

        // If we found the dashboard we add it
        if let Some(result_dash) = dash_config {
            dash_digis.push(result_dash);
        }
    }

    
    dash_digis.sort_by_key(|k| k.order.unwrap_or(0));

    if dash_digis.len() > 0 {
        // Need to get the initial dashboard

        default_dash = Some(dash_digis[0].clone());
    }

    result = json::to_value(DashboardsResult {
        dashboards: dash_digis,
        initial: default_dash,
    }).unwrap();


    Ok(result)
}
