use std::collections::HashMap;
use failure::Error;

use postgres::{rows}::{Row};
use chrono::prelude::*;
use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::models::form::{FormFeature};
use crate::utils::date_utils::*;


#[derive(Debug, Clone)]
pub struct LocationReporting {
    pub uuid: Uuid,
    pub name: Value,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub lti: Uuid,
    pub status: String,
    pub reporting_status: String,
    pub report_date: vec<NaiveDate>,
}

impl<'a> From<Row<'a>> for LocationReporting {
    fn from(row: Row) -> Self {
        LocationReporting {
            uuid: row.get(0),
            name: row.get(1),
            start_date: row.get(2),
            end_date: row.get(3),
            lti: row.get(4),
            status: row.get(5),
            reporting_status: row.get(6),
            report_dates: row.get(7),
        }
    }
}


impl LocationReporting {
    // Check if the report date has already been reported
    pub fn has_date(&self, rp: &NaiveDate) -> bool {
        if self.report_dates.contains(&rp) {
            true 
        } else {
            false 
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Assignment {
    pub uuid: Uuid,
    pub lid: Option<Uuid>,
    pub fid: Uuid,
    pub assign_type: String,
    pub assign_group: String,
    pub status: String,
    pub location_name: Option<Value>,
    pub form_name: Option<Value>,
    pub features: HashMap<String, FormFeature>,
}

impl<'a> From<Row<'a>> for Assignment {
    fn from(row: Row) -> Self {
        let features: HashMap<String, FormFeature> = json::from_value(row.get("features")).unwrap();
        Assignment {
            uuid: row.get("uuid"),
            lid: row.get("lid"),
            fid: row.get("fid"),
            assign_type: row.get("assign_type"),
            assign_group: row.get("assign_group"),
            status: row.get("status"),
            location_name: row.get("location_name"),
            form_name: row.get("form_name"),
            features,
        }
    }
}

impl Assignment {
    // Check if this form definition has an overdue interval
    // configured for it
    pub fn has_overdue(&self) -> bool {
        if let Some(c) = &self.features.get("OVERDUE".to_string()) {
            return true;
        }
        return false;
    }

    // Get the number of days for a record to be overdue
    pub fn get_overdue_interval(&self) -> i32 {
        0
    }

    // Check whether this assignment is a location-based 
    // reportig context
    pub fn is_location_based(&self) -> bool {
        if self.lid.is_some() {
            return true;
        }

        if &self.assign_type == "GROUP" {
            return true;
        }

        return false;
    }

    // Get the interval type used for the form
    pub fn get_interval(&self) -> String {
        if let Some(c) = self.features.get("INTERVAL_REPORTING") {
            if let Some(d) = c.get("interval") {
                d.clone()
            } else {
                "DAY".to_string()
            }

        } else {
            "DAY".to_string()
        }
    }

    // Pull out the location type that's used for reporting
    pub get_reporting_location_type(&self) -> Option<Uuid> {
        if let Some(c) = self.features.get("LOCATION_REPORTING") {
            if let Some(d) = d.get("lti") {
                return d.clone()
            } else {
                Option::None
            }
        } else {
            Option::None
        }
    }
}


static SQL_GET_ASSIGNS: &'static str = r#"
    SELECT a.uuid, a.lid, a.fid, a.assign_type, a.assign_group, a.status,
        l.name AS location_name, f.name AS form_name,
        f.features
    FROM {SCHEMA}.assignments AS a
        LEFT JOIN {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.form AS f
"#;

#[derive(Debug, Serialize, Clone)]
pub struct UpcomingContext {
    pub lid: Uuid,
    pub fid: Uuid,
    pub location_name: Value,
    pub form_name: Value,
    pub report_date: NaiveDate,
    pub form_interval: String,
}


pub fn get_upcoming(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = msg.user.uuid.clone();

    let assigns: Vec<Assignment> = match &conn.query(&SQL_GET_ASSIGNS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => rows.iter().map(|x| Assignment::from(x)).collect(),
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut reportables_locations: Vec<LocationReporting> = Vec::new();

    let end_of_week: NaiveDate = get_next_week_end();
    let end_of_day: NaiveDate = get_next_day_end();
    let end_of_month: NaiveDate = get_next_month_end();
    let end_of_year: NaiveDate = get_next_year_end();

    let mut intervals: HashMap<String, String> = HashMap::new();

    for assign in &assigns {
        if assign.has_interval() && assign.has_location() {
            let lti: Uuid = assign.get_reporting_location_type().unwrap();

            intervals.entry(&assign.fid)
                .modify(|x| *x = &assign.get_interval().clone())
                .or_insert(&assign.get_interval().clone());

            match assign.assign_type.as_ref() {
                "UNDER" => {
                    let locs: Vec<LocationReporting> = get_locations_under(&conn, &tki, &assign.get_lid(), &assign.fid);
                    reportable_locations.extend(locs.iter().filter(|x| &x.lti == &lti).cloned());
                },
                "GROUP" => {
                    let locs: Vec<LocationReporting> = get_locations_in_group(&conn, &tki, &assign.get_assign_group(), &assign.fid);
                    reportable_Locations.extend(locs.iter().filter(|x| &x.lti == &lti).clone());
                },
                "DEFAULT" => {
                    // Assugme a direct assignment is for the appropriate location type
                    let location_report = get_single_location(&conn, &tki, &assign.lid, &assign.fid);
                    reportable_locations.push(location_report.clone());
                }
            }
        }
    }

    let mut results: Vec<UpcomingContext> = Vec::new();


    for item in &reportable_locations {
        let interval: String = intervals.get(&item.fid).clone().unwrap_or("DAY".to_string());

        match interval.as_ref() {
            "DAY" => {
                if !item.has_date(&end_of_day) {
                    results.push(UpcomingContext {
                        lid: item.lid.clone(),
                        fid: item.fid.clone(),
                        location_name: item.name.clone(),
                        form_name: item.form_name.clone(),
                        report_date: end_of_day.clone(),
                        form_interval: interval.clone(),
                    });
                } 
            },
            "WEEK" => {
                if !item.has_date(&end_of_week) {
                    results.push(UpcomingContext {
                        lid: item.lid.clone(),
                        fid: item.fid.clone(),
                        location_name: item.name.clone(),
                        form_name: item.form_name.clone(),
                        report_date: end_of_week.clone(),
                        form_interval: interval.clone(),
                    });
                }
            },
            "MONTH" => {
                if !item.has_date(&end_of_month) {
                    results.push(UpcomingContext {
                        lid: item.lid.clone(),
                        fid: item.fid.clone(),
                        location_name: item.name.clone(),
                        form_name: item.form_name.clone(),
                        report_date: end_of_month.clone(),
                        form_interval: interval.clone(),
                    });
                }
            },
            "YEAR" => {
                if !item.has_date(&end_of_year) {
                    results.push(UpcomingContext {
                        lid: item.lid.clone(),
                        fid: item.fid.clone(),
                        location_name: item.name.clone(),
                        form_name: item.form_name.clone(),
                        report_date: end_of_year.clone(),
                        form_interval: interval.clone(),
                    });
                }
            },
            _ => {}
        }
    }

    Ok(json::to_value(results).unwrap())
}

static SQL_GET_LOCATION_REPORTING: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, r.start_date, r.end_date, r.status AS reporting_status,
    ARRAY(
        SELECT c.data->>'__dd__' 
        FROM {SCHEMA}.records AS c 
        WHERE c.data->>'__lid__' = l.uuid::TEXT
            AND c.fid = $1
    ) AS report_dates
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.reporting AS r ON r.lid = l.uuid AND r.fid = $2
    WHERE l.uuid = $3
    AND l.status = 'ACTIVE'
    AND r.status = 'ACTIVE';
"#;

fn get_single_location(conn: PoolConnection, tki: &String, id: &Uuid, fid: &Uuid) -> LocationReporting {
    match &conn.query(&SQL_GET_LOCATION_REPORTING.replace("{SCHEMA}", &tki), &[&fid, &id, &fid]) {
        Ok(rows) => rows.iter().map(|x| LocationReporting::from(x)).collect(),
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    }
}


static SQL_GET_LOCATIONS_UNDER: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, r.start_date, r.end_date, r.status AS reporting_status,
    ARRAY(
        SELECT c.data->>'__dd__' 
        FROM {SCHEMA}.records AS c 
        WHERE c.data->>'__lid__' = l.uuid::TEXT
            AND c.fid = $1
    ) AS report_dates
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.reporting AS r ON r.lid = l.uuid AND r.fid = $2
    WHERE l.lineage @> $3::UUID[]
    AND l.status = 'ACTIVE'
    AND r.status = 'ACTIVE';
"#;


fn get_locations_under(conn: &PoolConnection, tki: &String, id: &Uuid, fid: &Uuid) -> Vec<LocationReporting> {
    let vec_lid = vec![id.clone()];
    match &conn.query(&SQL_GET_LOCATIONS_UNDER.replace("{SCHEMA}", &tki), &[&fid, &fid, &vec_lid]) {
        Ok(rows) => rows.iter().map(|x| LocationReporting::from(x)).collect(),
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    }
}

static SQL_GET_LOCATIONS_IN_GROUP: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, r.start_date, r.end_date, r.status AS reporting_status,
    ARRAY(
        SELECT c.data->>'__dd__' 
        FROM {SCHEMA}.records AS c 
        WHERE c.data->>'__lid__' = l.uuid::TEXT
            AND c.fid = $1
    ) AS report_dates
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.reporting AS r ON r.lid = l.uuid AND r.fid = $2
    WHERE l.groups::TEXT[] @> $3::TEXT[]
    AND l.status = 'ACTIVE'
    AND r.status = 'ACTIVE';
"#;

fn get_locations_in_group(conn: &PoolConnection, tki: &string, group: &String, fid: &Uuid) -> Vec<LocationReporting> {
    let vec_group = vec![group.clone()];
    match &conn.query(&SQL_GET_LOCATIONS_IN_GROUP.replace("{SCHEMA}", &tki), &[&fid, &fid, &vec_group]) {
        Ok(rows) => rows.iter().map(|x| LocationReporting::from(x)).collect(),
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    }
}
