use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;

use postgres::rows::Row;
use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

static SQL_GET_ITEMS: &'static str = r#"
    SELECT a.*, u.name
    FROM {SCHEMA}.activity_feed AS a
        LEFT JOIN _iw.users AS u ON u.id = a.triggered_by
    ORDER BY a.created DESC
    LIMIT 10
    OFFSET $1;
"#;

static SQL_GET_COUNT: &'static str = r#"
    SELECT COUNT(*) FROM {SCHEMA}.activity_feed;
"#;

static SQL_GET_USER: &'static str = r#"
    SELECT u.name, u.role, o.name AS org_name
    FROM {SCHEMA}.users AS u
        LEFT JOIN _iw.organizations AS o
    WHERE u.id = $1;
"#;

static SQL_GET_RECORD: &'static str = r#"
    SELECT f.name->>'en' AS form_name,
        l.name->>'en' AS location_name,
        c.data_date,
        c.uuid,
        f.features->'INTERVAL_REPORTING'->>'interval' AS reporting_interval
    FROM {SCHEMA}.collections AS c
        LEFT JOIN {SCHEMA}.forms AS f ON f.id = c.form_id
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = c.location_id
    WHERE c.uuid::TEXT = $1;
"#;

#[derive(Debug, Serialize, Deserialize)]
struct User {
    pub name: String,
    pub role: String,
    pub org_name: Value,
}

impl User {
    pub fn from_row(row: &Row) -> Self {
        Self {
            name: row.get(0),
            role: row.get(1),
            org_name: row.get(2),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Record {
    pub form_name: String,
    pub location_name: Option<String>,
    pub data_date: Option<NaiveDate>,
    pub uuid: Uuid,
    pub reporting_interval: Option<String>,
}

impl Record {
    pub fn from_row(row: &Row) -> Self {
        Self {
            form_name: row.get(0),
            location_name: row.get(1),
            data_date: row.get(2),
            uuid: row.get(3),
            reporting_interval: row.get(4),
        }
    }
}


#[derive(Debug, Serialize, Deserialize)]
struct ActivityItem {
    pub uuid: Uuid,
    pub event_type: String,
    pub triggered_by: i32,
    pub created: NaiveDateTime,
    pub icon: Option<String>,
    pub likes: i32,
    pub attachments: Vec<(String, Value)>,
    pub content: Option<String>,
    pub name: Option<String>,
    pub meta: HashMap<String, Option<Value>>,
}

impl ActivityItem {
    pub fn from_row(row: &Row) -> Self {
        let attachments: Vec<(String, Value)> = json::from_value(row.get(6)).unwrap();
        Self {
            uuid: row.get(0),
            event_type: row.get(1),
            triggered_by: row.get(2),
            created: row.get(3),
            icon: row.get(4),
            likes: row.get(5),
            attachments,
            content: row.get(7),
            name: row.get(8),
            meta: HashMap::new(),
        }
    }

    pub fn update_meta(&mut self, data: HashMap<String, Option<Value>>) {
        self.meta = data;
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct ActivityResult {
    pub activity: Vec<ActivityItem>,
    pub count: i64,
}

pub fn get_activity(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: Vec<String> = Vec::new();
    Ok(json::to_value(data).unwrap())
}

pub fn _get_activity(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let offset: i64 = msg.args.get(0).unwrap().as_i64().unwrap();
    let mut items: Vec<ActivityItem> = conn.query(&SQL_GET_ITEMS.replace("{SCHEMA}", &tki), &[&offset]).unwrap()
        .iter()
        .map(|x| ActivityItem::from_row(&x)).collect();

    // Get attachments
    for item in &mut items {
        let mut meta: HashMap<String, Option<Value>> = HashMap::new();
        if item.attachments.len() > 0 {
            for attach in &item.attachments {
                let item_type: String = attach.0.clone();

                match item_type.as_ref() {
                    "USER" => {
                        let item_id: i32 = attach.1.as_i64().unwrap() as i32;
                        let user: Option<User> = match &conn.query(&SQL_GET_USER.replace("{SCHEMA}", &tki), &[&item_id]) {
                            Ok(rows) => {
                                if let Some(ref row) = rows.iter().next() {
                                    Some(User::from_row(row))
                                } else {
                                    Option::None
                                }
                            }
                            Err(_) => Option::None
                        };
                        meta.insert(item_type.to_owned(), Some(json::to_value(user).unwrap()));
                    }
                    "REPORT" => {
                        let item_id: String = attach.1.as_str().unwrap().to_string();
                        let record: Option<Record> = match &conn.query(&SQL_GET_RECORD.replace("{SCHEMA}", &tki), &[&item_id]) {
                            Ok(rows) => {
                                if let Some(ref row) = rows.iter().next() {
                                    Some(Record::from_row(row))
                                } else {
                                    Option::None
                                }
                            }
                            Err(_) => Option::None
                        };

                        meta.insert(item_type.to_owned(), Some(json::to_value(record).unwrap()));
                    },
                    _ => {}
                }
            }
        }

        item.meta = meta;
    }

    let count: i64 = match conn.query(&SQL_GET_COUNT.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            if let Some(ref row) = rows.iter().next() {
                row.get(0)
            } else {
                0
            }
        },
        Err(_) => 0
    };

    let result = ActivityResult {
        activity: items,
        count,
    };

    let output: Value = json::to_value(result).unwrap();
    Ok(output)
}
