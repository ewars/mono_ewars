use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use uuid::Uuid;


use crate::api::forms::{FormSummary};

static SQL_GET_DISTINCT_FORMS: &'static str = r#"
    SELECT DISTINCT(a.fid) 
    FROM {SCHEMA}.assignments AS a
    WHERE a.uid = $1;
"#;

static SQL_GET_FORMS: &'static str = r#"
    SELECT r.uuid, r.name, r.status
    FROM {SCHEMA}.forms AS r
    WHERE r.uuid = ANY($1);
"#;


// Get forms that a user has access to
pub fn get_user_forms(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let assigned_forms: Vec<Uuid> = match &conn.query(&SQL_GET_DISTINCT_FORMS.replace("{SCHEMA}", &tki), &[&msg.user.uuid]) {
        Ok(rows) => {
            rows.iter().map(|x| x.get("fid")).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let forms: Vec<FormSummary> = match &conn.query(&SQL_GET_FORMS.replace("{SCHEMA}", &tki), &[&assigned_forms]) {
        Ok(rows) => {
            rows.iter().map(|x| FormSummary::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(forms).unwrap())
}
