use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};



#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct FormFeature {
    pub lti: Option<Uuid>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct RawAssignment {
    pub uuid: Uuid,
    pub lid: Option<Uuid>,
    pub fid: Uuid,
    pub assign_type: String,
    pub assign_group: Option<String>,
    pub features: HashMap<String, FormFeature>,
    pub location_name: Option<Value>,
    pub form_name: Value,
    pub location_status: Option<String>,
    pub form_status: String,
    pub assign_status: String,
    pub reporting_status: Option<String>,
    pub start_date: Option<NaiveDate>,
    pub end_date: Option<NaiveDate>,
}

impl<'a> From<Row<'a>> for RawAssignment {
    fn from(row: Row) -> Self {
        let features: HashMap<String, FormFeature> = json::from_value(row.get("features")).unwrap();
        RawAssignment {
            uuid: row.get("uuid"),
            lid: row.get("lid"),
            fid: row.get("fid"),
            assign_type: row.get("assign_type"),
            assign_group: row.get("assign_group"),
            features,
            location_name: row.get("location_name"),
            form_name: row.get("form_name"),
            location_status: row.get("location_status"),
            form_status: row.get("form_status"),
            assign_status: row.get("assign_status"),
            reporting_status: row.get("reporting_status"),
            start_date: row.get("start_date"),
            end_date: row.get("end_date"),
        }
    }
}

impl RawAssignment {
    pub fn has_location(&self) -> bool {
        if self.features.contains_key("LOCATION_REPORTING") {
            return true;
        } else {
            return false;
        }
    }

    pub fn has_interval(&self) -> bool {
        if self.features.contains_key("INTERVAL_REPORTING") {
            return true;
        } else {
            return false;
        }
    }
}
