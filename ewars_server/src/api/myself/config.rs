use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

static SQL_GET_CONFIG: &'static str = r#"
    SELECT key, value
    FROM {SCHEMA}.conf;
"#;

pub fn get_config(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let tki: String = msg.user.tki.clone();

    let mut result: HashMap<String, Value> = HashMap::new();

    let items: Vec<(String, Value)> = match &conn.query(&SQL_GET_CONFIG.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| (x.get(0), x.get(1))).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };


    for item in items {
        result.entry(item.0.clone())
            .and_modify(|f| *f = item.1.clone())
            .or_insert(item.1.clone());
    }


    Ok(json::to_value(result).unwrap())
}
