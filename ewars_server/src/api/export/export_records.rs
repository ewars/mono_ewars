use std::collections::HashMap;
use std::io;
use std::path::{Path, PathBuf};

use failure::Error;
use serde_json as json;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use uuid::Uuid;
use chrono::prelude::*;
use csv;


use crate::api::export::{Record, RecordsExportQuery};
use crate::models::{Form};


static SQL_EXPORT_RECORDS: &'static str = r#"
    COPY (
        SELECT 
            d.location_id,
            d.submitted_by,
            d.data
        FROM {SCHEMA}.records AS d
            LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = d.data->>'__lid__'
        WHERE d.fid = $1
            AND d.data->>'__dd__' >= $2
            AND d.data->>'__dd__' <= $3
            AND (l.lineage @> ARRAY[$4]::TEXT[] OR l.lineage IS NULL)
            AND d.status = 'SUBMITTED'
    ) TO STDOUT (FORMAT CSV, HEADER TRUE, FORCE_QUOTE *, DELIMETER ',')
"#;

static SQL_GET_LOC_TYPES: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.location_types;
"#;

static SQL_GET_FORM: &'static str = r#"
    SELECT uuid, name, status, description, version, guidance, eid_prefix, features, stages, definition, permissions, created, created_by, modified, modified_by
    FROM {SCHEMA}.forms
    WHERE uuid = $1;
"#;

static SQL_GET_LOCATIONS: &'static str = r#"
    SELECT uuid, name, lti
    FROM {SCHEMA}.locations;
"#;

static SQL_GET_RECORDS: &'static str = r#"
    SELECT
        r.uuid,
        r.data,
        r.submitted,
        r.submitted_by,
        r.modified,
        r.modified_by,
        r.stage,
        r.eid
    FROM {SCHEMA}.records AS r
"#;

// Export records from the system based on a specification
// Writes to a file on disk and returns the file path for the 
// user to download the file
pub fn export_records(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let qr: RecordsExportQuery = json::from_value(msg.args.clone()).unwrap();

    let form: Option<Form> = match &conn.query(&SQL_GET_FORM.replace("{SCHEMA}", &tki), &[&qr.form_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Form::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if form.is_none() {
        bail!("ERR_NO_FORM");
    }

    let mut headers: Vec<String> = vec![
        "uuid".to_string(),
        "location_uuid".to_string(),
        "location_name".to_string(),
        "location_type".to_string(),
        "pcode".to_string(),
        "isoweek".to_string(),
        "isoyear".to_string(),
        "submitted_date".to_string(),
        "submitter_name".to_string(),
        "submitter_email".to_string(),
        "eid".to_string()
    ];

    // Pull data from the form
    let mut fields: Vec<String> = Vec::new();
    if let Some(c) = form {
        fields = c.get_fields_as_vec_path();
    }

    for field in &fields {
        headers.push(field.clone());
    }

    // TODO: Need to sort these properly
    eprintln!("{:?}", headers);

    let mut joins: Vec<String> = Vec::new();
    let mut wheres: Vec<String> = Vec::new();
    let mut selects: Vec<String> = Vec::new();

    // Location field is specified
    let mut has_loc: bool = false;
    if let Some(c) = qr.loc_field {
        joins.push(format!("LEFT JOIN {{SCHEMA}}.locations AS l ON l.uuid::TEXT = r.data->>'{}'", c));
        if let Some(plid) = qr.lid {
            has_loc = true;
            wheres.push(format!("l.lineage::TEXT[] @> ARRAY['{}']::TEXT[]", plid.to_string()));
            selects.push("l.uuid AS location_uuid".to_string());
            selects.push("l.name AS location_name".to_string());
            selects.push("l.lti AS location_type".to_string());
            selects.push("l.status AS location_status".to_string());
            selects.push("l.pcode AS location_pcode".to_string());
        }
    }

    // Filter based on a date field
    if let Some(c) = qr.date_field {
        if let Some(start_date) = qr.start_date {
            wheres.push(format!("CAST(r.data->>'{}' AS DATE) >= '{}'", c, start_date));
        }

        if let Some(end_date) = qr.end_date {
            wheres.push(format!("CAST(r.data->>'{}' AS DATE) <= '{}'", c, end_date));
        }
    }

    let records: Vec<Record> = match &conn.query(&SQL_GET_RECORDS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Record::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut file_path: PathBuf = PathBuf::from(&msg.settings.server.files_path);
    let new_file_name: String = Uuid::new_v4().to_string();
    file_path.push(format!("{}.csv", new_file_name));
    let end_file = file_path.to_str().unwrap();
    let mut writer = match csv::Writer::from_path(end_file) {
        Ok(res) => res,
        Err(err) => {
            bail!(err);
        }
    };

    let _ = writer.write_record(&headers).unwrap();

    for record in records {
        let mut values: Vec<String> = Vec::new();
        values.push(record.uuid.to_string());

        if let Some(loc_uuid) = record.location_uuid {
            values.push(loc_uuid.to_string());
            values.push(record.location_name.unwrap());
            values.push("".to_string()); // Location Type
        } else {
            values.push("".to_string());
            values.push("".to_string());
            values.push("".to_string());
        }

        if let Some(pcode) = record.location_pcode {
            values.push(pcode);
        } else {
            values.push("".to_string());
        }

        values.push("".to_string()); // isoweek
        values.push("".to_string()); // isoyear
        values.push(record.submitted.to_string()); // Submitted
        values.push("".to_string()); // Submitter name
        values.push("".to_string()); // submitter email

        if let Some(eid) = record.eid {
            values.push(eid);
        } else {
            values.push("".to_string());
        }

        for field in &fields {
            if let Some(c) = record.data.get(&field.clone()) {
                values.push(c.to_string());
            } else {
                values.push("".to_string());
            }
        }
        //eprintln!("{:?}", values);

        let _ = writer.write_record(&values).unwrap();
    }



    let _ = writer.flush().unwrap();

    Ok(json::to_value(new_file_name).unwrap())
}
