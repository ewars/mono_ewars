use std::collections::HashMap;
use std::io;
use std::path::{PathBuf};

use failure::Error;
use serde_json as json;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use uuid::Uuid;
use chrono::prelude::*;
use csv;

use crate::api::export::{Alert, AlertsExportQuery};
use crate::models::{Form};

static SQL_GET_ALERTS: &'static str = r#"
    SELECT a.uuid,
        al.uuid AS alid,
        al.name AS alarm_name,
        a.eid,
        a.alert_date,
        a.status,
        a.stage,
        a.raised,
        a.data,
        a.closed,
        a.modified
    FROM {SCHEMA}.alerts AS a
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = a.alid
"#;

pub fn export_alerts(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: AlertsExportQuery = json::from_value(msg.args.clone()).unwrap();

    let headers: Vec<String> = vec![
        "uuid".to_string(),
        "alid".to_string(),
        "alarm_name".to_string(),
        "location_uuid".to_string(),
        "location_name".to_string(),
        "location_type".to_string(),
        "pcode".to_string(),
        "eid".to_string(),
        "alert_date".to_string(),
        "status".to_string(),
        "stage".to_string(),
        "raised".to_string(),
        "verification_outcome".to_string(),
        "verification_comments".to_string(),
        "risk".to_string(),
        "outcome".to_string(),
        "outcome_comments".to_string(),
        "exposure_assessment".to_string(),
        "context_assessment".to_string(),
        "hazard_assessment".to_string(),
        "closed".to_string(),
        "modified".to_string(),
    ];

    //TODO: Need to update to use the definition from the alarm at a later date

    // TODO: Need to sort these properly
    eprintln!("{:?}", headers);

    let mut joins: Vec<String> = Vec::new();
    let mut wheres: Vec<String> = Vec::new();
    let mut selects: Vec<String> = Vec::new();

    // Location field is specified
    let mut has_loc: bool = false;
    if let Some(c) = query.lid {
        joins.push("LEFT JOIN {{SCHENA}}.locations AS l ON l.uuid = a.lid".to_string());
        joins.push("LEFT JOIN {{SCHEMA}}.location_types AS lt ON lt.uuid = l.lti".to_string());
        has_loc = true;
        wheres.push(format!("l.lineage::TEXT[] @> ARRAY['{}']::TEXT[]", c.to_string()));
        selects.push("l.uuid AS location_uuid".to_string());
        selects.push("l.name->>'en' AS location_name".to_string());
        selects.push("l.lti AS location_type".to_string());
        selects.push("lt.name AS lti_name".to_string());
        selects.push("l.status AS location_status".to_string());
        selects.push("l.pcode AS location_pcode".to_string());
    }

    if let Some(c) = query.alid {
        wheres.push(format!("al.uuid = '{}'", c.to_string()));
    }

    let date_dim: String = query.date_dim.clone();
    let mut date_field: String = "a.alert_date".to_string();
    match date_dim.as_ref() {
        "MODIFIED" => {
            date_field = "a.modified".to_string();
        },
        "RAISED" => {
            date_field = "a.raised".to_string();
        },
        "ALERT_DATE" => {
            date_field = "a.alert_date".to_string();
        },
        _ => {}
    }

    wheres.push(format!("{} >= '{}'", date_field, query.start_date));
    wheres.push(format!("{} <= '{}'", date_field, query.end_date));

    let alerts: Vec<Alert> = match &conn.query(&SQL_GET_ALERTS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Alert::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut file_path: PathBuf = PathBuf::from(&msg.settings.server.files_path);
    let new_file_name: String = Uuid::new_v4().to_string();
    file_path.push(format!("{}.csv", new_file_name));
    let end_file = file_path.to_str().unwrap();
    let mut writer = match csv::Writer::from_path(end_file) {
        Ok(res) => res,
        Err(err) => {
            bail!(err);
        }
    };

    let _ = writer.write_record(&headers).unwrap();

    for record in alerts {
        let mut values: Vec<String> = Vec::new();
        values.push(record.uuid.to_string());
        values.push(record.alid.to_string());
        values.push(record.alarm_name);

        if let Some(loc_uuid) = record.lid {
            values.push(loc_uuid.to_string());
            values.push(record.location_name.unwrap());
            values.push("".to_string()); // Location Type
            values.push(record.location_pcode.unwrap_or("".to_string()));
        } else {
            values.push("".to_string());
            values.push("".to_string());
            values.push("".to_string());
            values.push("".to_string());
        }

        // Eid
        if let Some(eid) = record.eid {
            values.push(eid);
        } else {
            values.push("".to_string());
        }

        values.push(record.alert_date.unwrap_or("".to_string()));
        values.push(record.status);
        values.push(record.stage);
        values.push(record.raised.format("%Y-%m-%d").to_string());
        values.push(record.verification_outcome.unwrap_or("".to_string()));
        values.push(record.verification_comments.unwrap_or("".to_string()));
        values.push(record.risk.unwrap_or("".to_string()));
        values.push(record.outcome.unwrap_or("".to_string()));
        values.push(record.outcome_comments.unwrap_or("".to_string()));
        values.push(record.exposure_assessment.unwrap_or("".to_string()));
        values.push(record.context_assessment.unwrap_or("".to_string()));
        values.push(record.hazard_assessment.unwrap_or("".to_string()));

        if let Some(closed) = record.closed {
            values.push(closed.format("%Y-%m-%d").to_string());
        } else {
            values.push("".to_string());
        }

        if let Some(modified) = record.modified {
            values.push(modified.format("%Y-%m-%d").to_string());
        } else {
            values.push("".to_string());
        }

        let _ = writer.write_record(&values).unwrap();
    }

    let _ = writer.flush().unwrap();

    Ok(json::to_value(new_file_name).unwrap())

}
