use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use uuid::Uuid;
use chrono::prelude::*;

#[derive(Debug, Serialize)]
pub struct Record {
    pub uuid: Uuid,
    pub data: HashMap<String, Value>,
    pub submitted_by: Option<Uuid>,
    pub submitted: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub stage: Option<String>,
    pub eid: Option<String>,
    pub location_name: Option<String>,
    pub location_uuid: Option<Uuid>,
    pub location_status: Option<String>,
    pub location_pcode: Option<String>,
    pub location_path: Option<String>,
    pub location_type: Option<String>,
    pub submitter_name: Option<String>,
    pub submitter_email: Option<String>,
}

impl<'a> From<Row<'a>> for Record {
    fn from(row: Row) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();
        Record {
            uuid: row.get("uuid"),
            data,
            submitted_by: row.get("submitted_by"),
            submitted: row.get("submitted"),
            modified_by: row.get("modified_by"),
            modified: row.get("modified"),
            stage: row.get("stage"),
            eid: row.get("eid"),
            location_status: match row.get_opt("location_status") {
                Some(res) => res.unwrap(),
                None => Option::None
            },
            location_name: match row.get_opt("location_name") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            location_uuid: match row.get_opt("location_uuid") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            location_pcode: match row.get_opt("location_pcode") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            location_path: match row.get_opt("location_path") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            location_type: match row.get_opt("location_type") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            submitter_name: match row.get_opt("submitter_name") {
                Some(res) => res.unwrap(),
                None => Option::None,
            },
            submitter_email: match row.get_opt("submitter_email") {
                Some(res) => res.unwrap(),
                None => Option::None,
            }
        }
    }
}


#[derive(Debug, Deserialize)]
pub struct RecordsExportQuery {
    pub form_id: Uuid,
    pub loc_field: Option<String>,
    pub lid: Option<Uuid>,
    pub date_field: Option<String>,
    pub start_date: Option<String>,
    pub end_date: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Alert {
    pub uuid: Uuid,
    pub alid: Uuid,
    pub alarm_name: String,
    pub eid: Option<String>,
    pub alert_date: Option<String>,
    pub status: String,
    pub lid: Option<Uuid>,
    pub stage: String,
    pub raised: DateTime<Utc>,
    pub closed: Option<DateTime<Utc>>,
    pub modified: Option<DateTime<Utc>>,
    pub verification_outcome: Option<String>,
    pub verification_comments: Option<String>,
    pub risk: Option<String>,
    pub outcome: Option<String>,
    pub outcome_comments: Option<String>,
    pub exposure_assessment: Option<String>,
    pub context_assessment: Option<String>,
    pub hazard_assessment: Option<String>,
    pub location_name: Option<String>,
    pub location_type: Option<HashMap<String, String>>,
    pub location_pcode: Option<String>,
}

impl<'a> From<Row<'a>> for Alert {
    fn from(row: Row) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();

        let risk: Option<String> = match data.get("risk") {
            Some(res) => {
                let value: (String, (i32, i32)) = json::from_value(res.clone()).unwrap();
                Some(value.0)
            },
            None => Option::None,
        };

        let outcome: Option<String> = match data.get("outcome") {
            Some(res) => Some(res.to_string()),
            None => Option::None
        };

        let verification_outcome: Option<String> = match data.get("verification_outcome") {
            Some(res) => Some(res.to_string()),
            None => Option::None
        };

        let verification_comments: Option<String> = match data.get("verification_comments") {
            Some(res) => Some(res.to_string()),
            None => Option::None
        };

        let outcome_comments: Option<String> = match data.get("outcome_comments") {
            Some(res) => Some(res.to_string()),
            None => Option::None,
        };

        let exposure_assessment: Option<String> = match data.get("exposure_assessment") {
            Some(res) => Some(res.to_string()),
            None => Option::None,
        };

        let context_assessment: Option<String> = match data.get("context_assessment") {
            Some(res) => Some(res.to_string()),
            None => Option::None,
        };

        let hazard_assessment: Option<String> = match data.get("hazard_assessment") {
            Some(res) => Some(res.to_string()),
            None => Option::None,
        };

        let location_name: Option<String> = match row.get_opt("location_name") {
            Some(res) => Some(res.unwrap()),
            None => Option::None,
        };

        let location_type: Option<HashMap<String, String>> = match row.get_opt("location_type") {
            Some(res) => {
                Some(json::from_value(res.unwrap()).unwrap())
            },
            None => Option::None
        };

        Alert {
            uuid: row.get("uuid"),
            alid: row.get("alid"),
            alarm_name: row.get("alarm_name"),
            eid: row.get("eid"),
            alert_date: row.get("alert_date"),
            status: row.get("status"),
            lid: row.get("lid"),
            stage: row.get("stage"),
            raised: row.get("raised"),
            closed: row.get("closed"),
            modified: row.get("modified"),
            verification_outcome,
            verification_comments,
            risk,
            outcome,
            outcome_comments,
            exposure_assessment,
            context_assessment,
            hazard_assessment,
            location_name,
            location_type,
            location_pcode: match row.get_opt("location_pcode") {
                Some(res) => Some(res.unwrap()),
                None => Option::None
            }
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct AlertsExportQuery {
    pub alid: Option<Uuid>,
    pub lid: Option<Uuid>,
    pub date_dim: String,
    pub start_date: String,
    pub end_date: String,
}

#[derive(Debug, Deserialize)]
pub struct ExportIndicatorsQuery {
    pub inds: Vec<Uuid>,
    pub lids: Vec<Uuid>,
    pub start_date: String,
    pub end_date: String,
    pub transpose: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocationRecord {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub pcode: Option<String>,
    pub codes: HashMap<String, String>,
    pub pid: Option<Uuid>,
    pub geometry_type: Option<String>,
    pub lti: Uuid,
    pub status: String,
    pub lti_name: HashMap<String, String>,
    pub lineage: Vec<String>,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
}

impl<'a> From<Row<'a>> for LocationRecord {
    fn from(row: Row) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        let codes: HashMap<String, String> = json::from_value(row.get("codes")).unwrap();
        let lti_name: HashMap<String, String> = json::from_value(row.get("lti_name")).unwrap();
        LocationRecord {
            uuid: row.get("uuid"),
            name,
            pcode: row.get("pcode"),
            codes,
            pid: row.get("pid"),
            geometry_type: row.get("geometry_type"),
            lti: row.get("lti"),
            status: row.get("status"),
            lti_name,
            lineage: row.get("lineage"),
            created: row.get("created"),
            modified: row.get("modified"),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct LocationsExportQuery {
    pub lid: Uuid,
    pub lti: Option<Uuid>,
    pub status: Option<String>,
}

