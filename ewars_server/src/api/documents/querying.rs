use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use crate::models::{UserSession};
use crate::share::{Query, QueryResult};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::utils::*;

use crate::api::documents::{Document};

static SQL_QUERY_DOCUMENTS: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.documents AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_DOCUMENTS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.documents AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_GET_DOCUMENT: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.documents AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn query_documents(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_DOCUMENTS, SQL_COUNT_DOCUMENTS, tki, Document)
}

static SQL_GET_ACTIVE_DOCUMENTS: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.documents AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.status = 'ACTIVE';
"#;

pub fn get_active_documents(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let results: Vec<Document> = match &conn.query(&SQL_GET_ACTIVE_DOCUMENTS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Document::from(x)).collect()
        },
        Err(err) => {
            Vec::new()
        }
    };

    Ok(json::to_value(results).unwrap())
}

static SQL_GET_ADMIN_DOCUMENTS: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.documents AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.status = ANY(ARRAY['ACTIVE', 'DRAFT']);
"#;

pub fn get_available_documents(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let results: Vec<Document> = match &conn.query(&SQL_GET_ADMIN_DOCUMENTS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| Document::from(x)).collect()
        },
        Err(err) => {
            Vec::new()
        }
    };

    Ok(json::to_value(results).unwrap())
}

pub fn get_document(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Document> = match &conn.query(&SQL_GET_DOCUMENT.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Document::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}
