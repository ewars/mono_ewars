use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::models::{UserSession};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::documents::{Document, DocumentCreate, DocumentUpdate};

static SQL_DELETE_DOCUMENT: &'static str = r#"
    DELETE FROM {SCHEMA}.documents WHERE uuid = $1;
"#;

// Delete
pub fn delete_document(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_DELETE_DOCUMENT.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_UPDATE_DOCUMENT: &'static str = r#"
    UPDATE {SCHEMA}.documents
        SET template_name = $1,
            instance_name = $2,
            status = $3,
            theme = $4,
            layout = %5,
            content = $6,
            data = $7,
            generation = $8,
            orientation = $9,
            template_type = $10,
            permissions = $11,
            modified_by = $12
    WHERE uuid = $13;
"#;

pub fn update_document(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let update: DocumentUpdate = json::from_value(msg.args.clone()).unwrap();

    match &conn.execute(&SQL_UPDATE_DOCUMENT.replace("{SCHEMA}", &tki), &[
        &update.template_name,
        &update.instance_name,
        &update.status,
        &update.theme,
        &update.layout,
        &update.content,
        &update.data,
        &update.generation,
        &update.orientation,
        &update.template_type,
        &update.permissions,
        &msg.user.uuid,
        &update.uuid,
    ]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Ok(json::to_value(false).unwrap())
        }
    }
}

static SQL_CREATE_DOCUMENT: &'static str = r#"
    INSERT INTO {SCHEMA}.documents
    (template_name, instance_name, status, version, theme, layout, content, data, generation, orientation, template_type, permissions, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING *;
"#;

// Create a new form in the system
pub fn create_document(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let new: DocumentCreate = json::from_value(msg.args.clone()).unwrap();

    let result: Value = match &conn.query(&SQL_CREATE_DOCUMENT.replace("{SCHEMA}", &tki), &[
        &new.template_name,
        &new.instance_name,
        &new.status,
        &new.version,
        &new.theme,
        &new.layout,
        &new.content,
        &new.data,
        &new.generation,
        &new.orientation,
        &new.template_type,
        &new.permissions,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                json::to_value(Document::from(row)).unwrap()
            } else {
                json::to_value(false).unwrap()
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            json::to_value(false).unwrap()
        }
    };

    Ok(result)
}

