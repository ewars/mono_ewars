use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct Document {
    pub uuid: Uuid,
    pub template_name: Value,
    pub instance_name: Value,
    pub status: String,
    pub version: i32,
    pub theme: String,
    pub layout: Option<Value>,
    pub content: Option<String>,
    pub data: Option<Value>,
    pub generation: Option<Value>,
    pub orientation: String,
    pub template_type: String,
    pub permissions: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub creator_name: Option<String>,
    pub creator_email: Option<String>,
    pub modifier_name: Option<String>,
    pub modifier_email: Option<String>,
}

impl<'a> From<Row<'a>> for Document {
    fn from(row: Row) -> Self {
        Document {
            uuid: row.get("uuid"),
            template_name: row.get("template_name"),
            instance_name: row.get("instance_name"),
            status: row.get("status"),
            version: row.get("version"),
            theme: row.get("theme"),
            layout: row.get("layout"),
            content: row.get("content"),
            data: row.get("data"),
            generation: row.get("generation"),
            orientation: row.get("orientation"),
            template_type: row.get("template_type"),
            permissions: row.get("permissions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator_name: row.get("creator_name"),
            creator_email: row.get("creator_email"),
            modifier_name: row.get("modifier_name"),
            modifier_email: row.get("modifier_email"),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct DocumentUpdate {
    pub uuid: Uuid,
    pub template_name: Value,
    pub instance_name: Value,
    pub status: String,
    pub version: Option<i32>,
    pub theme: String,
    pub layout: Option<Value>,
    pub content: Option<String>,
    pub data: Option<Value>,
    pub generation: Option<Value>,
    pub orientation: String,
    pub template_type: String,
    pub permissions: Option<Value>,
}


#[derive(Debug, Deserialize)]
pub struct DocumentCreate {
    pub template_name: Value,
    pub instance_name: Value,
    pub status: String,
    pub version: Option<i32>,
    pub theme: String,
    pub layout: Option<Value>,
    pub content: Option<String>,
    pub data: Option<Value>,
    pub generation: Option<Value>,
    pub orientation: String,
    pub template_type: String,
    pub permissions: Option<Value>,
}
