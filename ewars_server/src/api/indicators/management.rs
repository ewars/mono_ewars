use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::api::indicators::{Indicator, IndicatorUpdate, IndicatorCreate};

static SQL_CREATE_INDICATOR: &'static str = r#"
    INSERT INTO {SCHEMA}.indicators
    (group_id, name, status, description, itype, icode, permissions, definition, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *;
"#;

pub fn create_indicator(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: IndicatorCreate = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Indicator> = match conn.query(&SQL_CREATE_INDICATOR.replace("{SCHEMA}", &tki), &[
        &data.group_id,
        &data.name,
        &data.status,
        &data.description,
        &data.itype,
        &data.icode,
        &data.permissions,
        &data.definition,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Indicator::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    match result {
        Some(res) => {
            Ok(json::to_value(res).unwrap())
        },
        None => {
            bail!("ERR_CREATE_INDICATOR");
        }
    }


}

static SQL_UPDATE_INDICATOR: &'static str = r#"
    UPDATE {SCHEMA}.indicators
        SET group_id = $1,
            name = $2,
            status = $3,
            description = $4,
            itype = $5,
            icode = $6,
            permissions = $7,
            definition = $8,
            modified_by = $9,
            modified = NOW()
    WHERE uuid = $10;
"#;


pub fn update_indicator(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let update: IndicatorUpdate = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_UPDATE_INDICATOR.replace("{SCHEMA}", &tki), &[
        &update.group_id,
        &update.name,
        &update.status,
        &update.description,
        &update.itype,
        &update.icode,
        &update.permissions,
        &update.definition,
        &msg.user.uuid,
        &update.uuid,
    ]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_DELETE_INDICATOR: &'static str = r#"
    DELETE FROM {SCHEMA}.indicators WHERE uuid = $1;
"#;

pub fn delete_indicator(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_DELETE_INDICATOR.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
       },
       Err(err) => {
           bail!(err);
       }
    }
}
