use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use chrono::prelude::*;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditData {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

#[derive(Debug, Serialize, Clone)]
pub struct Indicator {
    pub uuid: Uuid,
    pub group_id: Option<Uuid>,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub itype: Option<String>,
    pub icode: Option<String>,
    pub permissions: Option<Value>,
    pub definition: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    #[serde(flatten)]
    pub audit_data: AuditData,
}


impl<'a> From<Row<'a>> for Indicator {
    fn from(row: Row) -> Self {
        Indicator {
            uuid: row.get("uuid"),
            group_id: row.get("group_id"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            itype: row.get("itype"),
            icode: row.get("icode"),
            permissions: row.get("permissions"),
            definition: row.get("definition"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            audit_data: AuditData {
                creator: match row.get_opt("creator") {
                    Some(res) => res.unwrap(),
                    None => Option::None,
                },
                modifier: match row.get_opt("modifier") {
                    Some(res) => res.unwrap(),
                    None => Option::None
                }
            }
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct IndicatorUpdate {
    pub uuid: Uuid,
    pub group_id: Uuid,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub itype: Option<String>,
    pub icode: Option<String>,
    pub permissions: Option<Value>,
    pub definition: Option<Value>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct IndicatorCreate {
    pub group_id: Uuid,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub itype: Option<String>,
    pub icode: Option<String>,
    pub permissions: Option<Value>,
    pub definition: Option<Value>,
}

