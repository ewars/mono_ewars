use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};

use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

#[derive(Debug, Clone)]
pub struct InboundForm {
    pub uuid: Uuid,
    pub name: Value,
    pub etl: Option<HashMap<String, Value>>,
}

impl<'a> From<Row<'a>> for InboundForm {
    fn from(row: Row) -> Self {
        let etl: Option<HashMap<String, Value>> = json::from_value(row.get(2)).unwrap();
        InboundForm {
            uuid: row.get(0),
            name: row.get(1),
            etl,
        }
    }
}

static SQL_GET_INBOUND: &'static str = r#"
    SELECT f.uuid, f.name, f.etl
    FROM {SCHEMA}.forms AS f
    WHERE f.status = 'ACTIVE';
"#;

// Get fields inbound from the form
pub fn get_inbound(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let ind_id: Uuid = json::from_value(msg.args.clone()).unwrap();
    let ind_s: String = ind_id.to_string();

    let items: Vec<InboundForm> = match &conn.query(&SQL_GET_INBOUND.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| InboundForm::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut results: Vec<(Uuid, Value)> = Vec::new();
    eprintln!("{}", ind_s);
    eprintln!("{:?}", items);

    for item in items {
        if let Some(etl) = item.etl {
            if etl.contains_key(&ind_s) {
                results.push((item.uuid.clone(), item.name.clone()));
            }
        }
    }

    Ok(json::to_value(results).unwrap())
}
