use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::models::{Draft};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

#[derive(Debug, Deserialize, Clone)]
pub struct NewDraft {
    pub fid: Uuid,
    pub data: Option<Value>,
}

static SQL_CREATE_DRAFT: &'static str = r#"
    INSERT INTO {SCHEMA}.drafts
    (fid, data, created_by, modified_by)
    VALUES ($1, $2, $3, $4) RETURNING *;
"#;

pub fn create_draft(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: NewDraft = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Draft> = match &conn.query(&SQL_CREATE_DRAFT.replace("{SCHEMA}", &tki), &[
        &data.fid,
        &data.data,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Draft::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };


    if result.is_some() {
        bail!("ERR");
    } else {
        Ok(json::to_value(result).unwrap())
    }
}

static SQL_DELETE_DRAFT: &'static str = r#"
    DELETE FROM {SCHEMA}.drafts WHERE uuid = $1;
"#;

pub fn delete_draft(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_DELETE_DRAFT.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_UPDATE_DRAFT: &'static str = r#"
    UPDATE {SCHEMA}.drafts
    SET data = $1,
        modified = NOW()
    WHERE uuid = $2
    RETURNING *;
"#;

#[derive(Debug, Deserialize)]
pub struct DraftUpdate {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub data: Value,
}

pub fn update_draft(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: DraftUpdate = json::from_value(msg.args.clone()).unwrap();

    match conn.query(&SQL_UPDATE_DRAFT.replace("{SCHEMA}", &tki), &[
        &data.data,
        &data.uuid,
    ]) {
        Ok(rows) => {
            if let Some(c) = rows.iter().next() {
                Ok(json::to_value(Draft::from(c)).unwrap())
            } else {
                bail!("ERR");
            }
        },
        Err(err) => {
            bail!(err);
        }
    }
}
