use serde_json as json;
use serde_json::Value;

use failure::Error;

use uuid::Uuid;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

static SQL_CREATE_INVITE: &'static str = r#"

"#;

pub fn invite_user(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}

static SQL_CREATE_SYSTEM_USER: &'static str = r#"
    INSERT INTO core.users
    (name, email, accounts, status, created_by, modified_by, system)
    VALUES ($1, $2, ARRAY[$3], 'ACTIVE', $4, $5, TRUE) RETURNING uuid;
"#;

static SQL_CREATE_LOCAL_USER: &'static str = r#"
    INSERT INTO {SCHEMA}.accounts
    (uuid, role, status, org_id, created_by, modified_by)
    VALUES ($1, $2, 'ACTIVE', $3, $4, $5);
"#;

static SQL_CHECK_USER: &'static str = r#"
    SELECT uuid FROM core.users
    WHERE email = $1;
"#;

#[derive(Debug, Deserialize)]
pub struct NewUser {
    pub name: String,
    pub email: String,
    pub password: String,
    pub status: String,
    pub role: String,
    pub org_id: Uuid,
}

pub fn create_system_user(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let new_user: NewUser = json::from_value(msg.args.clone()).unwrap();

    let mut email: String = new_user.email.clone();
    email = email.to_lowercase();

    let exists: Option<Uuid> = match &conn.query(&SQL_CHECK_USER.replace("{SCHEMA}", &tki), &[&new_user.email]) {
        Ok(rows) => {
            if let Some(c) = rows.iter().next() {
                Some(c.get(0))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    // A user with this email address already exists
    if exists.is_some() {
        bail!("ERR_USER_EXISTS");
    }

    let new_uuid: Option<Uuid> = match &conn.query(&SQL_CREATE_SYSTEM_USER.replace("{SCHEMA}", &tki), &[
        &new_user.name,
        &new_user.email,
        &msg.user.aid,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(c) = rows.iter().next() {
                Some(c.get(0))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(c) = new_uuid {
        match conn.execute(&SQL_CREATE_LOCAL_USER.replace("{SCHEMA}", &tki), &[
            &c,
            &new_user.role,
            &new_user.org_id,
            &msg.user.uuid,
            &msg.user.uuid,
        ]) {
            Ok(_) => {
                Ok(json::to_value(true).unwrap())
            },
            Err(err) => {
                bail!(err);
            }
        }
    } else {
        bail!("ERR_CREATING_USER");
    }
}

static SQL_DELETE_INVITE: &'static str = r#"
    DELETE FROM {SCHEMA}.invites
    WHERE uuid = $1;
"#;

pub fn delete_invite(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_DELETE_INVITE.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_UPDATE_USER: &'static str = r#"
    UPDATE {SCHEMA}.accounts
    SET role = $1,
        status = $2,
        permissions = $3,
        org_id = $4
    WHERE uuid = $5;
"#;

#[derive(Debug, Deserialize)]
pub struct UserUpdate {
    pub uuid: Uuid,
    pub role: String,
    pub status: String,
    pub permissions: Option<Value>,
    pub org_id: Option<Uuid>,
}

pub fn update_user(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: UserUpdate = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_UPDATE_USER.replace("{SCHEMA}", &tki), &[
        &query.role,
        &query.status,
        &query.permissions,
        &query.org_id,
        &query.uuid,
    ]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_REVOKE_USER: &'static str = r#"
    UPDATE {SCHEMA}.accounts
    SET status = 'REVOKED'
    WHERE uuid = $1;
"#;

pub fn revoke_access(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_REVOKE_USER.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}
