use serde_json as json;
use serde_json::Value;

use failure::Error;

use postgres::rows::{Row};
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::api::utils::*;
use crate::models::{Assignment};

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT r.*,
        u.name AS user_name,
        u.email AS user_email,
        f.name AS form_name,
        l.name AS location_name,
        (uu.name || ' ' || uu.email) AS creator,
        (uc.name || ' ' || uc.email) AS modifier
    FROM {SCHEMA}.assignments AS r
        LEFT JOIN {SCHEMA}.users AS u ON u.uuid = r.uid
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.users AS uu on uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uid = $1;
"#;

pub fn get_assignments(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let assigns: Vec<Assignment> = get_vec_from_args!(conn, SQL_GET_ASSIGNMENTS, Assignment, tki, &[&id]);


    Ok(json::to_value(assigns).unwrap())
}
