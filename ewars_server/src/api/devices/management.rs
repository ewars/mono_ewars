use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::models::{UserSession};

use crate::api::devices::{Device, DeviceUpdate};

static SQL_DELETE_DEVICE: &'static str = r#"
    DELETE FROM {SCHEMA}.devices WHERE uuid = $1;
"#;

pub fn delete_device(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match &conn.execute(&SQL_DELETE_DEVICE.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Ok(json::to_value(false).unwrap())
        }
    }
}

static SQL_UPDATE_DEVICE: &'static str = r#"
    UPDATE {SCHEMA}.devices
        SET info = $1,
            modified_by = $2,
            blocked = $3
    WHERE uuid = $4;
"#;

pub fn update_device(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let update: DeviceUpdate = json::from_value(msg.args.clone()).unwrap();

    match &conn.query(&SQL_UPDATE_DEVICE.replace("{SCHEMA}", &tki), &[
        &update.info,
        &msg.user.uuid,
        &update.blocked,
        &update.uuid,
    ]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Ok(json::to_value(false).unwrap())
        }
    }
}

static SQL_CREATE_DEVICE: &'static str = r#"
    INSERT INTO {SCHEMA}.devices
    (device_type, uid, info, created_by, modified_by, blocked)
    VALUES ($1, $2, $3, $4, $5, $6) RETURNING *;
"#;

pub fn create_device(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let new: DeviceUpdate = json::from_value(msg.args.clone()).unwrap();

    let result: Value = match &conn.query(&SQL_CREATE_DEVICE.replace("{SCHEMA}", &tki), &[
        &new.device_type,
        &new.uid,
        &new.info,
        &msg.user.uuid,
        &msg.user.uuid,
        &new.blocked,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                json::to_value(Device::from(row)).unwrap()
            } else {
                json::to_value(true).unwrap()
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            json::to_value(false).unwrap()
        }
    };

    Ok(result)
}

static SQL_REVOKE_DEVICE_ACCESS: &'static str = r#"
    UPDATE {SCHEMA}.devices
        SET blocked = TRUE
    WHERE uuid = $1;
"#;

pub fn revoke_device(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match &conn.execute(&SQL_REVOKE_DEVICE_ACCESS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            Ok(json::to_value(false).unwrap())
        }
    }

}

