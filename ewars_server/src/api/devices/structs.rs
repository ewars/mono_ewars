use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct Device {
    pub uuid: Uuid,
    pub device_type: String,
    pub last_seed: DateTime<Utc>,
    pub did: Option<String>,
    pub uid: Option<Uuid>,
    pub info: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub blocked: bool,
}

impl<'a> From<Row<'a>> for Device {
    fn from(row: Row) -> Self {
        Device {
            uuid: row.get("uuid"),
            device_type: row.get("device_type"),
            last_seed: row.get("last_seed"),
            did: row.get("did"),
            uid: row.get("uid"),
            info: row.get("info"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            blocked: row.get("blocked"),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct DeviceUpdate {
    pub uuid: Uuid,
    pub device_type: String,
    pub did: Option<String>,
    pub uid: Option<Uuid>,
    pub info: Option<Value>,
    pub blocked: bool,
}

#[derive(Debug, Deserialize)]
pub struct DeviceCreate {
    pub device_type: String,
    pub did: Option<String>,
    pub uid: Option<Uuid>,
    pub info: Option<Value>,
    pub blocked: bool,
}
