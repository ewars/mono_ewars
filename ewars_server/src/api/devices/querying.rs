use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::share::{Query, QueryResult};

use crate::api::devices::{Device};

use crate::api::utils::*;

static SQL_QUERY_DEVICES: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.devices AS r
"#;

static SQL_COUNT_DEVICES: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.devices AS r
"#;

pub fn query_devices(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_DEVICES, SQL_COUNT_DEVICES, tki, Device)
}

pub fn get_device(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}
