use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use postgres::rows::{Row};
use uuid::Uuid;

#[derive(Debug, Serialize, Clone)]
pub struct Invite {
    pub uuid: Uuid,
    pub email: String,
    pub user_id: Option<Uuid>,
    pub details: Option<Value>,
    pub send: DateTime<Utc>,
    pub expires: DateTime<Utc>
}

impl<'a> From<Row<'a>> for Invite {
    fn from(row: Row) -> Self {
        Invite {
            uuid: row.get("uuid"),
            email: row.get("email"),
            user_id: row.get("user_id"),
            details: row.get("details"),
            send: row.get("send"),
            expires: row.get("expires"),
        }
    }
}

