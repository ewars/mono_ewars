use std::collections::HashMap;

use failure::Error;

use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use chrono::prelude::*;
use uuid::Uuid;
use tera::{Tera};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::app::{Org, VerificationTaskData};
use crate::models::{Task, Organization};
use crate::services::{EmailService, Email};

lazy_static! {
    pub static ref TERA: Tera = {
        let mut tera = compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*"));
        tera
    };
}

#[derive(Debug, Deserialize, Clone)]
pub struct AccessRejection {
    pub uuid: Uuid,
    pub reason: String,
}

// Process and access rejection
pub fn reject_access_request(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AccessRejection = json::from_value(msg.args.clone()).unwrap();

    let mut outcome: HashMap<&str, &str> = HashMap::new();
    outcome.insert("outcome", "REJECTED");
    outcome.insert("reaszon", &data.reason);
    let outcome: Value = json::to_value(outcome).unwrap();

    let result: bool = Task::close_task(&conn, &tki, &data.uuid, &outcome, &msg.user.uuid);

    let task: Task = match Task::get_task_by_id(&conn, &tki, &data.uuid) {
        Some(res) => res,
        None => {
            bail!("ERR_NO_TASK");
        }
    };

    if !result {
        bail!("ERR_CLOSING_TASK");
    }

    let task_data: VerificationTaskData = json::from_value(task.data).unwrap();



    // Send email to use to let them know they've been rejected

    let mut ctx = tera::Context::new();
    ctx.insert("account_name", &msg.user.account_name);
    ctx.insert("reason", &data.reason);
    ctx.insert("email", &task_data.email);
    ctx.insert("name", &task_data.name);
    ctx.insert("subject", &format!("[EWARS] Access requeste rejected"));

    let mut email_content: Option<String> = Option::None;
    let email_content: String = match TERA.render("emails/access_rejected.html", &ctx) {
        Ok(res) => res.to_string(),
        Err(err) => {
            bail!(format!("{:?}", err));
        }
    };

    msg.email_svc.do_send(Email {
        recipients: vec![task_data.email.clone()],
        account_name: Some(msg.user.account_name.clone()),
        subject: format!("[EWARS] Access request for {} has been rejected", &msg.user.account_name),
        plain_content: Option::None,
        html_content: email_content,
        system: true,
    });


    Ok(json::to_value(true).unwrap())
}

#[derive(Debug, Deserialize, Clone)]
pub struct AccessApproval {
    pub uuid: Uuid,
    pub role: String,
    pub org_name: Option<String>,
    pub org_id: Option<Org>,
}

impl AccessApproval {
    pub fn get_org_id(&self) -> Option<Uuid> {
        if let Some(org) = &self.org_id {
            match &org {
                Org::Id(c) => {
                    Some(c.clone())
                },
                _ => Option::None
            }
        } else {
            Option::None
        }
    }
}

static SQL_INSERT_USER: &'static str = r#"
    INSERT INTO {SCHEMA}.accounts
    (uuid, role, status, org_id, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6);
"#;

static SQL_APPEND_ACCOUNT: &'static str = r#"
    UPDATE core.users
    SET accounts = accounts || $1
    WHERE uuid = $2;
"#;

pub fn approve_access_request(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AccessApproval = json::from_value(msg.args.clone()).unwrap();

    let mut outcome: HashMap<&str, &str> = HashMap::new();
    outcome.insert("outcome", "APPROVED");
    let outcome: Value = json::to_value(outcome).unwrap();

    let task: Task = match Task::get_task_by_id(&conn, &tki, &data.uuid) {
        Some(res) => res,
        None => {
            bail!("ERR_NO_TASK");
        }
    };
    let task_data: VerificationTaskData = json::from_value(task.data).unwrap();
    let result: bool = Task::close_task(&conn, &tki, &data.uuid, &outcome, &msg.user.uuid);

    if !result {
        bail!("ERR_CLOSING_TASK");
    }

    // Create the new orgnaization if it's required
    let mut org_id: Option<Uuid> = data.get_org_id();
    if org_id.is_none() {
        if let Some(c) = data.org_name {
            let org: Option<Organization> = Organization::create_new(&conn, &tki, &c, &"".to_string(), &msg.user.uuid);
            if let Some(o) = org {
                org_id = Some(o.uuid.clone());
            }
       }
    }

    // Add account to core users accounts columns
    let new_account: Vec<Uuid> = vec![msg.user.aid.clone()];
    let result: bool = match &conn.execute(&SQL_APPEND_ACCOUNT.replace("{SCHEMA}", &tki), &[&new_account, &task_data.uid]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if !result {
        bail!("ERR_ADDING_ACCOUNT");
    }

    let result: bool = match &conn.execute(&SQL_INSERT_USER.replace("{SCHEMA}", &tki), &[
        &task_data.uid,
        &data.role,
        &"ACTIVE",
        &org_id,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(_) => true,
        Err(err) => false
    };

    if !result {
        bail!("ERR_GRANTING_ACCESS");
    }

    let mut ctx = tera::Context::new();
    ctx.insert("account_name", &msg.user.account_name);
    ctx.insert("email", &task_data.email);
    ctx.insert("name", &task_data.name);
    ctx.insert("subject", &format!("[EWARS] Access request approved"));

    let email_content: String = match TERA.render("emails/access_approved.html", &ctx) {
        Ok(res) => res.to_string(),
        Err(err) => {
            bail!(format!("{:?}", err));
        }
    };

    msg.email_svc.do_send(Email {
        recipients: vec![task_data.email.clone()],
        account_name: Some(msg.user.account_name.clone()),
        subject: format!("[EWARS] Account access approved - {}", &msg.user.account_name),
        plain_content: Option::None,
        html_content: email_content,
        system: true,
    });

    Ok(json::to_value(true).unwrap())
}
