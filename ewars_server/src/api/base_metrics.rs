use serde_json as json;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

static SQL_GET_ACTIVE_FORM_COUNT: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.forms
    WHERE status = 'ACTIVE';
"#;

static SQL_SUBMISSIONS: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.collections AS c
        LEFT JOIN {SCHEMA}.forms AS f ON f.id = c.form_id
    WHERE c.status = 'SUBMITTED'
        AND f.status = 'ACTIVE';
"#;

static SQL_USERS: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.users
    WHERE status = 'ACTIVE';
"#;

static SQL_LOCATIONS: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.locations
    WHERE status = 'ACTIVE';
"#;

static SQL_ALERTS_OPEN: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.alerts
    WHERE state = 'OPEN';
"#;

static SQL_ALERTS_CLOSED: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.alerts
    WHERE state = 'CLOSED';
"#;

static SQL_ALERTS: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.alerts;
"#;

static SQL_DEVICES: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.devices;
"#;

static SQL_PARTNERS: &'static str = r#"
    SELECT COUNT(org_id)
    FROM {SCHEMA}.users
    WHERE status = 'ACTIVE'
    GROUP BY org_id;
"#;

static SQL_ALARMS: &'static str = r#"
    SELECT COUNT(*)
    FROM {SCHEMA}.alamrms_v2
    WHERE status = 'ACTIVE';
"#;

pub fn get_metric(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Value> {
    let metric_name: String = msg.args.get(0).unwrap().as_str().unwrap().to_string();

    let result: i64 = match metric_name.as_ref() {
        "ASSIGNMENTS" => 0,
        "REPORTING_LOCATIONS" => {
            match &conn.query(&SQL_LOCATIONS.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    }
                },
                Err(_) => 0
            }
        },
        "LOCATIONS" => {
            match &conn.query(&SQL_LOCATIONS.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    }
                },
                Err(_) => 0
            }
        },
        "USERS" => {
            match &conn.query(&SQL_USERS.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    }
                },
                Err(_) => 0
            }
        },
        "SUBMISSIONS" => {
            match &conn.query(&SQL_SUBMISSIONS.replace("{SCHEMA}", &tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    }
                },
                Err(_) => 0
            }
        },
        _ => 0
    };

    Ok(json::to_value(result).unwrap())
}
