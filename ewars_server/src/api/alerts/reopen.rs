use serde_json as json;
use serde_json::Value;

use failure::Error;
use postgres::rows::{Row};
use uuid::Uuid;
use chrono::prelude::*;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::alerts::{Alert, AlertEvent};

#[derive(Debug, Deserialize, Clone)]
pub struct AlertOpen {
    pub uuid: Uuid,
    pub reason: String,
}

static SQL_REOPEN_ALERT: &'static str = r#"
    UPDATE {SCHEMA}.alerts 
    SET status = 'OPEN',
        stage = 'VERIFICATION',
        events = $1
    WHERE uuid = $2;
"#;

static SQL_GET_ALERT: &'static str = r#"
    SELECT r.*,
        al.name AS alarm_name,
        l.name AS location_name,
                          (SELECT ARRAY(SELECT xx.name->>'en' FROM {SCHEMA}.locations AS xx WHERE xx.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name,
        lti.name AS lti_name,
        uu.name AS modifier_name,
        uu.email AS modifier_email,
        al.definition as alarm_definition,
        al.description AS alarm_description
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = r.alid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn reopen_alert(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AlertOpen = json::from_value(msg.args.clone()).unwrap();

    let alert: Option<Alert> = match &conn.query(&SQL_GET_ALERT.replace("{SCHEMA}", &tki), &[&data.uuid]) {
        Ok(rows) => {
            if let Some(c) = rows.iter().next() {
                Some(Alert::from(c))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if alert.is_none() {
        bail!("ERR_UNK_ALERT");
    }
    let alert: Alert = alert.unwrap();

    let mut events: Vec<AlertEvent> = Vec::new();
    if let Some(c) = alert.events {
        events = c.clone();
    }
    events.push(AlertEvent {
        uuid: Uuid::new_v4(),
        event_type: "REOPEN".to_string(),
        uid: Some(msg.user.uuid.clone()),
        transition: Some(vec![alert.stage.clone(), "VERIFICATION".to_string()]),
        reason: Some(data.reason.clone()),
        created: Utc::now(),
        comment: Option::None,
    });
    let events_val: Value = json::to_value(events).unwrap();

    let result: bool = match &conn.execute(&SQL_REOPEN_ALERT.replace("{SCHEMA}", &tki), &[
        &events_val,
        &data.uuid,
    ]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("ERR_REOPENING_ALERT");
    }
}
