use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::Row;

use crate::share::{Query, QueryResult};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::utils::*;

use crate::api::alerts::Alert;

static SQL_QUERY_ALERTS: &'static str = r#"
    SELECT r.*,
        al.name AS alarm_name,
        l.name AS location_name,
        (SELECT ARRAY(SELECT xx.name->>'en' FROM {SCHEMA}.locations AS xx WHERE xx.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name,
        lti.name AS lti_name,
        uu.name AS modifier_name,
        uu.email AS modifier_email
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = r.alid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
"#;

static SQL_COUNT_ALERTS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = r.alid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SHEMA}.location_types AS lti ON lti.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
"#;

static SQL_GET_ALERT: &'static str = r#"
    SELECT r.*,
        al.name AS alarm_name,
        l.name AS location_name,
                          (SELECT ARRAY(SELECT xx.name->>'en' FROM {SCHEMA}.locations AS xx WHERE xx.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name,
        lti.name AS lti_name,
        uu.name AS modifier_name,
        uu.email AS modifier_email,
        al.definition as alarm_definition,
        al.description AS alarm_description
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = r.alid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn query_alerts(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_ALERTS, SQL_COUNT_ALERTS, tki, Alert)
}

pub fn get_alert(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Alert> = match &conn.query(&SQL_GET_ALERT.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Alert::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}


static SQL_MAP_QUERY_ALERTS: &'static str = r#"
    SELECT r.*,
        al.name AS alarm_name,
        l.name AS location_name,
                          (SELECT ARRAY(SELECT xx.name->>'en' FROM {SCHEMA}.locations AS xx WHERE xx.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name,
        l.geojson AS loc_geom,
        l.geometry_type AS loc_geom_type,
        lti.name AS lti_name,
        uu.name AS modifier_name,
        uu.email AS modifier_email
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = r.alid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
"#;

pub fn query_alerts_map(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone())?;

    query_fn!(conn, query, SQL_MAP_QUERY_ALERTS, SQL_COUNT_ALERTS, tki, Alert)
}
