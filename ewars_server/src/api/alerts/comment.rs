use serde_json as json;
use serde_json::Value;

use failure::Error;
use chrono::prelude::*;
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::api::alerts::{Alert, AlertEvent};

#[derive(Debug, Deserialize, Clone)]
pub struct AlertComment {
    pub uuid: Uuid,
    pub comment: String,
}

static SQL_ADD_COMMENT: &'static str = r#"
    UPDATE {SCHEMA}.alerts
    SET events = events || $1::JSONB
    WHERE uuid = $2;
"#;

pub fn comment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AlertComment = json::from_value(msg.args.clone())?;

    let new_event = AlertEvent {
        uuid: Uuid::new_v4(),
        uid: Some(msg.user.uuid.clone()),
        created: Utc::now(),
        comment: Some(data.comment.clone()),
        reason: Option::None,
        transition: Option::None,
        event_type: "COMMENT".to_string(),
    };

    let event_val: Value = json::to_value(new_event).unwrap();
    let result: bool = match &conn.execute(&SQL_ADD_COMMENT.replace("{SCHEMA}", &tki), &[&event_val, &data.uuid]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    let _ = &conn.execute("NOTIFY sonoma, '{}'", &[]).unwrap();

    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("ERR_ADDING_COMMENT");

    }
}
