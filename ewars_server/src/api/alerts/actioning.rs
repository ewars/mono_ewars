use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use failure::Error;
use chrono::prelude::*;
use postgres::rows::{Row};
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::api::alerts::{Alert, AlertEvent};
use crate::api::utils::*;

#[derive(Debug, Deserialize, Clone)]
pub struct AlertAction {
    pub uuid: Uuid,
    pub stage: String,
    pub outcome: Option<String>,
    pub outcome_comments: Option<String>,
    pub verification_outcome: Option<String>,
    pub verification_comments: Option<String>,
    pub risk: Option<(String, (i32, i32))>,
    pub exposure_assessment: Option<String>,
    pub hazard_assessment: Option<String>,
    pub context_assessment: Option<String>,
}

static SQL_GET_ALERT: &'static str = r#"
    SELECT r.*,
        al.name AS alarm_name,
        l.name AS location_name,
        (SELECT ARRAY(SELECT xx.name->>'en' FROM {SCHEMA}.locations AS xx WHERE xx.uuid::TEXT = ANY(l.lineage::TEXT[]))) AS location_full_name,
        lti.name AS lti_name,
        uu.name AS modifier_name,
        uu.email AS modifier_email
    FROM {SCHEMA}.alerts AS r
        LEFT JOIN {SCHEMA}.alarms AS al ON al.uuid = r.alid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

static SQL_UPDATE_ALERT: &'static str = r#"
    UPDATE {SCHEMA}.alerts
    SET stage = $1,
        data = $2,
        events = $3,
        modified = NOW(),
        modified_by = $4,
        status = $5
    WHERE uuid = $6;
"#;

pub fn action_alert(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AlertAction = json::from_value(msg.args.clone())?;

    let alert: Option<Alert> = get_single_from_args!(conn, SQL_GET_ALERT, Alert, tki, &[&data.uuid]);
    
    if alert.is_none() {
        bail!("ERR_UNK_ALERT");
    } 
    let alert: Alert = alert.unwrap();

    let mut alert_data: HashMap<String, String> = HashMap::new();
    if let Some(c) = alert.data {
        alert_data = json::from_value(c.clone()).unwrap();
    }
    let mut next_stage: String = "".to_string();
    let mut next_status: String = "OPEN".to_string();
    let mut action_event: Option<AlertEvent> = Option::None;

    let current_stage: String = data.stage.clone();
    match current_stage.as_ref() {
        "VERIFICATION" => {
            let ver_outcome: String = data.verification_outcome.clone().unwrap();
            let mut event_type: String = "VERIFICATION".to_string();
            match ver_outcome.as_ref() {
                "MONITOR" => {
                    next_stage = "VERIFICATION".to_string();
                    next_status = "OPEN".to_string();
                },
                "DISCARD" => {
                    next_stage = "VERIFICATION".to_string();
                    next_status = "CLOSED".to_string();
                    event_type = "CLOSE".to_string();
                },
                "ADVANCE" => {
                    next_stage = "RISK_ASSESSMENT".to_string();
                },
                _ => {
                    next_stage = "VERIFICATION".to_string();
                }
            };

            alert_data.entry("verification_outcome".to_string())
                .and_modify(|x| *x = ver_outcome.clone())
                .or_insert(data.verification_outcome.unwrap());

            if let Some(c) = &data.verification_comments {
                alert_data.entry("verification_comment".to_string())
                    .and_modify(|x| *x = c.clone())
                    .or_insert(c.clone());
            }

            action_event = Some(AlertEvent {
                uuid: Uuid::new_v4(),
                uid: Some(msg.user.uuid.clone()),
                event_type: event_type.clone(),
                transition: Some(vec!["VERIFICATION".to_string(), "RISK_ASSESSMENT".to_string()]),
                created: Utc::now(),
                reason: Option::None,
                comment: Option::None,
            });
        },
        "RISK_ASSESSMENT" => {
            next_stage = "RISK_CHARACTERIZATION".to_string();

            if let Some(c) = data.exposure_assessment {
                alert_data.entry("exposure_assessment".to_string())
                    .and_modify(|x| *x = c.clone())
                    .or_insert(c.clone());
            }

            if let Some(c) = data.hazard_assessment {
                alert_data.entry("hazard_assessment".to_string())
                    .and_modify(|x| *x = c.clone())
                    .or_insert(c.clone());
            }

            if let Some(c) = data.context_assessment {
                alert_data.entry("context_assessment".to_string())
                    .and_modify(|x| *x = c.clone())
                    .or_insert(c.clone());
            }


            action_event = Some(AlertEvent {
                uuid: Uuid::new_v4(),
                uid: Some(msg.user.uuid.clone()),
                event_type: "RISK_ASSESSMENT".to_string(),
                transition: Some(vec!["RISK_ASSESSMENT".to_string(), "RISK_CHARACTERIZATION".to_string()]),
                created: Utc::now(),
                reason: Option::None,
                comment: Option::None,
            });
        },
        "RISK_CHARACTERIZATION" => {
            next_stage = "OUTCOME".to_string();

            if let Some(r) = data.risk {
                eprintln!("{:?}", r);
                let risk_val: String = json::to_string(&r).unwrap();
                alert_data.entry("risk".to_string())
                    .and_modify(|x| *x = risk_val.clone().to_string())
                    .or_insert(risk_val.clone().to_string());
            }

            action_event = Some(AlertEvent {
                uuid: Uuid::new_v4(),
                uid: Some(msg.user.uuid.clone()),
                event_type: "RISK_CHARACTERIZATION".to_string(),
                transition: Some(vec!["RISK_CHARACTERIZATION".to_string(), "OUTCOME".to_string()]),
                created: Utc::now(),
                reason: Option::None,
                comment: Option::None,
            });
        },
        "OUTCOME" => {
            let outcome: String = data.outcome.unwrap();
            next_stage = "OUTCOME".to_string();

            match outcome.as_ref() {
                "DISCARD" => {
                    next_status = "CLOSED".to_string();
                },
                "RESPOND" => {
                    next_status = "CLOSED".to_string();
                },
                "MONITOR" => {
                    next_status = "OPEN".to_string();
                },
                _ => {
                    next_stage = "OPEN".to_string();
                }
            }

            if let Some(c) = data.outcome_comments {
                alert_data.entry("outcome_comments".to_string())
                    .and_modify(|x| *x = c.clone())
                    .or_insert(c.clone());
            }

            alert_data.entry("outcome".to_string())
                .and_modify(|x| *x = outcome.clone())
                .or_insert(outcome.clone());


            action_event = Some(AlertEvent {
                uuid: Uuid::new_v4(),
                uid: Some(msg.user.uuid.clone()),
                event_type: "OUTCOME".to_string(),
                transition: Option::None,
                created: Utc::now(),
                reason: Option::None,
                comment: Option::None,
            });
        },
        _ => {}
    }

    let data_val: Value = json::to_value(alert_data).unwrap();
    let mut events: Vec<AlertEvent> = Vec::new();
    if let Some(c) = alert.events {
        events = c.clone();
    }

    if let Some(c) = action_event {
        events.push(c.clone());
    }
    let events_val: Value = json::to_value(events).unwrap();

    let result: bool = match &conn.execute(&SQL_UPDATE_ALERT.replace("{SCHEMA}", &tki), &[
        &next_stage,
        &data_val,
        &events_val,
        &msg.user.uuid,
        &next_status,
        &data.uuid,
    ]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    // Send notification to user


    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("ERR_ACTIONING_ALERT");
    }
}
