use serde_json as json;
use serde_json::Value;

use failure::Error;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::api::alerts::{Alert, get_alert_int};

pub struct Record {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub location_name: Option<Value>,
    pub form_name: Value,
    pub data: Value,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
    pub creator: Option<Creator>,
    pub modifier: Option<Modifier>,
    pub definition: Value,
    pub features: Value,
}

impl<'a> From<Row<'a>> for Record {
    fn from(row: Row) -> Self {
        Record {
            uuid: row.get("uuid"),

        }
    }
}

static SQL_GET_RECORD: &'static str = r#"
    SELECT r.uuid, 
        r.fid, 
        l.name AS location_name, 
        f.name AS form_name,
        r.data, 
        r.created, 
        r.modified, 
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email,
        f.definition,
        f.features
    FROM {SCHEMA}.records AS r
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = r.data->>'__lid__'
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;


static SQL_GET_RECORDS: &'static str = r#"
    SELECT r.uuid, 
        r.fid, 
        l.name AS location_name, 
        f.name AS form_name,
        r.data, 
        r.created, 
        r.modified, 
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email,
        f.definition,
        f.features
    FROM {SCHEMA}.records AS r
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = r.data->>'__lid__'
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.fid = ANY($1)
        AND CAST(r.data->>'__dd__' AS DATE) >= $2
        AND CAST(r.data->>'__dd__' AS DATE) <= $3;
"#;


// Get records related to an alert
pub fn get_related_records(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let alert = match get_alert_int(&conn, &tki, &id) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            bail!("ERR_NO_ALERT");
        }
    };

    let mut results: Vec<Record> = Vec::new();

    Ok(json::to_value(results).unwrap());
}

