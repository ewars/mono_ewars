use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use failure::Error;

use crate::models::{UserSession};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::alarms::{Alarm, AlarmCreate, AlarmUpdate};

static SQL_DELETE_ALERTS_BY_ALARM: &'static str = r#"
    DELETE FROM {SCHEMA}.alerts WHERE alid = $1;
"#;

static SQL_DELETE_ALARM: &'static str = r#"
    DELETE FROM {SCHEMA}.alarms WHERE uuid = $1;
"#;

pub fn delete_alarm(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    &conn.execute(&SQL_DELETE_ALERTS_BY_ALARM.replace("{SCHEMA}", &tki), &[&id]).unwrap();
    &conn.execute(&SQL_DELETE_ALARM.replace("{SCHEMA}", &tki), &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}

static SQL_INSERT_ALARM: &'static str = r#"
    INSERT INTO {SCHEMA}.alarms 
    (name, status, description, version, definition, workflow, permissions, guidance, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING uuid;
"#;

pub fn create_alarm(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let alarm: AlarmCreate  = json::from_value(msg.args.clone()).unwrap();


    let result: Option<Uuid> = match &conn.query(&SQL_INSERT_ALARM.replace("{SCHEMA}", &tki), &[
        &alarm.name,
        &alarm.status,
        &alarm.description,
        &alarm.version,
        &alarm.definition,
        &alarm.workflow,
        &alarm.permissions,
        &alarm.guidance,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(row.get("uuid"))
            } else {
                Option::None
            }
        },
        Err(err) => {
            Option::None
        }
    };

    if result.is_none() {
        bail!("ERR_CREATE");
    }

    Ok(json::to_value(result.unwrap()).unwrap())
}

static SQL_UPDATE_ALARM: &'static str = r#"
    UPDATE {SCHEMA}.alarms
        SET name = $1,
            status = $2,
            description = $3,
            version = $4,
            definition = $5,
            workflow = $6,
            permissions = $7,
            guidance = $8,
            modified = NOW(),
            modified_by = $9
    WHERE uuid = $10;
"#;

pub fn update_alarm(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let alarm: AlarmUpdate = json::from_value(msg.args.clone()).unwrap();

    match &conn.execute(&SQL_UPDATE_ALARM.replace("{SCHEMA}", &tki), &[
        &alarm.name,
        &alarm.status,
        &alarm.description,
        &alarm.version,
        &alarm.definition,
        &alarm.workflow,
        &alarm.permissions,
        &alarm.guidance,
        &msg.user.uuid,
        &alarm.uuid,
    ]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            eprintln!("{:?}", err);
            bail!("ERR_UPDATING");
        }
    }
}

pub fn export_alarm(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}
