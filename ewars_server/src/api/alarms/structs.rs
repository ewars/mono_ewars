use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use postgres::rows::{Row};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SystemIndicator {
    pub uuid: Uuid,
    pub metric: Option<String>,
    pub dimension: Option<String>,
    pub form_id: Option<Uuid>,
    pub alarm_id: Option<Uuid>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum IndicatorSource {
    System(SystemIndicator),
    Standard(Uuid),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CritSource {}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum Numeric {
    Integer(i64),
    Float(f64),
    Str(String),
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct AlarmDefinition {
    pub sti: Option<Uuid>,
    pub loc_id: Option<Uuid>,
    pub ds_type: Option<String>,
    pub loc_spec: Option<String>,
    pub ad_period: Option<Numeric>,
    pub ad_enabled: Option<bool>,
    pub crit_floor: Option<Numeric>,
    pub crit_value: Option<Numeric>,
    pub crit_source: String,
    pub ds_interval: Option<String>,
    pub ds_modifier: Option<Numeric>,
    pub inv_enabled: Option<bool>,
    pub crit_formula: Option<String>,
    pub crit_series: Option<Vec<Value>>,
    pub crit_ed_spec: Option<String>,
    pub crit_sd_spec: Option<String>,
    pub ds_agg_lower: Option<bool>,
    pub ds_aggregate: Option<String>,
    pub ds_indicator: Option<IndicatorSource>,
    pub inv_form_ids: Option<Vec<Uuid>>,
    pub loc_restrict: Option<bool>,
    pub monitor_type: Option<String>,
    pub sti_restrict: Option<bool>,
    pub crit_modifier: Option<Numeric>,
    pub crit_indicator: Option<IndicatorSource>,
    pub crit_reduction: Option<String>,
    pub sti_agg_rollup: Option<bool>,
    pub crit_comparator: Option<String>,
    pub ds_interval_type: Option<String>,
    pub crit_ed_intervals: Option<Numeric>,
    pub crit_sd_intervals: Option<Numeric>,
}


#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct Alarm {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub version: i32,
    pub definition: AlarmDefinition,
    pub workflow: Option<Value>,
    pub permissions: Option<Value>,
    pub guidance: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub creator: Option<String>,
    pub modifier: Option<String>,
}

impl<'a> From<Row<'a>> for Alarm {
    fn from(row: Row) -> Self {
        let definition: AlarmDefinition = json::from_value(row.get("definition")).unwrap();
        Alarm {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            version: row.get("version"),
            definition,
            workflow: row.get("workflow"),
            permissions: row.get("permissions"),
            guidance: row.get("guidance"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator: match row.get_opt("creator") {
                Some(res) => res.unwrap(),
                None => Option::None
            },
            modifier: match row.get_opt("modifier") {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct AlarmUpdate {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub version: i32,
    pub definition: Value,
    pub workflow: Option<Value>,
    pub permissions: Option<Value>,
    pub guidance: Option<String>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct AlarmCreate {
    pub name: String,
    pub status: String,
    pub description: Option<String>,
    pub version: i32,
    pub definition: Value,
    pub workflow: Option<Value>,
    pub permissions: Option<Value>,
    pub guidance: Option<String>,
}
