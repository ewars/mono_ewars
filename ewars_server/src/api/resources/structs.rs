use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct Resource {
    pub uuid: Uuid,
    pub name: String,
    pub status: String,
    pub version: i32,
    pub description: Option<String>,
    pub content_type: String,
    pub shared: Option<bool>,
    pub layout: Option<Value>,
    pub data: Option<Value>,
    pub style: Option<Value>,
    pub variables: Option<Value>,
    pub permissions: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub creator: Option<String>,
    pub modifier: Option<String>,
}

impl<'a> From<Row<'a>> for Resource {
    fn from(row: Row) -> Self {
        Resource {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            version: row.get("version"),
            description: row.get("description"),
            content_type: row.get("content_type"),
            shared: row.get("shared"),
            layout: row.get("layout"),
            data: row.get("data"),
            style: row.get("style"),
            variables: row.get("variables"),
            permissions: row.get("permissions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator: match row.get_opt("creator") {
                Some(res) => res.unwrap(),
                None => Option::None
            },
            modifier: match row.get_opt("modifier") {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

