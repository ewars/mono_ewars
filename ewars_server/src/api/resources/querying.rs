use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::share::{Query, QueryResult};

use uuid::Uuid;

use crate::api::utils::*;
use crate::api::resources::{Resource};

static SQL_QUERY_RESOURCES: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.resources AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_RESOURCES: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.resources AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_GET_RESOURCE: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.resources AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn query_resources(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value,Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_RESOURCES, SQL_COUNT_RESOURCES, tki, Resource)
}


pub fn get_resource(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Resource> = match &conn.query(&SQL_GET_RESOURCE.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Resource::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    match result {
        Some(res) => {
            Ok(json::to_value(res).unwrap())
        },
        None => {
            bail!("ERR_UNK");
        }
    }
}


