use serde_json::Value;

use postgres::rows::{Row};
use uuid::Uuid;
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditData {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Outbreak {
    pub uuid: Uuid,
    pub name: String,
    pub description: String,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub locations: Vec<Uuid>,
    pub forms: Vec<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    #[serde(flatten)]
    pub audit_data: AuditData,
}

impl<'a> From<Row<'a>> for Outbreak {
    fn from(row: Row) -> Self {
        Outbreak {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            start_date: row.get("start_date"),
            end_date: row.get("end_date"),
            status: row.get("status"),
            locations: row.get("locations"),
            forms: row.get("forms"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            audit_data: AuditData {
                modifier: match row.get_opt("modifier") {
                    Some(res) => res.unwrap(),
                    None => Option::None
                },
                creator: match row.get_opt("creator") {
                    Some(res) => res.unwrap(),
                    None => Option::None
                }
            }
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct OutbreakUpdate {
    pub uuid: Uuid,
    pub name: String,
    pub description: String,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub locations: Vec<Uuid>,
    pub forms: Vec<Uuid>,
}


#[derive(Debug, Deserialize)]
pub struct OutbreakCreate {
    pub name: String,
    pub description: String,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub locations: Vec<Uuid>,
    pub forms: Vec<Uuid>,
}
