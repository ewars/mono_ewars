use std::collections::HashMap;

use failure::Error;

use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use crate::api::records::{Record};
use crate::share::{Query, QueryResult, RecordQuery};

use crate::api::utils::*;
use crate::models::{Form, Field, User};

static SQL_QUERY_RECORDS: &'static str = r#"
    SELECT {SELECTS}
    FROM {SCHEMA}.records AS r
        {JOINS}
"#;

static SQL_COUNT_RECORDS: &'static str = r#"
    SELECT COUNT(r.uuid) AS total
    FROM {SCHEMA}.records AS r
        {JOINS}
"#;

static SQL_GET_LOCATIONS: &'static str = r#"
    SELECT DISTINCT(lid)
    FROM {SCHEMA}.assignments
    WHERE uid = $1;
"#;

static SQL_GET_LOCATIONS_USER: &'static str = r#"

"#;

pub fn query_records(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: RecordQuery = json::from_value(msg.args.clone()).unwrap();

    // USER and REGIONAL_ADMIN have restricted access to records based on location
    //
    let mut filters: HashMap<String, (String, Value)> = query.filters.clone();
    let mut orders: HashMap<String, (String, String)> = query.orders.clone();
    let limit: i32 = query.limit.clone();
    let offset: i32 = query.offset.clone();
    let fid: Uuid = query.fid.clone();

    let mut selects: Vec<String> = vec![
        "r.*".to_string(),
        "(uu.name || ' (' || uu.email || ')') AS submitter".to_string(),
        "(uc.name || ' (' || uc.email || ')') As modifier".to_string(),
    ];
    let mut joins: Vec<String> = vec![
        "LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.submitted_by".to_string(),
        "LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by".to_string(),
    ];
    let mut metadata: Vec<String> = Vec::new();

    let form: Form = match Form::get_form_by_id(&conn, &tki, &fid) {
        Ok(res) => {
            res
        },
        Err(err) => {
            bail!("ERR_FORM_NOT_FOUND");
        }
    };

    let location_fields: Vec<(String, Field)> = form.get_loc_fields();

    for loc_field in &location_fields {
        let mut field_name: String = loc_field.0.clone();
        let field: Field = loc_field.1.clone();

        let joined_name = field_name.replace(" ", "_");

        joins.push(format!("LEFT JOIN {{SCHEMA}}.locations AS {}_join ON {}_join.uuid = CAST(r.data->>'{}' AS UUID)", joined_name, joined_name, field_name));
        metadata.push(loc_field.0.clone());
    }

    if metadata.len() > 0 {
        let mut meta_sql = "jsonb_build_object(".to_string();

        let mut props: Vec<String> = Vec::new();

        for item in metadata {
            props.push(format!("'{}'", item.clone()));
            props.push(format!("{}_join.name", item.replace(" ", "_")));
        }

        meta_sql.push_str(&props.join(","));

        meta_sql.push_str(") AS metadata");

        selects.push(meta_sql.clone());
    }

    let mut sql: String = SQL_QUERY_RECORDS.to_string();
    let mut sql_count: String = SQL_COUNT_RECORDS.to_string();
    let mut wheres: Vec<String> = Vec::new();

    // Handle seach params
    if let Some(param) = query.param.clone() {
        let mut vals: Vec<String> = vec![
            "uu.email::TEXT".to_string(),
            "uu.name::TEXT".to_string(),
            "uc.email::TEXT".to_string(),
            "uc.name::TEXT".to_string(),
            "r.eid::TEXT".to_string(),
        ];

        for item in form.get_text_fields() {
            vals.push(format!("CAST(r.data->>'{}' AS TEXT)", item.0.clone()));
        }

        for loc_field in location_fields {
            let field_name: String = loc_field.0.clone();
            let field: Field = loc_field.1.clone();

            let joined_name = format!("{}_join", field_name.replace(" ", "_"));
            vals.push(format!("{}.name::TEXT", joined_name));
            vals.push(format!("{}.pcode::TEXT", joined_name));
            vals.push(format!("{}.groups::TEXT", joined_name));
        }

        let search_box: String = vals.join(", ").to_string();

        wheres.push(format!("CONCAT({}) ILIKE '%{}%'", search_box, param));
    }

    wheres.push(format!("r.fid = '{}'::UUID", query.fid));

    sql = sql.replace("{JOINS}", &joins.join("\n"));
    sql = sql.replace("{SELECTS}", &selects.join(", "));
    sql_count = sql_count.replace("{JOINS}", &joins.join("\n"));

    for (key, filter) in query.clone().filters {
        let (cmp, val) = query.filters.get(&key).unwrap();

        let mut s_key: String = format_key(&key);
        let mut s_val: Value = val.clone();
        let s_cmp = get_comparator(&cmp, &val);

        s_key = format_key(&key);
        s_key = format!("r.{}", s_key);

        let mut flt_str: String = format!("{} {}", &s_key, &s_cmp);
        wheres.push(flt_str.to_owned());
    }

    // There's a search param, we need to build the search query

    let loc_field: Option<Field> = form.get_reporting_location_field();

    if let Some(loc) = loc_field {
        let role: String = msg.user.role.clone();
        match role.as_ref() {
            "USER" => {
                eprintln!("HERE");
                let locations: Vec<(Uuid, Value)> = User::get_locations_for_form(&conn, &tki, &msg.user.uuid, &query.fid).unwrap();

                let loc_uuids: Vec<String> = locations.iter().map(|x| x.0.to_string()).collect();
                let param: String = loc_uuids.join("','").to_string();

                // TODO: Update this to use the field name from the configured field
                wheres.push(format!("r.data->>'__lid__' = ANY(ARRAY['{}'])", param));
            },
            "REGIONAL_ADMIN" => {
                eprintln!("HERE2");
                let locations: Vec<(Uuid, Value)> = User::get_locations_admin(&conn, &tki, &msg.user.uuid).unwrap();

                let loc_uuids: Vec<String> = locations.iter().map(|x| x.0.to_string()).collect();
                let param: String = loc_uuids.join("','").to_string();

                // TODO: need to update this to use the field name from the configuration reporting
                // field
                wheres.push(format!("__lid___join.lineage::TEXT[] && ARRAY['{}']::TEXT[]", param));
            },
            _ => {}
        }
    } else {
        // Non-location based form, only show forms that the user has access to if it's a reporting
        // user
        if &msg.user.role == "USER" {
            wheres.push(format!("r.submitted_by = '{}'", msg.user.uuid.to_string()));
        }
    }
    
    if wheres.len() > 0 {
        join_filters(&mut sql, &mut sql_count, &wheres);
    }

    let orderings: Vec<String> = apply_ordering(&query.orders);
    if orderings.len() > 0 {
        join_orders(&mut sql, &orderings);
    }

    sql.push_str(&format!(" LIMIT {}", limit));
    sql.push_str(&format!(" OFFSET {} ", offset));

    eprintln!("{}", sql);

    let records: Vec<Record> = match &conn.query(&sql.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| {
                Record::from(x)
            }).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };
    let count: i64 = get_scalar!(&conn, &sql_count, &tki);


    let result = QueryResult {
        results: json::to_value(records).unwrap(),
        count,
    };

    Ok(json::to_value(result).unwrap())
    

    // TODO: Filter the records available based on the users assignments
    // TODO: Check if the form has location fields and join that data to retrieve location
    // information
}


pub fn get_record(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}

pub fn search_records(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}
