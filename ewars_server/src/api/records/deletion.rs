use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;

use crate::models::{Form, Record, Task, DeletedRecord, Location};
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordDeletion {
    pub uuid: Uuid,
    pub reason: String,
}

static SQL_DELETE_RECORD: &'static str = r#"
    DELETE FROM {SCHEMA}.records 
    WHERE uuid = $1;
"#;

pub fn delete_record(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    // If this is a reportin guser attempting a deletion then we need to route to a task
    if &msg.user.role == "USER" {
        return create_retraction_task(&conn, &tki, &msg);
    }

    let data: RecordDeletion = json::from_value(msg.args.clone()).unwrap();

    // TODO: Check permissions of whether the user can delete this record

    let record: Option<Record> = Record::get_record_by_id(&conn, &tki, &data.uuid);
    if record.is_none() {
        bail!("ERR_RECORD_NOT_FOUND");
    }

    let record: Record = record.unwrap();

    let result: bool = match DeletedRecord::create(&conn, &tki, &record, &data.reason, &msg.user.uuid) {
        Ok(_) => true,
        Err(_) => false
    };

    if !result {
        bail!("ERR_MOVING_RECORD");
    }

    // Continue on...
    
    let result: bool = match &conn.execute(&SQL_DELETE_RECORD.replace("{SCHEMA}", &tki), &[&data.uuid]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("ERR_DELETING_RECORD");
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct TaskData {
    record_id: Uuid,
    uid: Uuid,
    form_name: Value,
    data: HashMap<String, Value>,
    reason: String,
    user: String,
    record_date: String,
    location_name: Option<(String, String)>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TaskContext {
    roles: Vec<String>,
    lid: Option<Uuid>,
}

static SQL_UPDATE_RECORD: &'static str = r#"
    UPDATE {SCHEMA}.records
    SET status = 'PENDING_RETRACTION'
    WHERE uuid = $1;
"#;

// Create a retraction task for approving the retraction of the record
fn create_retraction_task(conn: &PoolConnection, tki: &String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: RecordDeletion = json::from_value(msg.args.clone()).unwrap();

    // TODO: Check permissions of whether the user can delete this record

    let record: Option<Record> = Record::get_record_by_id(&conn, &tki, &data.uuid);
    if record.is_none() {
        bail!("ERR_RECORD_NOT_FOUND");
    }

    let record: Record = record.unwrap();

    let form: Option<Form> = match Form::get_form_by_id(&conn, &tki, &record.fid) {
        Ok(res) => Some(res),
        Err(_) => Option::None
    };

    if form.is_none() {
        bail!("ERR_RETR_FORM");
    }
    let form: Form = form.unwrap();

    let mut lid: Option<Uuid> = Option::None;
    let mut location_name: Option<(String, String)> = Option::None;
    if let Some(c) = record.data.get("__lid__") {
        lid = Some(json::from_value(c.clone()).unwrap());
        if let Some(l) = lid {
            location_name = match Location::get_location_name(&conn, &tki, &l) {
                Ok(res) => Some(res),
                Err(_) => Option::None
            };
        }
    }

    let dd: Date<Utc> = record.get_record_date().unwrap();
    let formatted_date: String = form.format_date(&dd);

    let task_data: Value = json::to_value(TaskData {
        record_id: data.uuid.clone(),
        reason: data.reason.clone(),
        uid: msg.user.uuid.clone(),
        form_name: form.name.clone(),
        data: record.data.clone(),
        user: format!("{} ({})", msg.user.name, msg.user.email),
        location_name: location_name,
        record_date: formatted_date,
    }).unwrap();

    let task_context: Value = json::to_value(TaskContext {
        roles: vec!["ACCOUNT_ADMIN".to_string(), "REGIONAL_ADMIN".to_string()],
        lid: lid
    }).unwrap();

    let result: bool = match &conn.execute(&SQL_UPDATE_RECORD.replace("{SCHEMA}", &tki), &[&data.uuid]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if !result {
        bail!("ERR_UPDATING_RECORD");
    }

    // Create the task
    let result: bool = match Task::create_task(&conn, &tki, &task_context, &task_data, &"RETRACTION_REQUEST") {
        Ok(_) => true,
        Err(_) => false
    };

    if !result {
        bail!("ERR_CREATING_TASK");
    } else {
        // Return the record with the updated status
        let record = Record::get_record_by_id(&conn, &tki, &data.uuid);
        Ok(json::to_value(record).unwrap())
    }
}


// Apprive the retraction of a record
pub fn approve_retraction(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let task: Task = match Task::get_task_by_id(&conn, &tki, &id) {
        Some(res) => res,
        None => {
            bail!("ERR_NO_TASK");
        }
    };

    let task_data: TaskData = json::from_value(task.data).unwrap();
    let record: Record = match Record::get_record_by_id(&conn, &tki, &task_data.record_id) {
        Some(res) => res,
        None => {
            bail!("ERR_NO_RECORD");
        }
    };

    let result: bool = match DeletedRecord::create(&conn, &tki, &record, &task_data.reason, &task_data.uid) {
        Ok(_) => true,
        Err(_) => false
    };

    if !result {
        bail!("ERR_DELETING_RECORD");
    }

    match record.delete_record(&conn, &tki) {
        Ok(_) => {},
        Err(_) => {
            bail!("ERR_DELETING_RECORD");
        }
    }


    let mut outcome: HashMap<&str, &str> = HashMap::new();
    outcome.insert("outcome", "APPROVED");
    let outcome: Value = json::to_value(outcome).unwrap();

    let result: bool = Task::close_task(&conn, &tki, &task.uuid, &outcome, &msg.user.uuid);

    if !result {
        bail!("ERR_UPDATING_TASK");
    }

    Ok(json::to_value(true).unwrap())
}

#[derive(Debug, Deserialize)]
pub struct RetractReject {
    pub uuid: Uuid,
    pub reason: String,
}

pub fn reject_retraction(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: RetractReject = json::from_value(msg.args.clone()).unwrap();

    let task: Option<Task> = Task::get_task_by_id(&conn, &tki, &data.uuid);
    if task.is_none() {
        bail!("ERR_NO_TASK");
    }

    let task: Task = task.unwrap();
    let task_data: TaskData = json::from_value(task.data).unwrap();
    let result: bool = Record::update_status(&conn, &tki, &task_data.record_id, "SUBMITTED");

    if !result {
        bail!("ERR_UPDATING_RECORD");
    }

    let mut outcome: HashMap<&str, &str> = HashMap::new();
    outcome.insert("outcome", "REJECTED");
    outcome.insert("reason", &task_data.reason);
    let outcome: Value = json::to_value(outcome).unwrap();

    let result: bool = Task::close_task(&conn, &tki, &task.uuid, &outcome, &msg.user.uuid);
    if !result {
        bail!("ERR_UPDATING_TASK");
    }

    // TODO: Send out email notification

    Ok(json::to_value(true).unwrap())

}
