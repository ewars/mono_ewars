mod management;
mod structs;
mod querying;
mod amendment;
mod deletion;

pub use self::management::{submit_record};
pub use self::structs::{Record, RecordSubmission, RecordAmendment};
pub use self::querying::{query_records, get_record};
pub use self::amendment::{amend_record, approve_amendment, reject_amendment};
pub use self::deletion::{delete_record, approve_retraction, reject_retraction};
