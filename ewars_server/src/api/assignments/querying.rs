use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::share::{Query, QueryResult};
use crate::api::assignments::{Assignment};

use crate::api::utils::*;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

static SQL_QUERY_ASSIGNMENTS: &'static str = r#"
    SELECT r.*,
        u.name AS user_name,
        u.email AS user_email,
        f.name AS form_name,
        l.name AS location_name,
        uu.name AS creator_name, uu.email AS creator_email,
        uc.name AS modifier_name, uc.email as modifier_email
    FROM {SCHEMA}.assignments AS r
        LEFT JOIN {SCHEMA}.users AS u ON u.uuid = r.uid
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.users AS uu on uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_ASSIGNMENTS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.assignments AS r
        LEFT JOIN {SCHEMA}.users AS u ON u.uuid = r.uid
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
        LEFT JOIN {SCHEMA}.users AS uu on uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

pub fn query_assignments(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_ASSIGNMENTS, SQL_COUNT_ASSIGNMENTS, tki, Assignment)
}

pub fn get_assignment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let ass_id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let mut sql = SQL_QUERY_ASSIGNMENTS.to_string();
    sql = format!("{} WHERE r.uuid = $1", sql);

    let result: Option<Assignment> = match &conn.query(&sql.replace("{SCHEMA}", &tki), &[&ass_id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Assignment::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}
