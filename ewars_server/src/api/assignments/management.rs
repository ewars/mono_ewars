use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use sonomalib::models::{Assignment, AssignmentCreate, AssignmentUpdate};
use sonomalib::traits::{Queryable, Deletable, Creatable, Updatable};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

pub fn create_assignment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AssignmentCreate = json::from_value(msg.args.clone())?;

    match data.create(&conn, Some(&tki), &msg.user.uuid) {
        Ok(_) => Ok(json::to_value(true)?),
        Err(err) => bail!(err)
    }
}

pub fn delete_assignment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match Assignment::delete_by_id(&conn, Some(&tki), &id) {
        Ok(_) => Ok(json::to_value(true).unwrap()),
        Err(err) => bail!(err)
    }
}

pub fn update_assignment(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: AssignmentUpdate = json::from_value(msg.args.clone()).unwrap();

    match data.update(&conn, Some(&tki), &msg.user.uuid) {
        Ok(_) => Ok(json::to_value(true).unwrap()),
        Err(err) => bail!(err)
    }
}

