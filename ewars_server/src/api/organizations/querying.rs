use serde_json as json;
use serde_json::Value;
use failure::Error;

use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::share::{Query, QueryResult};
use crate::api::utils::*;
use crate::api::organizations::Organization;

static SQL_QUERY_ORGS: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email AS creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.organizations AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_ORGS: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.organizations AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_GET_ORG: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email AS creator,
        uc.name || ' ' || uc.email AS modifer
    FROM {SCHEMA}.organizations AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub fn query_organizations(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_ORGS, SQL_COUNT_ORGS, tki, Organization)
}

pub fn get_organization(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Organization> = match &conn.query(&SQL_GET_ORG.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Organization::from(row))
            } else {
                Option::None
            }
        }
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    Ok(json::to_value(result).unwrap())
}
