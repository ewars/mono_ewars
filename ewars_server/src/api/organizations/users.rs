use serde_json as json;
use serde_json::Value;
use failure::Error;

use uuid::Uuid;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub uuid: Uuid,
    pub name: String,
    pub email: String,
    pub role: String,
}

impl<'a> From<Row<'a>> for User {
    fn from(row: Row) -> Self {
        User {
            uuid: row.get(0),
            name: row.get(1),
            email: row.get(2),
            role: row.get(3),
        }
    }
}

static SQL_GET_ORG_USERS: &'static str = r#"
    SELECT uuid, name, email, role
    FROM {SCHEMA}.users
    WHERE org_id = $1;
"#;

pub fn get_org_users(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let results: Vec<User> = match conn.query(&SQL_GET_ORG_USERS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            rows.iter().map(|x| User::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };


    Ok(json::to_value(results).unwrap())
}


