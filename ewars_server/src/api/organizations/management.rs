use serde_json as json;
use serde_json::Value;
use failure::Error;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use uuid::Uuid;

use crate::api::organizations::{Organization, OrganizationUpdate, OrganizationCreate};
use crate::api::utils::*;

static SQL_CREATE_ORG: &'static str = r#"
    INSERT INTO {SCHEMA}.organizations 
    (name, description, acronym, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5) RETURNING *;
"#;

pub fn create_organization(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: OrganizationCreate = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Organization> = match conn.query(&SQL_CREATE_ORG.replace("{SCHEMA}", &tki), &[
        &data.name,
        &data.description,
        &data.acronym,
        &msg.user.uuid,
        &msg.user.uuid
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Organization::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    match result {
        Some(res) => Ok(json::to_value(res).unwrap()),
        None => bail!("ERR_UNK")
    }
}

static SQL_RESET_ORG_USERS: &'static str = r#"
    UPDATE {SCHEMA}.accounts
        SET org_id = NULL
    WHERE org_id = $1;
"#;

static SQL_DELETE_ORG: &'static str = r#"
    DELETE FROM {SCHEMA}.organizations WHERE uuid = $1;
"#;

pub fn delete_organization(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let tx = conn.transaction().unwrap();

    let _ = &tx.execute(&SQL_RESET_ORG_USERS.replace("{SCHEMA}", &tki), &[&id]).unwrap();
    let res: bool = match &tx.execute(&SQL_DELETE_ORG.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(res) => true,
        Err(err) => {
            eprintln!("ERR: {:?}", err);
            false
        }
    };

    let result: bool = match tx.commit() {
        Ok(res) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if result {
        Ok(json::to_value(true).unwrap())
    } else {
        bail!("UNK_ERROR");
    }
}

static SQL_UPDATE_ORG: &'static str = r#"
    UPDATE {SCHEMA}.organizations
        SET name = $1,
            description = $2,
            acronym = $3,
            modified_by = $4,
            modified = NOW()
    WHERE uuid = $5 RETURNING *;
"#;

pub fn update_organization(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: OrganizationUpdate = json::from_value(msg.args.clone()).unwrap();

    let result: Option<Organization> = match conn.query(&SQL_UPDATE_ORG.replace("{SCHEMA}", &tki), &[
        &data.name,
        &data.description,
        &data.acronym,
        &msg.user.uuid,
        &data.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Organization::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(c) = result {
        Ok(json::to_value(c).unwrap())
    } else {
        bail!("ERR_UPDATING_ORG");
    }
}
