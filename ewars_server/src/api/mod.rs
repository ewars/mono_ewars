#[macro_use]
pub mod utils;
pub mod authentication;
pub mod dashboards;
pub mod myself;
pub mod base_metrics;
pub mod locations;
pub mod forms;
pub mod indicators;
pub mod indicator_groups;
pub mod alarms;
pub mod assignments;
pub mod invites;
pub mod resources;
pub mod drafts;
pub mod organizations;
pub mod documents;
pub mod alerts;
pub mod tasks;
pub mod users;
pub mod devices;
pub mod location_types;
pub mod records;
pub mod tests;
pub mod account;
pub mod outbreaks;
pub mod registration;
pub mod export;
pub mod import_projects;
pub mod reporting;
pub mod user;

#[derive(Debug, Serialize)]
pub struct Creator {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator_email: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct Modifier {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier_email: Option<String>,
}
