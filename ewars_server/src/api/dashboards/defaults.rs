use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DashItem {
    #[serde(rename = "type")]
    pub dash_type: &'static str,
    pub title: &'static str,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DefaultDashboard {
    pub name: &'static str,
    pub order: i32,
    pub color: Option<&'static str>,
    pub default: bool,
    pub metric: Option<&'static str>,
    pub definition: Vec<Vec<DashItem>>,
}

lazy_static! {
    pub static ref DEFAULT_DASHBOARDS: HashMap<&'static str, Vec<DefaultDashboard>> = {
        let mut m = HashMap::new();

        m.insert("ACCOUNT_ADMIN", vec![
            DefaultDashboard {
                name: "Overview",
                order: 0,
                color: Option::None,
                default: true,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "METRICS_OVERALL", title: "Summary Statistics"},
                        DashItem {dash_type: "DOCUMENTS", title: "Available Documents"},
                    ],
                    vec![
                        DashItem {dash_type: "ACTIVITY", title: "Activity"},
                    ],
                    vec![
                        DashItem {
                            dash_type: "METRICS",
                            title: "Summary Statistics",
                        }
                    ],
                ]

            },
            DefaultDashboard {
                name: "Surveillance",
                order: 1,
                color: Some("#febf32"),
                default: false,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "RECENT_REPORTS", title: "Recently Submitted Reports"}
                    ]
                ]

            },
            DefaultDashboard {
                name: "Alerts",
                order: 2,
                color: Some("#f18b23"),
                default: false,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "ALERTS", title: "Alerts"}
                    ]
                ]

            }
        ]);

        m.insert("REGIONAL_ADMIN", vec![
            DefaultDashboard {
                name: "Overview",
                order: 0,
                color: Option::None,
                default: true,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "METRICS_OVERALL", title: "Summary Statistics"},
                        DashItem {dash_type: "DOCUMENTS", title: "Available Documents"}
                    ],
                    vec![
                        DashItem {dash_type: "ACTIVITY", title: "Activity"}
                    ],
                    vec![
                        DashItem {
                            dash_type: "METRICS",
                            title: "Summary Statistics"
                        }
                    ]
                ]
            },
            DefaultDashboard {
                name: "Surveillance",
                order: 1,
                color: Some("#febf32"),
                default: false,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "RECENT_REPORTS", title: "Recently Submitted Records"}
                    ]
                ]
            },
            DefaultDashboard {
                name: "Alerts",
                order: 2,
                color: Some("#f18b23"),
                default: false,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "ALERTS", title: "Alerts"}
                    ]
                ]
            }
        ]);

        m.insert("USER", vec![
            DefaultDashboard {
                name: "Overview",
                order: 0,
                color: Option::None,
                default: true,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "METRICS_OVERALL", title: "Overview"},
                        DashItem {dash_type: "DOCUMENTS", title: "Documents"}
                    ],
                    vec![
                        DashItem {dash_type: "ACTIVITY", title: "Activity"},
                        DashItem {dash_type: "ASSIGNMENTS", title: "My Assignments"}
                    ],
                    vec![
                        DashItem {
                            dash_type: "METRICS",
                            title: "Metrics"
                        }
                    ]
                ]
            },
            DefaultDashboard {
                name: "Surveillance",
                order: 1,
                color: Some("#febf32"),
                default: false,
                metric: Option::None,
                definition: vec![
                    vec![
                        DashItem {dash_type: "OVERDUE", title: "Overdue Reports"},
                        DashItem {dash_type: "UPCOMING", title: "Upcoming Reports"}
                    ],
                    vec![
                        DashItem {dash_type: "AMENDMENTS", title: "Pending Amendments"},
                        DashItem {dash_type: "DELETIONS", title: "Pending Deletions"}
                    ]
                ]
            },
            DefaultDashboard {
                name: "Alerts",
                order: 2,
                color: Some("#f18b23"),
                metric: Some("OPEN_ALERTS"),
                default: false,
                definition: vec![
                    vec![
                        DashItem {dash_type: "ALERTS", title: "Alerts"}
                    ]
                ]
            }
        ]);

        m
    };
}
