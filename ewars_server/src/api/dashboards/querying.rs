use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use crate::models::{UserSession};
use crate::share::{Query, QueryResult};
use crate::api::resources::{Resource};

use crate::api::utils::*;

static SQL_QUERY_DASHBOARDS: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.resources AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_DASHBOARDS: &'static str = r#"
    SELECT COUNT(r.uuid) AS total
    FROM {SCHEMA}.resources AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

pub fn query_dashboards(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    let mut sql: String = SQL_QUERY_DASHBOARDS.to_string();
    let mut sql_count: String = SQL_COUNT_DASHBOARDS.to_string();

    let mut wheres: Vec<String> = Vec::new();

    // Pin this query to always using content_type = DASHBOARD
    // Dashboards are stored int he same table as all other
    // resources to simplify things and as there aren't a lot of them
    wheres.push(format!("r.content_type = 'DASHBOARD' "));

    for (key, filter) in query.clone().filters {
        let (cmp, val) = query.filters.get(&key).unwrap();

        let mut s_key: String = format_key(&key);
        let mut s_val: Value = val.clone();
        let s_cmp = get_comparator(&cmp, &val);

        s_key = format_key(&key);
        s_key = format!("r.{}", s_key);

        let mut flt_str: String = format!("{} {}", &s_key, &s_cmp);
        wheres.push(flt_str.to_owned());
    }

    let orderings: Vec<String> = apply_ordering(&query.orders);

    if wheres.len() > 0 {
        join_filters(&mut sql, &mut sql_count, &wheres);
    }

    if orderings.len() > 0 {
        join_orders(&mut sql, &orderings);
    }

    sql.push_str(&format!("LIMIT {} ", query.limit));
    sql.push_str(&format!("OFFSET {} ", query.offset));

    eprintln!("{}", sql);

    let records: Vec<Resource> = get_vec_from!(&conn, &sql, Resource, &tki);

    let count: i64 = get_scalar!(&conn, &sql_count, &tki);

    let result = QueryResult {
        results: json::to_value(records).unwrap(),
        count,
    };

    Ok(json::to_value(result).unwrap())
}

static SQL_GET_DASHBOARD: &'static str = r#"
    SELECT r.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email
    FROM {SCHEMA}.resources AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

// Get a specific dashboard from the system
pub fn get_dashboard(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    match conn.query(&SQL_GET_DASHBOARD.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(json::to_value(Resource::from(row)).unwrap())
            } else {
                bail!("DASH_NOT_FOUND");
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            bail!("DASH_NOT_FOUND");
        }
    }
}
