use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::Row;

#[derive(Debug, Serialize, Clone)]
pub struct ReportingPeriod {
    pub uuid: Uuid,
    pub lid: Uuid,
    pub fid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub pid: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for ReportingPeriod {
    fn from(row: Row) -> Self {
        ReportingPeriod {
            uuid: row.get("uuid"),
            lid: row.get("lid"),
            fid: row.get("fid"),
            start_date: row.get("start_date"),
            end_date: row.get("end_date"),
            status: row.get("status"),
            pid: row.get("pid"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct ReportingPeriodUpdate {
    pub uuid: Option<Uuid>,
    pub lid: Uuid,
    pub fid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
}

