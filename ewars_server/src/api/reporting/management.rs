/**
 *
 * Reporting period rules
 * A reporting period, is applied to all descendents of a given location regardless of location
 * status. 
 *
 * A reporting period which was applied as part of this waterfall behaviour is tagged with a parent
 * id value denoting the root reporting period that it inherits from. Inherited rules can not be
 * deleted, however they can be updated, which causes all children below that period to be updated
 * to use the new reporting period and are unlinked from the parent reporting period.
 *
 * ## Deleting a period
 * - You can not delete an inherited reporting period; you can however update it
 * - If you delete a root reporting period, find the next reporting period for the same form above
 * the delete reporting period and apply that reporting period down through the hierarchy
 * - When a reporting period is updated that is NOT inherited, (i.e. a root reporting period) apply
 * the changes to all child nodes that inherit directly from that reporting period.
 */

use serde_json as json;
use serde_json::Value;

use failure::Error;

use uuid::Uuid;
use postgres::rows::{Row};
use chrono::prelude::*;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use crate::api::utils::*;

use crate::api::reporting::{ReportingPeriod};


static SQL_GET_PERIOD: &'static str = r#"
    SELECT * FROM {SCHEMA}.reporting WHERE uuid = $1;
"#;

static SQL_GET_LOC_LINEAGE: &'static str = r#"
    SELECT lineage FROM {SCHEMA}.locations WHERE uuid = $1;
"#;

static SQL_GET_PERIODS_BY_LOC: &'static str = r#"
    SELECT r.*
    FROM {SCHEMA}.reporting AS r
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = r.lid
    WHERE r.lid = ANY($1) AND r.fid = $2
    ORDER BY array_length(lineage, 1) DESC;
"#;

static SQL_DELETE_CHILDS: &'static str = r#"
    DELETE FROM {SCHEMA}.reporting 
    WHERE pid = $1;
"#;

static SQL_DELETE_PERIOD: &'static str = r#"
    DELETE FROM {SCHEMA}.reporting WHERE uuid = $1;
"#;

static SQL_UPDATE_CHILDS: &'static str = r#"
    UPDATE {SCHEMA}.reporting
    SET status = $1,
        start_date = $2,
        end_date = $3,
        pid = $4
    WHERE pid = $5;
"#;

static SQL_UPDATE_PERIOD: &'static str = r#"
    UPDATE {SCHEMA}.reporting 
    SET status = $1,
        start_date = $2,
        end_date = $3,
        pid = $4
    WHERE uuid = $5;
"#;

pub fn delete_period(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let period: Option<ReportingPeriod> = match &conn.query(&SQL_GET_PERIOD.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(ReportingPeriod::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };


    // If the periodf 
    if period.is_none() {
        bail!("ERR_UNK_PERIOD");
    }
    let period: ReportingPeriod = period.unwrap();

    // You can not delete a non-root reporting period, you must update it or 
    // set it's status to INACTIVE in order to remove it from the location sub-tree
    if period.pid.is_some() {
        bail!("ERR_NON_ROOT_PERIOD_DELETION");

    }
    // We now know that we're dealing with a root-level reporting period. 
    // Find the next highest reporting period for the same form
    let lineage: Vec<Uuid> = match &conn.query(&SQL_GET_LOC_LINEAGE.replace("{SCHEMA}", &tki), &[&period.lid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                row.get(0)
            } else {
                Vec::new()
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let periods: Vec<ReportingPeriod> = match &conn.query(&SQL_GET_PERIODS_BY_LOC.replace("{SCHEMA}", &tki), &[&lineage, &period.fid]) {
        Ok(rows) => {
            rows.iter().map(|x| ReportingPeriod::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    let mut new_period: Option<ReportingPeriod> = Option::None;
    // Go through the lienage for the location in reverse working our way up the tree to find and
    // item
    for item in periods {
        // We want to skip the first one as it's the location we're actually operating agains
        if item.uuid != period.uuid {
            // Only operate if we haven't already found a reporting period
            if new_period.is_none() {
                new_period = Some(item.clone());
            }
        }
    }

    // There's no parent period to re-inherit from, so this was a standalone
    // we just delete the reporting period and it's children and call it a day
    if let Some(c) = new_period {
        // We've got a parent reporting period
        // Update the child periods to inherit from this one
        // Update the target period to inherit from this oneZ
        let _ = &conn.execute(&SQL_UPDATE_CHILDS.replace("{SCHEMA}", &tki), &[
            &c.status,
            &c.start_date,
            &c.end_date,
            &c.uuid,
            &period.uuid,
        ]).unwrap();
        let _ = &conn.execute(&SQL_UPDATE_PERIOD.replace("{SCHEMA}", &tki), &[
            &c.status,
            &c.start_date,
            &c.end_date,
            &c.uuid,
            &period.uuid,
        ]).unwrap();

        Ok(json::to_value(true).unwrap())
    } else {
        let _ = &conn.execute(&SQL_DELETE_CHILDS.replace("{SCHEMA}", &tki), &[&period.uuid]).unwrap(); // Delete the childs
        let _ = &conn.execute(&SQL_DELETE_PERIOD.replace("{SCHEMA}", &tki), &[&period.uuid]).unwrap(); // Delete the parent

        Ok(json::to_value(true).unwrap())
    }
}

#[derive(Debug, Deserialize)]
pub struct PeriodUpdate {
    pub uuid: Uuid,
    pub lid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
}


static SQL_UPDATE_PERIOD_STRICT: &'static str = r#"
    UPDATE {SCHEMA}.reporting
    SET pid = NULL,
        start_date = $1,
        end_date = $2,
        status = $3
    WHERE uuid = $4;
"#;

static SQL_GET_CHILDS: &'static str = r#"
    SELECT l.uuid
    FROM {SCHEMA}.locations AS l
    WHERE l.lineage @> $1::UUID[]
    AND l.uuid != $2;
"#;

static SQL_UPDATE_CHILDS_STRICT: &'static str = r#"
    UPDATE {SCHEMA}.reporting
    SET pid = $1,
        start_date = $2,
        end_date = $3,
        status = $4
    WHERE lid = ANY($5::UUID[])
    AND pid = $6;
"#;

pub fn update_period(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: PeriodUpdate = json::from_value(msg.args.clone()).unwrap();

    let period: Option<ReportingPeriod> = match &conn.query(&SQL_GET_PERIOD.replace("{SCHEMA}", &tki), &[&data.uuid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(ReportingPeriod::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if period.is_none() {
        bail!("ERR_NO_PERIOD");
    }
    let period: ReportingPeriod = period.unwrap();

    if period.pid.is_some() {
        // This period is the child of other periods.
        // Update the period: remove period, set properties (start_date, end_date, 
        // Update child periods ot inherit from the new one
        let _ = &conn.execute(&SQL_UPDATE_PERIOD_STRICT.replace("{SCHEMA}", &tki), &[
            &data.start_date,
            &data.end_date,
            &data.status,
            &data.uuid,
        ]).unwrap();

        let vec_lid: Vec<Uuid> = vec![period.lid.clone()];
        let locations: Vec<Uuid> = match &conn.query(&SQL_GET_CHILDS.replace("{SCHEMA}", &tki), &[&vec_lid, &period.lid]) {
            Ok(rows) => {
                rows.iter().map(|x| x.get(0)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };
        eprintln!("{:?}", locations);

        let _ =  &conn.execute(&SQL_UPDATE_CHILDS_STRICT.replace("{SCHEMA}", &tki), &[
            &period.uuid,
            &data.start_date,
            &data.end_date,
            &data.status,
            &locations,
            &period.pid,
        ]).unwrap();
    } else {
        let locations: Vec<Uuid> = match &conn.query(&SQL_GET_CHILDS.replace("{SCHEMA}", &tki), &[&period.lid, &period.lid]) {
            Ok(rows) => rows.iter().map(|x| x.get(0)).collect(),
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };

        let _ = &conn.execute(&SQL_UPDATE_CHILDS_STRICT.replace("{SCHEMA}", &tki), &[
              &period.uuid,
              &period.start_date,
              &period.end_date,
              &period.status,
              &locations,
              &period.uuid,
        ]).unwrap();

    }

    Ok(json::to_value(true).unwrap())

}

static SQL_CREATE_PERIOD: &'static str = r#"
    INSERT INTO {SCHEMA}.reporting
    (lid, fid, start_date, end_date, status, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING uuid;
"#;


static SQL_CREATE_PERIOD_CHILDS: &'static str = r#"
    WITH rows AS (
        SELECT l.uuid AS lid,
            $1::UUID AS fid,
            $2::UUID AS pid,
            $3::DATE AS start_date,
            $4::DATE AS end_date,
            $5 AS status,
            $6::UUID AS created_by, 
            $7::UUID AS modified_by
        FROM {SCHEMA}.locations AS l
            LEFT JOIN {SCHEMA}.reporting AS r ON r.lid = l.uuid AND r.fid = $8::UUID
        WHERE l.lineage::UUID[] @> $9::UUID[]
        AND l.uuid != $10::UUID
        AND r.uuid IS NULL
    )
    INSERT INTO {SCHEMA}.reporting
    (lid, fid, pid, start_date, end_date, status, created_by, modified_by)
    SELECT lid, fid, pid, start_date, end_date, status, created_by, modified_by FROM rows;
"#;

#[derive(Debug, Deserialize)]
pub struct NewPeriod {
    pub lid: Uuid,
    pub fid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
}

// Create a new reporting period
pub fn create_period(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: NewPeriod = json::from_value(msg.args.clone()).unwrap();

    let r_uuid: Option<Uuid> = match conn.query(&SQL_CREATE_PERIOD.replace("{SCHEMA}", &tki), &[
        &data.lid,
        &data.fid,
        &data.start_date,
        &data.end_date,
        &data.status,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(row.get(0))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if r_uuid.is_none() {
        bail!("ERR_CREATING_PERIOD");
    }

    let r_uuid: Uuid = r_uuid.unwrap();

    // Apply the period to all children, excluding children who already have reporting periods for
    // this form
    let vec_uuid: Vec<Uuid> = vec![data.lid.clone()];
    match conn.execute(&SQL_CREATE_PERIOD_CHILDS.replace("{SCHEMA}", &tki), &[
        &data.fid,
        &r_uuid,
        &data.start_date,
        &data.end_date,
        &data.status,
        &msg.user.uuid,
        &msg.user.uuid,
        &data.fid,
        &vec_uuid,
        &data.lid,
    ]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}
