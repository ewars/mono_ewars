use serde_json as json;
use serde_json::Value;

use failure::Error;

use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

pub fn get_reporting_period(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}

pub fn query_reporting_periods(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}
