use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};

use uuid::Uuid;
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditData {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub creator: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modifier: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct IndicatorGroup {
    pub uuid: Uuid,
    pub name: String,
    pub description: Option<String>,
    pub pid: Option<Uuid>,
    pub permissions: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    #[serde(flatten)]
    pub audit_data: AuditData,
}

impl<'a> From<Row<'a>> for IndicatorGroup {
    fn from(row: Row) -> Self {
        IndicatorGroup {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            pid: row.get("pid"),
            permissions: row.get("permissions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            audit_data: AuditData {
                creator: match row.get_opt("creator") {
                    Some(res) => res.unwrap(),
                    None => Option::None,
                },
                modifier: match row.get_opt("modifier") {
                    Some(res) => res.unwrap(),
                    None => Option::None
                }
            }
        }
        
    }
}

#[derive(Debug, Deserialize)]
pub struct IndicatorGroupCreate {
    pub name: String,
    pub description: Option<String>,
    pub pid: Option<String>,
    pub permissions: Option<Value>,
}

#[derive(Debug, Deserialize)]
pub struct IndicatorGroupUpdate {
    pub uuid: Uuid,
    pub name: String,
    pub description: Option<String>,
    pub pid: Option<String>,
    pub permissions: Option<Value>,
}
