use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};
use postgres::rows::{Row};
use chrono::prelude::*;
use uuid::Uuid;

use crate::api::locations::{Location};


#[derive(Debug, Deserialize)]
pub struct LocationCreate {
    pub name: Value,
    pub status: String,
    pub pcode: Option<String>,
    pub pid: Uuid,
    pub lti: Uuid,
    pub groups: Option<Vec<String>>,
    pub geometry_type: String,
    pub geojson: Option<Value>,
}


#[derive(Debug, Clone)]
pub struct ParentLocation {
    pub uuid: Uuid,
    pub lineage: Vec<Uuid>,
}

impl<'a> From<Row<'a>> for ParentLocation {
    fn from(row: Row) -> Self {
        ParentLocation {
            uuid: row.get(0),
            lineage: row.get(1),
        }
    }
}

static SQL_GET_PARENT: &'static str = r#"
    SELECT uuid, lineage 
    FROM {SCHEMA}.locations 
    WHERE uuid = $1;
"#;

static SQL_CREATE_LOCATION: &'static str = r#"
    INSERT INTO {SCHEMA}.locations 
    (uuid, name, status, pcode, groups, lti, pid, lineage, geometry_type, geojson, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);
"#;

static SQL_GET_PARENT_PERIODS: &'static str = r#"
    SELECT uuid, fid, start_date, end_date, status, pid, created_by, modified_by
    FROM {SCHEMA}.reporting 
    WHERE lid = $1;)
"#;

#[derive(Debug, Clone)]
pub struct ReportingPeriod {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub pid: Option<Uuid>,
    pub created_by: Option<Uuid>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for ReportingPeriod {
    fn from(row: Row) -> Self {
        ReportingPeriod {
            uuid: row.get(0),
            fid: row.get(1),
            start_date: row.get(2),
            end_date: row.get(3),
            status: row.get(4),
            pid: row.get(5),
            created_by: row.get(6),
            modified_by: row.get(7),
        }
    }
}

static SQL_COPY_PERIOD: &'static str = r#"
    INSERT INTO {SCHEMA}.reporting
    (lid, fid, start_date, end_date, status, pid, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8);
"#;


static SQL_GET_LOCATION: &'static str = r#"
    SELECT l.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email,
        lti.name AS ltu_name
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = l.created_by
        LEFT JOIN {SCHEMA}.users AS uc on uc.uuid = l.modified_by
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
    WHERE l.uuid = $1;
"#;

// Create a new location within the system
pub fn create_location(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: LocationCreate = json::from_value(msg.args.clone()).unwrap();

    let parent: Option<ParentLocation> = match &conn.query(&SQL_GET_PARENT.replace("{SCHEMA}", &tki), &[&data.pid]) {
        Ok(rows) => {
            if let Some(c) = rows.iter().next() {
                Some(ParentLocation::from(c))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    // Return early if we couldn't find the parent location
    if parent.is_none() {
        bail!("ERR_UNK_PARENT");
    }
    let parent: ParentLocation = parent.unwrap();

    let new_uuid: Uuid = Uuid::new_v4();
    let mut new_lineage: Vec<Uuid> = parent.lineage.clone();
    new_lineage.push(new_uuid.clone());

    let created: bool = match &conn.execute(&SQL_CREATE_LOCATION.replace("{SCHEMA}", &tki), &[
       &new_uuid,
       &data.name,
       &data.status,
       &data.pcode,
       &data.groups,
       &data.lti,
       &data.pid,
       &new_lineage,
       &data.geometry_type,
       &data.geojson,
       &msg.user.uuid,
       &msg.user.uuid,
    ]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    };

    if !created {
        bail!("ERR_CREATING_LOCATION");
    }

    // Add parent reporting periods to the new child
    let periods: Vec<ReportingPeriod> = match &conn.query(&SQL_GET_PARENT_PERIODS.replace("{SCHEMA}", &tki), &[&parent.uuid]) {
        Ok(rows) => {
            rows.iter().map(|x| ReportingPeriod::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    // iterate through the periods and apply them to the new location
    for period in periods {
        if period.pid.is_some() {
            let _ = &conn.execute(&SQL_COPY_PERIOD.replace("{SCHEMA}", &tki), &[
                &new_uuid,
                &period.fid,
                &period.start_date,
                &period.end_date,
                &period.status,
                &period.pid,
                &period.created_by,
                &period.modified_by,
            ]).unwrap();
        } else {
            let _ = &conn.execute(&SQL_COPY_PERIOD.replace("{SCHEMA}", &tki), &[
                &new_uuid,
                &period.fid,
                &period.start_date,
                &period.end_date,
                &period.status,
                &period.uuid,
                &period.created_by,
                &period.modified_by,
            ]).unwrap();
        }
    }

    let result: Option<Location> = match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&new_uuid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Location::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(c) = result {
        Ok(json::to_value(c).unwrap())
    } else {
        bail!("UNK_ERROR");
    }

}

