use serde_json as json;
use serde_json::Value;

use failure::Error;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

#[derive(Debug, Serialize, Deserialize)]
pub struct ReportingPeriod {
    pub uuid: Uuid,
    pub lid: Uuid,
    pub fid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub pid: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub form_name: Option<Value>,
}

impl<'a> From<Row<'a>> for ReportingPeriod {
    fn from(row: Row) -> Self {
        ReportingPeriod {
            uuid: row.get("uuid"),
            lid: row.get("lid"),
            fid: row.get("fid"),
            start_date: row.get("start_date"),
            end_date: row.get("end_date"),
            status: row.get("status"),
            pid: row.get("pid"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            form_name: row.get("form_name"),
        }
    }
}

static SQL_GET_PERIODS: &'static str = r#"
    SELECT r.*, f.name AS form_name
    FROM {SCHEMA}.reporting AS r
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
    WHERE r.lid = $1;
"#;

pub fn get_reporting_periods(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let periods: Vec<ReportingPeriod> = match &conn.query(&SQL_GET_PERIODS.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            rows.iter().map(|x| ReportingPeriod::from(x)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(periods).unwrap())
}
