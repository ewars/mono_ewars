use failure::Error;
use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use chrono::prelude::*;
use uuid::Uuid;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::locations::{Location};
use crate::api::utils::*;

static SQL_GET_LOCATION: &'static str = r#"
    SELECT l.*,
        uu.name AS creator_name,
        uu.email AS creator_email,
        uc.name AS modifier_name,
        uc.email AS modifier_email,
        lti.name AS ltu_name
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = l.created_by
        LEFT JOIN {SCHEMA}.users AS uc on uc.uuid = l.modified_by
        LEFT JOIN {SCHEMA}.location_types AS lti ON lti.uuid = l.lti
    WHERE l.uuid = $1;
"#;

#[derive(Debug, Deserialize)]
pub struct LocationUpdate {
    pub uuid: Uuid,
    pub name: Value,
    pub status: String,
    pub pid: Uuid,
    pub groups: Option<Vec<String>>,
    pub geometry_type: String,
    pub geojson: Option<Value>,
    pub lti: Uuid,
    pub pcode: Option<String>,
}

static SQL_UPDATE_LOCATION: &'static str = r#"
    UPDATE {SCHEMA}.locations
    SET name = $1,
        status = $2,
        pid = $3,
        groups = $4,
        geometry_type = $5,
        geojson = $6,
        lineage = $7,
        lti = $8,
        pcode = $9
    WHERE uuid = $10;
"#;

static SQL_GET_PARENT: &'static str = r#"
    SELECT uuid, lineage
    FROM {SCHEMA}.locations 
    WHERE uuid = $1;
"#;

#[derive(Debug)]
pub struct  ParentLocation {
    pub uuid: Uuid,
    pub lineage: Vec<Uuid>,
}

impl<'a> From<Row<'a>> for ParentLocation {
    fn from(row: Row) -> Self {
        ParentLocation {
            uuid: row.get(0),
            lineage: row.get(1),
        }
    }
}

static SQL_GET_CHILDS: &'static str = r#"
    SELECT uuid, lineage 
    FROM {SCHEMA}.locations
    WHERE lienage @> $1::UUID[];
"#;

#[derive(Debug, Clone)]
pub struct ChildLocation {
    pub uuid: Uuid,
    pub lineage: Vec<Uuid>,
}

impl<'a> From<Row<'a>> for ChildLocation {
    fn from(row: Row) -> Self {
        ChildLocation {
            uuid: row.get(0),
            lineage: row.get(1),
        }
    }
}

#[derive(Debug, Clone)]
pub struct ReportingPeriod {
    pub uuid: Uuid,
    pub lid: Uuid,
    pub fid: Uuid,
    pub start_date: NaiveDate,
    pub end_date: Option<NaiveDate>,
    pub status: String,
    pub pid: Option<Uuid>,
    pub created_by: Option<Uuid>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for ReportingPeriod {
    fn from(row: Row) -> Self {
        ReportingPeriod {
            uuid: row.get(0),
            lid: row.get(1),
            fid: row.get(2),
            start_date: row.get(3),
            end_date: row.get(4),
            status: row.get(5),
            pid: row.get(6),
            created_by: row.get(7),
            modified_by: row.get(8),
        }
    }
}

static SQL_UPDATE_LINEAGE: &'static str = r#"
    UPDATE {SCHEMA}.locations
    SET lineage = $1
    WHERE uuid = $2;
"#;

static SQL_DELETE_INHERITED: &'static str = r#"
    DELETE FROM {SCHEMA}.reporting
    WHERE pid IS NOT NULL
    AND lid = ANY($1);
"#;

// Get reporting periods in order of length of lineage (longest first)
static SQL_GET_CUSTOMS: &'static str = r#"
    SELECT l.uuid, l.lid, l.fid, l.start_date, l.end_date, l.status, l.pid, l.created_by, l.modified_by
    FROM {SCHEMA}.reporting AS l
        LEFT JOIN {SCHEMA}.locations AS lt ON lt.uuid = l.lid
    WHERE l.pid IS NULL
    AND lt.lineage @> $1::UUID[]
    ORDER BY array_length(lt.lineage, 1) DESC;
"#;

static SQL_GET_PARENT_PERIODS: &'static str = r#"
    SELECT uuid, lid, fid, start_date, end_date, status, pid, created_by, modified_by
    FROM {SCHEMA}.reporting
    WHERE lid = $1;
"#;


static SQL_APPLY_CUSTOM: &'static str = r#"
    WITH rows AS (
        SELECT l.uuid AS lid,
            $1::UUID AS fid,
            $2::DATE as start_date,
            $3::DATE as end_date,
            $4 AS status,
            $5::UUID AS pid,
            $6::UUID AS created_by,
            $7::UUID AS modified_by)
        FROM {SCHEMA}.locations AS l
            LEFT JOIN {SCHEMA}.reporting AS rp ON rp.lid = l.lid AND rp.fid = $8
        WHERE l.lineage @> $9::UUID[]
        AND l.uuid != $10
        AND rp.uuid IS NULL
    )
    INSERT INTO {SCHEMA}.reporting
    (lid, fid, start_date, end_date, status, pid, created_by, modified_by)
    SELECT lid, fid, start_date, end_date, status, pid, created_by, modified_by FROM rows;
"#;

static SQL_APPLY_PARENTS: &'static str = r#"
    WITH rows AS (
        SELECT l.uuid AS lid,
            $1::UUID AS fid,
            $2::DATE as start_date,
            $3::DATE as end_date,
            $4 AS status,
            $5::UUID AS pid,
            $6::UUID AS created_by,
            $7::UUID AS modified_by)
        FROM {SCHEMA}.locations AS l
            LEFT JOIN {SCHEMA}.reporting AS rp ON rp.lid = l.lid AND rp.fid = $8
        WHERE l.lineage @> $9::UUID[]
        AND l.uuid != $10
        AND rp.uuid IS NULL
    )
    INSERT INTO {SCHEMA}.reporting
    (lid, fid, start_date, end_date, status, pid, created_by, modified_by)
    SELECT lid, fid, start_date, end_date, status, pid, created_by, modified_by FROM rows;
"#;

pub fn update_location(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: LocationUpdate = json::from_value(msg.args.clone()).unwrap();

    let location: Option<Location> = match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&data.uuid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Location::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if location.is_none() {
        bail!("ERR_UNK_LOCATION");
    }
    let location: Location = location.unwrap();

    let mut lineage_change: bool = false;
    let mut lineage: Vec<Uuid> = location.lineage.clone();
    if data.pid != location.pid.clone().unwrap() {
        lineage_change = true;
        // This location has a new parent, there's a ton of work that needs doing...
        let new_parent: Option<ParentLocation> = get_single_from_args!(conn, SQL_GET_PARENT, ParentLocation, tki, &[&data.pid]);
        if let Some(c) = new_parent {
            lineage = c.lineage.clone();
            lineage.push(data.uuid.clone());
        }
    }

    // Update the location record
    match conn.execute(&SQL_UPDATE_LOCATION.replace("{SCHEMA}", &tki), &[
        &data.name,
        &data.status,
        &data.pid,
        &data.groups,
        &data.geometry_type,
        &data.geojson,
        &lineage,
        &data.lti,
        &data.pcode,
        &data.uuid,
    ]) {
        Ok(_) => {},
        Err(err) => {
            bail!(err);
        }
    }

    // TODO: Update children
    if lineage_change {
        let vec_lid: Vec<Uuid> = vec![data.uuid.clone()];
        let children: Vec<ChildLocation> = match &conn.query(&SQL_GET_CHILDS.replace("{SCHEMA}", &tki), &[&vec_lid]) {
            Ok(rows) => rows.iter().map(|x| ChildLocation::from(x)).collect(),
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };

        // Update the lineage of all the child locations
        for child in &children {
            let mut new_lineage = lineage.clone();

            let cur_lineage: Vec<Uuid> = child.lineage.clone();
            let mut tripped = false;
            for item in &cur_lineage {
                if item == &data.uuid {
                    tripped = true;
                }
                if tripped {
                    new_lineage.push(item.clone());
                }
            }

            let _ = &conn.execute(&SQL_UPDATE_LINEAGE.replace("{SCHEMA}", &tki), &[&child.uuid, &new_lineage]).unwrap();
        } 


        // Now reapply reporting periods down through the hierarchy
        // Delete inherited periods
        let mut vec_lids: Vec<Uuid> = children.iter().map(|x| x.uuid.clone()).collect();
        // Add the current location
        vec_lids.push(data.uuid.clone());

        // DElete all reporting periods which are inherited (i.e. have a pid set), this should
        // leave intact any custom set reporting periods within the sub-tree.
        let _ = &conn.execute(&SQL_DELETE_INHERITED.replace("{SCHEMA}", &tki), &[&vec_lids]).unwrap();
        // Re-propagate any custom set reporting periods within the tree
        // Get the reporting periods ordered by lineage length so that we traverse and apply from the
        // lowest point upwards.
        let customs: Vec<ReportingPeriod> = get_vec_from_args!(conn, SQL_GET_CUSTOMS, ReportingPeriod, tki, &[&vec_lid]);
        // Apply these reporting periods down through the hierarchy
        for custom in customs {
            let vec_inner_lid: Vec<Uuid> = vec![custom.lid.clone()];
            let _ = &conn.execute(&SQL_APPLY_CUSTOM.replace("{SCHEMA}", &tki), &[
                &custom.fid,
                &custom.start_date,
                &custom.end_date,
                &custom.status,
                &custom.uuid,
                &custom.created_by,
                &custom.modified_by,
                &custom.fid,
                &vec_inner_lid,
                &custom.lid,
            ]).unwrap();
        }

        // Need to apply reporting periods from the new parent now down throught the sub-tree
        let parent_periods: Vec<ReportingPeriod> = get_vec_from_args!(conn, SQL_GET_PARENT_PERIODS, ReportingPeriod, tki, &[&data.pid]);

        // Apply parent periods doewn through the hierarchy
        for period in parent_periods {
            let _ = &conn.execute(&SQL_APPLY_PARENTS.replace("{SCHEMA}", &tki), &[
                &period.fid,
                &period.start_date,
                &period.end_date,
                &period.status,
                &period.uuid,
                &period.created_by,
                &period.modified_by,
                &period.fid,
                &vec_lid,
                &period.lid,
            ]).unwrap();
        }

    }

    let location: Option<Location> = get_single_from_args!(conn, SQL_GET_LOCATION, Location, tki, &[&data.uuid]);

    if let Some(c) = location {
        Ok(json::to_value(c).unwrap())
    } else {
        bail!("ERR_UNK");
    }
}
