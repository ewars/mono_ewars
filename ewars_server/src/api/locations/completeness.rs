use std::collections::HashMap;

use serde_json as json;
use failure::Error;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::forms::{FormFeature};
use crate::utils::date_utils;

#[derive(Debug, Clone)]
pub struct LocationRecord {
    
}

#[derive(Debug, Clone)]
struct Form {
    uuid: Uuid,
    features: HashMap<String, FormFeature>,
}

impl<'a> From<Row<'a>> for Form {
    fn from(row: Row) -> Self {
        let features: HashMap<String, FormFeature> = json::from_value(row.get(1)).unwrap();
        Form {
            uuid: row.get(0),
            features,
        }
    }
}

impl Form {
    pub fn get_location_type(&self) -> Option<Uuid> {
        if let Some(feat) = self.features.get("LOCATION_REPORTING") {
            Some(feat.lti.clone().unwrap())
        } else {
            Option::None
        }
    }

    pub fn get_reporting_interval(&self) -> Option<String> {
        if let Some(feat) = self.features.get("INTERVAL_REPORTING") {
            Some(feat.interval.clone().unwrap())
        } else {
            Option::None
        }
    }
}

static SQL_GET_FORM: &'static str = r#"
    SELECT uuid, features
    FROM {SCHEMA}.forms
    WHERE uuid = $1;
"#;


static SQL_GET_LOCATIONS: &'static str = r#"
    SELECT l.uuid,
        rp.start_date,
        COALESCE(rp.end_date, NOW()::DATE) AS end_date,
        l.status
    FROM {SCHEMA}.locations AS l
        LEFT JOIN {SCHEMA}.reporting AS rp ON rp.lid = l.uuid
    WHERE rp.fid = $1
        AND l.lineage @> $2::UUID[]
        AND l.lti = $3
        AND l.status = 'ACTIVE'
        AND rp.status = 'ACTIVE';
"#;

#[derive(Debug, Clone)]
struct DataPoint {
    date: NaiveDate,
    locations: Vec<Uuid>,
    expected: i32,
    reported: i32,
}

struct Location {
    uuid: Uuid,
    start_date: NaiveDate,
    end_date: NaiveDate,
    status: String
}

impl<'a> From<Row<'a>> for Location {
    fn from(row: Row) -> Self {
        Location {
            uuid: row.get(0),
            start_date: row.get(1),
            end_date: row.get(2),
            status: row.get(3),
        }
    }
}

impl Location {
    // Check if the location is reporting for the date
    pub fn reported_for(&self, date: &NaiveDate) -> bool {
        self.start_date <= date.clone() && self.end_date >= date.clone()
    }
}

static SQL_GET_LOCATION_REPORTS: &'static str = r#"
    SELECT (r.data->>'__dd__')::DATE AS dd, l.uuid AS lid, COUNT(r.uuid) as total
    FROM {SCHEMA}.records AS r
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = r.data->>'__lid__'
        LEFT JOIN {SCHEMA}.reporting AS rp ON rp.lid = l.uuid
    WHERE r.fid = $1
        AND (r.status = 'SUBMITTED' OR r.status = 'PENDING_AMENDMENT')
        AND l.lineage @> $2::UUID[]
    GROUP BY (l.uuid, r.data->>'__dd__');
"#;

#[derive(Debug, Serialize)]
pub struct DataResult {
    pub s: i32,
    pub e: i32,
}

pub fn get_location_completeness(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let (loc_id, form_id, start_date, end_date): (Uuid, Uuid, NaiveDate, NaiveDate) = json::from_value(msg.args.clone()).unwrap();

    match calc_completeness(&conn, &tki, &loc_id, &form_id, &start_date, &end_date) {
        Ok(res) => {
            Ok(json::to_value(res).unwrap())
        },
        Err(err) => {
            bail!("ERR_CALC_COMPLETENESS");
        }
    }
}

pub fn calc_completeness(conn: &PoolConnection, tki: &String, loc_id: &Uuid, form_id: &Uuid, start_date: &NaiveDate, end_date: &NaiveDate) -> Result<Vec<(NaiveDate, DataResult)>, ()> {
    let form: Form = get_single_from!(conn, SQL_GET_FORM, Form, tki, form_id).unwrap();
    eprintln!("{:?}", form);

    let interval: String = form.get_reporting_interval().unwrap();
    let lti: Uuid = form.get_location_type().unwrap();

    let reporting_dates: Vec<NaiveDate> = date_utils::get_reporting_intervals(&start_date, &end_date, &interval).unwrap();

    let mut data_points: Vec<DataPoint> = Vec::new();

    // Extrapolate to structs to makle easier
    for item in reporting_dates {
        data_points.push(DataPoint {
            date: item.clone(),
            locations: Vec::new(),
            expected: 0,
            reported: 0,
        });
    }

    let locations: Vec<Location> = match &conn.query(
        &SQL_GET_LOCATIONS.replace("{SCHEMA}", &tki),
        &[
            &form_id,
            &vec![&loc_id],
            &lti,
        ]
        ) {
        Ok(rows) => {
            rows.iter().map(|x| Location::from(x)).collect()
        },
        Err(err) => {
            eprintln!("TT: {:?}", err);
            Vec::new()
        }
    };

    let mut location_ids: Vec<Uuid> = Vec::new();

    // Iterate through the locations and get incremenet
    // the collection where a record is expected
    // baed on the locations reporting period
    for loc in locations {
        for dp in data_points.iter_mut() {
            if loc.reported_for(&dp.date) {
                dp.expected += 1;
                dp.locations.push(loc.uuid.clone());
                location_ids.push(loc.uuid.clone());
            } 
        }
    }

    // Get a grouped count of locations with record counts for
    // for each date within the record set
    let counts: Vec<(NaiveDate, Uuid, i64)> = match &conn.query(
        &SQL_GET_LOCATION_REPORTS.replace("{SCHEMA}", &tki),
        &[
            &form_id,
            &location_ids,
        ]
        ) {
        Ok(rows) => {
            rows.iter().map(|x| (x.get(0), x.get(1), x.get(2))).collect()
        },
        Err(err) => {
            Vec::new()
        }
    };

    for count in counts {
        for dp in data_points.iter_mut() {
            if dp.date == count.0 {
                dp.reported += count.2 as i32;
            }
        }
    }

    let mut results: Vec<(NaiveDate, DataResult)> = Vec::new();

    for dp in data_points {
        results.push((dp.date.clone(), DataResult {
            s: dp.reported,
            e: dp.expected,
        }));
    }

    Ok(results)
}
