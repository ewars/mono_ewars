use serde_json as json;
use serde_json::Value;

use failure::Error;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use uuid::Uuid;

use crate::api::locations::structs::{Location};

static SQL_GET_LOCATION: &'static str = r#"
    SELECT * FROM {SCHEMA}.locations WHERE uuid = $1;
"#;

static SQL_CREATE_LOCATION: &'static str = r#"
    INSERT INTO {SCHEMA}.locations
    (name, status, pcode, codes, groups, data, lti, pid, lineage, organizations, image, geometry_type, geojson, population, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16) RETURNING *;
"#;

static SQL_UPDATE_LOCATION: &'static str = r#"
    UPDATE {SCHEMA}.locations
        SET name = $1,
            status = $2,
            pcode = $3,
            codes = $4,
            groups = $5,
            data = $6,
            lti = $7,
            pid = $8,
            lineage = $9,
            organizations = $10,
            image = $11,
            geometry_type = $12,
            geojson = $13,
            population = $14,
            modified = NOW(),
            modified_by = $15
    WHERE uuid = $16;
"#;

static SQL_DELETE_LOCATION: &'static str = r#"
    DELETE FROM {SCHEMA}.locations WHERE uuid = $1;
"#;

static SQL_DELETE_CHILD_LOCATIONS: &'static str = r#"
    DELETE FROM {SCHEMA}.locations WHERE uuid = ANY($1);
"#;

static SQL_GET_CHILD_LOCATIONS: &'static str = r#"
    SELECT uuid
    FROM {SCHEMA}.locations
    WHERE lineage @> ARRAY[$1]::UUID[];
"#;

static SQL_DELETE_ALERTS: &'static str = r#"
     DELETE FROM {SCHEMA}.alerts WHERE lid = ANY($1);
"#;

static SQL_DELETE_TASKS: &'static str = r#"
    DELETE FROM {SCHEMA}.tasks WHERE context->>'lid' = ANY($1::TEXT[]);
"#;


fn get_location(conn: PoolConnection, uuid: &Uuid, tki: String) -> Result<Location, Error> {
    match conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&uuid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(Location::from(row))
            } else {
                bail!("ERR_NO_LOCATION");
            }
        },
        Err(err) => {
            bail!(err);
        }
    }
}


pub fn delete_location(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let loc_id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let sub_locations: Vec<Uuid> = match &conn.query(&SQL_GET_CHILD_LOCATIONS.replace("{SCHEMA}", &tki), &[&loc_id]) {
        Ok(rows) => {
            rows.iter().map(|x| x.get(0)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    // Delete alerts
    &conn.execute(&SQL_DELETE_ALERTS.replace("{SCHEMA}", &tki), &[&sub_locations]).unwrap();

    // Delete tasks associated with these locations
    &conn.execute(&SQL_DELETE_TASKS.replace("{SCHEMA}", &tki), &[&sub_locations]).unwrap();

    // Delete child locations
    &conn.execute(&SQL_DELETE_CHILD_LOCATIONS.replace("{SCHEMA}", &tki), &[&sub_locations]).unwrap();
    // Delete location itself
    &conn.execute(&SQL_DELETE_LOCATION.replace("{SCHEMA}", &tki), &[&loc_id]).unwrap();


    Ok(json::to_value(true).unwrap())
}

static SQL_ENABLE_CHILDS: &'static str = r#"
    UPDATE {SCHEMA}.locations
    SET status = 'ACTIVE'
    WHERE lineage @> $1::UUID[]
    AND uuid != $2;
"#;

pub fn enable_child_locations(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let vec_lid: Vec<Uuid> = vec![id.clone()];
    match conn.execute(&SQL_ENABLE_CHILDS.replace("{SCHEMA}", &tki), &[&vec_lid, &id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

static SQL_DISABLE_CHILDS: &'static str = r#"
    UPDATE {SCHEMA}.locations
    SET status = 'DISABLED'
    WHERE lineage @> $1::UUID[]
    AND uuid != $2;
"#;

pub fn disable_child_locations(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let vec_lid: Vec<Uuid> = vec![id.clone()];
    match conn.execute(&SQL_DISABLE_CHILDS.replace("{SCHEMA}", &tki), &[&vec_lid, &id]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}
