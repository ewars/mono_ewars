use serde_json as json;
use serde_json::Value;

use failure::Error;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};

use uuid::Uuid;
use chrono::prelude::*;

use crate::api::location_types::{LocationType, LocationTypeUpdate, LocationTypeCreate};

lazy_static! {
    static ref UNCAT_UUID: Uuid = Uuid::parse_str("7fc3d0fb-6477-4b69-b484-c0b684314d40").unwrap();
}

static SQL_GET_LOCATION_TYPE: &'static str = r#"
    SELECT * FROM {SCHEMA}.location_types WHERE uuid = $1;
"#;

static SQL_CREATE_LOCATION_TYPE: &'static str = r#"
     INSERT INTO {SCHEMA}.location_types
     (name, description, restrictions, created_by, modified_by)
     VALUES ($1, $2, $3, $4, $5) RETURNING *;
"#;

pub fn create_location_type(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: LocationTypeCreate = json::from_value(msg.args.clone()).unwrap();

    let id: Option<LocationType> = match &conn.query(&SQL_CREATE_LOCATION_TYPE.replace("{SCHEMA}", &tki), &[
        &data.name,
        &data.description,
        &data.restrictions,
        &msg.user.uuid,
        &msg.user.uuid,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(LocationType::from(row))
            } else {
                Option::None
            }
        }, 
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if let Some(c) = id {
        Ok(json::to_value(c).unwrap())
    } else {
        bail!("ERR_CREATE_LOC_TYPE");
    }
}

static SQL_UPDATE_LOCATIONS: &'static str = r#"
    UPDATE {SCHEMA}.locations
        SET lti = $1
    WHERE lti = $2;
"#;

static SQL_DELETE_LOCATION_TYPE: &'static str = r#"
    DELETE FROM {SCHEMA}.location_types WHERE uuid = $1;
"#;

pub fn delete_location_type(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let _ = &conn.execute(&SQL_UPDATE_LOCATIONS.replace("{SCHEMA}", &tki), &[&*UNCAT_UUID, &id]).unwrap();
    let _ = &conn.execute(&SQL_DELETE_LOCATION_TYPE.replace("{SCHEMA}", &tki), &[&id]).unwrap();


    Ok(json::to_value(true).unwrap())
}

static SQL_UPDATE_LOCATION_TYPE: &'static str = r#"
    UPDATE {SCHEMA}.location_types
        SET name = $1,
            description = $2,
            restrictions = $3,
            modified_by = $4,
            modified = NOW()
    WHERE uuid = $5 RETURNING *;
"#;

pub fn update_location_type(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let update: LocationTypeUpdate = json::from_value(msg.args.clone()).unwrap();

    &conn.execute(&SQL_UPDATE_LOCATION_TYPE.replace("{SCHEMA}", &tki), &[
        &update.name,
        &update.description,
        &update.restrictions,
        &msg.user.uuid,
        &update.uuid,
    ]).unwrap();

    let result: Option<LocationType> = match &conn.query(&SQL_GET_LOCATION_TYPE.replace("{SCHEMA}", &tki), &[&update.uuid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(LocationType::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if result.is_none() {
        bail!("ERR_NO_RECORDS");
    }

    Ok(json::to_value(true).unwrap())
}
