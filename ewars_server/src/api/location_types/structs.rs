use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use crate::api::{Creator, Modifier};


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocationType {
    pub uuid: Uuid,
    pub name: Value,
    pub description: Option<String>,
    pub restrictions: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub creator: Option<String>,
    pub modifier: Option<String>,
}

impl<'a> From<Row<'a>> for LocationType {
    fn from(row: Row) -> Self {
        LocationType {
            uuid: row.get("uuid"),
            name: row.get("name"),
            description: row.get("description"),
            restrictions: row.get("restrictions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            creator: match row.get_opt("creator") {
                Some(res) => res.unwrap(),
                None => Option::None
            },
            modifier: match row.get_opt("modifier") {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct LocationTypeUpdate {
    pub uuid: Uuid,
    pub name: Value,
    pub description: Option<String>,
    pub restrictions: Option<Value>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct LocationTypeCreate {
    pub name: Value,
    pub description: Option<String>,
    pub restrictions: Option<Value>,
}
