use serde_json as json;
use serde_json::Value;

use failure::Error;
use crate::handlers::api_handler::{PoolConnection, ApiMethod};

use uuid::Uuid;
use crate::share::{Query, QueryResult};

use crate::api::location_types::{LocationType};

use crate::api::utils::*;

static SQL_QUERY_LOCATION_TYPES: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email as creator,
        uc.name || ' ' || uc.email AS modifier
    FROM {SCHEMA}.location_types AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
"#;

static SQL_COUNT_LOCATION_TYPES: &'static str = r#"
    SELECT COUNT(r.uuid)
    FROM {SCHEMA}.location_types AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    
"#;

pub fn query_location_types(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let query: Query = json::from_value(msg.args.clone()).unwrap();

    query_fn!(conn, query, SQL_QUERY_LOCATION_TYPES, SQL_COUNT_LOCATION_TYPES, tki, LocationType)
}

static SQL_GET_LOCATION_TYPE: &'static str = r#"
    SELECT r.*,
        uu.name || ' ' || uu.email AS creator,
        uc.name || ' ' || uc.email As modifier
    FROM {SCHEMA}.location_types AS r
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.modified_by
    WHERE uuid = $1;
"#;

pub fn get_location_type(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let id: Uuid = json::from_value(msg.args.clone()).unwrap();

    let result: Option<LocationType> = match &conn.query(&SQL_GET_LOCATION_TYPE.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(LocationType::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if result.is_none() {
        bail!("ERR_RECORD_NOT_FOUND");
    }

    Ok(json::to_value(result.unwrap()).unwrap())

}

static SQL_GET_LOCATION_TYPE_AS_OPTIONS: &'static str = r#"
    SELECT uuid, name FROM {SCHEMA}.location_types;
"#;

// Get a set of location types as a list of options
pub fn get_location_types_as_options(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let results: Vec<(Uuid, Value)> = match &conn.query(&SQL_GET_LOCATION_TYPE_AS_OPTIONS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| (x.get("uuid"), x.get("name"))).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    };

    Ok(json::to_value(results).unwrap())
}


