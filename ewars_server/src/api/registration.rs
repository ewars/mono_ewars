use std::io;

use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

use futures::Future;

use crate::db::db::DbExecutor;
use crate::handlers::reg_handler::AccessRequest;

use actix_web::{Error, HttpRequest, HttpResponse, Json, State, FutureResponse, Responder, AsyncResponder};
use crate::state::AppState;

#[derive(Debug, Serialize, Deserialize)]
pub struct RegRequest {
    pub name: String,
    pub email: String,
    pub email_confirm: String,
    pub password: String,
    pub password_confirm: String,
    pub org_id: Option<Uuid>,
    pub org_name: Option<String>,
}

pub fn web_register((item, req): (Json<RegRequest>, HttpRequest<AppState>)) -> FutureResponse<HttpResponse> {
    let host = req.connection_info().host().to_string();

    req.state()
        .db
        .send(AccessRequest::RegistrationRequest {
            name: item.name.clone(),
            email: item.email.clone(),
            password: item.password.clone(),
            org_id: item.org_id.clone(),
            org_name: item.org_name.clone(),
            host: host.clone(),
            email_svc: req.state().email_svc.clone(),
        })
        .from_err()
        .and_then(move |res| {
            match res {
                Ok(auth_result) => {
                    Ok(HttpResponse::Ok().json(auth_result))
                },
                Err(_) => Ok(HttpResponse::InternalServerError().into())
            }
        }).responder()
}


