use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct Account {
    pub uuid: Uuid,
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub language: Option<String>,
    pub status: String,
    pub domain: String,
    pub theme: Option<Value>,
    pub tki: String,
    pub config: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for Account {
    fn from(row: Row) -> Self {
        Account {
            uuid: row.get("uuid"),
            id: row.get("id"),
            name: row.get("name"),
            description: row.get("description"),
            language: row.get("language"),
            status: row.get("status"),
            domain: row.get("domain"),
            theme: row.get("theme"),
            tki: row.get("tki"),
            config: row.get("config"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}
