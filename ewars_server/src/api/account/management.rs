use serde_json as json;
use serde_json::Value;

use failure::Error;

use crate::handlers::api_handler::{PoolConnection, ApiMethod};

#[derive(Debug, Deserialize)]
pub struct ConfUpdate {
    pub key: String,
    pub value: Value,
}

static SQL_UPDATE_CONF_VALUE: &'static str = r#"
    UPDATE {SCHEMA}.conf 
    SET value = $1
    WHERE key = $2;
"#;

pub fn update_conf(conn: &PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let data: ConfUpdate = json::from_value(msg.args.clone()).unwrap();

    match conn.execute(&SQL_UPDATE_CONF_VALUE.replace("{SCHEMA}", &tki), &[&data.key, &data.value]) {
        Ok(_) => {
            Ok(json::to_value(true).unwrap())
        },
        Err(err) => {
            bail!(err);
        }
    }
}

pub fn update_account(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    unimplemented!()
}
