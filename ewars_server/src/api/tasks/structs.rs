use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Clone)]
pub struct Task {
    pub uuid: Uuid,
    pub status: String,
    pub task_type: String,
    pub data: Option<Value>,
    pub actions: Option<Value>,
    pub version: i32,
    pub form: Option<Value>,
    pub context: Option<Value>,
    pub outcome: Option<Value>,
    pub actioned: Option<DateTime<Utc>>,
    pub actioned_by: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for Task {
    fn from(row: Row) -> Self {
        Task {
            uuid: row.get("uuid"),
            status: row.get("status"),
            task_type: row.get("task_type"),
            data: row.get("data"),
            actions: row.get("actions"),
            version: row.get("version"),
            form: row.get("form"),
            context: row.get("context"),
            outcome: row.get("outcome"),
            actioned: row.get("actioned"),
            actioned_by: row.get("actioned_by"),
            created: row.get("created"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct TaskAction {
    pub uuid: Uuid,
    pub action: String,
    pub data: Option<Value>,
}

