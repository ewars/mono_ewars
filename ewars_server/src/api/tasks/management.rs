use serde_json as json;
use serde_json::Value;
use failure::Error;

use uuid::Uuid;

use crate::handlers::api_handler::{ApiMethod, PoolConnection};
use crate::api::tasks::{Task, TaskAction};

static SQL_GET_TASK: &'static str = r#"
    SELECT * FROM {SCHEMA}.tasks WHERE uuid = $1;)
"#;

pub fn action_task(conn: PoolConnection, tki: String, msg: &ApiMethod) -> Result<Value, Error> {
    let task_update: TaskAction = json::from_value(msg.args.clone()).unwrap();

    let task: Option<Task> = match &conn.query(&SQL_GET_TASK.replace("{SCHEMA}", &tki), &[&task_update.uuid]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(Task::from(row))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if task.is_none() {
        bail!("ERR_NO_RECORD");
    }

    let task = task.unwrap();

    match task.task_type.as_ref() {
        "ASSIGNMENT_REQUEST" => {

        },
        "RETRACTION_REQUEST" => {

        },
        "REGISTRATION_REQUEST" => {

        },
        "ACCESS_REQUEST" => {

        },
        _ => {}
    };

    Ok(json::to_value(true).unwrap())
}

