use std::io;
use std::collections::HashMap;

use futures::{Future};

use serde_json as json;
use serde_json::Value;

use actix::prelude::*;
use actix_web::{FutureResponse};
use failure::Error;
use uuid::Uuid;
use jwt::{encode, Header, Algorithm};

use crate::db::db::{DbExecutor};

pub type PoolConnection = PooledConnection<PostgresConnectionManager>;

#[derive(Debug, Serialize, Deserialize)]
pub struct ApiError {

}


