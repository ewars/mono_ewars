use std::io;
use std::collections::HashMap;

use postgres::rows::{Row};

use chrono::prelude::*;
use actix::prelude::*;
use failure::Error;
use jwt::{encode, Header, Algorithm};
use uuid::Uuid;

use crate::db::db::DbExecutor;
use crate::utils::auth::{verify_pass, generate_password};
use crate::share::common::Claims;

use crate::handlers::api_handler::{PoolConnection};

use tera;
use tera::{Tera, Context as TeraContext, Result as TeraResult};
use crate::services::{EmailService, Email};

lazy_static! {
    pub static ref TEMPLATES: Tera = {
        let mut tera = compile_templates!("templates/emails/**/*");
        tera.autoescape_on(vec!["html", ".sql"]);
        tera
    };
}

pub enum AccessRequest {
    RegistrationRequest {
        name: String,
        email: String,
        password: String,
        org_id: Option<Uuid>,
        org_name: Option<String>,
        host: String,
        email_svc: Addr<EmailService>,
    },
    AccessRequest {
        uuid: Uuid,
        host: String,
        org_id: Option<Uuid>,
        org_name: Option<String>,
        email_svc: Addr<EmailService>,
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct AccessResult {
    pub result: bool,
    pub error_code: Option<String>,
}

impl Message for AccessRequest {
    type Result = Result<AccessResult, AccessResult>;
}

#[derive(Debug, Clone)]
pub struct TargetAccount {
    pub uuid: Uuid,
    pub tki: String,
    pub name: String,
    pub status: String,
    pub domain: String,
}

static SQL_GET_ACCOUNT: &'static str = r#"
    SELECT uuid, tki, name, status, domain
    FROM core.accounts WHERE domain = $1;
"#;
    
fn get_account(conn: &PoolConnection, host: &String) -> Result<TargetAccount, ()> {
    match conn.query(&SQL_GET_ACCOUNT, &[&host]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(TargetAccount {
                    uuid: row.get(0),
                    tki: row.get(1),
                    name: row.get(2),
                    status: row.get(3),
                    domain: row.get(4),
                })
            } else {
                Err(())
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(())
        }
    }
}

#[derive(Debug, Clone)]
struct ExistingUser {
    uuid: Uuid,
    name: String,
    email: String,
    accounts: Vec<Uuid>,
    status: String,
}

static SQL_GET_EXISTING_USER: &'static str = r#"
    SELECT uuid, name, email, accounts, status
    FROM core.users WHERE email = $1;
"#;

fn get_user(conn: &PoolConnection, email: &String) -> Result<ExistingUser, ()> {
    match &conn.query(&SQL_GET_EXISTING_USER, &[&email]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(ExistingUser {
                    uuid: row.get(0),
                    name: row.get(1),
                    email: row.get(2),
                    accounts: row.get(3),
                    status: row.get(4),
                })
            } else {
                Err(())
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(())
        }
    }
}

#[derive(Debug, Clone)]
struct ExistingAccountUser {
    uuid: Uuid,
    role: String,
    status: String,
}

static SQL_GET_ACCOUNT_USER: &'static str = r#"
    SELECT uuid, role, status
    FROM {SCHEMA}.users
    WHERE uuid = $1;
"#;

fn get_account_user(conn: &PoolConnection, id: &Uuid, tki: &String) -> Result<ExistingAccountUser, ()> {
    match &conn.query(&SQL_GET_ACCOUNT_USER.replace("{SCHEMA}", &tki), &[&id]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(ExistingAccountUser {
                    uuid: row.get(0),
                    role: row.get(1),
                    status: row.get(2),
                })
            } else {
                Err(())
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(())
        }
    }
}

#[derive(Debug, Clone)]
struct SsoUser {
    id: i32,
    uuid: Uuid,
    name: String,
    pin_code: Option<i32>,
    email: String,
    password: String,
    password_pbkdf2: String,
    accounts: Vec<Uuid>,
    status: String,
    created: DateTime<Utc>,
    created_by: Option<Uuid>,
    modified: DateTime<Utc>,
    modified_by: Option<Uuid>,
    system: bool,
}

impl<'a> From<Row<'a>> for SsoUser {
    fn from(row: Row) -> Self {
        SsoUser {
            id: row.get("id"),
            uuid: row.get("uuid"),
            name: row.get("name"),
            pin_code: row.get("pin_code"),
            email: row.get("email"),
            password: row.get("password"),
            password_pbkdf2: row.get("password_pbkdf2"),
            accounts: row.get("accounts"),
            status: row.get("status"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            system: row.get("system"),
        }
    }
}

static SQL_CREATE_SSO: &'static str = r#"
    INSERT INTO core.users 
    (name, email, password, password_pbkdf2, accounts, status, created_by, modified_by, system)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *;
"#;

fn create_sso_user(conn: &PoolConnection, email: &String, name: &String, pass: &String, account: &TargetAccount) -> Result<SsoUser, AccessResult> {
    let no_pb: Option<String> = Option::None;
    let accounts: Vec<Uuid> = vec![account.uuid.clone()];
    let status = "PENDING_VERIFICATION".to_string();
    let creator: Option<Uuid> = Option::None;
    match &conn.query(&SQL_CREATE_SSO, &[
        &name,
        &email,
        &pass,
        &no_pb,
        &accounts,
        &status,
        &creator,
        &creator,
        &false,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(SsoUser::from(row))
            } else {
                Err(AccessResult {
                    result: false,
                    error_code: Some("UNK_ERROR".to_string()),
                })
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(AccessResult {
                result: false,
                error_code: Some("UNK_ERROR".to_string()),
            })
        }
    }
}

#[derive(Debug, Clone)]
struct AccUser {
    uuid: Uuid,
    role: String,
    status: String,
    org_id: Option<Uuid>,
    created_by: Option<Uuid>,
    modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for AccUser {
    fn from (row: Row) -> Self {
        AccUser {
            uuid: row.get("uuid"),
            role: row.get("role"),
            status: row.get("status"),
            org_id: row.get("org_id"),
            created_by: row.get("created_by"),
            modified_by: row.get("modified_by"),
        }
    }
}

static SQL_CREATE_ACC_USER: &'static str = r#"
    INSERT INTO {SCHEMA}.accounts
    (uuid, role, status, org_id, created_by, modified_by)
    VALUES ($1, $2, $3, $4, $5, $6) RETURNING *;
"#;

// Create the account user
fn create_acc_user(conn: &PoolConnection, user: &SsoUser, org_id: &Option<Uuid>, tki: &String) -> Result<AccUser, AccessResult> {
    let base_role = "USER".to_string();
    let base_status = "PENDING_APPROVAL".to_string();
    let toast: Option<Uuid> = Option::None;
    match &conn.query(&SQL_CREATE_ACC_USER.replace("{SCHEMA}", &tki), &[
        &user.uuid,
        &base_role,
        &base_status,
        &org_id,
        &toast,
        &toast,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Ok(AccUser::from(row))
            } else {
                Err(AccessResult {
                    result: false,
                    error_code: Some("UNK_ERROR".to_string()),
                })
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Err(AccessResult {
                result: false,
                error_code: Some("UNK_ERROR".to_string()),
            })
        }
    }
}

static SQL_CREATE_REG_TASK: &'static str = r#"
    INSERT INTO {SCHEMA}.tasks
    (status, task_type, data, roles, location)
    VALUES ('PENDING', 'REGISTRATION_REQUEST', $1, ARRAY['ACCOUNT_ADMIN'], NULL) RETURNING uuid;
"#;

#[derive(Debug, Serialize)]
pub struct TaskPayload {
    pub uid: Uuid,
    pub name: String,
    pub email: String,
    pub org_id: Option<Uuid>,
    pub org_name: Option<String>,
}

// Create a dummy task with pending stauts, we're waiting on the user to verify their email
fn create_dummy_task(conn: &PoolConnection, user: &TaskPayload, tki: &String) -> Result<Uuid, ()> {
    unimplemented!()
}

static SQL_DELETE_SSO: &'static str = r#"
    DELETE FROM core.users WHERE uuid = $1;
"#;

static SQL_DELETE_ACC: &'static str = r#"
    DELETE FROM {SCHEMA}.accounts WHERE uuid = $1;
"#;

// Delete traces of the user from the system so that they can re-attempt registration later
fn delete_user(conn: &PoolConnection, id: &Uuid, tki: &String) -> Result<(), ()> {
    let _ = &conn.execute(&SQL_DELETE_SSO, &[&id]).unwrap();
    let _ = &conn.execute(&SQL_DELETE_ACC.replace("{SCHEMA}", &tki), &[&id]).unwrap();

    Ok(())
}


impl Handler<AccessRequest> for DbExecutor {
    type Result = Result<AccessResult, AccessResult>;

    fn handle(&mut self, msg: AccessRequest, _: &mut Self::Context) -> Self::Result {
        let conn = self.0.get().unwrap();
        match msg {
            AccessRequest::RegistrationRequest {name, email, password, org_id, org_name, host, email_svc} => {
                let tmp_domain: String = host.split(":").collect::<Vec<&str>>()[0].to_string();
                let account: Option<TargetAccount> = match get_account(&conn, &tmp_domain) {
                    Ok(res) => Some(res),
                    Err(_) => Option::None,
                };

                // Couldn't find the account in question
                if account.is_none() {
                    return Err(AccessResult {
                        result: false,
                        error_code: Some("UNK_ACCOUNT".to_string()),
                    });
                }
                let account = account.unwrap();

                // The account is inactive, rather than exposing this, return unknown account error
                if account.status == "INACTIVE".to_string() {
                    return Err(AccessResult {
                        result: false,
                        error_code: Some("INACTIVE_ACCOUNT".to_string()),
                    });
                }

                let user: Option<ExistingUser> = match get_user(&conn, &email) {
                    Ok(res) => Some(res),
                    Err(_) => Option::None,
                };

                // Go through the possible error scenarios if there's an already existing user
                if let Some(r_user) = user {
                    // A user with this email address exists in the system, check whether they have
                    // access to this account already

                    // The user is pending verification, tell them to check their email
                    
                    let user_status: String = r_user.status.clone();
                    match user_status.as_ref() {
                        "PENDING_VERIFICATION" => {
                            return Err(AccessResult {
                                result: false,
                                error_code: Some("PENDING_VERIFICATION".to_string()),
                            });
                        },
                        "INACTIVE" => {
                            return Err(AccessResult {
                                result: false,
                                error_code: Some("INACTIVE_USER".to_string()),
                            });
                        },
                        "REVOKED" => {
                            return Err(AccessResult {
                                result: false,
                                error_code: Some("REVOKED".to_string()),
                            });
                        },
                        _ => {}
                    }

                    let acc_user: Option<ExistingAccountUser> = match get_account_user(&conn, &r_user.uuid, &account.tki) {
                        Ok(res) => Some(res),
                        Err(_) => Option::None,
                    };


                    if let Some(r_acc) = acc_user {
                        // The user is pending approval
                        let acc_user_status: String = r_acc.status.clone();
                        match acc_user_status.as_ref() {
                            "PENDING_APPROVAL" => {
                                return Err(AccessResult {
                                    result: false,
                                    error_code: Some("PENDING_APPROVAL".to_string()),
                                });
                            },
                            "INACTIVE" => {
                                return Err(AccessResult {
                                    result: false,
                                    error_code: Some("REVOKED_ACCESS".to_string()),
                                });
                            },
                            _ => {}
                        }

                        // THe user is normative and already active
                        return Err(AccessResult {
                            result: false,
                            error_code: Some("EXISTING_USER".to_string()),
                        });
                    }

                    // The user is valid, but it's a user nonetheless, so they should go through
                    // and access request instead
                    return Err(AccessResult {
                        result: false,
                        error_code: Some("EXISTS_REQ_ACCESS".to_string()),
                    });
                }
                
                // Create the storable password
                let pass = generate_password(&password).unwrap();

                // Create the single-sign on user
                let new_user: SsoUser = create_sso_user(&conn, &email, &name, &pass, &account)?;

                // Create the account user
                let new_acc_user: AccUser = create_acc_user(&conn, &new_user, &org_id, &account.tki)?;
                // Create the approval task in a hidden state until the user has verified their
                // email address

                let payload = TaskPayload {
                    uid: new_user.uuid.clone(),
                    name: new_user.name.clone(),
                    email: new_user.name.clone(),
                    org_id: org_id.clone(),
                    org_name: org_name.clone(),
                };

                let task: Uuid = create_dummy_task(&conn, &payload, &account.tki).unwrap();

                // Send the verification email
                let subject = "EWARS Registration - Email Verification".to_string();

                let mut ctx = tera::Context::new();
                ctx.insert("verification_code", &new_user.uuid);
                ctx.insert("tki", &account.tki);
                ctx.insert("host", &account.domain);
                ctx.insert("account_name", &account.name);
                ctx.insert("subject", &subject);

                let s = TEMPLATES.render("verification.html", &ctx).unwrap();
                // Send the email verification email message
                let user_email = Email {
                    recipients: vec![new_user.email],
                    account_name: Some(account.name.clone()),
                    subject,
                    plain_content: Option::None,
                    html_content: s.to_string(),
                    system: true
                };

                email_svc.do_send(user_email);

                Ok(AccessResult {
                    result: true,
                    error_code: Option::None,
                })
            },
            AccessRequest::AccessRequest {uuid, org_id, org_name, host, email_svc} => {
                Err(AccessResult {
                    result: false,
                    error_code: Some("".to_string())
                })
            },
            _ => {
                Err(AccessResult {
                    result: false,
                    error_code: Some("UNK_ERROR".to_string()),
                })
            }
        }
    }
}

