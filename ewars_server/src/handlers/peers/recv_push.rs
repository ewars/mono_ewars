use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use chrono::prelude::*;
use uuid::Uuid;

use sonomalib::models::*;

use crate::handlers::api_handler::{PoolConnection};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PeerData {
    pub ts_ms: usize,
    pub events: Vec<EventLogEntry>,
    pub tki: String,
    pub did: Uuid,
    pub uid: Uuid,
}

static SQL_UPDATE_PEER_TS_MS: &'static str = r#"

"#;

macro_rules! replay {
    ($model: ident, $inbound: expr, $conn: expr, $tki: expr, $rid: expr) -> {
        {
            let is_insert = false;
            let stream: EventStream = EventStream::get_by_res_id(&$conn, &$tki, &$key)?;
            
            // If the stream doesn't contain anything before we start adding things, then one
            // of the items in the incoming events should be an insert, so we need to do an
            // insert at the end of this rather than udpate an existing record
            if stream.len() <= 0 {
                is_insert = true;
            }

            // Add the incoming events to the stream
            for item in $inbound {
                stream.insert(&item)?;
            }

            // If the stream is tombstoned there's no point in doing any of this as we'll
            // delete it at the end of the function anyway
            if !stream.is_tombstoned() {
                // Get the first item in the stream which will be the INSERT event
                let base: $model = json::from_value(stream[0].data.clone()).unwrap();

                // For each item in the stream that is an update, process it
                // and apply it to the base data
                for item in stream {
                    match item.event_type.as_ref() {
                        "UPDATE" => {
                            base.ingest_event(&item);
                        },
                        _ => {}
                    }
                }

                // If there's a tombstone in the stream, delete the record,
                // This can trigger other events, for instance a deletion of a location will
                // delete all associated records, alerts, children, etc...
                // these should be handle by other events and a change against a single record
                // shouldn't have side-effects
                if is_insert {
                    base.insert(&$conn, &$tki)?;
                } else {
                    base.update(&$conn, &$tki)?;
                }
            } else {
                $model::delete_by_id(&$conn, &$tki, &$key)?;
            }
        }
    }
}

pub fn recv_push(conn: &PoolConnection, data: Value) -> Result<PeerCommandResult, Error> {
    let data: PeerData = json::from_value(data).unwrap();
    let tki: String = data.tki.clone();

    let completed: Vec<Uuid> = Vec::new(); // events that have completed processing


    let mut orged: HashMap<Uuid, EventLogEntry> = HashMap::new();
    let mut ts_ms: usize = 0;
    
    // Organize updates by resource id
    for item in &data.events {
        orged.entry(item.res_id.clone())
            .and_modify(|x| *x.push(item.clone()))
            .or_insert(vec![item.clone()]);
    }

    for (key, values) in orged {
        values.sort_by(|a, b| a.ts_ms.partial_cmp(b.ts_ms).unwrap());
        let res_type: String = values[0].res_type.clone();
        let res_id: Uuid = values[0].res_id.clone();

        match res_type.as_ref() {
            "location" => replay!(Location, values, conn, tki, res_id),
            "record" => replay!(Record, values, conn, tki, expr),
            "form" => replay!(Form, values, conn, tki, expr),
            "location_type" => replay!(LocationType, values, conn, tki, res_id),
            "assignment" => replay!(Assignment, values, conn, tki, res_id),
            "user" => replay!(User, values, conn, tki, res_id),
            "alert" => replay!(Alert, values, conn, tki, res_id),
            "alarm" => replay!(Alarm, values, conn, tki, res_id),
            _ => {}
        }
    }

    unimplemented!()
}
