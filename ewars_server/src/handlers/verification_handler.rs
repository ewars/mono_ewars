use std::io;
use std::collections::HashMap;

use postgres::rows::{Row};

use actix::prelude::*;
use failure::Error;
use jwt::{encode, Header, Algorithm};
use uuid::Uuid;

use crate::db::db::DbExecutor;
use crate::share::common::Claims;

use crate::handlers::api_handler::{PoolConnection};

use tera;
use tera::{Tera, Context as TeraContext, Result as TeraResult};

use crate::services::{EmailService, Email};

lazy_static! {
    pub static ref TEMPLATES: Tera = {
        let mut tera = compile_templates!("templates/emails/**/*");
        tera.autoescape_on(vec!["html", ".sql"]);
        tera
    };
}


pub struct VerificationQuery {
    pub verification_code: Uuid,
    pub tki: String,
    pub host: String,
    pub email_svc: Addr<EmailService>,
}

impl Message for VerificationQuery {
    type Result = Result<VerificationResult, VerificationResult>;
}

#[derive(Debug, Clone)]
pub struct VerificationResult {
    pub success: bool,
    pub error_code: Option<String>,
}

static SQL_GET_USER: &'static str = r#"
    SELECT uuid, name, email
    FROM core.users
    WHERE ver_code = $1;
"#;

static SQL_GET_ACCOUNT: &'static str = r#" 
    SELECT uuid, name, domain, tki
    FROM core.accounts WHERE tki = $1;)
"#;

static SQL_UPDATE_USER: &'static str = r#"
    UPDATE core.users SET status = 'ACTIVE'
    WHERE uuid = $1;
"#;

static SQL_UPDATE_TASK: &'static str = r#"
    UPDATE {SCHEMA}.tasks
        SET status = 'OPEN'
    WHERE data->>'uid' = $1::TEXT
        AND task_type = 'REGISTRATION_REQUEST';
"#;

static SQL_GET_TASK_ID: &'static str = r#"
    SELECT uuid
    FROM {SCHEMA}.tasks
    WHERE data->>'uid' = $1::TEXT
        AND task_type = 'REGISTRATION_REQUEST';
"#;

static SQL_GET_ADMINS: &'static str = r#"
    SELECT email 
    FROM {SCHEMA}.users
    WHERE status = 'ACTIVE'
        AND role = 'ACCOUNT_ADMIN'
        AND email NOT ILIKE '%@ewars.ws%'
        AND system = FALSE
        AND email = TRUE;
"#;

impl Handler<VerificationQuery> for DbExecutor {
    type Result = Result<VerificationResult, VerificationResult>;

    fn handle(&mut self, msg: VerificationQuery, _: &mut Self::Context) -> Self::Result {
        let conn = self.0.get().unwrap();

        // Get the account that we're operating against
        let account: Option<(Uuid, String, String, String)> = match &conn.query(&SQL_GET_ACCOUNT, &[&msg.tki]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some((row.get(0), row.get(1), row.get(3), row.get(4)))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        };

        // No account that matches up
        if account.is_none() {
            return Err(VerificationResult {
                success: false,
                error_code: Some("UNK_ACCOUNT".to_string()),
            });
        }

        let (acc_id, acc_name, domain, tki) = account.unwrap();

        // Get the user in question using their uuid as the verification code
        // TODO: Need to revise this to an expiring verificiation code not directly associated
        // with the user
        let user: Option<(Uuid, String, String)> = match &conn.query(&SQL_GET_USER, &[&msg.verification_code]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some((row.get(0), row.get(1), row.get(2)))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        };

        // Can't find a user based on that verification code
        if user.is_none() {
            return Err(VerificationResult {
                success: false,
                error_code: Some("UNK_CODE".to_string()),
            });
        }

        let (uid, name, email) = user.unwrap();

        match &conn.execute(&SQL_UPDATE_USER, &[&uid]) {
            Ok(_) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                Err(VerificationResult {
                    success: false,
                    error_code: Some("UNK_ERROR".to_string()),
                })
            }
        }?;

        match &conn.execute(&SQL_UPDATE_TASK.replace("{SCHEMA}", &tki), &[&uid]) {
            Ok(_) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                Err(VerificationResult {
                    success: false,
                    error_code: Some("UNK_ERROR".to_string()),
                })
            }
        }?;

        let task: Option<Uuid> = match &conn.query(&SQL_GET_TASK_ID.replace("{SCHEMA}", &tki), &[&uid]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(row.get(0))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        };

        if task.is_none() {
            return Err(VerificationResult {
                success: false,
                error_code: Some("UNK_ERROR".to_string()),
            });
        }

        // Send out an email to account admins that there's a registration request pending
        let mut ctx = tera::Context::new();

        let subject = "[EWARS] New registration request".to_string();
        ctx.insert("user_name", &name);
        ctx.insert("email", &email);
        ctx.insert("account_name", &acc_name);
        ctx.insert("host", &domain);
        ctx.insert("task_id", &task.unwrap().to_string());
        ctx.insert("subject", &subject);

        let admins: Vec<String> = match &conn.query(&SQL_GET_ADMINS.replace("{SCHEMA}", &tki), &[]) {
            Ok(rows) => {
                rows.iter().map(|x| x.get(0)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };

        let s = TEMPLATES.render("registration_request.html", &ctx).unwrap();
        // Send the email verification email message
        let user_email = Email {
            recipients: admins,
            account_name: Some(acc_name.clone()),
            subject,
            plain_content: Option::None,
            html_content: s.to_string(),
            system: true
        };

        msg.email_svc.do_send(user_email);

        Ok(VerificationResult {
            success: true,
            error_code: Option::None
        })
    }
}

