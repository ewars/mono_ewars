use std::collections::HashMap;
use std::str;

use serde_json as json;
use serde_json::Value;

use actix::prelude::*;

use uuid::Uuid;
use chrono::prelude::*;
use base64::{decode};
use tera;
use tera::{Tera};

use crate::handlers::command_handler::{CommandResult, DbCommand};
use crate::handlers::api_handler::{PoolConnection};
use crate::services::{Email, EmailService};

use crate::models::{CoreUser, Account};
use crate::app::{Org};

lazy_static! {
    pub static ref TERA: Tera = {
        let mut tera = compile_templates!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*"));
        tera
    };
}

// TODO: decode the code
// TODO: Get the user record for the user
// TODO: Get the account record
// TODO: Create the registration task for the specific account
// TODO: Send a notiifcation email to ACCOUNT_ADMINS for task

pub fn verify_user(conn: &PoolConnection, code: &String, email_svc: Addr<EmailService>) -> Result<CommandResult, ()> {
    let buf = decode(&code).unwrap();
    let decoded = match str::from_utf8(&buf) {
        Ok(v) => v,
        Err(err) => {
            panic!("Invalid UTF-8 sequence: {}", err);
        }
    };
    let decoded: String = decoded.to_string();


    let mut uid: Option<Uuid> = Option::None;
    let mut tki: Option<String> = Option::None;
    let mut org_id_str: Option<String> = Option::None;
    let mut org_name_str: Option<String> = Option::None;

    for item in decoded.split(";") {
        let splat: Vec<String> = item.split(":").map(|x| x.to_string()).collect();
        match splat[0].as_ref() {
            "u" => {
                uid = Some(Uuid::parse_str(&splat[1].clone()).unwrap());
            },
            "a" => {
                tki = Some(splat[1].clone());
            },
            "o" => {
                org_id_str = Some(splat[1].clone());
            },
            "on" => {
                org_name_str = Some(splat[1].clone());
            },
            _ => {}
        }
    }

    if tki.is_none() {
        return Ok(CommandResult::VerificationResult(false, Some("ERR_UNK_TKI".to_string())));
    }
    let tki: String = tki.unwrap();

    if uid.is_none() {
        return Ok(CommandResult::VerificationResult(false, Some("ERR_UNK_UID".to_string())));
    }
    let uid: Uuid = uid.unwrap();

    let user: Option<CoreUser> = CoreUser::from_uuid(&conn, &uid);
    let account: Option<Account> = Account::from_tki(&conn, &tki);

    if account.is_none() {
        return Ok(CommandResult::VerificationResult(false, Some("ERR_UNK_ACCOUNT".to_string())));
    }
    let account: Account = account.unwrap();

    // Can't find the user this is suppossed to correlate to
    if user.is_none() {
        return Ok(CommandResult::VerificationResult(false, Some("ERR_UNKNOWN".to_string())));
    }
    let user: CoreUser = user.unwrap();

    if &user.status == "PENDING_VERIFICATION" {
        // This user is definitely pending verification, we're going to verify them and 
        // start the process
        let result: bool = activate_user(&conn, &uid);

        if !result {
            return Ok(CommandResult::VerificationResult(false, Some("ERR_UNKNOWN".to_string())));
        }

        // Get the org_id as a UUID
        let mut org_id: Option<Org> = Option::None;
        if let Some(oid) = org_id_str {
            if &oid != "" {
                if &oid == "OTHER" {
                    org_id = Some(Org::Str(oid.clone()));
                } else {
                    org_id = Some(Org::Id(Uuid::parse_str(&oid).unwrap()));
                }
            }
        }

        // Get an org_name if the org_id is None
        let mut org_name: Option<String> = Option::None;
        if let Some(oname) = org_name_str {
            if &oname != "" {
                org_name = Some(oname.clone());
            }
        }
        let task_id: Option<Uuid> = create_task(&conn, &account.tki, &uid, &user.email, &user.name, org_id.clone(), org_name.clone());

        // We need them to specify an org name
        let task_id: Uuid = task_id.unwrap();

        
        let mut ctx = tera::Context::new();
        ctx.insert("domain", &account.domain);
        ctx.insert("task_id", &task_id);
        ctx.insert("user_email", &user.email);
        ctx.insert("user_name", &user.name);
        ctx.insert("account_name", &account.name);
        ctx.insert("subject", &format!("[EWARS] Access request received - {}", account.name.clone()));

        let mut email_content: Option<String> = Option::None;
        match TERA.render("emails/access_request.html", &ctx) {
            Ok(res) => {
                email_content = Some(res);
            },
            Err(err) => {
                eprintln!("{:?}", err);
            }
        }

        //TODO: Get account admins
        let account_admins: Vec<String> = get_account_admins(&conn, &account.tki);

        if let Some(c) = email_content {
            email_svc.do_send(Email {
                recipients: account_admins.clone(),
                account_name: Some(account.name.clone()),
                subject: format!("[EWARS] Access request received - {}", account.name.clone()),
                plain_content: Option::None,
                html_content: c.clone(),
                system: true,
            });
        }

    } else {
        let u_status: String = user.status.clone();

        match u_status.as_ref() {
            "ACTIVE" => {
                return Ok(CommandResult::VerificationResult(false, Some("ERR_VERIFIED".to_string())));
            },
            "REVOKED" => {
                return Ok(CommandResult::VerificationResult(false, Some("ERR_REVOKED".to_string())));
            },
            _ => {
                return Ok(CommandResult::VerificationResult(false, Some("ERR_UNKNOWN".to_string())));
            }
        }

        return Ok(CommandResult::VerificationResult(false, Some("ERR_UNKNOWN".to_string())));
    }

    // fall through
    Ok(CommandResult::VerificationResult(false, Some("ERR_UNKNOWN".to_string())))
}


static SQL_ACTIVATE_USER: &'static str = r#"
    UPDATE core.users
    SET status = 'ACTIVE'
    WHERE uuid = $1;
"#;

fn activate_user(conn: &PoolConnection, uid: &Uuid) -> bool {
    match &conn.execute(&SQL_ACTIVATE_USER, &[&uid]) {
        Ok(_) => true,
        Err(err) => {
            eprintln!("{:?}", err);
            false
        }
    }
}


static SQL_GET_ADMINS: &'static str = r#" 
    SELECT email 
    FROM {SCHEMA}.users
    WHERE status = 'ACTIVE' AND role = 'ACCOUNT_ADMIN';
"#;

pub fn get_account_admins(conn: &PoolConnection, tki: &String) -> Vec<String> {
    match &conn.query(&SQL_GET_ADMINS.replace("{SCHEMA}", &tki), &[]) {
        Ok(rows) => {
            rows.iter().map(|x| x.get(0)).collect()
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Vec::new()
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TaskData {
    pub uid: Uuid,
    pub email: String,
    pub name: String,
    pub org_id: Option<Org>,
    pub org_name: Option<String>,
}

static SQL_CREATE_TASK: &'static str = r#"
    INSERT INTO {SCHEMA}.tasks
    (status, task_type, data, context, roles)
    VALUES ('OPEN', 'ACCESS_REQUEST', $1, $2, $3) RETURNING uuid;
"#;


// Create the task for the user registration
pub fn create_task(conn: &PoolConnection, tki: &String, uid: &Uuid, email: &String, name: &String, org_id: Option<Org>, org_name: Option<String>) -> Option<Uuid> {
    let task_data = TaskData {
        uid: uid.clone(),
        email: email.clone(),
        name: name.clone(),
        org_id: org_id.clone(),
        org_name: org_name.clone(),
    };
    let t_data_val: Value = json::to_value(task_data).unwrap();

    let mut t_context: HashMap<String, String> = HashMap::new();
    let t_context_val: Value = json::to_value(t_context).unwrap();

    let roles: Vec<String> = vec!["ACCOUNT_ADMIN".to_string()];

    match &conn.query(&SQL_CREATE_TASK.replace("{SCHEMA}", &tki), &[
        &t_data_val,
        &t_context_val,
        &roles,
    ]) {
        Ok(rows) => {
            if let Some(row) = rows.iter().next() {
                Some(row.get(0))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    }

}
