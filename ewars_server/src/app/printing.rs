use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use uuid::Uuid;
use chrono::prelude::*;

use crate::api::forms::{Field, FormFeature};
#[macro_use]
use crate::api::utils::*;

use crate::handlers::api_handler::{PoolConnection};
use crate::handlers::command_handler::{CommandResult};


#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum RecordValue {
    Str(String),
    Int(i32),
    Float(f64),
    List(Vec<RecordValue>),
    UUID(Uuid),
    Date(NaiveDate),
}


#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct PrintField  {
    pub name: String,
    pub label: String,
    pub field_type: String,
    pub value: Option<String>,
    pub children: Option<Vec<PrintField>>,
    pub order: i32,
    pub header_style: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Record {
    pub uuid: Uuid,
    pub eid: Option<String>,
    pub data: HashMap<String, Value>,
    pub submitted: DateTime<Utc>,
    pub submitter: Option<String>,
    pub form_name: HashMap<String, String>,
    pub location_name: Option<HashMap<String, String>>,
    pub location_type_name: Option<HashMap<String, String>>,
    pub location_lineage: Option<Vec<String>>,
    pub definition: HashMap<String, Field>,
    pub events: Option<Value>,
    pub fields: Vec<PrintField>,
}

impl Record {
    pub fn get_form_name(&self, lang: &str) -> String {

        if let Some(ref c) = self.form_name.get(lang) {
            c.clone().to_string()
        } else {
            "Unknown Form".to_string()
        }
    }
}

impl<'a> From<Row<'a>> for Record {
    fn from(row: Row) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();
        let form_name: HashMap<String, String> = json::from_value(row.get("form_name")).unwrap();
        let definition: HashMap<String, Field> = match json::from_value(row.get("definition")) {
            Ok(res) => res,
            Err(err) => {
                eprintln!("{:?}", err);
                HashMap::new()
            }
        };
        let location_name: Option<HashMap<String, String>> = json::from_value(row.get("location_name")).unwrap();
        let location_type_name: Option<HashMap<String, String>> = json::from_value(row.get("location_type_name")).unwrap();

        Record {
            uuid: row.get("uuid"),
            eid: row.get("eid"),
            data: data,
            submitted: row.get("submitted"),
            submitter: row.get("submitter"),
            form_name,
            location_name,
            location_type_name,
            location_lineage: match row.get_opt("location_lineage") {
                Some(res) => res.unwrap(),
                None => Option::None
            },
            definition: definition,
            events: row.get("events"),
            fields: Vec::new(),
        }
    }
}

impl<'a> Record {
    pub fn get_value(&self, name: &String) -> Option<Value> {
        if let Some(c) = self.data.clone().get(name) {
            Some(c.clone())
        } else {
            Option::None
        }
    }
}

static SQL_GET_RECORD: &'static str = r#"
    SELECT r.uuid, 
        r.eid, 
        r.data,
        f.features,
        r.submitted,
        uu.name || ' ' || uu.email AS submitter,
        f.name AS form_name,
        l.name AS location_name,
        lt.name AS location_type_name,
        (SELECT array(
            SELECT name->>'en' FROM {SCHEMA}.locations AS t
            WHERE t.uuid::TEXT = ANY(l.lineage::TEXT[])
            ORDER BY array_length(t.lineage, 1)
        )) AS location_lineage,
        f.definition AS definition,
        r.revisions AS events
    FROM {SCHEMA}.records AS r
        LEFT JOIN {SCHEMA}.forms AS f ON f.uuid = r.fid
        LEFT JOIN {SCHEMA}.locations AS l ON l.uuid::TEXT = r.data->>'__lid__'
        LEFT JOIN {SCHEMA}.location_Types AS lt ON lt.uuid = l.lti
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.submitted_by
    WHERE r.uuid = $1;
"#;

#[derive(Debug, Deserialize, Clone)]
pub struct Location {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
}

impl<'a> From<Row<'a>> for Location {
    fn from(row: Row) -> Self {
        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();
        Location {
            uuid: row.get("uuid"),
            name: name,
        }
    }
}

impl Location {
    pub fn get_name(&self, lang: &str) -> Option<String> {
        if let Some(c) = self.name.clone().get(&lang.to_string()) {
            Some(c.to_string())
        } else {
            Option::None
        }
    }
}

static SQL_GET_LOCATION: &'static str = r#"
    SELECT uuid, name
    FROM {SCHEMA}.locations 
    WHERE uuid = $1;
"#;

// Get all the data necessary to print out a record
pub fn get_print_record(tki: &String, record_id: &Uuid, conn: &PoolConnection) -> Result<CommandResult, ()> {
    let record: Option<Record> = match &conn.query(&SQL_GET_RECORD.replace("{SCHEMA}", &tki), &[&record_id]) {
        Ok(rows) => {
            if let Some(c) = rows.iter().next() {
                Some(Record::from(c))
            } else {
                Option::None
            }
        },
        Err(err) => {
            eprintln!("{:?}", err);
            Option::None
        }
    };

    if record.is_none() {
        return Err(());
    }
    let mut record: Record = record.unwrap();


    let mut fields: Vec<PrintField> = Vec::new();

    for (name, field) in &record.definition {
        match field.field_type.as_ref() {
            "text" => {
                let val: Option<Value> = record.get_value(&name);

                let mut res: Option<String> = Option::None;
                if let Some(c) = val {
                    res = json::from_value(c).unwrap();
                }

                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: field.field_type.clone(),
                    value: res,
                    order: field.order(),
                    children: Option::None,
                    header_style: Option::None,
                })
            },
            "matrix" => {
                let mut child_rows: Vec<PrintField> = Vec::new();

                if let Some(rows) = field.fields.clone() {
                    for (row_name, row_field) in rows {
                        let mut child_cells: Vec<PrintField> = Vec::new();

                        if let Some(cells) = row_field.fields.clone() {
                            for (cell_name, cell_field) in cells {

                                let key_name: String = format!("{}.{}.{}", &name, &row_name, &cell_name);
                                let val: Option<Value> = record.get_value(&key_name);

                                let mut res: Option<String> = Option::None;

                                if let Some(c) = val {
                                    res = get_value(&c);
                                }

                                child_cells.push(PrintField {
                                    name: cell_name.clone(),
                                    label: cell_field.get_label("en"),
                                    field_type: cell_field.field_type.clone(),
                                    order: cell_field.order(),
                                    children: Option::None,
                                    value:  res,
                                    header_style: Option::None,
                                });
                            }
                        }

                        child_cells.sort_by_key(|x| x.order);
                        child_rows.push(PrintField {
                            name: row_name.clone(),
                            label: row_field.get_label("en"),
                            field_type: "row".to_string(),
                            order: row_field.order(),
                            children: Some(child_cells),
                            value: Option::None,
                            header_style: Option::None,
                        });
                    }
                }

                child_rows.sort_by_key(|x| x.order);
                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: field.field_type.clone(),
                    order: field.order(),
                    value: Option::None,
                    children: Some(child_rows),
                    header_style: Option::None,
                })
            },
            "textarea" => {
                let val: Option<Value> = record.get_value(&name);

                let mut res: Option<String> = Option::None;
                if let Some(c) = val {
                    res = json::from_value(c).unwrap();
                }

                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: field.field_type.clone(),
                    order: field.order(),
                    value: res,
                    children: Option::None,
                    header_style: Option::None,
                })
            },
            "date" => {
                let val: Option<Value> = record.get_value(&name);

                let mut res: Option<String> = Option::None;
                if let Some(c) = val {
                    let dt: NaiveDate = NaiveDate::parse_from_str(&c.as_str().unwrap(), "%Y-%m-%d").unwrap();
                    res = Some(dt.format("%Y-%m-%d").to_string());
                }

                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: field.field_type.clone(),
                    value: res,
                    order: field.order(),
                    children: Option::None,
                    header_style: Option::None,
                })
            },
            "location" => {
                let val: Option<Value> = record.get_value(&name);
                let mut res: Option<String> = Option::None;

                if let Some(c) = val {
                    let id: Uuid = json::from_value(c).unwrap();
                    let location: Option<Location> = match &conn.query(&SQL_GET_LOCATION.replace("{SCHEMA}", &tki), &[&id]) {
                        Ok(rows) => {
                            if let Some(c) = rows.iter().next() {
                                Some(Location::from(c))
                            } else {
                                Option::None
                            }
                        },
                        Err(err) => {
                            eprintln!("{:?}", err);
                            Option::None
                        }
                    };
                    if let Some(loc) = location {
                        res = loc.get_name("en");
                    }
                }

                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: field.field_type.clone(),
                    value: res,
                    order: field.order(),
                    children: Option::None,
                    header_style:Option::None,
                });
            },
            "select" => {
                let val: Option<Value> = record.get_value(&name);

                let mut result: Option<String> = Option::None;
                if let Some(c) = val {
                    if c.is_array() {
                        let res: Vec<String> = json::from_value(c).unwrap();
                        result = Some(res.join(", "));
                    } else {
                        result = Some(json::from_value(c).unwrap());
                    }
                }

                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: field.field_type.clone(),
                    value: result,
                    order: field.order(),
                    children: Option::None,
                    header_style: Option::None,
                })
                
            },
            "number" => {
                let val: Option<Value> = record.get_value(&name);

                let mut res: Option<String> = Option::None;

                if let Some(c) = val {
                    if c.is_number() {
                        res = Some(json::from_value(c).unwrap());
                    } else {
                        res = Some(json::from_value(c).unwrap());
                    }
                }

                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: field.field_type.clone(),
                    value: res,
                    order: field.order(),
                    children: Option::None,
                    header_style: Option::None,
                });
            },
            "interval" => {
                let val: Option<Value> = record.get_value(&name);

                let mut res: Option<String> = Option::None;
                if let Some(c) = val {
                    res = get_value(&c);
                }

                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: "interval".to_string(),
                    value: res,
                    order: field.order(),
                    children: Option::None,
                    header_style: Option::None,
                });
            },
            "header" => {
                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: "header".to_string(),
                    value: Option::None,
                    order: field.order(),
                    children: Option::None,
                    header_style: Some("style-title".to_string()),
                })
            },
            "display" => {
                fields.push(PrintField {
                    name: name.clone(),
                    label: field.get_label("en"),
                    field_type: "display".to_string(),
                    value: Option::None,
                    order: field.order(),
                    children: Option::None,
                    header_style: Option::None,
                });
            },
            _ => {},
        }
    }

    fields.sort_by_key(|x| x.order);
    record.fields = fields.clone();

    Ok(CommandResult::PrintRecord(record))
}


fn get_value(val: &Value) -> Option<String> {
    if !val.is_null() {
        if val.is_string() {
            return Some(val.to_string());
        }

        if val.is_i64() {
            let a_int: i64 = val.as_i64().unwrap();
            return Some(a_int.to_string());
        }

        if val.is_u64() {
            let a_int: u64 = val.as_u64().unwrap();
            return Some(a_int.to_string());
        }

        if val.is_f64() {
            let a_f: f64 = val.as_f64().unwrap();
            return Some(a_f.to_string());
        }

        if val.is_array() {
            let res: Vec<String> = json::from_value(val.clone()).unwrap();
            return Some(res.join(", "));
        }

        return Option::None;

    } else {
        return Option::None
    }
}
