use std::collections::HashMap;
use futures::future;
use futures::Future;

use tera;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use actix_web::{AsyncResponder, Error, HttpRequest, FutureResponse, HttpResponse, error};
use actix_web::http::{header, StatusCode};
use actix_web::middleware::identity::RequestIdentity;

use crate::api::forms::{Field, FormFeature};
use crate::models::{UserSession};
use crate::handlers::command_handler::{DbCommand, CommandResult};
use crate::app::{Record as PrintRecord};

use crate::state::AppState;


pub fn print_record(req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let record_id: Option<Uuid> = match req.match_info().get("record_id") {
        Some(c) => {
            Some(Uuid::parse_str(c).unwrap())
        }, 
        None => {
            Option::None
        }
    };

    // Return 404 if the record can't be found
    if record_id.is_none() {
        return Box::new(future::ok((HttpResponse::NotFound().content_type("text/html").body("Record not found"))));
    }
    let record_id: Uuid = record_id.unwrap();


    let mut user_session: Option<UserSession> = None;
    let mut end_role: UserSession;
    if let Some(ref session_user) = req.identity() {
        let session_user: UserSession = json::from_str(&session_user).unwrap();
        user_session = Some(UserSession {
            uuid: session_user.uuid.clone(),
            aid: session_user.aid.clone(),
            name: session_user.name.clone(),
            email: session_user.email.clone(),
            role: session_user.role,
            tki: session_user.tki.clone(),
            account_name: session_user.account_name.clone(),
        });
    }

    if user_session.is_none() {
        return Box::new(future::ok(HttpResponse::NotFound()
                                   .content_type("text/html")
                                   .body("")));
    }
    let user_session: UserSession = user_session.unwrap();



    let r = req.clone();
    req.state().db.send(DbCommand::GetPrintRecord(user_session.tki.clone(), record_id.clone()))
        .from_err()
        .and_then(move |res| {

            let record: Option<PrintRecord> = match res.unwrap() {
                CommandResult::PrintRecord(d) => {
                    Some(d)
                },
                _ => Option::None
            };

            if record.is_none() {
                Ok(HttpResponse::NotFound().into())
            } else {
                let record: PrintRecord = record.unwrap();

                let mut ctx = tera::Context::new();
                ctx.insert("record", &record);
                ctx.insert("theme", &"flex".to_string());

                if let Some(ref loc) = record.location_lineage {
                    ctx.insert("location", &loc.join(" \\ "));
                }

                ctx.insert("form_name", &record.get_form_name("en"));
                let s = r.state().template
                    .render("record_print.html", &ctx)
                    .map_err(|err| {
                        eprintln!("{:?}", err);
                        error::ErrorInternalServerError("TEMPLATE_ERROR")
                    }).unwrap();


                Ok(HttpResponse::Ok().content_type("text/html").body(s).into())
            }
        }).responder()
}
