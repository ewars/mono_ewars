pub mod component;
mod record_print;
mod access_request;
mod verification;
mod register;

pub use self::register::{handle_registration};
pub use self::record_print::{print_record};
pub use self::access_request::{handle_access_request};
pub use self::verification::{verify_email};
