use std::collections::HashMap;

use futures::{future, Future};

use tera;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use actix::prelude::*;
use actix_web::{AsyncResponder, Json, Error, HttpRequest, HttpResponse, error, FutureResponse};
use actix_web::http::{header, StatusCode};
use actix_web::middleware::identity::RequestIdentity;

use crate::state::AppState;
use crate::handlers::command_handler::{DbCommand, CommandResult};
use crate::services::{EmailService};


pub fn verify_email(req: &HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    let ver_code: String = req.match_info().query("ver_code").unwrap();
    let email_svc: Addr<EmailService> = req.state().email_svc.clone();

    let r = req.clone();
    req.state().db
        .send(DbCommand::VerificationRequest(ver_code.clone(), email_svc))
        .from_err()
        .and_then(move |res| {
            
            let mut result: bool = false;
            let mut error_code: Option<String> = Option::None;

            match res.unwrap() {
                CommandResult::VerificationResult(success, e_code) => {
                    if success {
                        result = true;
                    } else {
                        error_code = e_code.clone();
                    }
                },
                _ => {
                    result = false;
                    error_code = Some("UNK_ERROR".to_string());
                }
            }

            let mut ctx = tera::Context::new();

            ctx.insert("success", &result);
            ctx.insert("error_code", &error_code);

            let mut error_message: &str = "";
            if let Some(error) = error_code {
                match error.as_ref() {
                    "ERR_VERIFIED" => {
                        error_message = "The user account you are trying to verify is already verified.";
                    },
                    _ => {
                        error_message = "An unknown error occurred, please contact support@ewars.ws for more information.";
                    }
                }
            }
            ctx.insert("error_message", error_message);


            let s = r.state().template
                .render("verified.html", &ctx)
                .map_err(|err| {
                    eprintln!("{:?}", err);
                    error::ErrorInternalServerError("TEMPLATE_ERROR")
                }).unwrap();

            Ok(HttpResponse::Ok()
               .content_type("text/html")
               .body(s).into())

        }).responder()
}
