use serde_json::Value;

use postgres::rows::{Row};
use chrono::prelude::*;
use uuid::Uuid;

use crate::models::utils::*;
use crate::handlers::api_handler::{PoolConnection};
use crate::models::{Account};
use crate::utils::auth::*;
use crate::models::utils::*;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CoreUser {
    pub uuid: Uuid,
    pub id: i32,
    pub name: String,
    pub pin_code: Option<i32>,
    pub email: String,
    pub password: String,
    pub password_pbkdf2: Option<String>,
    pub accounts: Option<Vec<Uuid>>,
    pub status: String,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for CoreUser {
    fn from(row: Row) -> Self {
        CoreUser {
            uuid: row.get("uuid"),
            id: row.get("id"),
            name: row.get("name"),
            pin_code: row.get("pin_code"),
            email: row.get("email"),
            password: row.get("password"),
            password_pbkdf2: row.get("password_pbkdf2"),
            accounts: row.get("accounts"),
            status: row.get("status"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

static SQL_GET_CORE_USER_BY_EMAIL: &'static str = r#"
    SELECT * FROM core.users WHERE email = $1;
"#;

static SQL_GET_CORE_USER_BY_UUID: &'static str = r#"
    SELECT * FROM core.users WHERE uuid = $1;
"#;

static SQL_CREATE_SSO_USER: &'static str = r#"
    INSERT INTO core.users 
    (name, email, password, password_pbkdf2, status, accounts)
    VALUES ($1, $2, $3, $4, $5, $6) RETURNING *;
"#;

static SQL_VERIFY_USER: &'static str = r#"
    UPDATE core.users
    SET status = 'ACTIVE'
    WHERE uuid = $1;
"#;

impl CoreUser {
    pub fn from_email(conn: &PoolConnection, email: &String) -> Option<Self> {
        query_single!(conn, SQL_GET_CORE_USER_BY_EMAIL, CoreUser, &[&email])
    }

    pub fn from_uuid(conn: &PoolConnection, uid: &Uuid) -> Option<Self> {
        query_single!(conn, SQL_GET_CORE_USER_BY_UUID, CoreUser, &[&uid])
    }

    // Create a new user
    pub fn create(conn: &PoolConnection, email: &String, name: &String, password: &String) -> Option<Self> {
        let new_pass: String = generate_password(&password).unwrap();
        let mut pbkdf2_pass: String = "".to_string();

        let accounts: Vec<Uuid> = Vec::new();

        match &conn.query(&SQL_CREATE_SSO_USER, &[
            &name,
            &email,
            &new_pass,
            &pbkdf2_pass,
            &"PENDING_VERIFICATION",
            &accounts,
        ]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(CoreUser::from(row))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        }
    }

    // Update a users statis from AWAITING_VERIFICATION
    pub fn verify(conn: &PoolConnection, uid: &Uuid) -> Result<(), ()> {
        match &conn.execute(&SQL_VERIFY_USER, &[&uid]) {
            Ok(res) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                Err(())
            }
        }
    }

    // Get full account information for all accounts that this user has access to
    pub fn get_accounts(&self, conn: &PoolConnection) -> Vec<Account> {
        unimplemented!()
    }

    pub fn update(&self, conn: &PoolConnection) -> Result<Self, ()> {
        unimplemented!()
    }
}
