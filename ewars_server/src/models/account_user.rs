use std::collections::HashMap;

use uuid::Uuid;

use serde_json::Value;

pub struct AccountUser {
    pub uuid: String,
    pub aid: Uuid,
    pub name: String,
    pub email: String,
    pub password: String,
    pub sec_password: String,
    pub role: String,
    pub status: HashMap<String, bool>,
    pub permissions: HashMap<String, HashMap<String, bool>>,
    pub settings: Value,
    pub profile: Value,
    pub org_id: Option<String>,
    pub created: DateTime<Utc>,
    pub created_by: String,
    pub modified: DateTime<Utc>,
    pub modified_by: String,
}
