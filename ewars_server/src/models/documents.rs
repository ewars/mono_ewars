use serde_json as json;
use serde_json::Value;

pub struct Document {
    pub uuid: String,
    pub template_name: LanguageString,
    pub instance_name: LanguageString,
    pub description: LanguageString,
    pub status: String,
    pub version: u32,
    pub theme: Option<String>,
    pub layout: Value,
    pub content: Value,
    pub data: Value,
    pub generation_type: String,
    pub start_date: Date<Utc>,
    pub end_date: Option<Date<Utc>>,
    pub template_type: String,
    pub orientation: String,
    pub permissions: HashMap<&'static str, Value>,
    pub created: DateTime<Utc>,
    pub created_by: String,
    pub modified: DateTime<Utc>,
    pub modified_by: String,
}