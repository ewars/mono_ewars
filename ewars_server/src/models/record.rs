use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordRevision {
    pub uuid: Uuid,
    pub data: HashMap<String, Value>,
    pub uid: Uuid,
    pub ts: DateTime<Utc>,
    pub reason: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordComment {
    pub uuid: Uuid,
    pub content: Value,
    pub uid: Uuid,
    pub ts: DateTime<Utc>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Record {
    pub uuid: Uuid,
    pub status: String,
    pub stage: Option<String>,
    pub workflow: Option<Value>,
    pub fid: Uuid,
    pub data: HashMap<String, Value>,
    pub submitted: DateTime<Utc>,
    pub submitted_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub revisions: Option<Vec<RecordRevision>>,
    pub comments: Option<Vec<RecordComment>>,
    pub eid: Option<String>,
    pub import_id: Option<Uuid>,
    pub source: Option<String>,
}

impl<'a> From<Row<'a>> for Record {
    fn from(row: Row) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();
        let revisions: Option<Vec<RecordRevision>> = json::from_value(row.get("revisions")).unwrap();
        let comments: Option<Vec<RecordComment>> = json::from_value(row.get("comments")).unwrap();
        Record {
            uuid: row.get("uuid"),
            status: row.get("status"),
            stage: row.get("stage"),
            workflow: row.get("workflow"),
            fid: row.get("fid"),
            data: data,
            submitted: row.get("submitted"),
            submitted_by: row.get("submitted_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            revisions: revisions,
            comments: comments,
            eid: row.get("eid"),
            import_id: row.get("import_id"),
            source: row.get("source"),
        }
    }
}

static SQL_GET_RECORD: &'static str = r#"
    SELECT * FROM {SCHEMA}.records
    WHERE uuid = $1;
"#;

static SQL_UPDATE_STATUS: &'static str = r#"
    UPDATE {SCHEMA}.records
    SET status = $1 WHERE uuid = $2;
"#;

static SQL_DELETE_RECORD: &'static str = r#"
    DELETE FROM {SCHEMA}.records 
    WHERE uuid = $1;
"#;

impl Record {
    // Delete this record
    pub fn delete_record(&self, conn: &PoolConnection, tki: &String) -> Result<(), ()> {
        match &conn.execute(&SQL_DELETE_RECORD.replace("{SCHEMA}", &tki), &[&self.uuid]) {
            Ok(_) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                Err(())
            }
        }
    }

    pub fn get_record_by_id(conn: &PoolConnection, tki: &String, id: &Uuid) -> Option<Record> {
        match &conn.query(&SQL_GET_RECORD.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(Record::from(row))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        }
    }

    pub fn update_status(conn: &PoolConnection, tki: &String, id: &Uuid, status: &'static str) -> bool {
        match &conn.execute(&SQL_UPDATE_STATUS.replace("{SCHEMA}", &tki), &[&status, &id]) {
            Ok(_) => true,
            Err(err) => {
                eprintln!("{:?}", err);
                false
            }
        }
    }
    
    // Get the canonical record date for this record based on the interval reporting field
    // if no date is available return the submitted date.
    pub fn get_record_date(&self) -> Option<Date<Utc>> {
        if let Some(c) = self.data.clone().get("__dd__") {
            let dt: String = json::from_value(c.clone()).unwrap();
            let dtand: String = format!("{}T09:10:11Z", dt);
            let date: DateTime<Utc> = dtand.parse::<DateTime<Utc>>().unwrap();
            Some(date.date())
        } else {
            Some(self.submitted.clone().date())
        }
    }
}
