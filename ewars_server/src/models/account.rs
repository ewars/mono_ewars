use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use chrono::prelude::*;
use uuid::Uuid;

use crate::models::utils::*;
use crate::handlers::api_handler::{PoolConnection};

pub struct Account {
    pub uuid: Uuid,
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub language: Option<String>,
    pub status: String,
    pub domain: String,
    pub theme: Option<Value>,
    pub tki: String,
    pub config: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for Account {
    fn from(row: Row) -> Self {
        Account {
            uuid: row.get("uuid"),
            id: row.get("id"),
            name: row.get("name"),
            description: row.get("description"),
            language: row.get("language"),
            status: row.get("status"),
            domain: row.get("domain"),
            theme: row.get("theme"),
            tki: row.get("tki"),
            config: row.get("config"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}


static SQL_GET_BY_TKI: &'static str = r#"
     SELECT * FROM core.accounts
     WHERE tki = $1;
"#;

static SQL_GET_BY_ID: &'static str = r#"
    SELECT * FROM core.accounts
    WHERE uuid = $1;
"#;

static SQL_GET_BY_DOMAIN: &'static str = r#"
    SELECT * FROM core.accounts
    WHERE domain = $1;
"#;


impl Account {
    pub fn from_tki(conn: &PoolConnection, tki: &String) -> Option<Self> {
        query_single!(conn, SQL_GET_BY_TKI, Account, &[&tki])
    }

    pub fn from_uuid(conn: &PoolConnection, id: &Uuid) -> Option<Self> {
        query_single!(conn, SQL_GET_BY_ID, Account, &[&id])
    }

    pub fn from_domain(conn: &PoolConnection, domain: &String) -> Option<Self> {
        query_single!(conn, SQL_GET_BY_DOMAIN, Account, &[&domain])
    }
}
