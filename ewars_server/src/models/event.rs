use serde_json as json;
use serde_json::Value;

use postgres::{Row};

use chrono::prelude::*;
use uuid::{Uuid};

pub type ChangeSet = Vec<(&'static str, Value)>;
pub type ChangeData = HashMap<&'static str, Value>;

pub enum EventData {
    ChangeSet,
    ChangeData,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Event {
    pub uuid: &'static str,
    pub ref_type: &'static str,
    pub ref_id: &'static str,
    pub ref_ts: u32,
    pub command: &'static str,
    pub data: Option<EventData>,
    pub source: &'static str,
    pub ts_ms: u32,
    pub applied: bool,
    pub source_version: u32,
    pub device_id: &'static str,
}

impl Event {
    pub fn from_row(row: &Row) -> Event {
        Event {
            uuid: row.get("uuid"),
            ref_type: row.get("ref_type"),
            ref_id: row.get("ref_id"),
            ref_ts: row.get("ref_ts"),
            command: row.get("command"),
            data: row.get("data"),
            source: row.get("source"),
            ts_ms: row.get("ts_ms"),
            applied: row.get("applied"),
            source_version: row.get("source_version"),
            device_id: row.get("device_id"),
        }
    }
}