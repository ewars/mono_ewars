use serde_json as json;
use serde_json::Value;

use uuid::Uuid;

use crate::models::{Record};
use crate::handlers::api_handler::{PoolConnection};


#[derive(Debug, Clone)]
pub struct DeletedRecord {

}

static SQL_CREATE_RECORD: &'static str = r#"
    INSERT INTO {SCHEMA}.deleted_records
    (uuid, data, reason, deleted_by)
    VALUES ($1, $2, $3, $4);
"#;

impl DeletedRecord {
    pub fn create(conn: &PoolConnection, tki: &String, rec: &Record, reason: &String, uid: &Uuid) -> Result<(), ()> {
        let val: Value = json::to_value(rec).unwrap();

        match &conn.execute(&SQL_CREATE_RECORD.replace("{SCHEMA}", &tki), &[
            &rec.uuid,
            &val,
            &reason,
            &uid,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                Err(())
            }
        }
    }
}
