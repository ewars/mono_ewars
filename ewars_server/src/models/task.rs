use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::{Row};

use crate::handlers::api_handler::{PoolConnection};

#[derive(Debug, Serialize, Deserialize)]
pub struct Task {
    pub uuid: Uuid,
    pub status: String,
    pub task_type: String,
    pub data: Value,
    pub actions: Option<Value>,
    pub version: i32,
    pub form: Option<Value>,
    pub context: Value,
    pub roles: Vec<String>,
    pub location: Option<Uuid>,
    pub outcome: Option<Value>,
    pub actioned: Option<DateTime<Utc>>,
    pub actioned_by: Option<Uuid>,
    pub created: DateTime<Utc>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a> From<Row<'a>> for Task {
    fn from(row: Row) -> Self {
        Task {
            uuid: row.get("uuid"),
            status: row.get("status"),
            task_type: row.get("task_type"),
            data: row.get("data"),
            actions: row.get("actions"),
            version: row.get("version"),
            form: row.get("form"),
            context: row.get("context"),
            roles: row.get("roles"),
            location: row.get("location"),
            outcome: row.get("outcome"),
            actioned: row.get("actioned"),
            actioned_by: row.get("actioned_by"),
            created: row.get("created"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

static SQL_GET_TASK: &'static str = r#"
    SELECT * FROM {SCHEMA}.tasks WHERE uuid = $1;
"#;

static SQL_CREATE_TASK: &'static str = r#"
    INSERT INTO {SCHEMA}.tasks
    (status, task_type, data, context, roles)
    VALUES ('OPEN', $1, $2, $3, $4);
"#;

static SQL_CLOSE_TASK: &'static str = r#"
    UPDATE {SCHEMA}.tasks
    SET status = 'CLOSED',
        outcome = $1,
        actioned = NOW(),
        actioned_by = $2
    WHERE uuid = $3;
"#;

impl Task {
    pub fn create_task(conn: &PoolConnection, tki: &String, context: &Value, data: &Value, task_type: &str) -> Result<(), ()> {
        let roles: Vec<String> = match task_type.as_ref() {
            "RETRACTION_REQUEST" => vec!["ACCOUNT_ADMIN".to_string(), "REGIONAL_ADMIN".to_string()],
            "AMENDMENT_REQUEST" => vec!["ACCOUNT_ADMIN".to_string(), "REGIONAL_ADMIN".to_string()],
            "ACCESS_REQUEST" => vec!["ACCOUNT_ADMIN".to_string()],
            "REGISTRATION_REQUEST" => vec!["ACCOUNT_ADMIN".to_string()],
            "ASSIGNMENT_REQUEST" => vec!["ACCOUNT_ADMIN".to_string(), "REGIONAL_ADMIN".to_string()],
            _ => vec!["ACCOUNT_ADMIN".to_string()],
        };

        match &conn.execute(&SQL_CREATE_TASK.replace("{SCHEMA}", &tki), &[
            &task_type,
            &data,
            &context,
            &roles,
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                Err(())
            }
        }
    }

    pub fn close_task(conn: &PoolConnection, tki: &String, tid: &Uuid, outcome: &Value, uid: &Uuid) -> bool {
        match &conn.execute(&SQL_CLOSE_TASK.replace("{SCHEMA}", &tki), &[
            &outcome,
            &uid,
            &tid,
        ]) {
            Ok(_) => true,
            Err(err) => {
                eprintln!("{:?}", err);
                false
            }
        }
    }

    // Get a task by it's id
    pub fn get_task_by_id(conn: &PoolConnection, tki: &String, id: &Uuid) -> Option<Task> {
        match &conn.query(&SQL_GET_TASK.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Some(Task::from(row))
                } else {
                    Option::None
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Option::None
            }
        }
    }
}
