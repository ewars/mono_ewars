use serde_json as json;
use serde_json::Value;

use uuid::Uuid;
use chrono::prelude::*;
use postgres::rows::Row;

use crate::handlers::api_handler::{PoolConnection};
use crate::models::Assignment;

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT a.*, l.name AS location_name
    FROM {SCHEMA}.assignments AS a
    LEFT JOIN {SCHEMA}.locations AS l ON l.uuid = a.lid
    WHERE a.uid = $1;
"#;

pub struct User {
    pub uuid: Uuid,
}

impl User {
    pub fn get_assignments(conn: &PoolConnection, id: &Uuid) -> Result<Vec<Assignment>, ()> {
        unimplemented!()
    }

    pub fn get_locations_for_form(conn: &PoolConnection, tki: &String, id: &Uuid, fid: &Uuid) -> Result<Vec<(Uuid, Value)>, ()> {
        let assigns: Vec<Assignment> = match &conn.query(&SQL_GET_ASSIGNMENTS.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                rows.iter().map(|x| Assignment::from(x)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };

        let loc_based: Vec<Assignment> = assigns.iter().filter(|x| x.has_location()).cloned().collect();
        let form_based: Vec<Assignment> = loc_based.iter().filter(|x| &&x.fid() == &fid).cloned().collect();

        let mut locations: Vec<(Uuid, Value)> = Vec::new();

        for item in form_based {
            let sub_locations: Vec<(Uuid, Value)> = item.get_derived_locations(&conn, &tki);
            locations.extend(sub_locations);
        }


        Ok(locations)
    }

    pub fn get_locations_admin(conn: &PoolConnection, tki: &String, id: &Uuid) -> Result<Vec<(Uuid, Value)>, ()> {
        let assigns: Vec<Assignment> = match &conn.query(&SQL_GET_ASSIGNMENTS.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                rows.iter().map(|x| Assignment::from(x)).collect()
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Vec::new()
            }
        };


        let locations: Vec<(Uuid, Value)> = assigns.iter().map(|x| (x.lid.clone().unwrap(), x.location_name.clone().unwrap())).collect();

        Ok(locations)
    }
}
