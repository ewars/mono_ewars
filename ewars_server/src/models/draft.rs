use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;
use postgres::rows::{Row};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Draft {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub data: Value,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
    pub form_name: Option<Value>,
    pub location_name: Option<Value>,
}

macro_rules! extract {
    ($field: expr, $row: expr) => {
        {
            match $row.get_opt($field) {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

impl<'a> From<Row<'a>> for Draft {
    fn from(row: Row) -> Self {
        Draft {
            uuid: row.get("uuid"),
            fid: row.get("fid"),
            data: row.get("data"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
            form_name: extract!("form_name", row),
            location_name: extract!("location_name", row),
        }
    }
}
