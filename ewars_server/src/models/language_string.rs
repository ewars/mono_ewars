use serde_json as json;
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LanguageString {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub en: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fr: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ar: Option<String>,
}

impl LanguageString {
    pub fn get_english(&self) -> String {
        self.en.clone().unwrap()
    }
}

impl Default for LanguageString {
    fn default() -> LanguageString {
        LanguageString {
            en: Some("".to_string()),
            fr: Option::None,
            ar: Option::None
        }
    }
}