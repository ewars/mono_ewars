use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use postgres::rows::{Row};
use uuid::Uuid;
use chrono::prelude::*;

use crate::handlers::api_handler::{PoolConnection};
use crate::models::language_string::LanguageString;

static SQL_GET_FORM: &'static str = r#"
    SELECT r.*,
        uc.name AS creator_name,
        uc.email AS creator_email,
        uu.name AS updater_name,
        uc.email AS updater_email
    FROM {SCHEMA}.forms AS r
        LEFT JOIN {SCHEMA}.users AS uc ON uc.uuid = r.created_by
        LEFT JOIN {SCHEMA}.users AS uu ON uu.uuid = r.modified_by
    WHERE r.uuid = $1;
"#;

pub type Rule = (String, String, String);
pub type RuleGroup = Vec<(String, String, String)>;

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum FieldCondition {
    Rule((String, String, String)),
    RuleGroup(Vec<(String, String, String)>),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FeatureDefinition {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interval: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub threshold: Option<NumericValue>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocationFeature {
    pub lti: Uuid,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum FormFeature {
    Definition(FeatureDefinition),
    Location(LocationFeature),
    Toggle(bool),
}

impl FeatureDefinition {
    pub fn threshold(&self) -> Option<i64> {
        if let Some(c) = self.threshold.clone() {
            match c {
                NumericValue::Int(v) => {
                    Some(v as i64)
                },
                NumericValue::Float(v) => {
                    let a_int: i64 = v as i64;
                    Some(a_int)
                },
                NumericValue::Str(c) => {
                    let a_int: i64 = c.parse().unwrap();
                    Some(a_int)
                },
                _ => Option::None
            }
        } else {
            Option::None
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum NumericValue {
    Int(i32),
    Float(f64),
    Str(String),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum LabelValue {
    I18N(HashMap<String, String>),
    Standard(String),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Field {
    pub uuid: Option<Uuid>,
    pub name: Option<String>,
    pub label: LabelValue,
    pub options: Option<Vec<(String, String)>>,
    pub required: Option<bool>,
    #[serde(rename="type")]
    pub field_type: String,
    pub allow_future_dates: Option<bool>,
    pub show_mobile: Option<bool>,
    pub order: NumericValue,
    pub help: Option<LabelValue>,
    pub conditions: Option<Value>,
    pub conditional_bool: Option<bool>,
    pub interval: Option<String>,
    pub date_type: Option<String>,
    pub min: Option<NumericValue>,
    pub max: Option<NumericValue>,
    pub multi_select: Option<bool>,
    pub header_style: Option<String>,
    pub fields: Option<HashMap<String, Field>>,
    pub lti: Option<Uuid>,
}

impl Field {
    // get the display value for a given field and value
    // // only used for select-based fields
    pub fn get_select_display_value(&self, val: &String) -> Option<String> {
        let mut dis_val: String = val.clone();

        if let Some(options) = self.options.clone() {
            for item in options.iter() {
                if &item.0 == val {
                    dis_val = item.1.clone();
                }
            }
        }
        Some(dis_val)
    }

    // Get the language dependent label for the field
    pub fn get_label(&self, lang: &str) -> String {
        match self.label.clone() {
            LabelValue::Standard(c) => {
                c.to_string()
            },
            LabelValue::I18N(c) => {
                if let Some(d) = c.get(lang) {
                    d.to_string()
                } else {
                    "".to_string()
                }
            },
            _ => {
                "".to_string()
            }
        }
    }


    // Get the order from the field as an i32
    pub fn order(&self) -> i32 {
        match self.order.clone() {
            NumericValue::Int(c) => {
                c as i32
            },
            NumericValue::Float(c) => {
                let a_int: i32 = c as i32;
                a_int
            },
            NumericValue::Str(c) => {
                let o_int: i32 = c.parse().unwrap();
                o_int
            },
            _ => {
                0
            }
        }
    }

    // Get a child field from this field based on its path
    pub fn get_sub_field(&self, key: &String) -> Option<Field> {
        let path: Vec<String> = key.split(".").map(|x| x.to_string()).collect();

        let row_key: String = path[0].clone();
        let cell_key: String = path[1].clone();

        let mut result: Option<Field> = Option::None;

        if let Some(fields) = &self.fields {
            let row: Option<Field> = match &fields.get(&row_key) {
                Some(res) => Some(res.clone().to_owned()),
                None => Option::None
            };

            if let Some(row) = row {
                if let Some(row_fields) = &row.fields {
                    let cell: Option<Field> = match &row_fields.get(&cell_key) {
                        Some(res) => Some(res.clone().to_owned()),
                        None => Option::None
                    };

                    cell
                } else {
                    Option::None
                }
            } else {
                Option::None
            }
        } else {
            Option::None
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Form {
    pub uuid: Uuid,
    pub name: Value,
    pub status: String,
    pub description: Option<String>,
    pub version: i32,
    pub guidance: Option<String>,
    pub eid_prefix: Option<String>,
    pub features: HashMap<String, FormFeature>,
    pub stages: Option<Value>,
    pub definition: HashMap<String, Field>,
    pub etl: Value,
    pub changes: Option<Value>,
    pub permissions: Option<Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>
}

impl Form {
    pub fn get_fields_as_vec_path(&self) -> Vec<String> {
        let mut fields: Vec<String> = Vec::new();

        for (key, val) in &self.definition {
            let field_type: String = val.field_type.clone();
            match field_type.as_ref() {
                "matrix" => {
                    // Iterate down through to get the actual field names
                    if let Some(rows) = &val.fields { // Get rows
                        for (row_key, row_val) in rows { // iterate through rows
                            if let Some(cells) = &row_val.fields { // get cells
                                for (cell_key, cell_val) in cells { // iterate cells
                                    fields.push(format!("{}.{}.{}", key, row_key, cell_key));
                                }
                            }
                        }
                    }
                },
                "row" => {},
                "display" => {},
                "calculated" => {},
                _ => { 
                    fields.push(key.clone());
                }

            }
        }

        fields
    }

    // Format a date based on the interval type for this form type, 
    // if there is no interval-based reporting field in the form
    // then format as logner date string
    pub fn format_date(&self, dt: &Date<Utc>) -> String {
        if let Some(c) = self.definition.get("__dd__") {
            if let Some(interval) = &c.interval {
                match interval.as_ref() {
                    "WEEK" => {
                        dt.format("%G W%V").to_string()
                    },
                    "YEAR" => {
                        dt.format("%Y").to_string()
                    },
                    _ => {
                        dt.format("%Y-%m-%d").to_string()
                    }
                }
            } else {
                dt.format("%Y-%m-%d").to_string()
            }
        } else {
            dt.format("%Y-%m-%d").to_string()
        }
    }

    // Get a form by it's id
    pub fn get_form_by_id(conn: &PoolConnection, tki: &String, id: &Uuid) -> Result<Form, ()> {
        match &conn.query(&SQL_GET_FORM.replace("{SCHEMA}", &tki), &[&id]) {
            Ok(rows) => {
                if let Some(row) = rows.iter().next() {
                    Ok(Self::from(row))
                } else {
                    Err(())
                }
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Err(())
            }
        }
    }

    // Get location fields from the form
    pub fn get_loc_fields(&self) -> Vec<(String, Field)> {
        let mut fields: Vec<(String, Field)> = Vec::new();

        for (key, field) in &self.definition {
            if &field.field_type == "location" {
                fields.push((key.clone(), field.clone()));
            }
        }

        fields
    }

    // Get all text-based fields from the form
    pub fn get_text_fields(&self) -> Vec<(String, Field)> {
        let mut fields: Vec<(String, Field)> = Vec::new();

        for (key, field) in &self.definition {
            if &field.field_type == "text" {
                fields.push((key.clone(), field.clone()));
            }

            if &field.field_type == "textarea" {
                fields.push((key.clone(), field.clone()));
            }
        }

        fields
    }

    // Get the field which is considered the canonical location reporting field
    // based on the feature setting of the form
    pub fn get_reporting_location_field(&self) -> Option<Field> {
        match self.definition.get("__lid__") {
            Some(res) => Some(res.clone()),
            None => Option::None
        }
    }

    // Get the reporting location type for a form
    pub fn get_reporting_location_type(&self) -> Option<Uuid> {
        if let Some(c) = self.definition.get("__lid__") {
            if let Some(r) = c.lti {
                Some(r)
            } else {
                Option::None
            }
        } else {
            Option::None
        }
    }

    // Get a field by it's keyname
    pub fn get_field(&self, key: &String) -> Option<Field> {
        if key.contains(".") {
            let path: Vec<String> = key.split(".").map(|x| x.to_string()).collect();
            let mut cur_field: Option<Field> = Option::None;
            if path.len() == 3 {
                // If this is a path thats three long its' most likely a matrix
                if let Some(matrix) = self.definition.clone().get(&path[0]) {
                    match matrix.get_sub_field(&path[1..].join(".")) {
                        Some(res) => Some(res),
                        None => Option::None
                    }
                } else {
                    Option::None
                }
            } else {
                Option::None
            }
        } else {
            if let Some(c) = self.definition.clone().get(&key.clone()) {
                Some(c.clone())
            } else {
                Option::None
            }
        }
    }
}

impl<'a> From<Row<'a>> for Form {
    fn from(row: Row) -> Self {
        let features: HashMap<String, FormFeature> = json::from_value(row.get("features")).unwrap();
        let definition: HashMap<String, Field> = json::from_value(row.get("definition")).unwrap();

        Form {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            version: row.get("version"),
            guidance: row.get("guidance"),
            eid_prefix: row.get("eid_prefix"),
            features: features,
            stages: row.get("stages"),
            definition: definition,
            etl: row.get("etl"),
            changes: row.get("changes"),
            permissions: row.get("permissions"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}
