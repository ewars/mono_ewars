use std::collections::{HashMap, VecDeque};
use std::net;

use futures::{Stream};

use actix::prelude::*;
use chrono::prelude::*;
use uuid::Uuid;

use tokio::net::{TcpListener, TcpStream};
use tokio::codec::{FramedRead};
use tokio::io::{AsyncRead};

use crate::peer_server::{PeerConnection};
use crate::peer_server::{PeerCodec, NewPeerConnection, PeerDisconnected};
use crate::db::db::{DbExecutor};

static DEFAULT_ADDRESS: &'static str = r#"127.0.0.1:9009"#;

#[derive(Message)]
struct IncomingConnection(pub TcpStream, pub net::SocketAddr);

#[derive(Clone)]
pub struct Peer {
    pub addr: Addr<PeerConnection>,
    pub did: String,
    pub last_seen: DateTime<Utc>,
}

#[derive(Clone)]
pub struct PeerServer {
    pub peers: HashMap<Uuid, Addr<PeerConnection>>,
    pub db: Addr<DbExecutor>,
}

impl Actor for PeerServer {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let addr = "127.0.0.1:6142".parse().unwrap();
        let listener = TcpListener::bind(&addr).unwrap();

        ctx.add_message_stream(listener.incoming()
                               .map_err(|e| { 
                                    eprintln!("{:?}", e);
                               })
                               .map(|st| {
            let addr = st.peer_addr().unwrap();
            IncomingConnection(st, addr)
        }));
        eprintln!("Peer Server: Started - 127.0.0.1:6142");
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        eprintln!("Peer Server: Stopping....");
        Running::Stop
    }
}

impl Supervised for PeerServer {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("Peer Server: Restarting...");
    }
}

impl Drop for PeerServer {
    fn drop(&mut self) {
        eprintln!("Peer Server: Dropped");
    }
}

// Handles when a new connection is being built up
impl Handler<IncomingConnection> for PeerServer {
    type Result = ();

    // Handle an incoming new TCP connection and turn it into a new PeerConnection
    fn handle(&mut self, msg: IncomingConnection, ctx: &mut Self::Context) {
        let server_context = ctx.address();
        let cloned_db = self.db.clone();
        let session = PeerConnection::create(|actx| {
            eprintln!("Peer Server: New peer");
            let (r, w) = msg.0.split();

            PeerConnection::add_stream(FramedRead::new(r, PeerCodec), actx);
            PeerConnection::new(server_context, actix::io::FramedWrite::new(w, PeerCodec, actx), cloned_db)
        });
    }
}

// Handle when a new peer connection has been established
impl Handler<NewPeerConnection> for PeerServer {
    type Result = ();

    fn handle(&mut self, msg: NewPeerConnection, _: &mut Self::Context) -> Self::Result {
        self.peers.entry(msg.uuid.clone())
            .and_modify(|x| *x = msg.addr.clone())
            .or_insert(msg.addr.clone());
        ()
    }
}

// Handle when a peer disconnects
impl Handler<PeerDisconnected> for PeerServer {
    type Result = ();

    fn handle(&mut self, msg: PeerDisconnected, _: &mut Self::Context) -> Self::Result {
        eprintln!("PEER_DISCONNECTED");
    }
}

impl PeerServer {
    pub fn new(db: Addr<DbExecutor>) -> Self {
        PeerServer {
            peers: HashMap::new(),
            db: db,
        }
    }
}
