CREATE OR REPLACE FUNCTION fn_resource_delete()
  RETURNS TRIGGER AS
$BODY$
DECLARE
  event_id UUID;
  res_type TEXT;
  query    TEXT;
BEGIN
  IF (TG_TABLE_NAME = 'records')
  THEN
    res_type = 'RECORD';
  ELSIF (TG_TABLE_NAME = 'alarms')
    THEN
      res_type = 'ALARM';
  ELSIF (TG_TABLE_NAME = 'tasks')
    THEN
      res_type = 'TASK';
  ELSIF (TG_TABLE_NAME = 'resources')
    THEN
      res_type = 'RESOURCE';
  ELSIF (TG_TABLE_NAME = 'alerts')
    THEN
      res_type = 'ALERT';
  ELSIF (TG_TABLE_NAME = 'docuemnts')
    THEN
      res_type = 'DOCUMENT';
  ELSIF (TG_TABLE_NAME = 'assignments')
    THEN
      res_type = 'ASSIGNMENT';
  ELSIF (TG_TABLE_NAME = 'devices')
    THEN
      res_type = 'DEVICE';
  ELSIF (TG_TABLE_NAME = 'forms')
    THEN
      res_type = 'FORM';
  ELSIF (TG_TABLE_NAME = 'location_types')
    THEN
      res_type = 'LOCATION_TYPE';
  ELSIF (TG_TABLE_NAME = 'locations')
    THEN
      res_type = 'LOCATION';
  ELSIF (TG_TABLE_NAME = 'teams')
    THEN
      res_type = 'TEAM';
  ELSIF (TG_TABLE_NAME = 'team_messages')
    THEN
      res_type = 'TEAM_MESSAGE';
  ELSIF (TG_TABLE_NAME = 'indicators')
    THEN
      res_type = 'INDICATOR';
  ELSIF (TG_TABLE_NAME = 'indicator_groups')
    THEN
      res_type = 'INDICATOR_GROUP';
  ELSIF (TG_TABLE_NAME = 'reporting')
    THEN
      res_type = 'REPORTING';
  ELSIF (TG_TABLE_NAME = 'roles')
    THEN
      res_type = 'ROLE';
  ELSIF (TG_TABLE_NAME = 'intervals')
    THEN
      res_type = 'INTERVAL';
  END IF;

  query := 'INSERT INTO ' || QUOTE_IDENT(TG_TABLE_SCHEMA) ||
           '.event_log (ref_type, ref_id, ref_ts, command, data, source, ts_ms, applied, source_version, device_id) VALUES ( '
           || QUOTE_LITERAL(res_type) ||
           ', ' || QUOTE_LITERAL(NEW.uuid) || ', ' ||
           QUOTE_LITERAL(EXTRACT(epoch FROM NOW()) || ', ' || QUOTE_LITERAL('DELETE') ||
                         ',NULL' || ', ' || QUOTE_LITERAL('WEB') || ', ' ||
                         QUOTE_LITERAL(EXTRACT(epoch FROM NOW())) || ',TRUE , 0' ||
                                       ', ' || QUOTE_LITERAL('GLOBAL') || ') RETURNING uuid;';

  EXECUTE query
  INTO event_id;

  EXECUTE 'SELECT NOTIFY sonoma, ' ||
          QUOTE_NULLABLE(json_build_object('TASK_TYPE', 'DELETE', 'tki', TG_TABLE_SCHEMA, 'eid', event_id)) || ';';
  RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql
VOLATILE;