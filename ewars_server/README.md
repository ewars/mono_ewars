# Sonome Server

## Notes

The `cert.pem` and `key.pem` certificates should **only** be used for development purposes only as they are self-signed certificates used for TLS testing.

## Configuration



## Building for linux

### From OSX or Windows

We currently use the provided Vagrantfile to build the sonoma distributable for linux systems. At the moment we currently build for Ubuntu Linux specifically. There are some pre-requisites to building for this environment that you can see in the provided `Vagrantfile` in the repo.

In order to get started building for linux

```
vagrant up && vagrant ssh;
cd /opt/sonoma;
cargo build --release;
./scripts/package-linux.sh;
```

### On Linux to Linux

Install the pre-requisites `libssl-dev`, `pkg-config` and `openssl` 

```
cd /path/to/sonoma;
cargo build --release;
./scripts/package-linux.sh;
```

## Building for Windows

The windows distributable must be built on a windows machine, we currently use a virtual machine in Amaazon EC@ running Windows Server.

`tbw`
