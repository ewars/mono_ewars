window.__ENAME__ = "registration"; window.__EVERSION__ = "0.0.1";
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defTab = document.getElementById("def-tab"),
    accTab = document.getElementById("acc-tab"),
    helpTab = document.getElementById("help-tab"),
    defContent = document.getElementById("def-content"),
    accContent = document.getElementById("acc-content"),
    helpContent = document.getElementById("help-content"),
    regTitle = document.getElementById("reg-title"),
    regUnderTitle = document.getElementById("reg-desc"),
    subBtn = document.getElementById("sub-btn"),
    footer = document.getElementById("footer");

var rName = document.getElementById("name"),
    rEmail = document.getElementById("email"),
    rEmailConfirm = document.getElementById("email_confirm"),
    rPassword = document.getElementById("password"),
    rPasswordConfirm = document.getElementById("password_config");

defTab.addEventListener("click", function () {
    defTab.setAttribute("class", "ux-tab active");
    accTab.setAttribute("class", "ux-tab");
    helpTab.setAttribute("class", "ux-tab");
    footer.style.display = "block";

    defContent.style.display = "block";
    accContent.style.display = "none";
    helpContent.style.display = "none";

    regTitle.innerText = "New User Registration";
    regUnderTitle.innerText = "Please follow the steps below to register for an account.";
    subBtn.innerText = "Submit registration";
});

accTab.addEventListener("click", function () {
    defTab.setAttribute("class", "ux-tab");
    accTab.setAttribute("class", "ux-tab active");
    helpTab.setAttribute("class", "ux-tab");
    footer.style.display = "block";

    defContent.style.display = "none";
    accContent.style.display = "block";
    helpContent.style.display = "none";

    regTitle.innerText = "Account Access Request";
    regUnderTitle.innerText = "Fill out the form below to request access to this account.";
    subBtn.innerText = "Request access";
});

helpTab.addEventListener("click", function () {
    defTab.setAttribute("class", "ux-tab");
    accTab.setAttribute("class", 'ux-tab');
    helpTab.setAttribute("class", "ux-tab active");
    footer.style.display = "none";

    defContent.style.display = "none";
    accContent.style.display = "none";
    helpContent.style.display = "block";

    regTitle.innerText = "Registration Help";
    regUnderTitle.innerText = "Guidance on registering for EWARS.";
});

/***/ })
/******/ ]);
//# sourceMappingURL=registration-dev.map?[contentHash]