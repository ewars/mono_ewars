window.__ENAME__ = "indicators"; window.__EVERSION__ = "1.0.6";
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 58);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(1);
var core = __webpack_require__(0);
var ctx = __webpack_require__(39);
var hide = __webpack_require__(6);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(10);
var IE8_DOM_DEFINE = __webpack_require__(40);
var toPrimitive = __webpack_require__(21);
var dP = Object.defineProperty;

exports.f = __webpack_require__(4) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(7)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 5 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(3);
var createDesc = __webpack_require__(17);
module.exports = __webpack_require__(4) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(44);
var defined = __webpack_require__(23);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(26)('wks');
var uid = __webpack_require__(19);
var Symbol = __webpack_require__(1).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(11);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(71), __esModule: true };

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(38);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(48);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(95);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(99);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(48);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(43);
var enumBugKeys = __webpack_require__(27);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 19 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 20 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(11);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _assign = __webpack_require__(42);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _assign2.default || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/***/ }),
/* 23 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(26)('keys');
var uid = __webpack_require__(19);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(1);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),
/* 27 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 28 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(23);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(10);
var dPs = __webpack_require__(79);
var enumBugKeys = __webpack_require__(27);
var IE_PROTO = __webpack_require__(25)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(41)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(80).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(3).f;
var has = __webpack_require__(5);
var TAG = __webpack_require__(9)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(9);


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(1);
var core = __webpack_require__(0);
var LIBRARY = __webpack_require__(30);
var wksExt = __webpack_require__(34);
var defineProperty = __webpack_require__(3).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Spinner = __webpack_require__(110);

window.__hack__ = document.getElementById("application");

var PRESET_TEMPLATES = {
    form: React.createClass({
        displayName: "form",

        _onClick: function _onClick() {
            this.props.onClick(this.props.data);
        },

        render: function render() {
            var formName, accountName;
            var item = this.props.data[2];
            var className = 'item';
            formName = ewars.I18N(item.name);
            if (item.account) accountName = ewars.I18N(item.account.name);
            if (this.props.value == this.props.data[0]) className += " active";

            return React.createElement(
                "div",
                { onClick: this._onClick, className: className },
                React.createElement(
                    "div",
                    { className: "ew-list-item-title" },
                    formName
                ),
                React.createElement(
                    "span",
                    null,
                    accountName
                )
            );
        }
    }),
    alarm: React.createClass({
        displayName: "alarm",

        _onClick: function _onClick() {
            this.props.onClick(this.props.data);
        },

        render: function render() {
            var alarmName, accountName;
            var item = this.props.data[2];
            var className = 'item';
            if (this.props.value == this.props.data[0]) className += " active";
            alarmName = ewars.I18N(item.name);
            accountName = ewars.I18N(item.account.name);

            return React.createElement(
                "div",
                { onClick: this._onClick, className: className },
                React.createElement(
                    "div",
                    { className: "ew list-item-title" },
                    alarmName
                ),
                React.createElement(
                    "span",
                    null,
                    accountName
                )
            );
        }
    }),
    assignment: React.createClass({
        displayName: "assignment",

        _onClick: function _onClick() {
            this.props.onClick(this.props.data);
        },

        render: function render() {
            var shortName = ewars.I18N(this.props.data[2][0]);
            var longName = this.props.data[2][1];
            longName = longName.split(",");
            var longNameDOM = longName.map(function (item) {
                return React.createElement(
                    "span",
                    null,
                    item
                );
            });
            var className = 'item';
            if (this.props.value == this.props.data[0]) className += " active";

            return React.createElement(
                "div",
                { onClick: this._onClick, className: className },
                React.createElement(
                    "div",
                    { className: "ew list-item-title" },
                    shortName
                ),
                React.createElement(
                    "div",
                    { className: "loc-long-name" },
                    React.createElement(
                        "div",
                        { className: "wrap" },
                        longNameDOM
                    )
                )
            );
        }
    })
};

function _recurseOptions(items, label, val, level) {
    var options = [];
    var dashes = "";

    if (level) {
        dashes = _.times(level, function () {
            return " ";
        });
        dashes += "-";
    }

    _.each(items, function (option) {
        var itemLabel = dashes + " ";
        itemLabel += ewars.I18N(option[label]);

        options.push([option[val], itemLabel]);

        if (option.children.length > 0) {
            var subOptions = _recurseOptions(option.children, label, val, level + 1);
            options.push.apply(options, subOptions);
        }
    }, this);

    return options;
}

function _processExternalOptions(options, hProp) {
    var optionsMap = {};
    _.each(options, function (option) {
        if (!optionsMap[option.id]) optionsMap[option.id] = option;
        optionsMap[option.id].children = [];
    });

    _.each(optionsMap, function (option) {
        if (option.parent_id) {
            optionsMap[option.parent_id].children.push(option);
        }
    });

    var cleaned = _.filter(optionsMap, function (item) {
        if (item.parent_id) return false;
        return true;
    }, this);

    return cleaned;
}

var Handle = React.createClass({
    displayName: "Handle",

    render: function render() {
        var _this = this;

        var name = "No Selection";

        if (this.props.value) {
            var item = _.find(this.props.options, function (item) {
                if (item[0] == this.props.value) return true;
            }, this);

            if (item) name = item[1];
        }

        if (this.props.prefixed) {
            var suspects = this.props.prefixed.filter(function (item) {
                return item[0] == _this.props.value;
            });
            if (suspects.length > 0) {
                name = suspects[0][1];
            }
        }

        return React.createElement(
            "div",
            { className: "handle", onClick: this.props.onClick },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            null,
                            name
                        ),
                        React.createElement(
                            "td",
                            { width: "20px", className: "icon" },
                            React.createElement("i", { className: "fal fa-caret-down" })
                        )
                    )
                )
            )
        );
    }
});

var MultiSelectWrapper = React.createClass({
    displayName: "MultiSelectWrapper",

    getInitialState: function getInitialState() {
        return {};
    },

    _onClick: function _onClick() {
        this.props.onClick(this.props.data);
    },

    render: function render() {
        var iconClass = "fal";
        if (this.props.value) {
            if (this.props.value.indexOf(this.props.data[0]) >= 0) {
                iconClass += " fa-check-square";
            } else {
                iconClass += " fa-square";
            }
        } else {
            iconClass += " fa-square";
        }
        return React.createElement(
            "div",
            { className: "ew-select-multi-item" },
            React.createElement(
                "table",
                { width: "100%" },
                React.createElement(
                    "tbody",
                    null,
                    React.createElement(
                        "tr",
                        null,
                        React.createElement(
                            "td",
                            { width: "25px" },
                            React.createElement(
                                "div",
                                { className: "ew-select-check" },
                                React.createElement("i", { className: iconClass })
                            )
                        ),
                        React.createElement(
                            "td",
                            null,
                            this.props.children
                        )
                    )
                )
            )
        );
    }
});

var Item = React.createClass({
    displayName: "Item",

    _onClick: function _onClick() {
        this.props.onClick(this.props.data);
    },

    render: function render() {
        var className = 'item';
        if (this.props.value == this.props.data[0]) className += " active";

        return React.createElement(
            "div",
            { onClick: this._onClick, className: className },
            this.props.data[1]
        );
    }
});

var SelectField = React.createClass({
    displayName: "SelectField",

    _isLoaded: false,
    _initialLoaded: false,

    PropTypes: {
        config: React.PropTypes.object
    },

    getDefaultProps: function getDefaultProps() {
        return {
            path: null,
            name: null,
            config: {}
        };
    },

    getInitialProps: function getInitialProps() {
        return {
            name: null,
            config: {
                multiple: false,
                path: null,
                groups: false
            },
            emptyText: "No Selection"
        };
    },

    getInitialState: function getInitialState() {
        return {
            options: [],
            rawOptions: [],
            showOptions: false,
            placeKey: _.uniqueId("SELECT_")
        };
    },

    _init: function _init(props) {
        if (props.value && props.config.optionsSource) {
            if (props.config.multiple) {
                this._initialLoaded = true;
                if (this.isMounted()) this.forceUpdate();
                this._toggle(null);
                return;
            }
            var resource = props.config.optionsSource.resource.toLowerCase();
            var select = [props.config.optionsSource.labelSource, props.config.optionsSource.valSource];
            if (props.config.optionsSource.select) {
                select = props.config.optionsSource.select;
            }

            if (this.props.config.optionsSource.additionalProperties) {
                select = select.concat(this.props.config.optionsSource.additionalProperties);
            }

            var isPrefix = void 0;
            if (props.config.optionsSource.prefixed) {
                var suspects = props.config.optionsSource.prefixed.filter(function (item) {
                    return item[0] == props.value;
                });
                if (suspects.length > 0) isPrefix = true;
            }
            console.log(isPrefix);

            if (!isPrefix) {
                ewars.tx("com.ewars.resource", [resource, props.value, select, null]).then(function (resp) {
                    if (resp) {
                        this.state.options = [[resp[props.config.optionsSource.valSource], ewars.I18N(resp[props.config.optionsSource.labelSource])]];
                    }

                    if (props.config) {
                        if (props.config.optionsSource) {
                            if (props.config.optionsSource.additional) {
                                _.each(props.config.optionsSource.additional, function (option) {
                                    var optionValue = option[0];
                                    if (optionValue == null) optionValue = "null";
                                    this.state.options.unshift([optionValue, option[1]]);
                                }, this);
                            }
                        }
                    }

                    this._initialLoaded = true;
                    if (this.isMounted()) this.forceUpdate();
                }.bind(this));
            } else {
                this._initialLoaded = true;
            }
        }
    },

    componentWillMount: function componentWillMount() {
        this._id = _.uniqueId("SELECT_");
        if (this.props.config && this.props.config.optionsSource && this.props.value) {
            this._init(this.props);
        } else {
            this._initialLoaded = true;
            this.state.options = this.props.config.options;
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        if (nextProps.config && nextProps.config.optionsSource && nextProps.value) {
            if (this.props.value != nextProps.value) this._init(nextProps);
        } else {
            this._initialLoaded = true;
            this.state.options = nextProps.config.options;
        }
    },

    componentDidMount: function componentDidMount() {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    },

    componentWillUnmount: function componentWillUnmount() {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
    },

    handleBodyClick: function handleBodyClick(evt) {
        if (this.refs.selector) {
            var area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showOptions = false;
                this.forceUpdate();
            }
        }
    },

    handleClick: function handleClick(e) {
        e.stopPropagation();
    },

    _selectNone: function _selectNone(e) {
        e.preventDefault();

        var name = this.props.config.nameOverride || this.props.name;

        this.props.onUpdate(name, [], this.props.path, null);
    },

    _selectAll: function _selectAll(e) {
        e.preventDefault();

        var name = this.props.config.nameOverride || this.props.name;

        var value = void 0;
        if (this.props.config.optionsSource) {
            value = this.state.rawOptions.map(function (item) {
                return item[this.props.config.optionsSource.valSource];
            }.bind(this));
        } else {
            value = this.props.config.options.map(function (item) {
                return item[0];
            }.bind(this));
        }

        this.props.onUpdate(name, value, this.props.path, null);
    },

    _onPrefixSelect: function _onPrefixSelect(e) {
        if (this.props.readOnly) return;
        var val = e.target.getAttribute('data-value');

        var name = this.props.config.nameOverride || this.props.name;
        this.state.showOptions = false;

        this.props.onUpdate(name, val, this.props.path, null);
    },

    onChange: function onChange(item) {
        if (this.props.readOnly) return;
        this.state.showOptions = false;

        var name = this.props.config.nameOverride || this.props.name;

        var path = this.props.path || null;

        var node = item;
        if (this.props.config.optionsSource) {
            node = _.find(this.state.rawOptions, function (result) {
                if (result[this.props.config.optionsSource.valSource] == item[0]) return true;
            }, this);
        }

        if (!this.props.config.multiple) {
            if (this.props.config) this.props.onUpdate(name, item[0], path, node);
            if (!this.props.config) this.props.onUpdate(name, item[0], path, node);
        } else {
            var newVal = [];
            if (this.props.value && this.props.value.constructor === Array) {
                newVal = this.props.value;
            }

            if (newVal.indexOf(item[0]) < 0) {
                newVal.push(item[0]);
            } else {
                var tmp = _.filter(newVal, function (p) {
                    if (p == item[0]) return false;
                    return true;
                }, this);
                newVal = tmp;
            }

            this.props.onUpdate(name, newVal, path, node);
        }
    },

    _processConfig: function _processConfig() {
        var config = {};
        if (this.props.config) config = this.props.config;

        if (config.multiple == undefined || config.multiple == null) config.multiple = false;
        return config;
    },

    _getOptionDisplay: function _getOptionDisplay() {
        var result;
        for (var i in this.state.options) {
            if (this.state.options[i][0] == this.props.value) {
                result = this.state.options[i][1];
            }
        }
        return result;
    },

    _getOptions: function _getOptions() {
        if (this.props.options) return this.props.options;
        if (this.props.config.options) return this.props.config.options;
        if (this.state.options) return this.state.options;
        return [];
    },

    _toggle: function _toggle(e) {
        var _this2 = this;

        if (e) e.stopPropagation();
        this.state.showOptions = this.state.showOptions ? false : true;

        this.forceUpdate();

        if (this.props.config.optionsSource && this._isLoaded) {
            var options = [];

            if (this.props.config.optionsSource.hierarchical) {
                var optionsH = _processExternalOptions(this.state.rawOptions, this.props.config.optionsSource.hierarchyProp);
                options = _recurseOptions(optionsH, this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource, 0);
            } else {

                this.state.rawOptions.forEach(function (item) {
                    options.push([item[_this2.props.config.optionsSource.valSource], ewars.I18N(item[_this2.props.config.optionsSource.labelSource]), item]);
                });

                if (this.props.config) {
                    if (this.props.config.optionsSource) {
                        if (this.props.config.optionsSource.additional) {
                            _.each(this.props.config.optionsSource.additional, function (option) {
                                var optionValue = option[0];
                                if (optionValue == null) optionValue = "null";
                                options.unshift([optionValue, option[1]]);
                            }, this);
                        }
                    }
                }
            }

            this.state.options = options;
            if (this.isMounted()) this.forceUpdate();
        }

        if (this.props.config.optionsSource && !this._isLoaded) {
            // We need to load the options
            var resource = this.props.config.optionsSource.resource.toLowerCase();
            var select = [this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource];
            if (this.props.config.optionsSource.select) {
                select = this.props.config.optionsSource.select;
            }

            if (this.props.config.optionsSource.additionalProperties) {
                select = select.concat(this.props.config.optionsSource.additionalProperties);
            }

            var join = this.props.config.optionsSource.join || null;

            ewars.tx("com.ewars.query", [resource, select, this.props.config.optionsSource.query, null, null, null, join]).then(function (resp) {
                var options = [];

                if (this.props.config.optionsSource.hierarchical) {
                    var optionsH = _processExternalOptions(resp, this.props.config.optionsSource.hierarchyProp);
                    options = _recurseOptions(optionsH, this.props.config.optionsSource.labelSource, this.props.config.optionsSource.valSource, 0);
                } else {

                    _.each(resp, function (item) {
                        options.push([item[this.props.config.optionsSource.valSource], ewars.I18N(item[this.props.config.optionsSource.labelSource]), item]);
                    }, this);

                    if (this.props.config) {
                        if (this.props.config.optionsSource) {
                            if (this.props.config.optionsSource.additional) {
                                _.each(this.props.config.optionsSource.additional, function (option) {
                                    var optionValue = option[0];
                                    if (optionValue == null) optionValue = "null";
                                    options.unshift([optionValue, option[1]]);
                                }, this);
                            }
                        }
                    }

                    this.state.rawOptions = resp;
                }

                this.state.options = options;
                this._isLoaded = true;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    render: function render() {
        var _this3 = this;

        var dataClassName = "ew-select-data";

        if (!this._initialLoaded) {
            return React.createElement(
                "div",
                { className: "ew-select" },
                React.createElement(
                    "div",
                    { className: "handle" },
                    React.createElement("i", { className: "fal fa-spin fa-circle-o-notch" })
                )
            );
        }

        var multiple = this.props.config.multiple;
        if (multiple) {
            dataClassName += " multi-select";
        }

        if (this.props.config) {
            if (this.props.config.multiple != undefined || this.props.config.multiple != null) {
                multiple = this.props.config.multiple;
            }
        }

        var config = this._processConfig();

        if (this.props.readOnly && !multiple) {
            var value = this._getOptionDisplay();
            return React.createElement("input", { type: "text", disabled: true, value: value });
        }

        var rawOptions = this._getOptions();

        var ItemTemplate = this.props.ItemTemplate || this.props.config.ItemTemplate;
        var templateName = this.props.template || this.props.config.template || null;
        if (templateName) ItemTemplate = PRESET_TEMPLATES[templateName];
        if (!ItemTemplate) ItemTemplate = Item;

        var options = rawOptions.map(function (option) {
            var id = ewars.utils.uuid();

            var item = void 0;
            if (ItemTemplate) {
                item = React.createElement(ItemTemplate, {
                    readOnly: _this3.props.readOnly,
                    multiple: multiple,
                    data: option,
                    onClick: _this3.onChange,
                    value: _this3.props.value,
                    key: id });
            }

            if (multiple) {
                item = React.createElement(
                    MultiSelectWrapper,
                    {
                        readOnly: _this3.props.readOnly,
                        data: option,
                        onClick: _this3.onChange,
                        value: _this3.props.value },
                    item
                );
            }

            return item;
        });

        var prefixed = [];
        if (this.props.config.optionsSource) {
            if (this.props.config.optionsSource.prefixed) {
                prefixed = this.props.config.optionsSource.prefixed;
                this.props.config.optionsSource.prefixed.forEach(function (item) {
                    options.unshift(React.createElement(
                        "div",
                        {
                            key: item[0],
                            className: "item",
                            "data-key": item[0],
                            "data-value": item[0],
                            onClick: _this3._onPrefixSelect },
                        item[1]
                    ));

                    rawOptions.push(item);
                });
            }
        }

        if (!multiple) {
            options.unshift(React.createElement(
                "div",
                {
                    key: this.state.placeKey,
                    className: "item",
                    "data-key": this.state.placeKey,
                    "data-value": "null",
                    onClick: this.onChange },
                "No Selection"
            ));
        }

        if (this.props.config.optionsSource && !this._isLoaded) {
            options = React.createElement(Spinner, null);
        }

        var onClick = multiple ? null : this.handleBodyClick;
        var show = multiple ? true : this.state.showOptions;
        var handleClass = "ew-select";
        if (show) handleClass += " ew-select-open";

        return React.createElement(
            "div",
            { ref: "selector", className: handleClass, onClick: onClick },
            !multiple ? React.createElement(Handle, {
                onClick: this._toggle,
                emptyText: this.props.emptyText,
                value: this.props.value,
                prefixed: prefixed,
                options: rawOptions }) : null,
            show ? React.createElement(
                "div",
                { className: dataClassName },
                options
            ) : null,
            multiple && !this.props.readOnly ? React.createElement(
                "div",
                { style: { padding: 8, borderTop: "1px solid #CCC" } },
                React.createElement(
                    "a",
                    { onClick: this._selectAll, href: "#" },
                    "Select All"
                ),
                " | ",
                React.createElement(
                    "a",
                    { onClick: this._selectNone, href: "#" },
                    "Select None"
                )
            ) : null
        );
    }
});

module.exports = SelectField;

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(38);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (obj, key, value) {
  if (key in obj) {
    (0, _defineProperty2.default)(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(60), __esModule: true };

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(62);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(4) && !__webpack_require__(7)(function () {
  return Object.defineProperty(__webpack_require__(41)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(11);
var document = __webpack_require__(1).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(63), __esModule: true };

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(5);
var toIObject = __webpack_require__(8);
var arrayIndexOf = __webpack_require__(66)(false);
var IE_PROTO = __webpack_require__(25)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(45);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 45 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(70), __esModule: true };

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(5);
var toObject = __webpack_require__(29);
var IE_PROTO = __webpack_require__(25)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(74);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(85);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(30);
var $export = __webpack_require__(2);
var redefine = __webpack_require__(50);
var hide = __webpack_require__(6);
var has = __webpack_require__(5);
var Iterators = __webpack_require__(31);
var $iterCreate = __webpack_require__(78);
var setToStringTag = __webpack_require__(33);
var getPrototypeOf = __webpack_require__(47);
var ITERATOR = __webpack_require__(9)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(6);


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(43);
var hiddenKeys = __webpack_require__(27).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(20);
var createDesc = __webpack_require__(17);
var toIObject = __webpack_require__(8);
var toPrimitive = __webpack_require__(21);
var has = __webpack_require__(5);
var IE8_DOM_DEFINE = __webpack_require__(40);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(4) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(12);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(13);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(14);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(15);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(16);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Handle = function (_React$Component) {
    (0, _inherits3.default)(Handle, _React$Component);

    function Handle(props) {
        (0, _classCallCheck3.default)(this, Handle);
        return (0, _possibleConstructorReturn3.default)(this, (Handle.__proto__ || (0, _getPrototypeOf2.default)(Handle)).call(this, props));
    }

    (0, _createClass3.default)(Handle, [{
        key: "render",
        value: function render() {
            var label = "No Selection";
            if (this.props.value) label = __(this.props.value.name);

            console.log(this.props.value);

            return React.createElement(
                "div",
                { className: "handle", onClick: this.props.onClick },
                React.createElement(
                    "table",
                    { width: "100%" },
                    React.createElement(
                        "tbody",
                        null,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement(
                                "td",
                                null,
                                label
                            ),
                            React.createElement(
                                "td",
                                { width: "20px", className: "icon" },
                                React.createElement("i", { className: "fal fa-caret-down" })
                            )
                        )
                    )
                )
            );
        }
    }]);
    return Handle;
}(React.Component);

var Item = function (_React$Component2) {
    (0, _inherits3.default)(Item, _React$Component2);

    function Item(props) {
        (0, _classCallCheck3.default)(this, Item);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (Item.__proto__ || (0, _getPrototypeOf2.default)(Item)).call(this, props));

        _this2._isLoaded = false;

        _this2._toggle = function () {
            if (!_this2._isLoaded) {
                ewars.tx("com.sonoma.indicators.tree", _this2.props.data.uuid).then(function (res) {
                    _this2._isLoaded = true;
                    _this2.setState({
                        show: true,
                        data: res.filter(function (x) {
                            return x.type == "Group";
                        })
                    });
                }).catch(function (err) {
                    console.log(err);
                });
            } else {
                _this2.setState({
                    show: !_this2.state.show
                });
            }
        };

        _this2._onSelect = function () {
            _this2.props.onSelect(_this2.props.data);
        };

        _this2.state = {
            data: [],
            show: false
        };
        return _this2;
    }

    (0, _createClass3.default)(Item, [{
        key: "render",
        value: function render() {
            var _this3 = this;

            var iconClass = "fal fa-folder";
            if (this.state.show) iconClass = "fal fa-folder-open";

            return React.createElement(
                "div",
                { className: "block" },
                React.createElement(
                    "div",
                    { className: "block-content", style: { padding: 0 } },
                    React.createElement(
                        ewars.d.Row,
                        null,
                        React.createElement(
                            ewars.d.Cell,
                            { width: 22,
                                style: { maxWidth: 22, padding: 8 },
                                onClick: this._toggle },
                            React.createElement("i", { className: iconClass })
                        ),
                        React.createElement(
                            ewars.d.Cell,
                            { style: { padding: 8 },
                                onClick: this._onSelect },
                            ewars.I18N(this.props.data.name)
                        )
                    )
                ),
                this.state.show ? React.createElement(
                    "div",
                    { className: "block-children" },
                    this.state.data.map(function (item) {
                        return React.createElement(Item, {
                            data: item,
                            onSelect: _this3.props.onSelect });
                    })
                ) : null
            );
        }
    }]);
    return Item;
}(React.Component);

var GroupSelector = function (_React$Component3) {
    (0, _inherits3.default)(GroupSelector, _React$Component3);

    function GroupSelector(props) {
        (0, _classCallCheck3.default)(this, GroupSelector);

        var _this4 = (0, _possibleConstructorReturn3.default)(this, (GroupSelector.__proto__ || (0, _getPrototypeOf2.default)(GroupSelector)).call(this, props));

        _initialiseProps.call(_this4);

        _this4.state = {
            data: [],
            show: false
        };
        return _this4;
    }

    (0, _createClass3.default)(GroupSelector, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._onBodyClick);
            this._query(this.props);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._onBodyClick);
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            if (this.state.value) {
                if (nextProps.value != this.state.value.id) {
                    this._query(nextProps);
                }
            }
        }
    }, {
        key: "render",
        value: function render() {
            var _this5 = this;

            var items = this.state.data.map(function (item) {
                return React.createElement(Item, { key: "GROUP_" + item.uuid, data: item, onSelect: _this5._onSelect });
            });

            return React.createElement(
                "div",
                { ref: "selector", className: "ew-select", onClick: this._onBodyClick },
                React.createElement(Handle, {
                    onClick: this._toggle,
                    value: this.state.value }),
                this.state.show ? React.createElement(
                    "div",
                    { className: "ew-select-data", ref: function ref(el) {
                            _this5._itemsEl = el;
                        } },
                    React.createElement(
                        "div",
                        { className: "block-tree", style: { position: "relative", height: 250, padding: 0 } },
                        items
                    )
                ) : null
            );
        }
    }]);
    return GroupSelector;
}(React.Component);

var _initialiseProps = function _initialiseProps() {
    var _this6 = this;

    this.isLoaded = false;

    this._query = function (props) {
        if (props.value) {
            ewars.tx("com.sonoma.indicator_group", props.value).then(function (resp) {
                _this6.setState({
                    value: resp
                });
            });
        }
    };

    this._reload = function () {
        ewars.tx("com.sonoma.indicators.tree", null).then(function (resp) {
            _this6._isLoaded = true;
            _this6.setState({
                data: resp.filter(function (x) {
                    return x.type == "Group";
                }),
                show: true
            });
        });
    };

    this._onBodyClick = function (evt) {
        if (_this6.state.show) {
            if (!_this6._itemsEl.contains(evt.target)) {
                _this6.setState({
                    show: false
                });
            }
        }
    };

    this._toggle = function () {
        if (!_this6._isLoaded && !_this6.state.show == true) {
            _this6._reload();
        } else {
            _this6.setState({
                show: !_this6.state.show
            });
        }
    };

    this._onSelect = function (item) {
        _this6.setState({ show: false, value: item });
        _this6.props.onUpdate(_this6.props.name, item.uuid);
    };
};

exports.default = GroupSelector;

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var TextInputField = React.createClass({
    displayName: "TextInputField",

    propTypes: {
        _onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool
    },

    _options: {},

    componentDidMount: function componentDidMount() {
        if (this.props.focus) {
            this.refs.inputField.focus();
        }
    },

    getDefaultProps: function getDefaultProps() {
        return {
            path: null,
            name: null,
            config: {}
        };
    },

    getInitialState: function getInitialState() {
        return {};
    },

    _handleKeyDown: function _handleKeyDown(e) {
        if (this.props.onKeyDown) this.props.onKeyDown(e);
    },

    _onChange: function _onChange(event) {
        if (!this.props.config.useBlur) {
            this.props.onUpdate(this.props.name, event.target.value, this.props.path);
        }
    },

    _onBlur: function _onBlur(e) {
        // Perform validation here
        if (this.props.config.useBlur) {
            this.props.onUpdate(this.props.name, e.target.value, this.props.path);
        }
    },

    render: function render() {

        var value = this.props.value;
        if (value) {
            var strValue = String(value);
            if (strValue.indexOf("DUPE_") >= 0) {
                value = strValue.replace("DUPE_", "");
            }
        }

        return React.createElement("input", {
            ref: "inputField",
            disabled: this.props.readOnly,
            className: "form-control input",
            onBlur: this._onBlur,
            onChange: this._onChange,
            type: "text",
            value: value,
            placeholder: this.props.placeholder,
            name: this.props.name,
            onKeyDown: this._handleKeyDown });
    }

});

module.exports = TextInputField;

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var _languages = ["en", "fr", "ar"];

var LanguageNode = React.createClass({
    displayName: "LanguageNode",

    render: function render() {
        var className = "language";
        if (this.props.active) className += " active";

        return React.createElement(
            "div",
            { className: className },
            this.props.value
        );
    }
});

var LanguageStringField = React.createClass({
    displayName: "LanguageStringField",

    _defaultLang: "en",

    getInitialState: function getInitialState() {
        return {
            curLang: "en",
            showTree: false
        };
    },

    componentWillMount: function componentWillMount() {
        // A little clean up, fix labels which are not dicts to dicts
        this.state.value = this.props.value;
        if (!this.props.value) {
            this.state.value = { en: "" };
        }

        if (typeof this.props.value == "string") {
            this.state.value = { en: this.props.value };
        }
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        this.state.value = nextProps.value;
        if (!nextProps.value) {
            this.state.value = { en: "" };
        }

        if (typeof nextProps.value == "string") {
            this.state.value = { en: nextProps.value };
        }
    },

    onChange: function onChange(e) {
        var copy = this.state.value;
        copy[this.state.curLang] = e.target.value;
        this.props.onUpdate(this.props.name, copy);
    },

    _handleLanguageChange: function _handleLanguageChange(e) {
        e.preventDefault();
        e.stopPropagation();
        var language = e.target.dataset.lang;
        this.setState({
            showTree: false,
            curLang: language
        });
    },

    _hideTree: function _hideTree() {
        this.setState({
            showTree: false
        });
    },

    _toggleTree: function _toggleTree(e) {
        this.setState({
            showTree: this.state.showTree == true ? false : true
        });
    },

    render: function render() {
        var curValue = this.state.value[this.state.curLang];

        var languages = _languages.map(function (language) {
            var active = false;
            if (this.state.curLang == language) active = true;
            return React.createElement(LanguageNode, {
                key: language,
                value: language,
                active: active });
        }.bind(this));

        var nodes = [];
        for (var i in _languages) {
            var language = _languages[i];
            nodes.push(React.createElement(
                "li",
                null,
                React.createElement(
                    "div",
                    { className: "label", onClick: this._handleLanguageChange, "data-lang": language },
                    language
                )
            ));
        }

        var disabled = false;
        if (this.props.readOnly) disabled = true;

        return React.createElement(
            "div",
            { className: "location-selector" },
            React.createElement(
                "div",
                { className: "handle" },
                React.createElement(
                    "div",
                    { className: "current no-pad" },
                    React.createElement("input", { type: "text", disabled: disabled, value: curValue, name: "lang_string", onChange: this.onChange })
                ),
                React.createElement(
                    "div",
                    { className: "grip", onClick: this._toggleTree },
                    React.createElement(
                        "div",
                        { className: "languages" },
                        languages
                    ),
                    React.createElement("i", { className: "fal fa-language" })
                )
            ),
            this.state.showTree ? React.createElement(
                "div",
                { className: "location-tree-wrapper" },
                React.createElement(
                    "div",
                    { className: "location-tree" },
                    React.createElement(
                        "h4",
                        null,
                        "Available Languages"
                    ),
                    React.createElement(
                        "ul",
                        { className: "languages-options" },
                        nodes
                    )
                )
            ) : null
        );
    }
});

module.exports = LanguageStringField;

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var DisplayField = React.createClass({
    displayName: "DisplayField",

    getInitialState: function getInitialState() {
        return {};
    },

    render: function render() {
        return React.createElement(
            "div",
            { className: "display-field" },
            this.props.config.defaultValue || this.props.value
        );
    }
});

module.exports = DisplayField;

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var btnClass = {
    red: "btn-red",
    green: "btn-green",
    amber: "btn-amber"
};

var ButtonComponent = React.createClass({
    displayName: "ButtonComponent",

    _id: null,

    propTypes: {
        label: React.PropTypes.string,
        icon: React.PropTypes.string,
        type: React.PropTypes.string
    },

    getDefaultProps: function getDefaultProps() {
        return {
            enabled: true,
            processing: false,
            active: false
        };
    },

    getInitialState: function getInitialState() {
        return {
            enabled: true,
            processing: false,
            processingMessage: "Processing..."
        };
    },

    /**
     * Public method to set button to processing mode
     * @param message
     */
    setProcessing: function setProcessing(message) {
        this.state.processing = true;
        if (message != null && message != undefined) this.state.processingMessage = message;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method to set button to not processing mode
     */
    unsetProcessing: function unsetProcessing() {
        this.state.processing = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method used to disable the button
     */
    disable: function disable() {
        this.state.enabled = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Pubilc method used to enable button
     */
    enable: function enable() {
        this.state.enabled = true;
        if (this.isMounted()) this.forceUpdate();
    },

    shouldComponentUpdate: function shouldComponentUpdate() {
        return true;
    },

    _onClick: function _onClick(e) {
        if (!this.props.enabled) return;
        if (!this.state.enabled) return;
        if (this.state.processing) return;
        if (this.props.processing) return;

        e.preventDefault();
        e.stopPropagation();
        this.props.onClick(this.props.data ? this.props.data : null);
    },

    render: function render() {
        var colour;
        if (this.props.colour) colour = btnClass[this.props.colour];

        var className = "";
        if (colour) className += " " + colour;
        var iconClass;
        if (this.props.icon) iconClass = "fal " + this.props.icon;

        if (this.props.active) className += " active";

        var title;
        if (this.props.title) title = this.props.title;

        if (!this.state.enabled || !this.props.enabled) className += " disabled";

        var label;
        if (this.props.label) label = ewars.formatters.I18N_FORMATTER(this.props.label);

        if (this.state.processing || this.props.processing) {
            iconClass = "fal fa-circle-o-notch fa-spin";
            label = this.state.processingMessage || "Processing...";
        }

        var view;
        if (iconClass && label) {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                React.createElement("i", { className: iconClass }),
                "\xA0",
                label
            );
        } else if (iconClass && !label) {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                React.createElement("i", { className: iconClass })
            );
        } else {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                label
            );
        }

        return view;
    }
});

module.exports = ButtonComponent;

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Component = __webpack_require__(59);

ReactDOM.render(React.createElement(Component, null), document.getElementById('application'));

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _defineProperty2 = __webpack_require__(37);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends3 = __webpack_require__(22);

var _extends4 = _interopRequireDefault(_extends3);

var _React$createClass;

var _IndicatorTreeComponent = __webpack_require__(69);

var _IndicatorTreeComponent2 = _interopRequireDefault(_IndicatorTreeComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Filler = __webpack_require__(103);
var DropDownMenu = __webpack_require__(104);

var IndicatorEditor = __webpack_require__(105);
var GroupEditor = __webpack_require__(116);

var CONSTANTS = {
    EDIT_INDICATOR: "EDIT_INDICATOR",
    EDIT_GROUP: "EDIT_GROUP"
};

var IND_ACTIONS = [{ label: "Edit", icon: "fa-pencil", action: "EDIT" }, { label: "Delete", icon: "fa-trash", action: "DELETE" }];

var Component = React.createClass((_React$createClass = {
    displayName: "Component",

    getInitialState: function getInitialState() {
        return {
            dirty: false,
            editing: null,
            editType: "INDICATOR",
            view: null,
            errors: []
        };
    },

    _onNewGroup: function _onNewGroup() {
        this.setState({
            editing: {
                name: "New Indicator",
                pid: null
            },
            editType: "FOLDER",
            view: "EDIT"
        });
    },

    _onEdit: function _onEdit(type, indData) {
        if (['DEFAULT', 'INDICATOR', 'STATIC', 'FOLDER', null, undefined, "AGGREGATE", "COMPLEX"].indexOf(indData.itype) < 0) {
            ewars.growl("This indicator or grouping is not editable");
            return;
        }

        if (indData.type == "Group" || !indData.type) {
            ewars.tx("com.sonoma.indicator_group", indData.uuid).then(function (resp) {
                this.state.view = CONSTANTS.EDIT_GROUP;
                this.state.editType = "FOLDER";
                this.state.editing = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }

        if (indData.type == "IndicatorItem") {
            ewars.tx("com.sonoma.indicator", indData.uuid).then(function (resp) {
                this.state.view = CONSTANTS.EDIT_INDICATOR;
                this.state.editType = "INDICATOR";
                this.state.editing = resp;
                if (this.isMounted()) this.forceUpdate();
            }.bind(this));
        }
    },

    _editIndicator: function _editIndicator(indicator) {
        this.props.onEdit(indicator);
    },

    _onNewIndicator: function _onNewIndicator() {
        this.setState({
            view: CONSTANTS.EDIT_INDICATOR,
            editType: "INDICATOR",
            editing: {}
        });
    }

}, (0, _defineProperty3.default)(_React$createClass, "_onNewGroup", function _onNewGroup() {
    this.setState({
        view: CONSTANTS.EDIT_GROUP,
        editType: "FOLDER",
        editing: {}
    });
}), (0, _defineProperty3.default)(_React$createClass, "_closeEdit", function _closeEdit() {
    this.setState({
        view: null,
        editing: null,
        editType: null
    });
}), (0, _defineProperty3.default)(_React$createClass, "_onAction", function _onAction(action, data) {
    this._onEdit("INDICATOR", data);
}), (0, _defineProperty3.default)(_React$createClass, "_onIndAction", function _onIndAction(action, data) {
    if (action == "EDIT") {
        if (data.id) {
            this._onEdit("FOLDER", data);
        } else {
            this._onEdit("INDICATOR", data);
        }
    }

    if (["INDICATOR_SELECT", "CLICK"].indexOf(action) >= 0) {
        if (data.uuid) {
            this._onEdit("INDICATOR", data);
        } else {
            this._onEdit("FOLDER", data);
        }
    }

    if (action == "DELETE") {
        ewars.prompt("fa-trash", "Delete Item?", _l("DELETE_IND_GROUP"), function () {
            var bl = new ewars.Blocker(null, "Deleting item...");
            var com = "com.sonoma.indicator.delete";
            if (data.type == "Group") com = "com.sonoma.indicator_group.delete";

            ewars.tx(com, data.uuid).then(function (resp) {
                bl.destroy();
                ewars.emit("RELOAD_INDICATORS");
                ewars.growl("Item deleted");
            });
        }.bind(this));
    }
}), (0, _defineProperty3.default)(_React$createClass, "_onSet", function _onSet(data) {
    this.setState({
        editing: data
    });
}), (0, _defineProperty3.default)(_React$createClass, "onPropChange", function onPropChange(prop, value) {
    this.setState({
        editing: (0, _extends4.default)({}, this.state.editing, (0, _defineProperty3.default)({}, prop, value))
    });
}), (0, _defineProperty3.default)(_React$createClass, "render", function render() {

    var subView = React.createElement(Filler, { icon: "fa-code-branch" }),
        formDefinition;

    if (this.state.view == CONSTANTS.EDIT_INDICATOR) {
        subView = React.createElement(IndicatorEditor, {
            onChange: this.onPropChange,
            onSet: this._onSet,
            data: this.state.editing });
    }

    if (this.state.view == CONSTANTS.EDIT_GROUP) {
        subView = React.createElement(GroupEditor, {
            onChange: this.onPropChange,
            onSet: this._onSet,
            data: this.state.editing });
    }

    var create_menu = [{ label: "New Indicator", icon: "fa-plus", handler: this._onNewIndicator }, { label: "New Group", icon: "fa-plus", handler: this._onNewGroup }];

    return React.createElement(
        ewars.d.Layout,
        null,
        React.createElement(
            ewars.d.Toolbar,
            { label: "Indicators" },
            React.createElement(
                "div",
                { className: "btn-group pull-right" },
                React.createElement(ewars.d.Button, {
                    label: "New Indicator",
                    onClick: this._onNewIndicator,
                    icon: "fa-plus" }),
                React.createElement(ewars.d.Button, {
                    label: "New Group",
                    onClick: this._onNewGroup,
                    icon: "fa-plus" })
            )
        ),
        React.createElement(
            ewars.d.Row,
            { className: "ide-row" },
            React.createElement(
                ewars.d.Cell,
                { width: 300, borderRight: true },
                React.createElement(_IndicatorTreeComponent2.default, {
                    hideInactive: false,
                    actions: IND_ACTIONS,
                    onAction: this._onIndAction,
                    fillHeight: true,
                    onEdit: this._onEdit })
            ),
            React.createElement(
                ewars.d.Cell,
                null,
                subView
            )
        )
    );
}), _React$createClass));

module.exports = Component;

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(61);
var $Object = __webpack_require__(0).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(2);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(4), 'Object', { defineProperty: __webpack_require__(3).f });


/***/ }),
/* 62 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(64);
module.exports = __webpack_require__(0).Object.assign;


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(2);

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(65) });


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__(18);
var gOPS = __webpack_require__(28);
var pIE = __webpack_require__(20);
var toObject = __webpack_require__(29);
var IObject = __webpack_require__(44);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(7)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(8);
var toLength = __webpack_require__(67);
var toAbsoluteIndex = __webpack_require__(68);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(24);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(24);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(22);

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(46);

var _stringify2 = _interopRequireDefault(_stringify);

var _getPrototypeOf = __webpack_require__(12);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(13);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(14);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(15);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(16);

var _inherits3 = _interopRequireDefault(_inherits2);

var _ContextMenu = __webpack_require__(102);

var _ContextMenu2 = _interopRequireDefault(_ContextMenu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var IndicatorNode = function (_React$Component) {
    (0, _inherits3.default)(IndicatorNode, _React$Component);

    function IndicatorNode(props) {
        (0, _classCallCheck3.default)(this, IndicatorNode);

        var _this = (0, _possibleConstructorReturn3.default)(this, (IndicatorNode.__proto__ || (0, _getPrototypeOf2.default)(IndicatorNode)).call(this, props));

        _this._onBodyClick = function (evt) {
            if (!_this.refs.item.contains(evt.target)) {
                _this.setState({
                    showContext: false
                });
            }
        };

        _this._onContext = function (e) {
            e.preventDefault();
            _this.setState({
                showContext: true
            });
        };

        _this._onContextAction = function (action) {
            _this.setState({
                showContext: false
            });
            _this.props.onAction(action, _this.props.data);
        };

        _this._onClick = function () {
            _this.props.onAction("CHECK", _this.props.data);
        };

        _this._onCheck = function () {
            _this.props.onAction("CHECK", _this.props.data);
        };

        _this._onSelect = function () {
            _this.props.onAction("INDICATOR_SELECT", _this.props.data);
        };

        _this._dragStart = function (e) {
            e.dataTransfer.setData("item", (0, _stringify2.default)(_this.props.data));
        };

        _this.state = {
            showContext: false
        };
        return _this;
    }

    (0, _createClass3.default)(IndicatorNode, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._onBodyClick);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._onBodyClick);
        }
    }, {
        key: "render",
        value: function render() {
            var label = ewars.I18N(this.props.data.name);

            var icon = void 0;
            if (this.props.allowCheck) {
                icon = "fal fa-square";
                if (this.props.checked.indexOf(this.props.data.uuid) >= 0) icon = "fal fa-check-square";
            }

            var className = "block-content";
            className = className + (this.props.data.status == "ACTIVE" ? " status-green" : " status-red");

            return React.createElement(
                "div",
                { className: "block", onDragStart: this._dragStart, draggable: true, ref: "item",
                    onContextMenu: this._onContext, onClick: this._onSelect },
                React.createElement(
                    "div",
                    { className: className },
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        this.props.allowCheck ? React.createElement(
                            "div",
                            { className: "ide-col", style: { maxWidth: 25 } },
                            React.createElement("i", { className: icon })
                        ) : null,
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            label
                        )
                    ),
                    this.props.actions && this.state.showContext ? React.createElement(_ContextMenu2.default, {
                        absolute: true,
                        onClick: this._onContextAction,
                        actions: this.props.actions }) : null
                )
            );
        }
    }]);
    return IndicatorNode;
}(React.Component);

var IndicatorFolderNode = function (_React$Component2) {
    (0, _inherits3.default)(IndicatorFolderNode, _React$Component2);

    function IndicatorFolderNode(props) {
        (0, _classCallCheck3.default)(this, IndicatorFolderNode);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (IndicatorFolderNode.__proto__ || (0, _getPrototypeOf2.default)(IndicatorFolderNode)).call(this, props));

        _this2._onBodyClick = function (evt) {
            if (!_this2.refs.item.contains(evt.target)) {
                _this2.setState({
                    showContext: false
                });
            }
        };

        _this2._reload = function () {
            ewars.tx("com.sonoma.indicators.tree", _this2.props.data.uuid).then(function (resp) {
                this.setState({
                    data: resp
                });
            }.bind(_this2));
        };

        _this2.getChildren = function () {
            if (_this2.state.data) {
                _this2.setState({
                    showChildren: !_this2.state.showChildren
                });
                return;
            }

            _this2.refs.iconHandle.setAttribute("class", "fa fa-spin fa-gear");

            // Get groups first
            ewars.tx("com.sonoma.indicators.tree", _this2.props.data.uuid).then(function (resp) {
                this.setState({
                    data: resp,
                    showChildren: true
                });
            }.bind(_this2));
        };

        _this2._onCaretClick = function () {
            _this2.getChildren();
        };

        _this2._dragStart = function (e) {
            e.dataTransfer.setData("item", (0, _stringify2.default)(_this2.props.data));
        };

        _this2._onContext = function (e) {
            e.preventDefault();
            _this2.setState({
                showContext: true
            });
        };

        _this2._onContextAction = function (action) {
            _this2.setState({
                showContext: false
            });

            _this2.props.onAction(action, _this2.props.data);
        };

        _this2.state = ewars.g["FOLDER_" + _this2.props.data.id] || {
            showChildren: false,
            data: null,
            showContext: false
        };
        return _this2;
    }

    (0, _createClass3.default)(IndicatorFolderNode, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            window.__hack__.addEventListener("click", this._onBodyClick);
            ewars.subscribe("RELOAD_INDICATORS", this._reload);
        }
    }, {
        key: "componentWillUnmount",
        value: function componentWillUnmount() {
            window.__hack__.removeEventListener("click", this._onBodyClick);
            ewars.g["FOLDER_" + this.props.data.uuid] = ewars.copy(this.state);
        }
    }, {
        key: "render",
        value: function render() {

            var childs = void 0;

            if (this.state.data) {
                childs = this.state.data.map(function (item) {
                    if (item.type == "IndicatorItem") {
                        return React.createElement(IndicatorNode, (0, _extends3.default)({}, this.props, {
                            key: "IND_" + item.uuid,
                            data: item }));
                    }

                    if (item.type == "Group") {
                        return React.createElement(IndicatorFolderNode, (0, _extends3.default)({}, this.props, {
                            key: "FOLDER_" + item.uuid,
                            data: item }));
                    }
                }.bind(this));
            }

            var icon = "fal ";
            if (this.state.showChildren) icon += " fa-folder-open";
            if (!this.state.showChildren) icon += " fa-folder";

            var label = ewars.I18N(this.props.data.name);

            return React.createElement(
                "div",
                { className: "block", ref: "item" },
                React.createElement(
                    "div",
                    { onClick: this._onCaretClick, className: "block-content", onDragStart: this._dragStart,
                        draggable: true,
                        onContextMenu: this._onContext },
                    React.createElement(
                        "div",
                        { className: "ide-row" },
                        React.createElement(
                            "div",
                            { className: "ide-col", style: { maxWidth: 20, textAlign: "center", marginRight: 8 } },
                            React.createElement("i", { className: icon, ref: "iconHandle" })
                        ),
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            label
                        )
                    ),
                    this.props.actions && this.state.showContext ? React.createElement(_ContextMenu2.default, {
                        absolute: true,
                        onClick: this._onContextAction,
                        actions: this.props.actions }) : null
                ),
                this.state.showChildren ? React.createElement(
                    "div",
                    { className: "block-children" },
                    childs
                ) : null
            );
        }
    }]);
    return IndicatorFolderNode;
}(React.Component);

var TreeNodeComponent = React.createClass({
    displayName: "TreeNodeComponent",

    _hasLoaded: false,

    getDefaultProps: function getDefaultProps() {
        return {
            dark: false,
            hideInactive: true
        };
    },

    getInitialState: function getInitialState() {
        return {
            children: [],
            showChildren: false,
            showContext: false
        };
    },

    componentWillMount: function componentWillMount() {
        window.__hack__.addEventListener("click", this._onBodyClick);
        ewars.subscribe("RELOAD_INDICATORS", this._reload);
    },

    componentWillUnmount: function componentWillUnmount() {
        window.__hack__.removeEventListener("click", this._onBodyClick);
    },

    _onBodyClick: function _onBodyClick(evt) {
        if (!evt.contains(this.refs.item)) {
            this.setState({
                showContext: false
            });
        }
    },

    _onContext: function _onContext(e) {
        e.preventDefault();
        this.setState({
            showContext: true
        });
    },

    _onContextAction: function _onContextAction(action) {
        this.props.onAction(action, this.props.data);
    },

    _reload: function _reload() {
        if (this._hasLoaded && this.props.type != "INDICATOR") {
            // Get groups first

            ewars.tx("com.ewars.indicators", [this.props.data.id]).then(function (resp) {
                this._hasLoaded = true;
                this.setState({
                    children: resp,
                    showChildren: true
                });
            }.bind(this));
        }
    },

    _onClick: function _onClick() {
        if (this.state.showChildren) {
            this.setState({
                showChildren: false
            });
            return;
        }

        if (!this._hasLoaded) {
            ewars.tx("com.ewars.indicators", [this.props.data.id]).then(function (resp) {
                this._hasLoaded = true;
                this.setState({
                    children: resp,
                    showChildren: true
                });
            }.bind(this));
        } else {

            this.setState({
                showChildren: true
            });
        }
    },

    _hasChildren: function _hasChildren() {
        return true;
    },

    _onEdit: function _onEdit() {
        this.props.onEdit(null, this.props.data);
    },

    render: function render() {
        var className = "block";

        var label = ewars.I18N(this.props.data.name);

        var showSelect = false;

        var caret = void 0;
        if (["SYSTEM", "FOLDER"].indexOf(this.props.data.context) >= 0) {
            if (this.state.showChildren) caret = "fal fa-folder-open";
            if (!this.state.showChildren) caret = "fal fa-folder";
        }

        var children = void 0;

        if (this.state.showChildren && this._hasLoaded) {
            children = [];
            this.state.children.forEach(function (item) {
                children.push(React.createElement(TreeNodeComponent, {
                    key: item.uuid || "FOLDER-" + item.id,
                    actions: this.props.actions,
                    onSelect: this.props.onSelect,
                    data: item }));
            }.bind(this));
        }

        if (this.state.showChildren && !this._hasLoaded) {
            children = React.createElement(Spinner, null);
        }

        if (this.props.data.context == "INDICATOR") showSelect = true;

        var handleStyle = { flexGrow: 1, flexBasis: 0 };

        return React.createElement(
            "div",
            { className: className, ref: "item" },
            React.createElement(
                "div",
                { className: "block-content", onClick: this._onClick, onContextMenu: this._onContext },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    caret ? React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 25 } },
                        React.createElement("i", { className: caret })
                    ) : null,
                    React.createElement(
                        "div",
                        { className: "ide-col", style: handleStyle },
                        label
                    )
                ),
                this.props.actions && this.state.showContext ? React.createElement(_ContextMenu2.default, {
                    absolute: true,
                    onClick: this._onContextAction,
                    actions: this.props.actions }) : null
            ),
            this.state.showChildren ? React.createElement(
                "div",
                { className: "block-children" },
                children
            ) : null
        );
    }
});

function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

var SearchInput = function (_React$Component3) {
    (0, _inherits3.default)(SearchInput, _React$Component3);

    function SearchInput(props) {
        (0, _classCallCheck3.default)(this, SearchInput);

        var _this3 = (0, _possibleConstructorReturn3.default)(this, (SearchInput.__proto__ || (0, _getPrototypeOf2.default)(SearchInput)).call(this, props));

        _this3.sendForUpdate = function (e) {
            _this3.props.onChange(e.target.value);

            if (e.target.value.length > 2) {
                _this3.sendForSearch(e.target.value);
            }

            if (e.target.value == "") _this3.sendForSearch(null);
        };

        _this3.sendForSearch = function (searchTerm) {
            _this3.props.onSearch(searchTerm);
        };

        _this3.sendForSearch = debounce(_this3.sendForSearch, 300);
        return _this3;
    }

    (0, _createClass3.default)(SearchInput, [{
        key: "render",
        value: function render() {
            return React.createElement("input", {
                type: "text",
                onChange: this.sendForUpdate,
                value: this.props.value });
        }
    }]);
    return SearchInput;
}(React.Component);

SearchInput.defaultProps = {
    value: ''
};

var IndicatorTreeView = function (_React$Component4) {
    (0, _inherits3.default)(IndicatorTreeView, _React$Component4);

    function IndicatorTreeView(props) {
        (0, _classCallCheck3.default)(this, IndicatorTreeView);

        var _this4 = (0, _possibleConstructorReturn3.default)(this, (IndicatorTreeView.__proto__ || (0, _getPrototypeOf2.default)(IndicatorTreeView)).call(this, props));

        _this4._init = function () {
            ewars.tx("com.sonoma.indicators.tree", null).then(function (res) {
                _this4.setState({ data: res });
            }).catch(function (err) {
                console.log(err);
            });
        };

        _this4._onSearchChange = function (value) {
            _this4.setState({
                search: value
            });
        };

        _this4._onSearch = function (val) {
            if (val == "" || val == null) {
                _this4._init();
            } else {
                ewars.tx("com.ewars.query", ["indicator", null, { "name.en": { like: val } }, null, null, null, null]).then(function (resp) {
                    _this4.setState({
                        data: resp
                    });
                });
            }
        };

        _this4._clearSearch = function () {
            _this4.setState({
                search: ""
            });

            _this4._init();
        };

        _this4._onItemAction = function (action, data) {
            _this4.props.onAction(action, data);
        };

        _this4.state = {
            search: "",
            data: []
        };

        _this4._handleSearchChange = debounce(_this4._handleSearchChange, 500);
        return _this4;
    }

    (0, _createClass3.default)(IndicatorTreeView, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            this._init();
            ewars.subscribe("RELOAD_INDICATORS", this._init);
        }
    }, {
        key: "render",
        value: function render() {
            var _this5 = this;

            var nodes = this.state.data.map(function (item) {
                if (_this5.state.search != "" && _this5.state.search != null) {
                    return React.createElement(IndicatorNode, {
                        key: item.uuid || "FOLDER-" + item.id,
                        actions: _this5.props.actions,
                        onEdit: _this5.props.onEdit,
                        onAction: _this5._onItemAction,
                        data: item });
                } else {
                    return React.createElement(IndicatorFolderNode, (0, _extends3.default)({
                        key: "FOLDER_" + item.uuid
                    }, _this5.props, {
                        onAction: _this5._onItemAction,
                        data: item }));
                }
            });

            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Row,
                    { height: 45 },
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            "div",
                            { className: "search" },
                            React.createElement(
                                "div",
                                { className: "search-inner" },
                                React.createElement(
                                    ewars.d.Row,
                                    null,
                                    React.createElement(
                                        ewars.d.Cell,
                                        { addClass: "search-left", width: 25 },
                                        React.createElement("i", { className: "fal fa-search" })
                                    ),
                                    React.createElement(
                                        ewars.d.Cell,
                                        { addClass: "search-mid" },
                                        React.createElement(SearchInput, {
                                            value: this.state.search,
                                            onChange: this._onSearchChange,
                                            onSearch: this._onSearch })
                                    ),
                                    this.state.search != "" ? React.createElement(
                                        ewars.d.Cell,
                                        {
                                            addClass: "search-right",
                                            width: 25,
                                            onClick: this._clearSearch },
                                        React.createElement("i", { className: "fal fa-times" })
                                    ) : null
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            "div",
                            { className: "ide-panel ide-panel-absolute ide-scroll" },
                            React.createElement(
                                "div",
                                { className: "block-tree" },
                                nodes
                            )
                        )
                    )
                )
            );
        }
    }]);
    return IndicatorTreeView;
}(React.Component);

IndicatorTreeView.defaultProps = {
    allowCheck: false,
    checked: [],
    value: null,
    hideInactive: true
};
exports.default = IndicatorTreeView;

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(0);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(72);
module.exports = __webpack_require__(0).Object.getPrototypeOf;


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(29);
var $getPrototypeOf = __webpack_require__(47);

__webpack_require__(73)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(2);
var core = __webpack_require__(0);
var fails = __webpack_require__(7);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(75), __esModule: true };

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(76);
__webpack_require__(81);
module.exports = __webpack_require__(34).f('iterator');


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(77)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(49)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(24);
var defined = __webpack_require__(23);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(32);
var descriptor = __webpack_require__(17);
var setToStringTag = __webpack_require__(33);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(6)(IteratorPrototype, __webpack_require__(9)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(3);
var anObject = __webpack_require__(10);
var getKeys = __webpack_require__(18);

module.exports = __webpack_require__(4) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(1).document;
module.exports = document && document.documentElement;


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(82);
var global = __webpack_require__(1);
var hide = __webpack_require__(6);
var Iterators = __webpack_require__(31);
var TO_STRING_TAG = __webpack_require__(9)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(83);
var step = __webpack_require__(84);
var Iterators = __webpack_require__(31);
var toIObject = __webpack_require__(8);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(49)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),
/* 83 */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),
/* 84 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(86), __esModule: true };

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(87);
__webpack_require__(92);
__webpack_require__(93);
__webpack_require__(94);
module.exports = __webpack_require__(0).Symbol;


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(1);
var has = __webpack_require__(5);
var DESCRIPTORS = __webpack_require__(4);
var $export = __webpack_require__(2);
var redefine = __webpack_require__(50);
var META = __webpack_require__(88).KEY;
var $fails = __webpack_require__(7);
var shared = __webpack_require__(26);
var setToStringTag = __webpack_require__(33);
var uid = __webpack_require__(19);
var wks = __webpack_require__(9);
var wksExt = __webpack_require__(34);
var wksDefine = __webpack_require__(35);
var enumKeys = __webpack_require__(89);
var isArray = __webpack_require__(90);
var anObject = __webpack_require__(10);
var toIObject = __webpack_require__(8);
var toPrimitive = __webpack_require__(21);
var createDesc = __webpack_require__(17);
var _create = __webpack_require__(32);
var gOPNExt = __webpack_require__(91);
var $GOPD = __webpack_require__(52);
var $DP = __webpack_require__(3);
var $keys = __webpack_require__(18);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(51).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(20).f = $propertyIsEnumerable;
  __webpack_require__(28).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(30)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    if (it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    replacer = args[1];
    if (typeof replacer == 'function') $replacer = replacer;
    if ($replacer || !isArray(replacer)) replacer = function (key, value) {
      if ($replacer) value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(6)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(19)('meta');
var isObject = __webpack_require__(11);
var has = __webpack_require__(5);
var setDesc = __webpack_require__(3).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(7)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(18);
var gOPS = __webpack_require__(28);
var pIE = __webpack_require__(20);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(45);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(8);
var gOPN = __webpack_require__(51).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),
/* 92 */
/***/ (function(module, exports) {



/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(35)('asyncIterator');


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(35)('observable');


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(96), __esModule: true };

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(97);
module.exports = __webpack_require__(0).Object.setPrototypeOf;


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(2);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(98).set });


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(11);
var anObject = __webpack_require__(10);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(39)(Function.call, __webpack_require__(52).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(100), __esModule: true };

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(101);
var $Object = __webpack_require__(0).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(2);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(32) });


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(12);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(13);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(14);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(15);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(16);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ContextMenuItem = function (_React$Component) {
    (0, _inherits3.default)(ContextMenuItem, _React$Component);

    function ContextMenuItem(props) {
        (0, _classCallCheck3.default)(this, ContextMenuItem);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ContextMenuItem.__proto__ || (0, _getPrototypeOf2.default)(ContextMenuItem)).call(this, props));

        _this._onClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            _this.props.onClick(_this.props.data.action);
        };

        return _this;
    }

    (0, _createClass3.default)(ContextMenuItem, [{
        key: "render",
        value: function render() {
            var iconClass = "fal " + this.props.data.icon;

            return React.createElement(
                "div",
                { onClick: this._onClick, className: "context-menu-item" },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement(
                        "div",
                        { className: "ide-col", style: { maxWidth: 25 } },
                        React.createElement("i", { className: iconClass })
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        this.props.data.label
                    )
                )
            );
        }
    }]);
    return ContextMenuItem;
}(React.Component);

var ContextMenu = function (_React$Component2) {
    (0, _inherits3.default)(ContextMenu, _React$Component2);

    function ContextMenu(props) {
        (0, _classCallCheck3.default)(this, ContextMenu);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (ContextMenu.__proto__ || (0, _getPrototypeOf2.default)(ContextMenu)).call(this, props));

        _this2._onClick = function (action) {
            _this2.props.onClick(action);
        };

        return _this2;
    }

    (0, _createClass3.default)(ContextMenu, [{
        key: "render",
        value: function render() {
            var style = {};
            if (!this.props.absolute) style.position = "relative";

            var actions = this.props.actions.map(function (action) {
                return React.createElement(ContextMenuItem, {
                    key: action.action,
                    data: action,
                    onClick: this._onClick });
            }.bind(this));

            return React.createElement(
                "div",
                { className: "context-menu", style: style },
                actions
            );
        }
    }]);
    return ContextMenu;
}(React.Component);

exports.default = ContextMenu;

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Filler = React.createClass({
    displayName: "Filler",

    render: function render() {
        var iconClass = "fal " + this.props.icon;
        return React.createElement(
            "div",
            { className: "ide-filler" },
            React.createElement(
                "div",
                { className: "ide-filler-icon" },
                React.createElement("i", { className: iconClass })
            )
        );
    }
});

module.exports = Filler;

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var MenuItem = React.createClass({
    displayName: "MenuItem",

    _onClick: function _onClick() {
        this.props.data.handler();
        this.props.onClick();
    },

    render: function render() {
        var icon;
        if (this.props.data.icon) {
            icon = "fal " + this.props.data.icon;
        }

        return React.createElement(
            "div",
            { className: "ide-dd-item", onClick: this._onClick },
            icon ? React.createElement(
                "div",
                { className: "ide-dd-item-icon" },
                React.createElement("i", { className: icon })
            ) : null,
            React.createElement(
                "div",
                { className: "ide-dd-item-label" },
                this.props.data.label
            )
        );
    }
});

var DropDownMenu = React.createClass({
    displayName: "DropDownMenu",

    getInitialProps: function getInitialProps() {
        return {
            isExpanded: false
        };
    },

    getInitialState: function getInitialState() {
        return {
            isExpanded: false
        };
    },

    hide: function hide() {
        this.setState({ isExpanded: false });
        document.removeEventListener("click", this.hide);
    },

    _onHandleClick: function _onHandleClick() {
        this.state.isExpanded = this.state.isExpanded ? false : true;
        if (this.state.isExpanded) {
            document.addEventListener("click", this.hide);
        } else {
            document.removeEventListener("click", this.hide);
        }
        this.forceUpdate();
    },

    _onClick: function _onClick() {
        this.state.isExpanded = false;
        document.removeEventListener("click", this.hide);
        this.forceUpdate();
    },

    render: function render() {
        var iconName = "fal " + this.props.icon;

        var items;
        items = _.map(this.props.menu, function (menuItem) {
            return React.createElement(MenuItem, {
                data: menuItem,
                onClick: this._onClick });
        }, this);

        return React.createElement(
            "div",
            { className: "ide-dropdown" },
            React.createElement(
                "div",
                { className: "handle", onClick: this._onHandleClick },
                React.createElement(
                    "div",
                    { className: "ide-dd-icon" },
                    React.createElement("i", { className: "fal fa-plus" })
                ),
                this.props.label ? React.createElement(
                    "div",
                    { className: "ide-dd-label" },
                    this.props.label
                ) : null,
                React.createElement(
                    "div",
                    { className: "ide-dd-caret" },
                    React.createElement("i", { className: "fal fa-caret-down" })
                )
            ),
            this.state.isExpanded ? React.createElement(
                "div",
                { className: "ide-dd-menu" },
                items
            ) : null
        );
    }
});

module.exports = DropDownMenu;

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _defineProperty2 = __webpack_require__(37);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _stringify = __webpack_require__(46);

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = __webpack_require__(22);

var _extends3 = _interopRequireDefault(_extends2);

var _React$createClass;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var KeyMirror = __webpack_require__(106);

var GeneralSettings = __webpack_require__(107);
var DefinitionSettings = __webpack_require__(111);
var Guidance = __webpack_require__(115);

var CONSTANTS = KeyMirror({
    GENERAL: null,
    DEFINITION: null,
    GUIDANCE: null
});

var defaults = {
    uuid: null,
    group_id: null,
    name: "",
    description: "",
    status: "INACTIVE",
    protected: false,
    tracks_location: false,
    tracks_date: false,
    value_type: 'NUMERIC',
    history: {},
    created: null,
    created_by: window.user.id,
    last_modified: null,
    ranked: false,
    colour: null,
    itype: "DEFAULT",
    definition: null,
    accounts: []

};

var SettingsSection = React.createClass({
    displayName: "SettingsSection",

    _onClick: function _onClick() {
        this.props.onClick(this.props.view);
    },

    render: function render() {
        var className = "ide-settings-section";
        if (this.props.active) className += " ide-settings-section-active";
        var iconClass = "fa fa-caret-right";
        if (this.props.active) iconClass = "fa fa-caret-down";

        var subs = [];
        if (this.props.subs) {
            subs = _.map(this.props.subs, function (sub) {
                return React.createElement(
                    "div",
                    { className: "section-item" },
                    sub
                );
            }, this);
        }

        var hasSubs = subs.length > 0;

        return React.createElement(
            "div",
            { className: className },
            React.createElement(
                "div",
                { className: "section-header", onClick: this._onClick },
                React.createElement(
                    "div",
                    { className: "section-title" },
                    this.props.label
                )
            ),
            hasSubs ? React.createElement(
                "div",
                { className: "section-items" },
                subs
            ) : null
        );
    }
});

var IndicatorEditor = React.createClass((_React$createClass = {
    displayName: "IndicatorEditor",

    getInitialState: function getInitialState() {
        return {
            data: {},
            view: CONSTANTS.GENERAL
        };
    },

    componentWillUnmount: function componentWillUnmount() {
        this.state = {
            data: {},
            view: CONSTANTS.GENERAL
        };
    },


    componentWillMount: function componentWillMount() {
        this.state.data = (0, _extends3.default)({}, JSON.parse((0, _stringify2.default)(defaults)), this.props.data);
    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        this.state.data = (0, _extends3.default)({}, JSON.parse((0, _stringify2.default)(defaults)), nextProps.data);
    },

    _changeSection: function _changeSection(section) {
        this.state.view = section;
        this.forceUpdate();
    },

    _onChange: function _onChange(prop, value) {
        this.state.data[prop] = value;
        this.forceUpdate();
    },

    _save: function _save() {
        var bl = new ewars.Blocker(null, "Saving changes...");
        if (this.state.data.uuid) {
            ewars.tx("com.sonoma.indicator.update", this.state.data).then(function (resp) {
                bl.destroy();
                ewars.emit("RELOAD_INDICATORS");
                ewars.growl("Indicator updated");
            }.bind(this));
        } else {
            ewars.tx('com.sonoma.indicator.create', this.state.data).then(function (resp) {
                bl.destroy();
                ewars.emit("RELOAD_INDICATORS");
                ewars.growl("Indicator created");
                this.setState({
                    data: resp
                });
            }.bind(this));
        }
    }

}, (0, _defineProperty3.default)(_React$createClass, "_onChange", function _onChange(prop, value) {
    var data = ewars.copy(this.state.data);
    if (prop == "value") {
        if (!data.definition) data.definition = {};
        data.definition[prop] = value;
    } else {
        data[prop] = value;
    }
    this.setState({
        data: (0, _extends3.default)({}, data)
    });
}), (0, _defineProperty3.default)(_React$createClass, "render", function render() {

    var view;

    if (this.state.view == CONSTANTS.GENERAL) view = React.createElement(GeneralSettings, { data: this.state.data, onChange: this._onChange });
    if (this.state.view == CONSTANTS.DEFINITION) view = React.createElement(DefinitionSettings, { data: this.state.data, onChange: this._onChange });
    if (this.state.view == CONSTANTS.GUIDANCE) view = React.createElement(Guidance, { data: this.state.data, onChange: this._onChange });

    return React.createElement(
        ewars.d.Layout,
        null,
        React.createElement(
            ewars.d.Toolbar,
            { label: "Indicator" },
            React.createElement(
                "div",
                { className: "btn-group pull-right" },
                React.createElement(ewars.d.Button, {
                    icon: "fa-save",
                    label: "Save",
                    ref: "saveBtn",
                    onClick: this._save })
            )
        ),
        React.createElement(
            ewars.d.Row,
            null,
            React.createElement(
                ewars.d.Cell,
                { borderRight: true, width: 250 },
                React.createElement(
                    "div",
                    { className: "ide-settings-panel" },
                    React.createElement(
                        "div",
                        { className: "ide-settings-nav" },
                        React.createElement(SettingsSection, {
                            onClick: this._changeSection,
                            view: CONSTANTS.GENERAL,
                            active: this.state.view == CONSTANTS.GENERAL,
                            label: "General" }),
                        React.createElement(SettingsSection, {
                            onClick: this._changeSection,
                            view: CONSTANTS.DEFINITION,
                            active: this.state.view == CONSTANTS.DEFINITION,
                            label: "Definition" })
                    )
                )
            ),
            React.createElement(
                ewars.d.Cell,
                null,
                view
            )
        )
    );
}), _React$createClass));

module.exports = IndicatorEditor;

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright 2013-2014 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



/**
 * Constructs an enumeration with keys equal to their value.
 *
 * For example:
 *
 *   var COLORS = keyMirror({blue: null, red: null});
 *   var myColor = COLORS.blue;
 *   var isColorValid = !!COLORS[myColor];
 *
 * The last line could not be performed if the values of the generated enum were
 * not equal to their keys.
 *
 *   Input:  {key1: val1, key2: val2}
 *   Output: {key1: key1, key2: key2}
 *
 * @param {object} obj
 * @return {object}
 */
var keyMirror = function(obj) {
  var ret = {};
  var key;
  if (!(obj instanceof Object && !Array.isArray(obj))) {
    throw new Error('keyMirror(...): Argument must be an object.');
  }
  for (key in obj) {
    if (!obj.hasOwnProperty(key)) {
      continue;
    }
    ret[key] = key;
  }
  return ret;
};

module.exports = keyMirror;


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _GroupSelector = __webpack_require__(53);

var _GroupSelector2 = _interopRequireDefault(_GroupSelector);

var _Inbound = __webpack_require__(108);

var _Inbound2 = _interopRequireDefault(_Inbound);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TextField = __webpack_require__(54);
var I18NString = __webpack_require__(55);
var TextArea = __webpack_require__(109);
var SelectField = __webpack_require__(36);
var DisplayField = __webpack_require__(56);


var fieldConfigs = {
    status: {
        options: [["ACTIVE", "Active"], ["INACTIVE", "Inactive"]]
    },
    group_id: {
        optionsSource: {
            resource: "indicator_group",
            query: {},
            valSource: "id",
            labelSource: "name",
            select: ["id", "name", "parent_id"],
            additional: [["null", "No Parent"]],
            hierarchical: true,
            hierarchyProp: "parent_id"
        }
    },
    account_id: {
        optionsSource: {
            resource: "account",
            query: {
                status: { eq: "ACTIVE" }
            },
            valSource: "id",
            labelSource: "name"
        }
    },
    value_type: {
        options: [["NUMERIC", "Numeric"], ["TEXT", "Text"]]
    }
};

var GeneralSettings = React.createClass({
    displayName: "GeneralSettings",

    getInitialState: function getInitialState() {
        return {};
    },

    _onUpdate: function _onUpdate(prop, value) {
        this.props.onChange(prop, value);
    },

    render: function render() {
        return React.createElement(
            "div",
            { className: "ide-panel ide-panel-absolute ide-scroll" },
            React.createElement(
                "div",
                { className: "ide-settings-content" },
                React.createElement(
                    "div",
                    { className: "ide-settings-header" },
                    React.createElement(
                        "div",
                        { className: "ide-settings-icon" },
                        React.createElement("i", { className: "fal fa-cog" })
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-settings-title" },
                        "General Settings"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-settings-intro" },
                    React.createElement(
                        "p",
                        null,
                        "Basic information about the indicator"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-settings-form" },
                    React.createElement(
                        "div",
                        { className: "ide-section-basic" },
                        React.createElement(
                            "div",
                            { className: "header" },
                            "Description"
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "Name*"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(I18NString, {
                                    name: "name",
                                    value: this.props.data.name,
                                    onUpdate: this._onUpdate })
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "ICODE"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(TextField, {
                                    name: "icode",
                                    value: this.props.data.icode,
                                    onUpdate: this._onUpdate })
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "Status*"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(SelectField, {
                                    name: "status",
                                    value: this.props.data.status,
                                    onUpdate: this._onUpdate,
                                    config: fieldConfigs.status })
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "Value Type *"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(SelectField, {
                                    name: "value_type",
                                    value: this.props.data.value_type,
                                    onUpdate: this._onUpdate,
                                    config: fieldConfigs.value_type })
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "Group/Folder*"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(_GroupSelector2.default, {
                                    name: "group_id",
                                    value: this.props.data.group_id,
                                    onUpdate: this._onUpdate })
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "Description"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(TextArea, {
                                    name: "description",
                                    value: this.props.data.description,
                                    onUpdate: this._onUpdate,
                                    config: { i18n: true } })
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-section-basic" },
                        React.createElement(
                            "div",
                            { className: "header" },
                            "Style"
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "Colour"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(TextField, {
                                    name: "colour",
                                    value: this.props.data.colour,
                                    onUpdate: this._onUpdate })
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-section-basic" },
                        React.createElement(
                            "div",
                            { className: "header" },
                            "Derivation"
                        ),
                        React.createElement(
                            "div",
                            { className: "description" },
                            "The following are ACTIVE forms which are configured to feed data into this indicator."
                        ),
                        React.createElement(_Inbound2.default, { id: this.props.data.uuid })
                    )
                )
            )
        );
    }
});

module.exports = GeneralSettings;

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = __webpack_require__(12);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(13);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(14);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(15);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(16);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Inbound = function (_React$Component) {
    (0, _inherits3.default)(Inbound, _React$Component);

    function Inbound(props) {
        (0, _classCallCheck3.default)(this, Inbound);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Inbound.__proto__ || (0, _getPrototypeOf2.default)(Inbound)).call(this, props));

        _this._load = function (id) {
            if (!id) return;
            ewars.tx("com.sonoma.indicator.inbound", id).then(function (resp) {
                _this.setState({
                    inbound: resp
                });
            });
        };

        _this.state = {
            inbound: []
        };
        return _this;
    }

    (0, _createClass3.default)(Inbound, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            this._load(this.props.id);
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            if (this.props.id != nextProps.id) {
                this._load(nextProps.id);
            }
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "inline-list" },
                this.state.inbound.map(function (item) {
                    return React.createElement(
                        "div",
                        { className: "list-item" },
                        React.createElement(
                            "div",
                            { className: "ide-row" },
                            React.createElement(
                                "div",
                                { className: "ide-col" },
                                __(item[1])
                            )
                        )
                    );
                })
            );
        }
    }]);
    return Inbound;
}(React.Component);

exports.default = Inbound;

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var _languages = ["en", "fr", "ar"];

var LanguageNode = React.createClass({
    displayName: "LanguageNode",

    _onClick: function _onClick() {
        this.props.onClick(this.props.value);
    },

    render: function render() {
        var className = "language";
        if (this.props.active) className += " active";

        return React.createElement(
            "li",
            { className: className },
            React.createElement(
                "div",
                { onClick: this._onClick },
                this.props.value
            )
        );
    }
});

var TextAreaField = React.createClass({
    displayName: "TextAreaField",

    _value: null,

    getInitialProps: function getInitialProps() {
        return {
            config: {
                i18n: false,
                markdown: false
            }
        };
    },

    getInitialState: function getInitialState() {
        return {
            curLang: "en"
        };
    },

    componentWillMount: function componentWillMount() {
        // A little clean up, fix labels which are not dicts to dicts

    },

    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {},

    onChange: function onChange(event) {
        var path = this.props.config ? this.props.config.path : null;

        if (this.props.config.i18n) {
            this._value[this.state.curLang] = event.target.value;
            this.props.onUpdate(this.props.name, this._value, path);
        } else {
            this.props.onUpdate(this.props.name, event.target.value, path);
        }
    },

    _handleLanguageChange: function _handleLanguageChange(lang) {
        this.setState({
            curLang: lang
        });
    },

    render: function render() {
        this._value = this.props.value;
        var viewValue = this.props.value;

        if (this.props.config.i18n) {

            if (!this.props.value) {
                this._value = { en: "", fr: null };
            }

            if (typeof this.props.value == "string") {
                this._value = {
                    en: this.props.value,
                    fr: null
                };
            }

            if (!this._value[this.state.curLang]) this._value[this.state.curLang] = "";
            viewValue = this._value[this.state.curLang];
        }

        if (this.props.config.i18n) {
            var nodes = _.map(_languages, function (language) {
                return React.createElement(LanguageNode, {
                    value: language,
                    key: language,
                    onClick: this._handleLanguageChange,
                    active: this.state.curLang == language });
            }, this);

            nodes.unshift(React.createElement(
                "li",
                { className: "icon" },
                React.createElement(
                    "div",
                    { className: "icon" },
                    React.createElement("i", { className: "fal fa-language" })
                )
            ));
        }

        if (!viewValue) viewValue = "";

        return React.createElement(
            "div",
            { className: "textarea-wrapper" },
            this.props.config.i18n ? React.createElement(
                "div",
                { className: "text-area-controls" },
                React.createElement(
                    "ul",
                    { className: "language-list" },
                    nodes
                )
            ) : null,
            React.createElement("textarea", {
                onChange: this.onChange,
                disabled: this.props.readOnly,
                placeholder: this.props.placeholder,
                className: "form-control",
                name: this.props.name,
                value: viewValue }),
            this.props.config.markdown ? React.createElement(
                "p",
                { className: "small" },
                React.createElement("i", { className: "fal fa-markdown" }),
                " \xA0This field supports markdown"
            ) : null
        );
    }

});

module.exports = TextAreaField;

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Spinner = React.createClass({
    displayName: "Spinner",

    render: function render() {
        return React.createElement(
            "div",
            { className: "spinner-wrap" },
            React.createElement(
                "div",
                { className: "spinner" },
                React.createElement("i", { className: "fal fa-spin fa-circle-o-notch" })
            )
        );
    }
});

module.exports = Spinner;

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var SelectField = __webpack_require__(36);
var DisplayField = __webpack_require__(56);
var SwitchField = __webpack_require__(112);
var IndicatorDefinition = __webpack_require__(113);
var NumberField = __webpack_require__(114);

var fieldConfigs = {
    value_type: {
        options: [["NUMERIC", "Numeric"], ["JSON", "JSON"], ["TEXT", "Text"]]
    },
    itype: {
        options: [["DEFAULT", "Default"], ["STATIC", "Static"]]
    }
};

var DefinitionSettings = React.createClass({
    displayName: "DefinitionSettings",

    getInitialState: function getInitialState() {
        return {};
    },

    _onUpdate: function _onUpdate(prop, value) {
        this.props.onChange(prop, value);
    },

    render: function render() {

        var typeLabel = "Default";
        if (this.props.data.itype == "STATIC") typeLabel = "Static Value";

        return React.createElement(
            "div",
            { className: "ide-panel ide-panel-absolute ide-scroll" },
            React.createElement(
                "div",
                { className: "ide-settings-content" },
                React.createElement(
                    "div",
                    { className: "ide-settings-header" },
                    React.createElement(
                        "div",
                        { className: "ide-settings-icon" },
                        React.createElement("i", { className: "fal fa-cog" })
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-settings-title" },
                        "Definition"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-settings-intro" },
                    React.createElement(
                        "p",
                        null,
                        "Settings which dictate how this indicator operates within the overall EWARS system"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-settings-form" },
                    React.createElement(
                        "div",
                        { className: "ide-section-basic" },
                        React.createElement(
                            "div",
                            { className: "header" },
                            "General Settings"
                        ),
                        React.createElement(
                            "div",
                            { className: "hsplit-box" },
                            React.createElement(
                                "div",
                                { className: "ide-setting-label" },
                                "Indicator Type*"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-setting-control" },
                                React.createElement(SelectField, {
                                    name: "itype",
                                    value: this.props.data.itype,
                                    onUpdate: this._onUpdate,
                                    config: fieldConfigs.itype })
                            )
                        )
                    ),
                    this.props.data.itype == "STATIC" ? React.createElement(
                        "div",
                        { className: "ide-section-basic" },
                        React.createElement(
                            "div",
                            { className: "header" },
                            typeLabel || ""
                        ),
                        React.createElement(
                            "div",
                            { style: { paddingLeft: "16px", paddingRight: "16px", marginTop: "5px", marginBottom: "5px" } },
                            React.createElement(NumberField, {
                                value: this.props.data.definition ? this.props.data.definition.value || 0 : "",
                                name: "value",
                                onUpdate: this._onUpdate })
                        )
                    ) : null
                )
            )
        );
    }
});

module.exports = DefinitionSettings;

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var SwitchField = React.createClass({
    displayName: "SwitchField",

    getInitialState: function getInitialState() {
        return {};
    },

    onChange: function onChange() {
        var value = void 0;
        if ([true, "true"].indexOf(this.props.value) >= 0) value = true;
        if ([false, "false", -1, 0].indexOf(this.props.value) >= 0) value = false;

        console.log(value);
        if (this.props.config && this.props.config.path) {
            this.props.onUpdate(this.props.name, !value, this.props.config.path);
        } else {
            this.props.onUpdate(this.props.name, !value);
        }
    },

    render: function render() {
        var value = false;
        if (this.props.value) value = true;

        var name = ewars.utils.uniqueId("switch_");

        var className = "fal fa-toggle-off";
        if (value) className = "fal fa-toggle-on";

        var style = {};
        if (value) style.color = "green";

        return React.createElement(
            "div",
            { className: "ide-toggle", onClick: this.onChange },
            React.createElement("i", { style: style, className: className })
        );
    }
});

module.exports = SwitchField;

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _getPrototypeOf = __webpack_require__(12);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(13);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(14);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(15);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(16);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Button = __webpack_require__(57);
var TextField = __webpack_require__(54);
var SelectField = __webpack_require__(36);

var REDUCTION = {
    options: [["SUM", "Sum"], ["MEDIAN", "Median"]]
};

var CompoundManager = function (_React$Component) {
    (0, _inherits3.default)(CompoundManager, _React$Component);

    function CompoundManager(props) {
        (0, _classCallCheck3.default)(this, CompoundManager);

        var _this = (0, _possibleConstructorReturn3.default)(this, (CompoundManager.__proto__ || (0, _getPrototypeOf2.default)(CompoundManager)).call(this, props));

        _this._onUpdate = function (prop, value) {
            _this.props.onUpdate(prop, value);
        };

        return _this;
    }

    (0, _createClass3.default)(CompoundManager, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "basic" },
                React.createElement(
                    "div",
                    { className: "hsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Formula"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(TextField, {
                            name: "formula",
                            value: this.props.data.definition.formula,
                            onUpdate: this._onCompoundChange })
                    )
                ),
                React.createElement(
                    "div",
                    { className: "vsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Indicators"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(IndicatorList, { data: this.props.data, onUpdate: this._onUpdate })
                    )
                )
            );
        }
    }]);
    return CompoundManager;
}(React.Component);

var IndicatorItem = function (_React$Component2) {
    (0, _inherits3.default)(IndicatorItem, _React$Component2);

    function IndicatorItem(props) {
        (0, _classCallCheck3.default)(this, IndicatorItem);

        var _this2 = (0, _possibleConstructorReturn3.default)(this, (IndicatorItem.__proto__ || (0, _getPrototypeOf2.default)(IndicatorItem)).call(this, props));

        _this2._remove = function () {
            _this2.props.onRemove(_this2.props.index);
        };

        _this2.state = {
            indicator: {}
        };

        return _this2;
    }

    (0, _createClass3.default)(IndicatorItem, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            ewars.tx("com.ewars.resource", ["indicator", this.props.data, ["uuid", "name", "definition", "itype", "icode"], null]).then(function (resp) {
                this.setState({
                    indicator: resp
                });
            }.bind(this));
        }
    }, {
        key: "render",
        value: function render() {
            var indName = this.state.indicator.icode || "" + " " + ewars.I18N(this.state.indicator.name || "Loading");

            return React.createElement(
                "div",
                { className: "block", style: { padding: 0 } },
                React.createElement(
                    "div",
                    { className: "block-content", style: { padding: 0 } },
                    React.createElement(
                        "div",
                        { className: "ide-layout" },
                        React.createElement(
                            "div",
                            { className: "ide-row" },
                            React.createElement(
                                "div",
                                { className: "ide-col", style: { padding: 8 } },
                                indName
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-col", style: { padding: 8 } },
                                React.createElement(
                                    "div",
                                    { className: "btn-group pull-right" },
                                    React.createElement(ewars.d.Button, {
                                        onClick: this._remove,
                                        icon: "fa-times" })
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);
    return IndicatorItem;
}(React.Component);

var IndicatorList = function (_React$Component3) {
    (0, _inherits3.default)(IndicatorList, _React$Component3);

    function IndicatorList(props) {
        (0, _classCallCheck3.default)(this, IndicatorList);

        var _this3 = (0, _possibleConstructorReturn3.default)(this, (IndicatorList.__proto__ || (0, _getPrototypeOf2.default)(IndicatorList)).call(this, props));

        _this3._onDragEnter = function (e) {
            e.preventDefault();
        };

        _this3._onDragLeave = function (e) {};

        _this3._onDropped = function (e) {
            e.preventDefault();
            e.stopPropagation();

            var data = JSON.parse(e.dataTransfer.getData("item"));

            var definition = ewars.copy(_this3.props.data.definition);
            definition.series.push(data.uuid);

            _this3.props.onUpdate("series", definition.series);
        };

        _this3._remove = function (index) {
            var definition = ewars.copy(_this3.props.data.definition);
            definition.series.splice(index, 1);
            _this3.props.onUpdate("series", definition.series);
        };

        return _this3;
    }

    (0, _createClass3.default)(IndicatorList, [{
        key: "render",
        value: function render() {
            var view = void 0;
            if (this.props.data.definition.series.length <= 0) {
                view = React.createElement(
                    "div",
                    { className: "placeholder" },
                    "Drag 'n' Drop indicators here"
                );
            } else {
                view = this.props.data.definition.series.map(function (item, index) {
                    return React.createElement(IndicatorItem, {
                        data: item,
                        key: item.uuid || item,
                        onRemove: this._remove,
                        index: index });
                }.bind(this));
            }

            return React.createElement(
                "div",
                { className: "ind-drop-zone", onDragOver: this._onDragEnter, onDragLeave: this._onDragLeave,
                    onDrop: this._onDropped },
                React.createElement(
                    "div",
                    { className: "block-tree" },
                    view
                )
            );
        }
    }]);
    return IndicatorList;
}(React.Component);

var AggregateManager = function (_React$Component4) {
    (0, _inherits3.default)(AggregateManager, _React$Component4);

    function AggregateManager(props) {
        (0, _classCallCheck3.default)(this, AggregateManager);

        var _this4 = (0, _possibleConstructorReturn3.default)(this, (AggregateManager.__proto__ || (0, _getPrototypeOf2.default)(AggregateManager)).call(this, props));

        _this4._onUpdate = function (prop, value) {
            _this4.props.onUpdate(prop, value);
        };

        return _this4;
    }

    (0, _createClass3.default)(AggregateManager, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "basic" },
                React.createElement(
                    "div",
                    { className: "hsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Aggregation Type"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(SelectField, {
                            name: "reduction",
                            onUpdate: this._onUpdate,
                            config: REDUCTION,
                            value: this.props.data.definition.reduction })
                    )
                ),
                React.createElement(
                    "div",
                    { className: "vsplit-box" },
                    React.createElement(
                        "div",
                        { className: "ide-setting-label" },
                        "Indicators"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-setting-control" },
                        React.createElement(IndicatorList, { data: this.props.data, onUpdate: this._onUpdate })
                    )
                )
            );
        }
    }]);
    return AggregateManager;
}(React.Component);

var IndicatorDefinition = React.createClass({
    displayName: "IndicatorDefinition",

    getInitialState: function getInitialState() {
        return {
            definition: {}
        };
    },

    _updateDefinition: function _updateDefinition(prop, value) {
        var data = ewars.copy(this.props.value.definition);
        data[prop] = value;

        this.props.onUpdate("definition", data);
    },

    render: function render() {

        var edit = void 0;
        if (this.props.value) {
            if (this.props.value.itype == "COMPOUND") edit = React.createElement(CompoundManager, { data: this.props.value, onUpdate: this._updateDefinition });
            if (this.props.value.itype == "AGGREGATE") edit = React.createElement(AggregateManager, { data: this.props.value, onUpdate: this._updateDefinition });
        }

        return React.createElement("div", null);
    }
});

module.exports = IndicatorDefinition;

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _assign = __webpack_require__(42);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var reg = /^\d+$/;

var NumberField = React.createClass({
    displayName: "NumberField",

    propTypes: {
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool,
        config: React.PropTypes.object
    },

    getDefaultProps: function getDefaultProps() {
        return {
            placeholder: "Enter number",
            name: null,
            readOnly: false,
            path: null,
            config: {
                allow_negative: true,
                decimal_allowed: true,
                style: {}
            }
        };
    },

    _options: {},

    componentDidMount: function componentDidMount() {
        if (this.props.focus) {
            this.refs.inputField.focus();
        }
    },

    getInitialState: function getInitialState() {
        return {};
    },

    _onChange: function _onChange(e) {
        var value = e.target.value;

        if (!this.props.config.decimal_allowed) value = value.split(".")[0];
        if (!this.props.config.allow_negative) value = value.replace("-", "");

        value = value.replace(/[^\d.-]/g, '');

        var name = this.props.config.nameOverride || this.props.name;
        var path = this.props.config.path || this.props.name;

        //value = value.replace(/^0+/, '');
        this.props.onUpdate(name, value, path);
    },

    render: function render() {
        var value = this.props.value || "";

        var style = { paddingRight: "5px" };
        if (this.props.style) (0, _assign2.default)(style, this.props.style);

        return React.createElement("input", { ref: "inputField",
            disabled: this.props.readOnly,
            className: "form-control number-field",
            style: style,
            onChange: this._onChange,
            type: "text",
            value: value,
            placeholder: this.props.placeholder || 0,
            name: this.props.name });
    }

});

module.exports = NumberField;

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Guidance = React.createClass({
    displayName: "Guidance",

    getInitialState: function getInitialState() {
        return {};
    },

    _onUpdate: function _onUpdate(prop, value) {
        this.props.onChange(prop, value);
    },

    render: function render() {
        return React.createElement(
            "div",
            { className: "ide-panel ide-panel-absolute ide-scroll" },
            React.createElement(
                "div",
                { className: "ide-settings-content" },
                React.createElement(
                    "div",
                    { className: "ide-settings-header" },
                    React.createElement(
                        "div",
                        { className: "ide-settings-icon" },
                        React.createElement("i", { className: "fal fa-cog" })
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-settings-title" },
                        "General Settings"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-settings-intro" },
                    React.createElement(
                        "p",
                        null,
                        "Basic information about the indicator"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "ide-settings-form" },
                    React.createElement(
                        "div",
                        { className: "ide-section-basic" },
                        React.createElement(
                            "div",
                            { className: "placeholder" },
                            "Guidance editor coming soon"
                        )
                    )
                )
            )
        );
    }
});

module.exports = Guidance;

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _TextInputField = __webpack_require__(54);

var _TextInputField2 = _interopRequireDefault(_TextInputField);

var _GroupSelector = __webpack_require__(53);

var _GroupSelector2 = _interopRequireDefault(_GroupSelector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = __webpack_require__(57);
var LanguageStringField = __webpack_require__(55);


var defaults = {
    id: null,
    name: {
        en: null
    },
    parent_id: null
};

var fields = {
    parent_id: {
        optionsSource: {
            resource: "indicator_group",
            query: {},
            select: ["id", "name", "parent_id"],
            valSource: "id",
            labelSource: "name",
            additional: [["null", "No Parent"]],
            hierarchical: true,
            hierarchyProp: "parent_id"
        }
    }
};

var GroupEditor = React.createClass({
    displayName: "GroupEditor",

    getInitialState: function getInitialState() {
        return {};
    },

    _onChange: function _onChange(prop, value) {
        this.props.onChange(prop, value);
    },

    _onSave: function _onSave() {
        var _this = this;

        var bl = new ewars.Blocker(null, "Saving changes...");
        if (this.props.data.uuid) {
            ewars.tx('com.sonoma.indicator_group.update', this.props.data).then(function (res) {
                bl.destroy();
                _this.props.onSet(res);
                ewars.growl("Indicator group updated");
                ewars.emit("RELOAD_INDICATORS");
            }).catch(function (err) {
                console.log(err);
                bl.destroy();
                ewars.growl("Error saving indicator group");
            });
        } else {
            ewars.tx('com.sonoma.indicator_group.create', this.props.data).then(function (res) {
                _this.props.onSet(res);
                bl.destroy();
                ewars.growl("Indicator Group created");
                ewars.emit("RELOAD_INDICATORS");
            }).catch(function (err) {
                bl.destroy();
                ewars.growl("Error saving indicator group");
            });
        }
    },

    render: function render() {
        return React.createElement(
            ewars.d.Layout,
            null,
            React.createElement(
                ewars.d.Toolbar,
                { label: "Indicator Group" },
                React.createElement(
                    "div",
                    { className: "btn-group pull-right" },
                    React.createElement(ewars.d.Button, {
                        icon: "fa-save",
                        label: "Save",
                        color: "green",
                        onClick: this._onSave })
                )
            ),
            React.createElement(
                ewars.d.Row,
                null,
                React.createElement(
                    ewars.d.Cell,
                    null,
                    React.createElement(
                        "div",
                        { className: "ide-panel ide-panel-absolute ide-scroll" },
                        React.createElement(
                            "div",
                            { className: "ide-settings-panel" },
                            React.createElement(
                                "div",
                                { className: "ide-settings-content" },
                                React.createElement(
                                    "div",
                                    { className: "ide-settings-header" },
                                    React.createElement(
                                        "div",
                                        { className: "ide-settings-icon" },
                                        React.createElement("i", { className: "fal fa-cog" })
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "ide-settings-title" },
                                        "Indicator Group"
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-settings-intro" },
                                    React.createElement(
                                        "p",
                                        null,
                                        "Basic information about the indicator"
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-settings-form" },
                                    React.createElement(
                                        "div",
                                        { className: "ide-section-basic" },
                                        React.createElement(
                                            "div",
                                            { className: "header" },
                                            "General"
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "hsplit-box" },
                                            React.createElement(
                                                "div",
                                                { className: "ide-setting-label" },
                                                "Name"
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "ide-setting-control" },
                                                React.createElement(_TextInputField2.default, {
                                                    name: "name",
                                                    value: this.props.data.name,
                                                    onUpdate: this._onChange })
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "hsplit-box" },
                                            React.createElement(
                                                "div",
                                                { className: "ide-setting-label" },
                                                "Parent"
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "ide-setting-control" },
                                                React.createElement(_GroupSelector2.default, {
                                                    name: "pid",
                                                    value: this.props.data.pid,
                                                    onUpdate: this._onChange })
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
    }
});

module.exports = GroupEditor;

/***/ })
/******/ ]);
//# sourceMappingURL=indicators-dev.map?[contentHash]