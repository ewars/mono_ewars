window.__ENAME__ = "register"; window.__EVERSION__ = "1.0.2";
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Component = __webpack_require__(1);

var readyStateCheckInterval = setInterval(function () {
    if (document.readyState === "complete") {
        clearInterval(readyStateCheckInterval);

        var _user = document.getElementById("user"),
            _account = document.getElementById("account"),
            _orgs = document.getElementById("orgs");

        if (_user) {
            window.user = JSON.parse(_user.innerText || '{}');
            _user.innerText = "";
        }

        if (_account) {
            window.account = JSON.parse(_account.innerText || '{}');
            _account.innerText = "";
        }

        if (_orgs) {
            window.orgs = JSON.parse(_orgs.innerText || '[]');
            _orgs.innerText = "";
        }

        ReactDOM.render(React.createElement(Component, null), document.getElementById('app'));
    }
}, 10);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _defineProperty2 = __webpack_require__(39);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends4 = __webpack_require__(58);

var _extends5 = _interopRequireDefault(_extends4);

var _stringify = __webpack_require__(26);

var _stringify2 = _interopRequireDefault(_stringify);

var _getPrototypeOf = __webpack_require__(27);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(28);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(29);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(30);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(37);

var _inherits3 = _interopRequireDefault(_inherits2);

var _Selector = __webpack_require__(54);

var _Selector2 = _interopRequireDefault(_Selector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserDetails = __webpack_require__(98);


var CODES = {
    NO_PASSWORD: "Please provide a valid password.",
    NO_EMAIL: "Please provide a valid email address.",
    USER_PENDING_APPROVAL: "You have already requested access to the account specified, access is awaiting approval of an account administrator.",
    USER_REVOKED: "You had prior access to this account which was revoked by an account administrator, please contact an admin for assistance.",
    USER_EXISTS: "A user is already registered under the email address provided, please login or register with a different email address.",
    PENDING_VERIFICATION: "The account you are trying to request access for is currently awaiting email verification, please check your email account for an email from EWARS asking you to verify your email address.",
    USER_REMOVED: "You has prior access to this account which was revoked by an account administrator, please contact an admin for assistance.",
    SSO_EXISTS_NO_CONTEXT: "",
    PENDING_APPROVAL: "You have already submitted an access request for this account, it is currently pending approval form an Account Administrator."
};

var Completed = React.createClass({
    displayName: "Completed",

    render: function render() {
        return React.createElement(
            "div",
            { className: "registration-wizard" },
            React.createElement(
                "h1",
                { className: "register" },
                "Registration Complete"
            ),
            React.createElement(
                "div",
                { className: "widget" },
                React.createElement(
                    "div",
                    { className: "body" },
                    React.createElement(
                        "div",
                        { className: "article" },
                        React.createElement(
                            "p",
                            null,
                            "A verification email has been sent to the email that you have provided, please check your email inbox and click the link provided in the email to verify your ownership of the email address."
                        ),
                        React.createElement(
                            "p",
                            null,
                            "Once your email address has been verified an administrator will review your registration request and either approve or reject it, you will receive an email with the outcome. If you your account is approved, you will be able to login at that time."
                        ),
                        React.createElement(
                            "p",
                            null,
                            "Thank you for submitting your registration request."
                        )
                    )
                )
            )
        );
    }
});

var CompletedExisting = function (_React$Component) {
    (0, _inherits3.default)(CompletedExisting, _React$Component);

    function CompletedExisting() {
        (0, _classCallCheck3.default)(this, CompletedExisting);
        return (0, _possibleConstructorReturn3.default)(this, (CompletedExisting.__proto__ || (0, _getPrototypeOf2.default)(CompletedExisting)).apply(this, arguments));
    }

    (0, _createClass3.default)(CompletedExisting, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "registration-wizard" },
                React.createElement(
                    "h1",
                    { className: "register" },
                    "Request Sent"
                ),
                React.createElement(
                    "div",
                    { className: "widget" },
                    React.createElement(
                        "div",
                        { className: "body" },
                        React.createElement(
                            "div",
                            { className: "article" },
                            React.createElement(
                                "p",
                                null,
                                "Account administrators for the account you've requested access to have been notified of your request, you will receive an email once an Account Administrator approves or rejects your request for access."
                            ),
                            React.createElement(
                                "p",
                                null,
                                "Once approved you will be able to log in and access the account."
                            )
                        )
                    )
                )
            );
        }
    }]);
    return CompletedExisting;
}(React.Component);

var CELL_STYLE = {
    maxWidth: 150,
    fontWeight: "bold",
    textAlign: "right",
    paddingRight: 10,
    paddingTop: 8
};

var Help = function (_React$Component2) {
    (0, _inherits3.default)(Help, _React$Component2);

    function Help() {
        (0, _classCallCheck3.default)(this, Help);
        return (0, _possibleConstructorReturn3.default)(this, (Help.__proto__ || (0, _getPrototypeOf2.default)(Help)).apply(this, arguments));
    }

    (0, _createClass3.default)(Help, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "article", style: { padding: 16 } },
                React.createElement(
                    "h4",
                    null,
                    "Getting into EWARS"
                ),
                React.createElement(
                    "p",
                    null,
                    "You'll notice two sections above if you are currenty not logged into the system."
                ),
                React.createElement(
                    "ul",
                    { className: "register" },
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "New Users"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "Use this form to become a user of the EWARS system and request access to an EWARS account when you haven't previously registered before."
                    ),
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "Existing Users"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "If you currently already registered in the system, but would like access to other accounts (such as a different country or contextual account) you can do so from here."
                    ),
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "Help"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "This help document."
                    )
                ),
                React.createElement(
                    "h4",
                    null,
                    "Trouble logging in?"
                ),
                React.createElement(
                    "p",
                    null,
                    "If you've already registered as a user on EWARS and can't log in, there may be a number of reasons:"
                ),
                React.createElement(
                    "ul",
                    { className: "register" },
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "You haven't verified your email address"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "When you registered, the EWARS system will have sent an email to your email address requesting you click a link in the email. This is to ensure that the email address you registered with belongs to you. If you can't find the email, please check your junk/spam folder. You can request a new verification email ",
                        React.createElement(
                            "a",
                            {
                                href: "/verifier" },
                            "here"
                        ),
                        "."
                    ),
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "You're using an incorrect password"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "If you suspect the password you're using is incorrect, you can reset your password ",
                        React.createElement(
                            "a",
                            { href: "/recovery" },
                            "here"
                        )
                    ),
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "Your account access is currently awaiting approval"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "Access to specific accounts under EWARS is subject to approval from an administrator of the account. Until an administrator approves your registration or access request, you will not be able to access the account. Once access is approved you will receive an email at the address you registered with notifying you that you can now log in."
                    ),
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "An Account Administrator revoked your access"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "If you only have access to a single account, an account administrator may have revoked your access for internal reasons. Please contact an Account administrator for the account in question to find out why."
                    ),
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "Poor device connection"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "If you are attempting to log in using either the EWARS mobile or desktop applications, please check that you have internet access in order for the application to access EWARS to authenticate. You will only need to be connected to the internet for the initial login and syncing for these devices."
                    ),
                    React.createElement(
                        "li",
                        { className: "bolder" },
                        "Account Controlled Users"
                    ),
                    React.createElement(
                        "li",
                        null,
                        "If your user is an ",
                        React.createElement(
                            "strong",
                            null,
                            "account-controlled user"
                        ),
                        " you can not request access to other accounts. Account controlled users are users which are created strictly to work with the account in question in order that account administrators can exercise a higher level of control (changing passwords for instance). Because of this, account-controlled users can not be added to other accounts."
                    )
                ),
                React.createElement(
                    "h4",
                    null,
                    "Single Sign On"
                ),
                React.createElement(
                    "p",
                    null,
                    "The EWARS system uses a single sign-on scheme for managing users. This means that you can use your email address and password to access multiple different accounts without having to re-register the same address or manage multiple passwords."
                ),
                React.createElement(
                    "h4",
                    null,
                    "More help?"
                ),
                React.createElement(
                    "p",
                    null,
                    "If you're not sure what to do, or are experiencing problems not addressed above please contact ",
                    React.createElement(
                        "a",
                        { href: "mailto:support@ewars.ws" },
                        "support@ewars.ws"
                    )
                )
            );
        }
    }]);
    return Help;
}(React.Component);

var ExistingUserRequest = function (_React$Component3) {
    (0, _inherits3.default)(ExistingUserRequest, _React$Component3);

    function ExistingUserRequest(props) {
        (0, _classCallCheck3.default)(this, ExistingUserRequest);

        var _this3 = (0, _possibleConstructorReturn3.default)(this, (ExistingUserRequest.__proto__ || (0, _getPrototypeOf2.default)(ExistingUserRequest)).call(this, props));

        _this3._onSelect = function (data) {
            _this3.props.onChange("account", data);
            _this3.setState({
                show: false
            });
        };

        _this3._selectAccount = function () {
            _this3.setState({ show: true });
        };

        _this3._onModalAction = function (action) {
            if (action == "CLOSE") _this3.setState({ show: false });
        };

        _this3._clearAccount = function () {
            _this3.props.onChange("account_id", null);
        };

        _this3._onChange = function (e) {
            _this3.props.onChange(e.target.name, e.target.value);
        };

        var email = "";
        if (window.user) email = window.user.email;
        _this3.state = {
            show: false
        };
        return _this3;
    }

    (0, _createClass3.default)(ExistingUserRequest, [{
        key: "componentWillMount",
        value: function componentWillMount() {}
    }, {
        key: "render",
        value: function render() {
            var readOnly = false;
            if (window.user) readOnly = true;

            var emailLabel = "Email *";
            if (window.user) emailLabel = "Your Account Email";

            if (window.user) {
                if (window.user.system) {
                    return React.createElement(
                        "div",
                        null,
                        React.createElement(
                            "div",
                            { className: "article",
                                style: { paddingRight: 150, paddingLeft: 150, paddingTop: 30, paddingBottom: 30, marginBottom: 40 } },
                            React.createElement(
                                "p",
                                { className: "register" },
                                "You are signed in as an account-controlled user, account-controlled users are not allowed to access accounts other than their origin account at this time."
                            )
                        )
                    );
                }
            }

            return React.createElement(
                "div",
                null,
                React.createElement(
                    "div",
                    { className: "article",
                        style: { paddingRight: 150, paddingLeft: 30, paddingTop: 30, marginBottom: 40 } },
                    React.createElement(
                        "div",
                        { className: "ide-layout" },
                        React.createElement(
                            "div",
                            { className: "ide-row" },
                            React.createElement("div", { className: "ide-col", style: { maxWidth: 150 } }),
                            React.createElement(
                                "div",
                                { className: "ide-col" },
                                React.createElement(
                                    "p",
                                    null,
                                    "Please fill out the details below to request access to an account."
                                )
                            )
                        ),
                        !window.user ? React.createElement(
                            ewars.d.Row,
                            { style: { marginBottom: 8 } },
                            React.createElement(
                                ewars.d.Cell,
                                { style: CELL_STYLE },
                                emailLabel
                            ),
                            React.createElement(
                                ewars.d.Cell,
                                null,
                                React.createElement("input", {
                                    type: "text",
                                    name: "email",
                                    disabled: readOnly,
                                    onChange: this._onChange,
                                    value: this.props.data.email,
                                    placeholder: "Enter your email..." })
                            )
                        ) : null,
                        !window.user ? React.createElement(
                            ewars.d.Row,
                            { style: { marginBottom: 8 } },
                            React.createElement(
                                ewars.d.Cell,
                                { style: CELL_STYLE },
                                "Password *"
                            ),
                            React.createElement(
                                ewars.d.Cell,
                                null,
                                React.createElement("input", {
                                    type: "password",
                                    name: "password",
                                    disabled: readOnly,
                                    onChange: this._onChange,
                                    value: this.props.data.password,
                                    placeholder: "Enter your password..." })
                            )
                        ) : null,
                        React.createElement(
                            "div",
                            { className: "ide-row", style: { marginBottom: 8 } },
                            React.createElement(
                                "div",
                                { className: "ide-col",
                                    style: {
                                        maxWidth: 150,
                                        fontWeight: "bold",
                                        textAlign: "right",
                                        paddingRight: 10,
                                        paddingTop: 8
                                    } },
                                "Organization *"
                            ),
                            React.createElement(
                                "div",
                                { className: "ide-col" },
                                React.createElement(
                                    "select",
                                    { value: this.props.data.org_id,
                                        name: "org_id",
                                        onChange: this._onChange },
                                    React.createElement(
                                        "optgroup",
                                        null,
                                        React.createElement(
                                            "option",
                                            { value: "" },
                                            "None Selected"
                                        ),
                                        React.createElement(
                                            "option",
                                            { value: "OTHER" },
                                            "Other (Not listed)"
                                        )
                                    ),
                                    React.createElement(
                                        "optgroup",
                                        null,
                                        (window.orgs || []).map(function (item) {
                                            return React.createElement(
                                                "option",
                                                { value: item.uuid },
                                                item.name
                                            );
                                        })
                                    )
                                )
                            )
                        ),
                        this.props.data.org_id == "OTHER" ? React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "div",
                                { className: "ide-row", style: { marginBottom: 8 } },
                                React.createElement(
                                    "div",
                                    { className: "ide-col",
                                        style: {
                                            maxWidth: 150,
                                            fontWeight: "bold",
                                            textAlign: "right",
                                            paddingRight: 10,
                                            paddingTop: 8
                                        } },
                                    "Other Org Name *"
                                ),
                                React.createElement(
                                    "div",
                                    { className: "ide-col" },
                                    React.createElement("input", {
                                        name: "org_name",
                                        onChange: this._onChange,
                                        value: this.props.data.org_name,
                                        type: "text" })
                                )
                            )
                        ) : null
                    )
                ),
                this.state.show ? React.createElement(
                    ewars.d.Shade,
                    {
                        title: "Select Item",
                        icon: "fa-search",
                        onAction: this._onModalAction,
                        shown: this.state.show },
                    React.createElement(_Selector2.default, {
                        resource: "account",
                        onSelect: this._onSelect })
                ) : null
            );
        }
    }]);
    return ExistingUserRequest;
}(React.Component);

var re = {
    test: function test(val) {
        if (val.indexOf("@") < 0) return false;
        var split = val.split("@");
        if (split[1].indexOf(".") < 0) return false;
        var dom_split = split[1].split(".");
        if (dom_split.length <= 1) return false;
        if (split[0].length <= 0) return false;
        if (val.indexOf("'") >= 0) return false;
        if (val.indexOf('"') >= 0) return false;
        return true;
    }
};

var Component = React.createClass({
    displayName: "Component",

    getInitialState: function getInitialState() {
        var email = "";
        if (window.user) email = window.user.email;

        return {
            data: {
                name: "",
                email: "",
                confirm_email: "",
                password: "",
                confirm_password: "",
                account_id: "",
                org_id: "",
                user_id: null
            },
            exist: {
                email: email,
                password: "",
                account_id: null
            },
            view: "NEW"
        };
    },

    _validate: function _validate() {
        if (this.state.view == "EXIST") {
            if (window.user) {

                // We're OK, process the request
                this._doAccessRequest();
            } else {
                // No logged in user, so we need to authenticate
                if (!this.state.data.email || this.state.data.email == "") {
                    this.setState({ error: _l("Please enter a valid email") });
                    return;
                }

                if (!re.test(this.state.data.email)) {
                    this.setState({ error: _l("Please enter a valid email") });
                    return;
                }

                if (!this.state.data.password || this.state.data.password == "") {
                    this.setState({ error: l("Please enter a valid password") });
                    return;
                }

                if (!this.state.data.account_id || this.state.data.account_id == "") {
                    this.setState({ error: _l("Please select an account") });
                    return;
                }

                // We're OK, can now process the request
                this._doAccessRequest();
            }
            return;
        }

        if (!this.state.data.name || this.state.data.name == "") {
            this.setState({ error: "Please provide a valid name." });
            return;
        }

        if (!this.state.data.email || this.state.data.email == "") {
            this.setState({ error: "Please provide a valid email" });
            return;
        }

        if (!re.test(this.state.data.email)) {
            this.setState({ error: _l("Please provide a valid email address") });
            return;
        }

        if (this.state.data.email != this.state.data.confirm_email) {
            this.setState({ error: "Emails do not match" });
            return;
        }

        if (!this.state.data.password || this.state.data.password == "") {
            this.setState({ error: "Please provide a valid password: passwords must be a minimum of 6 characters one of which must be a number or non-standard character" });
            return;
        }

        if (this.state.data.password != this.state.data.confirm_password) {
            this.setState({ error: "Passwords do not match" });
            return;
        }

        if (!this.state.data.org_id || this.state.data.org_id == "") {
            this.setState({ error: "Please select a valid organization, or select Other if your organization isn't represented." });
            return;
        }

        this._doRegister();
    },

    _doAccessRequest: function _doAccessRequest() {
        var _this4 = this;

        var bl = new ewars.Blocker(null, "Requesting access...");
        var data = (0, _stringify2.default)({
            email: this.state.data.email,
            password: this.state.data.password
        });

        var r = new XMLHttpRequest();
        r.open("POST", "/register/access");
        r.onreadystatechange = function () {
            if (r.readyState != 4 && r.status != 200) {
                bl.destroy();
                _this4.setState({
                    error: "An unknown error occurred, please contact a system administrator at support@ewars.ws"
                });
            } else {
                bl.destroy();
                if (r.readyState == 4 && r.status == 200) {
                    var resp = JSON.parse(r.responseText);

                    if (resp.err) {
                        _this4.setState({ error: CODES[resp.code] });
                    } else {
                        _this4.setState({ completed: true });
                    }
                }
            }
        };

        r.send(data);
    },

    _doRegister: function _doRegister() {
        var _this5 = this;

        var bl = new ewars.Blocker(null, "Registering...");
        var data = (0, _stringify2.default)({
            name: this.state.data.name,
            email: this.state.data.email,
            password: this.state.data.password,
            org_id: this.state.data.org_id,
            org_name: this.state.data.org_name
        });

        var r = new XMLHttpRequest();
        r.open("POST", "/register");
        r.onreadystatechange = function () {
            if (r.readyState != 4 && r.status != 200) {
                bl.destroy();
                _this5.setState({
                    error: "An unknown error occurred, please contact a system administrator at support@ewars.ws"
                });
            } else {
                bl.destroy();
                if (r.readyState == 4 && r.status == 200) {
                    var resp = JSON.parse(r.responseText);

                    if (resp.err) {
                        _this5.setState({
                            error: CODES[resp.code]
                        });
                    } else {
                        _this5.setState({ completed: true });
                    }
                }
            }
        };

        r.send(data);
    },

    componentWillMount: function componentWillMount() {
        if (window.user) this.state.view = "EXIST";
    },


    _showNew: function _showNew() {
        this.setState({ view: "NEW" });
    },

    _showExist: function _showExist() {
        this.setState({ view: "EXIST" });
    },

    _showHelp: function _showHelp() {
        this.setState({ view: "HELP" });
    },

    _onExistChange: function _onExistChange(prop, value) {
        this.setState((0, _extends5.default)({}, this.state, {
            exist: (0, _extends5.default)({}, this.state.exist, (0, _defineProperty3.default)({}, prop, value))
        }));
    },

    _onChange: function _onChange(prop, value) {
        var _extends3;

        var account_id = this.state.data.account_id;
        if (prop === "account") {
            account_id = value.id;
        }

        if (prop == "account_id") {
            account_id = value;
        }
        this.setState((0, _extends5.default)({}, this.state, {
            error: null,
            data: (0, _extends5.default)({}, this.state.data, (_extends3 = {}, (0, _defineProperty3.default)(_extends3, prop, value), (0, _defineProperty3.default)(_extends3, "account_id", account_id), _extends3))
        }));
    },

    clearAccount: function clearAccount() {
        this.setState((0, _extends5.default)({}, this.state, {
            data: (0, _extends5.default)({}, this.state.data, {
                account: null,
                account_id: null
            })
        }));
    },

    _back: function _back() {
        document.location = "/";
    },

    render: function render() {
        if (this.state.completed) {
            if (this.state.view == "NEW") return React.createElement(Completed, null);
            if (this.state.view == "EXIST") return React.createElement(CompletedExisting, null);
        }

        var view = void 0,
            buttonLabel = "Submit registration",
            title = "New User Registration",
            content = "Please follow the steps below to register for an account.";

        if (this.state.view == "NEW") {
            view = React.createElement(UserDetails, { onChange: this._onChange, data: this.state.data });
        } else if (this.state.view == "EXIST") {
            view = React.createElement(ExistingUserRequest, { onChange: this._onChange, data: this.state.data });
            title = "Account Access Request";
            content = "Fill out the form below to request access to an account.";
            buttonLabel = "Request access";
        } else {
            view = React.createElement(Help, null);
            title = "Registration Help";
            content = "Guidance on registering an account with EWARS.";
        }

        var newClass = "ux-tab";
        if (this.state.view == "NEW") newClass += " active";
        var existClass = "ux-tab";
        if (this.state.view == "EXIST") existClass += " active";
        var helpClass = "ux-tab";
        if (this.state.view == "HELP") helpClass += " active";

        var backButton = void 0;
        if (window.user) {
            backButton = React.createElement(
                "div",
                { className: "btn-group" },
                React.createElement(ewars.d.Button, {
                    label: "Back to EWARS",
                    onClick: this._back })
            );
        }

        var hideSubmit = false;
        if (this.state.view == "HELP") hideSubmit = true;
        if (this.state.completed) hideSubmit = true;
        if (window.user) {
            if (window.user.system) hideSubmit = true;
        }

        var error = void 0;
        if (this.state.error) {
            error = React.createElement(
                "div",
                { className: "error" },
                React.createElement("i", { className: "fal fa-exclamation-triangle" }),
                "\xA0",
                this.state.error
            );
        }

        return React.createElement(
            "div",
            { className: "registration-wizard" },
            React.createElement(
                "h1",
                { className: "register" },
                "EWARS - ",
                window.account.name
            ),
            React.createElement(
                "h2",
                { className: "register" },
                title
            ),
            React.createElement(
                "p",
                { className: "register" },
                content
            ),
            React.createElement(
                "div",
                { className: "widget" },
                React.createElement(
                    "div",
                    { className: "widget-header" },
                    React.createElement(
                        "div",
                        { className: "ux-tabs" },
                        !window.user ? React.createElement(
                            "div",
                            { onClick: this._showNew, className: newClass },
                            "New Users"
                        ) : null,
                        React.createElement(
                            "div",
                            { onClick: this._showExist, className: existClass },
                            "Existing Users"
                        ),
                        React.createElement(
                            "div",
                            { onClick: this._showHelp, className: helpClass },
                            React.createElement("i", { className: "fal fa-question-circle" }),
                            "\xA0Help"
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { className: "body no-pad" },
                    error,
                    view
                ),
                !hideSubmit ? React.createElement(
                    "div",
                    { className: "widget-footer" },
                    backButton,
                    React.createElement(
                        "div",
                        { className: "btn-group pull-right" },
                        React.createElement(ewars.d.Button, {
                            label: buttonLabel,
                            color: "green",
                            icon: "fa-submit",
                            onClick: this._validate })
                    )
                ) : null
            ),
            React.createElement(
                "p",
                { className: "register" },
                "Already have an account? Sign in ",
                React.createElement(
                    "a",
                    { href: "/login" },
                    "here"
                )
            )
        );
    }
});

module.exports = Component;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 3 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(3);
var core = __webpack_require__(2);
var ctx = __webpack_require__(41);
var hide = __webpack_require__(8);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(12);
var IE8_DOM_DEFINE = __webpack_require__(42);
var toPrimitive = __webpack_require__(19);
var dP = Object.defineProperty;

exports.f = __webpack_require__(6) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(9)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 7 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(5);
var createDesc = __webpack_require__(15);
module.exports = __webpack_require__(6) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(45);
var defined = __webpack_require__(20);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(23)('wks');
var uid = __webpack_require__(16);
var Symbol = __webpack_require__(3).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(13);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(44);
var enumBugKeys = __webpack_require__(24);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 16 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 17 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(20);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(13);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 20 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 21 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(23)('keys');
var uid = __webpack_require__(16);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(3);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),
/* 24 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 25 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(66), __esModule: true };

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(67), __esModule: true };

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(40);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(49);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(12);
var dPs = __webpack_require__(74);
var enumBugKeys = __webpack_require__(24);
var IE_PROTO = __webpack_require__(22)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(43)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(75).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(5).f;
var has = __webpack_require__(7);
var TAG = __webpack_require__(11)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(11);


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(3);
var core = __webpack_require__(2);
var LIBRARY = __webpack_require__(31);
var wksExt = __webpack_require__(35);
var defineProperty = __webpack_require__(5).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(90);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(94);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(49);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var btnClass = {
    red: "btn-red",
    green: "btn-green",
    amber: "btn-amber"
};

var ButtonComponent = React.createClass({
    displayName: "ButtonComponent",

    _id: null,

    propTypes: {
        label: React.PropTypes.string,
        icon: React.PropTypes.string,
        type: React.PropTypes.string
    },

    getDefaultProps: function getDefaultProps() {
        return {
            enabled: true,
            processing: false,
            active: false
        };
    },

    getInitialState: function getInitialState() {
        return {
            enabled: true,
            processing: false,
            processingMessage: "Processing..."
        };
    },

    /**
     * Public method to set button to processing mode
     * @param message
     */
    setProcessing: function setProcessing(message) {
        this.state.processing = true;
        if (message != null && message != undefined) this.state.processingMessage = message;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method to set button to not processing mode
     */
    unsetProcessing: function unsetProcessing() {
        this.state.processing = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Public method used to disable the button
     */
    disable: function disable() {
        this.state.enabled = false;
        if (this.isMounted()) this.forceUpdate();
    },

    /**
     * Pubilc method used to enable button
     */
    enable: function enable() {
        this.state.enabled = true;
        if (this.isMounted()) this.forceUpdate();
    },

    shouldComponentUpdate: function shouldComponentUpdate() {
        return true;
    },

    _onClick: function _onClick(e) {
        if (!this.props.enabled) return;
        if (!this.state.enabled) return;
        if (this.state.processing) return;
        if (this.props.processing) return;

        e.preventDefault();
        e.stopPropagation();
        this.props.onClick(this.props.data ? this.props.data : null);
    },

    render: function render() {
        var colour;
        if (this.props.colour) colour = btnClass[this.props.colour];

        var className = "";
        if (colour) className += " " + colour;
        var iconClass;
        if (this.props.icon) iconClass = "fal " + this.props.icon;

        if (this.props.active) className += " active";

        var title;
        if (this.props.title) title = this.props.title;

        if (!this.state.enabled || !this.props.enabled) className += " disabled";

        var label;
        if (this.props.label) label = ewars.formatters.I18N_FORMATTER(this.props.label);

        if (this.state.processing || this.props.processing) {
            iconClass = "fal fa-circle-o-notch fa-spin";
            label = this.state.processingMessage || "Processing...";
        }

        var view;
        if (iconClass && label) {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                React.createElement("i", { className: iconClass }),
                "\xA0",
                label
            );
        } else if (iconClass && !label) {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                React.createElement("i", { className: iconClass })
            );
        } else {
            view = React.createElement(
                "button",
                { rel: "tooltip", title: title, className: className, onClick: this._onClick },
                label
            );
        }

        return view;
    }
});

module.exports = ButtonComponent;

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(40);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (obj, key, value) {
  if (key in obj) {
    (0, _defineProperty2.default)(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(55), __esModule: true };

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(57);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(6) && !__webpack_require__(9)(function () {
  return Object.defineProperty(__webpack_require__(43)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(13);
var document = __webpack_require__(3).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(7);
var toIObject = __webpack_require__(10);
var arrayIndexOf = __webpack_require__(63)(false);
var IE_PROTO = __webpack_require__(22)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(46);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 46 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(7);
var toObject = __webpack_require__(18);
var IE_PROTO = __webpack_require__(22)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(4);
var core = __webpack_require__(2);
var fails = __webpack_require__(9);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(69);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(80);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(31);
var $export = __webpack_require__(4);
var redefine = __webpack_require__(51);
var hide = __webpack_require__(8);
var has = __webpack_require__(7);
var Iterators = __webpack_require__(32);
var $iterCreate = __webpack_require__(73);
var setToStringTag = __webpack_require__(34);
var getPrototypeOf = __webpack_require__(47);
var ITERATOR = __webpack_require__(11)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(8);


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(44);
var hiddenKeys = __webpack_require__(24).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(17);
var createDesc = __webpack_require__(15);
var toIObject = __webpack_require__(10);
var toPrimitive = __webpack_require__(19);
var has = __webpack_require__(7);
var IE8_DOM_DEFINE = __webpack_require__(42);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(6) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(26);

var _stringify2 = _interopRequireDefault(_stringify);

var _getPrototypeOf = __webpack_require__(27);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(28);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(29);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(30);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(37);

var _inherits3 = _interopRequireDefault(_inherits2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Spinner = __webpack_require__(97);
var Button = __webpack_require__(38);

var Item = function (_React$Component) {
    (0, _inherits3.default)(Item, _React$Component);

    function Item(props) {
        (0, _classCallCheck3.default)(this, Item);
        return (0, _possibleConstructorReturn3.default)(this, (Item.__proto__ || (0, _getPrototypeOf2.default)(Item)).call(this, props));
    }

    (0, _createClass3.default)(Item, [{
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement(
                "div",
                { className: "block hoverable", onClick: function onClick() {
                        _this2.props.onClick(_this2.props.data);
                    } },
                React.createElement(
                    "div",
                    { className: "block-content" },
                    ewars.I18N(this.props.data.name)
                )
            );
        }
    }]);
    return Item;
}(React.Component);

function isNull(value) {
    if (value == null) return true;
    if (value == undefined) return true;
    if (value == "") return true;
    return false;
}

var Account = function (_React$Component2) {
    (0, _inherits3.default)(Account, _React$Component2);

    function Account(props) {
        (0, _classCallCheck3.default)(this, Account);

        var _this3 = (0, _possibleConstructorReturn3.default)(this, (Account.__proto__ || (0, _getPrototypeOf2.default)(Account)).call(this, props));

        _this3._query = function (resource, param) {
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/registration", true);
            oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            oReq.onreadystatechange = function () {
                var DONE = 4;
                var OK = 200;

                if (oReq.readyState == DONE) {
                    if (oReq.status == OK) {
                        this.setState({
                            data: JSON.parse(oReq.responseText)
                        });
                    } else {}
                }
            }.bind(_this3);

            oReq.send((0, _stringify2.default)({
                action: resource,
                param: isNull(param) ? null : param
            }));
        };

        _this3._onClick = function (item) {
            _this3.props.onSelect(item);
        };

        _this3._searchChange = function (e) {
            _this3.setState({
                search: e.target.value
            });

            if (_this3.state.search.length > 2) {
                _this3._query(_this3.props.resource, e.target.value);
            }
        };

        _this3._clSearch = function () {
            _this3.setState({ search: "" });
            _this3._query(_this3.props.resource, "");
        };

        _this3.state = {
            search: "",
            data: null
        };
        return _this3;
    }

    (0, _createClass3.default)(Account, [{
        key: "componentWillMount",
        value: function componentWillMount() {
            this._query(this.props.resource);
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(props) {
            this._query(this.props.resource);
        }
    }, {
        key: "render",
        value: function render() {

            var items = void 0;

            if (!this.state.data) items = React.createElement(Spinner, null);
            if (this.state.data) {
                items = this.state.data.map(function (item) {
                    return React.createElement(Item, { data: item, onClick: this._onClick });
                }.bind(this));
            }

            return React.createElement(
                ewars.d.Layout,
                null,
                React.createElement(
                    ewars.d.Row,
                    { height: "40" },
                    React.createElement(
                        ewars.d.Cell,
                        { style: { padding: 8 } },
                        React.createElement(
                            "div",
                            { className: "search-wrapper" },
                            React.createElement(
                                "div",
                                { className: "ide-row" },
                                React.createElement(
                                    "div",
                                    { className: "ide-col" },
                                    React.createElement("input", {
                                        placeholder: "Search...",
                                        onChange: this._searchChange,
                                        value: this.state.search,
                                        type: "text" })
                                ),
                                this.state.search != "" ? React.createElement(
                                    "div",
                                    { className: "ide-col", style: { maxWidth: 30, padding: 2 } },
                                    React.createElement(Button, { icon: "fa-times", onClick: this._clSearch })
                                ) : null
                            )
                        )
                    )
                ),
                React.createElement(
                    ewars.d.Row,
                    null,
                    React.createElement(
                        ewars.d.Cell,
                        null,
                        React.createElement(
                            "div",
                            { className: "ide-panel ide-panel-absolute ide-scroll" },
                            React.createElement(
                                "div",
                                { className: "block-tree" },
                                items
                            )
                        )
                    )
                )
            );
        }
    }]);
    return Account;
}(React.Component);

exports.default = Account;

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(56);
var $Object = __webpack_require__(2).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(4);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(6), 'Object', { defineProperty: __webpack_require__(5).f });


/***/ }),
/* 57 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _assign = __webpack_require__(59);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _assign2.default || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(60), __esModule: true };

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(61);
module.exports = __webpack_require__(2).Object.assign;


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(4);

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(62) });


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__(14);
var gOPS = __webpack_require__(25);
var pIE = __webpack_require__(17);
var toObject = __webpack_require__(18);
var IObject = __webpack_require__(45);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(9)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(10);
var toLength = __webpack_require__(64);
var toAbsoluteIndex = __webpack_require__(65);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(21);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(21);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(2);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(68);
module.exports = __webpack_require__(2).Object.getPrototypeOf;


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(18);
var $getPrototypeOf = __webpack_require__(47);

__webpack_require__(48)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(70), __esModule: true };

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(71);
__webpack_require__(76);
module.exports = __webpack_require__(35).f('iterator');


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(72)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(50)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(21);
var defined = __webpack_require__(20);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(33);
var descriptor = __webpack_require__(15);
var setToStringTag = __webpack_require__(34);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(8)(IteratorPrototype, __webpack_require__(11)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(5);
var anObject = __webpack_require__(12);
var getKeys = __webpack_require__(14);

module.exports = __webpack_require__(6) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(3).document;
module.exports = document && document.documentElement;


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(77);
var global = __webpack_require__(3);
var hide = __webpack_require__(8);
var Iterators = __webpack_require__(32);
var TO_STRING_TAG = __webpack_require__(11)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(78);
var step = __webpack_require__(79);
var Iterators = __webpack_require__(32);
var toIObject = __webpack_require__(10);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(50)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),
/* 78 */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),
/* 79 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(81), __esModule: true };

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(82);
__webpack_require__(87);
__webpack_require__(88);
__webpack_require__(89);
module.exports = __webpack_require__(2).Symbol;


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(3);
var has = __webpack_require__(7);
var DESCRIPTORS = __webpack_require__(6);
var $export = __webpack_require__(4);
var redefine = __webpack_require__(51);
var META = __webpack_require__(83).KEY;
var $fails = __webpack_require__(9);
var shared = __webpack_require__(23);
var setToStringTag = __webpack_require__(34);
var uid = __webpack_require__(16);
var wks = __webpack_require__(11);
var wksExt = __webpack_require__(35);
var wksDefine = __webpack_require__(36);
var enumKeys = __webpack_require__(84);
var isArray = __webpack_require__(85);
var anObject = __webpack_require__(12);
var toIObject = __webpack_require__(10);
var toPrimitive = __webpack_require__(19);
var createDesc = __webpack_require__(15);
var _create = __webpack_require__(33);
var gOPNExt = __webpack_require__(86);
var $GOPD = __webpack_require__(53);
var $DP = __webpack_require__(5);
var $keys = __webpack_require__(14);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(52).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(17).f = $propertyIsEnumerable;
  __webpack_require__(25).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(31)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    if (it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    replacer = args[1];
    if (typeof replacer == 'function') $replacer = replacer;
    if ($replacer || !isArray(replacer)) replacer = function (key, value) {
      if ($replacer) value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(8)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(16)('meta');
var isObject = __webpack_require__(13);
var has = __webpack_require__(7);
var setDesc = __webpack_require__(5).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(9)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(14);
var gOPS = __webpack_require__(25);
var pIE = __webpack_require__(17);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(46);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(10);
var gOPN = __webpack_require__(52).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),
/* 87 */
/***/ (function(module, exports) {



/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(36)('asyncIterator');


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(36)('observable');


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(91), __esModule: true };

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(92);
module.exports = __webpack_require__(2).Object.setPrototypeOf;


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(4);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(93).set });


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(13);
var anObject = __webpack_require__(12);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(41)(Function.call, __webpack_require__(53).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(95), __esModule: true };

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(96);
var $Object = __webpack_require__(2).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(4);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(33) });


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Spinner = React.createClass({
    displayName: "Spinner",

    render: function render() {
        return React.createElement(
            "div",
            { className: "spinner-wrap" },
            React.createElement(
                "div",
                { className: "spinner" },
                React.createElement("i", { className: "fal fa-spin fa-circle-o-notch" })
            )
        );
    }
});

module.exports = Spinner;

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _stringify = __webpack_require__(26);

var _stringify2 = _interopRequireDefault(_stringify);

var _defineProperty2 = __webpack_require__(39);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _keys = __webpack_require__(99);

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = __webpack_require__(27);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(28);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(29);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(30);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(37);

var _inherits3 = _interopRequireDefault(_inherits2);

var _React$createClass;

var _Selector = __webpack_require__(54);

var _Selector2 = _interopRequireDefault(_Selector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = __webpack_require__(38);
var Modal = __webpack_require__(102);

var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var USER_TYPES = [{
    icon: "fa-street-view",
    name: "Geographic Administrator",
    description: "Manage reports, users and tasks within a specific location.",
    code: "REGIONAL_ADMIN"
}, {
    icon: "fa-user",
    name: "Reporting User",
    description: "Submit and view reports.",
    code: "USER"
}];

var MODAL_BUTTONS = [{ label: "Cancel", icon: "fa-times", action: "CANCEL" }];

var MAPPER = {
    organization: 'org_id',
    account: 'account_id'
};

function isNull(value) {
    if (value == null) return true;
    if (value == undefined) return true;
    if (value == "") return true;
    return false;
}

var Completed = React.createClass({
    displayName: "Completed",

    render: function render() {
        return React.createElement(
            "div",
            { className: "widget" },
            React.createElement(
                "div",
                { className: "widget-header" },
                React.createElement(
                    "span",
                    null,
                    "Registration Submitted"
                )
            ),
            React.createElement(
                "div",
                { className: "body" },
                React.createElement(
                    "p",
                    null,
                    "A verification email has been sent to the email that you have provided, please check your inbox and click the link provided in the email to verify your ownership of the email address."
                ),
                React.createElement(
                    "p",
                    null,
                    "Once your email address has been verified an administrator will review your registration request and approve or reject the registration, you will receive an email with the outcome."
                ),
                React.createElement(
                    "p",
                    null,
                    "Your account can not be accessed until approved by an administrator."
                ),
                React.createElement(
                    "p",
                    null,
                    "Thank you for submitting your registration request."
                )
            )
        );
    }
});

var Error = function (_React$Component) {
    (0, _inherits3.default)(Error, _React$Component);

    function Error(props) {
        (0, _classCallCheck3.default)(this, Error);
        return (0, _possibleConstructorReturn3.default)(this, (Error.__proto__ || (0, _getPrototypeOf2.default)(Error)).call(this, props));
    }

    (0, _createClass3.default)(Error, [{
        key: "render",
        value: function render() {
            if (!this.props.data) return null;
            return React.createElement(
                "p",
                { className: "error" },
                React.createElement("i", { className: "fal fa-exclamation-triangle" }),
                " ",
                this.props.data
            );
        }
    }]);
    return Error;
}(React.Component);

var UserDetails = React.createClass((_React$createClass = {
    displayName: "UserDetails",

    getInitialState: function getInitialState() {
        return {
            err: {},
            done: false
        };
    },

    _validate: function _validate() {
        if (!this.props.data.name || this.props.data.name == "") {
            this._showError("Please provide a valid name");
            return false;
        }

        if (!re.test(this.props.data.email)) {
            this._showError("Please provide a valid email");
            return false;
        }

        if (!this.props.data.email || this.props.data.email == "") {
            this._showError("Please provide a valid email");
            return false;
        }
        if (this.props.data.email != this.props.data.confirm_email) {
            this._showError("Emails do not match");
            return false;
        }

        if (!this.props.data.password || this.props.data.password == "") {
            this._showError("Please provide a valid password");
            return false;
        }

        if (this.props.data.password != this.props.data.confirm_password) {
            this._showError("Passwords do not match");
            return false;
        }

        if (this.props.data.password.length < 8) {
            this._showError("Password must be a minimum of 8 characters and contain at least 1 number");
            return false;
        }

        if (!/\d/.test(this.props.data.password)) {
            this._showError("Passwords must be a minimum of 8 characters and contain at least 1 number");
            return false;
        }

        return true;
    },

    _showError: function _showError(message) {
        ewars.growl(message);
    },

    _onChange: function _onChange(e) {
        if ((0, _keys2.default)(this.state.err).length > 0) {
            this.setState({
                err: {}
            });
        }
        this.props.onChange(e.target.name, e.target.value);
    },

    _onModalAction: function _onModalAction(action) {
        if (action == "CLOSE") this.setState({ show: false, resource: null });
    },

    _onSelect: function _onSelect(item) {
        var resource = this.state.resource;
        this.setState((0, _defineProperty3.default)({
            resource: null,
            show: false
        }, resource, item));
        this.props.onChange(MAPPER[resource], item.id || item.uuid);
    }

}, (0, _defineProperty3.default)(_React$createClass, "_validate", function _validate() {

    var err = {};

    if (isNull(this.props.data.name)) err.name = "Please provide a valid name";
    if (isNull(this.props.data.email)) err.email = "Please provide a value email";
    if (!re.test(this.props.data.email)) err.email = "Please provide a valid email";
    if (isNull(this.props.data.confirm_email)) err.confirm_email = "Please confirm the email address provided";
    if (this.props.data.confirm_email != this.props.data.email && isNull(err.email)) err.email = "Emails do not match";

    if (isNull(this.props.data.password)) err.password = "Please provide a valid password";
    if (isNull(this.props.data.confirm_password)) err.confirm_password = "Please confirm your password";
    if (this.props.data.password.length < 6) err.password = "Password must be at least 6 characters long";
    if (this.props.data.password != this.props.data.confirm_password) err.password = "Passwords do no match";

    if (isNull(this.props.data.org_id)) err.org_id = "Please select an organization";

    if ((0, _keys2.default)(err).length > 0) {
        ewars.growl("There are errors in the registration form.");
        this.setState({
            err: err
        });
        return;
    }

    this._register();
}), (0, _defineProperty3.default)(_React$createClass, "_handleResult", function _handleResult(res) {
    if (res.err) {
        this.setState({
            err: res.data
        });
    } else {
        this.setState({
            done: true
        });
    }
}), (0, _defineProperty3.default)(_React$createClass, "_register", function _register() {
    var bl = new ewars.Blocker(null, "Processing Registration....");

    var oReq = new XMLHttpRequest();
    oReq.open("POST", "/register", true);
    oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    oReq.onreadystatechange = function () {
        var DONE = 4;
        var OK = 200;

        if (oReq.readyState == DONE) {
            if (oReq.status == OK) {
                bl.destroy();
                this._handleResult(JSON.parse(oReq.responseText));
            } else {}
        }
    }.bind(this);

    var data = {
        email: this.props.data.email.toLowerCase(),
        password: this.props.data.password,
        org_id: this.props.data.org_id,
        org_name: this.props.data.org_name
    };
    oReq.send((0, _stringify2.default)(data));
}), (0, _defineProperty3.default)(_React$createClass, "render", function render() {

    if (this.state.done) {
        return React.createElement(
            "div",
            { className: "widget" },
            React.createElement(
                "div",
                { className: "body no-pad" },
                React.createElement(Completed, null)
            )
        );
    }

    return React.createElement(
        "div",
        null,
        React.createElement(
            "div",
            { className: "article",
                style: { paddingRight: 150, paddingLeft: 30, paddingTop: 30, marginBottom: 40 } },
            React.createElement(
                "div",
                { className: "ide-layout" },
                React.createElement(
                    "div",
                    { className: "ide-row" },
                    React.createElement("div", { className: "ide-col", style: { maxWidth: 150 } }),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement(
                            "p",
                            null,
                            "Please provide us with the details below to start your registration."
                        )
                    )
                ),
                React.createElement(Error, { data: this.state.err.name }),
                React.createElement(
                    "div",
                    { className: "ide-row", style: { marginBottom: 8 } },
                    React.createElement(
                        "div",
                        { className: "ide-col",
                            style: {
                                maxWidth: 150,
                                fontWeight: "bold",
                                textAlign: "right",
                                paddingRight: 10,
                                paddingTop: 8
                            } },
                        "Your Name *"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement("input", { type: "text",
                            name: "name",
                            onChange: this._onChange,
                            value: this.props.data.name,
                            placeholder: "e.g. John Smith..." })
                    )
                ),
                React.createElement(Error, { data: this.state.err.email }),
                React.createElement(
                    "div",
                    { className: "ide-row", style: { marginBottom: 8 } },
                    React.createElement(
                        "div",
                        { className: "ide-col",
                            style: {
                                maxWidth: 150,
                                fontWeight: "bold",
                                textAlign: "right",
                                paddingRight: 10,
                                paddingTop: 8
                            } },
                        "Your Email *"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement("input", {
                            onChange: this._onChange,
                            type: "email",
                            value: this.props.data.email,
                            name: "email",
                            placeholder: "e.g. j.smith@ewars.ws..." })
                    )
                ),
                React.createElement(Error, { data: this.state.err.confirm_email }),
                React.createElement(
                    "div",
                    { className: "ide-row", style: { marginBottom: 8 } },
                    React.createElement(
                        "div",
                        { className: "ide-col",
                            style: {
                                maxWidth: 150,
                                fontWeight: "bold",
                                textAlign: "right",
                                paddingRight: 10,
                                paddingTop: 8
                            } },
                        "Confirm Email *"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement("input", {
                            onChange: this._onChange,
                            type: "email",
                            value: this.props.data.confirm_email,
                            name: "confirm_email",
                            placeholder: "e.g. j.smith@ewars.ws..." })
                    )
                ),
                React.createElement(Error, { data: this.state.err.password }),
                React.createElement(
                    "div",
                    { className: "ide-row", style: { marginBottom: 8 } },
                    React.createElement(
                        "div",
                        { className: "ide-col",
                            style: {
                                maxWidth: 150,
                                fontWeight: "bold",
                                textAlign: "right",
                                paddingRight: 10,
                                paddingTop: 8
                            } },
                        "Password *"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement("input", {
                            onChange: this._onChange,
                            type: "password",
                            value: this.props.data.password,
                            name: "password" })
                    )
                ),
                React.createElement(Error, { data: this.state.err.confirm_password }),
                React.createElement(
                    "div",
                    { className: "ide-row", style: { marginBottom: 8 } },
                    React.createElement(
                        "div",
                        { className: "ide-col",
                            style: {
                                maxWidth: 150,
                                fontWeight: "bold",
                                textAlign: "right",
                                paddingRight: 10,
                                paddingTop: 8
                            } },
                        "Confirm Password *"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement("input", {
                            onChange: this._onChange,
                            type: "password",
                            value: this.props.data.confirm_password,
                            name: "confirm_password" })
                    )
                ),
                React.createElement(Error, { data: this.state.err.org_id }),
                React.createElement(
                    "div",
                    { className: "ide-row", style: { marginBottom: 8 } },
                    React.createElement(
                        "div",
                        { className: "ide-col",
                            style: {
                                maxWidth: 150,
                                fontWeight: "bold",
                                textAlign: "right",
                                paddingRight: 10,
                                paddingTop: 8
                            } },
                        "Organization *"
                    ),
                    React.createElement(
                        "div",
                        { className: "ide-col" },
                        React.createElement(
                            "select",
                            { value: this.props.data.org_id,
                                name: "org_id",
                                onChange: this._onChange },
                            React.createElement(
                                "optgroup",
                                null,
                                React.createElement(
                                    "option",
                                    { value: "" },
                                    "None Selected"
                                ),
                                React.createElement(
                                    "option",
                                    { value: "OTHER" },
                                    "Other (Not listed)"
                                )
                            ),
                            React.createElement(
                                "optgroup",
                                null,
                                (window.orgs || []).map(function (item) {
                                    return React.createElement(
                                        "option",
                                        { value: item.uuid },
                                        item.name
                                    );
                                })
                            )
                        )
                    )
                ),
                this.props.data.org_id == "OTHER" ? React.createElement(
                    "div",
                    null,
                    React.createElement(Error, { data: this.state.err.org_id }),
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { marginBottom: 8 } },
                        React.createElement(
                            "div",
                            { className: "ide-col",
                                style: {
                                    maxWidth: 150,
                                    fontWeight: "bold",
                                    textAlign: "right",
                                    paddingRight: 10,
                                    paddingTop: 8
                                } },
                            "Other Org Name *"
                        ),
                        React.createElement(
                            "div",
                            { className: "ide-col" },
                            React.createElement("input", {
                                name: "org_name",
                                onChange: this._onChange,
                                value: this.props.data.org_name,
                                type: "text" })
                        )
                    )
                ) : null
            )
        )
    );
}), _React$createClass));

module.exports = UserDetails;

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(100), __esModule: true };

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(101);
module.exports = __webpack_require__(2).Object.keys;


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__(18);
var $keys = __webpack_require__(14);

__webpack_require__(48)('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var Button = __webpack_require__(38);
var SwitchField = __webpack_require__(103);

var ModalComponent = React.createClass({
    displayName: "ModalComponent",

    getDefaultProps: function getDefaultProps() {
        return {
            visible: false,
            wizard: false,
            height: null,
            light: false,
            toggle: null,
            footer: true
        };
    },

    componentWillMount: function componentWillMount() {},

    getInitialState: function getInitialState() {
        return {
            visible: false
        };
    },

    _onButtonAction: function _onButtonAction(data) {
        this.props.onAction(data.action);
    },

    _processButtons: function _processButtons() {
        var buttons = [];

        _.each(this.props.buttons, function (button) {
            var key = _.uniqueId("BUTTON_");
            buttons.push(React.createElement(ewars.d.Button, {
                key: key,
                data: button,
                enabled: button.enabled,
                colour: button.colour || null,
                icon: button.icon || null,
                onClick: button.onClick || this._onButtonAction,
                label: button.label }));
        }, this);

        return buttons;
    },

    _toggle: function _toggle() {
        this.props.onToggle(this.props.toggleState ? false : true);
    },

    _processToggle: function _processToggle() {
        return React.createElement(
            "div",
            { className: "modal-toggle-wrapper" },
            React.createElement(
                "table",
                null,
                React.createElement(
                    "tr",
                    null,
                    React.createElement(
                        "td",
                        null,
                        React.createElement(
                            "div",
                            { className: "modal-toggle-label" },
                            this.props.toggle.label
                        )
                    ),
                    React.createElement(
                        "td",
                        null,
                        React.createElement(
                            "div",
                            { className: "modal-switch-wrapper" },
                            React.createElement(SwitchField, {
                                name: "toggle",
                                onUpdate: this._toggle,
                                value: this.props.toggleState })
                        )
                    )
                )
            )
        );
    },

    render: function render() {
        var modalClass = "modal-wrapper";
        if (this.props.visible) modalClass += " show";
        if (!this.props.visible) modalClass += " hide";
        if (this.props.wizard) modalClass += " wizard";

        var buttons = this._processButtons();

        var hasButtons = false;
        if (buttons.length > 0) hasButtons = true;

        var style = {};
        if (this.props.width) {
            style.width = this.props.width + "px";
        }

        var bodyStyle = {};
        if (this.props.height) {
            bodyStyle.height = this.props.height;
        }
        var icon;
        if (this.props.icon) icon = "fal " + this.props.icon;

        var toggle;
        if (this.props.toggle) toggle = this._processToggle();

        return React.createElement(
            "div",
            { className: modalClass },
            React.createElement(
                "div",
                { className: "modal-content", style: style },
                React.createElement(
                    "div",
                    { className: "ide-layout" },
                    this.props.title ? React.createElement(
                        "div",
                        { className: "ide-row", style: { display: "block" } },
                        React.createElement(
                            "div",
                            { className: "modal-title" },
                            React.createElement(
                                "div",
                                null,
                                icon ? React.createElement(
                                    "div",
                                    { className: "modal-icon" },
                                    React.createElement("i", { className: icon })
                                ) : null,
                                React.createElement(
                                    "div",
                                    { className: "modal-title-content" },
                                    this.props.title
                                )
                            )
                        )
                    ) : null,
                    React.createElement(
                        "div",
                        { className: "ide-row", style: { display: "block", overflowY: "auto" } },
                        React.createElement(
                            "div",
                            { className: "modal-body", style: bodyStyle },
                            this.props.children
                        )
                    ),
                    this.props.footer ? React.createElement(
                        "div",
                        { className: "ide-row", style: { display: "block" } },
                        React.createElement(
                            "div",
                            { className: "modal-footer" },
                            toggle,
                            hasButtons ? React.createElement(
                                "div",
                                { className: "btn-group pull-right" },
                                buttons
                            ) : null
                        )
                    ) : null
                )
            )
        );
    }
});

module.exports = ModalComponent;

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var SwitchField = React.createClass({
    displayName: "SwitchField",

    getInitialState: function getInitialState() {
        return {};
    },

    onChange: function onChange() {
        var value = void 0;
        if ([true, "true"].indexOf(this.props.value) >= 0) value = true;
        if ([false, "false", -1, 0].indexOf(this.props.value) >= 0) value = false;

        console.log(value);
        if (this.props.config && this.props.config.path) {
            this.props.onUpdate(this.props.name, !value, this.props.config.path);
        } else {
            this.props.onUpdate(this.props.name, !value);
        }
    },

    render: function render() {
        var value = false;
        if (this.props.value) value = true;

        var name = ewars.utils.uniqueId("switch_");

        var className = "fal fa-toggle-off";
        if (value) className = "fal fa-toggle-on";

        var style = {};
        if (value) style.color = "green";

        return React.createElement(
            "div",
            { className: "ide-toggle", onClick: this.onChange },
            React.createElement("i", { style: style, className: className })
        );
    }
});

module.exports = SwitchField;

/***/ })
/******/ ]);
//# sourceMappingURL=register-dev.map?[contentHash]