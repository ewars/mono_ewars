window.__ENAME__ = "login"; window.__EVERSION__ = "1.0.3";
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Component = __webpack_require__(1);

ReactDOM.render(React.createElement(Component, null), document.getElementById('app'));

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _stringify = __webpack_require__(2);

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ERROR_CODES = {
    NO_AUTH: "Incorrect Email/Password",
    USER_DELETED: "Incorrect Email/Password",
    EMAIL_PENDING_VERIFICATION: "Account is pending email verification, please check your inbox.",
    USER_EXISTS_NO_ACCESS_ACCOUNTS: "You do not have access to any accounts at this time.",
    USER_EXISTS_NO_ACCESS_ACCOUNT: "You do not have access to this account at this time.",
    NO_ACCOUNT: "You do not have access to any accounts at this time",
    USER_ACCESS_REVOKED: "Your access to the account was revoked",
    USER_PENDING_APPROVAL: "You account access is currently pending approval",
    USER_NO_ACCESS_ACCOUNTS: "You currently do not have access to any accounts",
    UNK_ACCOUNT: "Unknown account"
};

var DEFAULT = 'Global EWARS is designed with the needs of frontline users in mind. We use technology and innovation to make disease control in emergencies easier and more effective.';

var style = {
    error: {
        position: "absolute",
        top: -50
    },
    arrowDown: {}
};

var Component = React.createClass({
    displayName: "Component",

    _isMobile: false,

    getInitialState: function getInitialState() {
        return {
            email: "",
            password: "",
            error: null,
            multiple: false
        };
    },

    componentWillMount: function componentWillMount() {
        var check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
        })(navigator.userAgent || navigator.vendor || window.opera);
        if (check) {
            // This is a mobile browser
            this._isMobile = true;
        }

        document.body.className = "app-public";
    },

    componentDidMount: function componentDidMount() {
        this.refs.emailInput.focus();
    },

    _onIndicatorChange: function _onIndicatorChange(event) {
        var curState = this.state;
        curState[event.target.name] = event.target.value;
        this.setState(curState);
    },

    _handleEnter: function _handleEnter(event) {
        if (event.keyCode == 13) {
            event.stopPropagation();
            var blocker = new ewars.Blocker(null, "Logging in...");
            this._doLogin(this.state.email, this.state.password, blocker);
        }
    },

    _onLogin: function _onLogin() {
        var blocker = new ewars.Blocker(null, "Logging in...");
        if (!this.state.password || this.state.password == "") {
            blocker.destroy();
            this.setState({
                error: "Please provide a valid email/password"
            });
            return;
        }

        if (!this.state.email || this.state.email == "") {
            blocker.destroy();
            this.setState({
                error: "Please provide a valid email/password"
            });
            return;
        }

        this._doLogin(this.state.email, this.state.password, blocker);
    },

    _doLogin: function _doLogin(email, password, blocker) {
        var _this = this;

        var self = this;
        var data = (0, _stringify2.default)({
            email: email,
            password: password
        });

        var r = new XMLHttpRequest();
        r.open("POST", "/login", true);
        r.setRequestHeader("Content-Type", "application/json");
        r.onreadystatechange = function () {
            blocker.destroy();
            if (r.readyState != 4 && r.status != 200) {
                _this.setState({
                    error: ERROR_CODES[3]
                });
            } else {
                if (r.readyState == 4 && r.status == 200) {
                    var resp = JSON.parse(r.responseText);

                    if (resp.code) {
                        switch (resp.code) {
                            case "MULTI_ACCOUNT":
                                _this.setState({
                                    multiple: true,
                                    accounts: resp.accounts
                                });
                                break;
                            default:
                                _this.setState({ error: ERROR_CODES[resp.code] });
                        }
                    } else {
                        if (resp.cortex) {
                            document.location = "/cortex";
                        } else {
                            sessionStorage.setItem("token", resp.token);
                            sessionStorage.setItem("user", (0, _stringify2.default)(resp.user));
                            document.location = "/";
                        }
                    }
                }
            }
        };
        r.send(data);
    },

    _clearError: function _clearError() {
        this.setState({
            error: null
        });
    },

    _selectAccount: function _selectAccount(aid) {
        var _this2 = this;

        var data = (0, _stringify2.default)({
            email: this.state.email,
            password: this.state.password,
            account_id: aid
        });

        var bl = new ewars.Blocker(null, "Authenticating...");
        var r = new XMLHttpRequest();
        r.open("POST", "/login");
        r.onreadystatechange = function () {
            bl.destroy();
            if (r.readyState != 4 && r.status != 200) {
                _this2.setState({
                    error: ERROR_CODES[3]
                });
            } else {
                if (r.readyState == 4 && r.status == 200) {
                    var resp = JSON.parse(r.responseText);

                    if (resp.err) {
                        _this2.setState({ error: ERROR_CODES[resp.code] });
                    } else {
                        document.location = "/";
                    }
                }
            }
        };
        r.send(data);
    },

    render: function render() {
        var _this3 = this;

        if (this.state.multiple) {
            return React.createElement(
                "div",
                { className: "registration-wizard" },
                React.createElement(
                    "h1",
                    { className: "register" },
                    "Accounts"
                ),
                React.createElement(
                    "p",
                    { className: "register" },
                    "Please select the account you would like to access."
                ),
                React.createElement(
                    "div",
                    { className: "widget" },
                    React.createElement(
                        "div",
                        { className: "body no-pad" },
                        React.createElement(
                            "div",
                            { className: "block-tree", style: { position: "relative" } },
                            this.state.accounts.map(function (acc) {
                                return React.createElement(
                                    "div",
                                    { className: "block hoverable", onClick: function onClick() {
                                            _this3._selectAccount(acc[0]);
                                        } },
                                    React.createElement(
                                        "div",
                                        { className: "block-content" },
                                        ewars.I18N(acc[1]),
                                        " ",
                                        acc[2]
                                    )
                                );
                            })
                        )
                    )
                )
            );
        }

        if (this._isMobile) {
            return React.createElement(
                "div",
                { className: "login-wrapper" },
                React.createElement(
                    "div",
                    { className: "mob" },
                    React.createElement(
                        "h3",
                        null,
                        "Mobile Unsupported"
                    ),
                    React.createElement(
                        "p",
                        null,
                        "Unfortunately, at this time, mobile browsers are not supported by the EWARS platform. A mobile application is under active development and will be released for various platforms (Android, iOS, Windows Phone) in the coming months."
                    ),
                    React.createElement(
                        "p",
                        null,
                        "In the meantime, please access the EWARS application through either a desktop browser, or the desktop client."
                    ),
                    React.createElement(
                        "p",
                        null,
                        "If you have any questions or concerns, please contact ",
                        React.createElement(
                            "a",
                            { href: "mailto:support@ewars.ws" },
                            "support@ewars.ws"
                        )
                    )
                )
            );
        }

        var accName = "Global";
        if (window.account) accName = window.account.name;

        var year = new Date().getFullYear();

        return React.createElement(
            "div",
            { className: "login-wrapper" },
            React.createElement(
                "div",
                { className: "login-container" },
                React.createElement(
                    "div",
                    { className: "login-controls" },
                    React.createElement(
                        "div",
                        { className: "login-control" },
                        React.createElement(
                            "h3",
                            null,
                            "Existing User? Sign in"
                        ),
                        this.state.error ? React.createElement(
                            "div",
                            { className: "error", style: style.error },
                            React.createElement("i", { className: "fal fa-warning" }),
                            "\xA0",
                            this.state.error
                        ) : null,
                        React.createElement(
                            "label",
                            { htmlFor: "Email" },
                            "Email"
                        ),
                        React.createElement("input", { type: "text", ref: "emailInput", name: "email", onFocus: this._clearError,
                            className: "form-control", onChange: this._onIndicatorChange, value: this.state.email }),
                        React.createElement("br", null),
                        React.createElement(
                            "label",
                            { htmlFor: "password" },
                            "Password"
                        ),
                        React.createElement("input", { type: "password", name: "password", onFocus: this._clearError,
                            className: "form-control",
                            onChange: this._onIndicatorChange,
                            value: this.state.password, onKeyDown: this._handleEnter }),
                        React.createElement("br", null),
                        React.createElement(
                            "div",
                            { className: "button-wrapper" },
                            React.createElement(
                                "div",
                                { className: "button",
                                    label: "Sign In",
                                    ref: "",
                                    onClick: this._onLogin },
                                "Login"
                            )
                        ),
                        React.createElement(
                            "a",
                            { href: "/recovery", className: "forgot-pass-link" },
                            "Forgot Password"
                        ),
                        React.createElement(
                            "a",
                            { href: "/register", className: "login-btn" },
                            "Create an Account"
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { className: "instance-details" },
                    React.createElement(
                        "div",
                        { className: "details" },
                        React.createElement(
                            "div",
                            { className: "access-details" },
                            window.account.account_flag && window.account.account_flag != "" ? React.createElement(
                                "div",
                                { className: "logo" },
                                React.createElement("img", { width: "100%", src: window.account.account_flag, alt: "" })
                            ) : null,
                            React.createElement(
                                "div",
                                { className: "names" },
                                React.createElement(
                                    "div",
                                    { className: "acc-name" },
                                    accName
                                ),
                                React.createElement(
                                    "div",
                                    { className: "instance-name" },
                                    window.config.APP_NAME || "EWARS"
                                )
                            ),
                            React.createElement("div", { className: "clearer" })
                        ),
                        window.account.name ? React.createElement(
                            "p",
                            null,
                            window.account.description
                        ) : React.createElement(
                            "p",
                            null,
                            DEFAULT
                        ),
                        React.createElement("div", { className: "clearer" }),
                        window.account.account_logo && window.account.account_logo != "" ? React.createElement("img", { src: window.account.account_logo, alt: "" }) : null
                    ),
                    React.createElement(
                        "div",
                        { className: "copyright" },
                        "All Content Copyright WHO ",
                        year,
                        ". All Rights Reserved. Powered by ",
                        React.createElement(
                            "a",
                            { href: "http://ewars-project.org", target: "_blank" },
                            "ewars"
                        )
                    )
                )
            )
        );
    }
});

module.exports = Component;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(3), __esModule: true };

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(4);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),
/* 4 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ })
/******/ ]);
//# sourceMappingURL=login-dev.map?[contentHash]