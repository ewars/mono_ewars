# 4.1.4

- EIDs and eid_prefix static for alarms/alerts
- EIDs and eid_prefix static for forms/submissions
- Fixed copyright date on login page
- Updated cortex to disallow editing of existing accounts
- Table widget hiding rows with no data where value <= 0
- Fixes an issue with duplicating columns in table widget editor
- Changes to the way that user guidance is displayed for system user
  property field in new user creation

## Internal

- ButtonGroup component not support help_context property to map
  selectable values to help strings to display under selection