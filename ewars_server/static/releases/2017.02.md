# v2017.02

## v2017.02.9

- Fixed an issue with loading overlay not removing after location deletion
- New Date Range Selector for widgets
- New date specification format
- New map range/threshold specification component

## v2017.02.8

- Fixes an issue with geometry editor in locations; regression
  potentially from update to MapboxDraw lib

## v2017.02.7

- Added ability to merge locations
- Added ability to edit JSON sources for Raw, Category and Series
  widgets
- Added new shader replacement UI component for modals
- Modified dashboard assignment request to use new shade
- Fixed location selector highlighting in admin
- Fixed SAVE button for new location creation
- Locations now properly reloads tree when changes are made
- Fixed issue with blocker rendering underneath shade
- Fixed x-index issue with growl rendering under modals when a growl is
  emitted (widget editor)
- Fixed an issue where assignments widget request button would render
  over higher layers

## v2017.02.6

- #251 Fixes an issue where verification stage is still editable despite
  an alert being auto-discarded

## v2017.02.5

- Fixes a missing alert date in alert emails
- Fixes landing page for release ver. info page displaying none

## v2017.02.4

- Fixes and issue with ALERTS_DISCARDED dimension for system indicators
- Updates list of dimensions for alerts system indicator

## v2017.02.3

- Fixes an issue in IE10 with rendering general layout (incorrect
  platform-specific css rule)
- Regional Administrators can now submit amendments without approval
- Fixes an issue with header toolbar in analysis widget editor

## v2017.02.2

- Fixes an issue with location folder in Report Manager for Geographic
  Administrators being forced back to their assigned location

## v2017.02.1

- Fixes an issue for GEO_ADMINS where forms in reporting were being
  rendered as read-only for creation
