# v2017.01

## v2017.01.2

- Fixed an issue with mogrifying UUID of alarm_id into SQL query for alerts indicators
- Updated footer copyright to 2017

## v2017.01.1

- Fixed an issue with the password recovery page not routing correctly
- Updated Alerts -> ALERTS_VERIFIED indicator dimension to not include AUTODISCARDED alerts in aggregation

