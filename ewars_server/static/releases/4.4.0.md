# 4.4.0

- BUGFIX: Fixes an issue with text overflow in the Alert -> Risk Characterization
- FEATURE: Adds ability to constrain alarm evaluation to a single record
- BUGFIX: Fixes an issue with saving POINT geometry in locations not interpreting fields correctly
- BUGFIX: Fixes an issue with nested complexes in form logic not allowing selection of the source field

