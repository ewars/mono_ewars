const EN = {
    SAVE_CHANGES: "Save Change(s)",
    CANCEL: "Cancel",
    CLOSE: "Close",
    EXIT: "Exit",
    REPORTING: "Reporting",
    DRAFTS: "Drafts",
    ALERTS: "Alerts",
    ALARMS: "Alarms",
    USER: "User",
    TASKS: "Tasks"
};

export default EN;
