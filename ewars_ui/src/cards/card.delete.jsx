import React from 'react';

class CardDelete extends React.Component {
    static defaultProps = {
        title: "Delete item?",
        content: "Are you sure you want to delete this item?",
        confirmation: false,
        confirmation_value: null,
        reason: false,
        buttons: [
            "DELETE",
            "CANCEL"
        ],
        callback: () => {}
    };

    constructor(props) {
        super(props);

        this.state = {
            confirmation_value: "",
            reason: ""
        }
    }

    render() {
        return (
            <div className="user-card-overlay"></div>
        )
    }

}

export default CardDelete;