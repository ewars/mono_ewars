import React from "react";

class AlertsPlaceholder extends React.Component {
    render() {
        return (
            <div className="column placeholder bgd-ps-neutral" style={{position: "relative", height: "100%"}}>
                <div className="placeholder-icon">
                    <i className="fal fa-bell"></i>
                </div>
                <div className="placeholder-title">
                    No active alerts
                </div>
                <div className="placeholder-text">
                    <p>Looks like there aren't any open alerts at this time, check back from time to time. We'll also notify you when an alert becomes available in your area.</p>
                </div>
            </div>
        )
    }
}

export default AlertsPlaceholder;
