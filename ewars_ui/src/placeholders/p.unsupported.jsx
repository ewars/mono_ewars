import React from "react";

class Unsupported extends React.Component {
    render() {
        return (
            <div className="column placeholder" style={{position: "relative"}}>
                <div className="placeholder-icon">
                    <i className="fal fa-exclamation-circle"></i>
                </div>
                <div className="placeholder-title">
                    Feature Unsupported
                </div>
                <div className="placeholder-text">
                    <p>This feature is not currently available for your version of EWARS. It will become available in a later release of the application.</p>
                </div>
            </div>
        )
    }
}

export default Unsupported;