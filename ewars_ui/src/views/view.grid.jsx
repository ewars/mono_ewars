import React from "react";

import DataGrid from "../controls/datagrid/c.datagrid.jsx";

import FORMS from "../constants/grids/grid.forms";
import DASHBOARDS from "../constants/grids/grid.dashboards";
import DOCUMENTS from "../constants/grids/grid.documents";
import HOOKS from "../constants/grids/grid.hooks";
import EXTERNAL from "../constants/grids/grid.external";
import TEAMS from "../constants/grids/grid.teams";
import PERIODS from "../constants/grids/grid.periods";
import USERS from "../constants/grids/grid.users";
import ALARMS from "../constants/grids/grid.alarms";
import ROLES from "../constants/grids/grid.roles";
import DEVICES from "../constants/grids/grid.devices";
import ASSIGNMENTS from "../constants/grids/grid.assignments";
import ORGANIZATIONS from "../constants/grids/grid.organizations";
import GATEWAYS from "../constants/grids/grid.gateways";
import SHARED from "../constants/grids/grid.shared";
import IMPORTS from "../constants/grids/grid.imports";
import DRAFTS from "../constants/grids/grid.drafts";

const GRIDS = {
    IMPORTS,
    DRAFTS,
    ASSIGNMENTS,
    SHARED,
    GATEWAYS,
    DEVICES,
    ROLES,
    ALARMS,
    USERS,
    FORMS,
    DASHBOARDS,
    DOCUMENTS,
    HOOKS,
    EXTERNAL,
    TEAMS,
    PERIODS,
    ORGANIZATIONS
};

class ViewGrid extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props);
        let gridId = this.props.config.grid;
        let GRID = GRIDS[gridId] || null;

        return (
            <DataGrid
                id={this.props.config.grid}
                grid_id={this.props.config.grid}
                search={GRID.search}
                title={GRID.title || ""}
                baseFiltering={GRID.baseFiltering}
                baseOrdering={GRID.baseOrdering}
                queryFn={GRID.query}
                actions={GRID.actions}
                rowActions={GRID.rowActions}
                pinnedRows={GRID.pinnedRows || []}
                onAction={GRID.action}
                grid={[GRID.columns]}/>
        )
    }
}

export default ViewGrid;
