import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import DraftsPlaceholder from "../../placeholders/p.drafts.jsx";

class DraftRecord extends React.Component {
    _action = (action) => {
        console.log(action);
    };

    render() {
        return (
            <div className="card">
                <div className="column">
                    <strong>{this.props.data.form_name.en || this.props.data.form_name}</strong>
                </div>
                <ActionGroup
                    actions={DRAFT_ACTIONS}
                    onAction={this._action}/>
            </div>
        )
    }
}

class ViewOverviewDrafts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.drafts', null, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_DRAFTS", err);
        })
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                </div>
            )
        }

        let items = [];

        if (this.state.data.length <= 0) {
            items = (
                <DraftsPlaceholder/>
            )
        } else {
            items = this.state.data.map(item => {
                return <DraftItem data={item}/>;
            })
        }

        return (
            <div className="column">
                {items}
            </div>
        )
    }
}

export default ViewOverviewDrafts;
