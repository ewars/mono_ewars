import React from "react";
import Moment from "moment";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import DateUtils from "../../utils/date_utils";

//import {remote} from "electron";

//const {Menu, MenuItem} = remote;

import AlertsPlaceholder from "../../placeholders/p.alerts.jsx";

const ACTIONS = [
    ["fa-eye", "BROWSE", "BROWSE_ALERTS"]
];

const STAGES = {
    OUTCOME: "Outcome",
    VERIFICATION: "Verification",
    RISK_CHAR: "Risk Characterization",
    RISK_ASSESS: "Risk Assessment"
};

class LocationAlert extends React.Component {
    constructor(props) {
        super(props);
    }

    _showAlert = () => {
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                type: "ALERT",
                name: __(this.props.data.alarm_name),
                alert_id: this.props.data.uuid
            }
        }));
    };


    render() {
        return (
            <div className="card-boxed">
                <div className="inner-card">
                    <div className="row block padded-3 flex-2">
                        <div className="dash-handle">
                            <div className="inner">
                                <div className="icon">
                                    <i className="fal fa-map-marker"></i>
                                </div>
                                <div className="label">
                                    <strong>{this.props.data.location_name.en || this.props.data.location_name}</strong>
                                </div>
                            </div>
                        </div>
                        <div className="dash-handle">
                            <div className="inner">
                                <div className="icon">
                                    <i className="fal fa-calendar"></i>
                                </div>
                                <div className="label">
                                    {Moment.utc(this.props.data.alert_date || null).format("YYYY-MM-DD")}
                                </div>
                            </div>
                        </div>
                        <div className="dash-handle">
                            <div className="inner">
                                <div className="icon">
                                    <i className="fal fa-bell"></i>
                                </div>
                                <div className="label">{STAGES[this.props.data.stage]}</div>
                            </div>
                        </div>
                        {this.props.data.eid ?
                            <div className="dash-handle">
                                <div className="inner">
                                    <div className="icon">
                                        <i className="fal fa-tag"></i>
                                    </div>
                                    <div className="label">
                                        {this.props.data.eid}
                                    </div>
                                </div>
                            </div>

                            : null}
                    </div>
                    <div className="row block card-button" onClick={this._showAlert}>
                        <i className="fal fa-eye"></i>&nbsp;View
                    </div>
                </div>
            </div>
        )
    }
}

class AlarmLocationOverview extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case "CREATE":
                window.state.addTab("RECORD", {
                    form_id: this.props.data.form_id,
                    location_id: this.props.data.location_id || null,
                    name: this.props.data.form_name.en || this.props.data.form_name
                });
                break;
            case "BROWSE":
                window.state.addTab("RECORDS", {
                    form_id: this.props.data.form_id,
                    name: this.props.data.form_name.en || this.props.data.form_name
                });
                break;
            default:
                break;
        }
    };

    render() {
        let location_name;
        if (this.props.data.location_name) {
            location_name = this.props.data.location_name.en || this.props.data.location_name;
        }


        return (
            <div className="report-overview">
                <div className="column">
                    <div className="row report-overview-inner">
                        <div className="column report-overview-info">
                            <div className="row block">
                                <span><strong>{location_name}</strong></span>

                            </div>
                        </div>
                        <ActionGroup
                            actions={ACTIONS}
                            vertical={true}
                            naked={true}
                            onAction={this._action}/>
                    </div>
                </div>
            </div>
        )
    }
}

const FILTERS_STYLE = {
    padding: "8px 16px"
};

const NO_PAD = {
    padding: "0"
}

class ViewOverviewAlerts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            location: null,
            alarm: null,
            stage: null
        }
    }

    componentDidMount() {
        window.sonomaClient.tx("com.sonoma.self.alerts", null, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_ALERTS", err);
        })
    }

    _showStageMenu = (e) => {
        e.preventDefault();
        e.stopPropagation();

        let menu = new window.Menu();
        menu.append(new window.MenuItem({
            label: "All stages",
            click: () => {
                this.setState({stage: null});
            }
        }));

        menu.append(new window.MenuItem({
            type: "separator"
        }));

        ["VERIFICATION", "RISK_ASSESS", "RISK_CHAR", "OUTCOME"].forEach(item => {
            menu.append(new window.MenuItem({
                label: __(item),
                click: () => {
                    this.setState({stage: item})
                }
            }));
        });

        menu.popup();
    };


    _showLocationFilter = (e) => {
        e.preventDefault();
        e.stopPropagation();

        let menu = new window.Menu();

        menu.append(new window.MenuItem({
            label: "All locations",
            click: () => {
                this.setState({location: null});
            }
        }));

        menu.append(new window.MenuItem({
            type: "separator"
        }));

        let locations_dict = {};

        this.state.data.forEach(item => {
            locations_dict[item.lid] = item.location_name.en || item.location_name;
        });

        for (let i in locations_dict) {

            menu.append(new window.MenuItem({
                label: locations_dict[i],
                click: () => {
                    this.setState({
                        location: [i, locations_dict[i]]
                    })
                }
            }))
        }

        menu.popup();
    };

    _showFormSelector = (e) => {
        e.preventDefault();
        e.stopPropagation();

        let menu = new window.Menu();

        menu.append(new window.MenuItem({
            label: "All alarms",
            click: () => {
                this.setState({alarm: null})
            }
        }));

        menu.append(new window.MenuItem({
            type: "separator"
        }));

        let alarms = {};
        this.state.data.forEach(item => {
            alarms[item.alid] = item.alarm_name.en || item.alarm_name;
        })

        for (let i in alarms) {
            menu.append(new window.MenuItem({
                label: alarms[i],
                click: () => {
                    this.setState({
                        alarm: [i, alarms[i]]
                    })
                }
            }))
        }

        menu.popup();
    };

    render() {
        let data = this.state.data || [];

        if (this.state.alarm) {
            data = data.filter(item => {
                return item.alid == this.state.alarm[0];
            })
        }

        if (this.state.location) {
            data = data.filter(item => {
                return item.lid == this.state.location[0];
            })
        }

        if (this.state.stage) {
            data = data.filter(item => {
                return item.stage == this.state.stage;
            });
        }

        let alarms = {};

        data.forEach(item => {
            if (alarms[item.alid]) {
                alarms[item.alid].push(item);
            } else {
                alarms[item.alid] = [item];
            }
        });

        let items = [];

        for (let i in alarms) {
            items.push(
                <div className="form-section">
                    {alarms[i][0].alarm_name.en || alarms[i][0].alarm_name}
                </div>
            );

            let alerts = [];
            alarms[i].forEach(item => {
                alerts.push(<LocationAlert data={item}/>);
            })

            items.push(
                <div className="row card-box-wrapper padded-16">{alerts}</div>
            )


        }

        if (items.length <= 0) {
            items = <AlertsPlaceholder/>;
        }

        let locationFilterLabel = "All locations";
        if (this.state.location) locationFilterLabel = this.state.location[1];
        let formFilterLabel = "All alarms";
        if (this.state.alarm) formFilterLabel = this.state.alarm[1];
        let alertStage = "All stages";
        if (this.state.stage) alertStage = __(this.state.stage);

        return (
            <div className="row bgd-ps-neutral" style={NO_PAD}>
                <div className="row block block-overflow" style={NO_PAD}>
                    <div className="block"
                         style={FILTERS_STYLE}>
                        <a href="#"
                           onClick={this._showLocationFilter}
                           className="quick-menu">
                            <i className="fal fa-map-marker"></i>&nbsp;{locationFilterLabel}&nbsp;<i
                            className="fal fa-caret-down"></i>
                        </a>
                        <a href="#"
                           onClick={this._showFormSelector}
                           className="quick-menu">
                            <i className="fal fa-clipboard"></i>&nbsp;{formFilterLabel}&nbsp;<i
                            className="fal fa-caret-down"></i>
                    </a>
                    <a href="#" onClick={this._showStageMenu} className="quick-menu">
                        <i className="fal fa-code-branch"></i>&nbsp;{alertStage}&nbsp;
                        <i className="fal fa-caret-down"></i>
                    </a>

                    </div>
                    {items}
                </div>
            </div>
        )
    }
}

export default ViewOverviewAlerts;
