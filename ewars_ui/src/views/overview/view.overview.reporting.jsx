import React from "react";
import Moment from "moment";

import DateUtils from "../../utils/date_utils";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import OverviewIndicator from "../../controls/c.overview.indicator.jsx";

//import VizSubmissions from "../../controls/visualisations/viz.submissions";

import Highcharts from "highcharts";

const ACTIONS = [
    ['fa-plus', "CREATE", "CREATE_RECORD"],
    ['fa-table', 'BROWSE', "BROWSE_RECORDS"]
];

const _convertToUtc = (value) => {
    let splat = value.split("-");
    return Date.UTC(splat[0], splat[1], splat[2]);
}


class FormStatsItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="over-alarm-ind">
                <div className="over-alarm-ind-label">
                    {__(this.props.label)}
                </div>
                <div className="over-alarm-ind-count">
                    {this.props.value}
                </div>
            </div>
        )
    }
}

class ReportingOverview extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            data: {
                total_submissions: null,
                max: null,
                results: {},
                overdue: 0,
                missing: 0
            }
        };
    }

    componentDidMount() {
    }

    _action = (action) => {
        switch (action) {
            case "CREATE":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "RECORD",
                        form_id: this.props.data.uuid,
                        location_id: this.props.data.location_id || null,
                        icon: "fa-clipboard",
                        name: __(this.props.data.name)
                    }
                }));
                break;
            case "BROWSE":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_RECORDS",
                        form_id: this.props.data.uuid,
                        icon: "fa-table",
                        name: __(this.props.data.name)
                    }
                }));
                break;
            default:
                break;
        }
    };

    render() {
        console.log(this.props);
        return (
            <div className="report-overview">
                <div className="column">
                    <div className="row report-overview-inner">
                        <div className="column report-overview-info">
                            <div className="row block">
                                <span>{__(this.props.data.name)}</span>
                            </div>

                        </div>
                        <ActionGroup
                            actions={ACTIONS}
                            vertical={true}
                            onAction={this._action}/>
                    </div>
                </div>
            </div>
        )
    }
}


class ViewOverviewReporting extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        };

    }

    componentDidMount() {
        window.sonomaClient.tx("com.sonoma.forms.active", null, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.error("ERROR_LOADING_FORMS", err);
        })
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                </div>
            )
        }
        let days = DateUtils.getYearDates();
        return (
            <div className="row block block-overflow bgd-ps-neutral">
                {this.state.data.map(item => {
                    return (
                        <ReportingOverview data={item} days={days}/>
                    )
                })}
            </div>
        )
    }
}

export default ViewOverviewReporting;
