import React from "react";
import Moment from "moment";

import AppContext from "../../context_provider";

import DateUtils from "../../utils/date_utils";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import OverviewIndicator from "../../controls/c.overview.indicator.jsx";

//import {remote} from "electron";

//,const {Menu, MenuItem} = remote;

const ACTIONS = [
    ['fa-table', 'BROWSE', "BROWSE_RECORDS"],
    '-',
    ['fa-ellipsis-v', 'MORE', 'MORE']
];

const ACTIONS_NON_INTERVAL = [
    ['fa-plus', 'CREATE', 'CREATE_RECORD'],
    ['fa-table', 'BROWSE', "BROWSE_RECORDS"]
];

const MONTHS = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
];

const PAD_INNER = {
    paddingLeft: "5px",
    paddingRight: "5px",
    width: "auto",
    maxWidth: "none"
};

const TOOL_HEIGHT = {
    maxHeight: "26px",
    minHeight: "26px"
};


class YearControl extends React.Component {
    constructor(props) {
        super(props);
    }

    _prev = () => {
        this.props.onChange(this.props.value - 1);
    };

    _next = () => {
        this.props.onChange(this.props.value + 1);
    };

    render() {
        return (
            <div>
                <div className="action-group">
                    <div className="action-item" onClick={this._prev}>
                        <i className="fal fa-chevron-left"></i>
                    </div>
                    <div className="action-item" style={PAD_INNER}>
                        {this.props.value}
                    </div>
                    <div className="action-item" onClick={this._next}>
                        <i className="fal fa-chevron-right"></i>
                    </div>
                </div>
            </div>
        )
    }
}

class YearMonthControl extends React.Component {
    constructor(props) {
        super(props);
    }

    _prev = () => {
        let curMonth = this.props.value[1],
            newMonth,
            curYear = this.props.value[0],
            newYear;

        if (curMonth == 1) {
            newMonth = 12;
            newYear = curYear - 1;
        } else {
            newMonth = curMonth - 1;
            newYear = curYear;
        }
        this.props.onChange([newYear, newMonth]);
    };

    _next = () => {
        let curMonth = this.props.value[1],
            newMonth,
            curYear = this.props.value[0],
            newYear;

        if (curMonth == 12) {
            newMonth = 1;
            newYear = curYear + 1;
        } else {
            newMonth = curMonth + 1;
            newYear = curYear;
        }
        this.props.onChange([newYear, newMonth]);
    };

    render() {
        return (
            <div>
                <div className="action-group">
                    <div className="action-item" onClick={this._prev}>
                        <i className="fal fa-chevron-left"></i>
                    </div>

                    <div className="action-item" style={{...PAD_INNER, width: "86px"}}>
                        {this.props.value[0]} - {MONTHS[this.props.value[1]]}
                    </div>
                    <div className="action-item" onClick={this._next}>
                        <i className="fal fa-chevron-right"></i>
                    </div>
                </div>
            </div>
        )
    }
}

class DateItem extends React.Component {
    static defaultProps = {
        interval: "DAY",
        point: null,
        exists: [],
        lid: null,
        fid: null,
        record_map: {}
    };

    constructor(props) {
        super(props);

        let submitted;
        if (this.props.record_map[this.props.point.format("YYYY-MM-DD")]) {
            submitted = this.props.record_map[this.props.point.format("YYYY-MM-DD")][1];
            submitted = Moment.utc(submitted);
        }

        this.label = "n/a";
        switch (this.props.interval) {
            case "DAY":
                this.label = this.props.point.format("DD");
                break;
            case "WEEK":
                this.label = this.props.point.format("[W]WW");
                break;
            default:
                this.label = this.props.point.format("DD");
                break;
        }


        this.className = "grid-cell ignore";
        this._label = "fal fa-plus";

        if (this.props.point.isAfter(this.props.today, 'd')) {
            this.className = "grid-cell ignore";
            this._label = "fal fa-plus";
        } else {
            if (this.props.point.isBefore(this.props.end_date, 'd') && this.props.point.isAfter(this.props.start_date, 'd')) {
                // the date is within the reporting range
                if ((this.props.exists || []).indexOf(this.props.point.format("YYYY-MM-DD")) >= 0) {
                    this.className = "grid-cell good";
                    this._label = "fal fa-eye";
                } else {
                    // id overdue
                    this.className = "grid-cell bad";
                }
            } else {
                this.className = "grid-cell ignore";
            }
        }

        // Check if this item is overduie
        if (this.props.overdue && this.props.point.isBefore(this.props.today, "d")) {
            let over = this.props.overdue.threshold + 1;
            if (submitted) {
                let overdueDate = this.props.point.clone().add(over, 'd');
                if (submitted.isAfter(overdueDate, 'd')) this.overdue = true;
            } else {
                // Record hasn't been submitted, determine if it's late
                if (this.props.today.diff(this.props.point, 'days') > over) this.overdue = true;
            }
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    _click = () => {
        let record_id = (this.props.record_map[this.props.point.format("YYYY-MM-DD")] || [null])[0];

        if (record_id) {
            window.dispatchEvent(new CustomEvent("ADD_TAB", {
                detail: {
                    type: "RECORD",
                    name: "New Record",
                    record_id: record_id,
                    form_id: this.props.fid
                }
            }));
        } else {
            window.dispatchEvent(new CustomEvent("ADD_TAB", {
                detail: {
                    type: "RECORD",
                    form_id: this.props.fid,
                    lid: this.props.lid || null,
                    dd: this.props.point.format("YYYY-MM-DD")
                }
            }));
        }
    };

    _onHover = () => {
        this._el.setAttribute("class", this._label);
        this._el.innerHTML = "";
    };

    _onMouseLeave = () => {
        this._el.innerHTML = this.label;
        this._el.setAttribute("class", "");
    };

    render() {
        return (
            <div className={this.className}
                 onClick={this._click}
                 onMouseLeave={this._onMouseLeave}
                 onMouseEnter={this._onHover}
                 onMouseOver={this._onHover}>
                <i ref={(el) => this._el = el}>{this.label}</i>
                {this.overdue ?
                    <div className="overdue"></div>
                    : null}
            </div>

        )
    }

}

class ReportingOverview extends React.Component {
    _mounted = false;

    constructor(props) {
        super(props);

        this.state = {
            data: {
                results: null
            },
            month: Moment().month(),
            year: Moment().year(),
            hasInterval: this.props.data.features.INTERVAL_REPORTING != null,
            hasLocation: this.props.data.features.LOCATION_REPORTING != null
        };
    }

    componentDidMount() {

        if (this.props.data.lid && this.props.data.definition.__dd__) {
            let interval = this.props.data.definition.__dd__.date_type || "DAY";
            if (interval == "WEEK") this.state.year = Moment().isoWeekYear();

            window.tx('com.sonoma.location.completeness', {
                uuid: this.props.data.lid,
                form_id: this.props.data.fid
            }, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                console.log(err);
            })
        }
    }

    _action = (action) => {
        switch (action) {
            case "CREATE":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "RECORD",
                        form_id: this.props.data.fid,
                        lid: this.props.data.lid || null,
                        name: window.__(this.props.data.form_name)
                    }
                }));
                break;
            case "BROWSE":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "BROWSE_RECORDS",
                        form_id: this.props.data.fid,
                        lid: this.props.data.lid || null,
                        name: window.__(this.props.data.form_name)
                    }
                }));
                break;
            case "MORE":
            default:
                break;
        }
    };

    _changeYear = (year) => {
        this.setState({
            year: year
        })
    };

    _changeYearMonth = (prop) => {
        this.setState({
            month: prop[1],
            year: prop[0]
        })
    };

    render() {
        let location_name;
        if (this.props.data.lid) {
            location_name = this.props.data.location_name.en || this.props.data.location_name;
        }

        let reportingInterval,
            grid,
            labelDate,
            control,
            className = "row",
            nextDue, actions = ACTIONS_NON_INTERVAL;

        if (this.state.hasInterval) {
            actions = ACTIONS;

            className += " bb";
            let today = Moment();
            let startDate, endDate;

            let interval = this.props.data.definition.__dd__.date_type || "DAY";

            switch (interval) {
                case "DAY":
                default:
                    control = <YearMonthControl onChange={this._changeYearMonth}
                                                value={[this.state.year, this.state.month]}/>;
                    startDate = Moment(`${this.state.year}-${this.state.month}-01`);
                    endDate = startDate.clone().endOf("month");
                    break;
                case "WEEK":
                case "MONTH":
                    control = <YearControl onChange={this._changeYear} value={this.state.year}/>;
                    startDate = Moment(`${this.state.year}-01-01`);
                    endDate = startDate.clone().endOf("year");
                    break;
                case "YEAR":
                    startDate = this.props.startDate.endOf("year");
                    endDate = this.props.endDate || Moment().endOf("year");
                    break;
            }

            let dates = DateUtils.getAllDatesInInterval(startDate.format("YYYY-MM-DD"), endDate.format("YYYY-MM-DD"), interval || "DAY");
            let nextDueDate = DateUtils.getNextPeriod(today, interval);

            let exists = (this.state.data.results || []).map(item => item[0]);

            let map = {};
            (this.state.data.results || []).forEach(item => {
                map[item[0]] = [item[1], item[2]];
            });

            let intervalLabel = "Daily";
            switch (interval) {
                case "DAY":
                    break;
                case "WEEK":
                    intervalLabel = "Weekly (ISO)";
                    break;
                case "MONTH":
                    intervalLabel = "Monthly";
                    break;
                case "YEAR":
                    intervalLabel = "Annually";
                    break;
                default:
                    labelDate = Moment.utc().format("YYYYY");
                    break;
            }

            reportingInterval = (
                <span
                    style={{color: "rgba(0,0,0,0.6)"}}>{intervalLabel} {startDate.format("YYYY-MM-DD")} to {endDate.format("YYYY-MM-DD")}</span>
            );

            if (this.state.data.results) {

                grid = (
                    <div className="row ro-details block">
                        <div className="grid">
                            {dates.map(item => {
                                return (
                                    <DateItem
                                        key={item.format("YYYY-MM-DD")}
                                        start_date={startDate}
                                        end_date={endDate}
                                        today={today}
                                        overdue={this.props.data.features.OVERDUE || null}
                                        fid={this.props.data.fid}
                                        lid={this.props.data.lid}
                                        record_map={map}
                                        interval={interval}
                                        exists={exists}
                                        point={item}/>
                                )
                            })}
                        </div>
                    </div>
                )

                nextDue = (
                    <div className="row bt gen-toolbar" style={TOOL_HEIGHT}>
                        <div className="column grid-title">Next Due: W41 2018</div>
                    </div>

                )
            } else if (this.state.data === false) {
                grid = (
                    <div className="row ro-details block" style={{paddingTop: "8px"}}>
                        <div className="no-reporting-period">
                            <div className="column icon">
                                <i className="fal fa-ban"></i>
                            </div>
                            <div className="column content">
                                <p>Looks like there isn't an open reporting period for this location/form</p>
                            </div>
                            <div className="column col-button">
                                <button>Start reporting</button>
                            </div>
                        </div>
                    </div>
                )
            } else {
                grid = (
                    <div className="row ro-details block">
                        <i className="fal fa-cog fa-spin"></i>
                    </div>
                )
            }
        }

        return (
            <div className="report-overview">
                <div className="row report-overview-inner">
                    <div className="column report-overview-info">
                        <div className={className}>
                            <div className="column block" style={{flex: 1, padding: "2px"}}>
                                <span
                                    style={{fontSize: "1.25rem"}}><strong>{location_name}</strong></span>
                                {reportingInterval}
                            </div>

                            <div className="column" style={{padding: "8px 0 8px 0px", flexGrow: 0, flexBasis: 0}}>
                                {control}
                            </div>
                            <div className="column" style={{padding: "8px 0 8px 8px", flexGrow: 0, flexBasis: 0}}>
                                <ActionGroup
                                    noCalc={true}
                                    dark={true}
                                    actions={actions}
                                    onAction={this._action}/>
                            </div>
                        </div>
                        {grid}
                        {nextDue}
                    </div>
                </div>
            </div>
        )
    }
}

class NonLocationReportingOverview extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="report-overview">
                <div className="row report-overview-inner">
                    <div className="column report-overview-info">
                        <div className="row ro-details block">
                            <div className="grid">
                            </div>
                        </div>
                    </div>
                    <ActionGroup
                        actions={ACTIONS}
                        vertical={true}
                        naked={true}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

const FILTERS_STYLE = {
    padding: "8px 16px"
};

const NO_PAD = {
    padding: "0"
}

class ViewOverviewAssignments extends React.Component {
    _mounted = false;

    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            location: null
        };
    }

    componentDidMount() {
        this._mounted = true;

        window.tx("com.sonoma.self.assignments", null, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_ASSIGNMENTS", err);
        })
    }

    _showLocationFilter = (e) => {
        e.preventDefault();
        e.stopPropagation();

        let menu = new window.Menu();

        menu.append(new window.MenuItem({
            label: "All locations",
            click: () => {
                this.setState({location: null});
            }
        }));

        menu.append(new window.MenuItem({
            type: "separator"
        }));

        let locations_dict = {};

        this.state.data.forEach(item => {
            locations_dict[item.lid] = item.location_name.en || item.location_name;
        });

        for (let i in locations_dict) {

            menu.append(new window.MenuItem({
                label: locations_dict[i],
                click: () => {
                    this.setState({
                        location: [i, locations_dict[i]]
                    })
                }
            }))
        }

        menu.popup();
    };

    _showFormSelector = (e) => {
        e.preventDefault();
        e.stopPropagation();

        let menu = new Menu();

        menu.append(new window.MenuItem({
            label: "All forms",
            click: () => {
                this.setState({form: null})
            }
        }));

        menu.append(new window.MenuItem({
            type: "separator"
        }));

        let forms_dict = {};
        this.state.data.forEach(item => {
            forms_dict[item.fid] = item.form_name.en || item.form_name;
        });


        for (let i in forms_dict) {
            menu.append(new window.MenuItem({ label: forms_dict[i],
                click: () => {
                    console.log(i, forms_dict[i]);
                    this.setState({
                        form: [i, forms_dict[i]]
                    })
                }
            }))
        }

        menu.popup();
    };


    render() {
        let items = [];

        // Group by form
        let forms = {};

        let data = this.state.data;

        console.log("FILTER", this.state.location, this.state.form);
        if (this.state.location) {
            data = data.filter(item => {
                return item.lid == this.state.location[0];
            })
        }

        if (this.state.form) {
            data = data.filter(item => {
                return item.fid == this.state.form[0];
            })
        }

        data.forEach(item => {
            if (forms[item.fid]) {
                forms[item.fid].push(item)
            } else {
                forms[item.fid] = [item];
            }
        });


        for (let i in forms) {
            items.push(
                <div className="form-section">
                    {forms[i][0].form_name.en || forms[i][0].form_name}
                </div>
            )
            if (forms[i][0].features.LOCATION_REPORTING) {

                forms[i].forEach(item => {
                    items.push(
                        <ReportingOverview
                            key={item.fid + "_" + (item.lid || "LONE")}
                            start_date={item.start_date}
                            end_date={item.end_date}
                            data={item}/>
                    )
                })
            } else {
                console.log("NO LOCATION | NO DATE", forms[i]);
            }
        }

        let locationFilterLabel = "All locations";
        if (this.state.location) locationFilterLabel = this.state.location[1];
        let formFilterLabel = "All forms";
        if (this.state.form) formFilterLabel = this.state.form[1];

        return (
            <div style={NO_PAD} id="report-overview" className="row block block-overflow bgd-ps-neutral">
                <div className="block"
                     style={FILTERS_STYLE}>
                    <a href="#"
                       onClick={this._showLocationFilter}
                       className="quick-menu">
                        <i className="fal fa-map-marker"></i>&nbsp;{locationFilterLabel}&nbsp;<i
                        className="fal fa-caret-down"></i>
                    </a>
                    <a href="#"
                       onClick={this._showFormSelector}
                       className="quick-menu">
                        <i className="fal fa-clipboard"></i>&nbsp;{formFilterLabel}&nbsp;<i
                        className="fal fa-caret-down"></i>
                    </a>
                </div>
                {items}
            </div>
        )
    }
}

export default ViewOverviewAssignments;
