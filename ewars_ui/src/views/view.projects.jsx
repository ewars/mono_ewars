import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-plus', 'CREATE', 'Create new project']
]

class ViewProjects extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Projects</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">

                </div>
            </div>
        )
    }
}

export default ViewProjects;