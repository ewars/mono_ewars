import React from "react";
import Moment from "moment";

import DocumentUtils from "../utils/document_utils";
import ReportUtils from "../utils/report_utils";

import ActionGroup from "../controls/c.actiongroup.jsx";
import ButtonGroup from "../controls/c.buttongroup.jsx";
import SelectField from "../controls/fields/f.select.jsx";
import Button from "../controls/c.button.jsx";

const ACTIONS = [
    ['fa-file-pdf', 'PDF', 'Download pdf'],
    ['fa-eye', 'VIEW', 'View document'],
    ['fa-share', 'SHARE', 'Share document']
];

class Spinner extends React.Component {
    render() {
        return (
            <div className="column"></div>
        )
    }
}

const DOC_ACTIONS = [
    ['fa-eye', 'VIEW', 'View document'],
    '-',
    ['fa-file-pdf', 'PDF', 'Download pdf'],
    ['fa-print', 'WINDOW', 'Open in new window'],
    ['fa-share', 'SHARE', 'Share']
];


class Week extends React.Component {
    _download = () => {

        let uri = "/document/pdf/";


        let segments = {
            a: window.user.aid,
            t: this.props.template.uuid,
            k: window.user.tki
        };

        if (this.props.data) segments.d = Moment(this.props.data).format("YYYY-MM-DD");
        if (this.props.data.report_uuid) segments.c = this.props.data.report_uuid;
        if (this.props.meta) segments.l = this.props.meta.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));
        uri = uri + "?ts=" + (new Date()).getTime();

        let r = new XMLHttpRequest();
        r.open("GET", uri);
        r.onreadystatechange = () => {
            if (r.readyState == 4 && r.status == 200) {
                let resp = JSON.parse(r.responseText);
                window.location.href = 'http://' + ewars.domain + '/document/download/' + resp.name;
                // window.open("http://" + ewars.domain + "/document/download/" + resp.name);
            }
        };

        r.send();
    };

    _data = () => {
        let data = {
            lid: this.props.meta.uuid,
            dd: this.props.data
        };

        window.sonomaClient.tx("com.ewars.document.data", [this.props.template.uuid, data])
            .then((resp) => {
            })

    };

    _share = () => {
        this.props.share(this.props.data, this.props.meta);
    };


    _action = (action) => {
        switch (action) {
            case "VIEW":
                let d_date = Moment.isMoment(this.props.data) ? this.props.data.format("YYYY-MM-DD") : this.props.data;
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "DOCUMENT",
                        meta: this.props.meta,
                        uuid: this.props.template.uuid,
                        name: this.props.template.template_name
                    }
                }));
                break;
            case "WINDOW":
                break;
            case "SHARE":
                break;
            default:
                break;
        }
    };

    _view = () => {
        if (Moment.isMoment(this.props.data)) {
            this.props.onClick(this.props.data.format("YYYY-MM-DD"), this.props.meta);
        } else {
            this.props.onClick(this.props.data, this.props.meta);
        }
    };

    _external = () => {
        let reportURI = "/document/";

        let segments = {
            a: window.user.aid,
            t: this.props.template.uuid,
        };

        if (this.props.data) {
            if (this.props.data.format) {
                segments.d = this.props.data.format("YYYY-MM-DD");
            } else {
                segments.d = this.props.data;
            }
        }
        if (this.props.meta.uuid) segments.l = this.props.meta.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        reportURI = reportURI + window.btoa(segs.join(";"));
        window.open(reportURI);
    };

    render() {
        let instanceName = ReportUtils.applyFormattingTags(
            this.props.template.instance_name,
            {
                location: this.props.meta,
                doc_date: this.props.data,
                interval: this.props.template.generation.interval
            }
        );

        return (
            <div className="doc-list-item" onClick={() => this._action("VIEW")}>
                <div className="row">
                    <div className="column label">{instanceName}</div>
                    <ActionGroup
                        naked={true}
                        actions={DOC_ACTIONS}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

class ListMultipleLocation extends React.Component {
    static defaultProps = {
        weeks: [],
        days: [],
        template: null,
        locations: null,
        limit: 60,
        filter: "ALL"
    }

    constructor(props) {
        super(props);

        this.state = {
            renderDay: null,
            renderLocationIndex: 0,
            locations: this.props.locations.sort((a, b) => {
                if (__(a.name) > __(b.name)) return 1;
                if (__(a.name) < __(b.name)) return -1;
                return 0;
            }),
            page: 0,
            offset: 0,
            limit: null,
            filter: null
        }

        let items = [];
        if (this.props.template.generation.interval == "WEEK") items = this.props.weeks;
        if (this.props.template.generation.interval == "DAY") items = this.props.days;


        let totalReports = this.state.locations.length * items.length;
        let chunks, remainder;
        if (totalReports <= 0) chunks = 0;
        if (totalReports <= this.props.limit) chunks = 1;
        if (totalReports > this.props.limit) {
            chunks = parseFloat(totalReports) / 60.0;
            remainder = parseFloat(totalReports) % 60.0;
        }

        this.state.totalReports = totalReports;

        let reports = [];
        items.forEach((itemDate) => {
            this.state.locations.forEach((location) => {
                reports.push([itemDate, location])
            })
        });
        this.state.documents = reports;

    }

    componentWillReceiveProps(nextProps) {
        let locations = this.props.locations;
        if (nextProps.filter != "ALL") {
            locations = this.props.locations.filter((item) => {
                return item.uuid == nextProps.filter;
            })
        }
        this.state.locations = locations;

        let items = [];
        if (this.props.template.generation.interval == "WEEK") items = this.props.weeks;
        if (this.props.template.generation.interval == "DAY") items = this.props.days;


        let totalReports = locations.length * items.length;
        let chunks, remainder;
        if (totalReports <= 0) chunks = 0;
        if (totalReports <= this.props.limit) chunks = 1;
        if (totalReports > this.props.limit) {
            chunks = parseFloat(totalReports) / 60.0;
            remainder = parseFloat(totalReports) % 60.0;
        }

        this.state.totalReports = totalReports;

        let reports = [];
        items.forEach((itemDate) => {
            locations.forEach((location) => {
                reports.push([itemDate, location])
            })
        });
        this.state.documents = reports;


    };

    componentDidMount() {
        this.state.height = this._el.getBoundingClientRect().height;
        this.state.limit = Math.round((this.state.height - 16) / (42 - 8));
        this.setState({
            height: this.state.height,
            limit: this.state.limit
        })
    };

    _start = () => {
        this.setState({
            offset: 0
        })
    };

    _end = () => {
        this.setState({
            offset: this.state.totalReports
        })
    };

    _next = () => {
        this.setState({
            offset: this.state.offset + this.state.limit
        })
    };

    _prev = () => {
        if (this.state.offset > 0) {
            this.setState({
                offset: this.state.offset - this.state.limit
            })
        }
    };

    render() {
        let items = [];
        if (this.state.limit != null) {
            items = this.state.documents.slice(this.state.offset, this.state.offset + this.state.limit);
        }

        let pageStart = this.state.offset > 0 ? this.state.offset + 1 : 1;
        let pageEnd = this.state.offset > 0 ? this.state.offset + this.state.limit + 1 : this.state.limit + 1;
        if (pageEnd > this.state.totalReports) pageEnd = this.state.totalReports;
        let pagination = `Showing ${pageStart} to ${pageEnd} of ${this.state.totalReports} documents`;
        return (
            <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                <div className="column">

                    <div ref={(el) => {
                        this._el = el;
                    }} style={{height: "100%", width: "100%"}}>
                        <div className="block-tree" style={{position: "relative"}}>
                            {items.map((item) => {
                                return <Week
                                    data={item[0]}
                                    mode={this.props.template.generation.interval}
                                    onClick={this.props.onClick}
                                    share={this.props.share}
                                    meta={item[1]}
                                    template={this.props.template}/>;
                            })}
                        </div>
                    </div>
                </div>
                <div className="btn-group pull-right">
                    <Button
                        onClick={this._prev}
                        icon="fa-backward"/>
                    <Button
                        onClick={this._start}
                        icon="fa-fast-backward"/>
                    <Button
                        onClick={this._end}
                        icon="fa-fast-forward"/>
                    <Button
                        onClick={this._next}
                        icon="fa-forward"/>

                </div>
            </div>
        )

    }
}

class ReportFlatList extends React.Component {
    static defaultProps = {
        weeks: [],
        days: [],
        template: null,
        locations: null
    };

    render() {
        let items = [];
        if (this.props.template.generation.interval == "WEEK") items = this.props.weeks;
        if (this.props.template.generation.interval == "DAY") items = this.props.days;


        return (
            <div>
                {items.map((date) => {
                    return <Week
                        data={date}
                        mode={this.props.template.generation.interval}
                        onClick={this.props.onClick}
                        share={this.props.share}
                        meta={this.props.meta}
                        template={this.props.template}/>;
                })}
            </div>
        )
    }
}

class Documents extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            years: [],
            year: null,
            days: [],
            day: null,
            weeks: [],
            meta: null,
            view: "DEFAULT",
            shareEmails: "",
            filter: "ALL",
            template: null
        }

        console.log(this.props);

        this._init(this.props.config.uuid);
        // ewars.subscribe("DATA_AVAILABLE", this._dataAvailable);

    }

    _init = (uuid) => {
        window.tx("com.sonoma.document", uuid, (res) => {

            let startDate = Moment.utc(res.generation.start_date);
            let endDate = Moment.utc(res.generation.end_date || undefined);
            let today = Moment.utc();

            let startYear = startDate.year(),
                endYear = endDate.year(),
                startMonth = startDate.month(),
                endMonth = endDate.month(),
                month = null,
                years = [],
                months = [],
                days = [],
                weeks = [];

            let year = today.year() > endYear ? endYear : today.year();
            months = DocumentUtils.getMonths(year, startDate, endDate);

            if (months[months.length - 1] > today.month()) {
                month = today.month();
            } else if (months[months.length - 1] <= today.month()) {
                month = months[months.length - 1];
            }

            if (res.generation.interval === "WEEK") {
                weeks = DocumentUtils.getWeeks(year, endDate);
            }

            if (res.generation.interval == "DAY") {
                days = DocuementUtils.getDays(year, month, startDate, endDate);
            }

            // get months
            years = DocumentUtils.range(startYear, year + 1, 1);

            this.setState({
                years,
                year,
                days,
                months,
                month,
                view: "DEFAULT",
                shareEmails: "",
                startDate,
                endDate,
                weeks,
                data: res
            }, () => {
                if (res.generation.location_spec === "SPECIFIC") {
                    this._getLocation(res);
                } else if (res.generation.location_spec == "TYPE") {
                    this._getLocations(res);
                }
            })
        }, (err) => {
            console.log(err);
        })
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.config.uuid != this.props.config.uuid) {
            this._init(nextProps.config.uuid);
        }
    }

    componentWillUnmount() {
        // ewars.unsubscribe("DATA_AVAILABLE", this._dataAvailable);
    }

    _dataAvailable = (data) => {
        this.setState({
            reportData: data,
            canDownload: true
        })
    };

    _downloadData = () => {
        // Get the document name
        let instanceName = ReportUtils.applyFormattingTags(
            this.props.data.instance_name,
            {
                location: this.props.meta || this.state.report.location,
                doc_date: this.state.report.doc_date,
                interval: this.props.data.generation.interval
            }
        );


        let domain = document.location.href;
        window.sonomaClient.tx("com.sonoma.document.data", [instanceName, this.state.reportData[0], this.state.reportData[1]], (res) => {
            document.location.href = 'http://' + domain + '/document/download/' + resp.n;
        }, (err) => console.log(err))

    };

    _getLocations = (data) => {
        window.sonomaClient.tx('com.sonoma.locations', {
            filters: {
                _: ['under', data.generation.location, 'location'],
                lti: ['eq', data.generation.location_type, 'int'],
                status: ['eq', 'ACTIVE', 'text']
            },
            sorters: {},
            limit: -1,
            offset: 0
        }, (res) => {
            this.setState({
                locations: res,
                meta: null
            })
        }, (err) => console.log(err));
    };


    _getLocation = (data) => {

        let location_id = data.generation.location || data.generation.location_uuid;
        window.sonomaClient.tx("com.sonoma.location.full", location_id, (res) => {
            this.setState({
                meta: res,
                locations: null
            })
        }, (err) => console.log(err));
    };

    _changeYear = (prop, value) => {
        let weeks = [],
            months = [],
            month = this.state.month,
            days = [];


        if (this.state.data.generation.interval == "WEEK") {
            weeks = DocumentUtils.getWeeks(value, this.state.endDate);
        }

        if (this.state.data.generation.interval === "DAY") {
            let startDate = Moment.utc(this.state.data.generation.start_date);
            let endDate = Moment.utc(this.state.data.generation.end_date || undefined);
            months = DocumentUtils.getMonths(value, this.state.startDate, this.state.endDate);
            month = months[months.length - 1];
            days = DocumentUtils.getDays(value, month, startDate, endDate);
        }


        this.setState({
            year: value,
            weeks: weeks,
            months: months,
            month: month,
            days: days
        });


    };

    _changeMonth = (prop, value) => {
        let startDate = Moment.utc(this.state.data.generation.start_date);
        let endDate = Moment.utc(this.state.data.generation.end_date || undefined);
        let days = DocumentUtils.getDays(this.state.year, value, startDate, endDate);
        this.setState({
            month: value,
            days: days
        })
    };

    _share = (report, location) => {
        if (report) {
            let doc_date = report;
            if (Moment.isMoment(doc_date)) {
                doc_date = doc_date.format("YYYY-MM-DD");
            }
            this.setState({
                showShade: true,
                report: {
                    doc_date: doc_date,
                    location: location
                }
            });
        } else {
            this.setState({
                showShade: true
            })
        }
    };

    _onEmailsChange = (e) => {
        this.setState({
            shareEmails: e.target.value
        })
    };

    _showReport = (doc_date, location) => {
        this.setState({
            view: "REPORT",
            canDownload: false,
            reportData: null,
            report: {
                doc_date: doc_date,
                location: this.state.meta || location
            }
        })
    };

    _sendShare = () => {

        let uri = "/document/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        segments.d = Moment(this.state.report.doc_date).format("YYYY-MM-DD");
        segments.l = this.state.meta ? this.state.meta.uuid : this.state.report.location.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        // Get the document name
        let instanceName = ReportUtils.applyFormattingTags(
            this.props.data.instance_name,
            {
                location: this.props.meta || this.state.report.location,
                doc_date: this.state.report.doc_date ? this.state.report.doc_date : this.state.report,
                interval: this.props.data.generation.interval
            }
        );

        window.sonomaClient.tx('com.ewars.document.share', [uri, this.state.shareEmails, instanceName])
            .then((resp) => {
                this.setState({
                    showShare: false,
                    shareEmails: ""
                });
            })
    };

    _clear = () => {
        this.setState({
            view: "DEFAULT",
            report: {}
        })
    };

    _download = (report) => {

        let uri = "/document/pdf/";

        let segments = {
            a: window.user.aid,
            t: this.props.data.uuid,
            k: window.user.tki
        };

        if (report) segments.d = Moment(report.doc_date).format("YYYY-MM-DD");
        segments.l = report.location.uuid;

        let segs = [];
        for (var i in segments) {
            segs.push(`${i}:${segments[i]}`);
        }

        uri = uri + window.btoa(segs.join(";"));

        let r = new XMLHttpRequest();
        r.open("GET", uri);
        r.onreadystatechange = () => {
            if (r.readyState == 4 && r.status == 200) {
                let resp = JSON.parse(r.responseText);
                bl.destroy();
                document.location.href = 'http://' + ewars.domain + '/document/download/' + resp.name;
            }
        };

        r.send();
    };

    _onModalAction = (action) => {
        this.setState({
            showShade: false
        })
    };

    _onFilterChange = (prop, value) => {
        this.setState({
            filter: value
        })
    };

    render() {
        console.log(this.state);
        if (!this.state.data) {
            return (
                <div className="column">
                    <p>Loading</p>
                </div>
            )
        }
        if (this.state.data.generation.location_spec == "SPECIFIC") {
            if (!this.state.meta) {
                return <Spinner/>;
            }
        } else if (!this.state.locations) {
            return <Spinner/>
        }

        let view;

        if (this.state.view == "DEFAULT" && this.state.data.generation.location_spec != "TYPE") {
            view = (
                <ReportFlatList
                    locations={this.state.locations}
                    data={this.state.year}
                    weeks={this.state.weeks}
                    days={this.state.days}
                    meta={this.state.meta}
                    share={this._share}
                    onClick={this._showReport}
                    template={this.state.data}/>
            );
        }

        if (this.state.view == "DEFAULT" && this.state.data.generation.location_spec == "TYPE") {
            view = (
                <ListMultipleLocation
                    filter={this.state.filter}
                    locations={this.state.locations}
                    data={this.state.year}
                    weeks={this.state.weeks}
                    days={this.state.days}
                    meta={this.state.meta}
                    share={this._share}
                    onClick={this._showReport}
                    template={this.state.data}/>
            )
        }

        let locations = [
            ["ALL", "All Locations"]
        ];
        if (this.state.data.generation.location_spec == "TYPE") {
            let options = this.state.locations.map((item) => {
                return [item.uuid, __(item.name)];
            })
            options = options.sort((a, b) => {
                if (a[1] > b[1]) return 1;
                if (a[1] < b[1]) return -1;
                return 0;
            })
            locations.push.apply(locations, options);
        }

        toolbar = (
            <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                <div className="column grid-title">{this.state.data.template_name}</div>
                {this.state.data.generation.interval == "WEEK" ?
                    <ButtonGroup
                        name="year"
                        value={this.state.year}
                        onUpdate={this._changeYear}
                        config={{
                            options: this.state.years.map((item) => {
                                return [item, item]
                            })
                        }}/>
                    : null}
                {this.state.data.generation.interval == "MONTH" ?
                    <ButtonGroup
                        name="month"
                        value={this.state.month}
                        onUpdate={this._changeMonth}
                        config={{
                            options: this.state.months.map((item) => {
                                return [item, DocumentUtils.MONTHS[item]]
                            })
                        }}/>
                    : null}

                {this.state.data.generation.location_spec == "TYPE" ?
                    <div style={{float: "right", marginRight: "8px", marginTop: "-2px"}}>
                        <SelectField
                            onChange={this._onFilterChange}
                            value={this.state.filter}
                            searchProp="n"
                            data={{
                                n: "location_id",
                                o: locations
                            }}/>
                    </div>
                    : null}

            </div>
        )

        return (
            <div className="column">
                {toolbar}
                <div className="row block block-overflow">
                    {view}
                </div>
            </div>
        )
    }
}

export default Documents;
