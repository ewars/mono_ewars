import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-print', 'PRINT', "PRINT_GUIDANCE"],
    ['fa-file-pdf', 'PDF', 'PDF_GUIDANCE']
];

const USER_TREE = [
    {
        t: "Overview",
        m: "user.overview.md"
    },
    {
        t: "Assignments",
        m: "user.assignments.md",
        c: [
            {t: "Requesting Assignments"},
            {t: "Ending an assignment"},
            {t: "Moving an assignment"}
        ]
    },
    {
        t: "Tasks",
        m: "user.tasks.md",
        c: []
    },
    {
        t: "Reporting",
        m: "user.reporting.md",
        c: []
    },
    {
        t: "Teams",
        m: "user.teams.md"
    },
    {
        t: "Alerts",
        m: "user.alerts.md",
    },
    {
        t: "Performance",
        m: "user.performance.md"
    },
    {
        t: "Data Export",
        m: "user.export.md"
    },
    {
        t: "Documents",
        m: "user.documents.md"
    },
    {
        t: "Projects",
        m: "user.projects.md"
    },
    {
        t: "Dashboards",
        m: "user.dashboards.md"
    }
];

const ANALYSIS_TREE = [
    {
        t: "Notebooks",
        m: "a.notebooks.md",
    },
    {
        t: "Plots",
        m: "a.plots.md"
    },
    {
        t: "Pivots",
        m: "a.pivots.md"
    },
    {
        t: "Maps",
        m: "a.pivots.md"
    },
    {
        t: "M&E Auditor",
        m: "a.auditor.md"
    },
    {
        t: "Alerts Auditor",
        m: "a.auditor.alerts.md"
    }
];

const ADMIN_TREE = [
    {
        t: "Overview",
        m: "admin.overview.md"
    },
    {
        t: "Forms",
        m: "admin.forms.md"
    },
    {
        t: "Locations & Location Types",
        m: "admin.locations.md"
    },
    {
        t: "Assignments",
        m: "admin.assignments.md"
    }
];

const ABOUT_TREE = [
    {
        t: "About EWARS",
        m: "gen.about.md"
    },
    {
        t: "Technology",
        m: "gen.technology.md"
    },
    {
        t: "Contributors",
        m: "gen.contributors.md"
    }
];

class TreeNode extends React.Component {
    static defaultProps = {};

    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    render() {
        let icon = "fal fa-file";
        if (this.props.data.c) {
            icon = this.state.show ? "fal fa-folder-open" : "fal fa-folder";
        }

        let clicker = this.props.data.c ? this._toggle : null;

        return (
            <div className="location-tree-node">
                <div className="node" onClick={clicker}>
                    <div className="row">
                        <div className="column node-icon" style={{maxWidth: "12px"}}>
                            <i className={icon}></i>
                        </div>
                        <div className="column node-name">{this.props.title}</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="children">
                        {this.props.data.c.map(item => {
                            return <TreeNode title={item.t} data={item}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class ViewKb extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="column panel br block block-overflow" style={{paddingLeft: "8px", paddingRight:"8px"}}>
                    <TreeNode
                        title="User"
                        data={{c: USER_TREE}}/>
                    <TreeNode
                        title="Admin"
                        data={{c: ADMIN_TREE}}/>
                    <TreeNode
                        title="About"
                        data={{c: ABOUT_TREE}}/>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                        <div className="column grid-title"></div>
                        <ActionGroup
                            onAction={this._action}
                            actions={ACTIONS}/>
                    </div>
                </div>


            </div>
        )
    }
}

export default ViewKb;