import React from "react";

import {Responsive, WidthProvider} from 'react-grid-layout';

const ResponsiveGridLayout = WidthProvider(Responsive);

import ws from "../libs/api";
import AnalysisUtils from "../utils/analysis_utils";

const WIDGETS = {
    METRIC: null,
    SERIES: null,
    HEATMAP: null,
    PIVOT: null,
    TABLE: null
};

const UNSUPPORTED = [
    "METRICS",
    "METRICS_OVERALL"
];

class WidgetUnsupported extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="legacy-dash-widget unsupported">
                <div className="row">
                    <i className="fal fa-exclamation-triangle"></i>
                </div>
                <div className="row">
                    <p>This widget type is no longer supported, please update to a newer widget.</p>
                </div>
            </div>
        )
    }
}

class Widget extends React.Component {
    render() {
        console.log(this.props.data);
        if (UNSUPPORTED.indexOf(this.props.data.type) >= 0) {
            return (
                <WidgetUnsupported/>
            )
        }
        return (
            <div className="legacy-dash-widget">
                <div className="column br bb bl bt" style={{height: "100%"}}>
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                        <div className="column grid-title">Widget&nbsp;<i className="fal fa-caret-down"></i></div>
                        <div className="groupbar">
                            <div className="bar-item">
                                <i className="fal fa-chart-line"></i>
                            </div>
                            <div className="bar-item">
                                <i className="fal fa-database"></i>
                            </div>
                            <div className="bar-item">
                                <i className="fal fa-cog"></i>
                            </div>
                        </div>
                    </div>
                    <div className="row block">

                    </div>
                </div>
            </div>
        )
    }
}

class DashboardView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let layout = this.props.data.layout.map(item => {
            item.static = true;
            return item;
        });

        return (
            <div className="column" style={{padding: "4px"}}>
                <div
                    ref={(el) => {
                        this._el = el;
                    }}
                    onDrop={this._onRootDrop}
                    onDragOver={this._onDragOver}
                    style={{
                        height: "100%",
                        overflowY: "auto",
                        border: "1px solid rgba(255,255,255,0.2)"
                    }}>
                    <ResponsiveGridLayout
                        className="layout"
                        rowHeight={30}
                        layouts={{lg: layout || []}}
                        autoSize={false}
                        verticalCompact={true}
                        onBreakPointChange={this.onBreakpointChange}
                        onLayoutChange={this._onLayoutChange}
                        breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
                        cols={{lg: 36, md: 36, sm: 36, xs: 4, xxs: 2}}>
                        {(layout || []).map(item => {
                            return (
                                <div key={item.i}>
                                    <Widget
                                        key={"WIDG_" + item.i}
                                        onEdit={this._onEdit}
                                        onDelete={this._onRemoveItem}
                                        data={this.props.data.data[item.i]}/>
                                </div>
                            )
                        })}
                    </ResponsiveGridLayout>
                </div>
            </div>
        )
    }
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            config: null
        }

        window.sonomaClient.tx('com.sonoma.resource', this.props.config.uuid, (res) => {
            this.setState({
                config: res
            })
        }, (err) => {
            window.state.error("An error occurred loading this dashboard", err);
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.config.uuid != this.props.config.uuid) {
            window.sonomaClient.tx("com.sonoma.resource", nextProps.config.uuid, (res) => {
                this.setState({
                    config: res
                })
            }, (err) => {
                window.state.error("An error occurred loading this dashboard", err);
            });
        }
    }

    render() {
        if (!this.state.config) {
            return (
                <div className="row block">
                    Loading...
                </div>
            )
        }

        let data = this.state.config;
        if (data.version == 1) {
            data = AnalysisUtils.upgrade(data);
        }

        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">
                        {this.state.config.name.en || this.state.config.name}
                    </div>
                </div>
                <DashboardView data={data}/>

            </div>
        )
    }
}

export default Dashboard;
