import React from "react";
import { styled, withStyle } from "styletron-react";

import ContextProvider from "../context_provider";
import AccountsPlaceholder from "../placeholders/p.accounts.jsx";

const ERRORS = {
    ERR_NO_CONNECTION: "You are currently offline / disconnected from the central server. Please check your internet connection and try again."
}

const RANDO_COLORS = [
    'lightblue',
    'red',
    'orange',
    'purple',
    'cream',
    'chartreuse',
    'indigo',
    'yellow',
    'amber',
    'scarlet'
];

const USER_TYPES = {
    USER: "Reporting User",
    ACCOUNT_ADMIN: "Account Administrator",
    REGIONAL_ADMIN: "Geographic Administrator"
};

const Button = styled("button", {
    display: "inline-block",
    background: "#F2F2F2",
    color: "rgba(0,0,0,0.9)",
    border: "1px solid rgba(0, 0, 0, 0.25)",
    boxShadow: "inset 0 -1px 0px rgba(0, 0, 0, 0.1)",
    padding: "8px 10px",
    borderRadius: "3px",
    textAlign: "center",
    transition: "all 0.5s ease",
    marginRight: "8px",
    ":focus": {
        outline: 0
    },
    ":hover": {
        background: "#484848",
        color: "#F2F2F2",
        boxShadow: "inset 0 1px 1px rgba(255, 255, 255, 0.4)"
    }
});

const BlockButton = withStyle(Button, {
    display: "block",
    marginRight: "0",
    marginTop: "4px",
    marginBottom: "4px"
});

const Column = styled("div", {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    position: "relative",
    minHeight: 0
});

const Row = styled("div", {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    minHeight: 0
});

const AccountList = styled(Row, {
    position: "absolute",
    display: "block",
    top: 0,
    right: 0,
    background: "#333333",
    width: "100%",
    height: "100%",
    overflowY: "auto",
    paddingTop: "4px",
    paddingBottom: "4px"
});

const Label = styled("label", {
    display: "block",
    marginBottom: "2px",
    fontSize: "0.9rem",
    color: "#333333"
});

const Input = styled("input", {
    background: "#FFFFFF",
    border: "1px solid #DADADA",
    fontSize: "14px",
    display: "block",
    width: "100%",
    clear: "both",
    marginBottom: "16px",
    padding: "3px"
});

const LoginHeader = styled("div", {
    textAlign: "center"
});

const LoginWrapper = styled("div", {
    display: "flex",
    marginLeft: "16px",
    marginRight: "16px",
    marginTop:"16px",
    padding: "16px",
    background: "white",
    borderRadius: "3px",
    boxShadow:"0px 3px 3px rgba(0, 0, 0, 0.5)"
});

const LoginMain = styled("div", {
    background: "#FFFFFF",
    paddingTop:"8px",
    paddingBottom: "8px",
    flex: 1
});

const AccountListItem = styled("div", {
    display: "flex",
    flexDirection: "row",
    height: "50px",
    marginBottom: "4px",
    marginTop: "4px",
    cursor: "pointer",
    transition: "all 0.5s ease",
    background: "rgba(255, 255, 255, 0.1)",
    transition: "background-color 0.2s ease-out",
    ":hover": {
        background: "rgba(255, 255, 255, 0.4)"
    }
});

const AccountItemStatus = styled("div", {
    display: "block",
    marginRight: "8px",
    width: "5px",
    maxWidth: "5px",
    flex: 1
});

const AccountItemDetails = styled("div", {
    display: "block",
    padding: "8px",
    justifyContent: "center"
});

const AccountItemName = styled("div", {
    display: "block",
    fontSize: "14px",
    color: "#FFFFFF",
    marginBottom: "5px"
});

const AccountItemRole = styled("div", {
    display: "block",
    fontSize: "12px",
    color: "#F2F2F2"
});

const DashSection = styled("div", {
    display: "block",
    padding: "0 10px",
    padding: "5px 16px",
    fontSize: "1.2rem",
    border: "none",
    margin: "12px 8px 5px 0px",
    fontWeight: "bold",
    color: "#F2F2F2"
});

class AccountItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        window.dispatchEvent(new CustomEvent("SET_USER", {detail: {
            user: this.props.data,
            account: this.props.account
        }}));
    };

    _context = () => {
        let menu = new Menu();

        menu.append(
            new MenuItem({
                label: "Remove Account",
                click: () => {
                    console.log("HERE");
                }
            })
        );

        menu.popup({window: remote.getCurrentWindow()});
    };

    render() {
        let className = "status";

        let style = {
            background: RANDO_COLORS[this.props.index]
        };

        return (
            <div className="login-block" onClick={this._onClick}>
                <h4>{this.props.data.name}</h4>
                <p>{USER_TYPES[this.props.data.role]}</p>
            </div>
        )
    }
}

class DebugView extends React.Component {
    static contextType = ContextProvider;
    constructor(props) {
        super(props);
    }

    _auth = () => {
        this.context.cx({
            type: "AuthRequest",
            email: "demo.admin@ewars.ws",
            password: "ewars123",
            domain: "_val.ewars.ws"
        }, (res) => {
            this.context.cx({
                type: "GetUsers"
            }, (res) => {
                console.log(res);
            }, (err) => {
                console.log(err);
            })

        }, (err) => {
            this.setState({
                error: err
            });
        });
    }

    _getusers = () => {
        this.context.cx({
            type: "GetUsers"
        }, (res) => {
            console.log(res);
        }, (err) => {
            console.log(err)
        });
    };

    _back = () => {
        this.props.onBack();
    };

    render() {

        return (
            <Column>
                <Button onClick={this._auth}>Authenticate</Button>
                <Button onClick={this._getusers}>Get Users</Button>
                <Button onClick={this._seedreq}>Request Seed</Button>
                <Button onClick={this._push}>Push Events</Button>
                <Button onClick={this._checkVersion}>Check Version</Button>
                <Button onClick={this._getUpdate}>Get Update</Button>
                <Button onClick={this._rebase}>Rebase</Button>
                <div style={{display: "block", height: "10px"}}></div>
                <Button onClick={this._back}>Back</Button>
            </Column>

        )
    }
}

class ViewLogin extends React.Component {
    static contextType = ContextProvider;
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            domain: "_val.ewars.ws",
            error: null,
            debug: false,
            users: [],
            accounts: []
        }

    }

    componentDidMount() {
        this._loadUsers();
    }

    _loadUsers = () => {
        this.context.cx({
            type: "GetUsers"
        }, (res) => {
            console.log(res);
            this.setState({
                users: res.users,
                accounts: res.accounts
            });
        }, (err) => {
            console.log(err);
        });
    };


    componentWillMount() {
        window.addEventListener("online", this._online);
        window.addEventListener("offline", this._offline);
    }

    componentWillUnmount() {
        window.removeEventListener("online", this._online);
        window.removeEventListener("offline", this._offline);
    }

    _debug = () => {
        this.setState({debug: true});
    };

    _online = () => {
        console.log("ONLINE")
    };

    _offline = () => {
        console.log("OFFLINE");
    };

    _onValueChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    _signIn = () => {
        // Validate the inputs
        if (this.state.email == "") return;
        if (this.state.email.indexOf("@") < 0) return;
        if (this.state.password == "") return;
        if (this.state.domain == "") return;
        if (this.state.domain.indexOf(".") < 0) return;

        let tenancies = [];

        let payload = {
            email: this.state.email,
            password: this.state.password,
            domain: this.state.domain
        };

        this.context.cx({
            type: "AuthRequest",
            email: this.state.email,
            password: this.state.password,
            domain: this.state.domain
        }, (res) => {
            if (res.success) {
                this._loadUsers();
            } else {
                this._loadUsers();
            }
        }, (err) => {
            console.log(err);
            this.setState({
                error: err
            });
        });

    };

    render() {
        let isOnline = navigator.onLine;

        if (this.state.debug) {
            return <DebugView onBack={() => { this.setState({debug: false}) }}/>;
        }

        let items = [];
        let user_dict = {};
        this.state.users.forEach(user => {
            if (user_dict[user.tki]) {
                user_dict[user.tki].push(user);
            } else {
                user_dict[user.tki] = [user];
            }
        });

        this.state.accounts.forEach(account => {
            items.push(<div className="account-section">{account.account_name}</div>);

            (user_dict[account.tki] || []).forEach(user => {
                items.push(<AccountItem data={user} account={account} key={user.uuid}/>);
            });

        });

        if (this.state.users.length <= 0) {
            items = (
                <AccountsPlaceholder/>
            );
        }

        let debug;
        if (global.debug) debug = <button className="btn-block" onClick={this._debug}>Debug</button>

        return (
            <div className="login-wrapper">
                <div className="login-column">
                        <h1>EWARS</h1>
                    <div className="login-main">

                        {this.state.error ?
                            <p className="errorGraph">{ERRORS[this.state.error] || "Unknown Error occurred"}</p>
                                : null}

                            <Label htmlFor="">Email/Username *</Label>
                            <Input
                                placeholder="e.g. r.user@ewars.ws..."
                                name="email"
                                value={this.state.email}
                                onChange={this._onValueChange}
                                type="text"/>

                            <Label htmlFor="password">Password *</Label>
                            <Input
                                name="password"
                                placeholder="Your ewars password"
                                type="password"
                                value={this.state.password}
                                onChange={this._onValueChange}/>

                            <button className="btn-block"
                                onClick={this._signIn}>
                                <i className="fal fa-sign-in"></i>&nbsp;Sign In
                            </button>

                            {debug}

                    </div>
                </div>
                <div className="account-column">
                    <div className="account-inner">
                        {items}
                    </div>
                </div>
                <div className="login-footer">
                    <div className="row bt gen-toolbar" style={{maxHeight: "20px", minHeight: "20px"}}>
                        <div className="column grid-title">Online - v0.0.1-develop - Build [ ] </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewLogin;
