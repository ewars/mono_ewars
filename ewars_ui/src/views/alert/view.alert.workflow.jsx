import React from "react";
import Moment from "moment";

import AlertStageOutcome from "./stages/alert.stage.outcome.jsx";
import AlertStageVerification from "./stages/alert.stage.verification.jsx";
import AlertStageRiskAssessment from "./stages/alert.stage.risk_assessment.jsx";
import AlertStageRiskCharacterization from "./stages/alert.stage.risk_characterization.jsx";

const STAGES = ["TRIGGERED", "VERIFICATION", "RISK_ASSESS", 'RISK_CHAR', "OUTCOME", "END"];

class AlertTriggered extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-workflow-triggered">
                <div className="alert-circle">
                    <div className="alert-icon">

                    </div>
                </div>
            </div>
        )
    }
}

class AlertInterStage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let className = "alert-inter-stage", label;
        let curStageIndex = STAGES.indexOf(this.props.data.stage);
        let toStageIndex = STAGES.indexOf(this.props.stage);
        if (toStageIndex > curStageIndex) {
            className += " completed";
        } else {
            className += " pending";
        }

        return (
            <div className={className}>
                <div className="inter-stage-date">{label}</div>
                <div className="inter-stage-line">
                </div>

            </div>
        )
    }
}

class AlertVerificationStage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-stage">

            </div>
        )
    }
}

class AlertRiskAssessmentStage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-stage">

            </div>
        )
    }
}

class AlertRiskCharStage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-stage">

            </div>
        )
    }
}

class AlertOutcomeStage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-stage">

            </div>
        )
    }
}

class AlertStage extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        if (this.props.stage != "TRIGGERED") {
            this.props.onClick(this.props.stage);
        }
    };

    render() {
        let icon = <i className="fal fa-cog"></i>;
        let style = {}, label = "STAGE";
        if (this.props.stage == "TRIGGERED") {
            icon = <i className="fal fa-bell"></i>
            label = "Triggered";
        } else {
            let curStageIndex = STAGES.indexOf(this.props.alert.stage);
            let stageIndex = STAGES.indexOf(this.props.stage);
            switch (this.props.stage) {
                case "OUTCOME":
                    label = "4. Outcome";
                    if (this.props.alert.status == "CLOSED") {
                        icon = <i className="fal fa-check"></i>
                    } else if (curStageIndex == stageIndex && this.props.alert.status == "OPEN") {
                        icon = <i className="fal fa-pencil"></i>;
                    } else {
                        icon = <i className="fal fa-lock"></i>;
                    }
                    break;
                case "RISK_CHAR":
                    label = "3. Risk Characterization";
                    if (curStageIndex > stageIndex) {
                        icon = <i className="fal fa-check"></i>;
                    } else if(curStageIndex == stageIndex) {
                        icon = <i className="fal fa-pencil"></i>;
                    } else {
                        icon = <i className="fal fa-lock"></i>;
                    }
                    break;
                case "RISK_ASSESS":
                    label = "2. Risk Assessment";
                    if (curStageIndex > stageIndex) {
                        icon = <i className="fal fa-check"></i>;
                    } else if (curStageIndex == stageIndex) {
                        icon = <i className="fal fa-pencil"></i>;
                    } else {
                        icon = <i className="fal fa-lock"></i>
                    }
                    break;
                case "VERIFICATION":
                    label = "1. Verification";
                    if (curStageIndex > stageIndex) {
                        icon = <i className="fal fa-check"></i>;
                    } else if (curStageIndex == stageIndex) {
                        icon = <i className="fal fa-pencil"></i>;
                    } else {
                        icon = <i className="fal fa-lock"></i>;
                    }
                    break;
                default:
                    break;
            }

        }

        let arrow = false;
        if (this.props.curView == this.props.stage) arrow = true;


        return (
            <div className="alert-stage" onClick={this._onClick}>
                <div className="alert-stage-circle">
                    {icon}
                </div>
                <div className="alert-stage-name">{label}</div>
                {arrow ?
                    <div className="alert-stage-arrow"></div>
                : null}
            </div>
        )
    }
}

const STAGE_VIEWS = {
    VERIFICATION: AlertStageVerification,
    RISK_ASSESS: AlertStageRiskAssessment,
    RISK_CHAR: AlertStageRiskCharacterization,
    OUTCOME: AlertStageOutcome
}

class AlertWorkflow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: this.props.alert.stage || "VERIFICATION",
            data: this.props.alert.data || {}
        }
    }

    _setView = (view) => {
        this.setState({
            view: view
        });
    };

    render() {
        let ViewCmp = STAGE_VIEWS[this.state.view];
        return (
            <div className="alert-workflow-wrapper">
                <div className="alert-workflow">
                    <AlertStage stage="TRIGGERED" onClick={this._setView}>
                        <AlertTriggered data={this.props.alert}/>
                    </AlertStage>
                    <AlertInterStage from="TRIGGERED" to="VERIFICATION" data={this.props.alert}/>
                    <AlertStage curView={this.state.view} stage="VERIFICATION" alert={this.props.alert} onClick={this._setView}>
                        <AlertVerificationStage data={this.props.alert}/>
                    </AlertStage>
                    <AlertInterStage from="VERIFICATION" to="RISK_ASSESS" data={this.props.alert}/>
                    <AlertStage curView={this.state.view} alert={this.props.alert} stage="RISK_ASSESS" onClick={this._setView}>
                        <AlertRiskAssessmentStage data={this.props.alert}/>
                    </AlertStage>
                    <AlertInterStage from="RISK_ASSESS" to="RISK_CHAR" data={this.props.alert}/>
                    <AlertStage alert={this.props.alert} curView={this.state.view} stage="RISK_CHAR" onClick={this._setView}>
                        <AlertRiskCharStage data={this.props.alert}/>
                    </AlertStage>
                    <AlertInterStage from="RISK_CHAR" to="OUTCOME" data={this.props.alert} alert={this.props.alert}/>
                    <AlertStage alert={this.props.alert} curView={this.state.view} stage="OUTCOME" onClick={this._setView}>
                        <AlertOutcomeStage data={this.props.alert}/>
                    </AlertStage>
                </div>
                {this.props.alert.status == "CLOSED" ?
                    <div className="row bb gen-toolbar">
                        <div className="column grid-title">Alert Closed</div>
                        <button>Re-open alert</button>
                    </div>
                : null}
                <div className="workflow-item">
                    <ViewCmp alert={this.props.alert}/>
                </div>
            </div>
        )
    }
}

export default AlertWorkflow;
