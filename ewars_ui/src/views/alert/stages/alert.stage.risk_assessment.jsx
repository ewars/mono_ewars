import React from "react";

import Form from "../../../controls/forms/form.reporting.jsx";

const DEFINITION = {
    assessment_hazard: {
        type: "textarea",
        required: true,
        label: "Hazard Assessment",
        help: "",
        root: true
    },
    assessment_context: {
        type: "textarea",
        required: true,
        label: "Context Assessment",
        help: "",
        root: true
    },
    assessment_exposure: {
        type: "textarea",
        required: true,
        label: "Exposure Assessment",
        help: "",
        root: true
    }
}

class AlertStageRiskAssessment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                assessment_hazard: this.props.alert.data.assessment_hazard || "",
                assessment_context: this.props.alert.data.assesment_context || "",
                assessment_exposure: this.props.alert.data.assessment_exposure || ""
            }
        }
    }

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    render() {
        let locked = true;
        if (this.props.alert.status == "OPEN" && this.props.alert.stage == "RISK_ASSESS") locked = false;
        return (
            <div className="alert-workflow-editor">
                <div className="row bb gen-toolbar" style={{minHeight: "30px"}}>
                    <div className="column grid-title">2. Risk Assessment</div>

                    {!locked ?
                        <div>
                            <button><i className="fal fa-send"></i>&nbsp;Submit</button>
                            <button><i className="fal fa-save"></i>&nbsp;Save</button>
                        </div>
                    : null}
                </div>
                <div className="column block overflowY">
                    <Form
                        readOnly={locked}
                        definition={DEFINITION}
                        onChange={this._onChange}
                        data={this.state.data}/>
                </div>

            </div>
        )
    }
}

export default AlertStageRiskAssessment;
