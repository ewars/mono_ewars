import React from "react";

import Form from "../../../controls/forms/form.reporting.jsx";

const DEFINITION = {
    risk: {
        type: "risk",
        label: "Risk",
        required: true,
        root: true
    }
}

class AlertStageRiskCharacterization extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                risk: this.props.alert.data.risk || []
            }
        }
    }

    render() {
        let locked = true;
        if (this.props.alert.status == "OPEN" && this.props.alert.stage == "RISK_CHAR") locked = false;
        return (
            <div className="alert-workflow-editor">
                <div className="row bb gen-toolbar" style={{minHeight: "30px"}}>
                    <div className="column grid-title">3. Risk Characterization</div>

                    {!locked ?
                    <div>
                        <button><i className="fal fa-send"></i>&nbsp;Submit</button>
                        <button><i className="fal fa-save"></i>&nbsp;Save</button>
                    </div>
                    : null}
                </div>
                <div className="row block overflowY">
                    <Form
                        readOnly={locked}
                        definition={DEFINITION}
                        onChange={this._onChange}
                        data={this.state.data}/>
                </div>

            </div>
        )
    }
}

export default AlertStageRiskCharacterization;
