import React from "react";

import Form from "../../../controls/forms/form.reporting.jsx";
import ActionGroup from "../../../controls/c.actiongroup.jsx";

const OPTIONS = [
    ["ADVANCE", "Start Risk Assessment"],
    ["DISCARD", "Discard"],
    ["MONITOR", "Monitor"]
]


const DEFINITION = {
    verification_comments: {
        type: "text",
        required: true,
        label: "Verification Comment(s)",
        root: true
    },
    verification_outcome: {
        type: "select",
        label: "Verification Outcome",
        options: OPTIONS,
        required: true,
        root: true
    }
}

class AlertStageVerification extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                verification_outcome: props.alert.data.verification_outcome || "",
                verification_comments: props.alert.data.verification_comments || ""
            }
        }
    }

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        });
    };

    render() {
        let locked = true;
        if (this.props.alert.status == "OPEN" && this.props.alert.stage == "VERIFICATION") locked = false;
        return (
            <div className="column alert-workflow-editor">
                <div className="row bb gen-toolbar" style={{minHeight: "30px"}}>
                    <div className="column grid-title">1. Verification</div>

                    {!locked ?
                    <div>
                        <button><i className="fal fa-submit"></i>&nbsp;Submit</button>
                        <button><i className="fal fa-save"></i>&nbsp;Save</button>
                    </div>
                    : null}

                </div>
                <div className="row block overflowY">
                    <Form 
                        readOnly={locked}
                        definition={DEFINITION}
                        onChange={this._onChange}
                        data={this.state.data}/>
                </div>

            </div>
        )
    }
}

export default AlertStageVerification;
