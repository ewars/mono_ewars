import React from "react";

import Form from "../../../controls/forms/form.reporting.jsx";

import ActionGroup from "../../../controls/c.actiongroup.jsx";

const ACTIONS = [
    
]; 
const OPTIONS = [
    ["RESPOND", "Respond"],
    ['DISCARD', 'Discard'],
    ['MONITOR', 'Monitor']
];


const DEFINITION = {
    outcome_comments: {
        field_type: "textarea",
        type: "textarea",
        required: true,
        label: "Verification Comment(s)",
        order: 1,
        root: true
    },
    outcome: {
        field_type: "select",
        type: "select",
        options: OPTIONS,
        required: true,
        label: "Outcome",
        order: 2,
        root: true
    }
}


class AlertStageOutcome extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                outcome: this.props.alert.data.outcome || "",
                outcome_comments: this.props.alert.data.outcome_comments || ""
            }
        }
    }

    _onChange = (prop, value) => {

    };

    render() {
        let locked = true;
        if (this.props.alert.stage == "OUTCOME" && this.props.alert.status == "OPEN") locked = false;
        return (
            <div className="column alert-workflow-editor">
                <div className="row bb gen-toolbar" style={{minHeight: "30px"}}>
                    <div className="column grid-title">4. Outcome</div>

                    {!locked ?
                    <div>
                        <button><i className="fal fa-send"></i>&nbsp;Submit</button>
                        <button><i className="fal fa-save"></i>&nbsp;Save Draft</button>
                    </div>
                    : null}
                </div>
                <div className="row block overflowY">
                    <Form 
                        readOnly={locked}
                        data={this.state.data}
                        definition={DEFINITION}
                        onChange={this._onChange}/>

                </div>
            </div>
        )
    }
}

export default AlertStageOutcome;
