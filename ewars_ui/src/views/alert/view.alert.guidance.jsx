import React from 'react';

import Showdown from "showdown";


class ViewAlertGuidance extends React.Component {
    constructor(props) {
        super(props);

        this.state = {content: null}

        window.sonomaClient.tx("com.sonoma.alarm.guidance", this.props.data.uuid, (res) => {
            this.setState({
                content: res
            })
        }, (err) => {
            console.log(err);
        })
    }

    render() {
        if (!this.state.content) {
            return (
                <div className="row">
                    <div className="loader-view">
                        <div className="loader-text">Loading guidance...</div>
                    </div>
                </div>
            )
        }


        let converter = new Showdown.Converter({
            tables: true,
            simplifiedAutoLink: true,
            strikethrough: true,
            tasklists: true
        });

        let guidance = converter.makeHtml(this.state.content || "");


        return (
            <div className="row block block-overflow" dangerouslySetInnerHTML={{__html: guidance}}>

            </div>
        )
    }
}

export default ViewAlertGuidance;