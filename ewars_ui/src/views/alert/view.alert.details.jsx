import React from "react";
import Moment from "moment";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import DEFAULT_WORKFLOW from "../../constants/forms/form.alert.default";

const ACTIONS = [
    ['fa-print', 'PRINT', "PRINT_ALERT"],
    ["fa-file-pdf", "PDF", "PDF_ALERT"],
    '-',
    ['fa-question-circle', "HELP", "HELP"]
];

const STAGES = ["VERIFICATION", "RISK_ASSESS", "RISK_CHAR", "OUTCOME"];
const STATUS = {
    CLOSED: "Closed",
    OPEN: "Open"
};

const STAGE_NAMES = {
    VERIFICATION: "1. Verification",
    RISK_ASSESS: "2. Risk Assessment",
    RISK_CHAR: "3. Risk Characterization",
    OUTCOME: "4. Outcome"
}


class Stage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let className = "fal fa-square";

        let curStageIndex = STAGES.indexOf(this.props.curStage);
        let stageIndex = STAGES.indexOf(this.props.stage);
        if (curStageIndex > stageIndex) {
            className = "fal fa-check-square";
        }

        if (this.props.curStage == "OUTCOME" && this.props.status == "CLOSED") {
            className = "fal fa-check-square";
        }

        return (
            <div className="alert-stage-node">
                <div className="row">
                    <div className="column" style={{maxWidth: "20px"}}>
                        <i className={className}></i>
                    </div>
                    <div className="column block">
                        {STAGE_NAMES[this.props.stage]}
                    </div>
                </div>
            </div>
        )
    }
}

class ViewAlertDetails extends React.Component {
    constructor(props) {
        super(props);
    }

    _location = () => {
        window.state.showLocationCard(this.props.data.lid);
    }

    render() {
        let location = this.props.data.location_name ? true : false;
        let closed = this.props.data.status == "CLOSED" ? true : false;

        let workflow = DEFAULT_WORKFLOW;

        let workflow_stages =["VERIFICATION", "RISK_ASSESS", "RISK_CHAR", "OUTCOME"];
        let stages = workflow_stages.map(stage => {
            return (
                <Stage
                    curStage={this.props.data.stage}
                    status={this.props.data.status}
                    stage={stage}/>
            )
        })


        return (
            <div className="column block block-overflow" style={{maxWidth: "300px", minWidth: "300px"}}>
                <div className="dash-section">
                    <i className="fal fa-bell"></i>&nbsp;{__(this.props.data.alarm_name)}
                </div>

                <div style={{paddingLeft: "16px", paddingRight: "16px"}}>
                    <div className="dash-handle">
                        <i className="fal fa-circle"></i>&nbsp;Status:&nbsp;{__(this.props.data.status)}
                    </div>

                    <div className="dash-handle">
                        <i className="fal fa-calendar"></i>&nbsp;Alert Date:&nbsp;{this.props.data.alert_date}
                    </div>

                    {location ?
                        <div className="dash-handle" onClick={this._location}>
                            <i className="fal fa-map-marker"></i>&nbsp;Location: {this.props.data.location_name.en || this.props.data.location_name}
                        </div>
                        : null}

                    <div className="dash-handle">
                        <i className="fal fa-calendar"></i>&nbsp;Raised:&nbsp;{Moment.utc(this.props.data.raised).local().format("YYYY-MM-DD HH:mm:ss")}
                    </div>

                    {closed ?
                        <div className="dash-handle">
                            <i className="fal fa-ban"></i>&nbsp;Closed:&nbsp;{Moment.utc(this.props.data.closed).local().format("YYYY-MM-DD HH:mm:ss")}
                        </div>
                            : null}
                </div>


                <div className="dash-section">
                    <i className="fal fa-map-marker"></i>&nbsp;Location Tree
                </div>

                <div className="alert-location-tree">

                </div>

                <div className="dash-section">
                    <i className="fal fa-people-carry"></i>&nbsp;Stage Completion
                </div>
                <div className="alert-stages-tree">
                    {stages}
                </div>
            </div>
        )
    }
}

export default ViewAlertDetails;
