import React from "react";

class AlertTabs extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="alert-tabs">
                <div
                    onClick={this.props.onChange("DEFAULT")}
                    className={"alert-tab" + (this.props.view == "DEFAULT" ? " active" : "")}>
                    <div className="icon"><i className="fal fa-bell"></i></div>
                    <div className="label">Alert</div>
                </div>
                <div
                    onClick={this.props.onChange("ANALYSIS")}
                    className={"alert-tab" + (this.props.view == "ANALYSIS" ? " active" : "")}>
                    <div className="icon"><i className="fal fa-chart-line"></i></div>
                    <div className="label">Analysis</div>
                </div>
                <div
                    onClick={this.props.onChange("USERS")}
                    className={"alert-tab" + (this.props.view == "USERS" ? " active" : "")}>
                    <div className="icon"><i className="fal fa-users"></i></div>
                    <div className="label">Users</div>
                </div>
                <div
                    onClick={this.props.onChange("RECORDS")}
                    className={"alert-tab" + (this.props.view == "RECORDS" ? " active" : "")}>
                    <div className="icon"><i className="fal fa-clipboard"></i></div>
                    <div className="label">Records</div>
                </div>
                <div
                    onClick={this.props.onChange("GUIDANCE")}
                    className={"alert-tab" + (this.props.view == "GUIDANCE" ? " active" : "")}>
                    <div className="icon"><i className="fal fa-graduation-cap"></i></div>
                    <div className="label">Guidance</div>
                </div>
            </div>
        )
    }
}

class ViewAlertMain extends React.Component {
    state = {
        view: "DEFAULT",
        alert: null
    };

    constructor(props) {
        super(props);

        this._mounted = false;

        window.sonomaClient.tx("com.sonoma.alert", this.props.config.uuid, (res) => {
            if (this._mounted) {
                this.setState({
                    alert: res
                })
            } else {
                this.state.alert = res;
            }
        }, (err) => {
            window.state.error("ERROR", err);
        })
    }

    componentDidMount() {
        this._mounted = true;
    }

    _view = (view) => {
        this.setState({
            view
        })
    };

    render() {
        if (!this.state.alert) {
            return (
                <div className="column">
                    <p>Loading...</p>
                </div>
            )
        }

        return (
            <div className="row">
                <div className="column br">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <AlertTabs view={this.state.view} onChange={this._view}/>
                    </div>
                </div>
                <div className="column panel">

                </div>
            </div>
        )
    }
}

export default ViewAlertMain;
