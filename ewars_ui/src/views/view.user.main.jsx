import React from "react";
import Moment from "moment";

//const {remote} = require("electron");
//const {Menu, MenuItem} = remote;

import GridCell from "../controls/c.gridcell.jsx";

import AccountId from "../controls/widgets/w.account.id.jsx";
import ReportingSynopsisWidget from "../controls/widgets/w.syn.reporting.jsx";
import ActivitySynopsisWidget from "../controls/widgets/w.syn.activity.jsx";
import AlertsSynopsisWidget from "../controls/widgets/w.syn.alerts.jsx";
import NewsSynopsisWidget from "../controls/widgets/w.syn.news.jsx";
import ResponseWidget from "../controls/widgets/w.syn.response.jsx";

const STYLES = {
    CARD: {
        background: "#FFFFFF",
        width: "100%",
        border: "1px solid rgba(0,0,0,0.1)"
    },
    CARD_HEADER: {
        padding: "8px",
        marginBottom: "8px",
        display: "flex"
    },
    CARD_LINK: {
        paddingLeft: "8px",
        color: "#4175f7"
    },
    CARD_TITLE: {
        fontSize: "1.2rem",
        color: "#333333",
    },
    CARD_BODY: {
        padding: "8px"
    },
    CARD_WRAPPER: {
        margin: "8px 8px 8px 0"
    }
}

class Card extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let style = {};
        if (this.props.marginRight) style.marginRight = this.props.marginRight;
        if (this.props.marginBottom) style.marginBottom = this.props.marginBottom;
        return (
            <div className="dash-card" style={style}>
                <div className="card-header">
                    <div className="card-title">{this.props.title}</div>
                    {this.props.link ?
                            <div className="card-link">{this.props.link[0]}</div>
                    : null}
                </div>
                <div className="card-body">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

const GRID = {
    display: "grid",
    height: "100%",
    margin: "8px",
    gridColumnGap: "8px",
    gridRowGap: "8px",
    justifyItems: "stretch",
    alignItems: "stretch",
    gridTemplateRows: "30px auto",
    gridTemplateColumns: "1fr 1fr 1fr"
}

class ViewUserMain extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column" style={{background: "#F2f2F2"}}>
                <div style={GRID}>
                    <GridCell row={1} col={1} rowEnd={2} colEnd={4}>
                        <AccountId/>
                    </GridCell>

                    <GridCell row={2} col={1} rowEnd={4} colEnd={2}>
                        <ReportingSynopsisWidget/>
                    </GridCell>

                    <GridCell row={2} rowEnd={3} col={2} colEnd={3}>
                        <AlertsSynopsisWidget/>
                    </GridCell>

                    <GridCell row={3} rowEnd={4} col={2} colEnd={3}>
                        <NewsSynopsisWidget/>
                    </GridCell>

                    <GridCell row={2} rowEnd={3} col={3} colEnd={4}>
                        <ResponseWidget/>
                    </GridCell>

                    <GridCell row={3} col={3} rowEnd={4} colEnd={4}>
                        <ActivitySynopsisWidget/>
                    </GridCell>
                </div>
            </div>
        )
    }
}

export default ViewUserMain;
