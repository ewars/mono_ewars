import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";
import SelectField from "../controls/fields/f.select.jsx";
import DateField from "../controls/fields/f.date.jsx";
import FormSelectField from "../controls/fields/f.form.jsx";

import DateUtils from "../utils/date_utils";

const METRICS = [
    ['COMPLETENESS', 'Completeness'],
    ['TIMELINESS', 'Timeliness']
];

var METRIC_DEFINITIONS = {
    COMPLETENESS: {
        format: function (value) {
            let perc;
            if (value[1].s <= 0 && value[1].e <= 0) {
                perc = "0%";
            } else if (value[0].s == 0 && value[1].s > 0) {
                perc = "0%";
            } else {
                perc = value[1].s / value[1].e;
            }

            return value[1].s + " / " + value[1].e + " (" + perc + ")";
        },
        getColor: function (dataPoint) {
            var perc = (dataPoint[1].s / dataPoint[1].e) * 100;
            if (perc < 60) return "red";
            if (perc >= 100) return "green";
            if (perc >= 60 && perc < 80) return "yellow";
            if (perc >= 80 && perc < 100) return "orange";
        }
    },
    TIMELINESS: {
        format: function (value) {
            let dt = value[1];

            let perc = "0%";
            perc = dt[0] / dt[1];
            if (dt[1] <= 0) perc = "0%";

            return `${dt[0]} / ${dt[1]} (${perc})`;
        },
        getColor: function (value) {
            let dt = value[1];

            let perc = dt[0] / dt[1] * 100;
            if (perc < 60) return "red";
            if (perc >= 100) return "green";
            if (perc >= 60 && perc < 80) return "yellow";
            if (perc >= 80 && perc < 100) return "orange";
        }
    }
};

class AuditorLocation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="auditor-row">
                <div className="column" style={{flex: 1}}>
                    <i className="fal fa-check-square"></i>
                </div>
                <div className="column">{__(this.props.location.name)}</div>

            </div>
        )
    }
}

class ViewAuditTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="row">
                    <div className="column">LocationName</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                </div>
                <div className="row">
                    <div className="column">Location</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                    <div className="column">value</div>
                </div>
            </div>
        )
    }
}


class ViewAuditor extends React.Component {
    state = {
        forms: null,
        metric: "COMPLETENESS",
        form: {},
        start_date: null,
        end_date: null,
        locations: []
    };

    constructor(props) {
        super(props);

    }

    componentDidMount() {
        window.tx("com.sonoma.self.locations", null, (res) => {
            console.log(res);
            this.setState({
                locations: res
            });
        }, (err) => {
            console.log(err);
        });
    }

    _onChange = (prop, value, form) => {
        let val = value;
        if (prop == "form") val = form;
        this.setState({
            ...this.state,
            [prop]: val
        });
    };

    render() {
        let intervalType = "DAY";
        let dateRange = [];

        return (
            <div className="column">
                <div className="row gen-toolbar bb" style={{maxHeight: "30px"}}>
                    <div className="column br">
                        <SelectField
                            options={METRICS}
                            name="metric"
                            value={this.state.metric}
                            onChange={this._onChange}/>
                    </div>
                    <div className="column br" style={{maxWidth: "20px", minWidth: "20px"}}>
                        <i className="fal fa-question-circle"></i>
                    </div>
                    <div className="column">
                        <FormSelectField
                            onChange={this._onChange}
                            name="form"
                            value={this.state.form.uuid || null}/>
                    </div>
                    <div className="column">
                        <DateField
                            value={this.state.start_date}
                            name="start_date"
                            date_type="DAY"
                            onChange={this._onChange}/>
                    </div>
                    <div className="column">
                        <DateField
                            value={this.state.end_date}
                            name="end_date"
                            date_type="DAY"
                            onChange={this._onChange}/>
                    </div>
                </div>
                <div className="row">
                    <div className="column br panel" style={{maxWidth: "250px"}}>
                        <div className="auditor-locations">
                            {this.state.locations.map(item => {
                                return <AuditorLocation location={item} dates={[]}/>;;
                            })}
                        </div>

                    </div>
                    <div className="column">
                        <div className="auditor-grid">

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewAuditor;
