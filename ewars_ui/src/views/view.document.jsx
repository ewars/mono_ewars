import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";
import DocumentUtils from "../utils/document_utils";
import ReportUtils from "../utils/report_utils";

import Client from "sonoma_ui";

const DOC_ACTIONS = [
    ['fa-file-pdf', 'PDF', "Download pdf"],
    ['fa-share', 'SHARE', 'Share'],
    ['fa-print', 'PRINT', 'Print'],
    ['fa-download', 'DOWNLOAD', "Download data"],
    ["fa-text", 'TEST', 'TEST']
];

function iFrameReady(iFrame, fn) {
    var timer;
    var fired = false;

    function ready() {
        if (!fired) {
            fired = true;
            clearTimeout(timer);
            fn.call(this);
        }
    }

    function readyState() {
        if (this.readyState === "complete") {
            ready.call(this);
        }
    }

    // cross platform event handler for compatibility with older IE versions
    function addEvent(elem, event, fn) {
        if (elem.addEventListener) {
            return elem.addEventListener(event, fn);
        } else {
            return elem.attachEvent("on" + event, function () {
                return fn.call(elem, window.event);
            });
        }
    }

    // use iFrame load as a backup - though the other events should occur first
    addEvent(iFrame, "load", function () {
        ready.call(iFrame.contentDocument || iFrame.contentWindow.document);
    });

    function checkLoaded() {
        var doc = iFrame.contentDocument || iFrame.contentWindow.document;
        // We can tell if there is a dummy document installed because the dummy document
        // will have an URL that starts with "about:".  The real document will not have that URL
        if (doc.URL.indexOf("about:") !== 0) {
            if (doc.readyState === "complete") {
                ready.call(doc);
            } else {
                // set event listener for DOMContentLoaded on the new document
                addEvent(doc, "DOMContentLoaded", ready);
                addEvent(doc, "readystatechange", readyState);
            }
        } else {
            // still same old original document, so keep looking for content or new document
            timer = setTimeout(checkLoaded, 1);
        }
    }
    checkLoaded();
}


class ReportFrame extends React.Component {
    static defaultProps = {
        uri: null
    };

    getFrame = () => {
        return this._el;
    };

    render() {

        // Add cache bust
        var endURI = this.props.uri + "&cache=" + Date.now();
        return (
            <div className="row block block-overflow" ref={(el) => this._el = el}
                 style={{position: "relative", padding: "0"}}>

                <iframe src={endURI}
                        ref={(el) => this._el = el}
                        frameBorder="0"
                        width="100%"
                        height="100%"
                        style={{
                            height: "100%",
                            width: "100%",
                            overflow: "hidden",
                            top: 0,
                            left: 0,
                            position: "absolute"
                        }}></iframe>

            </div>
        )
    }
};

const CONTENT_TEMPLATE = `
<!doctype html>
<html lang="en">
<head>

<script type="text/javascript">
        window._isPDF = true;
        window.dt = {
            template_id: "{TEMPLATE_ID}",
            report_date: "{REPORT_DATE}",
            location_id: "{LOCATION_ID}",
            pdf: "{PDF}",
            inline: "{INLINE}",
            location_name: '{LOCATION_NAME}',
            report: "{REPORT_DATE}",
            location: {},
            orientation: "{ORIENTATION}"
        };


        Function.prototype.bind = Function.prototype.bind || function (thisp) {
            var fn = this;
            return function () {
                return fn.apply(thisp, arguments);
            };
        }
    </script>

    <style>
        * {
            -webkit-print-color-adjust: exact !important; /* Chrome, Safari */
            color-adjust: exact !important; /*Firefox*/
        }
    </style>
    
</head>

<body style="height: auto; overflow: auto;">

<div id="report" style="background: white;">
    <div class="report-progress" id="progress-wrapper">
        <div class="report-progress-inner" id="progress"></div>
    </div>
    <div class="report-template-wrapper" id="report-content">
        {CONTENT}
    </div>
</div>

</html>
`;

class ReportWebView extends React.Component {
    static defaultProps = {
        report_date: "",
        location_id: ""
    };

    componentDidMount() {
        console.log(this._el);

        this._view = document.createElement("webview");

        let content = CONTENT_TEMPLATE;
        content = content.replace("{CONTENT}", this.props.data.content);
        content = content.replace("{REPORT_DATE}", this.props.report_date);
        content = content.replace("{ORIENTATION}", this.props.data.orientation);
        if (this.props.location_id) content = content.replace("{LOCATION_ID}", this.props.location_id);
        this._view.setAttribute("src", "data:text/html," + content);
        this._view.setAttribute("disablewebsecurity", "");
        this._view.setAttribute("nodeintegration", "");
        this._view.setAttribute("preload", "./document_bundle.js");

        this._view.style.width = "100%";

        if (this._el) {
            this._view.addEventListener("did-start-loading", () => {
                console.log("STARTED_LOADING");
            });

            this._view.addEventListener("did-stop-loading", () => {
                console.log("STOPPED_LOADING");
            });

            this._view.addEventListener("dom-ready", () => {
                this._view.openDevTools();

                console.log("DOM_READY")
            });

            this._view.addEventListener("console-message", (e) => {
                console.log("DOCUMENT: ", e.message);
            });


            this._el.appendChild(this._view);

        }
    }

    render() {
        return (
            <div className="row" ref={(el) => this._el = el}>

            </div>
        )
    }
}

class DocumentView extends React.Component {
    static defaultProps = {
        location_id: "",
        report_date: ""
    };

    componentDidMount() {
        let frame = document.getElementById("REPORT_FRAME");

        iFrameReady(frame, () => {
            console.log("READY")
            let doc = document.getElementById("REPORT_FRAME").contentDocument;
            this._document = new Client.Document(doc, this.props.data, this.props.report_date, this.props.location_id);
            this._document.init();
        });
    };

    print = () => {
        console.log("PRINT");
    };

    componentWillUnmount() {
        let frame = document.getElementById("REPORT_FRAME");
    }

    render() {
        let content = CONTENT_TEMPLATE;
        content = content.replace("{CONTENT}", this.props.data.content);
        content = content.replace("{REPORT_DATE}", this.props.report_date);
        content = content.replace("{ORIENTATION}", this.props.data.orientation);
        if (this.props.location_id) content = content.replace("{LOCATION_ID}", this.props.location_id);

        return (
            <div className="row block block-overflow" style={{overflowX: "auto", padding: "0"}}>
                <iframe srcDoc={content}
                        id="REPORT_FRAME"
                        style={{width: "100%", height: "100%"}}
                        ref={(el) => this._el = el}
                        frameBorder="0"></iframe>
            </div>
        )
    }
}

class ViewDocument extends React.Component {
    mounted = false;

    constructor(props) {
        super(props);

        this.state = {
            document: null,
            title: null
        };

        window.sonomaClient.tx('com.sonoma.document', this.props.config.uuid, (res) => {
            console.log(res);
            let instanceName = ReportUtils.applyFormattingTags(
                res.instance_name,
                {
                    location: this.props.config.lid || null,
                    doc_date: this.props.config.document_date || null,
                    interval: res.generation.interval
                }
            );
            if (this.mounted) {
                this.setState({
                    document: res,
                    title: instanceName
                });

            } else {
                this.state.document = res;
                this.state.title = instanceName;
            }
        }, (err) => {
            window.state.error("An error occurred", err);
        })
    }

    componentDidMount() {
        this.mounted = true;
    }

    _action = (action) => {
        switch (action) {
            case "PRINT":
                this._print();
                // let reportURI = "/document/";
                //
                // let segments = {
                //     a: window.state.account.user.aid,
                //     t: this.props.config.uuid,
                // };
                //
                // if (this.props.config.document_date) segments.d = this.props.config.document_date;
                // if (this.props.config.report_uuid) segments.c = this.props.config.report_uuid;
                // if (this.props.config.lid) segments.l = this.props.config.lid;
                //
                // let segs = [];
                // for (var i in segments) {
                //     segs.push(`${i}:${segments[i]}`);
                // }
                //
                // reportURI = reportURI + window.btoa(segs.join(";"));
                //
                // window.open(reportURI);
                break;
            case "PDF":
                this._pdf();
                break;
            default:
                break;
        }
    };

    _print = () => {
        let wv = document.createElement("webview");
        let doc = document.getElementById("REPORT_FRAME").contentDocument;
        let content = doc.body.innerHTML;

        wv.setAttribute("src", "data:text/html," + content);
        wv.addEventListener("did-stop-loading", () => {

            wv.print({
                printBackground: true,
            }, (err, data) => {
                console.log(err, data);
                document.body.removeChild(wv);
            })

        });
        document.body.appendChild(wv);
    }

    _pdf = () => {
        let wv = document.createElement("webview");
        let doc = document.getElementById("REPORT_FRAME").contentDocument;
        let content = doc.body.innerHTML;

        wv.setAttribute("src", "data:text/html," + content);
        wv.addEventListener("did-stop-loading", () => {

            wv.printToPDF({
                printBackground: true,
                marginsType:1,
                pageSize: "A4",
                landscape: this.state.document.orientation == "HORIZONTAL" ? true : false
            }, (err, data) => {
                console.log(err, data);
                document.body.removeChild(wv);
            })

        });
        document.body.appendChild(wv);


    };

    render() {
        if (!this.state.document) {
            return (
                <div className="column loading">
                    <p>Loading...</p>
                </div>
            )
        }

        let view;

        if (window.DESKTOP) {
            view = <DocumentView
                report_date={this.props.config.document_date}
                report_uuid={this.props.config.report_uuid || null}
                location_id={this.props.config.lid || null}
                data={this.state.document}/>;
            // view = <ReportWebView data={this.state.document}/>
        } else {

            let reportURI = "/document/";

            let segments = {
                a: window.state.account.user.aid,
                t: this.props.config.uuid,
            };

            if (this.props.config.document_date) segments.d = this.props.config.document_date;
            if (this.props.config.report_uuid) segments.c = this.props.config.report_uuid;
            if (this.props.config.lid) segments.l = this.props.config.lid;

            let segs = [];
            for (var i in segments) {
                segs.push(`${i}:${segments[i]}`);
            }

            reportURI = reportURI + window.btoa(segs.join(";"));
            view = <ReportFrame uri={reportURI} ref={(el) => this._iframe = el}/>
        }

        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">
                        {this.state.title}
                    </div>
                    <ActionGroup
                        actions={DOC_ACTIONS}
                        onAction={this._action}/>
                </div>
                {view}
            </div>
        )
    }
}

export default ViewDocument;

