import Moment from "moment";
import DEFAULT_WORKFLOW from "../constants/forms/form.alert.default";
import FormUtils from "../utils/form_utils";

import React from "react";

import DataGrid from "../controls/datagrid/c.datagrid.jsx";

import L from "leaflet";
// import turf from "turf";
// import centroid from "@turf/centroid";
// import clustersDbscan from "@turf/clusters-dbscan";


const GRID_ACTIONS = [
    ['fa-download', "EXPORT", "Export alerts"]
];

const ALERT_ACTIONS = [
    ['fa-eye', 'VIEW', "View alert"],
    ['fa-sync', 'REEVALUATE', 'Re-evaluate alert']
];


class AlertsMap {
    constructor(props, el) {
        this.props = props;
        this.el = el;
        this._map = null;
    }

    render() {
        let osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        let osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
        this._map = L.map(this.el, {});

        this._map.zoomControl.setPosition('bottomright');
        this._map.setView([0, 0], 0);
        let osm = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: 20, attribution: osmAttrib});
        this._map.addLayer(osm);
    }
}

class AlertsMapView extends React.Component {
    componentDidMount() {
        this.map = new AlertsMap(this.props.data, this._el);
        this.map.render();
    }

    render() {
        return (
            <div className="column block" ref={(el) => this._el = el}>

            </div>
        )
    }
}

class AlertsDataView extends React.Component {
    state = {
        filter: "OPEN"
    };

    constructor(props) {
        super(props);
    }

    _query = (sorting, filters, limit, offset) => {
        let endFilters = filters;

        if (this.props.risk) {
            endFilters.push({
                key: "data.risk",
                cmp: "eq",
                val_type: "text",
                value: this.props.risk
            });
        } 

        if (this.props.status) {
            endFilters.push({
                key: "status",
                cmp: "eq",
                val_type: "text",
                value: this.props.status
            });
        }

        if (this.props.lid) {
            endFilters.push({
                key: "location",
                cmp: "under",
                val_type: "location",
                value: this.props.lid
            });
        }

        return new Promise((resolve, reject) => {
            window.sonomaClient.tx(
                "com.sonoma.alerts",
                {
                    orders: sorting,
                    filters: endFilters,
                    limit: (limit == null ? 100 : limit),
                    offset: (offset == null ? 0 : offset)
                },
                (res) => {
                    console.log(res);
                    resolve(res);
                },
                (err) => {
                    console.log(err)
                });
        })
    };

    _action = (action, data) => {
        switch (action) {
            case "VIEW":
                let alarm_name = this.props.data.name.en || this.props.data.name;
                alarm_name = alarm_name.split(" ").map(item => item[0]).join("").toUpperCase();

                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "ALERT",
                        alert_id: data.uuid,
                        icon: "fa-bell",
                        name: window.__(alarm_name) + ": " + Moment.utc(data.alert_date).local().format("YY-MM-DD")
                    }
                }));
                break;
            case "DELETE":
                this._deleteAlert(data);
                break;
            default:
                break;
        }
    };

    render() {
        let grid;

        if (Object.keys(this.props.data.workflow).length > 0) {
            grid = FormUtils.alert_grid(this.props.data.workflow.definition || {});

        } else {
            grid = FormUtils.alert_grid(DEFAULT_WORKFLOW.definition);
        }

        let baseFilters = {
            alid: ["eq", "text", String(this.props.data.uuid)],
            status: ["eq", "text", this.state.filter]
        };

        return (
            <div className="column">
                <DataGrid
                    grid_id={this.props.data.uuid + "_ALERTS"}
                    title={"Alerts: " + (this.props.data.name.en || this.props.data.name)}
                    grid={grid}
                    actions={GRID_ACTIONS}
                    rowActions={ALERT_ACTIONS}
                    baseFiltering={baseFilters}
                    baseOrdering={{}}
                    onAction={this._action}
                    queryFn={this._query}/>
            </div>
        )
    }
}


class AlertsView extends React.Component {
    static defaultProps = {
        config: {aid: null}
    };

    state = {
        alarm: null,
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.alarm", this.props.config.aid, (res) => {
            console.log(res);
            this.setState({
                alarm: res
            })
        }, (err) => {
            window.state.error("Error loading alarm definition", err);
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.config.aid != this.props.config.aid) {
            window.sonomaClient.tx('com.sonoma.alarm', nextProps.config.aid, (res) => {
                this.setState({
                    alarm: res
                })
            }, (err) => {
                window.state.error("An error occurred", err);
            })
        }
    }

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        if (!this.state.alarm) {
            return (
                <div className="column block">
                    <p>Loading...</p>
                </div>
            )
        }

        let view;

        if (this.state.view == "DEFAULT") {
            view = (
                <AlertsDataView
                    {...this.props.config}
                    data={this.state.alarm}/>
            )
        }

        if (this.state.view == "MAP") {
            if (navigator.onLine) {
                view = (
                    <AlertsMapView
                        data={this.state.alarm}/>

                )
            } else {
                view = (
                    <div className="column block">
                        <p>You must be connected to the internet to use this feature.</p>
                    </div>
                )
            }
        }

        return view;
    }
}

export default AlertsView;
