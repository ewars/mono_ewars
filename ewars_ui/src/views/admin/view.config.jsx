import React from "react";

import Form from "../../controls/forms/form.reporting.jsx";
import ActionGroup from "../../controls/c.actiongroup.jsx";

const ACCOUNT_CONFIG = {
    name: {
        type: "text",
        label: "Account Name",
        i18n: true,
        required: true,
        order: 0
    },
    domain: {
        type: "text",
        label: "Domain",
        required: true,
        order: 1
    },
    description: {
        type: "textarea",
        label: "Description",
        required: true,
        order: 2
    },
    lang: {
        type: "select",
        label: "Default language",
        options: [],
        required: true,
        order: 3
    },
    timezone: {
        type: "select",
        label: "Default timezone",
        options: [],
        required: true,
        order: 4
    }
};

const SECURITY = {
    tow_factor: {
        type: "buttonset",
        label: "Two-Factor Authentication",
        options: [
            [true, "Enabled"],
            [false, "Disabled"]
        ],
        order: 1
    },
    enforce_ssl: {
        type: "buttonset",
        label: "Enforce SSL",
        options: [
            [true, "Enabled"],
            [false, "Disabled"]
        ],
        order: 2
    }
};


const SETTINGS = {
    GENERAL: ACCOUNT_CONFIG,
    SECURITY: SECURITY,
    THEME: null

}

class ViewConfig extends React.Component {
    state = {
        view: "GENERAL"
    };

    constructor(props) {
        super(props);
    }

    _view = (view) => {
        this.setState({view})
    };

    render() {
        let definition = SETTINGS[this.state.view] || {};

        return (
            <div className="row">
                <div className="column panel br block block-overflow" style={{paddingTop: "8px"}}>
                    <div
                        onClick={() => this._view("GENERAL")}
                        className="dash-handle">
                        <i className="fal fa-cog"></i>&nbsp;General Settings
                    </div>
                    <div
                        onClick={() => this._view("SECURITY")}
                        className="dash-handle">
                        <i className="fal fa-lock"></i>&nbsp;Security &amp; Authentication
                    </div>
                    <div
                        onClick={() => this._view("THEME")}
                        className="dash-handle">
                        <i className="fal fa-paint-brush"></i>&nbsp;Theme
                    </div>
                    <div
                        onClick={() => this._view("LANGUAGE")}
                        className="dash-handle">
                        <i className="fal fa-language"></i>&nbsp;Language
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            actions={[['fa-save', 'SAVE', 'Save change(s)']]}
                            onAction={this._action}/>
                    </div>
                    <div className="row">
                        <Form
                            definition={definition}
                            allRoot={true}
                            errors={{}}
                            onChange={this._onChange}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default ViewConfig;