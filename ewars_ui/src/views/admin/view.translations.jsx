import React from "react";

import ActionGroup from '../../controls/c.actiongroup.jsx';

const ACTIONS = [
    ['fa-plus', 'NEW', 'New translation']
]

class ViewTranslations extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="row dark">
                <div className="column panel br">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">Translations</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block block-overflow">
                        <div className="dash-handle">English</div>
                        <div className="dash-handle">French</div>
                        <div className="dash-handle">Arabic</div>
                    </div>

                </div>
                <div className="column block block-overflow">

                </div>

            </div>
        )
    }
}

export default ViewTranslations;
