import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";

import DimensionTree from "../../controls/c.tree.dimensions.jsx";
import MeasureTree from "../../controls/c.tree.measures.jsx";

class ViewOutput extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column block block-overflow">

            </div>
        )
    }
}


class ViewOutputs extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row dark">
                <div className="column panel br" style={{maxWidth: "250px"}}>
                    <div className="row section" style={{maxHeight: "20px"}}>
                        Dimensions
                    </div>
                    <div className="row block block-overflow" style={{maxHeight: "50%"}}>
                        <DimensionTree/>
                    </div>
                    <div className="row section" style={{maxHeight: "20px"}}>
                        Measures
                    </div>
                    <div className="row block block-overflow">
                        <MeasureTree/>
                    </div>
                </div>
                <div className="column">

                </div>
            </div>
        )
    }
}

export default ViewOutputs;
