import React from "react";

class ViewFormSettings extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="column panel br">

                    <div className="dash-handle"><i className="fal fa-cog"></i>&nbsp;General Settings</div>
                    <div className="dash-handle"><i className="fal fa-book"></i>&nbsp;Guidance</div>
                    <div className="dash-handle"><i className="fal fa-lock"></i>&nbsp;Permissions</div>

                </div>
                <div className="column block block-overflow">

                </div>
            </div>
        )
    }
}

export default ViewFormSettings;
