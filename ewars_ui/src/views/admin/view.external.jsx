import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import DataGrid from "../../controls/datagrid/c.datagrid.jsx";

const COLUMNS = [
    {
        name: "name",
        label: "Name",
        type: "text"
    },
    {
        name: "status",
        label: "Status",
        type: "select",
        options: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive']
        ]
    },
    {
        name: "created",
        label: "Created",
        type: "date"
    },
    {
        name: "modified",
        label: "Modified",
        type: "date"
    }
];

const GRID_ACTIONS = [
    ['fa-plus', 'CREATE', 'New external source']
];

const ROW_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit data source'],
    ['fa-trash', 'DELETE', 'Delete data source']
];

class ViewExternalData extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {

    };

    _query = () => {
        return new Promise((resolve, reject) => {
            resolve({
                records: [],
                count: 0
            })
        })
    }

    render() {
        return (
            <DataGrid
                title="External Data"
                columns={[COLUMNS]}
                actions={GRID_ACTIONS}
                rowActions={ROW_ACTIONS}
                baseFiltering={{}}
                baseOrdering={{}}
                onAction={this._action}
                queryFn={this._query}/>
        )
    }
}

export default ViewExternalData;
