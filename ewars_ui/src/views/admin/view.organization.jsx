import React from "react";
import Form from "../../controls/forms/form.reporting.jsx";


const ORG_FORM = {
    name: {
        name: "name",
        type: "text",
        require: true,
        label: "Name"
    }
};

class ViewUsers extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div></div>
        )
    }
}


class ViewOrganization extends React.Component {

    state = {
        view: "DEFAULT",
        data: {}
    };

    constructor(props) {
        super(props);

        if (this.props.config.uuid) {
            window.sonomaClient.tx('com.sonoma.organization', this.props.config.uuid, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                window.state.error("An error occurred loading this organization", err);
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.config.uuid != this.props.config.uuid) {
            window.sonomaClient.tx("com.sonoma.organization", nextProps.config.uuid, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                window.state.error("An error occurred loading this resource", err);
            })
        }
    }

    render() {
        let view;

        if (this.state.view == "DEFAULT") {
            view = (
                <div className="column block block-overflow">
                    <Form
                        allRoot={true}
                        definition={ORG_FORM}
                        data={this.state.data}
                        onChange={this._onChange}
                        errors={this.state.errors || {}}/>
                </div>
            )
        }

        if (this.state.view == "USERS") {
            view = (
                <ViewUsers data={this.state.data}/>
            )
        }


        return (
            <div className="row dark">
                <div className="column block br" style={{maxWidth: "35px", overflow: "hidden", position: "relative"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-cog"></i>
                        </div>
                        <div
                            onClick={() => this._view("USERS")}
                            className={"ide-tab" + (this.state.view == "USERS" ? " ide-tab-down" : "")}>
                            <i className="fal fa-users"></i>&nbsp;Users
                        </div>
                    </div>
                </div>
                {view}
            </div>
        )
    }

}

export default ViewOrganization;