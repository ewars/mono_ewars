import React from "react";

import ViewAdminLocation from "./view.admin.location.jsx";
import ViewLocationTypeEditor from "./view.location_type.jsx";
import ActionGroup from "../../controls/c.actiongroup.jsx";

const LOC_NAME_SORT = (a, b) => {
    return (a.name.en || a.name).localeCompare(b.name.en || b.name);
}

class LocationNode extends React.Component {
    state = {
        locations: [],
        loaded: false,
        hasChildren: false,
        showChildren: false
    };

    constructor(props) {
        super(props)
    }

    _toggle = () => {
        if (!this.state.loaded) {
            this._getKids();
        } else {
            this.setState({
                showChildren: !this.state.showChildren
            })
        }
    };

    _getKids = () => {

        this._iconEl.setAttribute("class", "fal fa-cog fa-spin");
        window.sonomaClient.tx("com.sonoma.location.children", this.props.data.uuid, (res) => {
            this.setState({
                children: res.sort(LOC_NAME_SORT),
                showChildren: true,
                loaded: true
            })
        }, (err) => console.log(err));
    };

    _select = () => {
        this.props.onSelect(this.props.data);
    };

    _dragStart = (e) => {
        e.dataTransfer.setData("n", JSON.stringify(this.props.data));
    };

    _dragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("n"));

        if (data.uuid == this.props.data.uuid) {
            window.state.error("You can not move a location into itself");
            return;
        }

        window.state.dialog({
            title: "MOVE_LOCATION",
            body: "Are you sure you want to move " + (data.name.en || data.name) + " into " + (this.props.data.name.en || this.props.data.name) + "?",
            buttons: [
                'MOVE',
                "CANCEL"
            ]
        }, (res) => {
            let bl = window.state.blocker("MOVING_LOCATION");
            window.sonomaClient.tx("com.sonoma.location.move", [data.uuid, this.props.data.uuid], (res) => {
                bl.rm();
                window.state.growl("fa-save", "LOCATION_MOVED");
                window.dispatchEvent(new CustomEvent("reloadtree", {uuid: this.props.data.uuid}));
                window.dispatchEvent(new CustomEvent("reloadtree", {uuid: data.pid}));
            }, (err) => {
                bl.rm();
                window.state.error("ERROR", err);
            })
        })
    };

    render() {
        let icon = "fa-folder";
        if (this.state.showChildren) icon = "fa-folder-open";

        let style = {borderLeftColor: "transparent"};

        if (this.props.data.status == 'ACTIVE') style.borderLeftColor = "lightgreen";
        if (this.props.data.status == 'INACTIVE') style.borderLeftColor = "lightcoral";
        if (this.props.data.status == 'DISABLED') style.borderLeftColor = "lightcoral";

        let hasChildren = this.props.data.child_count > 0;

        return (
            <div className="tree-node">
                <div className="item row"
                     onDragOver={this._dragOver}
                     onDrag={this._dragStart}
                     draggable={true}
                     style={style}
                     onDrop={this._onDrop}>
                    {hasChildren ?
                        <div className="grip" onClick={this._toggle}>
                            <i ref={(el) => {
                                this._iconEl = el;
                            }} className={"fal " + icon}/>
                        </div>
                        : null}
                    <div className="column label" onClick={this._select}>
                        {this.props.data.name.en || this.props.data.name}
                    </div>
                </div>
                {this.state.showChildren ?
                    <div className="tree-children">
                        {this.state.children.map(item => {
                            return <LocationNode onSelect={this.props.onSelect} data={item}/>;
                        })}

                    </div>
                    : null}
            </div>
        )
    }

}

class ViewAdminLocations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: [],
            search: ""
        };

        window.sonomaClient.tx("com.sonoma.locations.root", [], (res) => {
            this.setState({
                locations: res
            })
        }, (err) => console.log(err));
    }

    _onSearchChange = (e) => {
        this.setState({
            search: e.target.value
        })
    };

    render() {

        let view;

        if (this.state.location) {
            view = <ViewAdminLocation data={this.state.location}/>
        }

        return (
            <div className="row dark">
                <div className="column panel br dark" style={{maxWidth: "30%"}}>
                    <div className="row block" style={{maxHeight: "55px"}}>
                        <div className="search">
                            <div className="search-inner">
                                <div className="row">
                                    <div className="column search-left" style={{maxWidth: "25px"}}>
                                        <i className="fal fa-search"></i>
                                    </div>
                                    <div className="column search-mid">
                                        <input
                                            name="search"
                                            type="text"
                                            onChange={this._onSearcChange}
                                            value="search"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row block block-overflow" style={{padding: "0 8px 8px 8px"}}>
                        {this.state.locations.map(item => {
                            return <LocationNode
                                onSelect={(loc) => {
                                    this.setState({location: loc});
                                }}
                                data={item}/>
                        })}

                    </div>
                </div>
                <div className="column">
                    {view}
                </div>
            </div>
        )
    }
}


class LocationTypeItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _select = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        return (
            <div className="dash-handle" onClick={this._select}>
                {this.props.data.name.en || this.props.data.name}
            </div>
        )
    }
}


class ViewLocationTypes extends React.Component {
    state = {
        data: [],
        item: null
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.location_types", [], (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            console.log(err);
        });
    }

    _select = (data) => {
        this.setState({
            item: data
        })
    };

    _action = () => {
        this.setState({
            item: {
                name: {en: "New Location Type"}
            }
        })
    };

    render() {
        let view;

        if (this.state.item) {
            view = <ViewLocationTypeEditor data={this.state.item}/>
        }

        return (
            <div className="row">
                <div className="column panel br">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            actions={[['fa-plus', 'ADD', 'Add location type']]}
                            onAction={this._action}/>
                    </div>
                    <div className="row block block-overflow" style={{paddingTop: "8px"}}>
                        {this.state.data.map(item => {
                            return (
                                <LocationTypeItem data={item} onSelect={this._select}/>
                            )
                        })}
                    </div>
                </div>
                <div className="column">
                    {view}
                </div>
            </div>
        )
    }
}

class GroupLocation extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        window.dispatchEvent(new CustomEvent("reloadgroup", {}));
    };

    render() {
        return (
            <div className="row">
                <div className="column">
                    {this.props.data.name.en || this.props.data.name} [{this.props.data.type_name.en || this.props.data.type_name}]
                </div>
                <ActionGroup
                    actions={[['fa-trash', 'DELETE', 'REMOVE']]}
                    onAction={this._action}/>
            </div>
        )
    }
}

const GROUP_ACTIONS = [
    ['fa-plus', 'ADD', 'ADD_LOCATION'],
    '-',
    ['fa-download', 'EXPORT', 'EXPORT'],
    '-',
    ['fa-ban', 'CLEAR', 'CLEAR'],
];

class ViewLocationGroupEditor extends React.Component {
    state = {
        locations: []
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.location_group.locations", this.props.data, (res) => {
            this.setState({
                locations: res
            })
        }, (err) => {
            window.state.error("Could not load locations for this group", err);
        })
    }

    componentWillMount() {
        window.addEventListener("reloadgroup", this._reload);
    }

    componentWillUnmount() {
        window.removeEventListener("reloadgroup", this._reload);
    }

    _reload = () => {
        window.sonomaClient.tx('com.sonoma.location_group.locations', this.props.data, (res) => {
            this.setState({
                locations: res
            })
        }, (err) => {
            window.state.error("ERROR", err);
        })
    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">{this.props.data}</div>
                    <ActionGroup
                        actions={GROUP_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">
                    {this.state.locations.map(item => {
                        return (
                            <GroupLocation data={item}></GroupLocation>
                        )
                    })}
                </div>
            </div>
        )
    }
}

class ViewLocationGroups extends React.Component {
    state = {
        data: [],
        item: null
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.location.groups", [], (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            console.log(err);
        })
    }

    _view = (data) => {
        this.setState({
            item: data
        })
    };

    render() {
        let view;

        if (this.state.item) view = <ViewLocationGroupEditor data={this.state.item}/>;

        return (
            <div className="row">
                <div className="column panel br block block-overflow" style={{paddingTop: "8px"}}>
                    {this.state.data.map(item => {
                        return (
                            <div
                                onClick={() => this._view(item)}
                                className="dash-handle">{item}</div>
                        )
                    })}

                </div>
                <div className="column">
                    {view}
                </div>
            </div>
        )
    }
}

class ViewLocationMapping extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: [],
            searchTerm: ""
        };

        window.sonomaClient.tx("com.sonoma.locations.root", [], (res) => {
            this.setState({
                locations: res
            })
        }, (err) => console.log(err));
    }

    _onSearchChange = (e) => {
        this.setState({
            searchTerm: e.target.value
        })
    };

    render() {
        return (
            <div className="row">
                <div className="column panel br dark" style={{maxWidth: "30%"}}>
                    <div className="row block" style={{maxHeight: "55px"}}>
                        <div className="search">
                            <div className="search-inner">
                                <div className="row">
                                    <div className="column search-left" style={{maxWidth: "25px"}}>
                                        <i className="fal fa-search"></i>
                                    </div>
                                    <div className="column search-mid">
                                        <input type="text" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row block block-overflow" style={{padding: "0 8px 8px 8px"}}>
                        {this.state.locations.map(item => {
                            return <LocationNode
                                onSelect={(loc) => {
                                    this.setState({location: loc});
                                }}
                                data={item}/>
                        })}

                    </div>
                </div>
                <div className="column">

                </div>
            </div>
        )
    }
}

class ViewLocations extends React.Component {
    state = {
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);
    }

    render() {
        let view;

        if (this.state.view == "DEFAULT") view = <ViewAdminLocations/>;
        if (this.state.view == "TYPES") view = <ViewLocationTypes/>;
        if (this.state.view == "GROUPS") view = <ViewLocationGroups/>;
        if (this.state.view == "MAPPING") view = <ViewLocationMapping/>;

        return (
            <div className="row dark">
                <div className="column block"
                     style={{maxWidth: "35px", width: "35px", position: 'relative', overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this.setState({view: "DEFAULT"})}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-database"></i>&nbsp;Locations
                        </div>
                        <div
                            onClick={() => this.setState({view: "TYPES"})}
                            className={"ide-tab" + (this.state.view == "TYPES" ? " ide-tab-down" : "")}>
                            <i className="fal fa-clock"></i>&nbsp;Location Types
                        </div>
                        <div
                            onClick={() => this.setState({view: "GROUPS"})}
                            className={"ide-tab" + (this.state.view == "GROUPS" ? " ide-tab-down" : "")}>
                            <i className="fal fa-map-marker"></i>&nbsp;Groups
                        </div>
                        <div
                            onClick={() => this.setState({view: "MAPPING"})}
                            className={"ide-tab" + (this.state.view == "MAPPING" ? " ide-tab-down" : "")}>
                            <i className="fal fa-comments"></i>&nbsp;Mapping
                        </div>
                    </div>

                </div>
                {view}
            </div>
        )
    }
}

export default ViewLocations;
