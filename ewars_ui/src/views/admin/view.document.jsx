import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)']
];


class ViewDocument extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let view;

        return (
            <div className="row dark">
                <div className="column br" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div className="ide-tab">
                            <i className="fal fa-cog"></i>
                        </div>
                        <div className="ide-tab">
                            <i className="fal fa-list"></i>&nbsp;Layout
                        </div>
                        <div className="ide-tab">
                            <i className="fal fa-question"></i>&nbsp;Help
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">Editor</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewDocument;
