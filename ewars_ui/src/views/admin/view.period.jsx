import React from "react";

import ActionGroup from "../../controls/c.actiongroup.jsx";
import Form from "../../controls/forms/form.reporting.jsx";

const FORM = {
    name: {
        type: "text",
        label: "Name",
        required: true,
        order: 0
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive'],
            ['ARCHIVED', 'Archived']
        ],
        order: 1
    },
    description: {
        type: "textarea",
        label: "Description",
        order: 2
    },
    type: {
        type: "select",
        label: "Period type",
        options: [
            ['CUSTOM', "Custom"],
            ['MANUAL', 'Manual']
        ],
        order: 3
    },
    interval_start: {
        type: "date",
        label: "Period anchor",
        required: true,
        conditions: [
            ['type', 'eq', 'CUSTOM']
        ],
        order: 4
    },
    interval_offset: {
        type: "number",
        label: "Offset",
        required: true,
        conditions: [
            ['type', 'eq', 'CUSTOM']
        ],
        order: 5
    },
    format: {
        type: "text",
        label: "Formatting",
        required: false,
        conditions: [
            ['type', 'eq', 'CUSTOM']
        ],
        order: 6
    }
};

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    '-',
    ['fa-trash', 'DELETE', 'Delete period']
];


const CUSTOM_ACTIONS = [
    ['fa-plus', 'ADD', 'Add interval'],
    '-',
    ['fa-ban', 'CLEAR', "Clear intervals"],
    ['fa-upload', 'IMPORT', 'Import intervals']
];


class CustomPeriodEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                    <div className="column"></div>
                    <ActionGroup
                        actions={CUSTOM_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">

                </div>
            </div>
        )
    }
}

class ViewPeriod extends React.Component {
    state = {
        view: "DEFAULT",
        data: {}
    };

    constructor(props) {
        super(props);
    }

    _action = (action) => {

    };

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _view = (view) => {
        this.setState({view});
    };

    render() {
        let view;

        if (this.state.view == "DEFAULT") {
            view = (
                <div className="row">
                    <Form
                        definition={FORM}
                        onChange={this._onChange}
                        allRoot={true}
                        data={this.state.data}/>
                </div>
            )
        }

        if (this.state.view == "CUSTOM") {
            view = (
                <CustomPeriodEditor
                    data={this.state.data}
                    onChange={this._onChange}/>
            )
        }

        return (
            <div className="row">
                <div className="column block" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-cog"></i>
                        </div>
                        <div
                            onClick={() => this._view("CUSTOM")}
                            className={"ide-tab" + (this.state.view == 'CUSTOM' ? " ide-tab-down" : "")}>
                            <i className="fal fa-pencil"></i>&nbsp;Custom
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">Period</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    {view}
                </div>
            </div>
        )
    }
}

export default ViewPeriod;