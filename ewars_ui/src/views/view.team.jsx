import React from "react";

import Moment from "moment";

class ContentItem extends React.Component {
    static defaultProps = {
        last_user: null,
        last_ts: null,
        data: {}
    };

    render() {
        let ts = Moment(this.props.data.ts * 1000, "x").local();

        let sameUser = false;
        let name;
        if (this.props.data.created_by != this.props.last_user) {
            name = (
                <strong>{this.props.data.user_name}</strong>
            )
        } else {
            name = <span>&nbsp;</span>;
            sameUser = true;
        }

        let showTime = true;
        if (this.props.last_ts && sameUser) {
            if (ts.isSame(this.props.last_ts, "hour") && ts.isSame(this.props.last_ts, "minute")) {
                ts = null;
            } else {
                ts = ts.format("HH:mm");
            }
        } else {
            ts = ts.format("HH:mm");
        }

        return (
            <div className="team-msg">
                <div className="row">
                    <div className="column date" style={{maxWidth: "30px"}}>{ts}</div>
                    <div className="column content">{name}{this.props.data.data.content}</div>
                </div>
            </div>
        )
    }
}

class EntryControl extends React.Component {
    static defaultProps = {
        value: ""
    };

    _send = () => {
        window.sonomaClient.mx("com.master.team.message", {
            tid: this.props.data.uuid,
            data: this.props.value
        }, (res) => {
            this.props.onPublish(res);
        }, (err) => console.log(err));
    };

    _onKeyPress = (e) => {
        if (e.keyCode == 13) {
            this._send();
        }
    };

    _onPublishError = () => {
        this.props.onChange("");
    };

    render() {
        return (
            <div className="team-entry-control">
                <div className="inner">

                    <div className="column br button"
                         style={{maxWidth: "30px", justifyContent: "center", cursor: "pointer"}}>
                        <i className="fal fa-plus"></i>
                    </div>
                    <div className="column input">
                        <input
                            onKeyDown={this._onKeyPress}
                            onChange={(e) => {
                                this.props.onChange(e.target.value)
                            }}
                            value={this.props.value.content}
                            type="text"/>
                    </div>

                </div>
            </div>
        )
    }
}

class ViewTeam extends React.Component {
    state = {
        backlog: [],
        message: {
            content: ""
        },
        team: null
    };

    constructor(props) {
        super(props);

        // Get information about the team
        window.sonomaClient.tx("com.sonoma.team", [this.props.config], (res) => {
            this.setState({
                team: res[0] || null
            }, () => {
                // Get the backlog of messages for this team
                window.sonomaClient.tx("com.sonoma.team.backlog", [this.props.config], (res) => {
                    this.setState({
                        backlog: res
                    })
                }, (err) => console.log(res));
            })
        }, (err) => console.log(err));
    }

    _onChange = (e) => {
        this.setState({
            message: {
                content: e
            }
        })
    };

    componentDidMount() {
        console.log("HERE1");
        if (this._scroll) {
            console.log("HERE")
            this._scroll.scrollTop = this._scroll.scrollHeight;
        }
    }

    _onPublish = (data) => {
        let backlog = this.state.backlog;
        backlog.push(data);
        this.setState({
            backlog,
            message: {
                content: ""
            }
        }, () => {
            this._scroll.scrollTop = this._scroll.scrollHeight;
        });
    };

    render() {
        if (!this.state.team) {
            return (
                <div className="row block">
                    <p>Loading...</p>
                </div>
            )
        }

        let last_user,
            last_ts;

        let items = [];

        let today = Moment();
        this.state.backlog.forEach(item => {
            let ts = Moment(item.ts * 1000, "x").local();

            if (!last_ts) {
                items.push(
                    <div className="date-line">
                        <div className="column" style={{position: "relative"}}>
                            <div className="line"></div>
                        </div>
                        <div className="column" style={{maxWidth: "100px"}}>
                            <div className="text">{ts.format("YYYY-MM-DD")}</div>
                        </div>
                        <div className="column" style={{position: "relative"}}>
                            <div className="line"></div>
                        </div>
                    </div>
                )
            } else {
                if (!ts.isSame(last_ts, "day")) {
                    items.push(
                        <div className="date-line">
                            <div className="column" style={{position: "relative"}}>
                                <div className="line"></div>
                            </div>
                            <div className="column" style={{maxWidth: "100px"}}>
                                <div className="text">{ts.format("YYYY-MM-DD")}</div>
                            </div>
                            <div className="column" style={{position: "relative"}}>
                                <div className="line"></div>
                            </div>
                        </div>
                    )
                }
            }
            items.push(
                <ContentItem
                    data={item}
                    last_ts={last_ts}
                    last_user={last_user}/>
            );
            last_ts = ts;
            last_user = item.created_by;
        })


        return (
            <div className="row">
                <div className="column br">
                    <div className="row bb" style={{maxHeight: "34px"}}>
                        <div className="column grid-title"><i className="fal fa-hash"></i>&nbsp;{this.state.team.name}
                        </div>
                    </div>

                    <div className="row block block-overflow" style={{position: "relative"}} ref={(el) => {
                        this._scroll = el;
                    }}>

                        {items}

                    </div>

                    <div className="row block block-overflow" style={{position: "relative", maxHeight: "45px"}}>
                        <EntryControl
                            data={this.state.team}
                            value={this.state.message}
                            onPublish={this._onPublish}
                            onChange={this._onChange}/>
                    </div>
                </div>
                <div className=" panel">

                </div>
            </div>
        )
    }
}

export default ViewTeam;
