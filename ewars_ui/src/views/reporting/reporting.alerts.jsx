import React from "react";

import Moment from "moment";

import ActionGroup from "../../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-eye', 'VIEW', 'VIEW_ALERT']
];

const STAGES = {
    OUTCOME: "Outcome",
    VERIFICATION: "Verification",
    RISK_CHAR: "Risk Characterization",
    RISK_ASSESS: "Risk Assessment"
}

class LocationAlert extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case "VIEW":
                window.state.addTab("ALERT", {
                    name: this.props.alarm.name.en || this.props.alarm.name,
                    alert_id: this.props.data.uuid
                })
                break;
            case "DISCARD":
                break;
            default:
                break
        }
    };

    render() {
        return (
            <div className="card-boxed">
                <div className="inner-card">
                    <div className="row block padded-3" style={{flex: 2}}>
                        <div className="dash-handle">
                            <div className="inner">
                                <div className="icon">
                                    <div className="i fal fa-calendar"></div>
                                </div>
                                <div className="label">
                                    {this.props.data.alert_date}
                                </div>
                            </div>
                        </div>
                        <div className="dash-handle">
                            <div className="inner">
                                <div className="icon">
                                    <i className="fal fa-bell"></i>
                                </div>
                                <div className="label">{STAGES[this.props.data.stage]}</div>
                            </div>
                        </div>
                        {this.props.data.eid ?
                            <div className="dash-handle">
                                <div className="inner">
                                    <div className="icon"><i className="fal fa-tag"></i></div>
                                    <div className="label">
                                        {this.props.data.eid}
                                    </div>
                                </div>
                            </div>
                            : null}
                    </div>
                    <div className="row block card-button" onClick={() => this._action("VIEW")}>
                        <i className="fal fa-eye"></i>&nbsp;View
                    </div>
                    <div className="row block card-button red" onClick={() => this._action("DISCARD")}>
                        <i className="fal fa-trash"></i>&nbsp;Discard
                    </div>
                </div>
            </div>
        )
    }
}

class LocationAlerts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        let query = {aid: this.props.data.uuid, lid: this.props.location};
        window.sonomaClient.tx("com.sonoma.location.alerts_specific", query, (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_ALERTS", err);
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data.uuid != this.props.data.uuid) {
            let query = {aid: nextProps.data.uuid, lid: nextProps.location};
            window.sonomaClient.tx("com.sonoma.location.alerts_specific", query, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                window.state.error("ERROR_LOADING_ALERTS", err);
            })
        }
    }

    render() {
        if (!this.state.data) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                </div>
            )
        }

        if (this.state.data.length <= 0) {
            return (
                <div className="row">
                    <div className="column placeholder bgd-ps-neutral"
                         style={{position: "relative"}}>
                        <div className="placeholder-icon">
                            <i className="fal fa-bell"></i>
                        </div>
                        <div className="placeholder-title">
                            No active alerts for this location
                        </div>
                        <div className="placeholder-text">
                            <p>Looks like there aren't any active alerts of this type for this location</p>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "25px", minHeight: "25px"}}>
                    <div className="column grid-title">{this.props.data.name.en || this.props.data.name}</div>
                </div>
                <div className="row block block-overflow bgd-ps-neutral" style={{padding: "16px"}}>
                    <div className="row card-box-wrapper">
                        {this.state.data.map(item => {
                            return (
                                <LocationAlert alarm={this.props.data} data={item}/>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default LocationAlerts;