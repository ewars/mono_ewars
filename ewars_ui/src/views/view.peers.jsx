import React from "react";

class Peer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="dash-handle">

            </div>
        )
    }
}

class Peers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            peers: []
        };
    }

    render() {
        return (
            <div className="row">
                <div className="column panel block block-overflow br">
                    <div className="dash-section">
                        <i className="fal fa-atom"></i>&nbsp;Canonicals
                    </div>


                    <div className="dash-section">
                        <i className="fal fa-broadcast-tower"></i>&nbsp;Relays
                    </div>

                    <div className="dash-section">
                        <i className="fal fa-user"></i>&nbsp;Peers
                    </div>


                </div>
                <div className="column block block-overflow">

                </div>
            </div>
        )
    }
}

export default Peers;
