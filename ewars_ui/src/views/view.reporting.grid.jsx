import React from 'react';

import FormUtils from "../utils/form_utils";

import DataGrid from '../controls/datagrid/c.datagrid.jsx';
import Panel from "../controls/c.panel.jsx";
import SourceData from "../controls/c.source.data.jsx";
import dialog from "../utils/dialog";

const GRID_ACTIONS = [
    ['fa-plus', 'CREATE', 'New record']
    //'-',
    //['fa-download', "EXPORT", "Export data"]
];

const ROW_ACTIONS = [
    ['fa-eye', "VIEW", "View record"],
    ['fa-trash', 'DELETE', 'Delete record']
];

class NoDataCmp extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        this.props.onHammer();
    };

    render() {
        return (
            <div className="grid-overlay">
                <div className="row">
                    <div className="column" style={{maxWidth: "300px", padding: "8px"}}>
                        <div className="row"></div>
                        <div className="row block">
                            <h3>No Data</h3>

                            <p>It look like you haven't submitted any records yet for this form. There may be data
                                available
                                online
                                to download</p>

                            <button onClick={this._onClick}>Check for data</button>
                        </div>
                        <div className="row"></div>
                    </div>
                    <div className="column"></div>
                </div>
            </div>
        )
    }
}

class NoDataWebCmp extends React.Component {
    _onClick = () => {
        this.props.onAction("CREATE")
    };

    render() {
        return (
            <div className="row">
                <div className="column"></div>
                <div className="column" style={{maxWidth: "300px", padding: "8px"}}>
                    <div className="row"></div>
                    <div className="row block" style={{textAlign: "center", }}>
                        <h3 style={{fontSize: "20px", marginBottom: "8px", display: "block"}}>No Records</h3>

                        <p>It looks like there aren't any records for this form yet, Submit one now to get started.</p>
                        <br/>

                        <button onClick={this._onClick}>Create Record</button>
                    </div>
                    <div className="row"></div>
                </div>
                <div className="column"></div>
            </div>
        )
    }
}

class ViewReportingGrid extends React.Component {
    static defaultProps = {
        lid: null,
        fid: null
    };

    state = {
        panelType: null,
        ts_ms: (new Date).getTime()
    };

    constructor(props) {
        super(props);

        if (this.props.form) this._grid = FormUtils.grid(false, false, this.props.form.definition);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.form && this.props.form) {
            if (nextProps.form.id != this.props.form.id) this._grid = FormUtils.grid(false, false, nextProps.form.definition);
        } else if (nextProps.form) {
            this._grid = FormUtils.grid(false, false, nextProps.form.definition);
        }
    }

    _query = (orders, filters, limit, offset) => {
        let endFilters = filters;
        if (this.props.lid) {
            endFilters.push({
                key: "data.__lid__",
                cmp: "eq",
                val_type: "text",
                value: this.props.lid
            });
        }

        return new Promise((resolve, reject) => {
            window.tx(
                "com.sonoma.records",
                {
                    orders,
                    filters: endFilters,
                    limit: (limit == null ? 100 : limit),
                    offset: (offset == null ? 0 : offset),
                    fid: this.props.form.uuid
                },
                (res) => {
                    console.log("RECORDS", res);
                    resolve(res);
                },
                (err) => {
                    console.log(err)
                });
        })
    };

    _action = (action, data) => {
        switch (action) {
            case "CREATE":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "RECORD",
                        form_id: this.props.form.uuid,
                        name: __(this.props.form.name),
                        uuid: null
                    }
                }));
                break;
            case "DATA":
                this.setState({
                    panel: "DATA"
                });
                break;
            case "EXPORT":
                this.setState({
                    panel: "EXPORT"
                });
                break;
            case "IMPORT":
                this.setState({
                    panel: "IMPORT"
                });
                break;
            case "PRINT":
                this.setState({
                    panel: "PRINT"
                });
                break;
            case "VIEW":
                window.dispatchEvent(new CustomEvent("ADD_TAB", {
                    detail: {
                        type: "RECORD",
                        form_id: this.props.form.uuid,
                        name: __(this.props.form.name),
                        record_id: data.uuid
                    }
                }));
                break;
            case "DELETE":
                this._deleteRecordWeb(data);
                break;
            default:
                break;

        }
    };

    _deleteRecordWeb = (data) => {
        new dialog.Dialog({
            title: "Delete record?",
            body: "Are you sure you want to delete this record?",
            buttons: [
                'Delete',
                "Cancel"
            ]
        }, (res) => {
            if (res == 0) {
                window.sonomaClient.tx("com.sonoma.record.retract", data.uuid, (result) => {
                    window.state.growl("fa-exclamation-triangle", "Record deleted");
                    window.dispatchEvent(new CustomEvent("reloadgrid"));
                }, (err) => {
                    window.state.error("An error occurred while deleting the requested record", err);
                })
            }
        })
    };

    _deleteRecord = (data) => {
        dialog.showMessageBox({
            type: "question",
            buttons: ["Delete", "Cancel"],
            title: "Delete record?",
            message: "Are you sure you want to delete this record?",
            cancelId: 1
        }, (ind) => {
            if (ind == 0) {
                window.sonomaClient.tx("com.sonoma.record.retract", [data.uuid], (res) => {
                    this.setState({
                        data: {data: {}}
                    }, () => {
                        new Notification("Record deleted", {
                            body: "The record has been deleted",
                            silent: true,
                            sound: false
                        })
                    })
                })
            }
        })
    };

    render() {
        if (!this.props.form) {
            return (
                <div className="row" style={{position: "relative"}}>
                    <div
                        style={{display: "block"}}
                        ref={(el) => this._loader = el}
                        className="loader">
                        <div className="loader-middle">
                            <i className="fal fa-cog fa-spin"></i>
                        </div>
                    </div>
                </div>
            )
        }

        let panel,
            panelMinimal = true;

        if (this.state.panel) {
            if (this.state.panel == "DATA") {
                panel = (
                    <SourceData form={this.props.form}/>
                )
            }
        }

        let NoData = NoDataCmp;

        return (
            <div className="row">
                <DataGrid
                    grid_id={this.props.form.uuid + "_GRID"}
                    icon="fa-database"
                    grid={this._grid}
                    search={true}
                    onAction={this._action}
                    NoDataCmp={NoData}
                    contextActions={[
                        ["fa-edit", "EDIT", "Edit record"],
                        '-',
                        ['fa-trash', 'DELETE', 'Delete record']
                    ]}
                    onNoDataClick={() => {
                        this._action("DATA");
                    }}
                    actions={GRID_ACTIONS}
                    rowActions={ROW_ACTIONS}
                    title={this.props.form.name.en || this.props.form.name}
                    queryFn={this._query}/>

                {panel ?
                    <Panel
                        visible={true}
                        minimal={panelMinimal}
                        onClose={() => {
                            this.setState({panel: null})
                        }}>
                        {panel}
                    </Panel>
                    : null}
            </div>
        )
    }
}

export default ViewReportingGrid;
