import React from "react";

class MulticastClient extends React.Component {
    render() {
        return (
            <div className="connection">

            </div>
        )
    }
}

class CanonClient extends React.Component {
    render() {
        return (
            <div className="connection">

            </div>
        )
    }
}

class PeerConnection extends React.Component {
    render() {
        return (
            <div className="connection">

            </div>
        )
    }
}

class DebugViewConnections extends React.Component {
    state = {
        connections: []
    };

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.debug.connections", [], (res) => {
            this.setState({
                connections: res
            })
        }, (err) => console.log(err));
    }

    render() {
        return (
            <div className="column block block-overflow">
                <MulticastClient/>
                <CanonClient/>

            </div>
        )
    }
}

export default DebugViewConnections;
