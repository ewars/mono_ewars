import React from 'react';

import utils from "../../utils/utils";
import ActionGroup from "../../controls/c.actiongroup.jsx";
import Form from "../../controls/forms/form.reporting.jsx";

const ACTIONS = [
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-times', 'CLOSE', 'Close']
];

const FORM = {
    uid: {
        type: "user",
        label: "User",
        required: true,
        root: true
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ['ACTIVE', "Active"],
            ["INACTIVE", "Inactive"]
        ],
        root: true
    },
    assign_type: {
        label: "Assignment type",
        type: "select",
        options: [
            ['UNDER', 'Multiple locations'],
            ['DEFAULT', 'Single location'],
            ['GROUP', 'Location group']
        ],
        required: true,
        root: true
    },
    lid: {
        type: "location",
        label: "Location",
        required: true,
        root: true
    }
};


class ShadeAssignment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: utils.copy(this.props.data || {})
        }

    };

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _action = (action) => {
        switch(action) {
            case "CLOSE":
                this.props.onAction();
                break;
            case "SAVE":
                break;
            default:
                break;
        }
    };

    render() {
        return (
            <div className="column dark">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Assignment</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row system-form">
                    <Form
                        definition={FORM}
                        data={this.state.data}
                        allRoot={true}
                        onChange={this._onChange}
                        errors={{}}/>
                </div>
            </div>
        )
    }
}

export default ShadeAssignment;