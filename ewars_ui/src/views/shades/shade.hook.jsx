import React from "react";

import utils from "../../utils/utils";
import ActionGroup from "../../controls/c.actiongroup.jsx";
import Form from "../../controls/forms/form.reporting.jsx";
import DEFAULT_ACTIONS from "../../constants/const.default_actions";
import DEFAULT_STATUSES from "../../constants/const.default_statuses";

const FORM = {
    name: {
        type: "text",
        label: "Hook name",
        required: true,
        order: 0
    },
    status: {
        type: "select",
        label: "Status",
        options: DEFAULT_STATUSES,
        required: true,
        order: 1
    },
    description: {
        type: "textarea",
        label: "Description",
        order: 2
    },
    endpoint: {
        type: "text",
        label: "Endpoint",
        order: 3
    },
    method: {
        type: "select",
        label: "Method",
        options: [
            ['POST', "Post"],
            ['GET', "Get"],
            ['FORM', "Form"]
        ],
        order: 4
    },
    format: {
        type: "select",
        label: "Format",
        options: [
            ['XML', "XML"],
            ['JSON', "JSON"],
            ["YAML", "YAML"],
            ['TOML', "TOML"]
        ],
        order: 5
    },
    auth_user: {
        type: "text",
        label: "Username",
        order: 6
    },
    auth_pass: {
        type: "password",
        label: "Password",
        order: 7
    },
    ref_type: {
        type: "select",
        label: "Hook target",
        options: [
            ['FORM', 'Form'],
            ['ALERT', "Alert"],
            ["LOCATION", "Location"],
            ["ORGANIZATION", "Organization"]
        ],
        order: 8
    },
    push_updates: {
        type: "buttonset",
        label: "Push updates?",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        order: 9
    },
    mapped: {
        type: "buttonset",
        label: "Mapped",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        order: 10
    },
    mapping: {
        type: "mapping",
        label: "Mapping",
        vertical: true,
        order: 11
    }
};

class ShadeHook extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: utils.copy(this.props.data || {})
        }
    }

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    _action = (action) => {
        switch(action) {
            case "CLOSE":
                this.props.onAction();
                break;
            case "SAVE":
                this._save();
                break;
            default:
                break;
        }
    };

    render() {
        return (
            <div className="column dark">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Hook</div>
                    <ActionGroup
                        actions={DEFAULT_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row system-form">
                    <Form
                        definition={FORM}
                        data={this.state.data}
                        onChange={this._onChange}
                        errors={{}}
                        allRoot={true}/>
                </div>
            </div>
        )
    }
}

export default ShadeHook;