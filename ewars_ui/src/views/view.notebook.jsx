import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";
import renderWidget from '../viz/index.js';

const ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit notebook'],
    '-',
    ['fa-plus', 'ADD', 'Add Item'],
    ['fa-font', 'ADD_TEXT', 'Add note'],
    '-',
    ['fa-trash', 'DELETE', 'Delete notebook']
];

const NODE_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit'],
    '-',
    ['fa-caret-up', 'UP', 'Move up'],
    ['fa-caret-down', 'DOWN', 'Move down'],
    '-',
    ['fa-trash', 'DELETE', 'Delete']
];


class NotebookNode extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this._widget = renderWidget(this.props.data, this._el);
    }

    render() {
        let itemTitle = "Untitled";

        return (
            <div className="notebook-node" style={{border: "1px solid #DADADA", margin: "4px 8px"}}>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column grid-title">{itemTitle}</div>
                        <ActionGroup
                            naked={true}
                            actions={NODE_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="column block" style={{minHeight: "30px"}} ref={(el) => this._el = el}>

                    </div>
                </div>
            </div>
        )
    }
}

class NotebookView extends React.Component {
    state = {
        data: {}
    };

    constructor(props) {
        super(props);

        if (this.props.config.uuid) {
            window.sonomaClient.tx("com.sonoma.resource", this.props.config.uuid, (res) =>{
                this.setState({
                    data: res
                })
            }, (err) => {
                console.log(err);
            })
        }
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.config.uuid) {
            window.sonomaClient.tx("com.sonoma.resource", nextProps.config.uuid, (res) => {
                this.setState({
                    data: res
                })
            }, (err) => console.log(err));
        } else {
            this.setState({
                data: {}
            })
        }
    }

    _action = (action) => {

    };

    render() {

        let notebookName;
        if (this.state.data.name) notebookName = this.state.data.name.en || this.state.data.name;

        console.log(this.state.data);

        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: '34px'}}>
                    <div className="column grid-title">{notebookName}</div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">
                    {(this.state.data.definition || []).map(item => {
                        return (
                            <NotebookNode data={item}/>
                        )
                    })}

                </div>
            </div>
        )
    }
}

export default NotebookView;