import aQueue from '../../api';

const TITLE_POSITIONS = {
    'TL': [10, 10],
    'TC': [],
    'TR': [],
    'ML': [],
    'MR': [],
    'BL': [],
    'BC': [],
    'BR': [],
};

const SUBTITLE_POSITIONS = {
    TL: [],
    TC: [],
    TR: [],
    ML: [],
    MR: [],
    BL: [],
    BC: [],
    BR: []
};


const parseValueMapping = (mapping, value) => {

};

const parseColourMapping = (mapping, value) => {

};

const _f = (val, alternate) => {
    if (val == undefined || val == '' || val == null) return alternate;
    return val;
};

const createNode = (className, text) => {
    let domNode = document.createElement('div');
    if (className) domNode.setAttribute('class', className);
    if (text) domNode.appendChild(document.createTextNode(text));

    return domNode;
}

class Default {
    constructor(props, data, el) {
        this._config = props;
        this._data = data;
        this._el = el;

        this._title;
        this._subTitle;
        this._value;
        this._root;

        this.render();
    }

    render = () => {
        if (!this._config.n || this._config.n.length <= 0) {
            return;
        }
        aQueue.get(this._config.n, [])
            .then(resp => {
                this._renderHtml(resp);
            });
    };
    
    _setPos = (node, pos) => {
        
        switch (pos) {
            case 'TL':
                node.style.textAlign = 'left';
                node.style.top = 10;
                break;
            case 'TC':
                node.style.textAlign = 'center';
                node.style.top = 10;
                break;
            case 'TR':
                node.style.textAlign = 'right';
                node.style.top = 10;
                break;
            case 'ML':
                node.style.top = '50%';
                node.style.textAlign = 'left';
                break;
            case 'MR':
                node.style.top = '50%';
                node.style.textAlign = 'right';
                break;
            case 'BL':
                node.style.bottom = 10;
                node.style.top = undefined;
                node.style.textAlign = 'left';
                break;
            case 'BC':
                node.style.bottom = 10;
                node.style.top = undefined;
                node.style.textAlign = 'center';
                break;
            case 'BR':
                node.style.bottom = 10;
                node.style.top = undefined;
                node.style.textAlign = 'right';
                break;
            default:
                node.style.textAlign = 'center';
                node.style.top = 10;
                break;
        }

        return node;

    };

    _renderHtml = (data) => {
        console.log(this._config);
        let rootDOMNode = createNode('plot-point');
        rootDOMNode.style.position = 'relative';
        rootDOMNode.style.background = _f(this._config.backgroundColor, 'transparent');
        rootDOMNode.style.paddingTop = _f(this._config.paddingTop, '0') + 'px';
        rootDOMNode.style.paddingRight = _f(this._config.paddingRight, '0') + 'px';
        rootDOMNode.style.paddingBottom = _f(this._config.paddingBottom, '0') + 'px';
        rootDOMNode.style.paddingLeft = _f(this._config.paddingLeft, '0') + 'px';
        rootDOMNode.style.marginTop = _f(this._config.marginTop, '0') + 'px';
        rootDOMNode.style.marginRight = _f(this._config.marginRight, '0') + 'px';
        rootDOMNode.style.marginBottom = _f(this._config.marginBottom, '0') + 'px';
        rootDOMNode.style.marginleft = _f(this._config.marginLeft, '0') + 'px';
        this._root = rootDOMNode;

        if (this._config.border) {
            rootDOMNode.style.border = (_f(this._config.borderWidth, '1') + 'px') + ' solid ';
            rootDOMNode.style.border += _f(this._config.borderColor, '#333333')
        }
        rootDOMNode.style.borderRadius = _f(this._config.borderRadius, '0');


        if (this._config.title) {
            let titleDOM = document.createElement('div');
            titleDOM.appendChild(document.createTextNode(this._config.titleText));
            titleDOM.setAttribute('class', 'plot-title');
            titleDOM.style.position = 'absolute';
            titleDOM.style.width = '100%';
            titleDOM.style.padding = '3px';
            titleDOM.style.left = 0;
            titleDOM.style.fontWeight = _f(this._config.titleWeight, 'normal');
            titleDOM.style.fontSize = _f(this._config.titleSize, '12') + 'px';
            titleDOM.style.color = _f(this._config.titleColor, '#333333');

            titleDOM = this._setPos(titleDOM, this._config.titlePos);

            rootDOMNode.appendChild(titleDOM);
            this._title = titleDOM;
        }

        if (this._config.subTitle) {
            let sNode = document.createElement('div');
            sNode.appendChild(document.createTextNode(this._config.sTitleText));
            sNode.setAttribute('class', 'plot-title');
            sNode.style.position = 'absolute';
            sNode.style.width = '100%';
            sNode.style.padding ='3px';
            sNode.style.left = 0;
            sNode.style.fontWeight = _f(this._config.sTitleWeight, 'normal');
            sNode.style.fontSize = _f(this._config.sTitleSize, '12') + 'px';
            sNode.style.color = _f(this._config.sTitleColor, '#333333');

            sNode = this._setPos(sNode, _f(this._config.sTitlePos, 'BC'));

            rootDOMNode.appendChild(sNode);
            this._subTitle = sNode;
        }

        let innerDOMNode = createNode('plot-point-inner');
        rootDOMNode.appendChild(innerDOMNode);

        let valueDOMNode = createNode('plot-point-value');
        this._value = valueDOMNode;

        let value = 0;
        if (data.d.length > 0) {
            value = data.d[0][0];
        }
        if (this._config.valueFormat) {
            value = ewars.NUM(value, this._config.valueFormat);
        }

        if (this._config.prefix) value = this._config.prefix + ' ' + value;
        if (this._config.suffix) value += ' ' + this._config.suffix;
        valueDOMNode.appendChild(document.createTextNode(value));

        valueDOMNode.style.color = _f(this._config.color, '#333333');
        valueDOMNode.style.fontWeight = _f(this._config.fontWeight, 'normal');
        valueDOMNode.style.fontSize = _f(this._config.fontSize, '16') + 'px';
        innerDOMNode.appendChild(valueDOMNode);
        // formatting
        // Value mapping
        // Colour mapping
        // font size

        // title
        // scale
        // sub-title
        // prefix
        // suffix


        this._el.appendChild(rootDOMNode);
    };

    update = (data) => {
        console.log(data);
    };

    destroy = () => {

    }
}

export default Default;