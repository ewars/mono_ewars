import AnalysisUtils from "../utils/analysis_utils";

// import Source from './source';
// import Config from './config';

// import Annotation from './features/annotation.jsx';

// import PieChart from './chart.pie/index.jsx';
// import DefaultChart from './chart.default/index.jsx';
// import PivotTable from './table.pivot/index.jsx';
// import TableDefault from './table.default/index.jsx';
// import DefaultMap from './map.default/index.jsx';
// import HeatMap from './map.heat/index.jsx';
// import Default from './default/index.jsx';
// import Gauge from './other.gauge/index.jsx';
// import Sparkline from './chart.spark/index.jsx';
// import Candlestick from './chart.candle/index.jsx';
// import StreamGraph from './other.stream/index.jsx';
// import FunnelChart from './other.funnel/index.jsx';
// import BulletChart from './chart.bullet/index.jsx';
// import TreeMap from './map.tree/index.jsx';
// import XRange from './chart.xrange/index.jsx';
// import PyramidChart from './other.pyramid/index.jsx';

// import OtherHTML from './other.html/index.jsx';

const renderWidget = (config, el) => {
    console.log(config);
    let _config = config;
    if (!config.g && !config.n) {
        // This is a legacy node, we need to upgrade it
        _config = AnalysisUtils.upgradeWidget(null, _config);
    }
    console.log(_config);

};

export default renderWidget;
