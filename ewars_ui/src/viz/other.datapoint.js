import Base from "./base";

class DataPoint {
    constructor(props, el) {
        console.log(props);
        this.data = props;
        this.el = el;

        let nodes = this.data.n;

        window.sonomaClient.tx("com.sonoma.plot.point", [nodes], (res) => {
            if (res.error) {

            } else {
                this._render(res);
            }

        }, (err) => {
            console.log(err);
        })
    }

    _render = (value) => {
        this.el.innerHTML = "";
        this.el.appendChild(document.createTextNode(value));
    };
}

export default DataPoint;