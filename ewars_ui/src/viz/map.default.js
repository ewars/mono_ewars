import Source from './source';
import Moment from 'moment';

const d3 = require('d3');
import turf from 'turf';
import turfCombine from '@turf/combine';

import Base from './base';
import CONTROLS from './features';
import Highcharts from "highcharts";

Highcharts.setOptions({
    animation: false,
    chart: {
        animation: false
    }
});


const _getData = (data, filters, axis) => {
    return new Promise((resolve, reject) => {
        ewars.tx("com.ewars.plot", [data, filters, axis])
            .then((resp) => {
                resolve(resp);
            })
            .catch(err => {
                reject(err);
            })
    })
};


const _buildScale = (scale) => {
    let scaler = [];

    let lastStep;
    for (let i = 1; i <= scale.steps; i++) {
        let step = [0, 0, null];
        if (i == 0) {
            step[1] = scale.rangeTree[i];
            step[2] = parseFloat(scale.rangeColours[i]);
            lastStep = step[1];
            scaler.push(step);
        } else if (i >= scale.steps) {
            step[0] = lastStep;
            step[1] = 'INF';
            step[2] = scale.rangeColours[i];
            scaler.push(step);
        } else {
            step[0] = lastStep;
            step[1] = parseFloat(scale.rangeTree[i]);
            step[2] = scale.rangeColours[i];
            lastStep = step[1];
            scaler.push(step);
        }
    }

    scaler[0][0] = 0;

    return scaler;
};


class DefaultMap extends Base {
    constructor(props, data, el) {
        super(props, data, el);
        this.render();

        // Has the map been snapped in a pre-render control? If so, don't snap in the control
        this.snapped = false;
    }

    redraw = () => {
        // let [height, width] = this._getDims();
        // this.svg
        //     .attr("viewBox", `0 0 ${width} ${height}`)
        //     .attr("preserveAspectRatio", "xMidYMid meet");
    };

    render = () => {
        // Create two sets of groups, where the data is grouped together along the same axis
        let definition = this._config.n.filter(node => {
            return (
                node.t != 'A'
                && node.t != 'X'
            )
        });

        aQueue.get(definition, [], {loadGeom: true})
            .then(resp => {
                this._data = resp;
                return this._setupBaseMap();
            })
            .catch(err => {
                console.log(err);
            })
            .then(() => {
                return this._processPreControls();
            })
            .catch(err => {
                console.log(err);
            })
            .then(() => {
                return this._finalize();
            })
            .catch(err => {
                // Catch any errors on the client-side
                console.log(err);
            })
            .then(() => {
                return this._processPostControls();
            })
            .catch(err => {
                console.log(err)
            })

    };

    _getColor = (item) => {
        let measure = this._config.n.filter(node => {
            return ['M', 'C'].indexOf(node.t) && !node.p;
        })[0];

        let color = measure.color || 'white';

        let scale = this._config.n.filter(node => {
            return node.t == 'X' && node._t == 'COL_MAP';
        })[0] || null;

        if (scale) {
            let scaler = _buildScale(scale);

            scaler.forEach((range, index) => {
                if (index == 0) {
                    if (item.value == null || item.value == undefined) color = range[2];
                    if (item.value >= -Infinity && item.value <= range[1]) color = range[2];
                } else if (range[1] == 'INF') {
                    if (item.value > range[0]) color = range[2];
                } else {
                    if (item.value > range[0] && item.value <= range[1]) color = range[2];
                }
            })
        }
        return color;
    };

    /**
     * Sets up the initial map
     * @returns {Promise}
     * @private
     */
    _setupBaseMap = () => {
        return new Promise((resolve, reject) => {
            this._icon.style.display = 'none';

            let [height, width] = this._getDims();

            this.svg = d3.select(this._el).append('svg')
                .attr("viewBox", `0 0 ${width} ${height}`)
                .attr("preserveAspectRatio", "xMidYMid meet");

            this.projection = d3.geoMercator()
                .scale(1)
                .translate([0, 0]);

            this.path = d3.geoPath()
                .projection(this.projection);

            let bgd = this.svg.append('g');
            bgd.append('rect')
                .attr('class', 'background')
                .style('fill', this._config.backgroundColor || 'transparent')
                .attr('width', width)
                .attr('height', height);

            this.validLocs = [];
            this.validPointLocs = [];

            let locDim = this._config.n.filter(node => {
                return node.n == 'LOCATION' && node.t == 'D';
            })[0];

            // Get out valid geometries
            for (let i in this._data.m[locDim._].dict) {
                if (this._data.m[locDim._].dict[i].geometry != null) {
                    this.validLocs.push(this._data.m[locDim._].dict[i])
                }
            }


            // this.base.append('path')
            //     .datum(combined)
            //     .attr('d', this.path);

            resolve();
        })
    };

    /**
     * Process controls which need to be rendered before the main data
     * @returns {Promise}
     * @private
     */
    _processPreControls = () => {
        return new Promise((resolve, reject) => {
            // need to process static map layers first
            let layers = this._config.n.filter(node => {
                return node.t == 'X' && node._t == 'MAP_LAYER';
            })

            let loaded = 0;

            // We need to load additional data from the API for this map,
            // so we have to halt on here while we load up and draw the initial layers for the map
            if (layers.length > 0) {
                layers.forEach(layer => {
                    this._processMapLayer(layer)
                        .then(() => {
                            loaded++;
                            console.log(loaded, layers.length);
                            if (loaded >= layers.length) resolve();
                        })

                })
            }
        });
    };

    _getDims = () => {

        let height = this._el.clientHeight;
        let width = this._el.clientWidth;

        if (this._config.marginTop) height -= parseInt(this._config.marginTop);
        if (this._config.marginBottom) height -= parseInt(this._config.marginBottom);
        if (this._config.marginLeft) width -= parseInt(this._config.marginLeft);
        if (this._config.marginRight) width -= parseInt(this._config.marginRight);

        return [height, width];
    };

    _processMapLayer = (layer) => {
        return new Promise((resolve, reject) => {
            let spec = {
                spec: layer.spec,
                parent: layer.parent || null,
                sti: layer.sti || null,
                groups: layer.groups || null
            };
            ewars.tx('com.ewars.locations.geometry', [spec])
                .then(resp => {

                    if (typeof resp.geojson == 'string' || resp.geojson instanceof String) resp.geojson = JSON.parse(resp.geojson);


                    let [height, width] = this._getDims();


                    if (layer.snapTo) {
                        this.snapped = true;
                        let bbox = turf.bbox(resp.geojson);

                        let b = this.path.bounds(resp.geojson),
                            s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height),
                            center = this.path.centroid(resp.geojson),
                            t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];

                        this.projection.scale(s)
                            .translate(t);

                        this.path = d3.geoPath()
                            .projection(this.projection);

                    }

                    let targetLayer = this.svg.append('g');
                    targetLayer.append('path')
                        .datum(resp.geojson)
                        .attr('d', this.path)
                        .attr('class', 'mesh')
                        .style('fill-opacity', layer.opacity || 1)
                        .style('fill', layer.fill || 'white')
                        .style('stroke', layer.stroke || 'rgba(0,0,0,0.1)')
                        .style('stroke-width', layer.strokeWidth || 1);


                    resolve()
                })
        })
    };

    /**
     * Draw the main data for the map
     * @returns {Promise}
     * @private
     */
    _finalize = () => {
        return new Promise((resolve, reject) => {

            if (!this.snapped) {
                let geoms = {
                    type: 'FeatureCollection',
                    features: []
                };

                this.validLocs.map(item => {
                    let geom;
                    if (typeof item.geometry == 'string' || item.geometry instanceof String) {
                        geom = JSON.parse(item.geometry);
                    } else {
                        geom = item.geometry;
                    }

                    if (geom) {
                        if (geom.type == 'FeatureCollection') {
                            if (geom.features) {
                                geom.features.forEach(feature => {
                                    geoms.features.push(feature)
                                })
                            }
                        } else if (geom.type == 'Point') {
                            geoms.features.push({type: 'Feature', geometry: geom});
                        }
                    }
                });
                let combined = turfCombine(geoms);

                let b = this.path.bounds(combined),
                    s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height),
                    center = this.path.centroid(combined),
                    t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];

                this.projection.scale(s)
                    .translate(t);
            }

            let chloros = this.svg.append('g');
            let points = this.svg.append('g');
            let self = this;

            let pointLocations = [],
                geomLocations = [];

            this.validLocs.forEach(item => {
                if (typeof item.geometry == 'string' || item.geometry instanceof String) item.geometry = JSON.parse(item.geometry);
            });

            pointLocations = this.validLocs.filter(item => {
                return item.geometry.type == 'Point';
            });
            pointLocations = pointLocations.filter(item => {
                return (item.geometry.coordinates[0] != 0 && item.geometry.coordinates[1] != 0);
            });

            geomLocations = this.validLocs.filter(item => {
                return item.geometry.type == 'FeatureCollection' && (item.geometry.features || []).length > 0;
            });

            let dimIndex = this._config.n.filter(node => {
                return node.t == 'D' && !node.p;
            })[0];
            dimIndex = this._data.i[dimIndex._];

            let meas = this._config.n.filter(node => {
                return ['M', 'C'].indexOf(node.t) >= 0 && !node.p;
            })[0];
            let measIndex = this._data.i[meas._];

            pointLocations.forEach(node => {
                this._data.d.forEach(dp => {
                    if (dp[dimIndex] == node.uuid) node.value = dp[measIndex];
                })
            });

            geomLocations.forEach(node => {
                this._data.d.forEach(dp => {
                    if (dp[dimIndex] == node.uuid) node.value = dp[measIndex];
                })
            });

            if (geomLocations.length > 0) {
                chloros.selectAll('path')
                    .data(geomLocations)
                    .enter()
                    .append('path')
                    .attr('d', (d) => {
                        return this.path(d.geometry)
                    })
                    .attr('id', function (d) {
                        return d.uuid;
                    })
                    .attr('class', 'mesh')
                    .style('fill-opacity', 0.8)
                    .style('fill', this._getColor)
                    .style('stroke', 'rgba(0,0,0,0.1)')
                    .style('stroke-width', 1);
            }

            if (pointLocations.length > 0) {
                points.selectAll('circle')
                    .data(pointLocations)
                    .enter()
                    .append('circle')
                    .attr('r', 5)
                    .attr('transform', (d) => {
                        let geo = d.geometry || '{}';
                        return "translate(" + this.projection([geo.coordinates[0], geo.coordinates[1]]) + ")";
                    })
                    .style('fill', this._getColor);
            }

            if (this._config.showLabels) {
                let labels = this.svg.append('g');

                if (!this._config.labelStyle || this._config.labelStyle == 'DEFAULT') {

                    labels.selectAll("text")
                        .data(geomLocations)
                        .enter()
                        .append("text")
                        .style("text-anchor", "middle")
                        .style("fill", this._config.labelColor || "#000")
                        .style("font-size", this._config.labelSize || 12)
                        .style("text-align", "center")
                        .attr("x", (d) => {
                            return this.path.centroid(d.geometry)[0];
                        })
                        .attr("y", (d) => {
                            return this.path.centroid(d.geometry)[1];
                        })
                        .text(function (d) {
                            return d.name;
                        });

                    let pLabels = this.svg.append('g');
                    pLabels.selectAll('text')
                        .data(pointLocations)
                        .enter()
                        .append('text')
                        .style('text-anchor', (d) => {
                            return d.geometry.coordinates[0] > -1 ? "start" : "end";
                        })
                        .style('fill', this._config.labelColor || '#000')
                        .attr('transform', (d) => {
                            return "translate(" + this.projection(d.geometry.coordinates) + ")";
                        })
                        .attr('dy', '.35em')
                        .style('font-size', '11px')
                        .text(function (d) {
                            return d.name;
                        })

                    pLabels.selectAll('text')
                        .attr('x', (d) => {
                            return d.geometry.coordinates[0] > -1 ? 6 : -6;
                        })
                        .style('text-anchor', (d) => {
                            return d.geometry.coordinates[0] > -1 ? "start" : "end";
                        })
                }

                if (this._config.labelStyle == 'NUMBERED') {
                    let labels = this.svg.append("g");
                    labels.selectAll("text")
                        .data(geomLocations)
                        .enter()
                        .append("text")
                        .style("text-anchor", "middle")
                        .style("fill", this._config.labelColor || "#000")
                        .style("font-size", this._config.labelSize || 10)
                        .style("text-align", "center")
                        .attr("x", (d) => {
                            return this.path.centroid(d.geometry || "{}")[0];
                        })
                        .attr("y", (d) => {
                            return this.path.centroid(d.geometry || "{}")[1];
                        })
                        .text(function (d, i) {
                            return i + 1;
                        });

                    let pLabels = this.svg.append('g');
                    pLabels.selectAll('text')
                        .data(pointLocations)
                        .enter()
                        .append('text')
                        .style('text-anchor', (d) => {
                            return d.geometry.coordinates[0] > -1 ? "start" : "end";
                        })
                        .style('fill', this._config.labelColor || '#000')
                        .style('font-size', this._config.labelSize || 10)
                        .attr('transform', (d) => {
                            return "translate(" + this.projection(d.geometry.coordinates) + ")";
                        })
                        .attr('dy', '.35em')
                        .text(function (d, i) {
                            return 'P' + (i++);
                        })

                    pLabels.selectAll('text')
                        .attr('x', (d) => {
                            return d.geometry.coordinates[0] > -1 ? 6 : -6;
                        })
                        .style('text-anchor', (d) => {
                            return d.geometry.coordinates[0] > -1 ? "start" : "end";
                        })
                }
            }

            resolve();
        })
    };

    /**
     * Process data controls that need to processed after the data is rendered
     * scales, legends, etc...
     * @returns {Promise}
     * @private
     */
    _processPostControls = () => {
        console.log('here')
        return new Promise((resolve, reject) => {

            let controls = this._config.n.filter(node => {
                return node.t == 'X' && node._t != 'MAP_LAYER';
            });

            console.log(controls)

            controls.forEach(control => {
                if (control._t == 'SCALE') {
                    this._buildScale(control);
                }
            })

            console.log(controls);

            resolve();
        })
    };

    /**
     * Builds a color scale for display on the map
     * @param scale
     * @private
     */
    _buildScale = (scale) => {
        // get the colour range
        let range = this._config.n.filter(n => {
            if (n.t == 'X' && n._t == 'COL_MAP') return true;
        })[0];

        let scaleDef = _buildScale(range);

        let domRoot = document.createElement('div');
        if (scale.orientation == 'H') {
            domRoot.setAttribute('class', 'legend');
        } else {
            domRoot.setAttribute('class', 'legend-v');
        }
        domRoot.style.zIndex = 999;
        domRoot.style.position = 'absolute';

        const OFFSETS = {
            TL: [15, null, null, 15],
            TC: [15, null, null, '50%'],
            TR: [15, 15, null, null],
            ML: ['50%', null, null, 15],
            MR: ['50%', 15, null, null],
            BL: [null, null, 15, 15],
            BC: [null, null, 15, '50%'],
            BR: [null, 15, 15, null]
        }

        switch (scale.position) {
            case 'TL':
                domRoot.style.left = '15px';
                domRoot.style.top = '15px';
                break;
            case 'TC':
                domRoot.style.left = '50%';
                domRoot.style.top = '15px';
                break;
            case 'TR':
                domRoot.style.right = '15px';
                domRoot.style.top = '15px';
                break;
            case 'ML':
                domRoot.style.left = '15px';
                domRoot.style.top = '50%';
                break;
            case 'MR':
                domRoot.style.right = '15px';
                domRoot.style.top = '50%';
                break;
            case 'BL':
                domRoot.style.left = '15px';
                domRoot.style.bottom = '15px';
                break;
            case 'BC':
                domRoot.style.left = '50%';
                domRoot.style.bottom = '15px';
                break;
            case 'BR':
                domRoot.style.right = '15px';
                domRoot.style.bottom = '15px';
                break;
            default:
                domRoot.style.right = '15px';
                domRoot.style.bottom = '15px';
                break;
        }

        // Add 0
        let scaleInner = document.createElement('div');
        scaleInner.setAttribute('class', 'legend-inner');
        domRoot.appendChild(scaleInner);

        let startDOMNode = document.createElement('div');
        startDOMNode.setAttribute('class', 'tick');
        let startDOMNodeInner = document.createElement('div');
        startDOMNodeInner.setAttribute('class', 'tick-inner');
        startDOMNodeInner.appendChild(document.createTextNode('0'));
        startDOMNode.appendChild(startDOMNodeInner);
        scaleInner.appendChild(startDOMNode);

        for (let i = 1; i <= range.steps; i++) {
            let itemDOM = document.createElement('div');
            itemDOM.setAttribute('class', 'legend-item');
            itemDOM.style.backgroundColor = range.rangeColours[i];
            scaleInner.appendChild(itemDOM);

            if (i < range.steps) {
                let itemTickNode = document.createElement('div');
                itemTickNode.setAttribute('class', 'tick');
                let itemTickNodeInner = document.createElement('div');
                itemTickNodeInner.setAttribute('class', 'tick-inner');
                itemTickNodeInner.appendChild(document.createTextNode(range.rangeTree[i]))
                itemTickNode.appendChild(itemTickNodeInner);
                scaleInner.appendChild(itemTickNode);
            }
        }

        // Add ∞
        let endTick = document.createElement('div');
        endTick.setAttribute('class', 'tick');
        let endTickInner = document.createElement('div');
        endTickInner.setAttribute('class', 'tick-inner');
        endTickInner.appendChild(document.createTextNode('∞'));
        endTick.appendChild(endTickInner);
        scaleInner.appendChild(endTick);


        this._el.appendChild(domRoot);

    };

    /**
     * Builds a legend to display on the map
     * @param legend
     * @private
     */
    _buildLegend = (legend) => {

    };

    update = (data) => {
        this._config = data;
        this.render();
    };

    getData = () => {

    };

    destroy = () => {

    };
}

export default DefaultMap;
