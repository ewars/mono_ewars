class OtherHTML {
    constructor(props, data, el) {
        this.props = props;
        this._el = el;
        this._render();
    }

    _render() {
        this._el.innerHTML = this.props.html;
    }
};

export default OtherHTML;