class ControlNavigator {
    constructor(chartOptions, props) {
        this.options = chartOptions;

        this.options.plotOptions = {series: {showInNavigator: true}};
        this.options.navigator = {adaptToUpdatedData: false};
    }

    getOptions = () => {
        return this.options;
    }
}

export default ControlNavigator;