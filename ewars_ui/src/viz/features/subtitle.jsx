class ControlSubTitle {
    constructor(chartOptions, props) {
        this.options = chartOptions;

        this.options.chart.title = {
            text: props.title
        }
    }


    getOptions = () => {
        return this.options;
    }
}

export default ControlSubTitle;