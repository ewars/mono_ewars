import ControlNavigator from './navigator.jsx';
import ControlTitle from './title.jsx';
import ControlSubTitle from './subtitle.jsx'

export default {
    NAVIGATOR: ControlNavigator,
    TITLE: ControlTitle,
    SUBTITLE: ControlSubTitle
}

