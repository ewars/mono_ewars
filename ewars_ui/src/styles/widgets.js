const CARD = {
    CARD: {
        background: "#FFFFFF",
        width: "100%",
        border: "1px solid rgba(0,0,0,0.1)"
    },
    CARD_HEADER: {
        padding: "8px",
        marginBottom: "8px",
        display: "flex"
    },
    CARD_LINK: {
        paddingLeft: "8px",
        color: "#4175f7"
    },
    CARD_TITLE: {
        fontSize: "1.2rem",
        color: "#333333",
    },
    CARD_BODY: {
        padding: "8px"
    },
    CARD_WRAPPER: {
        margin: "8px 8px 8px 0"
    }
}

export default CARD;
