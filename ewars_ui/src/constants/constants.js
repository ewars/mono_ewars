module.exports = {
    // User Types
    SUPER_ADMIN: "SUPER_ADMIN",
    INSTANCE_ADMIN: "INSTANCE_ADMIN",
    GLOBAL_ADMIN: "GLOBAL_ADMIN",
    ACCOUNT_ADMIN: "ACCOUNT_ADMIN",
    USER: "USER",
    ORG_ADMIN: "ORG_ADMIN",
    LAB_USER: "LAB_USER",
    REGIONAL_ADMIN: "REGIONAL_ADMIN",
    COUNTRY_SUPERVISOR: "ACCOUNT_ADMIN",
    BOT: "BOT",
    API_USER: "API_USER",

    // General states
    DISABLED: "DISABLED",

    // Reporting intervals
    NONE: "NONE",
    DAY: "DAY",
    WEEK: "WEEK",
    MONTH: "MONTH",
    YEAR: "YEAR",

    // Filterable time periods
    ONEM: "1M",
    THREEM: "3M",
    SIXM: "6M",
    ONEY: "1Y",
    YTD: "YTD",
    ALL: "ALL",

    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    DRAFT: "DRAFT",
    DELETED: "DELETED",

    DEFAULT: "DEFAULT",

    DESC: "DESC",
    ASC: "ASC",

    FORWARD: "FORWARD",
    BACKWARD: "BACKWARD",

    // Icons
    ICO_DASHBOARD: "fal fa-tachometer",
    ICO_USERS: "fal fa-users",
    ICO_USER: "fal fa-user",
    ICO_REPORT: "fal fa-file",
    ICO_REPORTS: "fal fa-clipboard",
    ICO_GRAPH: "fal fa-chart-bar",
    ICO_INVESTIGATION: "fal fa-search",
    ICO_RELATED: "fal fa-link",
    ICO_GEAR: "fal fa-gear",
    ICO_CARET_DOWN: "fal fa-caret-down",
    ICO_SEND: "fal fa-envelope",
    ICO_SUBMIT: "fal fa-senf",
    ICO_SAVE: "fal fa-save",
    ICO_CANCEL: "fal fa-times",

    // Chart Types
    SERIES: "SERIES",
    BAR: "BAR",
    MAP: "MAP",
    PIE: "PIE",
    SCATTER: "SCATTER",

    // Indicator types
    CUSTOM: "CUSTOM",

    OPEN: "OPEN",
    CLOSED: "CLOSED",

    // Form types
    PRIMARY: "PRIMARY",
    INVESTIGATIVE: "INVESTIGATIVE",
    LAB: "LAB",
    SURVEY: "SURVEY",

    // Alert stages
    VERIFICATION: "VERIFICATION",
    RISK_ASSESS: "RISK_ASSESS",
    RISK_CHAR: "RISK_CHAR",
    OUTCOME: "OUTCOME",

    // OUtcomes

    // Stage states
    DISCARD: "DISCARD",
    MONITOR: "MONITOR",
    ADVANCE: "ADVANCE",
    COMPLETED: "COMPLETED",
    PENDING: "PENDING",
    RESPOND: "RESPOND",
    DISCARDED: "DISCARDED",
    AUTODISCARDED: "AUTODISCARDED",

    // Risk levels
    LOW: "LOW",
    MODERATE: "MODERATE",
    HIGH: "HIGH",
    SEVERE: "SEVERE",

    // Colors
    RED: "red",
    GREEN: "green",
    ORANGE: "orange",
    YELLOW: "yellow",

    // Default Formats
    DEFAULT_DATE: "YYYY-MM-DD",

    // Dates
    MONTHS_LONG: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    MONTHS_SHORT: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    CAL_DAYS_LABELS: ["Sun", "Mon", "Tue", "Wed", "Thurs", "Fri", "Sat"],
    CAL_DAYS_IN_MONTH: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
};