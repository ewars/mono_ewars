const TASK = {
    title: "Organization request",
    body: "A user has requested the creation of a new organization",
    form: [
        {
            name: "name",
            label: "Name",
            type: "text",
            required: true
        }
    ],
    buttons: [
        {
            label: "Approve",
            icon: "fa-check"
        },
        {
            label: "Reject",
            icon: "fa-times"
        }
    ],
    outcomes: [
        (data) => {

        },
        (data) =>{

        }
    ]
};

export default TASK;