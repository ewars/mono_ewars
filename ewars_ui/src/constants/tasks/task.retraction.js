const TASK = {
    title: "Deletion request",
    body: "A user has requested the deletion of a record which requires approval",
    form: [
        {
            name: "uuid",
            label: "Record",
            type: "record_preview"
        }
    ],
    buttons: [
        {
            label: "Approve",
            icon: "fa-check"
        },
        {
            label: "Reject",
            icon: "fa-times"
        }
    ],
    outcomes: [
        (data) => {

        },
        (data) =>{

        }
    ]
};

export default TASK;