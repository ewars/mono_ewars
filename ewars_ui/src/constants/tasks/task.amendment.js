const TASK = {
    title: "Amendment request",
    body: "A user has requested an amendment for a record that requires approval",
    form: [
        {
            name: "form_name",
            label: "Form",
            type: "display"
        },
        {
            name: "diff",
            label: "Changeset",
            type: "diff",
            required: true,
        }
    ],
    buttons: [
        {
            label: "Approve",
            icon: "fa-check"
        },
        {
            label: "Reject",
            icon: "fa-times"
        }
    ],
    outcomes: [
        (data) => {

        },
        (data) =>{

        }
    ]
};

export default TASK;