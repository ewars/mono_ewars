const TASK = {
    title: "New location request",
    body: "A user has requested the creation of a new location, please review the details below and take an appropriate action:",
    form: [
        {
            name: "name",
            label: "Location name",
            type: "text",
            required: true
        },
        {
            name: "pcode",
            label: "Pcode",
            type: "text",
            required: true
        },
        {
            name: "pid",
            label: "Parent",
            type: "location",
            required: true
        },
        {
            name: "lti",
            label: "Location type",
            type: "location_type",
            required: true
        },
        {
            name: "status",
            label: "Status",
            type: "select",
            options: []
        }
    ],
    buttons: [
        {
            label: "Approve",
            icon: "fa-check"
        },
        {
            label: "Reject",
            icon: "fa-times"
        }
    ],
    outcomes: [
        (data) => {

        },
        (data) =>{

        }
    ]
};

export default TASK;