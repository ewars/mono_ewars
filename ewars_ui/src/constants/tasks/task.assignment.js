const TASK = {
    title: "Assignment request",
    body: "A user has requested a new assignment, please review the details below and take an appropriate action",
    form: [
        {
            name: "form",
            label: "Form",
            type: "form",
            required: true
        },
        {
            name: "location_id",
            label: "Location",
            type: "location",
            required: true,
        }
    ],
    buttons: [
        {
            label: "Approve",
            icon: "fa-check"
        },
        {
            label: "Reject",
            icon: "fa-times"
        }
    ],
    outcomes: [
        (data) => {

        },
        (data) =>{

        }
    ]
};

export default TASK;