const ADMIN_COMPONENTS = [
    {
        name: "Config",
        icon: "fa-cogs",
        tab: false,
        viewCmp: "CONFIG"
    },
    {
        name: "Assignments",
        icon: "fa-clipboard",
        tab: false,
        viewCmp: "GRID",
        grid: "ASSIGNMENTS"
    },
    {
        name: "Alarms",
        icon: "fa-bell",
        tab: false,
        grid: "ALARMS",
        viewCmp: "GRID"
    },
    {
        name: "Dashboards",
        icon: "fa-tachometer",
        tab: false,
        grid: "DASHBOARDS",
        viewCmp: "GRID"
    },
    {
        name: "Data Import",
        icon: "fa-upload",
        tab: false,
        grid: "IMPORTS",
        viewCmp: "GRID"
    },
    {
        name: "Devices",
        icon: "fa-mobile",
        tab: false,
        grid: "DEVICES",
        viewCmp: "GRID"
    },
    {
        name: "Documents",
        icon: "fa-file",
        tab: false,
        grid: "DOCUMENTS",
        viewCmp: "GRID"
    },
    {
        name: "External Data",
        icon: "fa-database",
        tab: false,
        grid: "EXTERNAL",
        viewCmp: "GRID"
    },
    {
        name: "Forms",
        icon: "fa-list",
        tab: false,
        grid: "FORMS",
        viewCmp: "GRID"
    },
    {
        name: "Hooks",
        icon: "fa-download",
        tab: false,
        grid: "HOOKS",
        viewCmp: "GRID"
    },
    {
        name: "Indicators",
        icon: "fa-code-branch",
        tab: true,
        viewCmp: "INDICATORS"
    },
    {
        name: "Locations",
        icon: "fa-map-marker",
        tab: true,
        viewCmp: "LOCATIONS"
    },
    {
        name: "Organizations",
        icon: "fa-building",
        viewCmp: "GRID",
        grid: "ORGANIZATIONS"
    },
    {
        name: "Teams",
        icon: "fa-users",
        tab: false,
        grid: "TEAMS",
        viewCmp: "GRID"
    },
    {
        name: "Users",
        icon: "fa-users",
        tab: false,
        grid: "USERS",
        viewCmp: "GRID"
    },
    // {
    //     name: "Sites",
    //     icon: "fa-sitemap",
    //     tab: false,
    //     grid: "SITES",
    //     viewCmp: "GRID"
    // },
    {
        name: "SMS Gateways",
        icon: "fa-square",
        tab: false,
        grid: "GATEWAYS",
        viewCmp: "GRID"
    },
    {
        name: "Outputs",
        icon: "fa-download",
        tab: true,
        viewCmp: "OUTPUTS"
    },
    {
        name: "Roles",
        icon: "fa-users",
        tab: false,
        grid: "ROLES",
        viewCmp: "GRID"
    },
    {
        name: "Translations",
        icon: "fa-language",
        tab: true,
        viewCmp: "TRANSLATIONS"
    },
    {
        name: "Reporting Periods",
        icon: "fa-calendar",
        tab: false,
        grid: "PERIODS",
        viewCmp: "GRID"
    },
    {
        name: "Invitations",
        icon: "fa-envelope",
        tab: false,
        grid: "INVITES",
        viewCmp: "GRID"
    }
    /// #if DESKTOP
    ,
    {
        name: "USB Modem",
        icon: "fa-usb",
        tab: false,
        viewCmp: "MODEM"
    },
    {
        name: "Map Tiles",
        icon: "fa-map",
        tab: false,
        viewCmp: "MAP_TILES"
    }
    /// #endif
];

export default ADMIN_COMPONENTS;
