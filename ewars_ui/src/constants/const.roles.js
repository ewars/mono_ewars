module.exports = {
    "SUPER_ADMIN": {
        name: {
            en: "Super Administrator"
        },
        creators: [],
        description: {
            en: "A super administrator has access to all configuration, systems, data and information available within EWARS"
        },
        permissions: [
            "Create/Edit/Delete Users",
            "Create/Edit/Delete Locations",
            "Create/Edit/Delete Accounts",
            "Create/Edit/Delete Forms",
            "View/Analyse All Country Data",
            "View/Analyse Low-level System metrics",
            "Configure Workers and Worker formations",
            "Trigger data rebuilds",
            "View/Edit/Retract any report of any type globally",
            "Submit any report of any type globally",
            "Create/Edit/Delete Organizations",
            "Create/Edit/Delete Laboratories",
            "Create/Edit/Delete Outbreak Responses",
            "Create/Edit/Delete Analysis templates"
        ]
    },
    "GLOBAL_ADMIN": {
        name: {
            en: "Global Administrator"
        },
        creators: [],
        description: {
            en: "A Global Administrator has access to all configuration, systems, data and information availabel within EWARS"
        },
        permissions: [
            "Create/Edit/Delete Users",
            "Create/Edit/Delete Locations",
            "Create/Edit/Delete Accounts",
            "Create/Edit/Delete Forms",
            "View/Analyse All Country Data",
            "View/Edit/Retract any report of any type globally",
            "Submit any report of any type globally",
            "Create/Edit/Delete Organizations",
            "Create/Edit/Delete Laboratories",
            "Create/Edit/Delete Outbreak Responses",
            "Create/Edit/Delete Analysis templates"
        ]
    },
    "ACCOUNT_ADMIN": {
        name: {
            en: "Account Administrator"
        },
        creators: ["ACCOUNT_ADMIN"],
        description: {
            en: "A Account Administrator has access to all users, locations and data under their respective account"
        },
        permissions: [
            "Create/Edit/Delete Users within their account",
            "View Locations under their account",
            "View/Analyse all data within their country",
            "Approve/Reject Assignment requests",
            "Suppress/Escalate Alerts",
            "Create/Edit/Suppress Investigations",
            "Create/Edit/Suppress Outbreak Responses",
            "View/Analyse performance at any level within their country",
            "Export report/indicator data at any level within the country",
            "Approve/Reject User registrations within their country",
            "Submit reports of any type for any location within their country",
            "View/Amend/Delete any report of any type within their country"
        ]
    },
    "REGIONAL_ADMIN": {
        name: {
            en: "Geographic Administrator"
        },
        creators: ["ACCOUNT_ADMIN"],
        description: {
            en: "A Regional Administrator has special privileges to perform administrative actions for a geographic boundary within the system"
        },
        permissions: [
            "View locations under their region",
            "View/Analyse all data under their region",
            "Approve/Reject assignment request for their region",
            "Administrate alerts and investigations in their region",
            "View/Analyse performance at their regional level and lower",
            "Export report/indicator data for their regional location and lower",
            "Submit and review reports of any type submitted within their region",
            "View/Amend/Delete reports of any type submitted from within their region"
        ]
    },
    "USER": {
        name: {
            en: "Reporting User"
        },
        creators: ["ACCOUNT_ADMIN"],
        description: {
            en: "A reporting user belongs to a given account (and organization) and is responsible for submitting reports within the system"
        },
        permissions: [
            "Request assignments",
            "Submit reports for locations where they have a valid assignment",
            "View/Analyse data for locations where they have assignments"
        ]
    },
    "BOT": {
        name: {
            en: "Bot"
        },
        description: {
            en: "A Bot is a system-level user assigned to a specific account used for automated operations like data import where a user is unavailable"
        },
        permissions: [
            "Bulk import reports"
        ]
    }
};