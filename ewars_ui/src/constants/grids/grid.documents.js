import Moment from "moment";

const STATUSES = {
    ACTIVE: "Active",
    INACTIVE: "Inactive",
    ARCHIVED: "Archived"
};

const FORMS_GRID = {
    title: "Documents",
    icon: "fa-book",
    columns: [
        {
            name: "template_name",
            type: "text",
            label: "Template name",
            root: true
        },
        {
            name: "status",
            type: "select",
            label: "Status",
            options: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive'],
                ['DRAFT', 'Draft']
            ],
            root: true
        },
        {
            name: "created",
            type: "date",
            label: "Created",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            },
            root: true
        },
        {
            name: "modified",
            type: "date",
            label: "Modified",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            },
            root: true
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'Create new document'],
        '-',
        ['fa-upload', 'IMPORT', 'Import document'],
        '-',
        ['fa-asterisk', 'HUB', 'Browse Hub']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'Edit template'],
        ['fa-trash', 'DELETE', 'Delete template'],
        ['fa-download', 'EXPORT', 'Export template']
    ],
    action: function (action, data) {
        console.log(data);
        switch (action) {
            case "CREATE":
                state.addTab("ADMIN_DOCUMENT", {
                    name: "New Document"
                });
                break;
            case "EDIT":
                state.addTab("ADMIN_DOCUMENT", {
                    name: data.template_name.en || data.template_name,
                    uuid: data.uuid
                });
                break;
            case "EXPORT":
                break;
            case "DELETE":
                if (["ACCOUNT_ADMIN", 'REGIONAL_ADMIN', 'USER'].indexOf(data.uuid) >= 0) {
                    window.error("fa-trash", "System roles can not be deleted");
                } else {
                    window.dialog({
                        title: "DELETE_DOCUMENT",
                        body: "DELETE_DOCUMENT_BODY",
                        buttons: [
                            "Delete",
                            "Cancel"
                        ]
                    }, (res) => {
                        if (res == 0) {
                            let bl = window.blocker("DELETING_DOCUMENT");
                            window.tx("com.sonoma.document.delete", data.uuid, (res) => {
                                bl.rm();
                                window.growl("fa-trash", "DOCUMENT_DELETED");
                                window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                            }, (err) => {
                                bl.rm();
                                window.error("ERROR_DELETE", err);
                                window.dispatchEvent(new CustomEvent('reloadgrid', {}));
                            })
                        }
                    });
                }
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.tx("com.sonoma.documents", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            })
        })
    }
}

export default FORMS_GRID;
