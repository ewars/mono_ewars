import React from "react";
import Moment from "moment";


const COLUMNS = [
    {
        name: "form_name",
        label: "Form",
        type: "form",
        root: true
    },
    {
        name: "location_name",
        label: "Location",
        type: "location",
        root: true
    },
    {
        name: "data_date",
        label: "Record Date",
        type: "date",
        root: true
    },
    {
        name: "created",
        type: "date",
        label: "Created",
        root: true
    },
    {
        name: "modified",
        type: "date",
        label: "Modified",
        root: true
    }
];

const GRID = {
    title: "Drafts",
    icon: "fa-clipboard",
    columns: COLUMNS,
    actions: [],
    rowActions: [
        ['fa-pencil', 'EDIT', "EDIT_DRAFT"],
        ['fa-trash', 'DELETE', "DELETE_DRAFT"]
    ],
    action: function(action, data) {
        switch (action) {
            case "EDIT":
                break;
            case "DELETE":
                window.dialog({
                    title: "DELETE_DRAFT",
                    body: "DELETE_DRAFT_BODY",
                    buttons: [
                        "DELETE",
                        "CANCEL"
                    ]
                }, (res) => {
                    if (res == 0) {
                        window.blocker("DELETING_DRAFT");
                        window.tx("com.sonoma.draft.delete", data.uuid, (res) => {
                            window.removeBlocker();
                            window.growl("fa-trash", "DRAFT_DELETED");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            window.removeBlocker();
                            window.error("ERROR_DELETION", err);
                        })
                    }
                })
                break;
            default:
                break;
        }
    },
    query: function(orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx('com.sonoma.drafts', {orders, filters, limit,offset}, (res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            })
        })
    }
}

export default GRID;
