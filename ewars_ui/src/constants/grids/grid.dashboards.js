import Moment from "moment";

import dialog from "../../utils/dialog";

const GRID = {
    title: "Dashboards",
    icon: "fa-tachometer",
    columns: [
        {
            name: "name",
            label: "Name",
            type: "text",
            root: true
        },
        {
            name: "status",
            label: "Status",
            type: "select",
            options: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive']
            ],
            root: true
        },
        {
            name: "created",
            label: "Created",
            root: true,
            type: "date",
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "Modified",
            type: "date",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'Create new dashboard'],
        ['fa-upload', 'IMPORT', 'Import dashboard']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'Edit dashboard'],
        ['fa-download', 'EXPORT', 'Export dashboard'],
        ['fa-trash', 'DELETE', 'Delete dashboard']
    ],
    action: function (action, data) {
        switch(action) {
            case "CREATE":
                window.dispatchEvent(new CustomEvent("AddTab", {
                    detail: {
                        type: "EDITOR",
                        name: "New Dashboard"
                    }
                }));
                break;
            case "DELETE":
                window.dialog({
                    title: "Delete dashboard?",
                    body: "Are you sure you want to delete this dashboard?",
                    buttons: [
                        'Delete',
                        'Cancel'
                    ]
                }, (result) => {
                    if (result == 0) {
                        let bl = window.state.blocker("Deleting resource");
                        window.tx('com.sonoma.resource.delete', data.uuid, (res) => {
                            bl.rm();
                            window.growl("Dashboard deleted");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            dialog.error("There was an error deleting the dashboard", err);
                        })
                    }
                });
                break;
            case "EDIT":
                window.state.addTab("ADMIN_DASHBOARD", {
                    name: data.name.en || data.name,
                    uuid: data.uuid
                });
                break;
            default:
                break;
        }
    },
    baseFilters: {
        content_type: ['eq', 'text', 'DASHBOARD']
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.tx("com.sonoma.resources.dashboards", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => {
                console.log(err);
            })
        })
    }
}

export default GRID;
