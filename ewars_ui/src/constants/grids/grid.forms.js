import Moment from "moment";

const STATUSES = {
    ACTIVE: "Active",
    INACTIVE: "Inactive",
    ARCHIVED: "Archived"
};

const FORMS_GRID = {
    title: "Forms",
    icon: "fa-clipboard",
    columns: [
        {
            name: "name",
            type: "text",
            label: "Name",
            fmt: function (val) {
                return val.en || val;
            },
            root: true
        },
        {
            name: "version",
            type: "number",
            label: "Version",
            root: true
        },
        {
            name: "status",
            type: "select",
            label: "Status",
            options: [
                ['ACTIVE', 'Active'],
                ['INACTIVE', 'Inactive'],
                ['ARCHIVED', 'Archived']
            ],
            fmt: function (val) {
                return STATUSES[val] || "N/A";
            },
            root: true
        },
        {
            name: "created",
            type: "date",
            label: "Created",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            type: "date",
            label: "Modified",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'Create new form'],
        ['fa-upload', 'IMPORT', 'Import form'],
        '-',
        ['fa-asterisk', 'HUB', 'Browse Hub']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'Edit form'],
        ['fa-download', 'EXPORT', 'Export form'],
        ['fa-copy', 'DUPLICATE', 'Duplicate form'],
        ['fa-trash', 'DELETE', 'Delete form']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                dispatchEvent(new CustomEvent("AddTab", {
                    detail: {
                        type: "EDITOR",
                        resource: "FORM",
                        name: "New Form"
                    }
                }));
                break;
            case "EDIT":
                dispatchEvent(new CustomEvent("AddTab", {
                    detail: {
                        type: "EDITOR",
                        name: __(data.name),
                        type: "FORM",
                        uuid: data.uuid
                    }
                }));
                break;
            case "DELETE":
                window.dialog({
                    title: "DELETE_FORM",
                    body: "DELETE_FORM_BODY",
                    buttons: [
                        "Delete",
                        "Cancel"
                    ]
                }, (result) => {
                    if (result == 0) {
                        window.blocker("DELETING_FORM");
                        window.tx("com.sonoma.form.delete", data.uuid, (res) => {
                            window.removeBlocker();
                            window.growl("fa-trash", "DELETED_FORM");
                            window.dispatchEvent(new CustomEvent("reloadgrid"));
                        }, (err) => {
                            window.removeBlocker();
                            window.error("ERROR_FORM_DELETION", err);
                        })
                    }
                });
                break;
            case "DUPLICATE":
                window.tx('com.sonoma.form.duplicate', data.uuid, (res) => {
                    window.growl('fa-copy', "FORM_DUPLICATED");
                    window.dispatchEvent(new CustomEvent("AddTab", {
                        detail: {

                        }
                    }));
                }, (err) => {
                    window.error("fa-copy", "ERROR_COPYING", err);
                });
                break;
            case "IMPORT":
                window.error("NOT_IMPLEMENTED");
                break;
            case "EXPORT":
                window.error("NOT_IMPLEMENTED");
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.tx("com.sonoma.forms", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => {
                console.log(err);
            })
        })
    }
}

export default FORMS_GRID;
