import Moment from "moment";


const GRID = {
    title: "SMS Gateways",
    icon: "fa-mobile",
    columns: [
        {
            name: "name",
            type: "text",
            label: "Name",
            root: true
        },
        {
            name: "status",
            type: "select",
            label: "Status",
            options: [
                ['ACTIVE', 'Inactive'],
                ['INACTIVE', 'Inactive']
            ],
            root: true
        },
        {
            name: "created",
            type: "date",
            label: "Created",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            type: "date",
            label: "Modified",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                window.state.addTab("GATEWAY", {
                    name: "New gateway"
                });
                break;
            case "DELETE":
                window.state.dialog({
                    title: "DELETE_GATEWAY",
                    body: "DELETE_GATEWAY_BODY",
                    button: [
                        'DELETE',
                        'CANCEL'
                    ]
                }, (result) => {
                    if (result == 0) {
                        let bl = window.state.blocker("DELETING_GATEWAY");
                        window.sonomaClient.tx("com.sonoma.gateway.delete", data.uuid, (res) => {
                            bl.rm();
                            window.state.growl("fa-trash", "GATEWAY_DELETED");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            window.state.error("ERROR_DELETION", err);
                        })
                    }
                });
                break;
            case "EDIT":
                window.state.addTab("GATEWAY", {
                    name: data.name.en || data.name,
                    uuid: data.uuid
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.gateways", {orders, filters, limit, offset}, (res) => {
                resolve(res)
            }, (err) => {
                reject(err);
            });
        })
    }
}

export default GRID;