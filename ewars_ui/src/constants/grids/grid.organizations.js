import Moment from "moment";

const GRID = {
    title: "Organizations",
    icon: "fa-building",
    columns: [
        {
            name: "name",
            label: "NAME",
            type: "text",
            root: true,
            fmt: function(val) {
                return val.en || val;
            }
        },
        {
            name: "acronym",
            label: "ACRONYM",
            type: "text",
            root: true
        },
        {
            name: "created",
            label: "CREATED",
            type: "date",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "MODIFIED",
            type: "date",
            root: true,
            fmt: function (val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    search: true,
    actions: [
        ['fa-plus', 'CREATE', 'CREATE_NEW'],
        ['fa-upload', 'IMPORT', 'IMPORT'],
        '-',
        ['fa-asterisk', 'HUB', "HUB"]
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', 'EDIT'],
        ['fa-copy', 'DUPLICATE', 'DUPLICATE'],
        ['fa-download', 'EXPORT', 'EXPORT'],
        ['fa-trash', 'DELETE', 'DELETE']
    ],
    action: function (action, data) {
        switch (action) {
            case "EDIT":
                window.state.addTab("ORGANIZATION", {
                    name: data.name.en || data.name,
                    uuid: data.uuid
                });
                break;
            case "CREATE":
                window.state.addTab("ORGANIZATION", {
                    name: "New organization"
                });
                break;
            case "DELETE":
                window.stat.dialog({
                    title: "DELETE_ORGANIZATION",
                    body: "DELETE_ORGANIZATION_BODY",
                    buttons: [
                        'DELETE',
                        'CANCEL'
                    ]
                }, (result) => {
                    if (result == 0) {
                        let bl = window.state.blocker("DELETING_ORGANIZATION");
                        window.sonomaClient.tx("com.sonoma.organization.delete", data.uuid, (res) => {
                            bl.rm();
                            window.state.growl("fa-trash", "DELETED_ORGANIZATION");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("ERROR_DELETION", err);
                        })
                    }
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.sonomaClient.tx("com.sonoma.organizations", {orders, filters, limit, offset}, (res) => {
                resolve(res);
            }, (err) => reject(err));
        })
    }
}

export default GRID;