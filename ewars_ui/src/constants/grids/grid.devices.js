import Moment from "moment";

const GRID = {
    title: "Devices",
    icon: "fa-phone",
    columns: [
        {
            name: "name",
            label: "User name",
            type: "text",
            root: true
        },
        {
            name: "email",
            label: "User email",
            type: "text",
            root: true
        },
        {
            name: "device_type",
            label: "Device Type",
            type: "text",
            root: true
        },
        {
            name: "did",
            label: "Device ID",
            type: "text",
            root: true
        },
        {
            name: "last_seed",
            label: "Last Seen",
            type: "date",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "created",
            label: "Created",
            type: "date",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            label: "Modified",
            type: "date",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "blocked",
            label: "Blocked",
            type: "bool",
            root: true,
            fmt: function (val) {
                if (val) return "Yes";
                return "No";
            }
        }
    ],
    search: true,
    actions: [
        ['fa-plus', 'CREATE', 'Create device'],
        ['fa-upload', 'IMPORT', 'Import devices']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', ' Edit device'],
        ['fa-trash', 'DELETE', 'Delete device']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                window.state.addTab("DEVICE", {
                    name: "New Device"
                });
                break;
            case "EDIT":
                window.shade({
                    title: "Device",
                    data: data,
                    actions: [],
                    callback: (action) => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    },
                    cmp: "DEVICE"
                });
                break;
            case "DELETE":
                window.dialog({
                    title: "DELETE_DEVICE",
                    body: "BODY_DELETE_DEVICE",
                    buttons: [
                        'Delete',
                        'Cancel'
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.blocker("DELETING_DEVICE");
                        window.tx('com.sonoma.device.delete', data.uuid, (res) => {
                            bl.rm();
                            window.growl("fa-trash", "DELETED_DEVICE");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.error("ERROR", err);
                        })
                    }
                })
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.tx('com.sonoma.devices', {orders, filters, offset, limit}, (res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            })
        })
    }
}

export default GRID;
