import Moment from "moment";

import dialog from "../../utils/dialog";


const GRID = {
    title: "Assignments",
    icon: "fa-clipboard",
    columns: [
        {
            name: "name",
            type: "text",
            label: "User Name",
            root: true
        },
        {
            name: "email",
            type: "text",
            label: "User email",
            root: true
        },
        {
            name: "status",
            type: "select",
            label: "Status",
            options: [
                ['ACTIVE', 'Inactive'],
                ['INACTIVE', 'Inactive']
            ],
            root: true
        },
        {
            name: "assign_type",
            type: "select",
            label: "Type",
            root: true,
            options: [
                ['DEFAULT', 'Default'],
                ['UNDER', 'Multiple locations'],
                ['GROUP', 'Location group']
            ]
        },
        {
            name: "location_name",
            type: "text",
            label: "Location",
            root: true,
            fmt: function(val) {
                return val.en || val;
            }
        },
        {
            name: "created",
            type: "date",
            label: "Created",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        },
        {
            name: "modified",
            type: "date",
            label: "Modified",
            root: true,
            fmt: function(val) {
                return Moment.utc(val).local().format("YYYY-MM-DD HH:mm:ss");
            }
        }
    ],
    search: true,
    actions: [
        ['fa-plus', 'CREATE', 'Create new assignment']
    ],
    rowActions: [
        ['fa-pencil', 'EDIT', ' Edit assignment'],
        ['fa-trash', 'DELETE', 'Delete assignment']
    ],
    action: function (action, data) {
        switch (action) {
            case "CREATE":
                window.dispatchEvent(new CustomEvent("AddShade", {
                    detail: {
                        cmp: "ASSIGNMENT",
                        actions: [],
                        title: "Assignment",
                        data: {},
                        callback: () => {
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }
                    }
                }));
                break;
            case "EDIT":
                window.shade({
                    cmp: "ASSIGNMENT",
                    actions: [],
                    title: "Assignment",
                    data: data,
                    callback: () => {
                        window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                    }
                });
                break;
            case "DELETE":
                window.dialog({
                    title: "Delete assignment",
                    body: "This user will not longer be able to access records for this location.",
                    buttons: [
                        'Delete',
                        'Cancel'
                    ]
                }, (res) => {
                    if (res == 0) {
                        let bl = window.state.blocker("DELETING_ASSIGNMENT");
                        window.tx('com.sonoma.assignment.delete', data.uuid, (res) => {
                            bl.rm();
                            window.state.growl("fa-trash", "ITEM_DELETE");
                            window.dispatchEvent(new CustomEvent("reloadgrid", {}));
                        }, (err) => {
                            bl.rm();
                            window.state.growl("ERROR", err);
                        })
                    }
                });
                break;
            default:
                break;
        }
    },
    query: function (orders, filters, limit, offset) {
        return new Promise((resolve, reject) => {
            window.tx("com.sonoma.assignments", {orders, filters, limit, offset}, (res) => {
                resolve({
                    records: [],
                    results: [],
                    count: 0
                })
            }, (err) => {
                console.log(err)
                resolve({
                    records: [],
                    results: [],
                    count: 0
                });
            });
        })
    }
}

export default GRID;
