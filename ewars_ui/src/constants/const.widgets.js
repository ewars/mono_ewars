import DocumentsWidget from "../controls/widgets/w.documents";

const CMP = "CMP";
const CLS = "CLS";

const WIDGETS = {
    TEXT: {
        renderType: CMP,
        cmp: null
    },
    SERIES: {
        renderType: CLS,
        cmp: null
    },
    TABLE: {
        renderType: CMP,
        cmp: null
    },
    PIVOT: {
        renderType: CMP,
        cmp: null
    },
    PIE: {
        renderType: CLS,
        cmp: null
    },
    BAR: {
        renderType: CLS,
        cmp: null
    },
    MAP: {
        renderType: CLS,
        cmp: null
    },
    FUNNEL: {
        renderType: CLS,
        cmp: null
    },
    PYRAMID: {
        renderType: CLS,
        cmp: null
    },
    HTML: {
        renderType: CMP,
        cmp: null
    },
    EVENTS: {
        renderType: CMP,
        cmp: null
    },
    ALERT_LIST: {
        renderType: CMP,
        cmp: null
    },
    ALERT_MAP: {
        renderType: CLS,
        cmp: null
    },
    OVERDUE: {
        renderType: CMP,
        cmp: null
    },
    UPCOMING: {
        renderType: CMP,
        cmp: null
    },
    NODE_SEARCH: {
        renderType: CMP,
        cmp: null
    },
    GUIDE_PAGE: {
        renderType: CMP,
        cmp: null
    },
    TEAM_FILES: {
        renderType: CMP,
        cmp: null
    },
    TEAM_FEED: {
        renderType: CMP,
        cmp: null
    },
    ASSIGNMENTS: {
        renderType: CMP,
        cmp: null
    },
    TASKS: {
        renderType: CMP,
        cmp: null
    },
    STREAM: {
        renderType: CLS,
        cmp: null
    },
    GAUGE: {
        renderType: CLS,
        cmp: null
    },
    TREE: {
        renderType: CLS,
        cmp: null
    },
    SPARK: {
        renderType: CLS,
        cmp: null
    },
    XRANGE: {
        renderType: CLS,
        cmp: null
    },
    CANDLE: {
        renderType: CLS,
        cmp: null
    },
    BULLET: {
        renderType: CLS,
        cmp: null
    },
    DOCUMENTS: {
        renderType: CLS,
        cmp: DocumentsWidget
    }
}
