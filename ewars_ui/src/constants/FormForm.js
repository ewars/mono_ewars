/*
 * Copyright (c) 2015 JDU Software & Consulting Limited.
 * ALl Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of JDU Software &
 * Consulting Limited and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to JDU Software & Consulting Limited and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from JDU Software & Consulting Limited.
 */

var user_types = require("../../common/constants/user_types");

module.exports = {
    name: {
        order: 0,
        type: "language_string",
        label: "Form Name",
        required: true,
        help: "Enter a descriptive name for the form"
    },
    ftype: {
        order: 1,
        type: "select",
        label: {
            en: "Form Type"
        },
        options: [
            ["PRIMARY", "Primary Collection"],
            ["INVESTIGATIVE", "Investigative"]
            //["RESPONSE", "Response"],
            //["SURVEY", "Survey"],
            //["QUIZ", "Quiz"],
            //["LAB", "Lab Report"],
            //["TRACE", "Track & Trace"]
        ],
        help: {
            en: "Type of context that this form will be used within, default is 'Primary Collection' for normal surveillance"
        }
    },
    time_interval: {
        order: 2,
        type: "select",
        label: "Reporting Interval",
        options: [
            ["NONE", "None"],
            ["DAY", "Daily"],
            ["WEEK", "Weekly"],
            ["MONTH", "Monthly"],
            ["YEAR", "Yearly"]
        ],
        help: "Select the reporting period that this form should be submittable for, if None, a report can be submitted for any date",
        conditions: {
            application: "all",
            rules: [
                ["ftype", "eq", "PRIMARY"]
            ]
        }
    },
    account_id: {
        order: 3,
        type: "select",
        label: "Account",
        optionsSource: {
            resource: "account",
            query: {
                status: {eq: "ACTIVE"}
            },
            valSource: 'id',
            labelSource: "name"
        }
    },
    location_aware: {
        order: 4,
        type: "switch",
        label: "Location Aware",
        help: "Turn this on if this form should be submitted for specific locations",
        conditions: {
            application: "all",
            rules: [
                ["ftype", "eq", "PRIMARY"]
            ]
        }
    },
    site_type_id: {
        order: 5,
        type: "select",
        label: "Allowed Location Type",
        optionsSource: {
            resource: "location_type",
            query: {},
            valSource: 'id',
            labelSource: "name",
            additional: [
                ["ALL", "All Locations"]
            ]
        },
        conditions: {
            application: "all",
            rules: [
                ["location_aware", "eq", true],
                ["ftype", "eq", "PRIMARY"]
            ]
        }
    },
    single_report_context: {
        order: 6,
        type: "switch",
        label: {
            "en": "Single Report Restriction"
        },
        help: "Disable this setting if you would like to allow multiple reports with the same report date / location selected",
        conditions: {
            application: "all",
            rules: [
                ["time_interval", "ne", "NONE"],
                ["location_aware", "eq", true],
                ["ftype", "eq", "PRIMARY"]
            ]
        }
    },
    overdue_interval: {
        order: 7,
        type: "select",
        label: {
            en: "Overdue Interval"
        },
        options: [
            ["DAY", "Day(s)"],
            ["WEEK", "Week(s)"],
            ["MONTH", "Month(s)"],
            ["YEAR", "Year(s)"]
        ],
        conditions: {
            application: "all",
            rules: [
                ["single_report_context", "eq", true],
                ["ftype", "eq", "PRIMARY"]
            ]
        },
        help: {
            en: "The interval at which the report will become overdue"
        }
    },
    overdue_threshold: {
        order: 8,
        type: "number",
        label: {
            en: "Overdue Threshold"
        },
        conditions: {
            application: "all",
            rules: [
                ["single_report_context", "eq", true],
                ["ftype", "eq", "PRIMARY"]
            ]
        },
        help: {
            en: "The number of the Overdue Interval after which the report will become overdue"
        }
    },
    block_future_dates: {
        order: 9,
        type: "switch",
        label: {
            en: "Block Future Dates"
        },
        help: {
            en: "Do not allow reports to be submitted past the current date"
        },
        conditions: {
            application: "all",
            rules: [
                ["ftype", "eq", "PRIMARY"]
            ]
        }
    },
    status: {
        order: 11,
        type: "select",
        label: "Form Status",
        required: true,
        options: [
            ["DRAFT", "Draft"],
            ["ACTIVE", "Active"],
            ["ARCHIVED", "Archived"]
        ]
    },
    required: {
        order: 12,
        type: "select",
        label: "Required",
        options: [
            ["SINGLE", "Required"],
            ["REQUIRED_MIN_1", "Multiple allowed; 1 required"],
            ["NOT_REQUIRED", "Not required"]
        ],
        conditions: {
            application: "all",
            rules: [
                ["ftype", "eq", "INVESTIGATIVE"]
            ]
        }
    },
    description: {
        order: 12,
        type: "textarea",
        label: "Form Description",
        i18n: true,
        required: true,
        help: "Enter a concise description of the form"
    }
};