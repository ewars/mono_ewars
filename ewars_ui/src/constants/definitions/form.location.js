const FORM = {
    h_general: {
        type: "header",
        label: "General",
        icon: "fa-cog",
        order: 0
    },
    name: {
        type: "text",
        label: "Name",
        name: "name",
        required: true,
        i18n: true,
        order: 1
    },
    uuid: {
        type: "display",
        label: "UUID",
        order: 2
    },
    status: {
        type: "select",
        options: [
            ['ACTIVE', 'Active'],
            ['INACTIVE', 'Inactive'],
            ["ARCHIVED", "Archived"]
        ],
        label: "Status",
        required: true,
        order: 3
    },
    pcode: {
        type: "text",
        required: true,
        label: "Pcode",
        order: 4
    },
    codes: {
        type: "dict",
        label: "ID Codes",
        order: 5,
        vertical: true
    },
    pid: {
        type: "location",
        label: "Parent",
        order: 6
    },
    groups: {
        type: "groups",
        label: "Groups",
        order: 7
    },
    oid: {
        type: "organization",
        label: "Organization",
        required: false,
        order: 8
    },
    h_mapping: {
        type: "header",
        label: "Mapping",
        order: 9
    },
    geometry_type: {
        type: "select",
        label: "Geometry Type",
        options: [
            ['POINT', 'Point'],
            ["ADMIN", 'Administrative']
        ],
        order: 10
    },
    geometry: {
        type: "geometry",
        label: "Geometry",
        order: 11,
        vertical: true,
        conditions: [
            ["geometry_type", 'eq', 'ADMIN']
        ]
    }
};

export default FORM;
