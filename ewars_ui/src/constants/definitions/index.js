import LOCATION_FORM from "./form.locations";
import ALARM_FORM from "./form.alarm";

export default {
    LOCATION_FORM,
    ALARM_FORM
}
