const FORM_TABLE_STYLE = [
    {
        _o: 0,
        n: "show_title",
        t: "BUTTONSET",
        l: "Show title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 1,
        n: "b_headers",
        t: "BUTTONSET",
        l: "Show column headers",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 2,
        n: "headers",
        l: "Custom Headers",
        t: "OA"
    },
    {
        _o: 3,
        n: "hide_null_rows",
        l: "Hide NULL Rows",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    }
]

export default FORM_TABLE_STYLE;