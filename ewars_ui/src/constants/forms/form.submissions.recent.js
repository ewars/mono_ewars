const FORM_SUBMISSIONS_RECENT = [
    {
        _o: "0",
        t: "TEXT",
        l: "Title",
        n: "title"
    },
    {
        _o: 1,
        t: "SELECT",
        n: "fid",
        l: "Form",
        os: [
            "form",
            "id",
            "name",
            {status: {eq: "ACTIVE"}}
        ]
    }
]

export default FORM_SUBMISSIONS_RECENT;