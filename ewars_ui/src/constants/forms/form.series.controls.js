const FORM_SERIES_CONTROLS = [
    {
        _o: 0,
        n: "show_title",
        t: "BUTTONSET",
        l: "Show title",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 1,
        n: "tools",
        l: "Show tools",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 2,
        n: "zoom",
        l: "Allow zoom",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 3,
        n: "navigator",
        l: "Show navigator",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 4,
        n: "legend",
        l: "Show legend",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 5,
        n: "legend_position",
        l: "Legend Position",
        t: "SELECT",
        o: [
            ["TL", "Top-left"],
            ["TC", "Top-center"],
            ["TR", "Top-right"],
            ["MR", "Middle-right"],
            ["BR", "Bottom-right"],
            ["BC", "Bottom-center"],
            ["BL", "Bottom-left"],
            ["ML", "Middle-left"]
        ],
        c: [
            "ALL",
            "legend:eq:true"
        ]
    },
    {
        _o: 6,
        n: "legend_label_source",
        t: "SELECT",
        l: "Legend label source",
        o: [
            ["DEFAULT", "Source title"],
            ["CUSTOM", "Custom"],
        ],
        c: [
            "ALL",
            "legend:eq:true"
        ]
    },
    {
        _o: 7,
        n: "legend_lbl_source_custom",
        t: "TEXT",
        l: "Custom legend label",
        c: [
            "ALL",
            "legend_label_source:eq:CUSTOM"
        ]
    }
]

export default FORM_SERIES_CONTROLS;