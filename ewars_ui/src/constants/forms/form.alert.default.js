const FORM_ALERT_DEFAULT = {
    definition: {
        verification_outcome: {
            type: "select",
            label: "Verification Outcome",
            options: [
                ["ADVANCE", "Start Risk Assessment"],
                ["MONITOR", "Monitor"],
                ["DISCARD", "Discard"]
            ],
            stages: ['VERIFICATION'],
            required: true
        },
        verification_comments: {
            type: "textarea",
            guidance: `
- Has the event been reported by an official source (e.g. local health-care centre or clinic, public health authorities, animal health workers?
- Has the event been reported by multiple independent sources (e.g. residents, news media, health-care workers, animal health status )?
- Does the event description include details about time, place and people involved (e.g. six people are sick and two died three days after a ending a local celebration on in community X)?
- Is the clinical presentation of the cases described (e.g. a cluster of seven people admitted to hospital with atypical pneumonia, of whom two have died)?
- Has a similar event been reported previously (e.g. with a similar presentation, affecting a similar population and geographical area, over the same me period)?
            `,
            label: "Comments",
            stages: ['VERIFICATION']
        },
        hazard_assessment: {
            type: "textarea",
            label: "Hazard Assessment",
            guidance: `
Hazard assessment includes:

- identifying the hazard(s) that could be causing the event
- reviewing key information about the potential hazard(s) (i.e. characterizing the hazard)
- ranking potential hazards when more than one is considered a possible cause of the event (equivalent to a differential diagnosis in clinical medicine).
            `,
            required: true,
            stages: ["RISK_ASSESS"]
        },
        exposure_assessment: {
            type: "textarea",
            label: "Exposure Assessment",
            required: true,
            guidance: `
The key output of the assessment is an estimate of the:

- number of people or group known or likely to have been exposed.
- number of exposed people or groups who are likely to be susceptible (i.e capable of getting a disease because they not immune)
            `,
            stages: ["RISK_ASSESS"]
        },
        context_assessment: {
            type: "textarea",
            label: "Context Assessment",
            required: true,
            guidance: `
Context assessment addresses the following questions:

- What are the factors associated with the environment, health status, behaviours, social or cultural practices, health infrastructure and legal and policy frameworks that increase a population’s vulnerability?
            `,
            stages: ['RISK_ASSESS']
        },
        risk: {
            type: "risk",
            label: "Risk",
            required: true,
            stages: ["RISK_CHAR"]
        },
        outcome: {
            type: "select",
            label: "Outcome",
            help: "Please select an outcome for the alert below",
            options: [
                ['RESPOND', 'Response'],
                ['MONITOR', 'Monitor'],
                ['DISCARD', 'Discard']
            ],
            required: true,
            stages: ['OUTCOME']

        },
        outcome_comments: {
            help: "Add any other relevant comments",
            type: "textarea",
            label: "Comments",
            stages: ["OUTCOME"]
        }
    },
    stages: [
        {
            order: 0,
            name: "Verification",
            uuid: "VERIFICATION",
            permissions: null,
            task: true,
            description: "no description",
            routes: [
                {
                    conditions: [
                        ["data.verification_outcome", "eq", "DISCARD"]
                    ],
                    target: "CLOSE"
                },
                {
                    conditions: [
                        ['data.verification_outcome', 'eq', 'MONITOR']
                    ],
                    target: "WAIT"
                },
                {
                    conditions: [
                        ['data.verification_outcome', 'eq', 'ADVANCE']
                    ],
                    target: "RISK_ASSESS"
                }
            ]
        },
        {
            order: 1,
            name: "Risk Assessment",
            uuid: "RISK_ASSESS",
            actions: [],
            task: true,
            description: "No description",
            permissions: null,
            routes: [
                {
                    conditions: null,
                    target: "RISK_CHAR"
                }

            ]
        },
        {
            order: 2,
            name: "Risk Characterization",
            uuid: "RISK_CHAR",
            task: true,
            description: "No description",
            permissions: null,
            rules: [
                {
                    conditions: null,
                    target: "OUTCOME"
                }
            ]
        },
        {
            order: 3,
            name: "Outcome",
            uuid: "OUTCOME",
            description: "no description",
            task: true,
            permissions: null,
            rules: [
                {
                    conditions: [
                        ['data.outcome', 'eq', 'DISCARD']
                    ],
                    target: "CLOSE"
                },
                {
                    conditions: [
                        ['data.outcome', 'eq', 'MONITOR']
                    ],
                    target: "WAIT"
                },
                {
                    conditions: [
                        ['data.outcome', 'eq', 'RESPOND']
                    ],
                    target: "CLOSE"
                }
            ]
        }
    ]
};

export default FORM_ALERT_DEFAULT;
