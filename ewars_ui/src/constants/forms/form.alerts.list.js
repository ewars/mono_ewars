const FORM_ALERTS_LIST = [
    {
        _o: 0,
        t: "TEXT",
        l: "Title",
        n: "title",
        d: "Alerts"
    },
    {
        _o: 1,
        t: "SELECT",
        l: "Specific alarm",
        os: [
            "alarm",
            "uuid",
            "name",
            {}
        ]

    },
    {
        _o: 2,
        t: "TEXT",
        l: "Limit",
        n: "limit"
    }
]

export default FORM_ALERTS_LIST;