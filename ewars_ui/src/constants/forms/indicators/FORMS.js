 var FORMS = {
    status: {
        type: "select",
        label: "Status",
        options: [
            ["ACTIVE", "Active"],
            ["DRAFT", "Draft"]
        ]
    },
    dimension: {
        type: "select",
        label: "Dimension",
        options: [
            ["COUNT", "Count"],
            ["LOCATIONS", "Reporting Locations"]
        ]
    },
    form_id: {
        type: "select",
        label: "Form",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {
                status: {eq: "ACTIVE"}
            }
        }
    }
};

export default FORMS;

