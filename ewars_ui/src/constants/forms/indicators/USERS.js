const USERS = {
   status: {
       type: "select",
       label: "Status",
       options: [
           ["ACTIVE", "Active"],
           ["INACTIVE", "Inactive"],
           ["PENDING_APPROVAL", "Pending Approval"]
       ]
   },
    organization_id: {
        type: "select",
        label: "Organization",
        optionsSource: {
            resource: "organization",
            labelSource: "name",
            valSource: "uuid",
            query: {}
        }
    },
    type: {
        type: "select",
        label: "User Type",
        options: [
            ["USER", "Reporting User"],
            ["REGIONAL_ADMIN", "Geographic Administrator"],
            ["ACCOUNT_ADMIN", "Account Administrator"]
        ]
    }
};

export default USERS;

