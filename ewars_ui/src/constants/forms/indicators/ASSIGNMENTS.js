const ASSIGNMENTS = {
    form_id: {
        type: "select",
        label: "Form",
        optionsSource: {
            resource: "form",
            valSource: "id",
            labelSource: "name",
            query: {}
        }
    },
    status: {
        type: "select",
        label: "Status",
        options: [
            ["ACTIVE", "Active"],
            ["ELAPSED", "Elapsed"],
            ["INACTIVE", "Inactive"]
        ]
    }
};

export default ASSIGNMENTS;

