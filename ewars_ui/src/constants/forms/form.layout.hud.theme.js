const FORM_LAYOUT_HUD_THEME = [
    {
        _o: 0,
        t: "COLOUR",
        l: "Background colour",
        n: "bgdColor"
    },
    {
        _o: 1,
        t: "TEXT",
        l: "Padding",
        n: "padding"
    }
]

export default FORM_LAYOUT_HUD_THEME;