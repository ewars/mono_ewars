const FORM_CATEGORY_STYLE = [
    {
        _o: 0,
        n: "style",
        t: "SELECT",
        l: "Style",
        o: [
            ["DEFAULT", "Default"],
            ["DONUT", "Donut"],
            ["ARC", "Arc"],
            ["BAR", "Bar graph"]
        ]
    },
    {
        _o: 0.1,
        n: "hide_no_data",
        t: "BUTTONSET",
        l: "Hide slices with no data",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 1,
        n: "slice_ordering",
        l: "Ordering",
        t: "SELECT",
        o: [
            ["NONE", "None"],
            ["CAT_NAME", "Category Title Ascending"],
            ["CAT_NAME_DESC", "Category Title Descending"],
            ["VALUE_ASC", "Value Ascending"],
            ["VALUE_DESC", "Value Descending"]
        ]
    },
    {
        _o: 2,
        t: "H",
        l: "Y Axis"
    },
    {
        _o: 2.1,
        n: "y_axis_label",
        t: "TEXT",
        l: "Y axis label"
    },
    {
        _o: 2.2,
        n: "y_axis_format",
        t: "SELECT",
        l: "Y axis format",
        o: [
            ["0", "0"],
            ["0.00", "0.00"],
            ["0%", "0%"],
            ["0.00%", "0.00%"]
        ]
    },
    {
        _o: 2.3,
        n: "y_axis_allow_decimals",
        t: "BUTTONSET",
        l: "Allow decimals",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 3,
        t: "H",
        l: "Layout"
    },
    {
        _o: 4,
        t: "QUADSET",
        n: "m",
        l: "Margin",
        d: [0,0,0,0]
    }
]

export default FORM_CATEGORY_STYLE;