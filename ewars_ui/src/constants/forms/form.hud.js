const FORM_HUD = [
    {
        _o: 0,
        t: "TEXT",
        n: "name",
        l: __("DESCRIPTION"),
        r: true
    },
    {
        _o: 1,
        t: "SELECT",
        l: __("STATUS"),
        n: "status",
        o: [
            ["ACTIVE", "Active"],
            ["DRAFT", "Draft"],
            ["INACTIVE", "Inactive"]
        ]
    },
    {
        _o: 2,
        t: "TEXT",
        ml: true,
        l: __("DESCRIPTION"),
        n: "description"
    }
];

export default FORM_HUD;