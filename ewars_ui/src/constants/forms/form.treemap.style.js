const FORM_TREEMAP_STYLE = [
    {
        _o: 0,
        n: "c_range",
        l: "Colour Range",
        t: "TEXT"
    },
    {
        _o: 1,
        n: "b_title",
        l: "Show title",
        t: "BUTTONSET",
        o: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    {
        _o: 2,
        n: "x",
        l: "X Axis",
        t: "SELECT",
        o: [
            ["LOCATION", "Location"],
            ["INDICATOR", "Indicator"],
            ["DATE", "Date"]
        ]
    },
    {
        _o: 3,
        n: "y",
        l: "Y Axis",
        t: "SELECT",
        o: [
            ["LOCATION", "Location"],
            ["INDICATOR", "Indicator"],
            ["DATE", "Date"]
        ]
    },
    {
        _0: 4,
        n: "v",
        l: "Visualization type",
        t: "SELECT",
        o: [
            ["DEFAULT", "Default"],
            ["VORONOI", "Voronoi"],
            ["CPACK", "Circle Packing"],
            ["SUNBURST", "Sunburst"]

        ]
    }
];

export default FORM_TREEMAP_STYLE;