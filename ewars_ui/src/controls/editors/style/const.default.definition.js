const DEFAULT_DEFINITION = [
    {
        type: "header",
        label: ""
    }
    {
        name: "background",
        label: "Background Color",
        type: "colour"
    },
    {
        name: "borderTop",
        label: "Border (top)",
        type: "border"
    },
    {
        name: "borderRight",
        label: "Border (right)",
        type: "border"
    },
    {
        name: "borderBottom",
        label: "Border (bottom)",
        type: "border"
    },
    {
        name: "borderLeft",
        label: "Border (left)",
        type: "border"
    },
    {
        name: "marginTop",
        label: "Margin (top)",
        type: "numeric"
    },
    {
        name: "marginRight",
        label: "Margin (right)",
        type: "numeric"
    },
    {
        name: "marginBottom",
        label: ""
    }
]
