import React from "react";

import utils from "../../utils/utils";

import Pages from "./template/c.pages.jsx";
import Page from "./template/c.page.jsx";
import Droppables from "./layout/Droppables.jsx";

import LayoutEditor from "./layout/LayoutEditor.jsx";

class EditorTemplate extends React.Component {
    constructor(props) {
        super(props);

        let tmpl = {
                layout: [
                    {
                        title: "New Page",
                        orientation: "PORTRAIT",
                        layout: [],
                        order: 0,
                        uuid: utils.uuid()
                    }
                ],
                data: {}
        };

        this.state = {
            view: "DEFAULT",
            data: tmpl,
            page: tmpl.layout[0]

        }
    }

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    _onPageChange = (layout, data) => {
        this.setState({
            data: {
                ...this.state.data,
                data: data
            },
            page: {
                ...this.state.page,
                layout: layout
            }
        })
    };

    _onAddPage = () => {
        let pages = utils.copy(this.state.data.layout);
        let newPage = {
            order: 9999,
            title: "New Page",
            orientation: "PORTRAIT",
            uuid: utils.uuid(),
            layout: []
        };

        pages.push(newPage);

        let counter = 0;
        pages.forEach(item => {
            item.order = counter;
            counter++;
        });
        let curPage = this.state.page;
        let inIndex;
        pages.forEach((item, index) => {
            if (item.uuid == curPage.uuid) inIndex = index;
        });
        pages[inIndex] = curPage;

        this.setState({
            data: {
                ...this.state.data,
                layout: pages
            },
            page: newPage
        })
    };

    _swapPage = (uuid) => {
        let newPage = this.state.data.layout.filter(item => {
            return item.uuid == uuid;
        })[0] || null;
        let pages = this.state.data.layout;

        let curPage = this.state.page;
        if (curPage) {
            let ind;
            pages.forEach((item, index) => {
                if (item.uuid == curPage.uuid) ind = index;
            })
            pages[ind] = curPage;
        }

        this.setState({
            data: {
                ...this.state.data,
                layout: pages
            },
            page: newPage
        });
    };

    render() {
        let view;


        if (this.state.view == "DEFAULT") {
            view = <Pages pages={this.state.data.layout}
                          data={this.state.data.data}
                          onPageSelect={this._swapPage}
                          onRemovePage={this._onRemovePage}
                          onMovePage={this._onMovePage}
                          onAddPage={this._onAddPage}/>
        }

        if (this.state.view == "WIDGETS") {
            view = (
                <div className="column br panel">
                    <Droppables/>
                </div>
            )
        }

        return (
            <div className="row">
                <div className="column block br" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fa-file"></i>&nbsp;Pages
                        </div>
                        <div
                            onClick={() => this._view("WIDGETS")}
                            className={"ide-tab" + (this.state.view == "WIDGETS" ? " ide-tab-down" : "")}>
                            <i className="fal fa-cube"></i>&nbsp;Widgets
                        </div>
                        <div
                            onClick={() => this._view("STYLE")}
                            className={"ide-tab" + (this.state.view == "STYLE" ? " ide-tab-down" : "")}>
                            <i className="fal fa-paint-brush"></i>&nbsp;Style
                        </div>
                    </div>
                </div>
                {view}
                <Page
                    onPageChange={this._onPageChange}
                    data={this.state.page}
                    widgets={this.state.data.data}/>
            </div>
        )

    }
}

export default EditorTemplate;
