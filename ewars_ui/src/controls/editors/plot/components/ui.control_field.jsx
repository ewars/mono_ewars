import React from "react";
class ControlField extends React.Component {
    render() {
        return (
            <div className="control-field">
                <div className="row">
                    <div className="column" style={{maxWidth: "30%"}}>
                        {this.props.label}
                    </div>
                    <div className="column">
                        {this.props.children}
                    </div>
                </div>
           </div>
        )
    }
}

export default ControlField;
