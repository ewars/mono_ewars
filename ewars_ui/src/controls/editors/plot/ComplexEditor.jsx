import React from "react";

class ComplexEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="complex-editor">
                <ewars.d.Layout>
                    <ewars.d.Toolbar label="Complex Editor">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-save"/>
                            <ewars.d.Button
                                icon="fa-close"/>
                        </div>

                    </ewars.d.Toolbar>
                </ewars.d.Layout>
            </div>
        )
    }
}

export default ComplexEditor;
