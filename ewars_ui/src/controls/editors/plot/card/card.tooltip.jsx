import React from "react";
class CardTooltip extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false
        }
    }

    _showEditor = () => {
        this.setState({
            show: true
        })
    };

    render() {
        return (
            <div className="plot-settings" onClick={this._showEditor}>
                <div className="icon"><i className="fal fa-comment"></i></div>
                <div className="text">Tooltip</div>
            </div>
        )
    }
}

export default CardTooltip;
