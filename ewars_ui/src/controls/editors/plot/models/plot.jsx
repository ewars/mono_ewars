import React from "react";

class Group  {
    constructor(props) {
        this.id = props._;
        this.items = props.d;
        this.filters = props.f;
        this.config = props.c;
    }
}

class Plot {
     constructor(props) {
        this.groups = props.groups;
        this.config = props.config;

        this._groups = {};

        // Create a dict of the data for easier lookups
        this.groups.forEach(item => {
            this._groups[item._] = new Group(item);
        });
     }

    getGroup = (gid) => {
        return this._groups[gid] || null;
    };

    addGroup = () => {
        let newId = ewars.utils.uuid();
    };
}

export default Plot;
