import React from "react";

import utils from "../../../utils/utils";

import DataMainDrop from './data/data.drop.main.jsx';
import ControlGroup from './data/data.control.jsx';
import CalcGroup from './data/data.calculation.jsx';
import Group from './data/data.group.jsx';
import ControlAxis from './data/data.axis.jsx';
import ColourAxis from './data/data.axis.colour.jsx';
import SizeAxis from './data/data.axis.jsx';

import ActionGroup from "../../c.actiongroup.jsx";

import {instance} from './plot_instance';

const MAIN_ACTIONS = [
    ['fa-plus', 'ADD_GROUP']
]

class DataManager extends React.Component {
    static defaultProps = {
        controls: "*",
        groups: [],
        allowMultipleGroups: true,
        allowedAxisTypes: ['x', 'y', 'custom', 'colour', 'size']
    };

    constructor(props) {
        super(props)

        this.state = {
            cmp: null
        }
    }

    _onChange = () => {
        this.props.onChange();
    };

    _addGroup = () => {
        instance.addGroup()
    };

    _action = (action) => {
        switch (action) {
            case 'ADD_GROUP':
                this._addGroup();
                return;
            default:
                return
        }
    };

    render() {
        // Pull out any calculations
        let calcs = [], controls = [], axis = [], groups = [];

        instance.groups.forEach(group => {
            groups.push(
                <Group nodeId={group._} data={group}/>
            );
        })

        instance.nodes.forEach(node => {
            if (node.t == 'C') {
                calcs.push(
                    <CalcGroup
                        nodeId={node._}
                        key={node._}/>
                )
            } else if (node.t == 'X') {
                controls.push(
                    <ControlGroup
                        key={node._}
                        nodeId={node._}/>
                )
            } else if (node.t == 'A') {
                // axis
                axis.push(
                    <ControlAxis
                        nodeId={node._}
                        key={node._}/>
                )
            } else if (node._t == 'S') {
                axis.push(
                    <SizeAxis
                        nodeId={node._}
                        key={node._}/>
                )

            } else if (node._t == 'C') {
                axis.push(
                    <ColourAxis
                        nodeId={node._}
                        key={node._}/>
                )

            }

        });

        return (
            <div className="column">
                <div className="row block block-overflow" style={{paddingRight: "8px"}}>
                    {groups}
                    {calcs}
                    <div className="row" style={{flexWrap: "wrap", alignContent: "flex-start"}}>
                        {axis}
                        {controls}
                    </div>
                </div>
                {this.state.cmp}
            </div>
        )
    }

}

export default DataManager;
