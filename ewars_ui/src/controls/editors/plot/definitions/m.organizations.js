export default {
    name: "Number of Organizations",
    icon: 'fa-hashtag',
    description: "Return a count of organizations",
    aggs: [
        'COUNT,Count'
    ]
}