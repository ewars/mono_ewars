export default {
    name: 'Number of Location Types',
    icon: 'fa-hashtag',
    description: "Return the number of locations types",
    aggs: [
        'COUNT,Count'
    ]
}