export default {
    name: 'Number of Records',
    icon: 'fa-hashtag',
    description: 'Return a count of records',
    aggs: [
        'COUNT,Count'
    ]
}