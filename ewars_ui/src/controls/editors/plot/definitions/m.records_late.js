export default {
    name: '# of Late Records',
    icon: 'fa-hashtag',
    aggs: [
        'SUM,Sum'
    ]
}