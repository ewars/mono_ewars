export default {
    name: 'Number of Forms',
    icon: 'fa-hashtag',
    t: 'NUMERIC',
    aggs: [
        'COUNT,Count'
    ]
}