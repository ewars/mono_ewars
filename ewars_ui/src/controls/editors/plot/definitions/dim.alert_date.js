export default {
    name: 'Alert Date',
    icon: 'fa-calendar',
    f: ['ALERT_DATE'],
    m: ['ALERTS'],
    t: 'DATE'
}