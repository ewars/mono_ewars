export default {
    name: 'Record Date',
    icon: 'fa-calendar',
    f: ['RECORD_DATE'],
    m: ['RECORDS', 'FIELD'],
    t: 'DATE'
}
