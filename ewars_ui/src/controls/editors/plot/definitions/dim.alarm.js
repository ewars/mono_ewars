export default {
    name: 'Alarm',
    icon: 'fa-exclamation-triangle',
    f: ['ALARM', 'ALARM_STATUS']
};