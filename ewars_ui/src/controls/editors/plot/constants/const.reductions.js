// REDUCTIONS
export default [
    ["SUM", "Sum"],
    ["AVERAGE", "Average"],
    ["MEDIAN", "Median"],
    ["COUNT", "Count"],
    ["COUNT_D", "Count (Distinct)"],
    ["MAX", "Max. Value"],
    ["MIN", "Min. Value"],
    ["STD_DEV", "Std. Deviation"],
    ["STD_DEV_P", "Std. Deviation (Pop.)"],
    ["VAR", "Variance"],
    ["VAR_P", "Variance (Pop.)"]
];
