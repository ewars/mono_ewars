export default {
    t_value: {
        t: 'H',
        l: 'Map Settings'
    },
    showLabels: {
        t: 'bool',
        l: 'Show labels'
    },
    labelStyle: {
        t: 'select',
        l: 'Label style',
        o: [
            ['DEFAULT', 'Default'],
            ['NUMBERED', 'Numbered']
        ]
    },
    labelSize: {
        t: 'number',
        l: 'Font Size'
    },
    labelWeight: {
        t: 'select',
        l: 'Font weight',
        o: [
            ['normal', 'Normal'],
            ['bold', 'Bold']
        ]
    },
    labelColor: {
        t: 'color',
        l: 'Font colour'
    },
    valueFormat: {
        t: 'select',
        l: 'Formatting',
        o: [
            ['0', '0'],
            ['0.0', '0.0'],
            ['0.00', '0.00'],
            ['0,0.0', '0,0.0'],
            ['0,0.00', '0,0.00'],
            ['0,0', '0,0'],
            ['0%', '0%'],
            ['0.0%', '0.0%'],
            ['0.00%', '0.00%']
        ]
    }
};