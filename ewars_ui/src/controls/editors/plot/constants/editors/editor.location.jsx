const EDITOR_LOCATION = {
    name: "Location Filtering",
    modes: [
        ["LOCATION", 'Location']
    ],
}

export default EDITOR_LOCATION;