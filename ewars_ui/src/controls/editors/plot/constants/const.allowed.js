// A map of items and what items are allowed to be dropped on them as filters
export default {
    'LOCATION': ['LOCATION', 'LOCATION_TYPE', 'LOCATION_STATUS', 'LOCATION_GROUP'],
    'RECORD_DATE': ['RECORD_DATE'],
    'RECORD_SUBMISSION_DATE': ['RECORD_SUBMISSION_DATE'],
    'FORM': ['FORM', 'FORM_STATUS'],
    'USER': ['USER', 'USER_ROLE']
};
