import * as CONTROLS from '../controls/';

export default {
    LEGEND: {
        n: 'Legend',
        c: CONTROLS.LegendEditor,
        allowed: '*'
    },
    TEXT: {
        n: 'Text label',
        c: CONTROLS.TextEditor,
        allowed: '*'
    },
    SCALE: {
        n: 'Scale',
        c: CONTROLS.ScaleEditor,
        allowed: ['MAP', 'HEATMAP']
    },
    BAND: {
        n: 'Plot band',
        c: CONTROLS.BandEditor,
        allowed: ['DEFAULT', 'COLUMN']
    },
    LINE: {
        n: 'Plot line',
        c: CONTROLS.LineEditor,
        allowed: ['DEFAULT', 'COLUMN']
    },
    TITLE: {
        n: 'Title',
        c: CONTROLS.TitleEditor,
        allowed: '*'
    },
    TREND: {
        n: 'Trend line',
        c: CONTROLS.TrendEditor,
        allowed: ['DEFAULT', 'COLUMN']
    },
    AXIS: {
        n: 'Additional axis',
        c: CONTROLS.AxisEditor,
        allowed: ['DEFAULT', 'COLUMN', 'PIE']
    },
    ANNOTATION: {
        n: 'Annotations',
        c: CONTROLS.AnnotationsEditor,
        allowed: ['DEFAULT', 'COLUMN', 'PIE']
    },
    NAVIGATOR: {
        n: 'Navigator',
        c: CONTROLS.NavigatorEditor,
        allowed: ['DEFAULT', 'COLUMN']
    },
    POLYGON: {
        n: 'Polygon',
        c: CONTROLS.PolygonEditor,
        allowed: ['DEFAULT', 'MAP']
    },
    MAP_LAYER: {
        n: 'Static map layer',
        c: CONTROLS.MapLayerEditor,
        allowed: ['MAP']
    },
    M_POINT: {
        n: 'Static map points',
        c: CONTROLS.MapPointsEditor,
        allowed: ['MAP']
    },
    M_POLY: {
        n: 'Static map polygon',
        c: CONTROLS.MapPolygonEditor,
        allowed: ['MAP']
    },
    COL_MAP: {
        n: 'Colour Mapper',
        c: CONTROLS.RangeEditor,
        allowed: ['DATAPOINT']
    },
    VAL_MAP: {
        n: 'Value Mapper',
        c: CONTROLS.RangeEditor,
        allowed: ['DATAPOINT']
    },
    STACK: {
        n: 'Stack',
        c: CONTROLS.StackEditor,
        allowed: ['DEFAULT']
    }
}