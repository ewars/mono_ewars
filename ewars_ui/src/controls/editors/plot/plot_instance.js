import utils from "../../../utils/utils";

import {DEFAULTS, PRESET_NAMES} from './constants/';
import {generateId} from './utils';


class Node {
    constructor(node) {
        this.node = node;

        this.g = node.g || null; // Gropu id
        this._ = node._ || null; // node id
        this.p = node.p || null; // node parent id
        this.t = node.t || null; // node type
        this._t = node._t || null; // node sub-type
        this.n = node.n || null; // node name/code
        this.f = node.f || []; // filter settings
        this.x = node.x || null;
        this.fid = node.fid || null;
    }

    getFinal() {
        let result = utils.copy(this.node);
        result._ = this._;
        result.g = this.g;
        result.p = this.p;
        result.t = this.t;
        result._t = this._t;
        result.n = this.n;
        result.f = this.f;
        result.x = this.x;
        result.fid = this.fid;

        return result;
    };

    updateConfig(prop, value) {

    };

    isDate() {
        if (this.n == 'RECORD_DATE') return true;
        if (this.node._dt == 'DATE') return true;
        if (['ALERT_DATE', 'DATE_SUB'].indexOf(this.n) >= 0) return true;
        return false;
    };

    isLocation() {
        return this.node._dt == 'LOCATION';
    };

    isMeasure() {
        return this.node.t == 'M';
    };

    isDimension() {
        return this.node.t == 'D';
    };

    isCalculation() {
        return this.node.t == 'C';
    };

    isCalcVariable() {
        return this.node.p != null;
    };

    isField() {
        return this.n.indexOf('FIELD.') == 0;
    };

    /**
     * Is this node a filter
     * @returns {string}
     */
    isFilter() {
        return this._t == 'F';
    };

    getShortLabel() {
        if (this.t == 'C') return this.node.label || 'Calculation';
        let label;
        if (this.node.label) {
            return this.node.label;
        } else {
            if (this.n.indexOf('FIELD.') == 0) {
                // This is a field
                let labelParts = this.n.split('.');
                label = labelParts.slice(2, labelParts.length).join('.');
            }

            if (PRESET_NAMES[this.n]) {
                label = PRESET_NAMES[this.n];
            }
        }

        return label;

    }
}

let instance = null;

class PlotInstance {

    // static methodMap = {};

    constructor(props) {
        instance = this;
        this._ = props._;
        this.type = props.type;

        this.nodes = (props.n || []).map(node => {
            return new Node(node);
        });

        this.groups = props.g || [];

        this.config = props || {};
        this.style = props.style || {};

        this.listeners = {}; // listeners consist of an id, path or otherwise
        this._listeners = [];

        this.axis = (props.n || []).filter(node => {
            return node.t == 'A';
        })

    }

    addGroup() {
        this.groups.push({
            _: generateId(),
            label: "New Group"
        })
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    addColourNode(nid, data) {
        data._ = generateId();
        data.p = nid;
        data._t = 'C'; // Colour
        data.colours = [];

        this.nodes.push(new Node(data));
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    addDetailNode(nid, data) {
        data._ = generateId();
        data.p = nid;
        data._t = 'D';

        this.nodes.push(new Node(data));
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    addSizeNode(nid, data) {
        data._ = generateId();
        data.p = nid;
        data._t = 'S';
        data.sizes = [];

        this.nodes.push(new Node(data));
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    getFinal() {
        let result = {
            ...this.config,
            n: this.nodes.map(node => {
                return node.getFinal();
            }),
            g: this.groups
        };

        return result;
    };

    /**
     * Remove listener from the class instance
     * @param callback
     */
    removeListener(id, callback) {
        let rIndex;
        this.listeners.forEach((item, index) => {
            if (item == callback) rIndex = index;
        });
        if (rIndex != null) this.listeners.splice(rIndex, 1);

    };

    updateProp(prop, value) {
        this.config[prop] = value;
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    addCalculation(gid) {
        let newNode = {
            _: generateId(),
            t: 'C',
            fx: '',
            g: gid
        }

        this.nodes.push(new Node(newNode));
        window.dispatchEvent(new CustomEvent("plutupdated"));
    };

    addNodeToGroup(gid, data) {
        let newNode = utils.copy(data);
        newNode.g = gid;
        newNode._ = generateId();
        newNode.fid = data.fid || undefined;

        if (newNode.c) {
            if (newNode.c.type == 'DATE') {
                newNode._dt = 'DATE';
                newNode.interval = 'DAY';
            }
        }

        let axis = this.nodes.filter(node => {
            return node.t == 'A';
        });

        if (axis.length > 0) {
            newNode.x = axis[0]._;
        }

        if (newNode.t == 'M') newNode.agg = 'SUM';
        if (this.type == 'DEFAULT' && newNode.t == 'M') newNode.render = 'line';

        if (newNode.n == 'RECORD_DATE') {
            let newFilter = {
                _: generateId(),
                t: 'D',
                n: 'RECORD_DATE',
                p: newNode._,
                _dt: 'DATE',
                _t: 'F',
                f: ['{NOW}-30D,{NOW}']
            };
            this.nodes.push(new Node(newFilter));
        }

        if (newNode.n.indexOf('FIELD.') >= 0) {
            let formId = newNode.n.split('.')[1];
            let formFilter = {
                _: generateId(),
                t: 'D',
                n: 'FORM',
                _t: 'F',
                f: [`EQ:${formId}`],
                p: newNode._
            };
            this.nodes.push(new Node(formFilter))
        }

        this.nodes.push(new Node(newNode));
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    /**
     * Get a node by it's id
     * @param nid
     * @returns {*}
     */
    getNode(nid) {
        return this.nodes.filter(node => {
            return node._ == nid;
        })[0] || null;
    };

    /**
     * Get all axis from the configuration
     */
    getAxis() {
        return this.nodes.filter(node => {
            return node.t == 'A';
        })
    };

    /**
     * Update the axis that a node is pointed to
     * @param nid
     * @param aid
     */
    updateNodeAxis(nid, aid) {
        this.nodes.forEach(node => {
            if (node._ == nid) {
                node.x = aid;
            }
        })
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    updateNodeProp(nid, prop, value) {
        this.nodes.forEach(node => {
            if (node._ == nid) {
                if ([null, undefined, ''].indexOf(value) >= 0) {
                    delete node.node[prop];
                } else {
                    if (prop == 'x') {
                        node.x = value;
                    } else if (prop == 'type') {
                        node._t = value;
                    } else {
                        node.node[prop] = value;
                    }
                }
            }
        })
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    /**
     * Get the list of filters for specific node
     * @param nid
     */
    getFiltersForNode(nid) {
        return this.nodes.filter(node => {
            return (node.p == nid && ['D', 'M', 'C'].indexOf(node.t) >= 0 && node._t == 'F');
        })
    };

    /**
     * Add a node to a group based on a sibling
     * @param nid
     * @param data
     */
    addSibling(nid, data) {
        let sibling = this.getNode(nid);

        this.addNodeToGroup(sibling.g, data);
    };

    removeNode(nid) {
        let newNodes = this.nodes.filter(node => {
            return node._ != nid && node.p != nid;
        })
        this.nodes = newNodes;
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    /**
     * Add a filter to a node
     * @param nid
     * @param data
     */
    addFilterToNode(nid, data) {
        data._ = generateId();
        data.p = nid;
        data._t = 'F';
        data.f = [];

        this.nodes.push(new Node(data));

        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    /**
     * Update a filter
     * @param nid
     * @param data
     */
    updateFilter(nid, data) {
        this.nodes.forEach(node => {
            if (node._ == nid) {
                node.f = data;
            }
        });
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };


    hasAxis() {
        return ['TABLE', 'HEATMAP', 'DEFAULT'].indexOf(this.type) >= 0;
    };


    /**
     * Validate the configuration
     */
    validate() {

    };

    /**
     * Can this chart type have multiple groups specified
     * @returns {boolean}
     */
    canHaveMultipleGroups() {
        return ['DEFAULT'].indexOf(this.type) >= 0;
    };

    getDimensionsForGroup(gid) {
        return this.nodes.filter(node => {
            return (node.g == gid && node.p == null && node.t == 'D');
        })
    };

    getMeasuresForGroup(gid) {
        return this.nodes.filter(node => {
            return (node.g == gid && node.p == null && ['M', 'C'].indexOf(node.t) >= 0);
        })
    };

    /**
     * Get all variables for a calculation
     * @param cid
     */
    getCalculationVariables(cid) {
        return this.nodes.filter(node => {
            return node.p == cid;
        })
    };

    /**
     * Add a new node to a calculation
     * @param nid
     * @param data
     */
    addNodeToCalculation(nid, data) {
        data._ = generateId();
        data.p = nid;
        data.var = '';
        data.agg = 'SUM';

        if (data.t == 'M') data.agg = 'SUM';

        let newNode = new Node(data);

        if (newNode.isField()) {
            let formId = data.n.split('.')[1];
            let formFilter = {
                _: generateId(),
                t: 'D',
                _t: 'F',
                n: 'FORM',
                p: data._,
                f: [`EQ:${formId}`]
            };

            this.nodes.push(new Node(formFilter));
        }

        this.nodes.push(newNode);

        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    addControl(type) {
        let newNode = {
            _: generateId(),
            t: 'X',
            _t: type
        };

        if (type == 'AXIS') {
            newNode.t = 'A';
            newNode._t = null;
            delete newNode.type;
        }


        this.nodes.push(new Node(newNode));
        window.dispatchEvent(new CustomEvent("plotupdated"));
    };

    /**
     * Get Node detail points
     * @param nid
     */
    getNodeDetails(nid) {
        return this.nodes.filter(node => {
            return node.p == nid && node._t == 'D';
        })
    };

    /**
     * Get Colour ramp for a node
     * @param nid
     */
    getNodeColours(nid) {
        return this.nodes.filter(node => {
            return node.p == nid && node._t == 'C';
        })
    };

    /**
     * Get sizers for a node
     * @param nid
     */
    getNodeSizers(nid) {
        return this.nodes.filter(node => {
            return node.p == nid && node._t == 'S';
        })
    };

    addCalcVariable(nid, data) {
        let newNode = utils.copy(data);
        newNode._ = generateId();
        newNode.p = nid;
        newNode._t = 'CX';
        newNode.agg = 'SUM';

        if (newNode.n.indexOf('FIELD.') >= 0) {
            // add form filter

            let formId = newNode.n.split('.')[1];
            let filterNode = {
                _: generateId(),
                p: newNode._,
                n: 'FORM',
                t: 'D',
                _t: 'F',
                f: [`EQ:${formId}`]
            };

            this.nodes.push(new Node(filterNode));
        }

        this.nodes.push(new Node(newNode));
        window.dispatchEvent(new CustomEvent("plotupdated"));
    }


}

export {
    instance,
    PlotInstance
}

export default PlotInstance;
