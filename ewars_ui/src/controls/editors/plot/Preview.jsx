import React from 'react';
import Highcharts from "highcharts";

import Moment from "moment";

import * as d3 from "d3";
// import * as viz from '../../visualisation';

import {PRESETS, PRESET_NAMES} from './constants/';

const COLOURS = [
    "#4D4D4D",
    "#5DA5DA",
    "#FAA43A",
    "#60BD68",
    "#F17CB0",
    "#B2912F",
    "#B276B2",
    "#DECF3F",
    "#F15854"
];

if (Highcharts) {
    Highcharts.setOptions({
        global: {
            useUTC: true
        }
    });
}

    // constructor(el, props, ds) {
    //     console.log(el);
    //     console.log(props);
    //     console.log(ds);
    //     let chartOptions = {};
    //     chartOptions.chart.renderTo = this._el;
    //     this._chart = new Highcharts.Chart(chartOptions);
    //
    // }

const isSet = (val, fallback) => {
    if (val == undefined || val == null || val == '') return fallback;
    return val;
}

class Preview extends React.Component {
    static defaultProps = {
        el: null,
        ds: null,
        data: null
    };

    constructor(props) {
        super(props);

        this._chart = null;
        this._viz = null;
    }

    componentDidMount() {
        let renderClass;
        switch(this.props.data.type) {
            case 'PIE':
                renderClass = viz.PieChart;
                break;
            case 'COLUMN':
                renderClass = viz.ColumnChart;
                break;
            case 'FUNNEL':
                renderClass = viz.FunnelChart;
                break;
            case 'PYRAMID':
                renderClass = viz.PyramidChart;
                break;
            case 'HEATMAP':
                renderClass = viz.HeatMap;
                break;
            case 'TABLE':
                renderClass = viz.TableDefault;
                break;
            case 'DEFAULT':
                renderClass = viz.DefaultChart;
                break;
            case 'DATAPOINT':
                renderClass = viz.Default;
                break;
            case 'MAP':
                renderClass = viz.DefaultMap;
                break;
            default:
                renderClass = viz.DefaultChart;
                break;
        }

        this._viz = new renderClass(
            this.props.data,
            null,
            this._el
        )

    };

    componentWillReceiveProps(nextProps) {
        if (this._viz) {
            // this._viz.update(nextProps.data);
        }
    };

    render() {
        let style = {
            height: '100%',
            width: '100%',
            backgroundColor: isSet(this.props.data.backgroundColor, undefined),
            padding: isSet(this.props.data.margin, undefined),
            paddingTop: isSet(this.props.data.marginTop, undefined),
            paddingRight: isSet(this.props.data.marginRight, undefined),
            paddingBottom: isSet(this.props.data.marginBottom, undefined),
            paddingLeft: isSet(this.props.data.marginLeft, undefined)
        };

        return (
            <div
                style={style}
                className="chart"
                id="chart"
                ref={(el) => {this._el = el;}}>
            </div>
        )
    }

}

export default Preview;
