import NuggetDetail from './nugget.detail.jsx';
import NuggetFilter from './nugget.filter.jsx';
import NuggetColour from './nugget.colour.jsx';
import NuggetSize from './nugget.size.jsx';

export {
    NuggetDetail,
    NuggetFilter,
    NuggetColour,
    NuggetSize
}
