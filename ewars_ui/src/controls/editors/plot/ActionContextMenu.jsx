import React from "react";

class ActionContextMenu extends React.Component {
    static defaultProps = {
        shown: false
    }

    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.shown) return false;

        return (
            <div className="action-context-menu">
                {this.props.children}
            </div>
        )
    }
}

export default ActionContextMenu;
