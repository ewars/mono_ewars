import React from "react";
import Wing from './wing.jsx';
import CardListItem from './data.card.jsx';

import {instance} from '../plot_instance';
import ActionGroup from "../../../c.actiongroup.jsx";


const ACTIONS = [
    ['fa-caret-right', 'TOGGLE']
];


class CalcGroup extends React.Component {
    static defaultProps = {
        gid: null,
        nid: null,
        data: null,
        filters: {}
    };

    constructor(props) {
        super(props)
    }

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData('n'));

        if (data.t != 'M') return;
        instance.addCalcVariable(this.props.nodeId, data);
    };

    _onFormulaChange = (e) => {
        let value = e.target.value;
        instance.updateNodeProp(this.props.nodeId, 'fx', value);
    };

    _toggleVis = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'block' ? 'none' : 'block';
    };

    _action = (action) => {
        this._toggleVis()
    };

    render() {

        let items = instance.getCalculationVariables(this.props.nodeId);
        let node = instance.getNode(this.props.nodeId);


        let hasItems = items.length > 0 ? true : false;
        let onDrag, onDrop;
        if (!hasItems) {
            onDrag = this._onDragOver;
            onDrop = this._onDrop;
        }

        let nodes = [];
        if (hasItems) {
            let lastItem;
            items.forEach((item, index) => {
                nodes.push(
                    <Wing
                        left={true}
                        gid={this.props.nodeId}
                        key={item._ + 'L'}
                        calc={true}
                        nid={item._}/>
                )
                nodes.push(
                    <CardListItem
                        nodeId={item._}
                        key={item._}
                        calc={true}
                        index={index}
                        onRemove={this._removeItem}/>
                )
                lastItem = item;
            })

            nodes.push(
                <Wing
                    right={true}
                    key={'RWING'}
                    gid={this.props.nodeId}
                    calc={true}
                    nid={lastItem._}/>
            )
        }

        return (
            <div className="data-group">
                <div className="row data-group-header" style={{background: 'rgba(58,32,1,1)'}}>
                    <div className="column grid-title">Calculation</div>
                    <ActionGroup
                        right={true}
                        height="28px"
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div
                    className="data-group-calc"
                    style={{height: '30px'}}>
                    <div className="column">
                        <div className="row">
                            <div className="column"
                                 style={{maxWidth: "20px", textAlign: "center", background: "black"}}>
                                <i className="fal fa-calculator"></i>
                            </div>
                            <div className="column">
                                <input
                                    type="text"
                                    value={node.node.fx}
                                    onChange={this._onFormulaChange}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    ref={(el) => {
                        this._el = el;
                    }}
                    className="data-group-body"
                    onDragOver={onDrag}
                    onDrop={onDrop}>
                    <div className="column">
                        <div
                            style={{
                                background: "rgba(0,0,0,0.1)",
                                overflowY: 'scroll',
                                overflowX: 'show',
                                position: 'relative'
                            }}
                            className="row">
                            {nodes}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CalcGroup;
