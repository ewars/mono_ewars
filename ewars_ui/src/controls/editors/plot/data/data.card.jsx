import React from "react";
import CardItem from '../card/card.item.jsx';

import {isX} from '../utils';

import {instance} from '../plot_instance';
import ActionGroup from "../../../c.actiongroup.jsx";

/**
 * Wraps a card item in the group box
 */
class CardListItem extends React.Component {
    static defaultProps = {
        index: null,
        data: null,
        filters: null,
        nextMeas: false,
        ax: {},
        isCalcVariable: false,
        gid: null,
        nid: null,
        cid: null,
        axis: []
    };

    constructor(props) {
        super(props)
    }

    _onRemove = () => {
        this.props.onRemove(this.props.data[0])
    };

    _onAxisChange = (e) => {
        console.log(this.props);
        instance.updateNodeAxis(this.props.nodeId, e.target.value);
    };

    render() {
        let node = instance.getNode(this.props.nodeId);

        let axisOptions;
        if (['HEATMAP', 'TABLE'].indexOf(instance.type) >= 0) {
            axisOptions = [
                <option value="x">X axis</option>,
                <option value="y">Y axis</option>,
                <option value="DAGG">Disaggregate</option>
            ]
        } else {

            axisOptions = instance.getAxis().map(item => {
                let label = item.node.label + ' (' + ([true, "true"].indexOf(item.x) >= 0 ? 'x' : 'y') + ')';

                return (
                    <option value={item._}>{label}</option>
                )
            });

            if (node.t == 'D') {
                axisOptions.push(
                    <option value="DAGG">Disaggregate</option>
                )
            }
        }

        let hasAxis = instance.hasAxis();
        let isCalcVariable = (node.p && ['D', 'M', 'C'].indexOf(node.t) >= 0 && node.g == null);

        return (
            <div className="column" style={{maxWidth: "250px"}}>
                <div className="row">
                    <div className="column">
                        <div className="group-card-wrapper">
                            <CardItem
                                nodeId={this.props.nodeId}/>
                        </div>
                    </div>
                </div>
                {!isCalcVariable && hasAxis ?
                    <div className="row" style={{maxHeight: "35px", background: "#333333"}}>
                        <div className="column">
                            <select
                                onChange={this._onAxisChange}
                                className="card-axis"
                                name="axis"
                                value={node.x}>
                                {axisOptions}
                            </select>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

export default CardListItem;
