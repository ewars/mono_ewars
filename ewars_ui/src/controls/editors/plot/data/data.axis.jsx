import React from "react";

import {CONTROLS} from '../constants';

import {instance} from '../plot_instance';
import ActionGroup from "../../../c.actiongroup.jsx";

const CONTROL_ACTIONS = [
    ['fa-caret-right', 'TOGGLE'],
    ['fa-trash', 'DELETE']
];

class ControlField extends React.Component {
    render() {
        return (
            <div className="control-field">
                <div className="row">
                    <div className="column" style={{maxWidth: "30%"}}>
                        {this.props.label}
                    </div>
                    <div className="column">{this.props.children}</div>
                </div>
            </div>
        )
    }
}

const FIELDS = [
    ['label', 'text', 'Label'],
    ['type', 'select', 'Axis Type'],
    ['ceiling', 'number', 'Ceiling'],
    ['floor', 'number', 'Floor'],
    ['max', 'number', 'Max'],
    ['min', 'number', 'Min'],
    ['opposite', 'bool', 'Opposite'],
    ['axis', 'select', 'Axis'],
    ['visible', 'bool', 'Visible'],
    ['labelRotation', 'number', 'Label rotation'],
    ['format', 'text', 'Label format'],
    ['label.color', 'color', 'Label color'],
    ['tickColor', 'color', 'Tick color'],
    ['logarithmic', 'bool', 'Logarithmic']
];

class ControlAxis extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = (action) => {
        switch (action) {
            case 'DELETE':
                instance.removeNode(this.props.nodeId);
                return;
            case 'TOGGLE':
                this._toggle();
                return;
            default:
                return;
        }
    };

    _toggle = () => {
        if (this._el) this._el.style.display = this._el.style.display == 'none' ? 'block' : 'none';
    };

    _onChange = (e) => {
        instance.updateNodeProp(this.props.nodeId, e.target.name, e.target.value);
    };

    render() {
        let axis = instance.getNode(this.props.nodeId);

        let showNumField = axis._t == 'numeric' ? true : false;

        return (
            <div className="column" style={{maxWidth: "33.3%", minWidth: "33.3%"}}>

                <div className="data-group">
                    <div className="row data-group-header" style={{background: '#6f1842'}}>
                        <div className="column grid-title">{axis.node.label}</div>
                        <ActionGroup
                            actions={CONTROL_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div
                        ref={(el) => {
                            this._el = el;
                        }}
                        style={{height: 'auto', minHeight: '0'}}
                        className="data-group-body">
                        <div className="control-editor">
                            <div className="control-editor">
                                <ControlField label="Label">
                                    <input
                                        name="label"
                                        type="text"
                                        value={axis.node.label}
                                        onChange={this._onChange}/>
                                </ControlField>
                                <ControlField label="Type">
                                    <select
                                        value={axis._t}
                                        name="type"
                                        onChange={this._onChange}>
                                        <option value="date">Date</option>
                                        <option value="category">Categoric</option>
                                        <option value="numeric">Numeric</option>
                                        <option value="percentage">Percentage</option>
                                        <option value="logarithmic">Logarithmic</option>
                                    </select>
                                </ControlField>
                                {axis._t == 'date' ?
                                    <ControlField label="Date format">
                                        <select
                                            onChange={this._onChange}
                                            name="dateFormat"
                                            value={axis.node.dateFormat}>
                                            <option value="YYYY-MM-DD">YYYY-MM-DD (2017-01-01)</option>
                                            <option value="YYYY-[W]W">YYYY-[W]W (2017-W1)</option>
                                            <option value="YYYY/MM/DD">YYYY/MM/DD (2017/01/01)</option>
                                            <option value="YYYY-MM">YYYY-MM (2017-01)</option>
                                            <option value="[W]W">[W]W (W01)</option>
                                        </select>
                                    </ControlField>
                                    : null}
                                <ControlField label="Axis">
                                    <select
                                        name="x"
                                        value={axis.x}
                                        onChange={this._onChange}>
                                        <option value={true}>X Axis</option>
                                        <option value={false}>Y Axis</option>
                                    </select>
                                </ControlField>
                                <ControlField label="Opposite">
                                    <select
                                        name="opposite"
                                        value={axis.node.opposite || false}
                                        onChange={this._onChange}>
                                        <option value={false}>False</option>
                                        <option value={true}>True</option>
                                    </select>
                                </ControlField>
                                {showNumField ?
                                    <ControlField label="Ceiling">
                                        <input
                                            type="number"
                                            name="ceil"
                                            value={axis.node.ceil || 0}
                                            onChange={this._onChange}/>
                                    </ControlField>
                                    : null}
                                {showNumField ?
                                    <ControlField label="Floor">
                                        <input type="number"
                                               name="floor"
                                               value={axis.node.floor || ''}
                                               onChange={this._onChange}/>
                                    </ControlField>
                                    : null}
                                {showNumField ?
                                    <ControlField label="Max">
                                        <input type="number"
                                               name="max"
                                               value={axis.node.max || ''}
                                               onChange={this._onChange}/>
                                    </ControlField>
                                    : null}
                                {showNumField ?
                                    <ControlField label="Min">
                                        <input type="number"
                                               name="min"
                                               value={axis.node.min || 0}
                                               onChange={this._onChange}/>
                                    </ControlField>
                                    : null}
                                {!showNumField ?
                                    <ControlField label="Label rotation">
                                        <select
                                            value={axis.node.labelRotation || 0}
                                            onChange={this._onChange}
                                            name="labelRotation">
                                            <option value={0}>0˚</option>
                                            <option value={-45}>-45˚</option>
                                            <option value={-90}>-90˚</option>
                                            <option value={45}>45˚</option>
                                            <option value={90}>90˚</option>
                                        </select>
                                    </ControlField>
                                    : null}
                                <ControlField label="Label Colour">
                                    <input type="color"
                                           value={axis.node.color || '#333333'}
                                           name="color"
                                           onChange={this._onChange}/>
                                </ControlField>
                                <ControlField label="Tick Colour">
                                    <input type="color"
                                           value={axis.node.tickColor || '#333333'}
                                           name="tickColour"
                                           onChange={this._onChange}/>
                                </ControlField>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ControlAxis;
