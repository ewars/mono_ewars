import React from "react";

// xD, xM, yD, yM

// Describes what combinations of dimnensions and measures based on positioning
// can be used with which charts
// xDim, xM, yD, yM
const MATRIX = {
    "1.0.0.1": ["LINE", "COL", "SCATTER", "HEATMAP", "TABLE", "PIE", "MAP"],
    "1.0.0.n": ["LINE", "COL", "SCATTER", "HEATMAP", "TABLE", "PIE", "MAP"],
    "1.0.1.n": ["LINE", "SCATTER", "TABLE", "MAP"],
    "1.0.1.1": ["LINE", "SCATTER", "TABLE", "MAP"],

    "0.0.0.n": ["COL", "PIE", "TABLE", "MAP"],
    "0.0.0.1": ["COL", "TABLE", "MAP"],
    "0.0.1.1": ["COL", "PIE", "TABLE", "MAP"],
    "0.0.n.n": ["COL", "PIE", "TABLE", "MAP"],
    "0.0.1.n": ["COL", "PIE", "TABLE", "MAP"],
    "0.0.n.1": ["COL", "TABLE", "MAP"],

    "n.0.0.1": ["COL", "TABLE", "MAP"],
    "n.0.0.n": ["COL", "TABLE", "MAP"],
    "0.1.0.0": ["BAR", "TABLE", "MAP"],
    "0.n.0.0": ["BAR", "TABLE"],
    "1.n.0.0": ["BAR", "TABLE"],
    "n.n.0.0": ["BAR", "TABLE"]
};

class DataSource {
    constructor(props) {
        this.props = props;
        this._data = null;

        this._figure();

        ewars.z.subscribe("PE", this._onChange);
    }

    _onChange = () => {
        this.props = ewars.z.getState("PE");
    };

    update = (props) => {
        this.props = props;
        this._figure();
    };

    getProps = () => {
        return this.props;
    };

    getMeasures = () => {
        return this._measures;
    };

    getDimensions = () => {
        return this._dimensions;
    };

    _figure = () => {
        let xD = 0, xM = 0, yD = 0, yM = 0;

        this.props.x.forEach((item) => {
            if (item[1] == "D") xD++;
            if (item[1] != "D") xM++;
        })

        this.props.y.forEach((item) => {
            if (item[1] == "D") yD++;
            if (item[1] != "D") yM++;
        })

        if (xD > 1) xD = "n";
        if (xM > 1) xM = "n";
        if (yD > 1) yD = "n";
        if (yM > 1) yM = "n";

        let tag = `${xD}.${xM}.${yD}.${yM}`;

        let dimensions = [],
            measures = [],
            filters = this.props.filters || [];

        this.props.x.forEach((item) => {
            if (item[1] == "D") dimensions.push(item);
            if (item[1] != "D") measures.push(item);
        })

        this.props.y.forEach((item) => {
            if (item[1] == "D") dimensions.push(item);
            if (item[1] != "D") measures.push(item);
        })

        this._dimensions = dimensions;
        this._measures = measures;

        ewars.tx("com.ewars.plot", [this.props.x, this.props.y, filters])
            .then((resp) => {
                this._data = resp;
                ewars.emit("DATA_UPDATED", this._data);
            })
    };

    getData = () => {

        return this._data;
    }


}

export default DataSource;
