import React from "react";

import STYLES from "./styles";

class CenterDrop extends React.Component {
    static defaultProps = {
        allowedTypes: ["H", "V", "W"],
        data: {},
        widget: false,
        root: false
    };

    constructor(props) {
        super(props);

        ewars.subscribe("SHOW_DROPS", this._showDrops);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    componentWillUnmount() {
        ewars.unsubscribe("SHOW_DROPS", this._showDrops);
        ewars.unsubscribe("HIDE_DROPS", this._hideDrops);
    }

    _showDrops = (type) => {
        if (this.props.allowedTypes.indexOf(type) >= 0) {
            if (this._el) {
                this._el.style.display = "block";
            }
        }
    };

    _hideDrops = () => {
        if (this._el) {
            this._el.style.display = "none";
        }
    };

    _onDragOver = (e) => {
        e.preventDefault();
        if (this._elIcon) this._elIcon.style.transform = "scale(1.5)";
    };

    _onDragLeave = (e) => {
        e.preventDefault();
        if (this._elIcon) this._elIcon.style.transform = "none";
    };

    _onDrop = (e) => {
        this._el.style.display = "none";
        let data = JSON.parse(e.dataTransfer.getData("e"));

        let item = {};

        switch (data.t) {
            case "V":
                item.i = [];
                item.w = 100;
                item.h = 100;
                item.t = "V";
                break;
            case "H":
                item.i = [];
                item.w = 100;
                item.h = 100;
                item.t = "H";
                break;
            default:
                item.w = 100;
                item.h = 100;
                item.t = "W";
                item.c = {
                    type: data.t
                }
                break;
        }

        ewars.emit("HIDE_CONTROLS");
        if (this.props.root) {
            ewars.z.dispatch("E", "ADD_ROOT", item);
        } else if (this.props.widget) {
            // We're replacing a width with something else
            ewars.z.dispatch("E", "REPLACE_NODE", [this.props.data._, item]);
        } else {
            ewars.z.dispatch("E", "ADD_CHILD", [this.props.data._, item]);
        }
    };

    render() {
        let style = {display: "none"};
        return (
            <div
                ref={(el) => {
                    this._el = el;
                }}
                onDragOver={this._onDragOver}
                onDragLeave={this._onDragLeave}
                onDrop={this._onDrop}
                style={{...STYLES.CENTER_DROP, ...style}}>
                <i
                    ref={(el) => {
                        this._elIcon = el;
                    }}
                    style={{
                        position: "absolute",
                        top: "50%",
                        left: "50%",
                        marginLeft: "-12px",
                        marginTop: "-12px",
                        color: "#F2F2F2",
                        fontSize: "25px",
                        transformOrigin: "50% 50%"
                    }}
                    className="fal fa-download"></i>
            </div>
        )
    }
}

export default CenterDrop;
