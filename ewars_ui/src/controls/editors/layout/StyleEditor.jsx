import React from 'react';

import TextField from "../../fields/f.text.jsx";
import ColourThresholds from "../../fields/f.thresholds.colour.jsx";

const STYLE = {
    t_container_style: {
        l: "Container Styling",
        t: "H"
    },
    border: {
        l: "Border",
        reg: "{size} {style} {color}",
        // This property can break out into other properties
        c: {
            size: ["SIZE", "0px", "Stroke"],
            style: ["OPTION", "solid", "Style", "solid|dotted|dashed"],
            color: ["COLOR", "#000000", "Color"]
        },
        __: [
            "borderTop",
            "borderRight",
            "borderBottom",
            "borderLeft"
        ]
    },
    margin: {
        l: "Margin",
        reg: "{top} {right} {bottom} {left}",
        __: [
            "marginTop",
            "marginRight",
            "marginBottom",
            "marginLeft"
        ]
    },
    padding: {
        l: "Padding",
        reg: "{top} {right} {bottom} {left}",
        __: [
            "paddingTop",
            "paddingRight",
            "paddingBottom",
            "paddingLeft"
        ]
    },
    boxShadow: {
        l: "Box Shadow",
        reg: "{xOffset} {yOffset} {blur} {color}",
        c: {
            xOffset: ["SIZE", "0px", "X"],
            yOffset: ["SIZE", "0px", "y"],
            blur: ["SIZE", "0px", "blur"],
            color: ["COLOR", "0px", "color"]
        }
    },
    backgroundColor: {
        l: "Bgd Colour",
        reg: "{color}",
        c: {
            color: ["COLOR", "transparent", "Color"]
        }
    }
};

const TABLE_STYLE = {
    borderBottom: "1px solid rgba(0,0,0,0.2)"
}

const LABEL_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    borderRight: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "right",
    width: "25%",
    background: "rgba(0,0,0,0.2)"
};

const INPUT_STYLE = {
    borderTop: "1px solid rgba(0,0,0,0.2)",
    padding: "4px",
    textAlign: "left"
};

const SUB_LABEL_STYLE = {
    borderLeft: "12px solid rgba(0,0,0,0.2)",
    ...LABEL_STYLE
}

const INPUT_TXT_STYLE = {
    display: "block",
    border: "none",
    padding: "0px",
    margin: "0px",
    background: "transparent",
    borderRadius: "0px"
}

const HEADER_SECTION_STYLE = {
    padding: "8px 5px 5px 5px",
    fontWeight: "bold",
    textAlign: "left",
    textTransform: "uppercase",
    background: "rgba(0,0,0,0.5)"
};

const SELECT_STYLE = {
    border: "none",
    width: "100%",
    height: "100%",
    margin: 0,
    padding: 0,
    fontSize: "12px",
    background: "transparent",
    color: "#FFFFFF"
}

class Styler extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            edit: false,
            value: "",
            childs: false,
            showEditor: false
        }

        ewars.subscribe("CLOSE_EDITS", this._closeEdit)
    }

    componentWillUnmount() {
        ewars.unsubscribe("CLOSE_EDITS", this._closeEdit);
    }


    _edit = () => {
        ewars.emit("CLOSE_EDITS");
        this.setState({
            edit: true
        })
        // this._ref.focus();
    };

    _closeEdit = () => {
        this.setState({
            edit: false
        })
    };

    _onChange = (e) => {
        this.props.onChange(e.target.name, e.target.value);
    };

    _expand = () => {
        this.setState({
            childs: !this.state.childs
        })
    };

    _action = (action) => {
        if (action == "CLOSE") {
            this.setState({
                showEditor: false
            })
        }
    };

    render() {
        let valueStyle = {display: this.state.edit ? "none" : "block", height: "100%", width: "100%"};

        let value = this.props.styleData[this.props.node] || "";

        let editor;

        if (!this.props.data.t) {
            editor = (
                <input
                    style={INPUT_TXT_STYLE}
                    name={this.props.node}
                    onChange={this._onChange}
                    className=""
                    placeholder="..."
                    type="text"
                    value={value}/>
            )
        } else if (this.props.data.t == "o") {
            editor = (
                <select style={SELECT_STYLE} name={this.props.node} onChange={this._onChange} value={value}>
                    {this.props.data.o.map((item) => {
                        return (
                            <option value={item[0]}>{item[1]}</option>
                        )
                    })}
                </select>
            )
        } else if (this.props.data.t == "vmap") {
            editor = (
                <div>
                    <ewars.d.Button
                        onClick={() => {
                            this.setState({
                                showEditor: true
                            })
                        }}
                        icon="fa-pencil"/>
                    <ewars.d.Shade
                        onAction={this._action}
                        shown={this.state.showEditor}>
                        <ColourThresholds/>
                    </ewars.d.Shade>
                </div>
            )
        }

        return (
            <tbody>
            <tr>
                <th style={LABEL_STYLE}>
                    <ewars.d.Row>
                        {this.props.data.__ ?
                            <ewars.d.Cell width="10px" onClick={this._expand} style={{textAlign: "center"}}>
                                <i className={"fal " + (this.state.childs ? "fa-caret-down" : "fa-caret-right")}></i>
                            </ewars.d.Cell>
                            : null}
                        <ewars.d.Cell>
                            {this.props.data.l}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </th>
                <td style={INPUT_STYLE}>
                    {editor}
                </td>
            </tr>
            {this.state.childs ?
                this.props.data.__.map((item) => {
                    return (
                        <tr>
                            <th style={SUB_LABEL_STYLE}>{item}</th>
                            <td style={INPUT_STYLE}>
                                <input
                                    style={INPUT_TXT_STYLE}
                                    name={item}
                                    onChange={this._onChange}
                                    className=""
                                    placeholder="..."
                                    type="text"
                                    value={this.props.styleData[item] || ""}/>
                            </td>
                        </tr>
                    )
                })
            : null}
            </tbody>
        )
    }
}

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <th style={HEADER_SECTION_STYLE} colSpan="2">{__(this.props.data.l)}</th>
            </tr>
        )
    }
}


class StyleEditor extends React.Component {
    static defaultProps = {
        data: {},
        mode: "DEFAULT",
        defaults: true
    };

    constructor(props) {
        super(props);

        this.state = {
            search: ""
        }
    }

    _onSearchChange = (e) => {
        this.setState({
            search: e.target.value
        })
    };

    onChange = (prop, value) => {
        let data = ewars.copy(this.props.data.style || {});
        if (value == "") {
            if (data[prop]) delete data[prop];
        } else {
            data[prop] = value;
        }
        if (this.props.mode == "main") {
            ewars.z.dispatch("E", "UPDATE_SETTING", ["style", data]);
        } else {
            ewars.z.dispatch("E", "WIDGET_PROP_CHANGE", [this.props.id, "style", data]);
        }
    };

    render() {
        let items = [];

        if (this.props.ext) {
            for (let prop in this.props.ext) {
                if (this.props.ext[prop].t == "H") {
                    items.push(
                        <Header data={this.props.ext[prop]}/>
                    )
                } else {
                    items.push(
                        <Styler
                            node={prop}
                            onChange={this.onChange}
                            data={this.props.ext[prop]}
                            styleData={this.props.data.style || {}}/>
                    )
                }
            }
        }

        if (this.props.defaults) {
            for (let prop in STYLE) {
                if (STYLE[prop].t == "H") {
                    items.push(
                        <Header data={STYLE[prop]}/>
                    )
                } else {
                    items.push(
                        <Styler
                            node={prop}
                            onChange={this.onChange}
                            data={STYLE[prop]}
                            styleData={this.props.data.style || {}}/>
                    )
                }
            }
        }


        return (
            <ewars.d.Panel>
                <table width="100%" style={TABLE_STYLE}>
                    {items}
                </table>
            </ewars.d.Panel>
        )

    }
}

export default StyleEditor;
