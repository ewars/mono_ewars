import React from "react";

import DropArea from "./DropArea.jsx";
import VBox from "./VBox.jsx";
import Widget from "./Widget.jsx";
import CenterDrop from "./CenterDrop.jsx";
import ContextMenu from "./ContextMenu.jsx";

import STYLES from "./styles"

const ACTIONS = [
    ["fa-trash", "DELETE"],
    ["fa-times", "CLOSE"]
]

class HBoxItem extends React.Component {
    _isResizing = false;

    static defaultProps = {
        data: {},
        isLast: false
    };

    constructor(props) {
        super(props);

        ewars.subscribe("SHOW_DROPS", this._showDrops);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    _showDrops = (type) => {
        if (["V", "W"].indexOf(type) >= 0) {
            if (this._leftEl) this._leftEl.style.display = "block";
            if (this._rightEl) this._rightEl.style.display = "block";
        }
    };

    _hideDrops = () => {
        if (this._leftEl) this._leftEl.style.display = "none";
        if (this._rightEl) this._rightEl.style.display = "none";
    };

    _onMouseMove = (e) => {
        if (e.buttons > 0) {
            if (this._isResizing) {
                let offsetX = this._el.getBoundingClientRect().left;
                let parentWidth = this._el.parentNode.getBoundingClientRect().width;

                let newWidth = e.clientX - offsetX;
                let newPerc = (newWidth / parentWidth) * 100;

                ewars.z.dispatch("E", "RESIZER", [this.props.data._, newPerc]);
            }
        } else {
            this._isResizing = false;
            window.removeEventListener("mousemove", this._onMouseMove);
        }
    };

    _onMouseDown = (e) => {
        e.preventDefault();
        this._isResizing = true;
        window.addEventListener("mousemove", this._onMouseMove);
    };

    _onLeftDragOver = (e) => {
        e.preventDefault();
        if (this._leftEl) this._leftEl.style.transform = "scale(1.5)";
    };

    _onLeftDragLeave = (e) => {
        e.preventDefault();
        if (this._leftEl) this._leftEl.style.transform = "none";
    };

    _onRightDragOver = (e) => {
        e.preventDefault();
        if (this._rightEl) this._rightEl.style.transform = "scale(1.5)";
    };

    _onRightDragLeave = (e) => {
        e.preventDefault();
        if (this._rightEl) this._rightEl.style.transform = "none";
    };

    _onDrop = (place, e) => {
        let data = JSON.parse(e.dataTransfer.getData("e"));

        ewars.emit("HIDE_DROPS");

        if (place == "AFTER") {
            ewars.z.dispatch("E", "APPEND_NODE", [
                this.props.data._,
                data
            ])
        }
    };

    render() {

        return (
            <div
                ref={(el) => {
                    this._el = el;
                }}
                className="ide-col h-box-item"
                style={{
                    maxWidth: this.props.data.w + "%",
                    width: this.props.data.w + "%",
                    height: "100%",
                    maxHeight: "100%"
                }}>
                {this.props.index == 0 ?
                    <div
                        ref={(el) => {
                            this._leftEl = el;
                        }}
                        onDrop={(e) => {
                            this._onDrop("BEFORE", e);
                        }}
                        onDragOver={this._onLeftDragOver}
                        onDragLeave={this._onLeftDragLeave}
                        style={STYLES.DROPPER_L}>
                        <i className="fal fa-hand-point-left"></i>
                    </div>
                    : null}

                {this.props.children}
                {!this.props.root && !this.props.isLast ?
                    <div style={STYLES.rightControl} onMouseDown={this._onMouseDown} onMouseUp={this._onMouseOut}>
                    </div>
                    : null}
                <div
                    ref={(el) => {
                        this._rightEl = el;
                    }}
                    onDrop={(e) => {
                        this._onDrop("AFTER", e);
                    }}
                    onDragOver={this._onRightDragOver}
                    onDragLeave={this._onRightDragLeave}
                    style={STYLES.DROPPER_R}>
                    <i className="fal fa-hand-point-right"></i>
                </div>
            </div>
        )
    }
}

class HBox extends React.Component {
    _isResizing = false;

    static defaultProps = {
        height: "100px",
        width: "100%",
        resizable: true,
        pid: null,
        mode: "DEFAULT"
    };

    constructor(props) {
        super(props);

        this.state = {
            showContext: false
        }

        ewars.subscribe("HIDE_CONTROLS", this._hideControls);
    }

    componentWillUnmount() {
        ewars.unsubscribe("HIDE_CONTROLS", this._hideControls);
    }

    _hideControls = () => {
        if (this._contextEl) this._contextEl.style.display = "none";
    };

    _showContext = () => {
        let children = this.props.definition.filter((item) => {
            return item.p == this.props.data._;
        })
        if (children.length > 0) return;
        ewars.emit("HIDE_CONTROLS");
        if (this._contextEl) this._contextEl.style.display = "block";
    };

    _onAction = (action) => {
        if (action == "DELETE") {
            ewars.emit("HIDE_CONTROLS");
            ewars.z.dispatch("E", "REMOVE_NODE", this.props.data._);
        }

        if (action == "CLOSE") {
            if (this._contextEl) this._contextEl.style.display = "none";
        }
    };

    render() {
        // given the width passed in, divvy up
        let cap = this.props.width / 100;

        let showHandle = true;
        let height;
        if (this.props.root) {
            if (this.props.mode == "HUD") {
                height = "100%";
                showHandle = false;
            } else {
                height = this.props.data.h + "px";
            }
        } else {
            // calculate height
            height = this.props.data.h + "%";
        }

        let children = this.props.definition.filter((item) => {
            return item.p == this.props.data._;
        })
        children = children.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        })

        let items = children.map((item, index) => {
            let isLast = index == children.length - 1;
            if (item.t == "V") {
                return (
                    <HBoxItem
                        isLast={isLast}
                        data={item}
                        index={index}
                        key={item._ + "WRAPPER"}>
                        <VBox
                            width={cap * item.w}
                            height={height}
                            data={item}
                            definition={this.props.definition}
                            mode={this.props.mode}
                            key={item._ + "_" + index}
                            rowId={this.props.data._}
                            rowWidth={this.props.width}
                            row={this.props.data.i}/>
                    </HBoxItem>
                );
            }

            let hUnits = this.props.height / 100;
            if (item.t == "W") {
                return (
                    <HBoxItem
                        isLast={isLast}
                        data={item}
                        index={index}
                        key={item._ + "WRAPPER"}>
                        <Widget
                            width={cap * item.w}
                            height={hUnits * item.h}
                            definition={this.props.definition}
                            data={item}
                            mode={this.props.mode}
                            key={item._ + "_" + index}
                            rowId={this.props.data._}
                            rowWidth={this.props.width}
                            row={this.props.data.i}/>
                    </HBoxItem>
                )
            }
        })


        return (
            <div
                className="ide-row layout-row h-box"
                onClick={this._showContext}
                style={{
                    position: "relative",
                    width: "100%",
                    maxWidth: "100%",
                    resize: "vertical",
                    height: "100%",
                    maxHeight: "100%"
                }}>
                <div
                    style={{...STYLES.CONTEXT_MENU, width: "52px", marginLeft: "-26px"}}
                    ref={(el) => {
                        this._contextEl = el
                    }}>
                    <ewars.d.ActionGroup
                        actions={ACTIONS}
                        onAction={this._onAction}/>
                </div>
                {items}
                {children.length <= 0 ?
                    <CenterDrop allowedTypes={["V", "W"]} data={this.props.data}/>
                    : null}

            </div>
        )
    }
}

export default HBox;
