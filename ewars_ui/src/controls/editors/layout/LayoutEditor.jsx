import React from "react";

import utils from "../../../utils/utils";
import SystemForm from "../../forms/form.system.jsx";

import Droppables from "./Droppables.jsx";
import ViewVariables from "./views/view.variables.jsx";
import {PLOT_TYPES} from '../../../constants/const.types.jsx';


import {Responsive, WidthProvider} from 'react-grid-layout';

const ResponsiveGridLayout = WidthProvider(Responsive);


import Preview from "./Preview.jsx";
import StyleEditor from "./StyleEditor.jsx";
import Widget from './Widget.jsx';

import PlotEditor from "../_plot/plot_screen.jsx";

class SettingsPage extends React.Component {
    _onChange = (prop, value) => {
        ewars.z.dispatch("E", "SET_LAYOUT_PROP", [prop, value]);
    };

    render() {
        return (
            <div style={{padding: "8px"}}>
                <SystemForm
                    vertical={true}
                    definition={this.props.settings}
                    data={this.props.data}
                    onChange={this._onChange}
                    enabled={true}/>
            </div>
        )
    }
}

// TODO: Move defaults out ofo here
const DEFAULTS = {
    name: "New HUD",
    status: "DRAFT",
    widgets: {},
    definition: [],
    settings: {},
    permissions: {}
};

class LayoutEditor extends React.Component {
    static defaultProps = {
        mode: "DASHBOARD",
        aspect: null,
        onSave: null,
        onClose: null,
        permissions: false,
        settings: [],
        data: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            data: utils.copy(this.props.data || DEFAULTS),
            view: "DEFAULT",
            widget: null,
            mainView: "DEFAULT"
        }
    }

    _save = () => {
        if (this.props.onSave) {
            this.props.onSave(this.state.data)
                .then(res => {
                    console.log(res);
                    this.setState({
                        ...this.state,
                        data: res
                    });
                })
                .catch(err => {
                    utils.error(err);
                });
        }
    };

    _closeEdit = () => {
        this.setState({
            widget: null
        })
    };

    _onRootDrop = (e) => {
        let data = JSON.parse(e.dataTransfer.getData("e", '{}'));
        let layout = this.state.data.definition || [];
        let uuid = utils.uuid();
        layout.push({
            i: uuid,
            x: 0,
            y: 0,
            h: 5,
            w: 5,
            minW: 2,
            minH: 2
        });

        let widgets = this.state.data.widgets || {};
        widgets[uuid] = {
            _: uuid,
            type: data.t
        };
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                definition: layout,
                widgets: widgets
            }
        })
    };

    _addPage = () => {
    };

    _onChangeSetting = (prop, value) => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                settings: {
                    ...this.state.settings,
                    [prop]: value
                }
            }
        });
    };

    _onStyleChange = (prop, value) => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                style: {
                    ...this.state.style,
                    [prop]: value
                }
            }
        })
    };

    _onClosePlot = () => {
        this.setState({
            widget: null
        })
    };

    _onPlotSave = (data) => {
        console.log(data);
        let widgets = this.state.data.widgets;
        widgets[this.state.widget._] = data;
        this.setState({
            ...this.state,
            widget: null,
            data: {
                ...this.state.data,
                widgets: widgets
            }
        })
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    _onLayoutChange = (layout) => {
        // this.props.onLayoutChange(layout);
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                definition: layout
            }
        })
    };

    onBreakpointChange = (breakpoint, cols) => {
    };

    _onEdit = (data) => {
        console.log(data);
        this.setState({
            widget: utils.copy(data)
        })
    };

    _onRemoveItem = (data) => {
        let rIndex;
        let layout = this.state.data.definition;
        let widgets = this.state.data.widgets;
        delete widgets[data._];
        layout.forEach((item, index) => {
            if (item.i == data._) {
                rIndex = index;
            }
        })
        layout.splice(rIndex, 1);
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                definition: layout,
                widgets: widgets
            }
        })
    };

    _view = (view) => {
        this.setState({view: view})
    };

    _mainView = (view) => {
        this.setState({
            mainView: view
        })
    };

    render() {

        let view;


        if (this.state.view == "DEFAULT") view = <Droppables/>;
        if (this.state.mainView == "VARIABLES") mainView = <ViewVariables data={this.state.data.variables || []} onChange={this._onVarsChange}/>;
        if (this.state.view == "THEME") {
            view = <StyleEditor
                mode="main"
                data={this.state.data.settings || {style: {}}}
                onChange={this._onStyleChange}/>;
        }

        let curView;
        if (this.state.widget) {
            if (PLOT_TYPES.indexOf(this.state.widget.type) >= 0) {
                view = <PlotEditor
                    onSave={this._onPlotSave}
                    onClose={this._onClosePlot}
                    data={this.state.widget}/>

            } else {
                view = (
                    <WidgetEditor
                        onSave={this._onPlotSave}
                        onClose={this._onClosePlot}
                        data={this.state.widget}/>
                )
            }
        }

        let mainView,
            wrapperStyle = {};

        if (this.state.mainView == "DEFAULT") {
            wrapperStyle.background = "transparent";
            mainView = (
                <div
                    ref={(el) => {
                        this._el = el;
                    }}
                    onDrop={this._onRootDrop}
                    onDragOver={this._onDragOver}
                    style={{
                        height: "100%",
                        overflowY: "auto",
                        border: "1px solid rgba(255,255,255,0.2)"
                    }}>
                    <ResponsiveGridLayout
                        className="layout"
                        layouts={{
                            lg: this.state.data.definition || [],
                            md: this.state.data.definition || [],
                            sm: this.state.data.definition || []
                        }}
                        rowHeight={30}
                        autoSize={false}
                        verticalCompact={true}
                        onBreakPointChange={this.onBreakpointChange}
                        onLayoutChange={this._onLayoutChange}
                        breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
                        cols={{lg: 36, md: 36, sm: 36, xs: 4, xxs: 2}}>
                        {(this.state.data.definition || []).map(item => {
                            return (
                                <div key={item.i}>
                                    <Widget
                                        key={"WIDG_" + item.i}
                                        onEdit={this._onEdit}
                                        onDelete={this._onRemoveItem}
                                        data={this.state.data.widgets[item.i]}/>
                                </div>
                            )
                        })}
                    </ResponsiveGridLayout>
                </div>
            )
        }


        wrapperStyle.background = "#F2F2F2";

        return (
            <div className="row">
                <div className="column br block" style={{overflow: "hidden", position: "relative", maxWidth: "35px"}}>
                    <div className="ide-tabs">
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                            <i className="fal fa-cog"></i>&nbsp;Widgets
                        </div>
                        <div
                            onClick={() => this._view("SETTINGS")}
                            className={"ide-tab" + (this.state.view == "SETTINGS" ? " ide-tab-down" : "")}>
                            <i className="fal fa-file"></i>&nbsp;Settings
                        </div>
                        <div
                            onClick={() => this._view("PAGES")}
                            className={"ide-tab" + (this.state.view == "PAGES" ? " ide-tab-down" : "")}>
                            <i className="fal fa-file"></i>&nbsp;Pages
                        </div>
                        <div
                            onClick={() => this._mainView("VARIABLES")}
                            className={"ide-tab" + (this.state.mainView == "VARIABLES" ? " ide-tab-down" : "")}>
                            <i className="fal fa-code"></i>&nbsp;Variables
                        </div>
                        <div
                            onClick={() => this._view("THEME")}
                            className={"ide-tab" + (this.state.view == "THEME" ? " ide-tab-down" : "")}>
                            <i className="fal fa-paint-brush"></i>&nbsp;Theme
                        </div>
                    </div>
                </div>
                <div className="column panel br">
                    {view}
                </div>
                <div className="column block" style={{position: "relative", padding: "8px"}}>
                    {mainView}
                </div>
            </div>
        )
    }
}

export default LayoutEditor;
