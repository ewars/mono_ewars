import React from "react";

import CodeMirror from "react-codemirror";
require("codemirror/mode/python/python");


export default class Custom extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <CodeMirror
                options={{
                    mode: "python",
                    lineNumbers: true
                }}/>

        )
    }
}
