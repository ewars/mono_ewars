import React from "react";


import ActionGroup from "../../../c.actiongroup.jsx";
import MenuDroppables from '../menus/menu.droppables.jsx';


class VariablesView extends React.Component {
    static defaultProps = {
        data: []
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="column panel br">
                    <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>
                        <div className="column grid-title"></div>
                        <ActionGroup
                            onAction={this._action}
                            actions={[['fa-plus', 'ADD', 'Add variable']]}/>
                    </div>
                    <div className="row block block-overflow">

                    </div>
                </div>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "35px"}}>

                    </div>
                    <div className="row block block-overflow">

                    </div>
                </div>
            </div>
        )
    }
}

export default VariablesView;