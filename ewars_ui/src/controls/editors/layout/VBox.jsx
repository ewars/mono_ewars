import React from "react";

import DropArea from "./DropArea.jsx";
import Widget from "./Widget.jsx";
import HBox from "./HBox.jsx";
import CenterDrop from "./CenterDrop.jsx";
import ContextMenu from "./ContextMenu.jsx";

import STYLES from "./styles";

const VBOX_ACTIONS = [
    ["fa-trash", "DELETE"]
]

const ACTIONS = [
    ["fa-trash", "DELETE"],
    ["fa-times", "CLOSE"]
]

class VBoxItem extends React.Component {
    _isResizing = false;

    static defaultProps = {
        data: {},
        isLast: false
    };

    constructor(props) {
        super(props);

        ewars.subscribe("SHOW_DROPS", this._showDrops);
        ewars.subscribe("HIDE_DROPS", this._hideDrops);
    }

    _showDrops = (type) => {
        if (["H", "W"].indexOf(type) >= 0) {
            if (this._topEl) this._topEl.style.display = "block";
            if (this._bottomEl) this._bottomEl.style.display = "block";
        }
    };

    _hideDrops = () => {
        if (this._topEl) this._topEl.style.display = "none";
        if (this._bottomEl) this._bottomEl.style.display = "none";
    };

    _onMouseMove = (e) => {
        if (e.buttons > 0) {
            if (this._isResizing) {
                let offsetY = this._el.getBoundingClientRect().top;
                let parentHeight = this._el.parentNode.getBoundingClientRect().height;

                let newHeight = e.clientY - offsetY;
                let newPerc = (newHeight / parentHeight) * 100;

                ewars.z.dispatch("E", "RESIZER_V", [this.props.data._, newPerc]);
            }
        } else {
            this._isResizing = false;
            window.removeEventListener("mousemove", this._onMouseMove);
        }
    };

    _onMouseDown = (e) => {
        e.preventDefault();
        this._isResizing = true;
        window.addEventListener("mousemove", this._onMouseMove);
    };

    _onBottomDragOver = (e) => {
        e.preventDefault();
        if (this._bottomEl) this._bottomEl.style.transform = "scale(1.5)";
    };

    _onBottomDragLeave = (e) => {
        e.preventDefault();
        if (this._bottomEl) this._bottomEl.style.transform = "none";
    };

    _onTopDragOver = (e) => {
        e.preventDefault();
        if (this._topEl) this._topEl.style.transform = "scale(1.5)";
    };

    _onTopDragLeave = (e) => {
        e.preventDefault();
        if (this._topEl) this._topEl.style.transform = "scale(1)";
    };

    _onDrop = (place, e) => {
        let data = JSON.parse(e.dataTransfer.getData("e"));

        ewars.emit("HIDE_DROPS");

        if (place == "AFTER") {
            ewars.z.dispatch("E", "APPEND_NODE", [
                this.props.data._,
                data
            ])
        } else {
            ewars.z.dispatch("E", "PREPEND_NODE", [
                this.props.data._,
                data
            ])
        }
    };

    render() {

        let icon = "fal fa-hand-point-down";

        return (
            <div
                ref={(el) => {
                    this._el = el;
                }}
                className="ide-col v-box-item"
                style={{
                    maxHeight: this.props.data.h + "%",
                    height: this.props.data.h + "%",
                    width: "100%",
                    maxWidth: "100%",
                    position: "relative"
                }}>
                {this.props.index == 0 ?
                    <div
                        ref={(el) => {
                            this._topEl = el
                        }}
                        onDrop={(e) => {
                            this._onDrop("BEFORE", e);
                        }}
                        onDragOver={this._onTopDragOver}
                        onDragLeave={this._onTopDragLeave}
                        style={STYLES.DROPPER_T}>
                        <i className="fal fa-hand-point-up"></i>
                    </div>
                    : null}
                {this.props.children}
                {!this.props.root && !this.props.isLast ?
                    <div style={STYLES.bottomControl} onMouseDown={this._onMouseDown} onMouseUp={this._onMouseOut}>
                    </div>
                    : null}
                <div
                    ref={(el) => {
                        this._bottomEl = el
                    }}
                    onDrop={(e) => {
                        this._onDrop("AFTER", e);
                    }}
                    onDragOver={this._onBottomDragOver}
                    onDragLeave={this._onBottomDragLeave}
                    style={STYLES.DROPPER}>
                    <i className={icon}></i>
                </div>
            </div>
        )
    }
}

class VBox extends React.Component {
    static defaultProps = {
        height: "100%",
        width: "100%",
        resizable: true,
        mode: "DEFAULT",
        root: false
    };

    constructor(props) {
        super(props);

        this.state = {
            showContext: false
        };

        ewars.subscribe("HIDE_CONTROLS", this._hideControls);
    }

    componentWillUnmount() {
        ewars.unsubscribe("HIDE_CONTROLS", this._hideControls);
    }

    _hideControls = () => {
        if (this._contextEl) this._contextEl.style.display = "none";
    };

    _onDropped = (data) => {
        if (data.t != "V") {
            // we're adding a new vertical column
            let items = ewars.copy(this.props.data.i || []);
            items.sort((a, b) => {
                if (a._ > b._) return 1;
                if (a._ < b._) return -1;
                return 0;
            })
            let lastId,
                newHeight = 100,
                subtract = 0;
            if (items.length > 0) {
                lastId = items[items.length - 1]._;
                lastId = lastId.split(".");
                lastId[lastId.length - 1] = parseInt(lastId[lastId.length - 1]) + 1;
                lastId = lastId.join(".");
            } else {
                lastId = `${this.props.data._}.1`;
            }

            if (items.length > 0) {
                if (items.length === 1) subtract = 10;
                if (items.length > 1) subtract = 10 / items.length;
                newHeight = 10;

                items.forEach((item) => {
                    item.h = item.h - subtract;
                })
            }

            let newItem = {
                _: lastId,
                w: 100,
                h: newHeight
            }
            if (data.t == "H") {
                newItem.t = "H";
                newItem.i = [];
            }
            if (data.t != "H") {
                newItem.t = "W";
                newItem.c = {type: data.c.type};
            }

            items.push(newItem);

            ewars.z.dispatch("E", "RESIZE_CELLS", [this.props.rowId, items]);
        }


    };

    _showContext = () => {
        let children = this.props.definition.filter((item) => {
            return item.p == this.props.data._;
        })
        if (children.length > 0) return;

        ewars.emit("HIDE_CONTROLS");
        if (this._contextEl) this._contextEl.style.display = "block";
    };

    _onAction = (action) => {
        if (action == "DELETE") {
            ewars.emit("HIDE_CONTROLS");
            ewars.z.dispatch("E", "REMOVE_NODE", this.props.data._);
        }

        if (action == "CLOSE") {
            ewars.emit("HIDE_CONTROLS");
        }
    };

    render() {
        let children = this.props.definition.filter((item) => {
            return item.p == this.props.data._;
        })
        children = children.sort((a, b) => {
            if (a._ > b._) return 1;
            if (a._ < b._) return -1;
            return 0;
        })

        let items = [];
        items = children.map((item, index) => {
            let isLast = index == children.length - 1;
            if (item.t == "H") {
                return (
                    <VBoxItem
                        key={"WRAPPER" + item._}
                        isLast={isLast}
                        index={index}
                        data={item}>
                        <HBox
                            data={item}
                            definition={this.props.definition}
                            mode={this.props.mode}
                            cellId={this.props.data._}
                            cell={this.props.data.i}
                            width={this.props.width}
                            height={this.props.height}
                            key={item._}/>
                    </VBoxItem>
                )
            }

            if (item.t == "W") {
                return <VBoxItem
                    isLast={isLast}
                    index={index}
                    key={"WRAPPER" + item._}
                    data={item}>
                    <Widget
                        data={item}
                        definition={this.props.definition}
                        vertical={true}
                        mode={this.props.mode}
                        cellId={this.props.data._}
                        cell={this.props.data.i}
                        width={this.props.width}
                        height={this.props.height}
                        key={item._}/>
                </VBoxItem>
            }
        })


        return (
            <div className="ide-col v-box"
                 onDrop={this._onDrop}
                 onDragOver={this._onDragOver}
                 onClick={this._showContext}
                 style={{
                     resize: "horizontal",
                     position: "relative",
                     width: "100%",
                     maxWidth: "100%",
                     height: "100%"
                 }}>
                {items}
                {children.length <= 0 ?
                    <CenterDrop allowedTypes={["H", "W"]} data={this.props.data}/>
                    : null}
                <div
                    style={{...STYLES.CONTEXT_MENU, width: "52px", marginLeft: "-26px"}}
                    className="context-menu"
                    ref={(el) => {
                        this._contextEl = el
                    }}>
                    <ewars.d.ActionGroup
                        actions={ACTIONS}
                        onAction={this._onAction}/>
                </div>
            </div>
        )
    }

}

export default VBox;
