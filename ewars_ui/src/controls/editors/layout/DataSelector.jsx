import React from "react";

import Form from "./form.DataSelector.js";
import Custom from "./Custom.jsx";
import StyleEditor from "./StyleEditor.jsx";
import DataSourceTree from "../../c.tree.datasource.jsx";

/**
 * Modes:
 *  DEFAULT - Used form most data sources
 *  ROOT - Used for root-level code values
 *  RAW - Used for raws
 */

export default class DataSelector extends React.Component {
    static defaultProps = {
        mode: "DEFAULT",
        dimensions: false,
        hideSources: false,
        data: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            data: ewars.copy(this.props.data),
            view: "DEFAULT"
        }
    }

    _onSelect = (source) => {

    };

    _save = () => {
        this.props.onSave(this.state.data);

    };

    _close = () => {
        this.props.onClose();
    };

    _onChange = (prop, value) => {
        this.setState({
            data: {
                ...this.state.data,
                [prop]: value
            }
        })
    };

    render() {
        let view;

        let isComplex = false;
        if (this.state.data.sources) {
            if (this.state.data.sources.length > 1) isComplex = true;
        }

        let form_def = ewars.copy(Form);
        if (isComplex) {
            form_def.push({
                _o: 0.03,
                n: "formula",
                t: "TEXT",
                l: "Formula"
            })
        }

        view = (
            <ewars.d.SystemForm
                enabled={true}
                definition={form_def}
                vertical={true}
                onChange={this._onChange}
                data={this.state.data}/>
        );

        let sideView;
        if (this.state.view == "DEFAULT") {
            sideView = (
                <DataSourceTree
                    onSelect={this._onSelect}/>
            )
        }

        if (this.state.view == "STYLE") {
            sideView = (
                <StyleEditor
                    defaults={false}/>
            )

        }

        if (this.state.selectedType == "CUSTOM") {
            view = <Custom/>
        }

        return (
            <ewars.d.Layout>
                {!this.props.hideSources ?
                    <ewars.d.Toolbar label="Data Sources">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-save"
                                onClick={this._save}/>
                            <ewars.d.Button
                                icon="fa-close"
                                onClick={this._close}/>
                        </div>
                    </ewars.d.Toolbar>
                    : null}
                <ewars.d.Row>
                    {!this.props.hideSources ?
                        <ewars.d.Cell borderRight={true} width="350px">
                            <ewars.d.Layout>
                                <ewars.d.Toolbar>
                                    <div className="btn-group btn-tabs">
                                        <ewars.d.Button
                                            highlight={this.state.view == "DEFAULT"}
                                            onClick={() => {
                                                this.setState({
                                                    view: "DEFAULT"
                                                })
                                            }}
                                            icon="fa-cog"/>
                                        <ewars.d.Button
                                            highlight={this.state.view == "STYLE"}
                                            onClick={() => {
                                                this.setState({
                                                    view: "STYLE"
                                                })
                                            }}
                                            icon="fa-paint-brush"/>
                                    </div>

                                </ewars.d.Toolbar>
                                <ewars.d.Row>
                                    <ewars.d.Cell>
                                        {sideView}
                                    </ewars.d.Cell>
                                </ewars.d.Row>
                            </ewars.d.Layout>
                        </ewars.d.Cell>
                        : null}
                    <ewars.d.Cell>
                        <ewars.d.Panel>
                            {view}
                        </ewars.d.Panel>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
}
