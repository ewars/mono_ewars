import React from "react";

import ActionGroup from '../../controls/c.actiongroup.jsx';

import SelectField from "../../controls/fields/f.select.jsx";
import TextField from "../../controls/fields/f.text.jsx";
import NumericField from "../../controls/fields/f.numeric.jsx";
import LocationField from "../../controls/fields/f.location.jsx";
import LocationGroupField from "../../controls/fields/f.location.group.jsx";
import TextAreaField from "../../controls/fields/f.textarea.jsx";


const TYPES = {
    SELECT: SelectField,
    TEXT: TextField,
    TEXTAREA: TextAreaField,
    LOCATION: LocationField,
    NUMBER: NumericField,
    NUMERIC: NumericField,
    LOCATION_GROUP: LocationGroupField
}

const RULE_ACTIONS = [
    ['fa-copy', 'DUPLICATE', 'Copy rule'],
    ['fa-trash', 'DELETE', 'Delete rule']
];

const GROUP_ACTIONS = [
    ['fa-plus', 'ADD', 'Add sub-rule'],
    ['fa-copy', 'DUPLICATE', 'Duplicate group'],
    ['fa-trash', 'DELETE', 'Delete group']
];

const EDITOR_ACTIONS = [
    ['fa-plus', 'ADD', 'Add rule']
];

const CMP_STYLE = {
    position: "relative"
};

const CMP_HANDLE = {
    padding: "3px",
    background: "rgba(0,0,0,0.3)",
    width: "35px"
};


class RuleGroup extends React.Component {
    constructor(props) {
        super(props);
    }

    _onTypeChange = (e) => {
        let rules = this.props.value;

        rules[0] = e.target.value;

        this.props.onChange(this.props.index, rules);
    };

    _action = (action) => {
        switch(action) {
            case "ADD":
                let data = this.props.value;
                data[1].push(['', 'eq', '']);
                this.props.onChange(this.props.index, data);
                break;
            case "DELETE":
                this.props.onDelete(this.props.index);
                break;
            default:
                break;
        }
    };

    _onRuleChange = (index, rule) => {
        let data = this.props.value;
        data[1][index] = rule;

        this.props.onChange(this.props.index, data);
    };

    _onDeleteRule = (index) => {
        let data = this.props.value;
        data[1].splice(index, 1);

        this.props.onChange(this.props.index, data);
    };

    _onDupeRule = (index) => {
        let data = this.props.value;
        let tmp = JSON.parse(JSON.stringify(data[1][index]));

        data[1].push(tmp);

        this.props.onChange(this.props.index, data);
    };

    render() {
        return (
            <div className="rule-group" style={{background: "rgba(0,0,0,0.5)", margin: "4px 8px"}}>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column" style={{padding: "4px"}}>
                            <div className="select-wrapper">
                                <select
                                    onChange={this._onTypeChange}
                                    value={(this.props.value[0] || "").split(":")[0]}
                                    name="" id="">
                                    <option value=""></option>
                                    <option value="fx:">fx</option>
                                    <option value="AND">and group</option>
                                    <option value="OR">or group</option>
                                    <option value="--">---</option>
                                    {this.props.options.map(item => {
                                        return (
                                            <option value={item[0]}>{item[1]}</option>
                                        )
                                    })}
                                </select>
                                <i className="fal fa-caret-down"></i>
                            </div>
                        </div>
                        <ActionGroup
                            actions={GROUP_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row block block-overflow">
                        {(this.props.value[1] || []).map((item, index) => {
                            console.log(item);
                            return (
                                <Rule
                                    onDelete={this._onDeleteRule}
                                    onDuplicate={this._onDupeRule}
                                    index={index}
                                    options={this.props.options}
                                    value={item}
                                    onChange={this._onRuleChange}/>
                            )
                        })}

                    </div>
                </div>
            </div>
        )
    }
}

class NullCmp extends React.Component {
    render() {
        return (
            <div></div>
        )
    }
}

class FormulaInput extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="column" style={{maxWidth: "35px", background: "black", color: "#DADADA"}}>fx</div>
                <div className="column">
                    <input
                        value={this.props.value.split(":")[1] || ""}
                        onChange={(e) => this.props.onChange(e.target.value)}
                        type="text"/>
                </div>
            </div>
        )
    }
}

class Rule extends React.Component {
    constructor(props) {
        super(props);
    }

    _onItemChange = (e) => {

    };

    _onChange = (prop, value) => {
        let result = this.props.value;

        result[2] = value;

        this.props.onChange(this.props.index, result);
    };

    _action = (action) => {
        switch (action) {
            case "DELETE":
                this.props.onDelete(this.props.index);
                break;
            case "DUPLICATE":
                this.props.onDuplicate(this.props.index);
                break;
            default:
                break;
        }
    };

    _onFormulaChange = (val) => {
        let result = this.props.value;
        result[0] = `fx:${val}`;

        this.props.onChange(this.props.index, val);
    };

    onFieldChange = (e) => {
        let result = this.props.value;

        let cur_type = this.props.value[0] || "";
        let new_type = e.target.value;

        if (cur_type.indexOf("fx:") == 0) {
            // This rule is currently an fx

            if (new_type == "AND") {
                result = ["AND", [["", "eq", ""]]];
            } else if (new_type == "OR") {
                result = ['OR', [["", 'eq', '']]];
            }
        } else if (cur_type == "AND") {
            if (new_type == "OR") {
                result[0] = "OR";
            } else if (new_type.indexOf("fx:") == 0) {
                result = ["fx:", "eq", ""];
            } else {
                result[0] = new_type;
            }
            result[0] = e.target.value;
        } else if (cur_type == "OR") {
            if (new_type == "AND") {
                result[0] = "AND";
            } else if(new_type.indexOf("fx:") == 0) {
                result = ['fx:', 'eq', ''];
            } else {
                result = [new_type, 'eq', ''];
            }
        } else {
            if (["AND", "OR"].indexOf(new_type) >= 0) {
                result = [new_type, [["", 'eq', '']]];
            } else {
                result[0] = new_type;
            }
        }

        this.props.onChange(this.props.index, result);
    };

    _onCmpChange = (e) => {
        let result = this.props.value || [];
        result[1] = e.target.value;

        this.props.onChange(this.props.index, result);
    };

    render() {
        if (["AND", "OR"].indexOf(this.props.value[0]) >= 0) {
            return (
                <RuleGroup
                    onDelete={this.props.onDelete}
                    index={this.props.index}
                    onChange={this.props.onChange}
                    value={this.props.value}
                    options={this.props.options}
                    data={this.props.value}/>
            )
        }

        let InputCmp = NullCmp;

        let fxInput;
        if ((this.props.value[0] || "").indexOf("fx:") == 0) {
            // This is a formula calc from fields in the form
            fxInput = (
                <FormulaInput
                    value={this.props.value[0]}
                    onChange={this._onFormulaChange}/>
            )
        }


        let field = this.props.options.filter(item => {
            return item[0] == (this.props.value[0] || "")
        })[0] || null;

        if (field) {
            InputCmp = TYPES[field[2].type.toUpperCase()] || null;
            if (!InputCmp) {
                InputCmp = NullCmp;
            }
            field = field[2];
        }
        console.log(field);

        return (
            <div className="rule" style={{margin: "4px 8px", background: "rgba(0,0,0,0.4)", padding: "4px"}}>
                <div className="row">
                    <div className="column br" style={{padding: "3px"}}>
                        <div className="select-wrapper">
                            <select
                                onChange={this.onFieldChange}
                                value={(this.props.value[0] || "").split(":")[0]}
                                name="" id="">
                                <option value=""></option>
                                <option value="fx:">fx</option>
                                <option value="AND">and group</option>
                                <option value="OR">or group</option>
                                <option value="--">---</option>
                                {this.props.options.map(item => {
                                    return (
                                        <option value={item[0]}>{item[1]}</option>
                                    )
                                })}
                            </select>
                            <i className="fal fa-caret-down"></i>
                        </div>
                    </div>
                    <div className="column br" style={{padding: "3px", maxWidth: "35px"}}>
                        <div className="select-wrapper">
                            <select
                                onChange={this._onCmpChange}
                                value={this.props.value[1]}
                                name="" id="">
                                <option value="eq">eq</option>
                                <option value="neq">neq</option>
                                <option value="gt">gt</option>
                                <option value="gte">gte</option>
                                <option value="lt">lt</option>
                                <option value="lte">lte</option>
                                <option value="null">null</option>
                                <option value="nnull">not null</option>
                                <option value="contains">contains</option>
                                <option value="ncontains">ncontains</option>
                            </select>
                            <i className="fal fa-caret-down"></i>
                        </div>
                    </div>
                    <div className="column br" style={{padding: "3px"}}>
                        <InputCmp
                            options={(field || {}).options || []}
                            value={this.props.value[2] || ""}
                            onChange={this._onChange}/>
                    </div>
                    <ActionGroup
                        actions={RULE_ACTIONS}
                        onAction={this._action}/>
                </div>
                {fxInput}
            </div>
        )
    }
}


class RulesEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    onChange = () => {
        this.props.onChange()
    };

    _action = (action) => {
        switch (action) {
            case "ADD":
                let rules = this.props.data || [];
                rules.push([
                    null,
                    'eq',
                    null
                ]);
                this.props.onChange(rules);
                break;
            default:
                break;
        }
    };

    _deleteRule = (index) => {
        let rules = (this.props.data || []);
        rules.splice(index, 1);

        this.props.onChange(rules);
    };

    _duplicate = (index) => {
        let rules = (this.props.data || []);
        let item = JSON.parse(JSON.stringify(rules[index]));

        rules.push(item);

        this.props.onChange(rules);
    };

    _onChange = (index, data) => {
        let rules = (this.props.data || []);
        rules[index] = data;

        this.props.onChange(rules);
    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column"></div>
                    <ActionGroup
                        actions={EDITOR_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">
                    {(this.props.data || []).map((item, index) => {
                        return (
                            <Rule
                                onDelete={this._deleteRule}
                                onDuplicate={this._duplicate}
                                index={index}
                                options={this.props.options || []}
                                value={item}
                                onChange={this._onChange}/>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default RulesEditor;