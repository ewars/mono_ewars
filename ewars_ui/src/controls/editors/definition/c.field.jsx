import React from "react";

import FIELD_DEFINITION from "./const.field_definition.js";
import ActionGroup from "../../../controls/c.actiongroup.jsx";
import FormUtils from "../../../utils/form_utils";
import utils from "../../../utils/utils";
import ReportingForm from '../../../controls/forms/form.reporting.jsx';
import ICONS from "./const.icons";
import RulesEditor from "../../../controls/editors/editor.rules.jsx";

const ACTIONS = [
    ['fa-angle-up', 'UP', "Move up"],
    ['fa-angle-down', 'DOWN', 'Move down'],
    ['fa-copy', 'DUPE', 'Duplicate'],
    ['fa-trash', 'DELETE', 'Delete field']
];

const _formatLabel = (label) => {
    if (!label) return "Untitled field";
    if (label >= 70) {
        return label.substring(0, 70) + "...";
    }
    return label;
};

class FieldDefinition extends React.Component {
    static defaultProps = {
        isRoot: false
    };

    state = {
        showDetails: false,
        view: "DEFAULT"
    };

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        window.addEventListener("showdrops", this._showDrops);
        window.addEventListener("hidedrops", this._hideDrops);
    }

    componentWillUnmount() {
        window.removeEventListener("showdrops", this._showDrops);
        window.removeEventListener("hidedrops", this._hideDrops);
    }

    _view = (view) => {
        this.setState({view: view});
    };

    _showDrops = (data) => {
        if (data.detail != this.props.data.uuid)
            if (this._dropEl) this._dropEl.style.display = "block";
    };

    _hideDrops = () => {
        if (this._dropEl) this._dropEl.style.display = "none";
    };

    _action = (action, data) => {
        switch (action) {
            case "UP":
                this.props.onMoveFieldUp(this.props.data.uuid, -1);
                break;
            case "DOWN":
                this.props.onMoveFieldDown(this.props.data.uuid, 1);
                break;
            case "DUPE":
                this.props.onDuplicateField(this.props.data.uuid);
                break;
            case "TOGGLE":
                this.setState({
                    showDetails: !this.state.showDetails
                });
                break;
            case "DELETE":
                this.props.onDeleteField(this.props.data.uuid);
                break;
            case "ADD":
                this.addChild();
                break;
            default:
                break;
        }
    };

    onPropChange = (name, value) => {
        this.props.onChange(this.props.data.uuid, name, value);
    };

    _drop = (e) => {
        e.preventDefault();

        let data = e.dataTransfer.getData('e');
        let _data = JSON.parse(data);
        if (_data.new == true) {
            let newField = {
                type: _data.type,
                label: "New Field",
                required: false,
                uuid: utils.uuid()
            };
            this.props.onDropNew(newField, this.props.data.uuid);
        } else {
            this.props.onMoveField(data, this.props.data.uuid);
        }
    };

    addChild = () => {
        let childs = this.props.data.fields || {};

        let uuid = utils.uuid();

        let newFieldName = "new_" + uuid.split("-")[0];

        let arrChilds = FormUtils.getFieldsAsArrayShallow(childs);
        arrChilds.push({
            type: "text",
            uuid: uuid,
            name: newFieldName,
            order: 999999,
            label: {en: "Untitled"}
        });

        arrChilds = arrChilds.sort(FormUtils.ORDER_SORT);

        let objChilds = FormUtils.getFieldsAsObject(arrChilds);

        this.props.onChange(this.props.data.uuid, "fields", objChilds);
    };

    _dragEnd = () => {
        // TODO: Emit hide drops
        window.dispatchEvent(new CustomEvent("hidedrops", {}));
    };

    _dragStart = (e) => {
        e.dataTransfer.setData("e", this.props.data.uuid);
        setTimeout(() => {
            // TODO: Emit hide drops
            let event = new CustomEvent("showdrops", {detail: this.props.data.uuid});
            window.dispatchEvent(event);
        }, 500);
    };

    _onChange = (prop, value) => {
        this.props.onChange(this.props.data.uuid, prop, value);
    };

    _changeRules = (rules) => {
        this.props.onChange(this.props.data.uuid, 'conditions', rules);
    };

    render() {
        let isRoot = true,
            hasChildren = false;

        let subItems = [];
        if (['matrix', 'row', "group"].indexOf(this.props.data.type) >= 0) {
            hasChildren = true;
            let subArray = FormUtils.getFieldsAsArrayShallow(this.props.data.fields || {});

            subItems = subArray.map((item, index) => {
                let order = index;
                if (this.props.order) order = `${this.props.order}.${index + 1}`;
                return (
                    <FieldDefinition
                        definition={this.props.definition}
                        moveFieldUp={this.props.onMoveFieldUp}
                        moveFieldDown={this.props.onMoveFieldDown}
                        onDuplicateField={this.props.onDuplicateField}
                        onDeleteField={this.props.onDeleteField}
                        onAddChild={this._onAddChild}
                        onMoveField={this.props.onMoveField}
                        onDropNew={this.props.onDropNew}
                        fieldOptions={this.props.fieldOptions}
                        data={item}
                        parentName={item.name}
                        index={item.uuid}
                        order={order}
                        onChange={this.props.onChange}
                        key={item.uuid}/>
                )
            });
        }

        if (this.props.data.type == 'repeater') {
            hasChildren = true;
            let subArray = FormUtils.getFieldsAsArrayShallow(this.props.data.fields || {});

            subItems = subArray.map((item, index) => {
                let order = index;
                if (this.props.order) order = `${this.props.order}.${index + 1}`;
                return (
                    <FieldDefinition
                        definition={this.props.definition}
                        moveFieldUp={this.props.onMoveFieldUp}
                        moveFieldDown={this.props.onMoveFieldDown}
                        onDuplicateField={this.props.onDuplicateField}
                        onDeleteField={this.props.onDeleteField}
                        onAddChild={this._onAddChild}
                        onMoveField={this.props.onMoveField}
                        onDropNew={this.props.onDropNew}
                        fieldOptions={this.props.fieldOptions}
                        data={item}
                        parentName={item.name}
                        index={item.uuid}
                        order={order}
                        onChange={this.props.onChange}
                        key={item.uuid}/>
                )
            });
        }

        let actions = utils.copy(ACTIONS);
        let expander = this.state.showDetails ? "fa-minus-square" : "fa-plus-square";

        actions.push([expander, "TOGGLE"]);
        if (hasChildren) actions.push(["fa-plus", "ADD"]);

        let style = {
            padding: 0,
            background: "rgba(0,0,0,0.1)",
            border: "1px solid transparent"
        };
        if (this.state.showDetails) style.border = "1px solid #F2F2F2";

        let style_inner = {padding: 0};
        if (this.props.data.type == "header") {
            style_inner.background = "rgb(60, 146, 122)";
            style_inner.color = "#333333";
        }

        let icon = <i className={"fal " + ICONS[this.props.data.type] || "fa-list"}></i>;

        let labelShort = _formatLabel(this.props.data.label.en || this.props.data.label);

        let view = (
            <div className="column block block-overflow field-form">
                <ReportingForm
                    allRoot={true}
                    definition={FIELD_DEFINITION}
                    data={this.props.data}
                    enabled={true}
                    onChange={this._onChange}
                    errors={{}}/>
            </div>
        );

        if (this.state.view == "RULES") {
            view = (
                <RulesEditor
                    options={FormUtils.asOptions(this.props.definition)}
                    data={this.props.data.conditions || []}
                    onChange={this._changeRules}/>
            )
        }

        return (
            <div className="field-definition" style={style}>
                <div
                    onDragStart={this._dragStart}
                    onDragEnd={this._dragEnd}
                    className="field-definition-handle"
                    style={style_inner}
                    draggable={isRoot}>
                    <div className="row">
                        <div className="column block"
                             style={{maxWidth: "30px", padding: "8px 0px 8px 5px"}}> {this.props.order}
                        </div>
                        <div className="column block"
                             style={{maxWidth: "20px", padding: "8px 0px 8px 5px", textAlign: "center"}}>
                            {icon}
                        </div>
                        <div className="column block" style={{padding: "8px"}}>
                            <strong>{labelShort}</strong> - [{this.props.data.name.replace("DUPE_", "")}]
                        </div>
                        <ActionGroup
                            naked={true}
                            actions={actions}
                            onAction={this._action}/>
                    </div>
                </div>

                {this.state.showDetails ?
                    <div style={{border: "1px solid #262626", height: "500px", minHeight: 0, display: "flex"}}>
                        <div className="row">
                            <div className="column br block"
                                 style={{maxWidth: "35px", overflow: "hidden", position: "relative"}}>
                                <div className="ide-tabs">
                                    <div
                                        onClick={() => this._view("DEFAULT")}
                                        className={"ide-tab" + (this.state.view == "DEFAULT" ? " ide-tab-down" : "")}>
                                        <i className="fal fa-cog"></i>
                                    </div>
                                    <div
                                        onClick={() => this._view("RULES")}
                                        className={"ide-tab" + (this.state.view == "RULES" ? " ide-tab-down" : "")}>
                                        <i className="fal fa-cube"></i>
                                    </div>
                                </div>
                            </div>
                            {view}
                        </div>
                    </div>
                    : null}

                {hasChildren ?
                    <div style={{border: "1px solid #262626", minHeight: "100px"}}>
                        <div className="block">
                            {subItems}
                        </div>
                    </div>
                    : null}

                {this.props.isRoot ?
                    <div
                        ref={(el) => this._dropEl = el}
                        style={{display: "none"}}
                        onDragOver={(e) => e.preventDefault()}
                        className="field-drop-area"
                        onDrop={this._drop}>
                        Drop field here
                    </div>
                    : null}

            </div>
        )
    }
}

export default FieldDefinition;
