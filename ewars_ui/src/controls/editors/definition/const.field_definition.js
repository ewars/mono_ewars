import DROPPABLES from "../definition/const.droppables";

const FIELD_FORM = {
    label: {
        order: 0,
        i18: true,
        type: "text",
        label: "Field label",
        default: {en: "New Field"}
    },
    uuid: {
        type: "display",
        label: "Field ID",
        order: 1
    },
    type: {
        order: 2,
        type: "SELECT",
        label: "Field type",
        default: "text",
        options: DROPPABLES.filter(item => {
            return item.length > 1;
        })
    },
    unique: {
        order: 3,
        type: "BUTTONSET",
        label: "Unique",
        help: "Fields marked as unique will block duplicate records if a record contains the same unique values.",
        options: [
            [false, "No"],
            [true, 'Yes']
        ],
        default: false,
        conditions: [[
            "OR",
            "type:=:date",
            "type:=:interval",
            "type:=:location",
            "type:=:select",
            "type:=:number"
        ]]
    },
    name: {
        order: 4,
        type: "TEXT",
        label: "Field name",
        help: "This value will be used in exports and mappings for easier reading",
        required: true
    },
    "required": {
        order: 5,
        label: "REQUIRED",
        type: "BUTTONSET",
        options: [
            [false, "NO"],
            [true, "YES"]
        ],
        default: false,
        conditions: [
            ['type', 'neq', 'header'],
            ['type', 'neq', 'display'],
            ['type', 'neq', 'matrix'],
            ['type', 'neq', 'repeater'],
            ['type', 'neq', 'associated'],
            ['type', 'neq', 'row']
        ]
    },
    sti: {
        order: 6,
        type: 'location_type',
        label: 'Location type',
        required: true,
        conditions: [
            ['type', 'eq', 'location'],
            ['type', 'eq', 'reporting_location']
        ]

    },
    show_label: {
        order: 7,
        type: "BUTTONSET",
        label: "Show label",
        options: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    date_type: {
        order: 8,
        type: "interval",
        label: "Date type",
        required: true,
        default: "DAY",
        options: [
            ["DAY", "DAY"],
            ["WEEK", "WEEK"],
            ["MONTH", "MONTH"],
            ["YEAR", "YEAR"]
        ],
        conditions: [
            "ANY",
            ['type','eq', 'date'],
            ['type', 'eq', 'interval']
        ]
    },
    help: {
        order: 9,
        type: "TEXT",
        i18: true,
        label: "Field Help"
    },
    options: {
        order: 10,
        type: "OPTIONSDEF",
        label: "Options",
        i18: true,
        conditions: [
            "ALL",
            ['type', 'eq', 'select']
        ]
    },
    default: {
        order: 11,
        type: "TEXT",
        label: "DEFAULT_VALUE",
        conditions: [
            "ANY",
            "type:=:number",
            "type:=:select",
            "type:=:text",
            "type:=:textarea",
            "type:=:date",
            "type:=:location"
        ]
    },
    redacted: {
        order: 12,
        type: "BUTTONSET",
        label: "REDACTED",
        default: false,
        options: [
            [false, "No"],
            [true, "Yes"]
        ]
    },
    header_style: {
        order: 13,
        type: "SELECT",
        label: "HEADER_STYLE",
        default: "TITLE",
        options: [
            ["style-title", "TITLE"],
            ["style-sub-title", "SUB_TITLE"],
            ['H1', 'Heading 1'],
            ['H2', 'Heading 2'],
            ['H3', 'Heading 3'],
            ['H4', 'Heading 4']
        ],
        conditions: [
            "ALL",
            ['type', 'eq', 'header']
        ]
    },
    placeholder: {
        order: 14,
        type: "TEXT",
        label: "PLACEHOLDER",
        conditions: [
            "ANY",
            "type:=:text",
            "type:=:number",
            "type:=:textarea"
        ]
    },
    barcode: {
        order: 15,
        type: "BUTTONSET",
        label: "BARCODE",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        conditions: [
            "type:=:text"
        ]
    },
    allow_future: {
        order: 16,
        type: "BUTTONSET",
        label: "ALLOW_FUTURE_DATES",
        default: false,
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        conditions: [
            "ANY",
            "type:=:date",
            "type:=:datetime",
            "type:=:time",
            "type:=:interval",
            "type:=:period"
        ]
    },
    multiple: {
        order: 17,
        type: "BUTTONSET",
        label: "SELECT_MULTIPLE",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        default: false,
        conditions: [
            "ANY",
            "type:=:select",
            "type:=:user",
            "type:=:location",
            "type:=:location_type",
            "type:=:team"
        ]
    },
    max: {
        order: 18,
        type: "TEXT",
        label: "MAXIMUM",
        conditions: [
            "ALL",
            "type:=:number",
            "type:=:repeater",
            "type:=:select",
            "type:=:location"
        ]
    },
    min: {
        order: 19,
        type: "TEXT",
        label: "MINIMUM",
        conditions: [
            "ALL",
            "type:=:number",
            "type:=:repeater",
            "type:=:select",
            "type:=:location"
        ]
    },
    negative: {
        order: 20,
        type: "BUTTONSET",
        label: "ALLOW_NEGATIVE",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        default: true,
        conditions: [
            ['type', 'eq', 'number']
        ]
    },
    form: {
        order: 21,
        label: "Form",
        help: "Please ensure that users have access rights to the form in question",
        type: "form",
        conditions: [
            ['type', 'eq', 'associated']
        ]
    },
    assoc_fields: {
        order: 22,
        label: "Grid columns",
        type: "fields",
        vertical: true,
        multiple: true,
        field_id_source: "form",
        conditions: [
            ['type', 'eq', 'associated']
        ]
    },
    assoc_allow_create: {
        order: 23,
        label: "Allow creation",
        type: "BUTTONSET",
        options: [
            [false, "No"],
            [true, "Yes"]
        ],
        conditions: [
            ['type', 'eq', 'associated']
        ]
    },
    colour_mapping: {
        order: 24,
        label: "Colour mapping",
        type: "colour_mapping",
        help: "Change the colour of the field based on the value",
        required: false,
        conditions: [[
            "OR",
            "type:=:text",
            "type:=:numeric",
            "type:=:number"
        ]]
    },
    prefix: {
        type: "text",
        label: "Input prefix",
        conditions: [[
            "OR",
            "type:=:text",
            "type:=:numeric",
            "type:=:number"
        ]],
        order: 25
    },
    suffix: {
        type: "text",
        label: "Input Suffix",
        conditions: [[
            "OR",
            "type:=:text",
            "type:=:numeric",
            "type:=:number"
        ]],
        order: 26
    },
    repeater_view: {
        type: "select",
        options: [
            ['TABLE', 'Table view'],
            ['GRID', 'Grid view']
        ],
        label: "Repeater layout",
        conditions: [
            ["type", 'eq', 'repeater']
        ],
        order: 27
    }
}

export default FIELD_FORM;
