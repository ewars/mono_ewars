const DROPPABLE_TYPES = [
    ['Basic Field'],
    ["text", "Text field", 1],
    ['number', 'Numeric field', 1],
    ['textarea', 'Textarea field'],
    ['select', 'Select field'],
    ['lat_long', 'Lat/Lng field'],
    ['risk', 'Risk'],
    ['buttonset', 'Button Set'],
    ["organization", "Organization"],
    ['user', 'User'],
    ['team', 'Team'],
    ['language',' Language'],
    ["form", 'Form'],
    ['sms_gateway', 'SMS gateway'],
    ['role', 'User role'],
    ['tags', 'Tags'],

    ["Date Fields"],
    ['date', 'Date field', 1],
    ['time', 'Time'],
    ['datetime', 'Date/Time'],
    ['timezone', 'Timezone'],
    ["period", 'Period'],

    ["Location"],
    ['location', 'Location field', 1],
    ['location_group', 'Location group'],
    ['location_type', "Location type"],

    ['Complex'],
    ['matrix', 'Table'],
    ['repeater', 'Repeater'],
    ['associated', 'Associated'],
    ['group', 'Group'],

    ['Display'],
    ['display', 'Display field'],
    ['header', 'Header field'],
    ['markdown', 'Markdown field'],

    ['Files'],
    ['image', 'Image file'],
    ['video', 'Video file'],
    ['audio', 'Audio file'],
    ['file', 'File'],

    ['Reporting'],
    ['reporting_period', "Reporting period"],
    ["reporting_location", "Reporting location"],

    ['Workflow'],
    ['stage_name', "Stage Name"],
    ['stage_elapsed', "Stage elapsed time"],
    ['stage_enter', 'Stage enter date']
    // ["repeater", "Repeater"],
    // ['progress', "Progress bar"],
    // ['image', 'Image upload'],
    // ['file', 'File upload']
];

export default DROPPABLE_TYPES;
