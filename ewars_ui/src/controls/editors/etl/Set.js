import SetItem from "./SetItem";

class Set extends React.Component {
    constructor(props) {
        super(props);
    }

    _add = () => {
        ewars.store.dispatch({type: "SET_ADD_FIELD", id: this.props.indicator, path: this.props.path});
    };

    _addSet = () => {
        ewars.store.dispatch({type: "ADD_SET", id: this.props.indicator, path: this.props.path})
    };

    _onRemove = () => {
        ewars.store.dispatch({type: "REMOVE_SET", id: this.props.indicator, path: this.props.path});
    };

    _onChangeRule = (e) => {
        ewars.store.dispatch({
            type: "SET_CHANGE_RULE",
            id: this.props.indicator,
            path: this.props.path,
            value: e.target.value
        })
    };

    render() {

        let items = this.props.data.map((item, index) => {
            if (["ANY", "ALL"].indexOf(item) >= 0) {
                return (
                    <div className="application">
                        <div className="ide-row">
                            <div className="ide-col" style={{maxWidth: 55}}>
                                <select value={this.props.data[0]} onChange={this._onChangeRule}>
                                    <option value="ALL">All</option>
                                    <option value="ANY">Any</option>
                                </select>
                            </div>
                            <div className="ide-col border-right" style={{padding: 15}}> of the following are TRUE</div>
                            <div className="ide-col icon border-right" style={{maxWidth: 30, padding: 15}} onClick={this._add}>
                                <i className="fal fa-plus"/>
                            </div>
                            <div className="ide-col icon border-right" style={{maxWidth: 22, padding: 15}} onClick={this._addSet}>
                                <i className="fal fa-ellipsis-h"/>
                            </div>
                            <div className="ide-col icon" style={{maxWidth: 22, padding: 15}} onClick={this._onRemove}>
                                <i className="fal fa-trash"/>
                            </div>
                        </div>
                    </div>
                )
            } else if (typeof item == "string") {
                return <SetItem
                    data={item}
                    indicator={this.props.indicator}
                    path={`${this.props.path}.${index}`}
                    definition={this.props.definition}
                    index={index}/>
            } else {
                // This is sub-ste
                return <Set
                    data={item}
                    path={`${this.props.path}.${index}`}
                    index={index}
                    indicator={this.props.indicator}
                    definition={this.props.definition}/>
            }
        });

        return (
            <div className="set">
                {items}
            </div>
        )
    }
}

export default Set;