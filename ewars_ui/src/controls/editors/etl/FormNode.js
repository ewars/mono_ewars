function flatten(definition) {
    let fields = [];

    for (var i in definition) {
        fields.push({
            ...definition[i],
            name: i
        })
    }

    fields = fields.sort(function (a, b) {
        if (a.order < b.order) return -1;
        if (a.order > b.order) return 1;
        return 0;
    });

    return fields;
}

class FormNode extends React.Component {
    static defaultProps = {
        root: false
    };

    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = () => {
        if (this.props.data.fields || this.props.root) {
            this.setState({
                show: !this.state.show
            })
        }
    };

    _onDragStart = (e) => {
        ewars.emit("SHOW_DROPS");
        e.dataTransfer.setData("d", this.props.path);
    };

    _dragEnd = () => {
        ewars.emit("HIDE_DROPS");
    };

    render() {
        let children = [];
        if (this.state.show) {
            if (!this.props.root) {
                if (this.props.data.fields) {
                    let fields = flatten(this.props.data.fields);
                    children = fields.map(function (item) {
                        if (['display', 'header'].indexOf(item.type) < 0) {
                            let path = `${this.props.path}.${item.name}`;
                            return <FormNode data={item} path={path}/>
                        }
                    }.bind(this))
                }
            }
        }

        let hasChilds = this.props.data.fields ? true : false;
        if (this.props.root) hasChilds = true;

        let label = "Form";
        if (!this.props.root) label = ewars.I18N(this.props.data.label) + " [" + this.props.data.type + "]";

        let draggable = false;
        let className = "block-content";
        let onDragStart = null;
        if (["root", "matrix", "header", "display", "row"].indexOf(this.props.data.type) < 0) {
            draggable = true;
            className += " status-green";
            onDragStart = this._onDragStart;
        }

        let dropClass = "fal fa-folder";
        if (this.state.show) dropClass = "fal fa-folder-open";

        return (
            <div className="block">
                <div className={className} draggable={draggable} onDragStart={onDragStart} onDragEnd={this._dragEnd}
                     style={{padding: 0}} onClick={this._toggle}>
                    <div className="ide-row">
                        {hasChilds ?
                            <div className="ide-col border-right" style={{maxWidth: 29, padding: 8, textAlign: "center"}}>
                                <i className={dropClass}></i>
                            </div>
                            : null}
                        <div className="ide-col" style={{padding: 8}}>{label}</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="block-children">
                        {this.props.root ?
                            <FormNode
                                name="data_date"
                                path="data_date"
                                data={{type: "date", "label": "Report Date"}}/>
                            : null}
                        {this.props.root ?
                            <FormNode
                                name="data"
                                path="data"
                                data={{type: "root", fields: this.props.data, label: "Data"}}/>
                            : null}

                        {!this.props.root ? children : null}

                    </div>
                    : null}
            </div>
        )
    }
}

export default FormNode;