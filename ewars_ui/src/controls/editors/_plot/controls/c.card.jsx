import React from "react"

import TextField from "../../../fields/f.text.jsx";
import SelectField from "../../../fields/f.select.jsx";
import ActionGroup from "../../../c.actiongroup.jsx";

const AGG_OPTIONS = [
    ['SUM', 'Sum'],
    ["MED", 'Median'],
    ["AVG", 'Average']
];

const CALC_OPTIONS = [
    ['POINT', '@ point'],
    ['AGGREGATE', '@ aggregate']
];


class FormulaEditor extends React.Component {
    render() {
        return (
            <div className="row" style={{margin: "3px"}}>
                <div className="column" style={{maxWidth: "20px", textAlign: "center", background: "black"}}>
                    <i>fx</i>
                </div>
                <div className="column">
                    <TextField
                        mask="formula"
                        onChange={this.props.onChange}
                        value={this.props.value || ""}/>
                </div>
            </div>
        )
    }
}


class Nugget extends React.Component {
    render() {
        return (
            <div className="nugget">
                <div className="row">
                    <div className="column" style={{maxWidth: "20px"}}>[icon]</div>
                    <div className="column">[name]</div>
                    <div className="column" style={{maxWidth: "20px"}}><i className="fal fa-caret-down"></i></div>
                </div>
            </div>
        )
    }
}


class AxisSelector extends React.Component {
    render() {
        return (
            <SelectField
                options={[]}
                value={""}
                onChange={this.props.onChange}/>
        )
    }
}

class Row extends React.Component {
    render() {
        return (
            <div className="row block" style={{maxHeight: "30px"}}>
                {this.props.children}
            </div>
        )
    }
}

class MainView extends React.Component {
    _onDrop = (e) => {
        console.log(e.dataTransfer.getData("n"));
        let data = JSON.parse(e.dataTransfer.getData("n"));

        let newItem = {
            ...data,
            p: this.props.data._
        }
    };

    _onDragOver = (e) => {
        e.preventDefault();
    };

    render() {
        return (
            <div className="column" style={{paddingTop: "8px", flex: 5}}>
                <Row>
                    <TextField
                        onChange={this._onChange}
                        value={this.props.data.lbl || ""}
                        name="label"/>
                </Row>
                <Row>
                    <SelectField
                        options={AGG_OPTIONS}
                        name="agg"
                        value={this.props.data.agg || ""}
                        onChange={this._onChange}/>
                </Row>
                {this.props.data.t == "C" ?
                    <Row>
                        <FormulaEditor
                            onChange={this._onFormulaChange}
                            values={this.props.data.fx || ""}/>
                    </Row>
                    : null}
                {this.props.data.t == "C" ?
                    <Row>
                        <SelectField
                            options={CALC_OPTIONS}
                            onChange={this._onChange}
                            name="calc_context"
                            value={this.props.data.fxp || ""}/>
                    </Row>
                    : null}
                {this.props.data.t != 'C' ?
                    <div
                        onDrop={this._onDrop}
                        onDragOver={this._onDragOver}
                        className="row block bt" style={{paddingBottom: "16px"}}>
                        <Nugget/>
                        <Nugget/>
                    </div>
                    : null}
            </div>
        )
    }
}

class StyleView extends React.Component {
    render() {
        return (
            <div className="column" style={{paddingTop: "8px"}}>

            </div>
        )
    }
}

class LocationFilterView extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="column br block" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div className="ide-tab"><i className="fal fa-cog"></i></div>
                        <div className="ide-tab"><i className="fal fa-cube"></i></div>
                    </div>
                </div>
                <div className="column">

                </div>
            </div>
        )
    }
}

class DateFilterView extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="column br block" style={{maxWidth: "35px", position: "relative", overflow: "hidden"}}>
                    <div className="ide-tabs">
                        <div className="ide-tab"><i className="fal fa-cog"></i></div>
                        <div className="ide-tab"><i className="fal fa-cube"></i></div>
                    </div>
                </div>
                <div className="column">

                </div>
            </div>
        )
    }
}

class PopoverEditor extends React.Component {
    render() {
        return (
            <div className="column" style={{paddingTop: "8px"}}>

            </div>
        )
    }
}

class DimFilter extends React.Component {
    render() {
        return (
            <div></div>
        )
    }
}

class Card extends React.Component {
    state = {
        view: 'DEFAULT'
    };

    constructor(props) {
        super(props);
    }

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    _action = (action) => {
        switch (action) {
            case "DELETE":
                this.props.onRemoveNode(this.props.data._);
                break;
            default:
                break;
        }
    };


    render() {
        let view;

        if (this.state.view == "DEFAULT") view = <MainView nodes={[]} data={this.props.data}/>;
        if (this.state.view == "STYLE") view = <StyleView nodes={[]} data={this.props.data}/>;
        if (this.state.view == "LOCATION") view = <LocationFilterView nodes={[]} data={this.props.data}/>;
        if (this.state.view == "DATE") view = <DateFilterView nodes={[]} data={this.props.data}/>;
        if (this.state.view == "POPOVER") view = <PopoverEditor nodes={[]} data={this.props.data}/>;
        if (this.state.view == "DIM_FILTER") view = <DimFilter nodes={[]} data={this.props.data}/>;

        let style = {
            maxWidth: "300px",
            minWidth: "300px",
            border: "1px solid #505152",
            margin: "8px 4px",
            background: "#333333",
            borderRadius: "3px"
        };

        if (this.props.data.t == "C") {
            style.background = "rgb(58, 32, 1)";
        }

        if (this.props.data.t == "D") {
            style.border = "1px solid lightblue";
        }

        console.log(this.props.data);


        return (
            <div className="column card"
                 style={style}>
                <div className="row bb gen-toolbar" style={{minHeight: "35px", maxHeight: "35px", borderTopRightRadius: "3px", borderTopLeftRadius: "3px"}}>
                    <div className="column">
                        <div className="groupbar">
                            <div
                                onClick={() => this._view("DEFAULT")}
                                className={"bar-item" + (this.state.view == "DEFAULT" ? " active" : "")}>
                                <i className="fal fa-cog"></i>
                            </div>
                            {this.props.data.t == "M" ?
                                <div
                                    onClick={() => this._view("STYLE")}
                                    className={"bar-item" + (this.state.view == "STYLE" ? " active" : "")}>
                                    <i className="fal fa-paint-brush"></i>
                                </div>
                                : null}
                            {this.props.data.ft == "location" ?
                                <div
                                    onClick={() => this._view("LOCATION")}
                                    className={"bar-item" + (this.state.view == "LOCATION" ? " active" : "")}>
                                    <i className="fal fa-map-marker"></i>
                                </div>
                                : null}
                            {this.props.data.ft == "DATE" ?
                                <div
                                    onClick={() => this._view("DATE")}
                                    className={"bar-item" + (this.state.view == "DATE" ? " active" : "")}>
                                    <i className="fal fa-calendar"></i>
                                </div>
                                : null}
                            {this.props.data.t == "M" ?
                                <div
                                    onClick={() => this._view("POPOVER")}
                                    className={"bar-item" + (this.state.view == "POPOVER" ? " active" : "")}>
                                    <i className="fal fa-comment"></i>
                                </div>
                                : null}
                            {this.props.data.t == "D" ?
                                <div
                                    onClick={() => this._view("DIM_FILTER")}
                                    className={"bar-item " + (this.state.view == "DIM_FILTER" ? " active" : "")}>
                                    <i className="fal fa-filter"></i>
                                </div>
                                : null}

                        </div>
                    </div>
                    <ActionGroup
                        naked={true}
                        actions={[['fa-trash', 'DELETE', 'Remove']]}
                        onAction={this._action}/>
                </div>
                {view}
                <div className="row block bt gen-toolbar card-foot" style={{minHeight:"35px", maxHeight: "35px", borderBottomLeftRadius:"3px", borderBottomRightRadius: "3px"}}>
                    <AxisSelector/>
                </div>
            </div>
        )
    }
}


export default Card;