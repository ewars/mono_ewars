import React from "react";
import {instance} from "../../plot/plot_instance";


const TYPES = {
    DEFAULT: ['ANNOTATION', 'AXIS', 'BAND', 'BELL', 'LEGEND', 'LINE', 'SCALE', 'TREND', 'STACK', "GROUP"],
    TABLE: ['LEGEND', 'SCALE', 'VAL_MAP', 'COL_MAP'],
    DATAPOINT: ['SCALE', 'VAL_MAP', 'COL_MAP'],
    PIE: ['LEGEND', "GROUP"],
    MAP: ['LEGEND', 'SCALE', 'MAP_LAYER', 'MAP_POINT', 'COL_MAP', "GROUP"],
    HEATMAP: ['LEGEND', 'SCALE', 'VAL_MAP', 'COL_MAP', "GROUP"],
    OTHER: ['LEGEND', 'TEXT']
};


const CONTROLS = [
    ["ANNOTATION", "Annotation", "fa-font"],
    ["AXIS", "Axis", "fa-chart-line"],
    ["BAND", "Plot band", "fa-column"],
    ['BELL', 'Bell curve', "fa-bell"],
    ["LEGEND", "Legend", "fa-tag"],
    ["LINE", "Plot line", "fa-bar"],
    ['LOG', 'Logarithm line', "fa-chart-line"],
    ['MAP_LAYER', 'Static Map layer', "fa-map"],
    ['MAP_POINT', 'Static Map point', "fa-map-marker"],
    ['MAP_POLY', 'Static map poly', "fa-object"],
    ['MAP_NAV', 'Map navigation controls', "fa-move"],
    ['MAP_ZOOM', 'Map zoom controls', "fa-zoom"],
    ["SCALE", "Scale", "fa-scales"],
    ["TEXT", "Text", "fa-text"],
    ["TREND", "Trend line", "fa-chart-line"],
    ['VAL_MAP', 'Value mapping', "fa-cube"],
    ['COL_MAP', 'Colour mapping', "fa-cube"],
    ['STACK', 'Stacking group', "fa-object-group"],
    ["GROUP", 'Group', "fa-copy"]
];

class Control extends React.Component {
    _onDragStart = (e) => {

        window.dispatchEvent(new CustomEvent("showdrops", {details: "C"}));
        e.dataTransfer.setData("n", this.props.data[0])
    };

    _onDragEnd = (e) => {
        ewars.emit('HIDE_DROPS');
        window.dispatchEvent(new CustomEvent("hidedrops"));
    };

    render() {
        return (
            <div className="dash-handle"
                 onDragEnd={this._onDragEnd}
                 draggable={true}
                 onDragStart={this._onDragStart}>
                <div className="block-content">
                    <i className={"fal " + this.props.data[2]}></i>&nbsp;{this.props.data[1]}
                </div>
            </div>
        )
    }
}

class ControlsList extends React.Component {
    static defaultProps = {
        instance_type: "DEFAULT"
    };

    constructor(props) {
        super(props);

        let inst = TYPES[this.props.instance_type] || [];
        this.options = CONTROLS.filter(item => {
            return inst.indexOf(item[0]) >= 0;
        })
    }

    render() {
        let style = {
            transition: "all 0.5s ease"
        };
        if (this.props.show) {
            style = {
                maxHeight: "100%",
                flex: 5
            }
        } else {
            style = {
                padding: 0,
                height: "0px",
                maxHeight: "0px"
            }
        }

        return (
            <div className="row block block-overflow" style={style}>
                {this.options.map(item => {
                    return <Control data={item} key={item[0]}/>
                })}

            </div>
        )
    }
}

export default ControlsList;