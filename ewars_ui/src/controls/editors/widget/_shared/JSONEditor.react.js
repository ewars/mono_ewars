import {Layout, Row, Cell} from "../../../cmp/Layout";
import Toolbar from "../../../cmp/Toolbar";
const Button = require("../../ButtonComponent.react");

const STYLE = {
    display: "block",
    width: "100%",
    height: "100%",
    position: "relative"
};

const EDIT_STYLE = {
    padding: 10,
    height: "100%"
};

var JSONEditor = React.createClass({
    getInitialState: function () {
        return {
            data: '{}',
            error: false
        }
    },

    componentWillMount() {
        this.state.data = JSON.stringify(this.props.data || {}, null, "\t");
    },

    componentWillReceiveProps(nextProps) {
        this.state.data = JSON.stringify(nextProps.data || {}, null, "\t");
    },

    _onChange: function (e) {
        let isValid = true;

        this.setState({
            data: e.target.value,
            error: false
        })
    },

    _save: function () {
        let isValid = true;

        try {
            JSON.parse(this.state.data);
        } catch (e) {
            isValid = false;
        }

        if (!isValid) {
            this.setState({
                error: true
            })
        } else {
            ewars.growl("Change(s) applied");
            this.props.onSave(JSON.parse(this.state.data));
        }
    },

    render: function () {
        return (
            <ewars.d.Layout>
                <ewars.d.Toolbar label="Source Editor">
                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            icon="fa-save"
                            onClick={this._save}
                            label="Update"/>
                    </div>

                </ewars.d.Toolbar>
                <ewars.d.Row>
                    <ewars.d.Cell>
                        {this.state.error ?
                            <div className="error" style={{marginBottom: 0}}><i
                                className="fal fa-exclamation-triangle"></i>&nbsp;There is an error in your JSON, please
                                review and make changes.</div>
                            : null}
                        <div style={STYLE} ref="editorPanel">
                            <textarea value={this.state.data} style={EDIT_STYLE} onChange={this._onChange}></textarea>
                        </div>
                    </ewars.d.Cell>
                </ewars.d.Row>
            </ewars.d.Layout>
        )
    }
});

module.exports = JSONEditor;