var CONSTANTS = require("../../../constants");

var VertTab = require("../../ide/VertTab.react");

var VIEWS = {
    SETTINGS: "SETTINGS",
    SOURCE: "SOURCE"
};

var Settings = require("./SettingsView.react");
var JSONEditor = require("../_shared/JSONEditor.react");

var Editor = React.createClass({
    displayName: "Raw Widget Editor",

    getInitialState: function () {
        return {
            view: VIEWS.SETTINGS
        }
    },

    _onChange: function (prop, value) {
        this.props.onChange(prop, value);
    },

    _changeShoulder: function (view) {
        this.setState({
            view: view
        })
    },

    render: function () {

        var view;
        if (this.state.view == VIEWS.SETTINGS) {
            view = <Settings
                mode={this.props.mode}
                data={this.props.data}
                onChange={this._onChange}/>;
        }
        if (this.state.view == VIEWS.SOURCE) {
            view = <JSONEditor
                mode={this.props.mode}
                data={this.props.data}/>
        }

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col ide-relative" style={{maxWidth: "34px"}}>
                        <div className="ide-panel">
                            <div className="ide-tabs">
                                <VertTab
                                    view={VIEWS.SETTINGS}
                                    label="Settings"
                                    icon="fa-globe"
                                    active={this.state.view == VIEWS.SETTINGS}
                                    onClick={this._changeShoulder}/>
                                <VertTab
                                    view={VIEWS.SOURCE}
                                    label="Source"
                                    icon="fa-code"
                                    active={this.state.view == VIEWS.SOURCE}
                                    onClick={this._changeShoulder}/>
                            </div>
                        </div>
                    </div>

                    <div className="ide-col">
                        {view}
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Editor;