var Button = require("../../ButtonComponent.react");

import DEFAULTS_LAYER from './defaults.layer';
import FORM_LAYER from "./f.layer";

var Form = require("../../Form.react");

if (!ewars.copy) ewars.copy = function (item) {
    return JSON.parse(JSON.stringify(item));
};

class Column extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isExpanded: false,
            errors: []
        }
    }

    _moveUp = () => {
        this.props.moveUp(this.props.data, this.props.index);
    };

    _moveDown = () => {
        this.props.moveDown(this.props.data, this.props.index);
    };

    _edit = () => {
        this.setState({
            isExpanded: this.state.isExpanded ? false : true
        })
    };

    _delete = () => {
        this.props.onRemove(this.props.index);
    };

    _update = (data, prop, value, _data) => {
        console.log(data, _data);
        this.props.onChange(this.props.index, prop, value, data);
    };

    render() {
        var colTitle = ewars.I18N(this.props.data.title);

        var editIcon = "fal fa-pencil";
        if (this.state.isExpanded) editIcon = "fal fa-times";

        return (
            <div className="data-item-wrapper">
                <div className="series-list-item">
                    <div className="name">{colTitle}</div>
                    <div className="control-wrapper">
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-caret-up"
                                onClick={this._moveUp}/>
                            <ewars.d.Button
                                icon="fa-caret-down"
                                onClick={this._moveDown}/>
                        </div>
                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon={editIcon}
                                onClick={this._edit}/>
                            <ewars.d.Button
                                icon="fa-trash"
                                colour="red"
                                onClick={this._delete}/>
                        </div>
                    </div>
                </div>
                {this.state.isExpanded ?
                    <div className="data-item-settings">
                        <div className="ide-settings-content" style={{paddingBottom: 16}}>
                            <div className="ide-settings-form">
                                <div className="chart-edit">
                                    <Form
                                        definition={FORM_LAYER}
                                        data={this.props.data}
                                        updateAction={this._update}
                                        readOnly={false}
                                        errors={this.state.errors}/>

                                </div>
                            </div>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
};

class LayerManager extends React.Component {
    constructor(props) {
        super(props)
    }

    _addSeries = () => {
        let layers = ewars.copy(this.props.data.layers || []);
        layers.push({
            ...DEFAULTS_LAYER,
            uuid: ewars.utils.uuid()
        });
        this.props.onChange(null, "layers", layers);
    };

    _onChange = (index, prop, value) => {
        let layers = ewars.copy(this.props.data.layers || []);
        layers[index][prop] = value;
        this.props.onChange(null, "layers", layers);
    };

    _moveUp = (col, index) => {
        if (index <= 0) return;

        let targetIndex = index - 1;
        let layers = ewars.copy(this.props.data.layers || []);
        let tmp = layers[targetIndex];
        layers[targetIndex] = col;
        layers[index] = tmp;
        this.props.onChange(null, "layers", layers);
    };

    _moveDown = (col, index) => {
        if (index >= this.props.data.layers.length - 1) return;

        let targetIndex = index + 1;
        let layers = ewars.copy(this.props.data.layers || []);
        let tmp = cols[targetIndex];
        layers[targetIndex] = col;
        layers[index] = tmp;
        this.props.onChange(null, "layers", layers);
    };

    _onRemove = (index) => {
        let layers = ewars.copy(this.props.data.layers || []);
        layers.splice(index, 1);
        this.props.onChange(null, "layers", layers);
    };

    render() {
        return (
            <div className="data-manager" id="dm">
                <ewars.d.Layout>
                    <ewars.d.Toolbar label="Layers" icon="fa-align-justify">

                        <div className="btn-group pull-right">
                            <ewars.d.Button
                                icon="fa-plus"
                                onClick={this._addSeries}
                                label="Add Layer"/>
                        </div>
                    </ewars.d.Toolbar>
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <div className="ide-panel ide-panel-absolute ide-scroll">
                                <div className="series-list">
                                    {(this.props.data.layers || []).map((item, index) => {
                                        return (
                                            <Column
                                                onChange={this._onChange}
                                                data={item}
                                                moveUp={this._moveUp}
                                                moveDown={this._moveDown}
                                                onRemove={this._onRemove}
                                                index={index}/>
                                        )
                                    })}
                                </div>
                            </div>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
};


export default LayerManager;
