module.exports = {
    spec: 2,
    // General
    title: {en: ""},
    show_title: true,
    type: "SERIES",
    interval: "WEEK",
    tools: false,
    zoom: false,
    animate: false,
    navigator: false,

    period: [],

    // Category
    position: "bottom",
    start_on_axis: false,
    offset: 0,
    category_title: null,
    title_bold: true,
    title_colour: "#000000",
    title_font_size: 14,
    title_rotation: 0,

    // Legend
    legend_enabled: false,
    reverse_order: false,
    legend_font_size: 11,

    // Number Formatting
    number_format: "0,0.00",
    percent_precision: 2,
    precision: -1,

    series: [],

    y_axis_label: "Number",
    y_axis_format: "0",
    y_axis_allow_decimals: false,
    stack: false,
    max_y_value: null,
    y_tick_interval: null
};