/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

module.exports = {
    gen_header: {
        type: "header",
        label: "General"
    },
    dataType: {
        type: "hidden"
    },

    title: {
        type: "language_string",
        label: {
            en: "Title"
        },
        help: {
            en: "The series title displayed in the chart, if left blank, indicator name will be used"
        }
    },
    loc_spec_header: {
        type: "header",
        label: {
            en: "Location Specification"
        }
    },
    loc_spec: {
        type: "button_group",
        label: "Location source",
        options: [
            ["SPECIFIC", "Specific Location"],
            ["GENERATOR", "Generator"],
            ["GROUP", "Group(s)"]
        ]
    },
    group_ids: {
        type: "location_group",
        label: "Location Group(s)",
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GROUP"]
            ]
        }
    },
    group_output: {
        type: "button_group",
        label: "Group Output",
        help: "Aggregate: A single line is produced aggregating the locations data together, Individual: Individual lines are produced for each location in the group",
        options: [
            ["AGGREGATE", "Aggregate"],
            ["INDIVIDUAL", "Individual"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GROUP"]
            ]
        }
    },
    generator_locations_parent: {
        type: "location",
        label: {
            en: "Generator parent location"
        },
        hideInactive: true,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_locations_type: {
        type: "select",
        label: {
            en: "Generator location type"
        },
        optionsSource: {
            resource: "location_type",
            labelSource: "name",
            valSource: "id",
            query: {}
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    generator_location_status: {
        type: "button_group",
        label: "Location status",
        options: [
            ["ANY", "Any"],
            ["ACTIVE", "Active"],
            ["INACTIVE", "Inactive"]
        ],
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "GENERATOR"]
            ]
        }
    },
    location: {
        type: "location",
        required: true,
        label: {
            en: "Location"
        },
        help: {
            en: "The location to source the indicator data at"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["loc_spec", "eq", "SPECIFIC"]
            ]
        }
    },
    ind_spec_header: {
        type: "header",
        label: {
            en: "Indicator Specification"
        }
    },
    source_type: {
        type: "button_group",
        label: "Data source",
        options: [
            ["SERIES", "Indicator"],
            ["SERIES_COMPLEX", "Complex"]
        ]
    },
    indicator: {
        type: "indicator",
        required: true,
        label: {
            en: "Indicator Source"
        },
        help: {
            en: "The data to use"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SERIES"]
            ]
        }
    },
    formula: {
        type: "text",
        required: true,
        label: {
            en: "Formula"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SERIES_COMPLEX"]
            ]
        }
    },
    series: {
        type: "_complex_sources",
        label: {
            en: "Variables"
        },
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["source_type", "eq", "SERIES_COMPLEX"]
            ]
        }
    },
    override: {
        type: "button_group",
        label: "Period",
        options: [
            ["INHERIT", "Inherit"],
            ["OVERRIDE", "Override"]
        ]
    },
    period: {
        type: "date_range",
        label: false,
        conditional_bool: true,
        conditions: {
            application: "all",
            rules: [
                ["override", "eq", "OVERRIDE"]
            ]
        }
    },
    style_header: {
        type: "header",
        label: {
            en: "Styling"
        }
    },
    style: {
        type: "button_group",
        label: "Style",
        required: true,
        options: [
            ["line", "Line"],
            ["bar", "Bar"],
            ["area", "Area"],
            ["column", "Column"],
            ["scatter", "Scatter"],
            ["spline", "Spline"],
            ["waterfall", "Waterfall"]
        ],
        help: {
            en: "The type of series chart to display"
        }
    },
    colour: {
        type: "color",
        label: "Colour",
        help: {
            en: "The colour to use for the series, if unset, will default to indicator defined colour or random"
        }
    },
    line_width: {
        type: "text",
        label: {
            en: "Line Width"
        }
    },
    line_style: {
        type: "select",
        label: "Line Style",
        options: [
            ["Solid", "Solid"],
            ["ShortDash", "Short Dash"],
            ["ShortDot", "Short Dot"],
            ["ShortDashDot", "Short Dash Dot"],
            ["ShortDashDotDot", "Short Dash Dot Dot"],
            ["Dot", "Dot"],
            ["Dash", "Dash"],
            ["LongDash", "Long Dash"],
            ["DashDot", "Dash Dot"],
            ["LongDashDot", "Long Dash Dot"],
            ["LongDashDotDot", "Long Dash Dot Dot"]
        ],
        conditional_bool: true,
        conditions: {
            application: "any",
            rules: [
                ["style", "eq", "spline"],
                ["style", "eq", "line"],
                ["style", "eq", "area"]
            ]
        }
    },
    marker_radius: {
        type: "text",
        label: {
            en: "Marker Radius"
        }
    },
    marker_symbol: {
        type: "button_group",
        label: "Marker style",
        options: [
            ["circle", "Circle"],
            ["square", "Square"],
            ["diamond", "Diamond"],
            ["triangle", "Triangle"],
            ["triangle-down", "Triangle-Down"]
        ]
    }
};

