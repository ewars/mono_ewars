/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

module.exports = {
    spec: 2,
    title: {en: "New Widget"},
    type: "CATEGORY",
    interval: "WEEK",
    style: "DEFAULT",
    slices: [],
    show_legend: false,
    slice_order: "NONE",
    width: null,
    height: null,
    hide_no_data: false,
    y_axis_type: "NUMERIC",
    y_axis_label: "Number",
    y_axis_format: "0",
    y_axis_allow_decimals: false,
    start_date: new Date(),
    end_date: new Date()
};