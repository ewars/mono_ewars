/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

module.exports = {
    title: {
        en: "New Category"
    },
    colour: null,
    start_date: new Date(),
    end_date: new Date(),
    indicator: null,
    location: null,
    reduction: "SUM",
    source_type: "SLICE",
    interval: "NONE",
    loc_spec: "SPECIFIC",
    override: "INHERIT"

};