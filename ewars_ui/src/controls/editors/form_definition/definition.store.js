/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

const Dispatcher = require("../../../common/dispatchers/ClientDispatcher");
var EventEmitter = require("events").EventEmitter;
var Constants = require("./FormDefinitionConstants");
var assign = require("object-assign");

var CHANGE_EVENT = "change";

var _items = {};
var _itemsFlat = {};
var _itemsArray = [];


var _fieldPrototype = function (instantiationType) {
    var newData = {
        type: instantiationType ? instantiationType : "text",
        label: "Untitled Field " + ewars.utils.uniqueId(),
        uuid: ewars.utils.uuid()
    };

    var name = newData.label.replace(/\s/g, "_").toLowerCase();

    return [name, newData];
};

function _setDefinition(definition) {

    _items = JSON.parse(JSON.stringify(definition));
    // Need to ensure that all fields have a uuid
    _normalize(_items);
    _items = _getArrayAsObject(_fixNumbering(_getFieldsAsArray(_items)));
}

/**
 * Fixes numbering issues
 * @param items {array}
 * @returns asArray {array}
 */
function _fixNumbering(items) {
    var asArray = items.sort(_comparator);
    var counter = 0;
    for (var i in asArray) {
        asArray[i].order = counter;

        if (asArray[i].fields) {
            asArray[i].fields = _fixNumbering(asArray[i].fields);
        }

        counter++;
    }

    return asArray;

}

// Fixes issues like missin gorder and uuid properties.
function _normalize(sourceFields) {
    var prevItem = null;
    for (var token in sourceFields) {
        if (!sourceFields[token].uuid) sourceFields[token].uuid = ewars.utils.uuid();

        if (sourceFields[token].fields) {
            _normalize(sourceFields[token].fields);
        }
    }
}

function _comparator(a, b) {
    if (parseInt(a.order) < parseInt(b.order)) {
        return -1;
    }
    if (parseInt(a.order) > parseInt(b.order)) {
        return 1;
    }
    return 0;
}

function _getFieldsAsArray(sourceFields) {
    var fields = [];
    for (var token in sourceFields) {
        if (token) {
            var field = JSON.parse(JSON.stringify(sourceFields[token]));
            field.name = token;

            if (field.fields) {
                var children = _getFieldsAsArray(field.fields);
                field.fields = children;
            }

            fields.push(field);
        }
    }

    return fields.sort(_comparator);
}

function _getArrayAsObject(sourceArray) {
    var fields = {};

    for (var index in sourceArray) {
        var field = sourceArray[index];
        fields[field.name] = field;
        fields[field.name].order = index;

        if (field.fields) {
            fields[field.name].fields = _getArrayAsObject(field.fields);
        }

        delete fields[field.name].name;
    }

    return fields;
}

function _updateFieldByUUID(uuid, prop, value) {

}

function _setActiveFieldByUUID(uuid) {

}

function _getFormFieldOptions(sourceFields, path, namePath) {
    var options = [];
    var subPath = path;
    if (!subPath) subPath = [];
    var curLabelPath = namePath;
    if (!curLabelPath) curLabelPath = [];

    for (var token in sourceFields) {
        var field = sourceFields[token];
        if (["matrix", "row"].indexOf(field.type) >= 0) {
            if (field.fields) {
                var pathVal = JSON.parse(JSON.stringify(subPath));
                pathVal.push(token);
                var nextLabelPath = JSON.parse(JSON.stringify(curLabelPath));
                nextLabelPath.push(ewars.I18N(field.label));
                var children = _getFormFieldOptions(field.fields, pathVal, nextLabelPath);
                options.push.apply(options, children);
            }
        } else {
            if (["display", "header"].indexOf(field.type) < 0) {
                var pathVal = token;
                if (subPath.length > 0) pathVal = subPath.join(".") + "." + token;
                var labelPath = ewars.I18N(field.label);
                if (subPath.length > 0) labelPath = curLabelPath.join(" > ") + " > " + ewars.I18N(field.label);
                options.push([pathVal, field, labelPath]);
            }
        }

    }

    return options;
}

function _iterateChildren(childs, fn, basePath, parent) {
    if (!basePath) basePath = "";
    for (var i in childs) {
        var path = basePath + "." + i;
        var child = childs[i];
        fn(i, child, childs, path.slice(1, path.length), parent);

        if (child.fields) {
            _iterateChildren(child.fields, fn, path, child);
        }
    }
}

function _addChildToField(uuid) {
    _iterateChildren(_items, function (i, item, curItems) {
        if (item.uuid == uuid) {
            if (!item.fields) item.fields = {};
            if (item.type == "matrix") {
                var newField = _fieldPrototype("row");
            } else {
                var newField = _fieldPrototype();
            }
            newField[1].order = Object.keys(item.fields).length;
            item.fields[newField[0]] = newField[1];
        }
    });
}

/**
 * Changes the `name` property of a field
 * @param uuid {string} The uuid of the field to update
 * @param value {string} The new name to give the field
 * @private
 */
function _changeFieldName(uuid, value) {
    var itemPath = null;
    var found;
    _iterateChildren(_items, function (i, item, curItems, path) {
        if (item.uuid == uuid) {
            found = item;
            itemPath = path;
        }
    });

    if (itemPath) {
        // Path is at root
        if (itemPath.indexOf(".") < 0) {
            // Root level key change
            found.name = value;
            _items[value] = JSON.parse(JSON.stringify(found));
            delete _items[itemPath];
        } else {

            var curPath = _items;
            var pathSegments = itemPath.split(".");
            pathSegments = pathSegments.slice(0, pathSegments.length - 1);

            var itemCurrentName = itemPath.split(".");
            itemCurrentName = itemCurrentName.slice(itemCurrentName.length - 1, itemCurrentName.length);

            for (var i in pathSegments) {
                if (curPath[pathSegments[i]]) {
                    curPath = curPath[pathSegments[i]].fields;
                }
            }

            if (curPath) {
                var copy = JSON.parse(JSON.stringify(curPath[itemCurrentName]));
                copy.name = value;
                curPath[value] = copy;
                delete curPath[itemCurrentName];
            }
        }
    }

}

/**
 * Sets a specific fields property
 * @param uuid {string} The uuid of the field
 * @param prop {string} The property name to change
 * @param value {*} The value to set the property to
 * @private
 */
function _setFieldProperty(uuid, prop, value) {
    // name is hndled separately as it is used as an identifier within the system
    if (prop == "name") {
        _changeFieldName(uuid, value);
    } else {
        _iterateChildren(_items, function (name, item) {
            if (item.uuid == uuid) {
                item[prop] = value;
            }
        })
    }

}

/**
 * Delete a field from the form definition
 * @param uuid
 * @private
 */
function _deleteField(uuid) {

    _iterateChildren(_items, function (name, child, items) {
        if (child.uuid == uuid) {
            delete items[name];
        }
    })

}

function _addField(field) {
    // Get the highest ordering

    var high = 0;
    _.each(_items, function (item) {
        if (item.order > high) high = item.order;
    }, this);

    field.order = high + 1;
    _items[field.name] = field;
    _normalize(_items);
}

function _convertToObject(def) {
    var result = {};

    _.each(def, function (item) {
        result[item.name] = item;

        if (item.fields) {
            result[item.name].fields = _convertToObject(item.fields);
        }

    }, this);

    return result;
}

function _moveField(item_uuid, direction) {
    var parentField;

    // Find the items group
    // Find the item
    // Move the item
    // Reset the groups items

    _iterateChildren(_items, function (name, child, items, path, parent) {
        if (child.uuid == item_uuid) {
            parentField = parent;
        }
    });

    var itemIndex;
    var fieldsAsArray;
    if (parentField) {
        fieldsAsArray = _getFieldsAsArray(parentField.fields).sort(_comparator);
    } else {
        fieldsAsArray = _getFieldsAsArray(_items).sort(_comparator);
    }

    for (var i in fieldsAsArray) {
        if (fieldsAsArray[i].uuid == item_uuid) {
            itemIndex = parseInt(i);
        }
    }

    var movement = parseInt(itemIndex) + parseInt(direction);

    fieldsAsArray.splice(movement, 0, fieldsAsArray.splice(itemIndex, 1)[0]);

    var counter = 0;
    _.each(fieldsAsArray, function (item) {
        item.order = counter;
        counter++;
    }, this);
    var asObj = _convertToObject(fieldsAsArray);

    if (parentField) {
        parentField.fields = asObj;
    } else {
        _items = asObj;
    }
}

var FormDefinitionStore = assign({}, EventEmitter.prototype, {
    /**
     * Return the collection
     * @returns {{}}
     */
    getItems: function () {
        return _items;
    },

    getDefinitionAsArray: function () {
        return _getFieldsAsArray(_items);
    },

    getDefinitionAsObject: function (objectDefinition) {
        return _getArrayAsObject(objectDefinition);
    },

    getFieldOptions: function () {
        return _getFormFieldOptions(_items);
    },

    setDefinition: function (definition) {
        _setDefinition(definition);
    },

    addChildToField: function (data) {
        _addChildToField(data);
    },

    setFieldProperty: function (uuid, prop, value) {
        _setFieldProperty(uuid, prop, value);
    },

    deleteField: function (uuid) {
        _deleteField(uuid);
    },

    moveField: function (uuid, curIndex, newIndex) {
        _moveField(uuid, curIndex, newIndex);
    },

    addField: function (data) {
        _addField(data);
    },

    /**
     * Change emitter
     */
    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * Add a change listener to the collection
     * @param callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * Remove a change listener from the collection
     * @param callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

module.exports = FormDefinitionStore;
