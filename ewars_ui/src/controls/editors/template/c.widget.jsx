import React from "react";

import ActionGroup from "../../../controls/c.actiongroup.jsx";

const ACTIONS = [
    ["fa-pencil", "EDIT"],
    ["fa-trash", "DELETE"]
];

let LABELS = {
    DATAPOINT: "Data point",
    DEFAULT: "Default chart",
    TABLE: "Table",
    MAP: "Map"
}

class NullWidget extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let style = {
            width: "100%",
            height: "100%",
            background: "#000000"
        }

        return (
            <div style={style}>

            </div>
        )
    }
}

class Widget extends React.Component {

    static defaultProps = {
        vertical: false,
        height: "100%",
        width: "100%",
        resizable: true,
        showContext: false
    };

    constructor(props) {
        super(props);

        this.state = {
            context: false
        }
    }

    componentWillReceiveProps() {
        if (this._viz) {
            if (this._viz.redraw) this._viz.redraw();
        }
    }

    componentDidMount() {
        let renderClass;

        /**
         switch (this.props.data.type) {
            case 'DEFAULT':
                renderClass = viz.DefaultChart;
                break;
            case 'PIE':
                renderClass = viz.PieChart;
                break;
            case 'COLUMN':
                renderClass = viz.ColumnChart;
                break;
            case 'FUNNEL':
                renderClass = viz.FunnelChart;
                break;
            case 'PYRAMID':
                renderClass = viz.PyramidChart;
                break;
            case 'HEATMAP':
                renderClass = viz.HeatMap;
                break;
            case 'DATAPOINT':
                renderClass = viz.Default;
                break;
            case 'MAP':
                renderClass = viz.DefaultMap;
                break;
            case 'TABLE':
                renderClass = viz.TableDefault;
                break;
            case 'HTML':
                renderClass = viz.OtherHTML;
                break;
            default:
                break;
        }
         **/

        if (renderClass) {
            this._viz = new renderClass(
                this.props.data,
                null,
                this._elChart
            )
        }
    }


    _onAction = (action) => {
        if (action == "EDIT") {
            this.props.onEdit(this.props.data);
        }

        if (action == "DELETE") {
            this.props.onDelete(this.props.data);
        }

        if (action == "CLOSE") {
            window.dispatchEvent(new CustomEvent("hidecontrls", {}));
        }
    };

    render() {
        let style = {
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column"
        };

        let label = "Widget";
        if (LABELS[this.props.data.type]) label = LABELS[this.props.data.type];
        if (this.props.data.title) label = this.props.data.title.en || this.props.data.title;
        return (
            <div
                className="layout-widget widget-cell"
                ref={(el) => {
                    this._el = el
                }}
                onContextMenu={this._onContext}
                style={style}>
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px", background: "rgba(0,0,0,0.5)"}}>
                        <div className="column grid-title">{label}</div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._onAction}/>
                    </div>
                    <div className="row">
                        <div
                            className="widget-content"
                            ref={(el) => {
                                this._elChart = el;
                            }}
                            onDoubleClick={this._onEdit}>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Widget;
