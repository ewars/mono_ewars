import React from "react";

import SelectField from "../fields/f.select.jsx";
import NumberField from "../fields/f.numeric.jsx";
import DateField from "../fields/f.date.jsx";
import TextField from "../fields/f.text.jsx";
import TextAreaField from "../fields/f.textarea.jsx";
import Button from "../c.button.jsx";

import Field from "./etl/c.field.jsx";
import SetItem from "./etl/c.setitem.jsx";
import Set from "./etl/c.set.jsx";
import Complex from "./etl/c.complex.jsx";
import FormNode from "./etl/c.formnode.jsx";
import Indicator from "./etl/c.indicator.jsx";

import FormUtils from "../../utils/form_utils";

class SetAdder extends React.Component {}

class SetAdder extends React.Component {
    constructor(props) {
        super(props);
    }

    _onDragStart = (e) => {
        ewars.emit("SHOW_DROPS");
        e.dataTransfer.setData("d", "{set}");
    };

    _dragEnd = () => {
        ewars.emit("HIDE_DROPS");
    };


    render() {
        return (
            <div className="block" draggable={true} onDragStart={this._onDragStart} onDragEnd={this._dragEnd}>
                <div className="block-content status-green">Complex Set</div>
            </div>
        )
    }
}

let ITEM_COUNT = 0;

class ETLEditor extends React.Component {
    _isLoaded = false;
    _itemsLoaded = 0;
    _totalItems = 0;

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentWillMount() {
        this._getInds(this.props);
    }

    _getInds = (props) => {
        let etl = {};
        if (props.form.version.etl) etl = props.form.version.etl;
        if (Object.keys(etl).length > 0) {
            let uuids = Object.keys(etl).filter((item) => {
                return item.indexOf("NEW") < 0;
            });

            if (uuids.length > 0) {
                let query = {
                    uuid: {in: uuids}
                };
                ewars.tx("com.ewars.query", ["indicator", ['uuid', 'name'], query, null, null, null, null, null])
                    .then(resp => {
                        let inds = {};
                        resp.forEach(item => {
                            inds[item.uuid] = item;
                        });
                        this._isLoaded = true;
                        this.setState({
                            indicators: inds
                        })
                    })
            } else {
                this._isLoaded = true;
            }
        } else {
            this._isLoaded = true;
        }
    };

    componentWillUnmount() {
        this._isLoaded = false;
        this.state.indicators = null;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.form.version.uuid != this.props.form.version.uuid) {
            this._getInds(nextProps);
        }
    }

    _add = () => {
        let logic = ewars.copy(this.props.form.version.etl || {});

        ITEM_COUNT++;
        let newUUID = ewars.utils.uuid();
        logic[`NEW_${newUUID}`] = {
            i: null,
            o: -1,
            r: "SUM",
            n: []
        };

        let items =

            ewars.store.dispatch({type: "SET_FORM_LOGIC", data: logic});
    };

    _remove = (index) => {
        let logic = ewars.copy(this.props.form.version.etl || {});
        ewars.store.dispatch({type: "SET_FORM_LOGIC", data: logic});
        this.props.onChange("version.etl", logic);
    };

    _onCircuitChange = (indicator, data) => {
        let logic = ewars.copy(this.props.form.version.etl || {});
        logic[indicator] = data;
        ewars.store.dispatch({type: "SET_FORM_LOGIC", data: logic});
    };

    _onIndChange = (orig, replace) => {
        let logic = ewars.copy(this.props.form.version.etl || {});
        logic[replace] = logic[orig];
        logic[replace].i = replace;
        delete logic[orig];

        ewars.store.dispatch({type: "SET_FORM_LOGIC", data: logic});
    };

    render() {
        let definition = ewars.copy(this.props.form.version.definition);

        let indications = [];
        if (this._isLoaded) {
            let asArray = [];

            for (var p in this.props.form.version.etl) {
                asArray.push({
                    ...this.props.form.version.etl[p],
                    i: p
                })
            }

            asArray.forEach((item) => {
                if (item.o == undefined || item.o == null) {
                    item.o = -1;
                }
            });

            asArray = asArray.sort((a, b) => {
                if (a.o > b.o) return 1;
                if (a.o < b.o) return -1;
                return 0;
            });

            indications = asArray.map((item, index) => {
                return (
                    <Indicator
                        key={`IND_${item.i}`}
                        indicator={item.i}
                        index={index}
                        indicators={this.state.indicators}
                        definition={this.props.form.version.definition}
                        onIndChange={this._onIndChange}
                        etl={this.props.form.version.etl}
                        data={item}
                        onChange={this._onCircuitChange}
                        onRemove={this._remove}/>
                )
            });

        } else {
            indications = <Spinner/>;
        }

        return (
            <div className="ide-layout">
                <div className="ide-row">
                    <div className="ide-col border-right" style={{maxWidth: 300}}>
                        <div className="ide-panel ide-panel-absolute ide-scroll">
                            <div className="block-tree">
                                <FormNode
                                    data={definition}
                                    path={null}
                                    root={true}/>
                                <SetAdder/>
                            </div>
                        </div>
                    </div>
                    <div className="ide-col">
                        <div className="ide-layout">
                            <div className="ide-row" style={{maxHeight: 35}}>
                                <div className="ide-col">
                                    <div className="ide-tbar">
                                        <div className="btn-group pull-right">
                                            <ewars.d.Button
                                                onClick={this._add}
                                                icon="fa-plus"
                                                label="Add Indicator"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ide-row">
                                <div className="ide-col">
                                    <div className="ide-panel ide-panel-absolute ide-scroll">
                                        <div className="logical">
                                            {indications}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ETLEditor;