const STYLES = {
    PERMIT_EDITOR: {
        display: "block",
        width: "30px",
        height: "30px",
        border: "1px solid rgba(0,0,0,0.1)",
        borderRadius: "3px"
    }
}

class PermitEdit extends React.Component {
    render() {
        return (
            <td width="30px">
                <div style={{...STYLES.PERMIT_EDITOR}}>

                </div>
            </td>
        )
    }
}

class PermissionsEditor extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <ewars.d.Panel style={{padding: "16px"}}>
                <table width="100%" className="permit-table">
                    <thead>
                    <tr>
                        <th>User/Group</th>
                        <th>Permissions</th>
                        {this.props.config.map((item) => {
                            return (
                                <th>{item[1]}</th>
                            )
                        })}
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Account Admin</th>
                        <td>
                            <select>
                                <option value="CUSTOM">Custom</option>
                                <option value="VIEWER">Viewer</option>
                                <option value="INTERACTOR">Interactor</option>
                                <option value="NONE">None</option>
                                <option value="DENIED">Denied</option>
                            </select>
                        </td>
                        {this.props.config.map((item) => {
                            return (
                                <PermitEdit/>
                            )
                        })}
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Geo Admin</th>
                        <td>
                            <select>
                                <option value="CUSTOM">Custom</option>
                                <option value="VIEWER">Viewer</option>
                                <option value="INTERACTOR">Interactor</option>
                                <option value="NONE">None</option>
                                <option value="DENIED">Denied</option>
                            </select>
                        </td>
                        {this.props.config.map((item) => {
                            return (
                                <PermitEdit/>
                            )
                        })}
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>Reporting User</th>
                        <td>
                            <select>
                                <option value="CUSTOM">Custom</option>
                                <option value="VIEWER">Viewer</option>
                                <option value="INTERACTOR">Interactor</option>
                                <option value="NONE">None</option>
                                <option value="DENIED">Denied</option>
                            </select>
                        </td>
                        {this.props.config.map((item) => {
                            return (
                                <PermitEdit/>
                            )
                        })}
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>
                            <ewars.d.Button
                                icon="fa-plus"
                                label="Add a user or group"/>
                        </th>
                        <td colSpan={this.props.config.length + 2}></td>
                    </tr>
                    </tfoot>

                </table>

            </ewars.d.Panel>
        )
    }
}

export default PermissionsEditor;