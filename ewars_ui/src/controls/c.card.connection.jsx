import React from 'react';

import ActionGroup from "./c.actiongroup.jsx";

const ACTIONS = [
    ['fa-times', 'CLOSE', 'CLOSE']
];

const STYLES = {
    ICON_OUTER: {
        justifyContent: "center",
        alignItems: "center",
        maxWidth: "60px",
        fontSize: "2rem"
    },
    ICON_ROW: {
        justifyContent: "center",
        alignItems: "center",
        marginBottom: "8px",
        minHeight: "40px"
    },
    ICON_SPAN: {
        fontSize: "12px",
        textAlign: "center",
        display: "block"
    }
};

class ConnectionCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: "DETAILS"
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = "1";
        }, 10);
    }

    _close = () => {
        this._el.removeEventListener("transitionend", this._close);
        window.state.accountsCard = null;
        window.state.emit("CHANGE");
    };

    _showMenu = () => {
        let menu = new Menu();

        menu.append(
            new MenuItem({
                label: "Test",
                click: () => {
                    console.log("TEXT")
                }
            })
        );
        menu.popup({window: remote.getCurrentWindow()});
    };

    _action = (action) => {
        switch (action) {
            case "CLOSE":
                this._el.addEventListener("transitionend", this._close);
                this._el.style.transition = "all 0.1s ease-out";
                this._el.style.transform = "scale(0)";
                this._el.style.opacity = "0";
                break;
            case "MORE":
                this._showMenu();
                break;
            default:
                break;
        }
    };

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        return (
            <div className="user-card-outer">
                <div
                    className="modal-inner"
                    ref={(el) => this._el = el}
                    style={{transform: "scale(0)", opacity: "0"}}>
                    <div className="column" style={{height: "100%"}}>
                        <div className="row bb gen-toolbar">
                            <div className="column grid-title">Connection Status</div>
                            <ActionGroup
                                actions={ACTIONS}
                                onAction={this._action}/>
                        </div>
                        <div className="row padded-8">
                            <div className="column" style={STYLES.ICON_OUTER}>
                                <div className="column" style={STYLES.ICON_ROW}>
                                    <i className="fal fa-laptop"></i>
                                    <span style={STYLES.ICON_SPAN}>Me</span>
                                </div>
                            </div>
                            <div className="column">
                                <div className="row"></div>
                                <div className="row line"></div>
                                <div className="row"></div>
                                <div className="row-line"></div>
                            </div>
                            <div className="column">
                                <div className="row">Internet</div>
                                <div className="row line"></div>
                                <div className="row">UDP</div>
                                <div className="row-line">

                                </div>
                            </div>
                            <div className="column" style={STYLES.ICON_OUTER}>
                                <div className="column" style={STYLES.ICON_ROW}>
                                    <i className="fal fa-cloud"></i>
                                    <span style={STYLES.ICON_SPAN}>EWARS Global</span>
                                </div>

                                <div className="column" style={STYLES.ICON_ROW}>
                                    <i className="fal fa-server"></i>
                                    <span style={STYLES.ICON_SPAN}>Relays</span>
                                </div>
                                <div className="column" style={STYLES.ICON_ROW}>
                                    <i className="fal fa-desktop"></i>
                                    <span style={STYLES.ICON_SPAN}>Local Peers</span>
                                </div>

                                <div className="column" style={STYLES.ICON_ROW}>
                                    <i className="fal fa-mobile"></i>
                                    <span style={STYLES.ICON_SPAN}>GSM/SMS</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ConnectionCard;
