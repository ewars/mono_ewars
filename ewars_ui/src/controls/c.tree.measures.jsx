import React from "react";

import FormUtils from "../utils/form_utils";

const LABELS = {
    COMPLETENESS: "Completeness",
    TIMELINESS: "Timeliness",
    RECORDS: "Number of records",
    RECORDS_EXPECTED: "Records expected",
    RECORDS_TIMELY: "Records on-time",
    RECORDS_LATE: "Records late",
    LOCATION: "Location",
    DATE: "Date"
};

const ICONS = {
    number: "fa-hashtag",
    numeric: "fa-hashtag",
    text: "fa-font",
    textarea: "fa-font",
    select: "fa-list",
    date: "fa-calendar",
    datetime: "fa-calendar",
    time: "fa-clock",
    interval: "fa-clipboard",
    reporting_location: "fa-map-marker"
}

class Node extends React.Component {
    static defaultProps = {
        type: null
    };

    state = {
        data: [],
        selected: null
    }

    _onDragStart = (e) => {
        let code, config = {};
        switch (this.props.type) {
            case "FIELD":
                code = `FIELD.${this.props.data[0]}.${this.props.data[2]}`;
                break;
            case "LOCATION":
                code = this.props.type;
                config.sti = this.props.data.id;
                break;
            default:
                code = this.props.type;
                config.id = null;
                break;
        }

        window.dispatchEvent(new CustomEvent("showdrops", {detail: "M"}));

        e.dataTransfer.setData("n", JSON.stringify({
            t: "M",
            n: code,
            label: LABELS[this.props.type] || "",
            c: config,
            fid: this.props.data.fid || null
        }))
    };

    _onDragEnd = () => {
        window.dispatchEvent(new CustomEvent("hidedrops", {}));
    };

    render() {
        let label, icon;

        switch (this.props.type) {
            case "LOCATION":
                icon = "fal fa-map-marker";
                label = "Location";
                break;
            case "DATE":
                icon = 'fal fa-calendar';
                label = "Date";
                break;
            case "FIELD":
                icon = 'fal fa-list';
                label = this.props.data[4] || null;

                if (this.props.data[3] == 'date') icon = 'fal fa-calendar';
                if (this.props.data[3] == 'text') icon = 'fal fa-font';
                if (this.props.data[3] == 'select') icon = 'fal fa-list-alt';
                if (this.props.data[3] == 'location') icon = 'fal fa-map-marker';
                if (this.props.data[3] == 'number') icon = 'fal fa-hashtag';
                break;
            case "RECORDS_LATE":
            case "RECORDS_TIMELY":
            case "RECORDS_EXPECTED":
            case "RECORDS":
            case "COMPLETENESS":
            case "TIMELINESS":
                icon = 'fal fa-hashtag';
                label = LABELS[this.props.type];
                break;
            default:
                icon = "fal fa-hashtag";
                label = this.props.data.name;
                break;
        }

        return (
            <div className="node">
                <div className="node-handle">
                    <div className="node-control" draggable={true} onDragStart={this._onDragStart}
                         onDragEnd={this._onDragEnd}>
                        <div className="row">
                            <div className="column" style={{maxWidth: "20px", textAlign: "center"}}>
                                <i className={icon}></i>
                            </div>
                            <div className="column">{label}</div>
                            <div className="column block" style={{maxWidth: "20px"}}>
                                <div className="node-ctx">
                                    <i className="fal fa-caret-down"></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}


class FieldNode extends React.Component {
    _onDrag = (e) => {
        let label;
        if (this.props.data.sunk) {
            label = this.props.data.label.map(item => {
                return item.en || item;
            }).join(" - ");
        } else {
            label = this.props.data.label.en || this.props.data.label;
        }
        e.dataTransfer.setData("n", JSON.stringify({
            t: "M",
            n: `FIELD.${this.props.fid}.${this.props.data.name}`,
            label: label,
            fid: this.props.data.fid || null,
            ft: this.props.data.type,
            o: this.props.data.options || undefined
        }))
    };

    render() {
        let label;
        if (this.props.data.sunk) {
            label = this.props.data.label.map(item => {
                return item.en || item;
            }).join(" - ");
        } else {
            label = this.props.data.label.en || this.props.data.label;
        }

        let icon = <i className={"fal " + (ICONS[this.props.data.type] || "fa-list")}></i>

        return (
            <div className="node">
                <div className="node-handle" onDragStart={this._onDrag} draggable={true}>
                    <div className="row">
                        <div className="column" style={{maxWidth: "20px", padding: "6px 5px", textAlign: "center"}}>
                            {icon}
                        </div>
                        <div className="column" style={{padding: "6px 5px"}}>
                            {label}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Form extends React.Component {
    state = {
        show: false
    };

    constructor(props) {
        super(props);
    }

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    render() {
        let label = this.props.data.name.en || this.props.data.name;
        let icon = "fal " + (this.state.show ? "fa-folder-open" : "fa-folder");

        return (
            <div className="node">
                <div className="node-handle" onClick={this._toggle}>
                    <div className="row">
                        <div className="column" style={{maxWidth: "20px", padding: "6px 5px", textAlign: "center"}}>
                            <i className={icon}></i>
                        </div>
                        <div className="column" style={{padding: "6px 5px"}}>
                            {label}
                        </div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="node-children" style={{marginLeft: "16px", marginBottom: "8px", marginTop: "4px"}}>
                        {this.props.data.nodes.map(item => {
                            return (
                                <FieldNode data={item} fid={this.props.data.id}/>
                            )
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class MeasureTree extends React.Component {
    state = {
        data: {}
    };

    static defaultProps = {
        draggable: true,
        onClick: null,
        expanding: false,
        show: false
    };

    constructor(props) {
        super(props)
        this.mounted = false;


        window.sonomaClient.tx("com.sonoma.measures", [], (res) => {
            if (this.mounted) {
                this.setState({
                    data: res
                })
            } else {
                this.state.data = res;
            }
        }, (err) => {
            window.state.error("fa-download", 'Error retrieving measures');
        })
    }

    componentDidMount() {
        this.mounted = true;
    }

    _onClick = (data) => {
        this.props.onClick(data);
    };

    render() {
        let style = {
            padding: "5px",
            maxHeight: "100%"
        };

        if (this.props.expanding && this.props.show) {
            style.flex = 5;
        }

        if (this.props.expanding && !this.props.show) {
            style.maxHeight = "0px";
            style.overflow = "hidden";
            style.height = "0px";
            style.padding = "0px";
        }

        let forms = [];

        for (let i in this.state.data) {
            forms.push(
                <Form data={this.state.data[i]}/>
            )
        }

        return (
            <div className="plot-tree" style={style}>
                {forms}
                <Node type="RECORDS" data={{name: "Number of Records"}}/>
                <Node type="USERS" data={{name: "Number of Users"}}/>
                <Node type="LOCATIONS" data={{name: "Number of Locations"}}/>
                <Node type="LOCATION_TYPES" data={{name: "Number of Location Types"}}/>
                <Node type="ALERTS" data={{name: "Number of Alerts"}}/>
                <Node type="ALARMS" data={{name: "Number of Alarms"}}/>
                <Node type="DEVICES" data={{name: "Number of Devices"}}/>
                <Node type="FORMS" data={{name: "Number of Forms"}}/>
                <Node type="ORGANIZATIONS" data={{name: "Number of Organizations"}}/>
            </div>
        )
    }
}

export default MeasureTree;
