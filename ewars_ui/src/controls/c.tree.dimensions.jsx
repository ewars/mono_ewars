import React from "react";

import FormUtils from "../utils/form_utils";


const TYPE_MAP = {
    LOCATION: {
        icon: "fal fa-map-marker",
        label: "Location"
    },
    LOCATION_TYPE: {
        icon: "fal fa-map-marker",
        label: "Location type"
    },
    LOCATION_GROUP: {
        icon: "fal fa-map-marker",
        label: "Location group"
    },
    LOCATION_STATUS: {
        icon: "fal fa-map-marker",
        label: "Location status"
    },
    LOCATION_GEOM: {
        icon: "fal fa-map-marker",
        label: "Location geometry"
    },
    RECORD_DATE: {
        icon: "fal fa-calendar",
        label: "Record date"
    },
    USER_TYPE: {
        icon: "fal fa-user",
        label: "User role"
    },
    SOURCE: {
        icon: "fal fa-laptop",
        label: "Source type"
    },
    STATUS: {
        icon: "fal fa-toggle-on",
        label: "Status"
    },
    ORGANIZATION: {
        icon: "fal fa-building",
        label: "Organization"
    },
    USER: {
        icon: "fal fa-user",
        label: "User"
    },
    FORM_STATUS: {
        icon: "fal fa-list-all",
        label: "Form status"
    }
};

const ICONS = {
    number: "fa-hashtag",
    numeric: "fa-hashtag",
    text: "fa-font",
    textarea: "fa-font",
    select: "fa-list",
    date: "fa-calendar",
    datetime: "fa-calendar",
    time: "fa-clock",
    interval: "fa-clipboard",
    reporting_location: "fa-map-marker"
}


const NODES = [
    ["FORM", {name: "Form"}],
    ["FORM_STATUS", {name: "FORM_STATUS"}],
    ["LOCATION", {name: "Location"}],
    ["LOCATION_STATUS", {name: "Location Status"}],
    ["LOCATION_TYPE", {name: "Location type"}],
    ["LOCATION_GROUP", {name: "Location group"}],
    ["LOCATION_PCODE", {name: "Location pcode"}],
    ["LOCATION_GEOM", {name: "Loc. Geom. Type"}],
    ["RECORD_DATE", {name: "Record date", type: "DATE"}],
    ["DATE_SUB", {name: "Record Submission date", type: "DATE"}],
    ["USER_TYPE", {name: "User type"}],
    ["USER", {name: "User"}],
    ["ORGANIZATION", {name: "Organization"}],
    ["SOURCE", {name: "Source"}],
    ["ALARM", {name: "Alarm"}],
    ["ALERT_STATE", {name: "Alert state"}],
    ["ALERT_STAGE", {name: "Alert stage"}],
    ["ALERT_DATE", {name: "Alert date", type: "DATE"}]
];

class Node extends React.Component {
    static defaultProps = {
        type: null,
        selected: []
    };

    state = {
        selected: false
    }

    constructor(props) {
        super(props);
    }

    _onDragStart = (e) => {
        let code, config = {};
        code = this.props.type;
        config.type = this.props.data.type || null;

        window.dispatchEvent(new CustomEvent("showdrops", {detail: "D"}));
        e.dataTransfer.setData("n", JSON.stringify({
            t: "D",
            n: code,
            label: TYPE_MAP[this.props.data.type].name || "",
            c: config
        }));
    };

    _onDragEnd = (e) => {
        window.dispatchEvent(new CustomEvent("hidedrops", {}));
    };

    _toggle = (e) => {
        if (e.shiftKey) {
            this.setState({
                selected: !this.state.selected
            })
        }
    };

    render() {
        let label, icon;

        if (Object.keys(TYPE_MAP).indexOf(this.props.type) >= 0) {
            icon = TYPE_MAP[this.props.type].icon;
            label = TYPE_MAP[this.props.type].label;
        } else {
            switch (this.props.type) {
                case "FIELD":
                    icon = "fal fa-list";
                    label = this.props.data[4];

                    if (this.props.data[3] == "date") icon = "fal fa-calendar";
                    if (this.props.data[3] == "text") icon = 'fal fa-font';
                    if (this.props.data[3] == 'select') icon = 'fal fa-list-alt';
                    if (this.props.data[3] == 'location') icon = "fal fa-map-marker";
                    break;
                default:
                    icon = "fal fa-list-alt";
                    if (this.props.data.type == "DATE") icon = "fal fa-calendar";
                    label = this.props.data.name;
                    break;
            }
        }

        let className = "node";
        if (this.state.selected) className += " selected";
        return (
            <div className={className} onClick={this._toggle}>
                <div className="node-handle">
                    <div className="node-control" draggable={true} onDragStart={this._onDragStart}
                         onDragEnd={this._onDragEnd}>
                        <div className="row">
                            <div className="column" style={{maxWidth: "20px", textAlign: "center"}}>
                                <i className={icon}></i>
                            </div>
                            <div className="column">
                                {label}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}


class FieldNode extends React.Component {
    _onDrag = (e) => {
        let label;
        if (this.props.data.sunk) {
            label = this.props.data.label.map(item => {
                return item.en || item;
            }).join(" - ");
        } else {
            label = this.props.data.label.en || this.props.data.label;
        }

        e.dataTransfer.setData("n", JSON.stringify({
            t: "D",
            n: `FIELD.${this.props.fid}.${this.props.data.name}`,
            label: label,
            fid: this.props.data.fid || null,
            ft: this.props.data.type,
            o: this.props.data.options || undefined
        }))
    };

    render() {
        let label;
        if (this.props.data.sunk) {
            label = this.props.data.label.map(item => {
                return item.en || item;
            }).join(" - ");
        } else {
            label = this.props.data.label.en || this.props.data.label;
        }

        let icon = <i className={"fal " + (ICONS[this.props.data.type] || "fa-list")}></i>

        return (
            <div className="node">
                <div className="node-handle" onDragStart={this._onDrag} draggable={true}>
                    <div className="row">
                        <div className="column" style={{maxWidth: "20px", padding: "6px 5px", textAlign: "center"}}>
                            {icon}
                        </div>
                        <div className="column" style={{padding: "6px 5px"}}>
                            {label}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Form extends React.Component {
    state = {
        show: false
    };

    constructor(props) {
        super(props);
    }

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    render() {
        let label = this.props.data.name.en || this.props.data.name;
        let icon = "fal " + (this.state.show ? "fa-folder-open" : "fa-folder");

        return (
            <div className="node">
                <div className="node-handle" onClick={this._toggle}>
                    <div className="row">
                        <div className="column" style={{maxWidth: "20px", padding: "6px 5px", textAlign: "center"}}>
                            <i className={icon}></i>
                        </div>
                        <div className="column" style={{padding: "6px 5px"}}>
                            {label}
                        </div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="node-children" style={{marginLeft: "16px", marginBottom: "8px", marginTop: "4px"}}>
                        {this.props.data.nodes.map(item => {
                            return (
                                <FieldNode data={item} fid={this.props.data.id}/>
                            )
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

class DimensionTree extends React.Component {
    state = {
        data: {}
    };

    static defaultProps = {
        draggable: true,
        onClick: null,
        expanding: false,
        show: false,
        data: {}
    };

    constructor(props) {
        super(props);

        this.mounted = false;
        window.sonomaClient.tx("com.sonoma.dimensions", [], (res) => {
            if (this.mounted) {
                this.setState({data: res});
            } else {
                this.state.data = res
            }
        }, (err) => {
            window.state.error("fa-download", 'Error loading dimensions');
        })

    }

    componentDidMount() {
        this.mounted = true;
    }

    _onClick = (data) => {
        this.props.onClick(data);
    };

    render() {
        let style = {
            maxHeight: "100%",
            padding: "5px",
            transition: "all 0.5s ease"
        };

        if (this.props.expanding && this.props.show) {
            style.flex = 5;
        }

        if (this.props.expanding && !this.props.show) {
            style.maxHeight = "0px";
            style.overflow = "hidden";
            style.height = "0px";
            style.padding = "0px";
        }

        let forms = [];

        for (let i in this.state.data) {
            forms.push(
                <Form data={this.state.data[i]}/>
            )
        }

        return (
            <div className="plot-tree" style={style}>
                {forms}
                {NODES.map(item => {
                    return <Node type={item[0]} data={item[1]}/>
                })}
            </div>
        )
    }
}

export default DimensionTree;
