import React from "react";

class DayMarker extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="discussion-day-marker">

            </div>
        )
    }
}

class Comment extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="discussion-comment">

            </div>
        )
    }
}

class Event extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="discussion-event">

            </div>
        )
    }
}

class Discussion extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="column">
                    <div className="row">

                    </div>
                    <div className="row bt" style={{maxHeight: "30px"}}>

                    </div>
                </div>
                <div className="bl column panel block block-overflow">

                </div>
            </div>
        )
    }
}

export default Discussion;
