const React = require("react");
const $ = React.createElement;

const TAB_TYPES = {
    RECORDS: "fal fa-database",
    RECORD: "fal fa-clipboard",
    ALERTS: "fal fa-bell",
    ALERT: "fal fa-bell",
    DASHBOARD: "fal fa-tachometer",
    DEFAULT: "fal fa-home",
    DOCUMENTS: "fal fa-book",
    PROJECTS: "fal fa-briefcase",
    LOCATIONS: "fal fa-map-marker",
    USERS: "fal fa-users",
    ASSIGNMENTS: "fal fa-clipboard",
    TASKS: "fal fa-check-square",
    PEERS: "fal fa-broadcast-tower"
};

class Tab extends React.Component {
    constructor(props) {
        super(props);
    }

    _set = () => {
        let id = this.props.data.id;
        window.dispatchEvent(new CustomEvent("SET_TAB", {detail: id}));
    };

    _delete = () => {
        let id = this.props.data.id;
        window.dispatchEvent(new CustomEvent("REMOVE_TAB", {detail: id}));
    };

    render() {
        let className = "row br tab" + (this.props.active ? " active-tab" : "");

        let iconClass = TAB_TYPES[this.props.data.type] || "fal fa-cog";

        let showClose = true;
        if (this.props.data.type == "DEFAULT") showClose = false;

        return (
            <div className={className} onClick={this._set}>
                <div className="column" style={{maxWidth: "20px", minWidth: "20px", textAlign: "center"}}>
                    <i className={iconClass}></i>
                </div>
                <div className="column tab-label">
                    {this.props.data.name}
                </div>
                {showClose ?
                    <div className="column closer" style={{maxWidth: "20px", minWidth: "20px", textAlign: "center"}}
                         onClick={this._delete}>
                        <i className="fal fa-times"></i>
                    </div>
                    : null}
            </div>
        )
    }
}

class Tabs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }


    _change = () => {
        this.setState({});
    };

    render() {
        if (this.props.tabs.length <= 1) {
            return null;
        }


        return (
            <div className="row bb gen-toolbar" style={{maxHeight: "25px", minHeight: "25px"}}>
                <div className="row">
                    {this.props.tabs.map(item => {
                        return <Tab data={item} active={this.props.activeTab.id == item.id}/>
                    })}
                </div>
            </div>
        )
    }
}

export default Tabs;
