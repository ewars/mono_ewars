import React from "react";

import Moment from "moment";
import StringUtils from "../utils/string_utils";

const LEGACY_TASK_TYPES = {
    REGISTRATION_REQUEST: "Registration request",
    ACCESS_REQUEST: "Access request",
    ASSIGNMENT_REQUEST: "Assignment request",
    LOCATION_REQUEST: "New location request",
    ORG_REQUEST: "New organization request",
    AMENDMENT_REQUEST: "Amendment request",
    RETRACTION_REQUEST: "Retraction request"
};

const TASK_TYPES = {
    CUSTOM: "Custom",
    ASSIGNMENT: "Assignment request",
    LOCATION: "Location request",
    REPORTING_REQUEST: "Reporting request",
    ACCESS_REQUEST: "Access request"
};

const TASK_DESCRIPTIONS = {
    ASSIGNMENT: "${user_name} has requested an assignment to ${location_name}",
    LOCATION: "${user_name} has requested a new location ${location_name} of type ${location_type}",
    REPORTING_REQUEST: "${user_name} has requested to start reporting ${form_name} for location ${Location_name}",
    ACCESS_REQUEST: "${user_name} (${user_email}) has requested access to this account",
    REGISTRATION_REQUEST: "${user_name} (${user_email}) has submitted a registration request",
    RETRACTION_REQUEST: "${user_name} (${user_email}) has requested deletion of a ${form_name} record",
    AMENDMENT_REQUEST: "${user_name} (${user_email}) has requested an amendment to a ${form_name} record",
    ORGANIZATION: "${user_name} (${user_email}) has requested an new organization ${org_name}",
    ALARM_TASK: "The ${stage_name} for an alarm of type ${alarm_name} is awaiting completion",
    REPORTING_TASK: "The ${stage_name} for an ${form_name} record is awaiting completion",
    HUB_REQUEST: "${account_name}: ${name} (${email}) has requested access to this accounts ${package_name} data",
    SITE_ACCESS_REQUEST: "${email} has requested access to your ${site_name} site",
    RECORD_CONFLICT: "A record conflict for ${form_name} has occurred, please review the details below",
    ALERT_CONFLICT: "An alert update for an ${alarm_name} alert has resulted in a conflict, please review the details below",
};


class LegacyTaskItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = () => {
        window.state.addTab("TASK", {
            uuid: this.props.data.uuid,
            name: "Task"
        })
    };

    render() {
        let task_type = this.props.data.data.task_type;

        let label = LEGACY_TASK_TYPES[task_type] || "Unknown task type";
        let description = "";
        if (TASK_DESCRIPTIONS[task_type]) {
            description = TASK_DESCRIPTIONS[task_type];
        }
        return (
            <div className="task-item">
                <div className="row bb gen-toolbar" style={{borderColor: "#DADADA"}}>
                    <div className="column block" style={{padding: "5px 0"}}><strong>{label}</strong></div>
                    <div className="column block" style={{textAlign: "right", padding: "5px 0"}}>
                        <strong>Created:</strong> {Moment.utc(this.props.data.created).local().format("YYYY-MM-DD")}
                    </div>
                </div>
                <div className="row" style={{paddingTop: "5px"}}>
                    <div className="column block">
                        <p>{description}</p>
                    </div>
                    <div className="reporting-circle"
                         onClick={this._toggle}
                         style={{borderColor: "green"}}>
                        <i className="fal fa-marker"></i>
                    </div>
                </div>
                {this.state.show ?
                    <div className="row task-inner">

                    </div>
                    : null}
            </div>
        )
    }
}

class TaskItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="reporting-item">
                <div className="column">
                    <div className="row">
                        <strong>Routine Surveillance Form</strong> - Alpha Facility
                    </div>
                    <div className="row" style={{maxHeight: "13px"}}>
                        <div className="column" style={{flex: 2}}>2018-W1</div>
                        <div className="column block" style={{textAlign: "right", paddingRight: "8px"}}>Due:
                            2018-12-31
                        </div>

                    </div>
                </div>
                <div className="reporting-circle">

                </div>

            </div>
        )
    }
}

export {
    TaskItem,
    LegacyTaskItem
}
export default TaskItem;

