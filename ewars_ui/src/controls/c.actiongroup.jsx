import React from "react";


class ActionGroup extends React.Component {
    static defaultProps = {
        actions: [],
        onAction: () => {},
        data: null,
        noCalc: false
    };

    constructor(props) {
        super(props);
    }

    _handleOver = (data) => {
        window.dispatchEvent(new CustomEvent("tooltipshown", {detail: data[2]}));
    };

    _handleLeave = () => {
        window.dispatchEvent(new CustomEvent("tooltiphide", {}));
    };

    _click = (e, action) => {
        e.preventDefault();
        e.stopPropagation();
        this.props.onAction(action, this.props.data || null);
    };

    render() {
        let style = {};
        if (!this.props.noCalc) {
            let width = (32 * this.props.actions.length) + 'px';
            style = {
                width,
                maxWidth: width
            };
        }
        let className = "action-group";
        if (this.props.naked) className += " naked";

        let items = [];
        if (this.props.actions.indexOf("-") >= 0) {
            let groups_length = this.props.actions.length;
            let groups = [];
            let cur_group = [];

            this.props.actions.forEach((item, index) => {
                if (item == "-") {
                    groups.push(
                        <div className="sub-group">
                            {cur_group}
                        </div>
                    );
                    cur_group = [];
                } else {
                    let itemClassName = "action-item";
                    if (item[4]) itemClassName += " " + item[4];
                    cur_group.push(
                        <div
                            onMouseOver={() => this._handleOver(item)}
                            onMouseLeave={() => this._handleLeave()}
                            onClick={(e) => this._click(e, item[1])}
                            className={itemClassName}>
                            <i className={"fal " + item[0]}/>
                        </div>
                    );
                }

                if (index == groups_length - 1) {
                    groups.push(
                        <div className="sub-group">
                            {cur_group}
                        </div>
                    );
                }
            });


            items = groups;
        } else {
            items = this.props.actions.map(item => {
                if (item == '-') {
                    return (
                        <div className="action-item">
                            <fa className="fal fa-ellipsis-v"></fa>
                        </div>
                    )
                }

                let itemClassName = "action-item";
                if (item[4]) itemClassName += " " + item[4];
                return (
                    <div
                        onMouseOver={() => this._handleOver(item)}
                        onMouseLeave={() => this._handleLeave()}
                        onClick={(e) => this._click(e, item[1])}
                        className={itemClassName}>
                        <i className={"fal " + item[0]}/>
                    </div>
                )
            })
        }


        return (
            <div
                style={style}
                className={className}>
                {items}

            </div>
        )

    }
}

export default ActionGroup;
