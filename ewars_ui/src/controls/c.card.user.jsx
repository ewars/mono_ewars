import React from "react";

import ActionGroup from "./c.actiongroup.jsx";

const ACTIONS = [
    ['fa-ellipsis-v', 'MORE', 'MORE'],
    ['fa-times', 'CLOSE', 'CLOSE']
];

class UserCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'DETAILS'
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = "1";
        }, 10);
    }

    _close = () => {
        this._el.removeEventListener("transitionend", this._close);
        window.state.userCard = null;
        window.state.emit("CHANGE");
    };

    _action = () => {
        switch (action) {
            case "CLOSE":
                this._el.addEventListener("transitionend", this._close);
                this._el.style.transition = "all 0.1s ease-out";
                this._el.style.transform = "scale(0)";
                this._el.style.opacity = "0";
                break;
            default:
                break;
        }
    };

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    render() {
        let userName = "";
        return (
            <div className="user-card-outer">
                <div ref={(el) => this._el = el}
                     style={{transform: "scale(0)", opacity: "0"}}
                     className="modal-inner">
                    <div className="column" style={{height: "100%"}}>
                        <div className="row bb" style={{padding: "16px"}}>
                            <div className="column" style={{maxWidth: "60px", minWidth: "60px"}}>
                                <div className="circle">
                                    <div className="status-circle-inner"></div>
                                </div>
                            </div>
                            <div className="column">
                                <strong>{userName}</strong>
                                <span>Role</span>
                                <span>Organization</span>
                            </div>
                            <ActionGroup
                                actions={ACTIONS}
                                onAction={this._action}/>

                        </div>
                        <div className="row" style={{maxHeight: "34px", minHeight: "34px"}}>
                            <div className="row-tabs">
                                <div onClick={() => this._view("DETAILS")}
                                     className={"row-tab" + (this.state.view == "DETAILS" ? " active" : "")}>
                                    <i className="fal fa-info-circle"></i>&nbsp;Details
                                </div>
                                <div onClick={() => this._view("ASSIGNS")}
                                     className={"row-tab" + (this.state.view == "ASSIGNS" ? " active" : "")}>
                                    <i className="fal fa-clipboard"></i>&nbsp;Assignments
                                </div>
                                <div onClick={() => this._view("TEAMS")}
                                     className={"row-tab" + (this.state.view == "TEAMS" ? " active" : "")}>
                                    <i className="fal fa-users"></i>&nbsp;Teams
                                </div>
                            </div>
                        </div>
                        <div className="row bt gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                            <div className="column grid-title">UUID: {this.props.data.uuid || ""}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default UserCard;