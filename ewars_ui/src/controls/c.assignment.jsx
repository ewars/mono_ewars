import React from 'react';

class Record extends React.Component {
    render() {
        return (
            <div className="assignment-record">
                <div className="node"></div>
                <div className="popover">
                    <div className="arrow"></div>
                    <div className="text">[record date]</div>
                </div>
            </div>
        )
    }
}

class RecordNonInterval extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="assignment-record">
                Records: [no_records], rate: [record_date]
            </div>
        )
    }
}

class Assignment extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className="assignment">
                <div className="form-name"></div>
                <div className="location-name"></div>
                <div className="records">

                </div>
            </div>
        )
    }
}

export default Assignment;