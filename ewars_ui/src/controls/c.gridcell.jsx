import React from "react";

class GridCell extends React.Component {
    render() {
        let style = {
            gridArea: `${this.props.row} / ${this.props.col} / ${this.props.rowEnd || this.props.row} / ${this.props.colEnd || this.props.col}`,
            justifySelf: "stretch",
            display: "flex"
        };
        return (
            <div style={style}>
                {this.props.children}
            </div>
        )
    }
}

export default GridCell;
