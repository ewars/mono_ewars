import React from "react";
import AppContext from "../context_provider";

const USERS = {
    REGIONAL_ADMIN: "Geographic Administrator",
    ACCOUNT_ADMIN: "Account Administrator",
    USER: "Reporting User"
}

class MenuId extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="row bb id-menu" style={{background: "#FFFFFF"}}>
                <div className="id-user-name">{this.context.user.name}</div>
                <div className="id-user-attr">{this.context.user.email}</div>
                <div className="id-user-attr">{USERS[this.context.user.role]}</div>
            </div>
        )
    }
}

export default MenuId;
