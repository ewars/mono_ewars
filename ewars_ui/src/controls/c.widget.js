const React = require('react');
const PropTypes = require("prop-types");
const { styles } = require("styletron-react");
const $ = React.createElement;

class Widget extends React.Component {
    constructor(props) {
        super(props);
    }

    async componentDidUpdate() {
        const { userColors } = this.context.theme;

        const {
            fontFamily,
            fontSize,
            backgroundColor,
            baseTextColor
        } = this.context.theme.editor;
    }

    render() {
        return $("div", {
            className: "widget"

        }, [
            $("div", {
                className: "widget-header"
            }),
            $("div", {
                className: "widget-body"
            }),
            $("div", {
                className: "widget-footer"
            })
        ])
    }
}

Widget.contextType = {
    theme: PropTypes.object
};

module.exports = Widget;
