import React from 'react';

import ActionGroup from "../c.actiongroup.jsx";

const NEW_ACTIONS = [
    ['fa-save', 'SAVE', 'SAVE'],
    ['fa-times', 'CANCEL', "CANCEL"]
];

const _isArray = (data) => {
    return data.constructor === Array;
}

class GroupItem extends React.Component {
    static defaultProps = {
        selected: []
    };

    constructor(props) {
        super(props);
    }

    _select = () => {
        this.props.onSelect(this.props.data);
    };

    render() {
        let icon = "fa-square";
        if (this.props.selected.indexOf(this.props.data) >= 0) icon = "fa-check-square";

        return (
            <div className="row br bb bl bt"
                 onClick={this._select}
                 style={{maxHeight: "20px", padding: "8px", marginBottom: "4px", marginLeft: "8px", marginRight: "8px", background: "rgba(255,255,255,0.2)"}}>
                <div className="column" style={{maxWidth: "20px", textAlign: "center"}}>
                    <i className={"fal " + icon}></i>
                </div>
                <div className="column" style={{paddingLeft: "5px"}}>
                    {this.props.data}
                </div>
            </div>

        )
    }
}

class GroupsField extends React.Component {
    static defaultProps = {
        value: [],
        name: null
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            add: false
        };

        window.sonomaClient.tx('com.sonoma.location_groups', [], (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("ERROR_GROUPS_LOAD", err);
        })
    }

    _action = (action) => {
        switch (action) {
            case "ADD":
                this.setState({
                    add: true,
                    group: ""
                });
                break;
            case "CANCEL":
                this.setState({
                    add: false,
                    group: ""
                });
                break;
            case "DELETE":
                let items = this.props.value || [];
                if (!_isArray(items)) items = [];
                this.props.onChange(this.props.name, items);
                break;
            case "SAVE":
                let current = this.props.value || [];
                if (!_isArray(current)) current = [];
                let node = this.state.group;
                current.push(node);
                let all = this.state.data;
                all.push(current);
                this.setState({
                    add: false,
                    group: "",
                    data: all
                }, () => {
                    this.props.onChange(this.props.name, current);
                });
                break;
            default:
                break;
        }
    };

    _onChange = (e) => {
        let value = e.target.value;
        value = value.toUpperCase();
        value = value.replace(" ", "_");

        this.setState({
            group: value
        });
    };

    _select = (group) => {
        let groups = this.props.value || [];
        if (!_isArray(groups)) groups = [];
        if (groups.indexOf(group) >= 0) {
            groups.splice(groups.indexOf(group), 1);
            this.props.onChange(this.props.name, groups);
        } else {
            groups.push(group);
            this.props.onChange(this.props.name, groups);
        }
    };

    render() {
        return (
            <div className="column group-selector" style={{background: "rgba(255,255,255,0.2)"}}>
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column"></div>
                    <ActionGroup
                        actions={[["fa-plus", "ADD", "ADD"]]}
                        onAction={this._action}/>
                </div>
                {this.state.add ?
                    <div className="row bb" style={{maxHeight: "34px"}}>
                        <div className="column block">
                            <input
                                value={this.state.group}
                                onChange={this._onChange}
                                type="text"/>
                        </div>
                        <ActionGroup
                            actions={NEW_ACTIONS}
                            onAction={this._action}/>
                    </div>
                    : null}
                <div className="row block block-overflow" style={{minHeight: "150px", maxHeight: "150px"}}>
                    {this.state.data.map(item => {
                        return (
                            <GroupItem
                                data={item}
                                selected={this.props.value || []}
                                onSelect={this._select}/>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default GroupsField;