import React from "react";

class DisplayField extends React.Component {
    render() {
        return (
            <div className="display-field">
                {this.props.defaultValue || this.props.value}
            </div>
        )
    }
}

export default DisplayField;
