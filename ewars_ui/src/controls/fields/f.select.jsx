import React from "react";

import {__, _l} from "../../libs/lang";
import _ from "underscore";
import utils from '../../utils/utils';

class SelectOption extends React.Component {
    _select = () => {
        this.props.onClick(this.props.data);
    };

    render() {
        return (
            <div className="select-option" onClick={this._select}>
                {__(this.props.data[1])}
            </div>
        )
    }
}

class SingleSelect extends React.Component {
    _isLoaded = false;
    _initialLoaded = false;

    static defaultProps = {
        o: [],
        os: null,
        value: null,
        ro: false,
        searchProp: null
    };

    constructor(props) {
        super(props);

        this.state = {
            shown: false,
            options: props.o || [],
            rawOptions: null,
            align: "left",
            search: ''
        };

        this.handleChange = this._onSearchChange.bind(this);
        this.emitChangeDebounced = _.debounce(this.emitChange, 3);
    }

    componentDidMount() {

        let winWidth = window.innerWidth;
        let right = this.refs.selector.getBoundingClientRect().left + this.refs.selector.getBoundingClientRect().width;
        if ((winWidth - right) < 200) {
            this.setState({
                align: "right"
            })
        }
    }

    componentWillUnmount() {
    }

    handleBodyClick = (e) => {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(e.target)) {
                this.setState({
                    shown: false,
                    search: ''
                })
            }
        }
    };

    _handleClick = (e) => {
        e.stopPropagation();
    };

    componentWillMount() {
        this._id = utils.uuid();
        if (this.props.os && this.props.value) {
            this._init(this.props);
        } else {
            this._initialLoaded = true;
            this.state.options = this.props.o;
        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.os && nextProps.value) {
            if (nextProps.value != this.props.value) {
                this._init(nextProps);
            }
        } else {
            this._initialLoaded = true;
            this.state.options = nextProps.o;
        }
    }

    _select = (data) => {
        this.setState({
            shown: false
        });
        this.props.onSelect(data[0]);
    };

    _init = (props) => {

    };

    _toggle = (e) => {
        if (e) e.stopPropagation();

        this.setState({
            shown: !this.state.shown,
            search: ''
        })
    };

    _onSearchChange = (e) => {
        this.emitChangeDebounced(e.target.value);
    };

    emitChange = (value) => {
        this.setState({
            search: value
        })
    };

    render() {
        let dispValue = __("NONE_SELECTED");

        if (this.props.value) {
            this.state.options.forEach((item) => {
                if (item[0] == this.props.value) {
                    dispValue = item[1]
                }
            })
        }

        let style = {};
        if (this.state.align == "right") {
            style.right = 0;
            style.left = "auto";
        } else {
            style.left = 0;
            style.right = "auto";
        }

        let options = this.state.options;
        if (this.state.search != '') {
            let lowered = this.state.search.toLowerCase();
            options = this.state.options.filter(item => {
                return item[1].toLowerCase().indexOf(lowered) >= 0;
            })
        }

        return (
            <div ref="selector" className="ew-select">
                <div className="handle" onClick={this._toggle} style={{maxHeight: "26px", overflow: "hidden"}}>
                    <div className="row">
                        <div className="column block handle-label" style={{overflowY: "hidden", lineHeight: "12px"}}>
                            {dispValue}
                        </div>
                        {!this.props.ro ?
                            <div className="column block grip">
                                <i className="fal fa-caret-down"></i>
                            </div>
                            : null}
                    </div>
                </div>
                {this.state.shown && !this.props.ro ?
                    <div className="ew-select-data" style={style}>
                        <div className="ew-select-inner">
                            {this.props.searchProp ?
                                <div className="row block" style={{maxHeight: "30px"}}>
                                    <input
                                        placeholder="Search..."
                                        onChange={this._onSearchChange}
                                        value={this.state.search}
                                        type="text"/>

                                </div>
                                : null}
                            <div className="row block items">
                                {this.props.options.map((item) => {
                                    return (
                                        <SelectOption
                                            onClick={this._select}
                                            data={item}/>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

class MultiSelect extends React.Component {
    render() {
        return (
            <div></div>
        )
    }
}

export default class SelectField extends React.Component {
    static defaultProps = {
        data: {
            o: [],
            os: null,
            c: [],
            l: "",
            s: false,
            r: false,
            ml: false

        },
        searchProp: null,
        ro: false
    };

    _change = (e) => {
        this.props.onChange(this.props.name, e.target.value);
    };

    _click = () => {
        this._el.click();
    };

    shouldComponentUpdate(nextProps) {
        if (nextProps.value != this.props.value) return true;
        return false;
    }

    render() {
        if (this.props.readOnly) {
            let displayValue = "";
            (this.props.options || []).forEach(item => {
                if (item[0] == this.props.value) displayValue = item[1];
            })

            return (
                <input type="text" disabled value={displayValue}/>
            )
        }

        if (this.props.data.ml || this.props.data.multiple) {
            return (
                <MultiSelect
                    {...this.props}
                    ro={this.props.ro}
                    onAction={this._action}
                    value={this.props.value}
                    o={this.props.data.o}/>
            )
        } else {
            let options = (this.props.options || []).map(item => {
                return (
                    <option value={item[0]}>{item[1]}</option>
                )
            })

            return (
                <div className="select-wrapper">
                    <select
                        ref={(el) => this._el = el}
                        disabled={this.props.readOnly}
                        onChange={this._change}
                        value={this.props.value || ""}
                        name="" id="">
                        <option value="">None Selected</option>
                        {options}
                    </select>
                </div>
            )
        }

    }
}
