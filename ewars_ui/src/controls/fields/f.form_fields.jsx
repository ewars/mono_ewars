import React from "react";

class FormFields extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="form-fields" style={{height: "200px", maxHeight: "200px"}}>
                <div className="row" style={{height: "100%"}}>
                    <div className="column block block-overflow" style={{maxWidth: "50%"}}>

                    </div>
                    <div className="column bl br" style={{maxWidth: "20px"}}>
                        <div className="row">
                            <i className="fal fa-caret-right"></i>
                        </div>
                        <div className="row">
                            <i className="fal fa-caret-left"></i>
                        </div>
                    </div>
                    <div className="column block block-overflow" style={{maxWidth: "50%"}}>

                    </div>
                </div>
            </div>
        )
    }
}

export default FormFields;