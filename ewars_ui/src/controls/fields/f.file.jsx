class File extends React.Component {
    constructor(props) {
        super(props);
    }

    onChange = () => {
    };

    render() {

        let hasFile = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            hasFile = true;
        }

        if (!hasFile) {
            return (
                <div className="placeholder">Unsupported browser.</div>
            )
        }

        return (
            <input type="file" ref="fileData" name="fileToken"/>
        )
    }
}

export default File;