import React from 'react';

import Button from "../c.button.jsx";

class ButtonSet extends React.Component {
    static defaultProps = {
        context_help: null,
        value: null,
        options: []
    };

    _onClick = (data) => {
        if (this.props.ro) return;
        this.props.onChange(this.props.name, data[0]);
    };

    render() {
        let contextHelp;
        if (this.props.context_help) {
            if (this.props.value != null) {
                contextHelp = __(this.props.context_help[this.props.value])
            }
        }
        return (
            <div className="btn-group formed">
                {this.props.options.map((button, index) => {
                    let key = `BUTTON_${index}`;
                    return <Button
                        icon={button[2] || null}
                        label={button[1]}
                        key={key}
                        active={this.props.value == button[0]}
                        onClick={this._onClick}
                        data={button}/>
                })}
                <div className="clearer" style={{display: "block", paddingBottom: "5px"}}></div>
                {contextHelp ?
                    <p>{contextHelp}</p>
                    : null}
            </div>
        )
    }
}

export default ButtonSet;
