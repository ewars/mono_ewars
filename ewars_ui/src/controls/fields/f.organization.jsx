import React from "react";

import SelectField from "./f.select.jsx";


class OrganizationField extends React.Component {
    state = {
        data: []
    };

    constructor(props) {
        super(props);


    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.organizations.all', [], (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            window.state.error("Could not load organizations", err);
        })
    }

    render() {

        let options = [
            ["", 'No Selection'],
            ['NEW', 'New organization']
        ];

        this.state.data.forEach(item => {
            options.push([item.uuid, (item.name.en || item.name)]);
        })

        return <SelectField
                name={this.props.name}
                onChange={this.props.onChange}
                options={options}
                value={this.props.value}/>
    }
}

export default OrganizationField;