import React from "react";

import SelectField from "./f.select.jsx";

class RoleField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: []
        }


        if (window.state.roles) {
            this.state.data = window.state.roles;
        } else {
            window.sonomaClient.tx('com.sonoma.roles.active', [], (res) => {
                this.setState({
                    data: res
                })
            }, (err) => {
                console.log(err);
            })
        }
    }

    _onChange = (name, value) => {
        this.props.onChange(this.props.name, value);
    };

    render() {
        let options = [
            ["ACCOUNT_ADMIN", "Account Administrator"],
            ['REGIONAL_ADMIN', "Regional Administrator"],
            ['USER', "Default User"]
        ];

        if (this.state.data) {
            this.state.data.forEach(item => {
                options.push([item.uuid, item.name]);
            })
        }

        return (
            <SelectField
                value={this.props.value || null}
                options={options}
                name="role"/>

        )
    }
}

export default RoleField;