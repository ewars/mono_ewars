import React from 'react';

const Moment = require("moment");

class Day extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        if (this.props.active) {
            this.props.onClick(this.props.date);
        }
    };

    render() {

        let className = "cal-day";
        if (
            this.props.value.date() == this.props.date.date() &&
                this.props.value.month() == this.props.date.month() &&
                this.props.value.year() == this.props.date.year()
        ) {
            className += " selected";
        }

        if (!this.props.active) className += " inactive";

        if (!this.props.curDate.isSame(this.props.date, "M")) className = "cal-day-nan";

        let day = this.props.date.date();

        return (
            <td onClick={this._onClick} className={className}>{day}</td>
        )
    }
}

class DayPicker extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            curDate: Moment.utc(this.props.value) || Moment.utc()
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            curDate: Moment.utc(nextProps.value) || Moment.utc()
        })
    }

    _prevMonth = () => {
        this.setState({
            curDate: this.state.curDate.clone().subtract(1, 'M')
        })
    };

    _nextMonth = () => {
        this.setState({
            curDate: this.state.curDate.clone().add(1, "M")
        })
    };

    _onDateSelect = (newDate) => {
        this.props.onChange(newDate);
    };

    render() {

        let selected = Moment.utc(this.props.value);
        let value = this.state.curDate || Moment.utc();
        let year = value.year();

        let logical_month = value.month() - 1;

        // get the first day of the month
        let first_day = Moment([value.year(), value.month(), 1]);
        let first_day_weekday = first_day.day() == 0 ? 7 : first_day.day();

        // Find number of dasy in month
        let month_length = this.state.curDate.daysInMonth();
        let previous_month_length = Moment.utc([this.state.curDate.year(), this.state.curDate.month(), 1]).subtract(1, "M").daysInMonth();

        let monthShortName = this.state.curDate.format("MMM");


        let day = 1,
            prev = 1,
            next = 1;

        let rows = [];
        for (var i = 0; i < 9; i++) {
            let cells = [];
            let weekNo = Moment.utc([value.year(), value.month(), day]).isoWeek();
            cells.push(<td className="cal-week-no">{weekNo}</td>);
            for (var j = 1; j <= 7; j++) {
                if (day <= month_length && (i > 0 || j >= first_day_weekday)) {
                    let active = true;
                    if (this.props.block_future) {
                        let targetDate = Moment.utc([value.year(), value.month(), day]);
                        let today = Moment.utc();

                        if (targetDate.isAfter(today, "d")) active = false;
                    }
                    cells.push(
                        <Day
                            date={Moment.utc([value.year(), value.month(), day])}
                            value={selected}
                            active={active}
                            onClick={this._onDateSelect}
                            curDate={this.state.curDate}/>
                    );
                    day++;
                } else {
                    if (day <= month_length) {
                        let dayValue = previous_month_length - first_day_weekday + prev + 1;
                        cells.push(<td className="cal-day-nan">{dayValue}</td>);
                        prev++;
                    } else {
                        cells.push(<td className="cal-day-nan">{next}</td>);
                        next++;
                    }
                }
            }

            // Stop making rows if we've run out of days
            rows.push(<tr>{cells}</tr>);
            if (day > month_length) {
                break;
            }

        }

        return (
            <div className="date-picker">
                <input type="date"/> 
            </div>
        )
    }
}

export default DayPicker;
