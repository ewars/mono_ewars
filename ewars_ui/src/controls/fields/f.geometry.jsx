import React from "react";

import Modal from "../c.modal.jsx";
import ActionGroup from "../c.actiongroup.jsx";

import L from "leaflet";
import LeafletDraw from "leaflet-draw";

const _isValidGeom = (geom) => {
    if (geom == null) return false;
    if (geom == undefined) return false;
    if (geom == "") return false;
    if (geom.features == null) return false;
    return true;
};

const _isValidPoint = (data) => {
    if (data == null) return false;
    if (data == undefined) return false;
    if (data == "") return false;
    if (data.coordinates == null) return false;
    return true;
};

const DATA_FIELDS = [
    "type",
    "source",
    "ind_id",
    "lid",
    "sti",
    "group",
    "formula",
    "marker_type"
];


const EDITOR_ACTIONS = [
    ['fa-save', 'SAVE', 'Save Change(s)'],
    ['fa-times', 'CLOSE', 'Close editor']
];

class Map {
    constructor(data, el) {
        this._mounted = false;
        this._data = data;

        this._el = el;

        this._el.style.width = this._el.parentElement.clientWidth + "px";
        this._el.style.height = this._el.parentElement.clientHeight + "px";

        this._root = null;
    }

    on = (event, callback) => {
        this._onChange = callback;
    };

    off = (event, callback) => {
        this._onChange = null;
    };

    mount = () => {
        this._mounted = true;

        var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
        this._map = L.map(this._el, {})
        this._map.zoomControl.setPosition('bottomright');
        this._map.setView(
            [0, 0],
            12
        );

        var osm = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: 20, attribution: osmAttrib});
        this._map.addLayer(osm);

        var editableLayers = new L.FeatureGroup();
        this._map.addLayer(editableLayers);

        var MyCustomMarker = L.Icon.extend({
            options: {
                shadowUrl: null,
                iconAnchor: new L.Point(12, 12),
                iconSize: new L.Point(24, 24),
                iconUrl: 'link/to/image.png'
            }
        });

        var options = {
            position: 'topright',
            draw: {
                polyline: false,
                polygon: {
                    allowIntersection: true, // Restricts shapes to simple polygons
                    drawError: {
                        color: '#e1e100', // Color the shape will turn when intersects
                        message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
                    },
                    shapeOptions: {
                        color: '#bada55'
                    }
                },
                circle: false, // Turns off this drawing tool
                rectangle: false,
                marker: false
            },
            edit: {
                featureGroup: editableLayers, //REQUIRED!!
                remove: false
            }
        };

        var drawControl = new L.Control.Draw(options);
        this._map.addControl(drawControl);

        this._map.on(L.Draw.Event.CREATED, function (e) {
            var type = e.layerType,
                layer = e.layer;

            if (type === 'marker') {
                layer.bindPopup('A popup!');
            }

            editableLayers.addLayer(layer);
        });

    };

    unmount = () => {

    }
}

class EditingOverlay extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        if (this._el) {
            this._map = new Map(this.props.data || {}, this._el);
            this._map.mount();
            this._map.on("change", this._onChange);
        }
    }

    componentWillUnmount() {
        if (this._map) {
            this._map.off("change", this._onChange);
            this._map.unmount();
            this._map = null;
            this._el.innerHTML = "";
        }
    }

    _action = (action) => {
        switch (action) {
            case "CLOSE":
                this.props.onClose();
                break;
            case "SAVE":
                this.props.onSave();
                break;
            default:
                break;
        }
    };

    render() {
        return (
            <div className="column">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                    <div className="column"></div>
                    <ActionGroup
                        actions={EDITOR_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block">
                    <div
                        ref={(el) => this._el = el}
                        id="map">

                    </div>
                </div>
            </div>
        )
    }
}


class GeometryField extends React.Component {
    state = {
        showEditor: false
    };

    constructor(props) {
        super(props);
    }

    _showEditor = () => {
        this.setState({
            showEditor: true
        })
    };

    _closeModal = () => {
        this.setState({
            showEditor: false
        })
    };

    _saveChanges = (data) => {
        this.setState({
            showEditor: false
        }, () => {
            this.props.onChange(this.props.name, data);
        })
    };

    _onChange = (e) => {
        this.props.onChange(this.props.name, JSON.parse(e.target.value || '{}'));
    };

    render() {
        return (
            <div className="column">
                <div className="row">
                    <div className="column" style={{maxWidth: "25%"}}>Editor</div>
                    <div className="column">
                        <button onClick={this._showEditor}>Edit Geometry</button>
                    </div>
                </div>
                <div className="row">
                    <div className="column" style={{maxWidth: "25%"}}>GeoJSON</div>
                    <div className="column">
                    <textarea
                        name=""
                        id=""
                        onChange={this._onChange}
                        cols="30"
                        value={JSON.stringify(this.props.value || {})}
                        rows="10"></textarea>
                    </div>
                </div>
                {this.state.showEditor ?
                    <Modal actions={[]} onAction={this._action}>
                        <EditingOverlay
                            onSave={this._saveChanges}
                            onClose={this._closeModal}
                            data={this.props.value}/>
                    </Modal>
                    : null}
            </div>
        )
    }
}

export default GeometryField;