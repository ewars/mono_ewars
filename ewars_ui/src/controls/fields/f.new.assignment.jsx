class Handle extends React.Component {
    render() {
        let formName, locationName;
        if (this.props.value.form_id) {
            this.props.assignments.forEach((item) => {
                if (item.form_id == this.props.value.form_id) {
                    formName = ewars.I18N(item.form_name);

                    if (this.props.value.location_id) {
                        item.locations.forEach((loc) => {
                            if (loc.location_id == this.props.value.location_id) {
                                locationName = ewars.I18N(loc.name[0]);
                            }
                        })
                    }
                }
            })
        }

        let name = "No Selection";
        if (formName && locationName) name = `${formName} - ${locationName}`;

        return (
            <div className="handle" onClick={this.props.onClick}>
                <ewars.d.Layout>
                    <ewars.d.Row>
                        <ewars.d.Cell addClass="handleText">{name}</ewars.d.Cell>
                        <ewars.d.Cell width="31px" addClass="handleIcon">
                            <i className="fal fa-caret-down"></i>
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </ewars.d.Layout>
            </div>
        )
    }
}

class SelectChild extends React.Component {
    render() {
        let locationName = ewars.I18N(this.props.data.name[0]);
        return (
            <div className="block" onClick={() => {this.props.onClick(this.props.data)}}>
                <div className="block-content">{locationName}</div>
            </div>
        )
    }
}

class SelectItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    _onClick = (location) => {
        this.props.onSelect([this.props.data, location]);
    };

    render() {
        let formName = ewars.I18N(this.props.data.form_name);

        let className = this.state.show ? "fal fa-caret-down" : "fal fa-caret-right";
        return (
            <div className="block">
                <div className="block-content" onClick={this._toggle}>
                    <ewars.d.Row>
                        <ewars.d.Cell width={20}>
                            <i className={className}></i>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            {formName}
                        </ewars.d.Cell>
                    </ewars.d.Row>
                </div>
                {this.state.show ?
                    <div className="block-children">
                        {this.props.data.locations.map((item) => {
                            return <SelectChild data={item} onClick={this._onClick}/>
                        })}
                    </div>
                    : null}
            </div>
        )
    }
}

export default class AssignmentSelector extends React.Component {
    _initialLoad = false;
    _initialized = false;

    static defaultProps = {
        showForm: true
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            assignment: null,
            show: false
        };

        ewars.tx('com.ewars.user.assignments')
            .then((resp) => {
                this._isLoaded = true;
                this.setState({
                    data: resp
                })
            })

    }

    componentDidMount() {
        window.__hack__.addEventListener('click', this.handleBodyClick);
    }

    componentWillUnmount() {
        window.__hack__.removeEventListener('click', this.handleBodyClick);
    }

    handleBodyClick = (evt) => {
        if (this.refs.selector) {
            const area = this.refs.selector.getDOMNode ? this.refs.selector.getDOMNode() : this.refs.selector;

            if (!area.contains(evt.target)) {
                this.state.showLocations = false;
                this.forceUpdate();
            }
        }
    };

    _toggle = (e) => {
        e.stopPropagation();
        this.setState({
            show: !this.state.show
        })
    };

    handleClick = (e) => {
        e.stopPropagation();
    };

    _init = (props) => {
        ewars.tx("com.ewars.user.assignments")
            .then((resp) => {
                this.setState({
                    data: resp
                })
            })
    };

    _onChange = (data) => {
        this.props.onUpdate(this.props.name, data);
    };

    _onSelect = (data) => {
        let loc = data[1];
        let value = loc.uuid;
        this.state.show = false;
        this.props.onUpdate(this.props.name, value, data);
    };

    render() {
        return (
            <div ref="selector" className="ew-select locale-selector" onClick={this.handleBodyClick}>
                <Handle
                    onClick={this._toggle}
                    readOnly={this.props.readOnly}
                    assignments={this.state.data}
                    value={this.props.value}/>
                {this.state.show ?
                    <div className="ew-select-data">
                        <div className="block-tree" style={{position: "relative"}}>
                            {this.state.data.map((item) => {
                                return (
                                    <SelectItem data={item} onSelect={this._onSelect}/>
                                )
                            })}
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}