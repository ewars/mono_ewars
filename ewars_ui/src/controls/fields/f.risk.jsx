import React from "react";

class CellClick extends React.Component {
    onHover = () => {
        this.props.onHover(this.props.data, this.props.row, this.props.cell - 1);
    };

    onClick = () => {
        this.props.onClick(this.props.data, this.props.row, this.props.cell - 1);
    };

    render() {
        if (this.props.data.length == 1) {
            return (
                <div className="header">{this.props.data[0]}</div>
            )
        }

        var className = "cell " + this.props.data[0];
        if (this.props.active) {
            if (this.props.row == this.props.active[0] && this.props.cell == this.props.active[1] + 1) className += " active";
        }
        return (
            <div onClick={this.onClick} onHover={this.onHover} className={className}>
                <div className="icon"><i className="fal fa-check"></i></div>
            </div>
        )
    }
};


const CONSTANTS = {
    LOW: "LOW",
    MODERATE: "MODERATE",
    HIGH: "HIGH",
    SEVERE: "SEVERE",
    GREEN: "green",
    YELLOW: "yellow",
    RED: "red",
    ORANGE: "orange"
}

var RISK_CONTENT = {};
RISK_CONTENT[CONSTANTS.LOW] = "Managed according to standard response protocols, routine control programmes and regulation (e.g. monitoring through routine surveillance systems) ";
RISK_CONTENT[CONSTANTS.MODERATE] = "Roles and responsibility for the response must be specified. Specific monitoring or control measures required (e.g. enhanced surveillance, additional vaccination campaigns) ";
RISK_CONTENT[CONSTANTS.HIGH] = "Senior management attention needed: there may be a need to establish command and control structures; a range of additional control measures will be required some of which may have significant consequences ";
RISK_CONTENT[CONSTANTS.SEVERE] = "Immediate response required even if the event is reported out of normal working hours. Immediate senior management attention needed (e.g. the command and control structure should be established within hours); the implementation of control measures with serious consequences is highly likely ";

const LIKELIHOOD = [
    "Is expected to occur in most circumstances (e.g. probability of 95% or more) ",
    "Will probably occur in most circumstances (e.g. a probability of between 70% and 94%) ",
    "Will occur some of the time (e.g. a probability of between 30% and 69%) ",
    "Could occur some of the time (e.g. a probability of between 5% and 29%) ",
    "Could occur under exceptional circumstances (e.g. a probability of less than 5%)"
];

const CONSEQUENCES = [
    [
        "Limited impact on the affected population",
        "Little disruption to normal activities and services",
        "Routine responses are adequate and there is no need to implement additional control measures",
        "Few extra costs for authorities and stakeholders "
    ],
    [
        "Minor impact for a small population or at-risk group ",
        "Limited disruption to normal activities and services",
        "A small number of additional control measures will be needed that require minimal resources",
        "Some increase in costs for authorities and stakeholders"
    ],
    [
        "Moderate impact as a large population or at-risk group is affected",
        "Moderate disruption to normal activities and services",
        "Some additional control measures will be needed and some of these require moderate resources to implement",
        "Moderate increase in costs for authorities and stakeholders "
    ],
    [
        "Major impact for a small population or at-risk group ",
        "Major disruption to normal activities and services",
        "A large number of additional control measures will be needed and some of these require significant resources to implement",
        "Significant increase in costs for authorities and stakeholders "
    ],
    [
        "Severe impact for a large population or at-risk group ",
        "Severe disruption to normal activities and services",
        "A large number of additional control measures will be needed and most of these require significant resources to implement",
        "Serious increase in costs for authorities and stakeholders"
    ]
];


const definition = [
    [
        ["Almost Certain"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Highly Likely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Likely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Unlikely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.RED, CONSTANTS.SEVERE]
    ],
    [
        ["Very Unlikely"],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.GREEN, CONSTANTS.LOW],
        [CONSTANTS.YELLOW, CONSTANTS.MODERATE],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH],
        [CONSTANTS.ORANGE, CONSTANTS.HIGH]
    ],
    [
        [""],
        ["Minimal"],
        ["Minor"],
        ["Moderate"],
        ["Major"],
        ["Severe"]
    ]
];

const RISK_NAMES = {
    LOW: [CONSTANTS.GREEN, "Low Risk"],
    MODERATE: [CONSTANTS.YELLOW, "Moderate Risk"],
    HIGH: [CONSTANTS.ORANGE, "High Risk"],
    SEVERE: [CONSTANTS.RED, "Very High Risk"]
};


class RiskField extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (prop, value) => {
        console.log(prop, value);
        // this.state.current.data[prop] = value;
    };


    _onClick = (data, row, cell) => {
        this.props.onChange(this.props.name, {
            riskIndex: [row, cell],
            risk: data[1]
        })
    };

    render() {
        var riskTag,
            actionsContent,
            readOnly = false,
            buttons,
            matrixRows,
            likeContent,
            riskName,
            consContent;

        matrixRows = definition.map((row, index) => {
            let cells = row.map((cell, cellIndex) => {
                return <CellClick onClick={this._onClick}
                                  active={this.props.value.riskIndex || [-1, -1]}
                                  data={cell}
                                  row={index}
                                  cell={cellIndex}/>
            })
            return (
                <div className="risk-row">{cells}</div>
            )
        });

        if (this.props.value.risk) actionsContent = RISK_CONTENT[this.props.value.risk];
        if (this.props.value.riskIndex != null) likeContent = LIKELIHOOD[this.props.value.riskIndex[0]];
        if (this.props.value.riskIndex != null) {
            let list = CONSEQUENCES[this.props.value.riskIndex[1]].map(item => {
                return <li>{item}</li>
            });
            consContent = <ul className="consequences">{list}</ul>

        }

        if (this.props.value.riskIndex) {
            var className = "risk-tag " + RISK_NAMES[this.props.value.riskIndex[0]];
            riskTag = <div className={className}>{RISK_NAMES[this.props.value.riskIndex[1]]}</div>
        }


        return (
            <table width="100%">
                <tbody>
                <tr>
                    <td width="50%">
                        <div className="risk-table">
                            {matrixRows}
                        </div>
                    </td>
                    <td className="risk-description" style={{position: "relative", paddingLeft: "8px"}}>
                        <div className="alert-stage-heading"><strong>Overall level of risk</strong></div>
                        <br />
                        {actionsContent ?
                            <div>
                                {riskTag}
                                <div className="clearer"></div>
                                <label htmlFor="">Likelihood</label>
                                <p>{likeContent}</p>
                                <label htmlFor="">Consequences</label>
                                <p>{consContent}</p>
                                <label htmlFor="">Actions</label>
                                <p>{actionsContent}</p>
                            </div>
                            : null}
                        {!actionsContent ?
                            <div className="placeholder">Please select a risk level from
                                the options at
                                left.</div>
                            : null}
                    </td>
                </tr>
                </tbody>
            </table>
        )
    }
};

export default RiskField;
