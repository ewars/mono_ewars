import React from "react";
var ConditionsRow = require("./conditions/ConditionRow.react");

class ConditionsField extends React.Component {
    state = {
        data: {
            rules: [
                ['none', 'eq', null]
            ]
        }
    };

    _addRule = () => {
        var newConditions = this.state.data.rules ? this.state.data.rules : [];
        newConditions.push([null, "eq", null]);
        this.state.data.rules = newConditions;
        this.props.onUpdate(this.props.name, this.state.data);
    };

    _removeRule = (index) => {
        var rules = ewars.copy(this.state.data);
        rules.rules.splice(index, 1);
        this.props.onUpdate(this.props.name, rules);
    };

    _defaultRow: ["none", "eq", "none"],

    componentWillMount() {
        if (!this.props.value || this.props.value.length >= 0) {
            this.state.data = {
                application: "ALL",
                rules: [
                    ["none", "eq", false]
                ]
            }
        } else {
            if (this.props.config.value != undefined) this.state.data = this.props.config.value;
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state.data = nextProps.value;
    }

    _rowUpdate = (index, data) => {
        this.state.data.rules[index] = data;
        this.props.onUpdate(this.props.name, this.state.data);
    };

    _onApplicationChange = (e) => {
        this.state.data.application = e.target.value;
        this.props.onUpdate(this.props.name, this.state.data);
    };

    render() {
        var rules = [];

        if (this.state.data.rules) {
            rules = _.map(this.state.data.rules, function (rule, index) {
                return <ConditionsRow
                    index={index}
                    data={rule}
                    map={this.props.map}
                    fieldOptions={this.props.fieldOptions}
                    onUpdate={this._rowUpdate}
                    onRemove={this._removeRule}
                    onAdd={this._addCondition}/>
            }, this);
        }

        return (
            <div className="conditions-wrapper">
                <div className="conditions-rule-application">
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td className="passer">Pass When</td>
                            <td>
                                <select value={this.state.data.application} onChange={this._onApplicationChange}>
                                    <option value="all">All rules are met</option>
                                    <option value="any">Any rules are met</option>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    {rules}

                    <div className="btn-group pull-right">
                        <ewars.d.Button
                            label="Add Rule"
                            colour="green"
                            onClick={this._addRule}/>
                    </div>
                </div>
            </div>
        )
    }
}


export default ConditionsField;
