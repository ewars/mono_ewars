import React from "react";

import ActionGroup from "../c.actiongroup.jsx";
import utils from "../../utils/utils";

const ACTIONS = [
    ['fa-plus', 'ADD', 'ADD'],
    '-',
    ['fa-ban', 'CLEAR', 'CLEAR']
];

const _isArray = (data) => {
    return data.constructor === Array;
};


class DictRow extends React.Component {
    constructor(props) {
        super(props);
    }

    _onChange = (e) => {
        let name = e.target.name;

        if (name == "key") this.props.onKeyChange(this.props.data[0], e.target.value);
        if (name == "value") this.props.onValueChange(this.props.data[0], e.target.value);
    };

    _action = (action) => {
        this.props.onDelete(this.props.data[0]);
    };

    render() {
        return (
            <div className="row selected">
                <div className="column">
                    <input type="text" value={this.props.data[1]} name="key" onChange={this._onChange}/>
                </div>
                <div className="column">
                    <input type="text" value={this.props.data[2] || ""} name="value" onChange={this._onChange}/>
                </div>
                <ActionGroup
                    naked={true}
                    actions={[["fa-trash", "DELETE", "DELETE"]]}
                    onAction={this._action}/>

            </div>
        )

    }
}

class DictField extends React.Component {
    state = {
        selected: null,
        key: "",
        value: ""
    };

    constructor(props) {
        super(props);
    }

    _action = (action, item) => {
        switch (action) {
            case "DELETE":
                let data = this.props.value || [];
                if (!_isArray(data)) data = [];
                data.splice(item.index, 1);

                this.props.onChange(this.props.name, data);
                break;
            case "ADD":
                let codes = this.props.value || [];
                if (!_isArray(codes)) codes = [];
                codes.push([
                    utils.uuid(),
                    "NEW_KEY",
                    ""
                ]);
                this.props.onChange(this.props.name, codes);
                break;
            case "CLEAR":
                window.state.dialog({
                    title: "REMOVE_CODES",
                    body: "REMOVE_CODES_BODY",
                    buttons: [
                        "DELETE",
                        "CANCEL"
                    ]
                }, (res) => {
                    if (res == 0) {
                        let newCodes = [];
                        this.porps.onChange(this.props.name, newCodes);
                    }
                });
                break;
            default:
                break;
        }
    };

    _onKeyChange = (uuid, newKey) => {
        let codes = this.props.value || [];
        codes.forEach(item => {
            if (item[0] == uuid) item[1] = newKey;
        });
        this.props.onChange(this.props.name, codes);
    };

    _onValueChange = (uuid, newValue) => {
        let codes = this.props.value || [];
        codes.forEach(item => {
            if (item[0] == uuid) item[2] = newValue;
        });

        this.props.onChange(this.props.name, codes);
    };

    _onDeleteKey = (uuid) => {
        let codes = this.props.value || [];
        let rIndex;
        codes.forEach((item, index) => {
            if (item[0] == uuid) rIndex = index;
        })

        codes.splice(rIndex, 1);
        this.props.onChange(this.props.name, codes);
    };

    render() {
        let items = [];

        let data = this.props.value || [];
        if (!_isArray(data)) data = [];
        data.forEach(item => {
            items.push(
                <DictRow
                    key={item[0]}
                    data={item}
                    onDelete={this._onDeleteKey}
                    onKeyChange={this._onKeyChange}
                    onValueChange={this._onValueChange}/>
            )
        });

        return (
            <div className="dict-field">
                <div className="row bb gen-toolbar">
                    <div className="column" style={{padding: "3px"}}>Key</div>
                    <div className="column" style={{padding: "3px"}}>Value</div>
                </div>
                <div className="column block block-overflow">
                    {items}
                </div>
                <div className="row bt gen-toolbar">
                    <div className="column"></div>
                    <ActionGroup
                        naked={true}
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
            </div>
        )
    }
}

export default DictField;