import React from "react";

class PermissionsRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <th>{this.props.role.name}</th>
                <td>[setting]</td>
                <td>[setting]</td>
            </tr>
        )

    }
}

const BASE_ROLES = [
    {
        name: "Reporting User",
        uuid: "USER",
    },
    {
        name: "Regional Admin",
        uuid: "REGIONAL_ADMIN"
    }
];

class PermissionsField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="permissions-editor">
                <table width="100%">
                    <thead>
                    <tr>
                        <th>Role</th>
                        <th>Permission 1</th>
                        <th>Permission 2</th>
                    </tr>
                    </thead>
                    <tbody>
                    {BASE_ROLES.map(item => {
                        return (
                            <PermissionsRow
                                role={item}
                                permissions={this.props.permissions || []}
                                onChange={this._onRoleChange}/>
                        )
                    })}
                    </tbody>
                </table>
            </div>

        )
    }
}

export default PermissionsField;