import React from "react";

import SelectField from "./f.select.jsx";

class FormSelectField extends React.Component {
    state = {
        forms: []
    };

    componentWillMount() {
        window.sonomaClient.tx('com.sonoma.forms.available', [], (res) => {
            this.setState({
                forms: res
            })
        }, (err) => {
            window.state.error("There was an error loading forms", err);
        })
    }

    _onSelect = (prop, value) => {
        let form = this.state.forms.filter(item => {
            return item.uuid == value;
        })[0] || null;

        this.props.onChange(prop, value, form);
    };

    render() {
        if (this.state.forms.length <= 0) {
            return (
                <div className="field-loading">
                    Loading...
                </div>
            )
        }

        let options = this.state.forms.map(item => {
            return [item.uuid, __(item.name)];
        });

        return (
            <SelectField
                options={options}
                onChange={this._onSelect}
                value={this.props.value}
                name={this.props.name}/>
        )
    }

}

export default FormSelectField;
