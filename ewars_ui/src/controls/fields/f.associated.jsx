import React from "react";

import ActionGroup from "../c.actiongroup.jsx";

const ACTIONS = [
    ['fa-plus', 'ADD', 'Add record'],
    ['fa-link', 'ATTACH',' Attach record']
];

const GRID_ACTIONS = [
    ['fa-chevron-left', 'B', "Prev page"],
    ['fa-chevron-double-left', 'BB', 'Start'],
    ['fa-chevron-double-right', 'FF', 'End'],
    ['fa-chevron-right', 'F', 'Next page'],
    '-',
    ['fa-sync', 'REFRESH', 'Refresh']
];

class DataGrid extends React.Component {
    state = {
        form: {}
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="inline-data-grid">

            </div>
        )
    }
}

class AssociatedRecordsField extends React.Component {
    state = {
        records: [],
        data: {data: {}}
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="field-associated" style={{padding: 0}}>
                <div className="column">
                    <div className="row bb gen-toolbar">
                        <div className="column"></div>
                        <ActionGroup
                            actions={ACTIONS}
                            onAction={this._action}/>
                    </div>
                    <div className="row" style={{minHeight: "40px"}}>

                    </div>
                    <div className="row bt gen-toolbar">
                        <div className="column"></div>
                        <ActionGroup
                            actions={GRID_ACTIONS}
                            onAction={this._action}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default AssociatedRecordsField;