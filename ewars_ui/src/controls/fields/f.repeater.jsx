import React from 'react';

import ActionGroup from "../c.actiongroup.jsx";

import FormUtils from "../../utils/form_utils";

const ACTIONS = [
    ['fa-plus', 'ADD', 'Add row']
];

const ROW_ACTIONS = [
    ['fa-pencil', 'EDIT', 'Edit item'],
    ['fa-trash', 'DELETE', 'Delete item'],
    ['fa-copy', 'COPY', 'Copy item']
];


class RepeaterInline extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="repeater-inline">

            </div>
        )
    }
}

class RowEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <td colspan="3">
                    [row editor]
                </td>
            </tr>
        )
    }
}

class RepeaterRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <th>
                    <ActionGroup
                        naked={true}
                        actions={ROW_ACTIONS}
                        onAction={this._action}/>
                </th>
                {this.props.fields.map(item => {
                    return (
                        <td>
                            {this.props.data[item.name] || ""}
                        </td>
                    )
                })}
            </tr>
        )
    }
}

const TEST_DATA = {
    age: 12,
    name: "Jeff Uren",
    sex: "M"
}

class RepeaterField extends React.Component {

    constructor(props) {
        super(props);
    }

    _onRowSave = (index, data) => {

    };

    render() {
        let arrFields = FormUtils.getFieldsAsArray(this.props.fields || {});
        return (
            <div className="repeater-field">
                <div className="column">
                    <div className="row bb gen-toolbar" style={{maxHeight: "34px"}}>
                        <div className="column"></div>
                        <ActionGroup
                            onAction={this._action}
                            actions={ACTIONS}/>
                    </div>
                    <div className="row block block-overflow" style={{minHeight: "200px", overflowX: "scroll", padding: 0}}>
                        <table width="100%" className="repeater-field-table">
                            <thead>
                            <tr>
                                <th width="30px">&nbsp;</th>
                                {arrFields.map(item => {
                                    let label = (item.label.en || item.label);
                                    if (item.required) label += " *";
                                    return (
                                        <th>{label}</th>
                                   )
                                })}
                            </tr>
                            </thead>
                            <tbody>
                            <RepeaterRow fields={arrFields} data={TEST_DATA} index={0} onChange={this._onRowSave}/>
                            <RepeaterRow fields={arrFields} data={TEST_DATA} index={1} onChange={this._onRowSave}/>
                            <RepeaterRow fields={arrFields} data={TEST_DATA} index={2} onChange={this._onRowSave}/>
                            <RepeaterRow fields={arrFields} data={TEST_DATA} index={3} onChange={this._onRowSave}/>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default RepeaterField;