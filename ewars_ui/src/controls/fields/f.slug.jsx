/*
 * Copyright (c) 2015 WHO.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of WHO and its suppliers, if any. The intellectual concepts contained
 * herein are proprietary to WHO and its suppliers and
 * may be covered under U.S. and Foreign Patents, patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or reproduction
 * of this material is strictly forbidden unless prior written permission is obtained
 * from WHO.
 */

var TextInputField = React.createClass({
    propTypes: {
        _onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        name: React.PropTypes.string,
        readOnly: React.PropTypes.bool
    },

    _options: {

    },

    componentWillMount: function () {
        this.state.value = this.props.value;
    },

    componentDidMount: function () {
        if (this.props.focus) {
            this.refs.inputField.getDOMNode().focus();
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.state.value = nextProps.value;
    },

    getInitialState: function () {
        return {
            value: ""
        };
    },

    _handleKeyDown: function (e) {
        if (this.props.onKeyDown) this.props.onKeyDown(e);
    },

    _onBlur: function (event) {
        if (this.props.config.trigger != "change") {
            if (this.state.value != this.props.value) {
                this.props.onUpdate(this.props.name, this.state.value);
            }
        }
    },

    _onChange: function (e) {
        var value = e.target.value;
        value = value.replace(/\s/g, "_").toLowerCase();
        value = value.replace("-", "_");
        value = value.replace(";", "_");

        if (this.props.config.trigger == "change") {
            this.props.onUpdate(this.props.name, value)
        } else {
            this.setState({
                value: value
            })
        }
    },

    render: function() {

        return (
            <input id="slug_field" ref="inputField" disabled={this.props.readOnly} className="form-control input-sm" onChange={this._onChange} onBlur={this._onBlur} type="text" value={this.state.value} placeholder={this.props.placeholder} name={this.props.name} onKeyDown={this._handleKeyDown}/>
        )

    }

});

module.exports = TextInputField;