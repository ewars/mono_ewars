import React from "react";

const langs = [
    ['en', 'English'],
    ['fr', 'French'],
    ['', '---'],
    ["af", "Afrikaans"],
    ["sq", 'Albanian'],
    ['ar', 'Arabic'],
    ['hy', 'Armenian'],
    ['eu', 'Basque'],
    ['bn', 'Bengal'],
    ['bg', "Bulgarian"],
    ['ca', 'Catalan'],
    ['km', 'Cambodian'],
    ['zh', 'Mandarin'],
    ['hr', 'Croatian'],
    ['cs', 'Czech'],
    ['da', 'Danish'],
    ['nl', 'Dutch'],
    ['en', 'English'],
    ['et', 'Estonian'],
    ['fj', 'Fijian'],
    ['fi', 'Finnish'],
    ['fr', 'French'],
    ['ka', 'Georgian'],
    ['de', 'German'],
    ['el', 'Greek'],
    ['gu', 'Gujarati'],
    ['he', 'Hebrew'],
    ['hi', 'Hindi'],
    ['hu', 'Hungarian'],
    ['is', 'Icelandic'],
    ['id', 'Indonesian'],
    ['ga', 'Irish'],
    ['it', 'Italian'],
    ['ja', 'Japanese'],
    ['jw', 'Javanese'],
    ['ko', 'Korean'],
    ['la', 'Latin'],
    ['lv', 'Latvian'],
    ['lt', 'Lithuanian'],
    ['mk', 'Macedonian'],
    ['ms', 'Malay'],
    ['ml', 'Malayalam'],
    ['mt', 'Maltese'],
    ['mi', 'Maori'],
    ['mr', 'Marathi'],
    ['mn', 'Mongolian'],
    ['ne', 'Nepali'],
    ['no', 'Norwegian'],
    ['fa', 'Persian'],
    ['pl', 'Polish'],
    ['pt', 'Portuguese'],
    ['pa', ' Punjabi'],
    ['qu', 'Quechua'],
    ['ro', 'Romanian'],
    ['ru', 'Russian'],
    ['sm', 'Samoan'],
    ['sr', 'Serbian'],
    ['sk', 'Slovak'],
    ['sl', 'Slovenia'],
    ['es', 'Spanish'],
    ['sw', 'Swahili'],
    ['sv', 'Swedish'],
    ['ta', 'Tamil'],
    ['tt', 'Tatar'],
    ['te', 'Telugu'],
    ['th', 'Thai'],
    ['bo', 'Tibetan'],
    ['to', 'Tonga'],
    ['tr', 'Turkish'],
    ['uk', 'Ukrainian'],
    ['ur', 'Urdu'],
    ['uz', 'Uzbek'],
    ['vi', 'Vietnamese'],
    ['cy', 'Welsh'],
    ['xh', 'Xhosa']
];

class TextField extends React.Component {
    static props = {
        i18n: false
    };

    state = {
        lang: "en"
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.focus) {
            this._el.focus();
        }
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.value != this.props.value) return true;
        return false;
    }

    _handleKeyDown = (e) => {
        if (this.props.onKeyDown) this.props.onKeyDown(e);
    };

    _onChange = (e) => {
        this.props.onChange(this.props.name, e.target.value);
    };

    _onChangei18n = (e) => {
        let data = this.props.value || {};
        data[this.state.lang] = e.target.value;
        this.props.onChange(this.props.name, data);
    };

    render() {

        if (this.props.i18n) {
            // Multilingual field, need to wrap text and pull active language
            let pvalue = "";
            if (this.props.value) {
                if (this.props.value[this.state.lang]) pvalue = this.props.value[this.state.lang] || "";
            }
            return (
                <div className="row">
                    <div className="column">
                        <input
                            ref={(el) => this._el}
                            disabled={this.props.readOnly}
                            className="form-control input"
                            onBlur={this._onBlur}
                            onChange={this._onChangei18n}
                            type="text"
                            value={pvalue}
                            placeholder={this.props.placeholder}
                            name={this.props.name}
                            onKeyDown={this._handleKeyDown}/>
                    </div>
                    <div className="column" style={{maxWidth: "90px"}}>
                        <div className="select-wrapper">
                            <select name="" id="" onChange={(e) => this.setState({lang: e.target.value})}>
                                {langs.map(item => {
                                    return <option value={item[0]}>{item[1]}</option>
                                })}
                            </select>
                            <i className="fal fa-caret-down"></i>
                        </div>
                    </div>
                </div>

            )
        }

        let value = this.props.value;
        if (value) {
            let strValue = String(value);
            if (strValue.indexOf("DUPE_") >= 0) {
                value = strValue.replace("DUPE_", "");
            }
        }

        return (
            <input
                ref={(el) => this._el}
                disabled={this.props.readOnly}
                className="form-control input"
                onBlur={this._onBlur}
                onChange={this._onChange}
                type="text"
                value={value}
                placeholder={this.props.placeholder}
                name={this.props.name}
                onKeyDown={this._handleKeyDown}/>
        )
    }

}

export default TextField;
