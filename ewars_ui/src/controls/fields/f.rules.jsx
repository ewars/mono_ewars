import React from "react";
import SelectField from "./f.select.jsx";
import TextField from "./f.text.jsx";

import ActionGroup from "../../c.actiongroup.jsx";

const ACTIONS = [
    ["fa-plus", "ADD"]
];

const APPLICATION = {
    n: "application",
    o: [
        ["ALL", "All of the following are true"],
        ["ANY", "Any of the following are true"]
    ]
};

const ROW_ACTIONS = [
    ["fa-trash", "DELETE"]
]

class Rule extends React.Component {
    _onChange = (prop, value) => {
        let rule = ewars.copy(this.props.data);
        if (prop == 't_value') rule[2] = value;
        if (prop == 'comparator') rule[1] = value;
        if (prop == "field") rule[0] = value;

        this.props.onChange(this.props.index, rule);
    };

    _onAction = (action) => {
        this.props.onRemove(this.props.index);
    };

    render() {
        let val, cmp;
        if (this.props.data[0] != null) {
            let field;
            console.log(this.props);
            this.props.fields.forEach((item) => {
                if (item[0] == this.props.data[0]) field = item;
            })

            switch (field[2].type) {
                case "text":
                case "number":
                    cmp = (
                        <TextField
                            value={this.props.data[2]}
                            onChange={this._onChange}
                            name="t_value"/>
                    )
                    break;
                case "select":
                    cmp = (
                        <SelectField
                            value={this.props.data[2]}
                            name="t_value"
                            options={field[1].options}
                            onChange={this._onChange}/>
                    )
                    break;
                default:
                    // default case is text
                    break;

            }
        }

        return (
            <div className="block">
                <div className="block-content">
                    <ewars.d.Row>
                        <ewars.d.Cell>
                            <SelectField
                                value={this.props.data[0]}
                                onChange={this._onChange}
                                name="field"
                                options={this.props.fields}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            <SelectField
                                value={this.props.data[1]}
                                onChange={this._onChange}
                                name="comparator"
                                options={[
                                        ["eq", "Equal to"],
                                        ["neq", "Not equal to"],
                                        ["gt", "Greater than"],
                                        ["gte", "Greater than / equal to"],
                                        ["lt", "Less than"],
                                        ["lte", "Less than / equal to"]
                                ]}/>
                        </ewars.d.Cell>
                        <ewars.d.Cell>
                            {cmp}
                        </ewars.d.Cell>
                        <ewars.d.Cell width="30px" style={{display: "block", width: "25px"}}>
                            <ewars.d.ActionGroup
                                right={true}
                                actions={ROW_ACTIONS}
                                height="28px"
                                onAction={this._onAction}/>
                        </ewars.d.Cell>
                    </ewars.d.Row>

                </div>

            </div>
        )
    }
}

const DEFAULT = {
    application: "ALL",
    rules: []
};

export default class RulesEditor extends React.Component {
    static defaultProps = {
        value: {
            application: "ALL",
            rules: []
        }
    };

    constructor(props) {
        super(props);
    }

    _action = (action, args) => {
        if (action == "ADD") {
            let cp = ewars.copy(this.props.value || DEFAULT);
            cp.rules.push([null, "eq", null]);

            this.props.onChange(this.props.data.n, cp);
        }

        if (action == "DELETE") {
            let cp = ewars.copy(this.props.value || DEFAULT);
            cp.rules.splice(args, 1);
            this.props.onChange(this.props.data.n, cp);
        }

        if (action == "MERGE") {

        }
    };

    _onTypeChange = (prop, value) => {
        let v = ewars.copy(this.props.value || DEFAULT);
        v.application = value;

        this.props.onChange(this.props.data.n, v);
    };

    _onRuleChange = (index, value) => {
        let v = ewars.copy(this.props.value || DEFAULT);
        v.rules[index] = value;
        this.props.onChange(this.props.data.n, v);
    };

    _removeRule = (index) => {
        let v = ewars.copy(this.props.value || DEFAULT);
        v.rules.splice(index, 1);
        this.props.onChange(this.props.data.n, v);
    };

    render() {
        let application = "ALL";
        if (this.props.value) {
            application = this.props.value.application || "ALL";
        }

        let rules = [];
        if (this.props.value) {
            if (this.props.value.rules) {
                rules = this.props.value.rules;
            }
        }

        let items = rules.map((item, index) => {
            return (
                <Rule
                    index={index}
                    onChange={this._onRuleChange}
                    onRemove={this._removeRule}
                    fields={this.props.data.fo}
                    data={item}/>
            )
        });

        return (
            <div className="column bt br bb bl">
                <div className="row bb" style={{maxHeight: "35px"}}>
                    <div className="column">
                        <SelectField
                            value={application}
                            onChange={this._onTypeChange}
                            {...APPLICATION}/>
                    </div>
                    <div className="column">
                        <ActionGroup
                            onAction={this._action}
                            actions={ACTIONS}/>
                    </div>
                </div>
                <div className="row block">
                    {items}
                </div>
            </div>
        )
    }
}
