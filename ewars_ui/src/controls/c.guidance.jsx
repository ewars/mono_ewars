import React from "react";

class Guidance extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column">
                <div className="row bb" style={{maxHeight: "34px"}}>
                    <div className="column grid-title">Guidance</div>
                </div>
                <div className="column block block-overflow">
                    [item guidance]
                </div>
            </div>
        )
    }
}

export default Guidance;
