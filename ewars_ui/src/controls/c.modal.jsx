import React from "react";

import ActionGroup from "./c.actiongroup.jsx";

class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    _onAction = (action) => {
        this.props.onAction(action);
    };

    render() {
        return (
            <div className="modal">
                <div className="row">
                    <div className="column modal-closer" style={{maxWidth: "15%"}}>
                        <i className="fal fa-times"></i>
                    </div>
                    <div className="column" style={{background: "#333333"}}>
                        {this.props.actions.length > 0 ?
                            <div className="row bb gen-toolbar">
                                <div className="column grid-title">

                                </div>
                                <ActionGroup
                                    actions={this.props.actions || []}
                                    onAction={this._action}/>
                            </div>
                            : null}
                        <div className="row">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Modal;
