import React from "react";

class Panel extends React.Component {
    static defaultProps = {
        width: "15%",
        onClose: () => {},
        visible: false,
        minimal: false
    };

    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.visible) return null;

        if (this.props.minimal) {
            return (
                <div className="ide-panel">
                    <div className="row">
                        <div className="column" onClick={this.props.onClose}>

                        </div>
                        <div className="row panel-main-minimal">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="ide-panel">
                <div className="row">
                    <div
                        style={{maxWidth: this.props.width, width: this.props.width}}
                        className="column closer"
                        onClick={this.props.onClose}>
                        <div className="closer-icon">
                            <i className="fal fa-times"></i>
                        </div>
                    </div>
                    <div className="row panel-main">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default Panel;
