import FormUtils from "../utils/form_utils";


function flatten(definition) {
    let results = [];

    for (var i in definition) {
        let item = definition[i];
        item.name = i;

        results.push(item);
    }

    return results.sort(function (a, b) {
        if (a.order < b.order) return -1;
        if (a.order > b.order) return 1;
        return 0
    })
}

class Handle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let label;
        if (this.props.field) label = ewars.I18N(this.props.field.label) + ` [${this.props.field.name}]`;

        return (
            <div className="handle" onClick={this.props.onClick}>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>{label}</td>
                        <td width="20px" className="icon">
                            <i className="fal fa-caret-down"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

class FieldItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _onClick = () => {
        if (this.props.data.fields) {
            this.setState({show: !this.state.show})
        } else {
            this.props.onSelect(this.props.name, this.props.data);
        }
    };

    render() {
        let iconClass;
        if (this.props.data.fields) {
            if (this.state.show) {
                iconClass = "fa fa-caret-down";
            } else {
                iconClass = "fa fa-caret-right";
            }
        }

        let children;
        if (this.state.show) {
            children = flatten(this.props.data.fields);
            children = children.map(function (item) {
                if (["display", "header"].indexOf(item.type) < 0) {
                    return <FieldItem
                        data={item}
                        name={`${this.props.name}.${item.name}`}
                        onSelect={this.props.onSelect}/>
                }
            }.bind(this))
        }

        return (
            <div className="block">
                <div className="block-content" onClick={this._onClick}>
                    <div className="ide-row">
                        <div className="ide-col" style={{maxWidth: 22}} ><i className={iconClass}></i></div>
                        <div className="ide-col">{ewars.I18N(this.props.data.label)}</div>
                    </div>
                </div>
                {this.state.show ?
                    <div className="block-children">
                        {children}
                    </div>
                    : null}
            </div>
        )
    }
}

class FieldSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    _toggle = () => {
        this.setState({
            show: !this.state.show
        })
    };

    _onSelect = (field_name, data) => {
        this.setState({show: false});
        this.props.onUpdate(this.props.name, field_name);
    };

    render() {

        let flattened = flatten(this.props.definition, null);

        let rootNodes = flattened.map(function (item) {
            if (["header", "display"].indexOf(item.type) < 0) {
                return <FieldItem
                    data={item}
                    name={`data.${item.name}`}
                    onSelect={this._onSelect}/>
            }
        }.bind(this));

        let field;
        if (this.props.value && this.props.value != "null") {
            field = FormUtils.field(this.props.definition, this.props.value);
        }

        return (
            <div className="depth">
                <div ref="selector" className="ew-select locale-selector">
                    <Handle
                        onClick={this._toggle}
                        field={field}
                        value={field}/>
                    {this.state.show ?
                        <div className="ew-select-data" style={{height: 200}}>
                            <div style={{position: "relative", height: 200}}>
                                <div className="block-tree" style={{padding: 0}}>
                                    {rootNodes}
                                </div>
                            </div>
                        </div>
                        : null}


                </div>
            </div>
        )
    }
}

export default FieldSelector;