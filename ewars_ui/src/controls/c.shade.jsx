import React from "react";

import ActionGroup from "./c.actiongroup.jsx";

import ShadeAssignment from "../views/shades/shade.assignment.jsx";
import ShadeHook from "../views/shades/shade.hook.jsx";
import ShadeAssignments from "../views/shades/shade.assignments.jsx";
import GeometryShade from "../views/shades/shade.geometry.jsx";
import ShadeTeam from "../views/shades/shade.team.jsx";
import ShadeProject from "../views/shades/shade.project.jsx";
import ShadeHub from "../views/shades/shade.hub.jsx";
import ShadeRole from "../views/shades/shade.role.jsx";

const STYLES = {
    SHADER: {
        position: "fixed",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        width: "100%",
        height: "100%",
        zIndex: 99999999,
        display: "flex",
        flexDirection: "row"
    }
};

const CMP = {
    ASSIGNMENT: ShadeAssignment,
    HOOK: ShadeHook,
    ROLE: ShadeRole,
    HUB: ShadeHub,
    ASSIGNMENTS: ShadeAssignments,
    GEOMETRY: GeometryShade,
    TEAMS: ShadeTeam,
    PROJECT: ShadeProject
}

class Shade extends React.Component {
    constructor(props) {
        super(props);
    }

    _action = () => {
        this.props.onAction();
    };

    render() {
        let view;

        if (CMP[this.props.cmp]) {
            let Cmp = CMP[this.props.cmp];
            view = <Cmp data={this.props.data} onAction={this._action}/>;
        }

        return (
            <div className="shade" style={STYLES.SHADER}>
                <div className="column" style={{maxWidth: "15%", background: "rgba(0,0,0,0.5)"}}>

                </div>
                <div className="column shade-inner" style={{background: "#333333"}}>
                    {view}
                </div>
            </div>
        )
    }
}

export default Shade;