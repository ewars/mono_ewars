import React from "react";

import DataTableHeader from "./c.datagrid.header.jsx";
import DataGridSearch from "./c.datagrid.search.jsx";
import Panel from "../c.panel.jsx";
import DatagridColumns from "./c.datagrid.columns.jsx";
import utils from "../../utils/utils";
import SelectField from "../fields/f.select.jsx";
import TextField from "../fields/f.text.jsx";
import NumberField from "../fields/f.numeric.jsx";
import DateField from "../fields/f.date.jsx";

//const {remote} = require("electron");
//const {Menu, MenuItem} = remote;

import ActionGroup from "../c.actiongroup.jsx";

const GRID_ACTIONS = [
    //['fa-check-square', 'MULTISELECT', 'SELECT_MULTIPLE'],
    ['fa-sync', 'REFRESH', 'Refresh'],
    '-',
    ['fa-chevron-left', 'B', "Prev page"],
    ['fa-chevron-double-left', 'BB', 'Start'],
    ['fa-chevron-double-right', 'FF', 'End'],
    ['fa-chevron-right', 'F', 'Next page'],
];

const GRID_ACTIONS_ONE_PAGE = [
    ['fa-sync', 'REFRESH', 'Refresh']
];

const GRID_HEADER_ACTIONS = [
    //["fa-columns", "COLUMNS", "View/Hide Columns"]
];

class DataTableRow extends React.Component {
    shouldComponentUpdate() {
        return false
    }

    _contextAction = (action) => {
        console.log(action);
    };

    _double = () => {
        console.log("DOUBLE");
    };

    _context = () => {
        if (this.props.contextActions.length > 0) {
            let menu = new Menu();

            this.props.contextActions.forEach(item => {
                if (item == '-') {
                    menu.append(new MenuItem({
                        type: "separator"
                    }))
                } else {
                    menu.append(new MenuItem({
                        label: item[2],
                        click: () => {
                            this._contextAction(item[1]);
                        }
                    }))
                }
            })

            menu.popup({window: remote.getCurrentWindow()});
        }
    }

    render() {
        return (
            <tr className="grid-row" onDoubleClick={this._double}
                onContextMenu={this._context}>{this.props.children}</tr>
        )
    }
}

const FILTER_PANEL_ACTIONS = [
    ['fa-plus', 'ADD', 'Add filter'],
    '-',
    ['fa-save', 'SAVE', 'Save change(s)'],
    ['fa-ban', 'CLEAR', 'Clear filters'],
    ['fa-times', ' CLOSE', 'Close panel']
];

const CMP_OPTIONS = [
    ['eq', '='],
    ['neq', '!='],
    ['lt', '<'],
    ['lte', '<='],
    ['gt', '>'],
    ['gte', '>='],
    ['null', 'NULL'],
    ['nnull', 'NOT NULL'],
    ['contains', 'Contains'],
    ['ncontains', '!Contains']
];

const ITEM_ACTIONS = [
    ['fa-trash', 'DELETE', 'Delete rule']
];

import ComparatorField from "../fields/c.comparator.jsx";

class FilterItem extends React.Component {
    render() {
        return (
            <div className="row" style={{minHeight: "30px", maxHeight: "30px", marginBottom: "8px"}}>
                <div className="column block" style={{flexBasis: 0, flexGrow: 0}}>
                    <ComparatorField/>
                </div>
                <div className="column block">
                    <div className="input-wrapper">
                        <input type="text"/>
                    </div>
                </div>
                <ActionGroup
                    actions={[['fa-trash', 'DELETE', 'DELETE_ITEM']]}
                    onAction={this._action}/>
            </div>
        )
    }
}

const FILTER_ACTIONS = [
    ['fa-save', 'SAVE', 'SAVE_FILTERS'],
    ['fa-times', 'CANCEL', 'CANCEL_FILTERS'],
    '-',
    ['fa-trash', 'CLEAR', 'CLEAR_FILTERS']
];

const FILTER_BOTTOM = [
    ['fa-plus', 'ADD', 'ADD_FILTER']
];

class ColumnFilter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filters: []
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this._el.style.transform = "scale(1)";
            this._el.style.opacity = "1";
        }, 200);
    }

    _transEnd = () => {
        this._el.removeEventListener("transitionend", this._transEnd);
        this.props.onClose();
    };

    _close = () => {
        this._el.addEventListener("transitionend", this._transEnd);
        this._el.style.transition = "all 0.1s ease-out";
        this._el.style.transform = "scale(0)";
        this._el.style.opacity = "0";
    };

    _action = (action) => {
        switch (action) {
            case "CANCEL":
                this._close();
                break;
            default:
                break;
        }
    };

    render() {
        let showClear = true;
        return (
            <div className="col-filter-panel">
                <div className="col-filter-panel-inner"
                     ref={(el) => this._el = el}
                     style={{transform: "scale(0)", opacity: "0"}}>
                    <div className="column">
                        <div className="row bb gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                            <div className="column grid-title">Filters</div>
                            <ActionGroup
                                actions={FILTER_ACTIONS}
                                onAction={this._action}/>
                        </div>
                        <div className="row block" style={{padding: "8px"}}>
                            <FilterItem/>
                            <FilterItem/>
                            <FilterItem/>
                        </div>
                        <div className="row bt gen-toolbar" style={{maxHeigh: "34px", minHeight: "34px"}}>
                            <div className="column grid-title"></div>
                            <ActionGroup
                                actions={FILTER_BOTTOM}
                                onAction={this._action}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class DataTableCell extends React.Component {
    render() {
        let val = "";
        if (this.props.root) val = this.props.item[this.props.colname] || "";
        if (!this.props.root) {
            val = this.props.item.data[this.props.colname] || "";
        }

        if (this.props.colfmt) val = this.props.colfmt(val, {
            type: this.props.coltype,
            name: this.props.colname
        }, this.props.item);

        let className = "";
        if (this.props.coltype == 'number') className = "numeric";

        let fullVal;
        if (this.props.coltype == "textarea" || this.props.coltype == "text") {
            if (val.length > 60) {
                fullVal = val;
                val = val.substring(0, 60) + "...";
            }
        }

        if (this.props.coltype == 'select') {
            if (this.props.options) {
                let result = this.props.options.filter(item => {
                    return item[0] == val;
                })[0] || null;

                if (result) {
                    val = result[1];
                }
            }
        }


        return (
            <td
                key={this.props.item.uuid + "_" + this.props.colname}
                rel={fullVal}
                className={className}>
                {val}
            </td>
        )
    }
}

class ViewPort extends React.Component {
    state = {
        ts_ms: -1
    }

    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.ts_ms > this.props.ts_ms) {
            return true;
        }
        return false
    }

    render() {
        return (
            <tbody>
            {this.props.children}
            </tbody>
        )
    }
}

class DataGrid extends React.Component {
    static defaultProps = {
        grid: false,
        columns: [],
        headerActions: [],
        onAction: () => {},
        contextActions: [],
        search: false,
        filters: [],
        orders: [],
        editable: false,
        resource: "",
        map: {},
        hiddenColumns: [],
        panel: null,
        NoDataCmp: null,
        grid_id: null,
        fid: null
    };

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            ts_ms: (new Date).getTime(),
            search: "",
            selected: [],
            filters: [],
            orders: [],
            limit: 60,
            offset: 0,
            hasLoaded: false,
            hiddenColumns: [],
            page: 0,
            count: 0,
            initialized: false,
            query_time: 0,
            showMultiselect: false
        };

        window.addEventListener("reloadgrid", this._handleGlobalReload);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.query_time != this.state.query_time) return true;
        if (nextProps.grid_id != this.props.grid_id) return true;
        if (nextState.panel != this.state.panel) return true;
        if (nextState.showFilter != this.state.showFilter) return true;
        return false;
    }

    componentDidMount() {
        if (!this.state.initialized) {
            this._query({}, {}, this.state.limit, this.state.offset, 0);
        }

        this._scroller.addEventListener("scroll", this._translateHeaders);
    }

    _handleGlobalReload = () => {
        this._query({}, {}, this.state.limit, this.state.offset, 0);
    };

    _query = (filtering, sorting, limit, offset, page) => {
        this._loader.style.display = "block";
        let filters = this.state.filters;
        filters.push.apply(filters, (this.props.filters || []));

        let orders = this.state.orders;
        orders.push.apply(orders, (this.props.orders || []));

        this.props.queryFn(orders, filters, limit, offset)
            .then(res => {
                let records = res.results;
                if (this.props.pinnedRows) {
                    this.props.pinnedRows.forEach(item => {
                        records.unshift(item)
                    })
                }
                this.setState({
                    data: records,
                    count: res.count,
                    offset: offset,
                    page: page,
                    hasLoaded: true,
                    orders: sorting,
                    filters: filters,
                    ts_ms: (new Date).getTime(),
                    initialized: true,
                    query_time: new Date()
                });
                this._loader.style.display = "none";
            })
            .catch(err => {
                this._loader.style.display = "none";
                console.log(err);
            })
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.grid_id != this.props.grid_id) {
            if (window.state.getGridCache(nextProps.grid_id)) {
                this.setState(window.state.getGridCache(nextProps.grid_id));
            } else {
                if (this._loader) this._loader.style.display = "block";
                let filters = {
                    ...nextProps.baseFiltering
                };

                let orders = {
                    ...nextProps.baseOrdering
                };
                nextProps.queryFn(orders, filters, this.state.limit, this.state.offset)
                    .then(res => {
                        let records = res.records;
                        if (this.props.pinnedRows) {
                            this.props.pinnedRows.forEach(item => {
                                records.unshift(item)
                            })
                        }
                        this.setState({
                            data: records,
                            count: res.count,
                            filter: filters,
                            order: orders,
                            limit: 60,
                            offset: 0,
                            page: 0,
                            ts_ms: (new Date).getTime(),
                            hasLoaded: true,
                            query_time: new Date()
                        }, () => {
                            window.state.cacheGrid(nextProps.grid_id, this.state);
                        });
                        if (this._loader) this._loader.style.display = "none";
                    })
                    .catch(err => {
                        if (this._loader) this._loader.style.display = "none";
                        console.log(err);
                    })
            }
        }
    }

    componentWillUnmount() {
        if (this._scroller) this._scroller.removeEventListener("scroll", this._translateHeaders);
        window.removeEventListener("reloadgrid", this._handleGlobalReload);
    }

    _action = (action, data) => {
        switch (action) {
            case "COLUMNS":
                this.setState({
                    panel: "COLUMNS"
                });
                break;
            case "FILTER":
                this.setState({
                    panel: "FILTER"
                });
                break;
            case "REFRESH":
                this._query(this.state.filters, this.state.order, this.state.limit, this.state.offset, this.state.page);
                break;
            case "B":
                if (this.state.page == 0) {
                    return;
                }
                break;
            case "F":
                let newPage = this.state.page + 1;
                this._query(this.state.filters, this.state.order, this.state.limit, newPage * this.state.limit, newPage);
                break;
            case "BB":
                this._query(this.state.filters, this.state.order, this.state.limit, 0, 0);
                break;
            case "FF":
                this._query(this.state.filters, this.state.order, this.state.limit, 0, 0);
                break;
            default:
                this.props.onAction(action, data);
                break;
        }
    };

    _onSearchChange = (e) => {
        this.setState({
            search: e.target.value
        });
    };

    _clearSearch = () => {
        this.setState({
            search: ""
        });
    };

    _closePanel = () => {
        this.setState({
            panel: null
        })
    };

    _saveGrid = (data) => {
        this.setState({
            columns: data,
            panel: null
        })
    };

    _onSort = (sorters) => {
        this._query(this.state.filters, sorters, this.state.limit, 0, 0);
    };

    _filter = (field) => {
        this.setState({
            showFilter: true,
            filterField: field
        })
    };

    _closeFilterPanel = () => {
        this.setState({
            showFilter: false,
            filterField: false
        })
    };

    // Computes and translates the data grid headers position based on transforms when scrolling
    _translateHeaders = (e) => {
        let x = this._scroller.scrollLeft;
        let y = this._scroller.scrollTop;

        let topHeaders = document.querySelectorAll("thead th");

        topHeaders.forEach((topHeader, i) => {
            topHeader.style.transform = `translate(0px, ${y}px)`;
        })

    };

    render() {
        let headerRows = [],
            rows = [];

        if (this.props.grid) {
            this.props.grid.forEach(row => {
                let _row = []

                if (this.props.rowActions && this.props.rowActions.length > 0) {
                    _row.push(
                        <th width="30px">&nbsp;</th>
                    )
                }

                row.forEach(cell => {
                    let id = cell.name || cell.uuid;
                    if (!id) id = utils.uuid();
                    _row.push(
                        <DataTableHeader
                            onFilter={this._filter}
                            key={"HEADER_CELL_" + id}
                            onSort={this._onSort}
                            filters={this.state.filters || {}}
                            sorters={this.state.orders || {}}
                            data={cell}/>
                    )
                });

                headerRows.push(
                    <tr>{_row}</tr>
                )

            })
        }

        var paginationString;
        let startShow, endShow;
        if (this.state.page == 0) {
            startShow = 1;
            endShow = this.state.limit;
        } else {
            startShow = ((this.state.limit) * this.state.page) + 1;
            endShow = startShow = 9;
        }

        if (this.state.count == this.state.data.length) {
            endShow = this.state.count;
        }

        paginationString = `Showing ${startShow} to ${endShow} of ${this.state.count}`;
        if (this.state.count <= 0) paginationString = "No records found";

        let search;
        if (this.props.search) {
            search = (
                <DataGridSearch
                    onChange={this._onSearchChange}
                    term={this.state.search}/>
            )
        }

        this.state.data.forEach(item => {
            let cells = [];
            if (this.props.rowActions && this.props.rowActions.length > 0) {
                cells.push(
                    <td className="action-cell">
                        <ActionGroup
                            actions={this.props.rowActions}
                            data={item}
                            onAction={this._action}/>
                    </td>
                )
            }

            let hasMatrices = this.props.grid.length > 1;
            let index = hasMatrices ? 2 : 0;
            this.props.grid[index].forEach(col => {
                cells.push(
                    <DataTableCell
                        item={item}
                        key={item.uuid + "_" + col.name}
                        root={col.root}
                        options={col.options || null}
                        colname={col.name}
                        coltype={col.type}
                        colfmt={col.fmt}/>
                )
            });
            rows.push(
                <DataTableRow
                    contextActions={this.props.contextActions || []}
                    key={item.uuid}>
                    {cells}
                </DataTableRow>
            )
        });

        let panel;

        if (this.state.panel) {
            if (this.state.panel === "COLUMNS") panel = (
                <DatagridColumns
                    columns={this.state.hiddenColumns}
                    grid={this.props.grid}
                    onSave={this._saveGrid}
                    onClose={this._closePanel}/>
            )

            if (this.state.panel === "FILTER") {
                panel = (
                    <Filtering
                        filters={this.state.filters}
                        onSave={this._saveFilterChanges}
                        columns={this.props.grid}/>
                )
            }
        }

        let header_actions = utils.copy(GRID_HEADER_ACTIONS);
        if (this.props.actions.length > 0) {
            header_actions = utils.copy(this.props.actions);
            header_actions.push(["-"]);
            GRID_HEADER_ACTIONS.forEach(item => {
                header_actions.push(item);
            });
        }

        if (this.state.hasLoaded && this.state.data.length <= 0 && this.props.NoDataCmp) {
            if (this.props.NoDataCmp) {
                let Cmp = this.props.NoDataCmp;
                rows = <Cmp onAction={this._action}/>
            }

        }

        let grid_acts = JSON.parse(JSON.stringify(GRID_ACTIONS));
        if (this.state.count < this.state.limit) {
            grid_acts = GRID_ACTIONS_ONE_PAGE;
        }

        if (this.state.count <= 0 && this.state.hasLoaded && this.props.NoDataCmp) {
            return (
                <div className="dt-wrapper">
                    <div className="row br bb dt-toolbar" style={{maxHeight: "35px", height: "35px"}}>
                        <div className="column grid-title"
                             style={{fontWeight: 'bold', flex: 2}}>{this.props.title || ""}</div>
                        {search}
                        <ActionGroup
                            right={true}
                            actions={header_actions}
                            onAction={(action) => this._action(action)}/>

                    </div>
                    <div className="row">
                        {rows}
                    </div>
                </div>
            )
        }

        return (
            <div className="dt-wrapper" ref={(el) => this._wrapper = el} style={{position: "relative"}}>
                <div
                    style={{display: "none"}}
                    ref={(el) => this._loader = el}
                    className="loader">
                    <div className="loader-middle">
                        <i className="fal fa-cog fa-spin"></i>
                    </div>
                </div>
                <div className="row br bb dt-toolbar" style={{maxHeight: "35px", height: "35px"}}>
                    <div className="column grid-title"
                         style={{flex: 2}}>{this.props.title || ""}</div>
                    {search}
                    <ActionGroup
                        right={true}
                        actions={header_actions}
                        onAction={(action) => this._action(action)}/>

                </div>
                <div className="row block dt-scroll bgd-ps-neutral" ref={(el) => this._scroller = el}>
                    <div className="dt" ref={(el) => {
                        this._dt = el
                    }}>
                        <table className="dtCore">
                            <thead>
                            {headerRows}
                            </thead>
                            <tbody>
                            {rows}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="row bt dt-footer" style={{maxHeight: "30px", height: "30px"}}>
                    <div className="column grid-title">
                        {paginationString}
                    </div>

                    <div className="column">

                    </div>

                    <ActionGroup
                        actions={grid_acts}
                        onAction={this._action}/>
                </div>
                {panel ?
                    <Panel
                        visible={true}
                        minimal={true}
                        onClose={this._closePanel}>
                        {panel}
                    </Panel>
                    : null}

                {this.state.showFilter ?
                    <ColumnFilter onClose={this._closeFilterPanel}/>
                    : null}
            </div>
        )
    }
}

export default DataGrid;
