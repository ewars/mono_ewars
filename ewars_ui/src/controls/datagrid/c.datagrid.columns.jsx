import React from "react";

import ActionGroup from "../c.actiongroup.jsx";

const ACTIONS = [
    ["fa-check-circle", "SELECT_ALL", "Select all"],
    ["fa-circle", "SELECT_NONE", "Select none"],
    ['fa-check', 'APPLY', "Apply change(s)"],
    ["fa-times", "CLOSE", "Close"]
];

class ColumnItem extends React.Component {
    static defaultProps = {
        label: "",
        name: "",
        hiddens: []
    };

    constructor(props) {
        super(props);
    }

    _onChange = () => {
        this.props.onChange(this.props.name);
    };

    render() {
        let icon = <i className="fal fa-check-circle"></i>;

        if (this.props.hiddens.indexOf(this.props.name) >= 0) {
            icon = <i className="fal fa-circle"></i>;
        }
        return (
            <div className="grid-column-toggle" onClick={this._onChange}>
                <div className="row">
                    <div className="column block" style={{maxWidth: "20px"}}>
                        {icon}
                    </div>
                    <div className="column">
                        {this.props.label}
                    </div>
                </div>

            </div>
        )
    }
}

class DatagridColumns extends React.Component {
    static defaultProps = {
        grid: [],
        items: []
    };

    state = {
        columns: []
    };

    constructor(props) {
        super(props);

        this.state = {
            columns: this.props.columns || []
        }
    }

    _action = (action) => {
        switch(action) {
            case 'CLOSE':
                this.props.onClose();
                break;
            case 'APPLY':
                this.props.onSave(this.state.columns);
                break;
            case "SELECT_ALL":
                this.setState({
                    columns: []
                });
                break;
            case "SELECT_NONE":
                this.setState({
                    columns: this.props.grid[2].map(item => {
                        return item.name;
                    })
                });
                break;
            default:
                break;
        }
    };

    _onColumnChange = (col) => {
        let columns = this.state.columns;
        let place = this.state.columns.indexOf(col);
        if (place >= 0) {
            columns.splice(place, 1);
        } else {
            columns.push(col);
        }

        this.setState({
            columns: columns
        })
    };

    render() {

        let layerOne = {},
            layerTwo = {},
            layerThree = {};

        let tree = [];

        if (this.props.grid.length > 0) {
            // handle first layer
            this.props.grid[0].forEach(item => {
                if (item != "FILL") {
                    layerOne[item.name] = item;
                }
            })

            this.props.grid[1].forEach(item => {
                if (item != "FILL") layerTwo[item.name.split(".")[1]] = item;
            })

            this.props.grid[2].forEach(item => {
                if (item.name.indexOf(".") >= 0) {
                    // This column has ancestors
                    let splat = item.name.split(".");
                    let layerOneLabel = layerOne[splat[0]].label;
                    let layerTwoLabel = layerTwo[splat[1]].label;
                    let label = `${layerOneLabel.en || layerOneLabel} / ${layerTwoLabel.en || layerTwoLabel} / ${item.label.en || item.label}`;
                    tree.push(
                        <ColumnItem
                            onChange={this._onColumnChange}
                            hiddens={this.state.columns}
                            name={item.name}
                            label={label}/>
                    )
                } else {
                    tree.push(
                        <ColumnItem
                            onChange={this._onColumnChange}
                            hiddens={this.state.columns}
                            name={item.name}
                            label={item.label.en || item.label}/>
                    )
                }
            })
        }

        return (
            <div className="column columns-editor">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px", height: "34px"}}>
                    <div className="column grid-title">
                        <div className="grid-title"><i className="fal fa-columns"></i>&nbsp;Columns</div>
                    </div>
                    <ActionGroup
                        actions={ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow" style={{padding: "8px"}}>
                    {tree}
                </div>
            </div>
        )
    }
}

export default DatagridColumns;
