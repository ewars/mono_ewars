import React from "react";

class DataTableHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showControl: false,
            controls: false,
            showFilters: false
        };
    }

    static defaultProps = {
        sortable: false,
        filterable: false
    };

    _sort = (e) => {
        e.preventDefault();
        let colName = this.props.data.name;
        if (!this.props.data.root) colName = "data." + colName;

        let sorters = this.props.sorters || {};
        if (!sorters[colName]) {
            sorters[colName] = [this.props.data.type, "DESC"];
        } else if (sorters[colName][1] == "DESC") {
            sorters[colName] = [this.props.data.type, "ASC"];
        } else if (sorters[colName][1] == "ASC") {
            delete sorters[colName];
        }

        this.props.onSort(sorters);
    };

    render() {
        if (this.props.data == 'FILL') {
            return (
                <th className="dt-fill">&nbsp;</th>
            )
        }

        if (this.props.data.type == "colspan") {
            return (
                <th className="dt-header-cell bare"
                    colspan={this.props.data.span}>{this.props.data.label}</th>
            )
        }

        let fieldType = this.props.data.type;

        let hasControls = true;
        if (["display", "header"].indexOf(this.props.data.type) >= 0) hasControls = false;

        let width = (this.props.data.width || 90) + "px";

        let sortIcon;

        let colName;
        if (this.props.data) {
            if (this.props.data.name) colName = this.props.data.name
        }
        if (this.props.data.name) colName = this.props.data.name;
        if (!this.props.data.root) colName = "data." + colName;

        sortIcon = <i className="fal fa-sort"></i>;
        if (this.props.sorters[colName]) {
            if (this.props.sorters[colName][1] == "DESC") sortIcon = <i className="fal fa-caret-down"></i>;
            if (this.props.sorters[colName][1] == "ASC") sortIcon = <i className="fal fa-caret-up"></i>;
        }

        let actionControlClass = "ide-col dt-filter-btn";
        if (this.props.filters[this.props.data.filterKey || this.props.data.name]) actionControlClass += " active";

        let filterIconClass = "fal fa-filter";
        if (this.props.filters[this.props.data.filterKey || this.props.data.name]) filterIconClass += " green";

        let hasSort = true;
        let hasFiltering = true;
        let style = {position: "relative"};
        if (['row', 'matrix', 'display'].indexOf(this.props.data.type) >= 0) {
            hasSort = false;
            hasFiltering = false;
            style.background = "rgba(0,0,0,0.1)"
        }


        let rowSpan = 1;
        if (this.props.data.rowspan) rowSpan = parseInt(this.props.data.rowspan);

        let label = this.props.data.label.en || this.props.data.label;
        if (label.length > 30) label = label.substr(0, 30) + "...";

        let filterIcon = "fal fa-filter";

        return (
            <th rowSpan={rowSpan}
                rowspan={rowSpan}
                colSpan={1}
                className="dt-header-cell"
                style={style}>
                <div className="row" style={{height: "100%", minHeight: "25px"}}>
                    <div className="column block" style={{padding: "5px 10px"}}>{label}</div>
                    {hasSort ?
                        <div className="column block sort-handle" style={{maxWidth: "20px", padding: "5px"}}
                             onClick={this._sort}>
                            {sortIcon}
                        </div>
                        : null}
                    {hasFiltering ?
                        <div className="column block filter-handle"
                             onClick={() => this.props.onFilter(this.props.data)}
                             style={{maxWidth: "20px", padding: "5px"}}>
                            <i className="fal fa-filter"></i>
                        </div>
                        : null}
                </div>
            </th>
        )
    }

}

export default DataTableHeader;
