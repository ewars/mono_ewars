import React from "react";

class DataGridSearch extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="column" style={{flex: 1}}>
                <div className="dg-search">
                    <div className="column search-icon" style={{maxWidth: "20px"}}>
                        <i className="fal fa-search"></i>
                    </div>
                    <div className="column">
                        <input type="text"
                               onChange={(e) => {
                                   this.props.onChange(e.target.value)
                               }}
                               value={this.props.term || ""}/>
                    </div>
                    <div className="column" style={{maxWidth: "20px"}}>
                        <i className="fal fa-close"></i>
                    </div>
                </div>
            </div>
        )
    }
}

export default DataGridSearch;
