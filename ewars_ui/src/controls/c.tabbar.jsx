import React from "react";


const TAB_NAMES = {
    ADMIN_DASHBOARD: "Admin: Dashboard",
    "DEFAULT": null,
    ALARM: "Alarm",
    LOCATIONS: "Locations",
    INDICATORS: "Indicators",
    FORM: "Form",
    RECORD: "Record",
    ALERT: "Alert",
    CONNECTIONS: "Connections",
    PEERS: "Peers",
    TRANSLATIONS: "Translations",
    NOTEBOOKS: "Notebooks",
    PIVOTS: "Pivots",
    PLOTS: "Plots",
    MAPS: "Maps",
    HUDS: "HUDs",
    ME_AUDITOR: "M&E Auditor",
    ALERT_AUDITOR: "Alert Auditor",
    EXPORT: "Export",
    ROLES: "Roles",
    ROLE: "Role",
    DOCUMENT: "Document",
    HUD: "HUD",
    LOG: "Event Log",
    OUTPUTS: "Outputs",
    ADMIN_DOCUMENT: "Admin: Document"
};


class Tab extends React.Component {
    constructor(props) {
        super(props);

    }

    _set = () => {
        window.state.setTab(this.props.data);
    };

    _removeTab = () => {
        window.state.removeTab(this.props.data[1]._id);
    };

    render() {
        return (
            <div
                className={"bar-item" + (this.props.activeTab == this.props.data[0] ? " active" : "")}>
                <div className="row">
                    <div className="column name" style={{paddingRight: "10px", whiteSpace: "nowrap"}} onClick={this._set}>
                        {TAB_NAMES[this.props.data[0]]}
                    </div>
                    <div className="column block bl icon" style={{maxWidth: "16px", textAlign: "center"}}
                         onClick={this._removeTab}>
                        <i className="fal fa-times"></i>
                    </div>
                </div>
            </div>
        )
    }
}

class TabBar extends React.Component {
    static defaultProps = {
        tabs: [],
        activeTab: "DEFAULT"
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="tabbar">
                <div
                    onClick={() => state.setTab(["DEFAULT", {}])}
                    className="bar-item active main"
                    style={{padding: "10px 5px", background: "lightgreen", color: "#333333"}}>
                    <i className="fal fa-home"></i>&nbsp;{window.state.user.name}
                </div>


                <div className="tabbar-items" style={{flex: 3}}>
                    {this.context.tabs.map(item => {
                        return (
                            <Tab data={item}
                                 activeTab={this.props.activeTab}/>
                        )
                    })}
                </div>

                <div
                    onClick={() => state.toggleAccounts()}
                    className="bar-item accounts"
                    style={{padding: "10px 5px", background: "lightblue", color: "#333333"}}>
                    <i className="fal fa-building"></i>
                </div>
            </div>
        )
    }
}

export default TabBar;
