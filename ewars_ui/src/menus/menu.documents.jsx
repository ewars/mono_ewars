import React from "react";

class DocumentItem extends React.Component {
    _show = () => {
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                type: "DOCUMENTS",
                uuid: this.props.data.uuid,
                name: __(this.props.data.template_name)
            }
        }));
    };

    render() {
        return (
            <div className="dash-handle" onClick={this._show}>
                <div className="inner">
                    {__(this.props.data.template_name)}
                </div>
            </div>
        )
    }
}

class MenuDocuments extends React.Component {
    state = {
        documents: []
    }

    constructor(props) {
        super(props);

        this.state = {documents: []}

    }

    componentDidMount() {
        window.sonomaClient.tx("com.sonoma.self.documents", [], (res) => {
            this.setState({
                documents: res
            })
        }, (err) => {
            console.log(err);
        });
    }

    componentWillMount() {
        document.body.addEventListener("click", this._handleClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._handleClick);
    }

    _handleClick = (e) => {
        if (this._el) {
            if (!this._el.contains(e.target)) {
                window.state.menu = null;
                window.state.emit("CHANGE");
            }
        }
    };

    _view = (view) => {
        switch (view) {
            default:
                window.state.addTab("DOCUMENT", {});
                break;
        }
    };

    render() {
        return (
            <div
                ref={(el) => this._el = el}
                className="column block block-overflow">
                {this.state.documents.map(item => {
                    return <DocumentItem data={item}/>
                })}
            </div>
        )
    }
}

export default MenuDocuments;
