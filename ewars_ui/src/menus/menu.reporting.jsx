import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";

const ACTIONS = [
    ['fa-plus', 'CREATE', 'New record']
];

class FormItem extends React.Component {
    constructor(props) {
        super(props);
    }

    _onClick = () => {
        window.state.getState("CORE").setState({
            view: 'REPORTING',
            viewConfig: {
                form_id: this.props.data.uuid
            }
        })
    };

    _action = (action) => {
        switch (action) {
            case "CREATE":
                window.state.addTab("RECORD", {
                    form_id: this.props.data.uuid,
                    name: this.props.data.name
                });
                break;
            case "BROWSE":
                window.state.addTab("RECORDS", {
                    form_id: this.props.data.uuid,
                    name: this.props.data.name
                });
                break;
            default:
                break;
        }
    };

    render() {
        let actions = ACTIONS;
        if (this.props.archived) actions = [];
        return (
            <div className="dash-handle with-actions" onClick={() => this._action("BROWSE")}>
                <div className="row">
                    <div className="column"
                         style={{
                             paddingTop: "5px",
                             fontSize: "11px",
                             paddingRight: "5px"
                         }}>{this.props.data.name.en || this.props.data.name}</div>
                    <ActionGroup
                        naked={true}
                        onAction={this._action}
                        actions={actions}/>
                </div>
            </div>
        )
    }
}

class Locations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: null
        }
    }

    componentDidMount() {
        window.sonomaClient.tx("com.sonoma.self.locations", null, (res) => {
            this.setState({
                locations: res
            })
        }, err => {
            console.log(err);
        })
    }

    _showLocation = (item) => {
        window.state.addTab("LOCATION_REPORTING", {
            name: item.name.en || item.name,
            uuid: item.uuid
        })
    };

    render() {
        if (!this.state.locations) {
            return (
                <div>
                    <p>Loading...</p>
                </div>
            )
        }

        let items = this.state.locations.sort((a, b) => {
            return (a.name.en || a.name).localeCompare((b.name.en || b.name));
        });

        let loc_types = {};

        items.forEach(loc => {
            if (loc_types[loc.lti]) {
                loc_types[loc.lti].push(loc);
            } else {
                loc_types[loc.lti] = [loc];
            }
        });

        let nodes = [];

        for (let i in loc_types) {
            let lti_name = loc_types[i][0].lti_name;

            nodes.push(
                <div className="dash-section">{lti_name.en || lti_name}</div>
            )

            loc_types[i].forEach(loc => {
                nodes.push(
                    <div onClick={() => this._showLocation(loc)} className="dash-handle">
                        {loc.name.en || loc.name}
                    </div>

                )
            })
        }


        return (
            <div>
                <button onClick={window.state.showAssignmentRequest}
                        className="block-button dark">
                    <i className="fal fa-plus"></i>&nbsp;Add Location
                </button>
                {nodes}
            </div>
        )
    }
}

class MenuReporting extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            forms: state.forms || [],
            view: "DEFAULT"
        };

        if (!window.state.forms) {
            window.sonomaClient.tx("com.sonoma.self.forms", [], (res) => {
                window.state.forms = res;
                this.setState({
                    forms: res
                });
            }, (err) => {
                console.log(err);
            });
        } else {
            this.state.forms = window.state.forms;
        }
    }

    componentWillMount() {
        document.body.addEventListener("click", this._handleClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._handleClick);
    }

    _handleClick = (e) => {
        if (this._el) {
            if (!this._el.contains(e.target)) {
                window.state.menu = null;
                window.state.emit("CHANGE");
            }
        }
    };

    _showDrafts = () => {
        window.state.addTab("GRID", {
            name: "Drafts",
            grid: "DRAFTS"
        });
    };

    _showPerformance = () => {
        window.state.addTab("PERFORMANCE", {
            name: "Performance"
        })
    };

    _view = (view) => {
        this.setState({
            view: view
        })
    };

    _showLocation = (uuid) => {
        window.state.addTab("LOCATION_REPORTING", {
            name: "Some Location",
            uuid: uuid || null
        })
    };

    render() {
        let forms = this.state.forms.sort((a, b) => {
            return (a.name.en || a.name).localeCompare(b.name.en || b.name);
        });

        let active_forms = forms.filter(item => {
            return item.status == 'ACTIVE';
        });

        let archived_forms = forms.filter(item => {
            return item.status == 'ARCHIVED';
        });

        let general = active_forms.filter(item => {
            return item.features.LOCATION_REPORTING == null;
        });

        let location = active_forms.filter(item => {
            return item.features.LOCATION_REPORTING != null;
        })

        let showArchived = archived_forms.length > 0 ? true : false;

        let inner;

        if (this.state.view == "DEFAULT") {
            inner = (
                <div>
                    <button onClick={window.state.showAssignmentRequest}
                            className="block-button dark">
                        <i className="fal fa-plus"></i>&nbsp;Add Form
                    </button>

                    <div className="dash-section">
                        <i className="fal fa-clipboard"></i>&nbsp;General
                    </div>

                    {general.map(item => {
                        return (
                            <FormItem
                                data={item}/>
                        )
                    })}

                    <div className="dash-section">
                        <i className="fal fa-map-marker"></i>&nbsp;Location-Based
                    </div>

                    {location.map(item => {
                        return (
                            <FormItem data={item}/>
                        )
                    })}

                    <div className="dash-section">
                        <i className="fal fa-box"></i>&nbsp;Archived
                    </div>

                    {archived_forms.map(item => {
                        return (
                            <FormItem data={item}/>
                        )
                    })}

                </div>
            )
        }

        if (this.state.view == "LOCATIONS") inner = <Locations/>;

        return (
            <div className="row dark"
                 id="reporting-menu"
                 ref={(el) => this._el = el}
                 style={{maxWidth: "250px", minWidth: "250px", background: "rgb(47, 49, 54)", padding: "0"}}>
                <div className="column">
                    <div className="row block bb"
                         style={{maxHeight: "25px", minHeight: "25px", padding: "4px 4px 0px"}}>
                        <div
                            onClick={() => this._view("DEFAULT")}
                            className={"clicker " + (this.state.view == "DEFAULT" ? " active" : "")}>
                            <i className="fal fa-clipboard"></i>&nbsp;Forms
                        </div>
                        <div onClick={() => this._view("LOCATIONS")}
                             className={"clicker " + (this.state.view == "LOCATIONS" ? " active" : "")}>
                            <i className="fal fa-map-marker"></i>&nbsp;Locations
                        </div>
                    </div>
                    <div className="row block block-overflow" style={{padding: "0"}}>


                        {inner}

                    </div>
                </div>


            </div>
        )
    }
}

export default MenuReporting;
