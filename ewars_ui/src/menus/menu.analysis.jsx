import React from "react";

import ActionGroup from "../controls/c.actiongroup.jsx";

const SUB_ACTIONS = [
    ['fa-plus', 'CREATE', 'Create new'],
    ['fa-upload', 'IMPORT', 'Import']
];

const RESOURCES = {
    NOTEBOOKS: "NOTEBOOK",
    MAPS: "MAP",
    PLOTS: "PLOT",
    PIVOTS: "PIVOT"
}


class ItemHandle extends React.Component {
    constructor(props) {
        super(props);
    }

    _click = () => {
        window.state.addTab(this.props.data.content_type, {
            uuid: this.props.data.uuid,
            name: this.props.data.name.en || this.props.data.name
        });
    };

    render() {
        return (
            <div
                onClick={this._click}
                className="dash-handle">
                <div className="inner">
                    {this.props.data.name.en || this.props.data.name}
                </div>
            </div>
        )
    }
}

class SubMenu extends React.Component {
    state = {
        data: null
    };

    constructor(props) {
        super(props);
        this.mounted = false;
    }

    _action = (action) => {
        switch (action) {
            case "BACK":
                this.props.onBack();
                break;
            default:
                break;
        }
    };

    _onClick = (e) => {

    };

    componentWillMount() {
        window.sonomaClient.tx("com.sonoma.self.resources", RESOURCES[this.props.data], (res) => {
            this.setState({
                data: res
            })
        }, (err) => {
            console.log(err);
        })
    }

    render() {
        return (
            <div className="column block block-overflow">
                <div className="row bb gen-toolbar" style={{maxHeight: "34px", minHeight: "34px"}}>
                    <ActionGroup
                        naked={true}
                        actions={[['fa-caret-left', 'BACK', 'Back']]}
                        onAction={this._action}/>
                    <div className="column"></div>
                    <ActionGroup
                        naked={true}
                        actions={SUB_ACTIONS}
                        onAction={this._action}/>
                </div>
                <div className="row block block-overflow">

                    <div className="dash-header">Owned</div>
                    {(this.state.data || []).map(item => {
                        return (
                            <ItemHandle view={this.props.data} data={item}/>
                        )
                    })}

                    <div className="dash-header">Shared</div>

                </div>
            </div>
        )
    }
}

class MenuAnalysis extends React.Component {
    state = {
        documents: []
    }

    constructor(props) {
        super(props);

        window.sonomaClient.tx("com.sonoma.documents.available", [], (res) => {
            this.setState({
                documents: res
            })
        }, (err) => {
            console.log(err);
        });
    }

    componentWillMount() {
        document.body.addEventListener("click", this._handleClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._handleClick);
    }

    _handleClick = (e) => {
        if (this._el) {
            if (!this._el.contains(e.target)) {
                window.state.menu = null;
                window.state.emit("CHANGE");
            }
        }
    };

    _loadItems = (view) => {
        this.setState({
            subType: view
        })
    };

    _view = (view) => {
        switch (view) {
            case "ME_AUDITOR":
                window.state.addTab("ME_AUDITOR", {});
                break;
            case "EXPORT":
                window.state.addTab("EXPORT", {});
                break;
            case "ALERT_AUDITOR":
                window.state.addTab("ALERT_AUDITOR", {});
                break;
            default:
                window.state.getState("CORE").setState({
                    view: "BROWSER",
                    viewConfig: {type: view}
                });
                break;
        }
    };

    render() {
        if (this.state.subType) {
            return (
                <SubMenu
                    onBack={() => this.setState({subType: null})}
                    data={this.state.subType}/>
            )
        }

        return (
            <div className="column block block-overflow"
                 ref={(el) => this._el = el}>
                <div className="dash-handle" onClick={() => this._loadItems("NOTEBOOKS")}>
                    <div className="inner">
                        <i className="fal fa-book"></i>&nbsp;Notebooks
                    </div>
                </div>

                <div
                    onClick={() => this._loadItems("MAPS")}
                    className="dash-handle">
                    <div className="inner">
                        <i className="fal fa-map"></i>&nbsp;Mapping
                    </div>
                </div>
                <div
                    onClick={() => this._loadItems("PLOTS")}
                    className="dash-handle">
                    <div className="inner">
                        <i className="fal fa-chart-bar"></i>&nbsp;Plots
                    </div>
                </div>
                <div
                    onClick={() => this._loadItems("PIVOTS")}
                    className="dash-handle">
                    <div className="inner">
                        <i className="fal fa-table"></i>&nbsp;Pivots
                    </div>
                </div>
                <div
                    onClick={() => this._view("ME_AUDITOR")}
                    className="dash-handle">
                    <div className="inner">
                        <i className="fal fa-search"></i>&nbsp;M&E Auditor
                    </div>
                </div>
                <div
                    onClick={() => this._view("ALERT_AUDITOR")}
                    className="dash-handle">
                    <div className="inner">
                        <i className="fal fa-search"></i>&nbsp;Alert Auditor
                    </div>
                </div>
                <div
                    onClick={() => this._view("EXPORT")}
                    className="dash-handle">
                    <div className="inner">
                        <i className="fal fa-download"></i>&nbsp;Data Export
                    </div>
                </div>
            </div>
        )
    }
}

export default MenuAnalysis;
