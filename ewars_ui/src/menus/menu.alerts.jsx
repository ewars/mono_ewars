import React from "react";


class AlarmItem extends React.Component {
    _click = () => {
        window.state.addTab("ALERTS", {
            aid: this.props.data.uuid,
            name: this.props.data.name
        });
    };

    render() {
        let label = this.props.data.name.en || this.props.data.name;
        if (this.props.data.open_alerts) label += " (" + this.props.data.open_alerts + ")";
        return (
            <div className="dash-handle" onClick={this._click}>{label}</div>
        )
    }
}

class MenuAlerts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alarms: []
        };

        if (!window.state.alarms) {
            window.sonomaClient.tx("com.sonoma.self.alarms", [], (res) => {
                window.state.alarms = res;
                this.setState({
                    alarms: res
                })
            }, (err) => {
                console.log(err);
            });
        } else {
            this.state.alarms = window.state.alarms;
        }
    }

    componentWillMount() {
        document.body.addEventListener("click", this._handleClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._handleClick);
    }

    _handleClick = (e) => {
        if (this._el) {
            if (!this._el.contains(e.target)) {
                window.state.menu = null;
                window.state.emit("CHANGE");
            }
        }
    };

    render() {
        return (
            <div className="row block block-overflow dark"
                 id="alert-menu"
                 ref={(el) => this._el = el}
                 style={{maxWidth: "250px", minWidth: "250px", background: "rgb(47, 49, 54)"}}>

                <div className="dash-section">
                    <i className="fal fa-check"></i>&nbsp;Active Alarms
                </div>
                {this.state.alarms.map(item => {
                    return <AlarmItem data={item}/>
                })}

                <div className="dash-section">
                    <i className="fal fa-square"></i>&nbsp;Archived Alarms
                </div>

            </div>
        )
    }
}

export default MenuAlerts;
