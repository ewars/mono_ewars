import React from "react";

class ItemHandle extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        <div className="dash-handle">
            {this.props.data.name.en || this.props.data.name}
        </div>
    }
}

class TeamItem extends React.Component {
    _click = () => {
        window.state.getState("CORE").setState({
            view: "TEAM",
            viewConfig: this.props.data.uuid
        })
    };

    render() {
        return (
            <div className="dash-handle sub-item" onClick={this._click}>
                <i className="fal fa-hashtag"></i>&nbsp;{this.props.data.name}
            </div>
        )
    }
}

class DashHandle extends React.Component {
    _click = () => {

        let hasTab = window.state.tabs.filter(item => {
            return item.uuid == this.props.data.uuid && item.type == "DASHBOARD";
        })[0] || null;

        if (hasTab) {
            window.state.setActiveTab(hasTab._id);
        } else {
            window.state.addTab("DASHBOARD", {
                uuid: this.props.data.uuid,
                name: this.props.data.name.en || this.props.data.name
            });
        }
    };

    render() {
        return (
            <div className="dash-handle sub-item" onClick={this._click}>
                {this.props.data.name.en || this.props.data.name}
            </div>
        )
    }
}

class Dashboards extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dashboards: []
        }

        window.sonomaClient.tx('com.sonoma.self.dashboards', [], (res) => {
            this.setState({
                dashboards: res
            })
        }, (err) => {
            window.state.error("There was an error loading your dashborads", err);
        })
    }

    render() {
        if (this.state.dashboards.length <= 0) return null;

        return (
            <div>
                <div className="dash-section">
                    <i className="fal fa-tachometer"></i>&nbsp;Dashboards
                </div>
                {this.state.dashboards.map(item => {
                    return (
                        <DashHandle data={item}/>
                    )
                })}

            </div>
        )
    }
}

class Teams extends React.Component {
    state = {
        teams: []
    };

    constructor(props) {
        super(props);

        this.state.teams = state._teams || [];

        window.sonomaClient.tx("com.sonoma.self.teams", [], (res) => {
            this.setState({
                teams: res
            })
        }, (err) => console.log(err));
    }

    componentWillMount() {
        window.state.on("TEAMS_CHANGED", this._onTeamsChange);
    }

    componentWillUnmount() {
        window.state.off("TEAMS_CHANGED", this._onTeamsChange);
    }

    _onTeamsChange = () => {
        this.setState({teams: window.state._teams});
    };

    render() {
        if (this.state.teams.length <= 0) return null;

        return (
            <div>
                <div className="dash-section">
                    <i className="fal fa-users"></i>&nbsp;Teams
                </div>
                {(this.state.teams || []).map(item => {
                    return <TeamItem data={item}/>
                })}
            </div>
        )
    }

}


class MainMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            forms: [],
            alarms: [],
        }
    }

    _main = () => {
        window.state.setActiveTab("DEFAULT");
    };

    _projects = () => {
        window.state.addTab("PROJECTS", {});
    };

    render() {
        let _state = window.state.getState("CORE");
        return (
            <div className="row block block-overflow dark"
                 style={{maxWidth: "250px", minWidth: "250px", background: "rgb(44, 49, 55)"}}>
                <div
                    onClick={this._main}
                    className="dash-handle">
                    <i className="fal fa-user"></i>&nbsp;{window.state.getName()}
                </div>

                <div
                    onClick={this._projects}
                    className="dash-handle">
                    <i className="fal fa-briefcase"></i>&nbsp;Projects
                </div>

                <Dashboards/>
                <Teams/>
            </div>
        )
    }
}

export default MainMenu;
