import React from "react";

class DashHandle extends React.Component {
    _click = () => {

        let hasTab = window.state.tabs.filter(item => {
            return item.uuid == this.props.data.uuid && item.type == "DASHBOARD";
        })[0] || null;

        if (hasTab) {
            window.state.setActiveTab(hasTab._id);
        } else {
            window.state.addTab("DASHBOARD", {
                uuid: this.props.data.uuid,
                name: this.props.data.name.en || this.props.data.name
            });
        }
    };

    render() {
        return (
            <div className="dash-handle" onClick={this._click}>
                {this.props.data.name.en || this.props.data.name}
            </div>
        )
    }
}


class MainMenu extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            dashboards: []
        };

        window.sonomaClient.tx('com.sonoma.self.dashboards', [], (res) => {
            this.setState({
                dashboards: res
            })
        }, (err) => {
            window.state.error("There was an error loading your dashborads", err);
        })
    }

    componentWillMount() {
        document.body.addEventListener("click", this._handleClick);
    }

    componentWillUnmount() {
        document.body.removeEventListener("click", this._handleClick);
    }

    _handleClick = (e) => {
        if (this._el) {
            if (!this._el.contains(e.target)) {
                window.state.menu = null;
                window.state.emit("CHANGE");
            }
        }
    };

    _main = () => {
        window.state.setActiveTab("DEFAULT");
    };

    _projects = () => {
        window.state.addTab("PROJECTS", {});
    };

    render() {
        let _state = window.state.getState("CORE");
        return (
            <div
                ref={(el) => this._el = el}
                className="row block block-overflow dark"
                style={{maxWidth: "250px", minWidth: "250px", background: "rgb(44, 49, 55)"}}>
                {this.state.dashboards.map(item => {
                    return (
                        <DashHandle data={item}/>
                    )
                })}
            </div>
        )
    }
}

export default MainMenu;
