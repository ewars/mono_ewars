import React from 'react';
import AppContext from "../context_provider";


import MenuAdmin from "./menu.admin.jsx";
import MenuAlerts from "./menu.alerts.jsx";
import MenuAnalysis from "./menu.analysis.jsx";
import MenuDocuments from "./menu.documents.jsx";
import MenuReporting from "./menu.reporting.jsx";
import MenuMain from "./menu.main.jsx";
import MenuDashboards from "./menu.dashboards.jsx";
import MenuTasks from "./menu.tasks.jsx";
import MenuId from "../controls/c.menu.id.jsx";

import ActionGroup from "../controls/c.actiongroup.jsx";

const FORM_ACTIONS = [
    ['fa-plus', 'CREATE', 'CREATE_RECORD']
];

const LOC_NAME_SORT = (a, b) => {
    return (a.name.en || a.name).localeCompare(b.name.en || b.name);
}


const OTHERS = [
    // ['fa-bed', 'CAPACITY', 'Capacity'],
    // ['fa-user-md', 'STAFF', 'Staffing'],
    // ['fa-diagnoses', 'PATIENTS', 'Patients'],
    // ['fa-stopwatch', 'PERFORMANCE', 'Performance'],
    // ['fa-users', 'POPULATION', 'Population'],
    // ['fa-users', 'PEERS', 'Peers'],
    // ['fa-map', 'MAPPING', 'Mapping'],
    // ['fa-sticky-note', 'NOTES', 'Notes'],
    // ['fa-stamp', 'MOVEMENT', 'Movement'],
    // ['fa-syringe', 'CAMPAIGNS', 'Campaigns'],
    // ['fa-calendar', 'EVENTS', 'Events'],
    // ['fa-image', 'PHOTOS', 'Photos'],
    // ['fa-inventory', 'INVENTORY', 'Inventory'],
    // ['fa-comments', 'DISCUSSION', 'Discussion'],
    // ['fa-street-view', 'CHECKIN', 'Check-ins']
];

const BLANKED = true;

class MenuItem extends React.Component {
    _view = () => {
        this.props.onChange(this.props.view);
    };

    _click = () => {
        switch (this.props.view) {
            case "TASKS":
                window.state.menu = null;
                window.state.addTab("TASKS", {name: "Tasks"});
                break;
            case "DEFAULT":
                window.state.menu = null;
                window.state.setTab("DEFAULT");
                break;
            default:
                window.state.setMenu(this.props.view);
                break;
        }
    }

    render() {
        let _state = window.state.getState("CORE");
        let className = "menu-item" + (this.props.active ? " active" : "");

        let style = {borderLeftColor: this.props.color};

        return (
            <div className={className}
                 style={style}
                 onClick={this._click}>
                <div className="marker"></div>
                <div className="row menu-item-icon block">
                    <i className={"fal " + this.props.icon}></i>
                </div>
                <div className="row menu-item-label block">{this.props.label}</div>
            </div>
        )
    }
}

class MenuPart extends React.Component {
    render() {
        let className = "menu-part " + (this.props.active ? " active" : "");
        return (
            <div className={className}></div>
        )
    }
}

class MultiMenu extends React.Component {
    constructor(props) {
        super(props);
    }

    _showDrafts = () => {
        window.state.addTab("DRAFTS", {
            name: "Drafts"
        })
    };

    _showAssignment = () => {
        window.state.showAssignmentRequest();
    };

    render() {
        return (
            <div className="column br block block-overflow dark"
                 style={{padding: "0 8px 0 8px", background: "#202225"}}>

                <button onClick={this._showDrafts}
                        className="block-button dark">
                    <i className="fal fa-clipboard"></i>&nbsp;Drafts
                </button>
                <button onClick={this._showAssignment}
                        className="block-button dark">
                    <i className="fal fa-plus"></i>&nbsp;Add Assignment
                </button>
                <div className="dash-section">
                    <i className="fal fa-clipboard"></i>&nbsp;Forms
                </div>

                <div className="dash-section">
                    <i className="fal fa-map-marker"></i>&nbsp;Locations
                </div>
            </div>

        )
    }
}

class Menu extends React.Component {
    _menuWidth = 60;
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentWillMount() {
        window.state.on("MENU_CHANGE", this._change);
    }

    componentWillUnmount() {
        window.state.off('MENU_CHANGE', this._change);
    }

    _handleDragStart = (e) => {
        this._lastDownX = e.clientX;
        document.addEventListener("mousemove", this._handleMouseMove);
    };

    _handleDragEnd = (e) => {
        this._lastDownX = null;
        document.removeEventListener("mousemove", this._handleMouseMove);
        document.removeEventListener("mouseup", this._handleDragEnd);
        this.setState({});
    };

    _handleMouseMove = (e) => {
        let newWidth = e.clientX - this._menu.offsetLeft;
        if (newWidth <= 60) newWidth = 60;
        if (newWidth >= 250) newWidth = 250;
        this._menuWidth = newWidth;
        this._menu.style.maxWidth = newWidth + "px";
        this._menu.style.minWidth = newWidth + "px";
    };

    componentDidMount() {
        this._dragger.addEventListener("mousedown", this._handleDragStart);
        document.addEventListener("mouseup", this._handleDragEnd);
        this._dragger.addEventListener("mouseup", this._handleDragEnd);
    }

    _change = () => {
        this.setState({});
    };

    render() {
        let menu = null;

        if (this._menuWidth >= 200) {
            // We're showing the full menu view
            return (
                <div className="row" style={{flex: 1, flexBasis: 0, flexGrow: 0}}>
                    <div
                        ref={(el) => this._menu = el}
                        style={{minWidth: this._menuWidth + "px", maxWidth: this._menuWidth + "px"}}
                        className="column main-menu">
                        <MultiMenu/>;
                    </div>
                    <div className="column" style={{
                        maxWidth: "1px",
                        minWidth: "1px",
                        padding: "0px",
                        overflow: "hidden",
                        position: "relative"
                    }}>
                        <div className="drag" ref={(el) => this._dragger = el}></div>
                    </div>
                </div>
            )

        }

        if (window.state.menu === "REPORTING") menu = <MenuReporting/>;
        if (window.state.menu === "ALERTS") menu = <MenuAlerts/>;
        if (window.state.menu === "ANALYSIS") menu = <MenuAnalysis/>;
        if (window.state.menu === "DOCUMENTS") menu = <MenuDocuments/>;
        if (window.state.menu === "ADMIN") menu = <MenuAdmin/>;
        if (window.state.menu == "DASHBOARDS") menu = <MenuDashboards/>;

        return (
            <div className="row" style={{flex: 1, flexBasis: 0, flexGrow: 0}}>
                <div className="column block main-menu block-overflow"
                     ref={(el) => this._menu = el}
                     id="son-main-menu"
                     style={{maxWidth: "66px", minWidth: "66px", padding: "0"}}>
                    {!BLANKED ?
                        <MenuItem
                            color="blue"
                            active={window.state.menu == "DASHBOARDS"}
                            icon="fa-tachometer"
                            view="DASHBOARDS"
                            label="Dashboards"/>
                        : null}
                    <MenuItem
                        color="orange"
                        active={window.state.menu == "REPORTING"}
                        icon="fa-clipboard"
                        view="REPORTING"
                        label="Reporting"/>
                    <MenuItem
                        color="red"
                        active={window.state.menu == "ALERTS"}
                        icon="fa-bell"
                        view="ALERTS"
                        label="Alerts"/>
                    {false ?
                        <MenuItem
                            color="green"
                            active={window.state.menu == "TASKS"}
                            icon="fa-check-square"
                            view="TASKS"
                            label="Tasks"/>
                        : null}
                    {!BLANKED ?
                        <div>
                            <MenuItem
                                color="pink"
                                active={window.state.menu == "TEAMS"}
                                icon="fa-users"
                                view="TEAMS"
                                label="Teams"/>
                            <MenuItem
                                color="purple"
                                active={window.state.menu == "ANALYSIS"}
                                icon="fa-chart-line"
                                view="ANALYSIS"
                                label="Analysis"/>
                            <MenuItem
                                color="violet"
                                active={window.state.menu == "DOCUMENTS"}
                                icon="fa-file"
                                view="DOCUMENTS"
                                label="Documents"/>
                            <MenuItem
                                color="black"
                                active={window.state.menu == "ADMIN"}
                                icon="fa-cog"
                                view="ADMIN"
                                label="Admin"/>
                        </div>
                        : null}
                </div>
                <div className="column" style={{
                    maxWidth: "1px",
                    minWidth: "1px",
                    padding: "0px",
                    overflow: "hidden",
                    position: "relative"
                }}>
                    <div className="drag" ref={(el) => this._dragger = el}></div>
                    {!BLANKED ?
                        <MenuPart active={window.state.menu == "DASHBOARDS"}/>
                        : null}
                    <MenuPart active={window.state.menu == "REPORTING"}/>
                    <MenuPart active={window.state.menu == "ALERTS"}/>
                    <MenuPart active={window.state.menu == "TASKS"}/>
                    {!BLANKED ?
                        <div>
                            <MenuPart active={window.state.menu == "TEAMS"}/>
                            <MenuPart active={window.state.menu == "ANALYSIS"}/>
                            <MenuPart active={window.state.menu == "DOCUMENTS"}/>
                            <MenuPart active={window.state.menu == "ADMIN"}/>
                        </div>
                        : null}
                    <div className="menu-bottom"></div>
                </div>
                {menu ?
                    <div className="menu-flyover" id="menu-popout">
                        {menu}
                    </div>
                    : null}
            </div>
        )
    }
}

class FlatReporting extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);

        this.state = {
            forms: []
        }
    }

    componentDidMount() {
        this.context.tx("com.sonoma.self.forms", null, (res) => {
            this.setState({forms: res});
        }, (err) => {
            window.error("Error loading assigments");
            console.log(err);
        });
    }

    _showRecords = (item) => {
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                type: "BROWSE_RECORDS",
                name: __(item.name),
                form_id: item.uuid
            }
        }));
    };

    _showFormRequest = () => {
        window.state.showAssignmentRequest("FORM");
    };

    render() {
        let items = this.state.forms.sort(LOC_NAME_SORT);
        return (
            <div className="dash-padder">
                {items.map(item => {
                    let iconClass = "fal fa-file";
                    if (item.features.LOCATION_REPORTING) iconClass = "fal fa-map-marker";
                    return (
                        <div className="dash-handle" onClick={() => this._showRecords(item)}>
                            <div className="inner">
                                <div className="label">{item.name.en || item.name}</div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }
}

class FlatLocations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: [],
            loaded: false
        }
    }

    componentDidMount() {
        window.tx('com.sonoma.self.locations', null, (res) => {
            this.setState({
                locations: res,
                loaded: true
            })
        })
    }

    _showLocation = (data) => {
        // this.props.onSelect(data);
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                type: "LOCATION_REPORTING",
                name: __(data.name),
                uuid: data.uuid
            }
        }));
    };

    _addLocation = () => {
        window.state.showAssignmentRequest("LOCATION");
    };

    render() {
        console.log(this.state);

        if (this.state.loaded && this.state.locations.length <= 0) {
            return (
                <div className="column">
                    <div className="row block block-overflow padded-8">
                        <p>Looks like you don't have any location assignments yet.</p>

                        <p>You can request access to a new location by clicking the button below.</p>

                        <button className="block-button">
                            <i className="fal fa-plus"></i>&nbsp;Add location
                        </button>
                    </div>
                </div>
            )
        }

        let groups = {};

        this.state.locations.forEach(item => {
            if (groups[item.lti]) {
                groups[item.lti].push(item);
            } else {
                groups[item.lti] = [item];
            }
        });

        let items = [];

        for (let i in groups) {
            items.push(
                <div className="dash-header">
                    {groups[i][0].lti_name.en || groups[i][0].lti_name}
                </div>
            );

            groups[i].forEach(item => {
                items.push(
                    <div className="dash-handle" onClick={() => this._showLocation(item)}>
                        <div className="inner">
                            <div className="icon"><i className="fal fa-map-marker"></i></div>
                            <div className="label">{item.name.en || item.name}</div>
                        </div>
                    </div>
                )
            });
        }

        return (
            <div className="column">
                <div className="row block block-overflow padded-0">
                    {items}
                </div>
                <div className="menu-foot" onClick={this._addLocation}>
                    <i className="fal fa-plus"></i>&nbsp;Add location
                </div>
            </div>
        )
    }
}

class FlatAlerts extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);

        this.state = {
            alerts: []
        }
    }

    _click = (data) => {
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                type: "BROWSE_ALERTS",
                aid: data.uuid,
                name: window.__(data.name),
                status: "OPEN"
            }
        }));
    };

    componentDidMount() {
        window.tx("com.sonoma.self.alarms", null, (res) => {
            this.setState({
                alerts: res || []
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_ALARMS", err);
        })
    }

    render() {
        return (
            <div className="dash-padder">
                {this.state.alerts.map(item => {
                    return (
                        <div className="dash-handle" onClick={() => {
                            this._click(item)
                        }}>
                            <div className="inner">
                                <div className="label">
                                    {item.name}
                                </div>
                                <div className="icon text-align-right">{item.alert_count || 0}</div>
                            </div>
                        </div>

                    )
                })}
            </div>
        )
    }
}


class LocationNode extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            locations: [],
            loaded: false,
            hasChildren: false,
            showChildren: false
        }

    }

    componentWillUnmount() {
    }

    _toggle = () => {

        if (!this.state.loaded) {
            this._getKids();
        } else {
            this.setState({
                showChildren: !this.state.showChildren
            })
        }
    };

    _getKids = () => {
        this._iconEl.setAttribute("class", "fal fa-cog fa-spin");
        window.sonomaClient.tx("com.sonoma.location.children", this.props.data.uuid, (res) => {
            this.setState({
                children: res.sort(LOC_NAME_SORT),
                showChildren: true,
                loaded: true
            })
        }, (err) => console.log(err));
    };

    _select = () => {
        window.state.addTab("LOCATION_REPORTING", {
            name: this.props.data.name.en || this.props.data.name,
            uuid: this.props.data.uuid
        });
    };

    render() {
        let icon = "fa-folder";
        if (this.state.showChildren) icon = "fa-folder-open";

        let style = {borderLeftColor: "transparent"};

        if (this.props.data.status == 'ACTIVE') style.borderLeftColor = "lightgreen";
        if (this.props.data.status == 'INACTIVE') style.borderLeftColor = "lightcoral";
        if (this.props.data.status == 'DISABLED') style.borderLeftColor = "lightcoral";

        let hasChildren = this.props.data.child_count > 0;

        return (
            <div className="tree-node">
                <div className="item row"
                     style={style}>
                    {hasChildren ?
                        <div className="grip" onClick={this._toggle}>
                            <i ref={(el) => {
                                this._iconEl = el;
                            }} className={"fal " + icon}/>
                        </div>
                        : null}
                    <div className="column label" onClick={this._select}>
                        {this.props.data.name.en || this.props.data.name}
                    </div>
                </div>
                {this.state.showChildren ?
                    <div className="tree-children">
                        {this.state.children.map(item => {
                            return <LocationNode onSelect={this.props.onSelect} data={item}/>;
                        })}

                    </div>
                    : null}
            </div>
        )
    }

}

class AdminLocations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: [],
            search: ""
        }
    }

    componentDidMount() {
        window.sonomaClient.tx('com.sonoma.locations.root', null, (res) => {
            this.setState({
                locations: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_LOCATIONS", err);
        })
    }

    render() {
        return (
            <div className="column">
                <div className="row block" style={{maxHeight: "55px"}}>
                    <div className="search">
                        <div className="search-inner">
                            <div className="row">
                                <div className="column search-left" style={{maxWidth: "25px"}}>
                                    <i className="fal fa-search"></i>
                                </div>
                                <div className="column search-mid">
                                    <input
                                        name="search"
                                        type="text"
                                        placeholder="search..."
                                        onChange={this._onSearcChange}
                                        value={this.state.search}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row block block-overflow" style={{padding: "0"}}>
                    {this.state.locations.map(item => {
                        return <LocationNode
                            onSelect={(loc) => {
                                this.setState({location: loc});
                            }}
                            data={item}/>
                    })}

                </div>
            </div>
        )
    }
}

class RegionalAdminLocations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: [],
            search: ""
        }
    }

    componentDidMount() {
        window.sonomaClient.tx("com.sonoma.self.locations", null, (res) => {
            console.log(res);
            this.setState({
                locations: res
            })
        }, (err) => {
            window.state.error("ERROR_LOADING_LOCATIONS", err);
        })
    }

    render() {
        return (
            <div className="column">
                <div className="row block" style={{maxHeight: "55px"}}>
                    <div className="search">
                        <div className="search-inner">
                            <div className="row">
                                <div className="column search-left" style={{maxWidth: "25px"}}>
                                    <i className="fal fa-search"></i>
                                </div>
                                <div className="column search-mid">
                                    <input
                                        name="search"
                                        type="text"
                                        placeholder="search..."
                                        onChange={this._onSearcChange}
                                        value={this.state.search}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row block block-overflow" style={{padding: "0"}}>
                    {this.state.locations.map(item => {
                        return <LocationNode
                            onSelect={(loc) => {
                                this.setState({location: loc});
                            }}
                            data={item}/>
                    })}

                </div>
            </div>
        )
    }
}


class FlatMenu extends React.Component {
    static contextType = AppContext;
    constructor(props) {
        super(props);

        this.state = {
            view: "DEFAULT",
            location: null
        }
    }

    _view = (view) => {
        this.setState({view: view})
    };

    _selectLocation = (location) => {

    };

    _clear = () => {
        this.setState({
            location: null
        })
    };

    _showTasks = () => {
        window.dispatchEvent(new CustomEvent("ADD_TAB", {
            detail: {
                type: "TASKS",
                name: __("TASKS"),
                icon: "fa-check-square"
            }
        }));
    };

    _showAssignment = () => {
        window.dispatchEvent(new CustomEvent("SHOW_ASSIGNMENT_SELECT", {}));
    };

    render() {
        let view;

        switch (this.state.view) {
            case "LOCATIONS":
                if (this.context.user.role == "USER") view = <FlatLocations/>;
                if (this.context.user.role == "ACCOUNT_ADMIN") view = <AdminLocations/>;
                if (this.context.user.role == "REGIONAL_ADMIN") view = <RegionalAdminLocations/>;
                break;
            case "TASKS":
                view = <MenuTasks/>;
                break;
            case "ANALYSIS":
                view = <MenuAnalysis/>;
                break;
            case "ADMIN":
                view = <MenuAdmin/>;
                break;
            case "DOCUMENTS":
                view = <MenuDocuments/>;
                break;
            default:
                view = (
                    <div className="column">
                        <div className="row block block-overflow">

                            {false ?
                            <div className="dash-handle" onClick={this._showTasks}>
                                <div className="inner">
                                    <i className="fal fa-tasks"></i>&nbsp;Tasks
                                </div>
                            </div>
                            : null}

                            <div className="dash-header"><i className="fal fa-clipboard"></i>&nbsp;Reporting</div>
                            <FlatReporting/>

                            <div className="dash-header"><i className="fal fa-bell"></i>&nbsp;Alerts</div>
                            <FlatAlerts/>
                        </div>
                        {this.context.user.role == "USER" ?
                            <div className="menu-foot" onClick={this._showAssignment}>
                                <i className="fal fa-plus"></i>&nbsp;Add assignment
                            </div>
                            : null}
                    </div>
                );
                break;
        }

        return (
            <div className="column panel br" style={{padding: "0"}}>
                <MenuId/>
                {view}

            </div>

        )

            /**
        return (
            <div className="column panel br" style={{padding: "0"}}>
                <MenuId/>
                <div className="row bb gen-toolbar"
                     style={{maxHeight: "25px", minHeight: "25px", padding: "4px 4px 0px 4px"}}>
                    <div onClick={() => this._view("DEFAULT")}
                         className={"clicker" + (this.state.view == "DEFAULT" ? " active" : "")}>
                        <i className="fal fa-user"></i>
                    </div>
                    {true ?
                    <div onClick={() => this._view("LOCATIONS")}
                         className={"clicker" + (this.state.view == "LOCATIONS" ? " active" : "")}>
                        <i className="fal fa-map-marker"></i>
                    </div>
                    : null}

                    {false ?
                        <div onClick={() => this._view("ANALYSIS")}
                             className={"clicker" + (this.state.view == "ANALYSIS" ? " active" : "")}>
                            <i className="fal fa-chart-line"></i>
                        </div>
                        : null}
                    {false ?
                        <div onClick={() => this._view("DOCUMENTS")}
                             className={"clicker" + (this.state.view == "DOCUMENTS" ? " active" : "")}>
                            <i className="fal fa-file-contract"></i>
                        </div>
                        : null}

                    {false && this.context.user.role == "ACCOUNT_ADMIN" ?
                        <div onClick={() => this._view("ADMIN")}
                             className={"clicker" + (this.state.view == "ADMIN" ? " active" : "")}>
                            <i className="fal fa-cog"></i>
                        </div>
                        : null}
                </div>
                {view}

            </div>
        )
             **/
    }

}

export default Menu;
export {FlatMenu, Menu};
