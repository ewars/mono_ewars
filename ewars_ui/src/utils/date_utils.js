import Moment from "moment";

const CONSTANTS = require("../constants/constants");

/**
 * Get most recently compvared interval date
 * @param specDate
 * @param interval
 * @returns {*}
 */
var getActualDate = function (specDate, interval, no_future) {
    if (interval == CONSTANTS.DAY) return specDate;

    if (interval == CONSTANTS.WEEK) {
        var actualEndWeek = specDate.clone().endOf("isoweek");

        if (no_future) {
            if (actualEndWeek.isAfter(specDate, "d")) {
                return actualEndWeek.clone().subtract(7, "d");
            } else {
                return actualEndWeek;
            }
        } else {
            return actualEndWeek;
        }
    }

    if (interval == CONSTANTS.MONTH) {
        var actualEndMonth = specDate.clone().endOf("month");

        if (specDate.isSame(actualEndMonth, "day")) return actualEndMonth;
        if (specDate.isBefore(actualEndMonth, "day")) return actualEndMonth.clone()
            .subtract(1, "M")
            .endOf("month");
    }

    if (interval == CONSTANTS.YEAR) {
        var actualEndYear = specDate.clone().endOf("year");

        if (specDate.isSame(actualEndYear, "day")) return actualEndYear;
        if (specDate.isBefore(actualEndYear, "day"))
            return actualEndYear.clone().subtract(1, "y").endOf("year");
    }
};

var getDaysBetween = function (startDate, endDate) {
    var resultantDates = [startDate];

    var cur_date = startDate.clone();

    while (cur_date.isBefore(endDate, CONSTANTS.DAY)) {
        var newDate = cur_date.clone().add(1, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;

};

var getWeeksBetween = function (startDate, endDate) {
    var resultantDates = [startDate.clone()];

    var cur_date = startDate.clone();

    while (cur_date.isBefore(endDate, "day")) {
        var newDate = cur_date.clone().add(7, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }


    return resultantDates;
};


var getMonthsBetween = function (startDate, endDate) {

};

var getYearsBetween = function (startDate, endDate) {

};

var INTERVAL_MAP = {
    "NONE": "d",
    "DAY": "d",
    "WEEK": "d",
    "MONTH": "M",
    "YEAR": "Y",
    "EPI_WEEK": "d"
};

/**
 * retrieve the most recently completed interval from a date
 * @param sourceDate The date to source from
 * @param interval The interval type
 * @returns {*}
 */
function getMostRecentlyCompletedInterval(sourceDate, interval) {
    if (interval == "DAY") return Moment.utc(sourceDate).clone().subtract(1, "d");
    if (interval == "WEEK") {
        let selectedWeek = Moment.utc(sourceDate).clone();
        let today = Moment.utc();

        if (selectedWeek.isAfter(today, 'day') || selectedWeek.isSame(today, 'day')) {
            // Date is in the future or same date, get the last completed week from todays date
            let prevEnd = today.subtract(7, 'd');
            prevEnd.isoWeekday(7);
            return prevEnd;
        } else {
            // The date is before the current date in this case, we need to find the last completed week
            let curEnd = sourceDate.clone();
            curEnd = curEnd.isoWeekday(7);
            return curEnd;
        }

    }
    if (interval == "MONTH") return Moment.utc(sourceDate).clone();
    if (interval == "YEAR") return Moment.utc(sourceDate).clone();
    return Moment.utc(sourceDate).clone();
}

function getBeginningOfPeriod(sourceDate, interval) {
    if (interval == "DAY") return sourceDate.clone();
    if (interval == "WEEK") {
        let selectedWeek = sourceDate.clone();
        selectedWeek.isoWeekday(1);
        return selectedWeek;
    }

    if (interval == "MONTH") {
        return sourceDate
            .clone()
            .day(1);
    }

    if (interval == "YEAR") {
        return sourceDate
            .clone()
            .month(1)
            .day(1);
    }
}


const DateUtils = {};

DateUtils.getYearDates = () => {
    let today = Moment(),
        yearStart = Moment().startOf("year"),
        yearEnd = Moment().endOf("year");

    let dates = [];

    while (yearStart.isBefore(yearEnd, "d")) {
        dates.push(yearStart.format("YYYY-MM-DD"));
        yearStart = yearStart.add(1, "d");
    }

    return dates;
}


DateUtils.getMonthsInPeriod = (startDate, o_endDate) => {
    let endDate = Moment(o_endDate || new Date()).format("YYYY-MM-DD");
    let results = [];
    let today = new Moment().format("YYYY-MM-DD");

    let curYear = parseInt(startDate.split("-")[0]),
        curMonth = parseInt(startDate.split('-')[1]),
        thisYear = parseInt(today.split("-")[0]),
        thisMonth = parseInt(today.split('-')[0]);

    if (endDate) {
        thisYear = parseInt(endDate.split("-")[0]);
        thisMonth = parseInt(endDate.split("-")[1]);
    }


    while (curYear <= thisYear && curMonth <= thisMonth) {
        let newDate = Moment(`${curYear}-${curMonth}-01`).endOf("month").format("YYYY-MM-DD");
        results.push(newDate);

        if (curMonth == 12) {
            curMonth = 1;
            curYear += 1;
        } else {
            curMonth++;
        }
    }


    return results.reverse();
};

DateUtils.getYearsInPeriod = (startDate, o_endDate) => {
    let endDate = Moment(o_endDate || new Date()).format("YYYY-MM-DD");

    let results = [];

    let today = new Moment().format("YYYY-MM-DD");

    let curYear = parseInt(startDate.split("-")[0]),
        thisYear = parseInt(today.split("-")[0]);

    if (endDate) {
        thisYear = parseInt(endDate.split("-")[0]);
    }

    while (curYear <= thisYear) {
        result.push(`${curYear}-12-31`);

        curYear++;
    }

    return results;
}

DateUtils.getWeeksInPeriod = (startDate, o_endDate) => {
    let endDate = o_endDate || Moment().format("YYYY-MM-DD");

    if (startDate.split("-")[0] < 2017) {
        startDate = "2017-01-01";
    }

    let resultantDates = [];

    let cur_date = Moment.utc(startDate).clone();
    let end_date = Moment.utc(endDate).clone();

    while (cur_date.isBefore(end_date, "day")) {
        let newDate = cur_date.clone().add(7, "d");
        resultantDates.push(newDate.format("YYYY-MM-DD"));
        cur_date = newDate;
    }

    return resultantDates;
};

DateUtils.getDatesInInterval = function (start_date, end_date, interval) {

    var startDate = Moment.utc(start_date),
        endDate;

    if (end_date) endDate = Moment.utc(end_date);
    if (!end_date) endDate = Moment();

    if (interval == CONSTANTS.DAY) endDate.subtract(1, 'd');

    var actualEnd = getActualDate(endDate, interval, true);
    var actualStart = getActualDate(startDate, interval);

    if (interval == CONSTANTS.DAY) return getDaysBetween(actualStart, actualEnd);
    if (interval == CONSTANTS.WEEK) return getWeeksBetween(actualStart, actualEnd);
    if (interval == CONSTANTS.MONTH) return getMonthsBetween(actualStart, actualEnd);
    if (interval == CONSTANTS.YEAR) return getYearsBetween(actualStart, actualEnd);

    return []
};

DateUtils.getAllDatesInInterval = (start_date, end_date, interval) => {
    console.log(start_date, end_date);
    let results = [];

    let startDate = Moment.utc(start_date), endDate;
    startDate = getActualDate(startDate, interval);

    switch (interval) {
        case "DAY":
        default:
            endDate = startDate.clone().endOf("month");
            endDate = getActualDate(endDate, interval);
            break;
        case "WEEK":
            endDate = startDate.clone().endOf("year");
            endDate = getActualDate(endDate, interval);
            results = getWeeksBetween(startDate, endDate);
            break;
        case "YEAR":
            endDate = startDate.endOf("year");
            endDate = getActualDate(endDate, interval);
            results = getYearsBetween(startDate, endDate);
            break;
        case "MONTH":
            endDate = startDate.endOf("year");
            endDate = getActualDate(endDate, interval);
            results = getMonthsBetween(startDate, endDate);
            break;
    }

    return results;
}

DateUtils.getMostRecentlyEndedInterval = function (reportDate, interval) {
    return getActualDate(reportDate, interval);
};

DateUtils.getStartDate = function (specDate, interval) {
    if (!interval) return Moment(specDate).clone();
    if (interval == CONSTANTS.NONE) return Moment(specDate).clone();
    if (interval == CONSTANTS.DAY) return Moment(specDate).clone();
    if (interval == CONSTANTS.WEEK) return Moment(specDate).clone().startOf("isoweek");
    if (interval == CONSTANTS.MONTH) return Moment(specDate).clone().startOf("month");
    if (interval == CONSTANTS.YEAR) return Moment(specDate).clone().startOf("year");
};

DateUtils.getEndDate = function (specDate, interval) {
    if (!interval) return Moment(startDate).clone();
    if (interval == CONSTANTS.NONE) return Moment(specDate).clone();
    if (interval == CONSTANTS.DAY) return Moment(specDate).clone();
    if (interval == CONSTANTS.WEEK) return Moment(specDate).clone().endOf("isoweek");
    if (interval == CONSTANTS.MONTH) return Moment(specDate).clone().endOf("month");
    if (interval == CONSTANTS.YEAR) return Moment(specDate).clone().endOf("year");
};

DateUtils.processDateSpec = function (definition, reportDate) {
    var startDate,
        endDate;

    if (definition.filter) {
        if (!reportDate) endDate = Moment.utc();
        var filterSpec = definition.filter;

        if (filterSpec == "1M") startDate = endDate.clone().subtract(1, 'M');
        if (filterSpec == "3M") startDate = endDate.clone().subtract(3, "M");
        if (filterSpec == "6M") startDate = endDate.clone().subtract(6, "M");
        if (filterSpec == "1Y") startDate = endDate.clone().subtract(1, "Y");
        if (filterSpec == "YTD") {
            startDate = endDate.clone().startOf("year");
        }
        if (filterSpec == "ALL") startDate = endDate.clone().subtract(20, "Y");

    } else if (definition.end_date_spec && definition.start_date_spec) {
        if (definition.end_date_spec == "MANUAL") {
            endDate = Moment.utc(definition.end_date);
        }

        if (definition.end_date_spec == "CURRENT") {
            endDate = Moment.utc();

            endDate = getMostRecentlyCompletedInterval(endDate, definition.interval || "DAY");
        }

        if (definition.end_date_spec == "REPORT_DATE") {
            endDate = Moment.utc(reportDate);
        }

        if (definition.end_date_spec == "REPORT_DATE_SUBTRACTION") {
            var units = parseInt(definition.end_intervals);
            // If the time interval is weeks, we use days for the subtraction as
            // isoweeks isn't very reliable
            var interval = definition.timeInterval || definition.interval;
            if (interval == "WEEK") {
                units = 7 * units;
            }
            endDate = Moment.utc(reportDate).clone().subtract(units, "d");
        }

        if (definition.start_date_spec == "MANUAL") {
            startDate = Moment.utc(definition.start_date);
        }

        if (definition.start_date_spec == "END_SUBTRACTION") {
            var units = parseInt(definition.start_intervals);
            // If the time interval is weeks, we use days for the subtraction as
            // isoweeks isn't very reliable
            var interval = definition.timeInterval || definition.interval;
            if (definition.type == "RAW" && !interval) interval = "WEEK";

            if (interval == "WEEK") {
                units = 7 * units;
            }

            startDate = Moment(endDate).clone().subtract(units, 'd');
        }

        if (definition.start_date_spec == "CURRENT") {
            let tmpEndDate = getMostRecentlyCompletedInterval(startDate, definition.interval || "DAY");
            startDate = getBeginningOfPeriod(tmpEndDate, definition.interval || "DAY");
        }

        if (definition.start_date_spec == "REPORT_DATE") {
            var interval = definition.timeInterval || definition.interval;
            startDate = Moment.utc(reportDate).startOf(INTERVAL_MAP[interval || "NONE"]);
        }

        return [startDate.format("YYYY-MM-DD"), endDate.format("YYYY-MM-DD")];
    } else {
        startDate = Moment.utc(definition.start_date);
        endDate = Moment.utc(definition.end_date);
    }

    return [startDate.format("YYYY-MM-DD"), endDate.format("YYYY-MM-DD")];

};

DateUtils.getNextPeriod = function (startDate, interval) {
    if ([CONSTANTS.NONE, CONSTANTS.DAY].indexOf(interval) >= 0) return startDate.clone().add(1, 'd');
    if (interval == CONSTANTS.WEEK) return startDate.clone().add(7, 'd');
    if (interval == CONSTANTS.MONTH) return null;
    if (interval == CONSTANTS.YEAR) return startDate.clone().add(365, "d");
};

DateUtils.getPreviousPeriod = function (startDate, interval) {
    if ([CONSTANTS.NONE, CONSTANTS.DAY].indexOf(interval) >= 0) return startDate.clone().subtract(1, "d");
    if (interval == CONSTANTS.WEEK) return startDate.clone().subtract(7, 'd');
    if (interval == CONSTANTS.MONTH) return null;
    if (interval == CONSTANTS.YEAR) return startDate.clone().subtract(365, 'd');
};


export default DateUtils;
