const Moment = require("moment");

let RangeUtils = {};

const OFFSET_REG = /([-|+][0-9]*[DWMY])/;
const DATE_REG = /([0-9]{4}-[0-9]{2}-[0-9]{2})/;
const CODE_REG = /^([{][A-Z_]*[}])/;

const NUMERALS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const CODES = {
    "{D_DATE}": "Document date",
    "{NOW}": "Today",
    "{END}": "End date",
    "{D_W_S}": "Document week start",
    "{D_W_E}": "Document week end",
    "{D_M_S}": "Document month start",
    "{D_M_E}": "Document month end",
    "{D_Y_S}": "Document year start",
    "{D_Y_E}": "Document year end",
    "{N_W_S}": "Current week start",
    "{N_W_E}": "Current week end",
    "{N_M_S}": "Current month start",
    "{N_M_E}": "Current month end",
    "{N_Y_S}": "Current year start",
    "{N_Y_E}": "Current year end",
    "{D_Y_S_ISO": "Document year start (ISO)",
    "{D_Y_E_ISO": "Document year end (ISO)"
};

const PRESETS_DEFAULT = {
    "-30D": ["{NOW}-30D", "{NOW}"],
    "-60D": ["{NOW}-60D", "{NOW}"],
    "-6M": ["{NOW}-6M", "{NOW}"],
    "-1Y": ["{NOW}-1Y", "{NOW}"],
    "-2Y": ["{NOW}-2Y", "{NOW}"],
    "-5Y": ["{NOW}-5Y", "{NOW}"],
    "-1D": ["{NOW}-1D", "{NOW}-1D"],
    "-2D": ["{NOW}-2D", "{NOW}-2D"],
    "-7D": ["{NOW}-7D", "{NOW}-7D"],
    "PREV_WEEK": ["{N_W_S}-1W", "{N_W_E}-1W"],
    "PREV_MONTH": ["{N_M_S}-1M", "{N_M_E}-1M"],
    "PREV_YEAR": ["{N_Y_S}-1Y", "{N_Y_E}-1Y"],
    "NOW": ["{NOW}", "{NOW}"],
    "CUR_WEEK": ["{N_W_S}", "{N_W_E}"],
    "CUR_MONTH": ["{N_M_S}", "{N_M_E}"],
    "CUR_YEAR": ["{N_Y_S}", "{N_Y_E}"],

    // TEMPLATE PRESETS
    "DD-30D": ["{D_DATE}-30D", "{D_DATE}"],
    "DD-60D": ["{D_DATE}-60D", "{D_DATE}"],
    "DD-6M": ["{D_DATE}-6M", "{D_DATE}"],
    "DD-1Y": ["{D_DATE}-1Y", "{D_DATE}"],
    "DD-2Y": ["{D_DATE}-2Y", "{D_DATE}"],
    "DD-5Y": ["{D_DATE}-5Y", "{D_DATE}"],
    "DD-1D": ["{D_DATE}-1D", "{D_DATE}-1D"],
    "DD-2D": ["{D_DATE}-2D", "{D_DATE}-2D"],
    "DD-7D": ["{D_DATE}-7D", "{D_DATE}-7D"],
    "D_PREV_WEEK": ["{D_W_S}-1W", "{D_W_E}-1W"],
    "D_PREV_MONTH": ["{D_M_S}-1M", "{D_M_E}-1M"],
    "D_PREV_YEAR": ["{D_Y_S}-1Y", "{D_Y_E}-1Y"],
    "D_DATE": ["{D_DATE}", "{D_DATE}"],
    "D_CUR_WEEK": ["{D_W_S}", "{D_W_E}"],
    "D_CUR_MONTH": ["{D_M_S}", "{D_M_E}"],
    "D_CUR_YEAR": ["{D_Y_S}", "{D_Y_E}"]
};

const PRESETS_TEMPLATE = {
    "-30D": ["{D_DATE}-30D", "{D_DATE}"],
    "-60D": ["{D_DATE}-60D", "{D_DATE}"],
    "-6M": ["{D_DATE}-6M", "{D_DATE}"],
    "-1Y": ["{D_DATE}-1Y", "{D_DATE}"],
    "-2Y": ["{D_DATE}-2Y", "{D_DATE}"],
    "-5Y": ["{D_DATE}-5Y", "{D_DATE}"],
    "-1D": ["{D_DATE}-1D", "{D_DATE}"],
    "-2D": ["{D_DATE}-2D", "{D_DATE}"],
    "-7D": ["{D_DATE}-7D", "{D_DATE}"],
    "PREV_WEEK": ["{D_W_S}-7D", "{D_W_E}-7D"],
    "PREV_MONTH": ["{D_M_S}-1M", "{D_M_E}-1M"],
    "PREV_YEAR": ["{D_Y_S}-1Y", "{D_Y_E}-1Y"],
    "NOW": ["{D_DATE}", "{D_DATE}"],
    "CUR_WEEK": ["{D_W_S}", "{D_W_E}"],
    "CUR_MONTH": ["{D_M_S}", "{D_M_E}"],
    "CUR_YEAR": ["{D_Y_S}", "{D_Y_E}"]
};

const INT_MARKERS = ["D", "W", "M", "Y"];
const DIGITS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const DIGIT_STRINGS = {
    D: "day(s)",
    W: "week(s)",
    M: "month(s)",
    Y: "year(s)"
};

RangeUtils.processPreset = function (preset) {
    return PRESETS_DEFAULT[preset];
};

RangeUtils.formatDateSpec = function (dateSpec) {
    let result = "";

    if (DATE_REG.test(dateSpec)) {
        let datePortion = dateSpec.match(DATE_REG)[0];
        let offset = RangeUtils.getOffset(dateSpec);

        if (offset == "") {
            return datePortion;
        }

        result = datePortion;

        if (offset.indexOf("-") >= 0) {
            result = result + " - ";
            offset = offset.replace("-", "");
        }

        if (offset.indexOf("+") >= 0) {
            result = result + " + ";
            offset = offset.replace("+", "");
        }

        let lastChar = offset.substr(offset.length - 1);
        let period;
        if (INT_MARKERS.indexOf(lastChar) >= 0) {
            period = DIGIT_STRINGS[lastChar];
            offset = offset.replace(lastChar, "");
        } else {
            period = "day(s)";
        }

        result = result + " " + offset + " " + period;
        return result;

    } else if (CODE_REG.test(dateSpec)) {
        let codePortion = dateSpec.match(CODE_REG)[0];
        let offset = RangeUtils.getOffset(dateSpec);

        result = CODES[codePortion];

        if (offset == "") {
            return result;
        }

        if (offset.indexOf("-") >= 0) {
            result = result + " - ";
            offset = offset.replace("-", "");
        }

        if (offset.indexOf("+") >= 0) {
            result = result + " + ";
            offset = offset.replace("+", "");
        }

        let lastChar = offset.substr(offset.length - 1);
        let period;
        if (INT_MARKERS.indexOf(lastChar) >= 0) {
            period = DIGIT_STRINGS[lastChar];
            offset = offset.replace(lastChar, "");
        } else {
            period = "day(s)";
        }

        result = result + " " + offset + " " + period;
        return result;
    }

    return dateSpec;
};


/**
 * Check if the string is a date
 * @param dateSpec
 * @returns {boolean}
 */
RangeUtils.isDate = function (dateSpec) {
    return DATE_REG.test(dateSpec);
};

RangeUtils.getDate = function (dateSpec) {
    return dateSpec.match(DATE_REG)[0];
};

/**
 * Determine if a date specification contains a code
 * @param dateSpec
 * @returns {boolean}
 */
RangeUtils.hasCode = function (dateSpec) {
    let hasCode = false;

    for (let i in CODES) {
        if (dateSpec.indexOf(i) >= 0) hasCode = true;
    }

    return hasCode;
};

RangeUtils.getCode = function (dateSpec) {
    let code;

    for (let i in CODES) {
        if (dateSpec.indexOf(i) >= 0) code = i;
    }

    code = code.replace("{", "");
    code = code.replace("}", "");

    return code;
};

/**
 * Test if a string contains an offset
 * @param dateSpec
 * @returns {boolean}
 */
RangeUtils.hasOffset = function (dateSpec) {
    let result = OFFSET_REG.test(dateSpec); // We don't have a full offset, but we might have a partial

    return result;
};

/**
 * Get the offset out of a string spec
 * @param dateSpec
 * @returns {string}
 */
RangeUtils.getOffset = function (dateSpec) {
    let result = "";

    if (DATE_REG.test(dateSpec)) {
        // This has a date in it, let's get it and strip it
        let tested = dateSpec.match(DATE_REG);
        result = dateSpec.replace(tested[0], "");
        return result;
    } else if (CODE_REG.test(dateSpec)) {
        let codeTest = dateSpec.match(CODE_REG);
        result = dateSpec.replace(codeTest[0], "");
        return result;
    }
};


/**
 * Apply an offset to the date spec
 * @param dateSpec
 * @param newOffset
 * @returns {*}
 */
RangeUtils.applyOffset = function (dateSpec, newOffset) {
    let curOffset = RangeUtils.getOffset(dateSpec);

    if (curOffset == "") {
        return dateSpec + newOffset;
    } else {
        if (DATE_REG.test(dateSpec)) {
            let origDate = dateSpec.match(DATE_REG)[0];
            return origDate + newOffset;
        } else if (CODE_REG.test(dateSpec)) {
            let origCode = dateSpec.match(CODE_REG)[0];
            return origCode + newOffset;
        }
    }

    return dateSpec;
};

RangeUtils.parseCode = function (code, reportDate, endDate) {
    switch (code) {
        case "{NOW}":
            return Moment.utc().format("YYYY-MM-DD");
        case "{D_DATE}":
            return Moment.utc(reportDate).format("YYYY-MM-DD");
        case "{END}":
            return endDate;
        case "{D_W_S}":
            return Moment.utc(reportDate).clone().startOf("isoweek").format("YYYY-MM-DD");
        case "{D_W_E}":
            return Moment.utc(reportDate).clone().endOf("isoweek").format("YYYY-MM-DD");
        case "{D_M_S}":
            return Moment.utc(reportDate).clone().startOf("month").format("YYYY-MM-DD");
        case "{D_M_E}":
            return Moment.utc(reportDate).clone().endOf("month").format("YYYY-MM-DD");
        case "{D_Y_S}":
            return Moment.utc(reportDate).clone().startOf("year").format("YYYY-MM-DD");
        case "{D_Y_E}":
            return Moment.utc(reportDate).clone().endOf("year").format("YYYY-MM-DD");
        case "{N_W_S}":
            return Moment.utc().startOf("isoweek").format("YYYY-MM-DD");
        case "{N_W_E}":
            return Moment.utc().endOf("isoweek").format("YYYY-MM-DD");
        case "{N_M_S}":
            return Moment.utc().startOf("month").format("YYYY-MM-DD");
        case "{N_M_E}":
            return Moment.utc().endOf("month").format("YYYY-MM-DD");
        case "{N_Y_S}":
            return Moment.utc().startOf("year").format("YYYY-MM-DD");
        case "{N_Y_E}":
            return Moment.utc().endOf("year").format("YYYY-MM-DD");
        case "{D_Y_S_ISO}":
            return Moment.utc(reportDate).isoWeek(1).endOf('isoweek').format('YYYY-MM-DD');
        case '{D_Y_E_ISO}':
            let weeksInYear = Moment.utc(reportDate).isoWeeksInYear();
            return Moment.utc(reportDate).isoWeek(weeksInYear).endOf('isoweek').format('YYYY-MM-DD');
        default:
            return Moment.utc().format("YYYY-MM-DD");
    }
};

RangeUtils.getOperator = function (offset) {
    if (offset.indexOf("-") >= 0) {
        return "-";
    }

    if (offset.indexOf("+") >= 0) {
        return "+";
    }

    return "+";
};

RangeUtils.getRawOffset = function (offset) {
    let result = offset.replace("-", "");
    result = result.replace("+", "");
    result = result.replace("D", "");
    result = result.replace("W", "");
    result = result.replace("M", "");
    result = result.replace("Y", "");

    return result;
};

RangeUtils.getInterval = function (offset) {
    let result = offset.replace("-", "");
    result = result.replace("+", "");

    for (var i in NUMERALS) {
        result = result.replace(NUMERALS[i], "");
    }

    return result;
};

RangeUtils.getAsDays = function (count, period) {
    if (period == "D") return count;
    if (period == "W") return count * 7;
    if (period == "M") return count * 30;
    if (period == "Y") return count * 365;
};

/**
 * Process a date spec into an actual date
 */
RangeUtils.process = function (dateSpec, reportDate, endDate) {
    let code, offset, spec;

    let resultDate;

    if (CODE_REG.test(dateSpec)) {
        code = dateSpec.match(CODE_REG)[0];
    }

    if (DATE_REG.test(dateSpec)) {
        spec = dateSpec.match(DATE_REG)[0];
    }

    if (OFFSET_REG.test(dateSpec)) {
        offset = dateSpec.match(OFFSET_REG)[0];
    }

    if (!code && !offset) return spec;

    if (code && !offset) {
        return RangeUtils.parseCode(code, reportDate)
    }

    if (code && offset) {
        let result = RangeUtils.parseCode(code, reportDate, endDate);
        let operator = RangeUtils.getOperator(offset);
        let rawValue = RangeUtils.getRawOffset(offset);
        let interval = RangeUtils.getInterval(offset);

        let calcOffsetDays = RangeUtils.getAsDays(rawValue, interval);

        let recResult;
        if (operator == "-") recResult = Moment.utc(result).subtract(parseInt(calcOffsetDays), "d");
        if (operator == "+") recResult = Moment.utc(result).add(parseInt(calcOffsetDays), "d");

        return recResult.format("YYYY-MM-DD");
    }

    if (offset && spec) {
        let operator = RangeUtils.getOperator(offset);
        let rawValue = RangeUtils.getRawOffset(offset);
        let interval = RangeUtils.getInterval(offset);

        let calcOffsetDays = RangeUtils.getAsDays(rawValue, interval);

        let recResult;
        if (operator == '-') recResult = Moment.utc(spec).subtract(parseInt(calcOffsetDays), 'd');
        if (operator == '+') recResult = Moment.utc(spec).add(parseInt(calcOffsetDays), 'd');

        return recResult.format('YYYY-MM-DD');
    }

};

RangeUtils.getDaysBetween = function (startDate, endDate) {
    let resultantDates = [startDate];

    let cur_date = Moment.utc(startDate).clone();
    let end_date = Moment.utc(endDate).clone();

    while (cur_date.isBefore(end_date, "d")) {
        let newDate = cur_date.clone().add(1, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;
};

RangeUtils.getWeeksBetween = function (startDate, endDate) {
    let resultantDates = [Moment.utc(startDate).clone()];

    let cur_date = startDate.clone();
    let end_date = Moment.utc(endDate).clone();

    while (cur_date.isBefore(end_date, "day")) {
        let newDate = cur_date.clone().add(7, "d");
        resultantDates.push(newDate);
        cur_date = newDate;
    }

    return resultantDates;
};

RangeUtils.getDatesInRange = function (startDate, endDate, interval) {
    if (interval == "DAY") return RangeUtils.getDaysBetween(startDate, endDate);
    if (interval == "WEEK") return RangeUtils.getWeeksBetween(startDate, endDate);

};

const INTERVAL_REMAP = {
    "DAY": "D",
    "WEEK": "W",
    "MONTH": "M",
    "YEAR": "Y"
};
/**
 * Convert a legacy time period to the new format
 * @param config
 */
RangeUtils.convertLegacy = function (config) {
    let startDate;
    let endDate;

    if (config.start_date_spec) {
        if (config.start_date_spec == "REPORT_DATE") {
            startDate = "{D_DATE}";
        } else if (config.start_date_spec == "MANUAL") {
            startDate = config.start_date;
        } else if (config.start_date_spec == "CURRENT") {
            startDate = "{TODAY}";
        } else if (config.start_date_spec == "END_SUBTRACTION") {
            startDate = "{END}-" + config.start_intervals + INTERVAL_REMAP[config.timeInterval || config.interval];
        }
    } else {
        startDate = config.start_date;
    }

    if (config.end_date_spec) {
        if (config.end_date_spec == "REPORT_DATE") {
            endDate = "{D_DATE}";
        } else if (config.end_date_spec == "MANUAL") {
            endDate = config.end_date;
        } else if (config.end_date_spec == "CURRENT") {
            endDate = "{NOW}";
        } else if (config.end_date_spec == "REPORT_DATE_SUBTRACTION") {
            endDate = "{D_DATE}-" + config.end_intervals + INTERVAL_REMAP[config.timeInterval || config.interval];
        }
    } else {
        endDate = config.end_date;
    }

    return [startDate, endDate];

};

export default RangeUtils;
