import utils from "./utils";

const AnalysisUtils = {};
const EXAMPLE = [
    [{
        "mode": "DASHBOARD",
        "spec": 2,
        "type": "RAW",
        "title": {"en": "Measles cases"},
        "format": "0",
        "series": [],
        "interval": "WEEK",
        "loc_spec": "SPECIFIC",
        "location": "df622134-ccf9-4ea3-a09b-8fbcf52c07df",
        "indicator": "b93d2d85-4645-45af-8c0d-f7c5f7b6c62a",
        "reduction": "SUM",
        "widgetIcon": "fa-circle-o",
        "source_type": "SLICE",
        "end_date_spec": "CURRENT",
        "widgetSubText": "Cumulative cases (past 4 weeks)",
        "additionalClass": null,
        "start_date_spec": "END_SUBTRACTION",
        "start_intervals": "4",
        "widgetBaseColor": "#bfe4f7",
        "widgetFooterText": "Source: Weekly IDSR Reporting Form",
        "widgetHighlightColor": "#2aa2de"
    }, {
        "mode": "DASHBOARD",
        "spec": 2,
        "type": "RAW",
        "title": {"en": "Measles cases"},
        "format": "0",
        "series": [],
        "interval": "WEEK",
        "loc_spec": "SPECIFIC",
        "location": "df622134-ccf9-4ea3-a09b-8fbcf52c07df",
        "indicator": "b93d2d85-4645-45af-8c0d-f7c5f7b6c62a",
        "reduction": "SUM",
        "widgetIcon": "fa-circle-o",
        "source_type": "SLICE",
        "end_date_spec": "CURRENT",
        "widgetSubText": "Number of cases (past week)",
        "additionalClass": null,
        "start_date_spec": "CURRENT",
        "widgetBaseColor": "#bfe4f7",
        "widgetFooterText": "Source: Weekly IDSR Reporting Form",
        "widgetHighlightColor": "#2aa2de"
    }, {
        "mode": "DASHBOARD",
        "spec": 2,
        "type": "RAW",
        "title": {"en": "Measles Deaths"},
        "format": "0",
        "series": [],
        "interval": "WEEK",
        "loc_spec": "SPECIFIC",
        "location": "df622134-ccf9-4ea3-a09b-8fbcf52c07df",
        "indicator": "1302c548-b35c-4959-94dd-7d4131471be4",
        "reduction": "SUM",
        "widgetIcon": "fa-circle-o",
        "source_type": "SLICE",
        "end_date_spec": "CURRENT",
        "widgetSubText": "Number of deaths (past week)",
        "additionalClass": null,
        "start_date_spec": "CURRENT",
        "widgetBaseColor": "#bfe4f7",
        "widgetFooterText": "Source: Weekly IDSR Reporting Form",
        "widgetHighlightColor": "#2aa2de"
    }, {
        "mode": "DASHBOARD",
        "spec": 2,
        "type": "RAW",
        "title": {"en": "Measles Alerts"},
        "format": "0",
        "series": [],
        "interval": "WEEK",
        "loc_spec": "SPECIFIC",
        "location": "df622134-ccf9-4ea3-a09b-8fbcf52c07df",
        "indicator": {
            "uuid": "a18b5f1f-4775-4bc9-89cf-b0d83abab45a",
            "state": "OPEN",
            "alarm_id": "b65e76d9-e085-45ee-8a5c-285e9e387f97",
            "definition": {}
        },
        "reduction": "SUM",
        "widgetIcon": "fa-bell-o",
        "source_type": "SLICE",
        "end_date_spec": "CURRENT",
        "widgetSubText": "Number of active alerts (past week)",
        "additionalClass": null,
        "start_date_spec": "CURRENT",
        "widgetBaseColor": "#bfe4f7",
        "widgetFooterText": "Source: Weekly IDSR Reporting Form",
        "widgetHighlightColor": "#2aa2de"
    }], [{
        "mode": "DASHBOARD",
        "spec": 2,
        "type": "SERIES",
        "zoom": false,
        "stack": false,
        "title": {"en": "Trend in measles cases over time"},
        "tools": false,
        "offset": 0,
        "series": [{
            "type": "SERIES",
            "zoom": false,
            "style": "line",
            "title": {"en": "Measles cases"},
            "tools": false,
            "colour": "#2aa2de",
            "series": [],
            "animate": false,
            "loc_spec": "SPECIFIC",
            "location": "d8ec3f57-c33d-429b-8a0c-be848012d28e",
            "indicator": "b93d2d85-4645-45af-8c0d-f7c5f7b6c62a",
            "navigator": false,
            "line_width": "2",
            "show_title": true,
            "source_type": "SERIES",
            "y_axis_label": "Number",
            "marker_radius": 2,
            "marker_symbol": "circle",
            "y_axis_format": "0",
            "legend_enabled": false,
            "y_axis_allow_decimals": false
        }],
        "animate": false,
        "end_date": "2016-11-25T09:44:22.226Z",
        "interval": "WEEK",
        "position": "bottom",
        "navigator": false,
        "precision": -1,
        "show_title": false,
        "start_date": "2016-08-22",
        "title_bold": true,
        "date_format": "YYYY-MM-DD",
        "max_y_value": null,
        "show_legend": true,
        "title_colour": "#000000",
        "y_axis_label": "Number",
        "end_date_spec": "CURRENT",
        "number_format": "0,0.00",
        "reverse_order": false,
        "start_on_axis": false,
        "y_axis_format": "0",
        "category_title": null,
        "legend_enabled": false,
        "title_rotation": 0,
        "start_date_spec": "MANUAL",
        "title_font_size": 14,
        "y_tick_interval": null,
        "legend_font_size": 11,
        "widgetFooterText": "Source: Weekly IDSR Reporting Form",
        "percent_precision": 2,
        "y_axis_allow_decimals": false
    }, {
        "mode": "DASHBOARD",
        "type": "MAP",
        "title": {"en": "Map of measles cases"},
        "width": "100%",
        "format": "0",
        "height": "400px",
        "period": ["{N_W_S}-1W", "{N_W_E}-1W"],
        "default": {
            "type": "MAP",
            "width": "100%",
            "height": 300,
            "end_date": "2016-11-24T13:01:44.440Z",
            "indicator": null,
            "reduction": "SUM",
            "start_date": "2016-11-24T13:01:44.439Z"
        },
        "interval": "WEEK",
        "location": "df622134-ccf9-4ea3-a09b-8fbcf52c07df",
        "indicator": "b93d2d85-4645-45af-8c0d-f7c5f7b6c62a",
        "reduction": "SUM",
        "loc_source": "TYPE",
        "thresholds": [["0", "0", "#d2d7da"], ["1", "1", "#bfe4f7"], ["2", "10", "#7cc5ed"], ["11", "20", "#2aa2de"]],
        "source_type": "SLICE",
        "site_type_id": 18
    }
    ]
];


const EXAMPLE_OUT = {
    "definition": [{
        "w": 11,
        "h": 6,
        "x": 0,
        "y": 0,
        "i": "57f30d19-1c47-4792-ac9d-b89c90157ff3",
        "moved": false,
        "static": false
    }],
    "widgets": {
        "57f30d19-1c47-4792-ac9d-b89c90157ff3": {
            "_": "1b0babcce90b",
            "g": [{"_": "09c33343368b", "label": "New Group"}],
            "n": [{
                "t": "M",
                "n": "RECORDS",
                "c": {"id": null},
                "g": "09c33343368b",
                "_": "c1a4c773b18d",
                "agg": "SUM",
                "p": null,
                "_t": null,
                "f": [],
                "x": null,
                "fid": null
            }],
            "title": "",
            "style": {},
            "type": "DATAPOINT"
        }
    }
};

const MAPPING = {
    RAW: "DATAPOINT",
    SERIES: "DEFAULT",
    CATEGORY: "PIE"
};


const ALERTS_FIELD_MAP = {
    ALERTS_TRIGGERED: null,
    ALERTS_OPEN: {
        t: "D",
        _t: "F",
        n: "ALERT_STATUS",
        f: [
            "EQ:OPEN"
        ]
    },
    ALERTS_CLOSED: {
        t: "D",
        _t: "F",
        n: "ALERT_STATUS",
        f: [
            "EQ:CLOSED",
            "EQ:AUTODISCARDED"
        ]
    },
    ALERTS_CLOSED_NON_AUTO: {
        t: "D",
        _t: "F",
        n: "ALERT_STATUS",
        f: [
            "EQ:CLOSED"
        ]
    },
    ALERTS_AUTO_DISCARDED: {
        t: "D",
        _t: "F",
        n: "ALERT_STATUS",
        f: [
            "EQ:AUTODISCARDED"
        ]
    },
    ALERTS_DISCARDED: {
        t: "D",
        _t: "F",
        n:"FIELD.data.verification_outcome",
        f: ["EQ:DISCARD"]
    },
    ALERTS_MONITORED: {
        t: "D",
        _t: "F",
        n: "FIELD.data.verification_outcome",
        f: ["EQ:MONITOR"]
    },
    ALERTS_RESPOND: {
        t: "D",
        _t: "F",
        n: "FIELD.data.verification_outcome",
        f: ["EQ:RESPOND"]
    },
    ALERTS_IN_VERIFICATION: {
        t: "D",
        _t: "F",
        n: "ALERT_STAGE",
        f: [
            "EQ:VERIFICATION"
        ]
    },
    ALERTS_AWAIT_VERIFICATION: {
        t: "D",
        _t: "F",
        n: "FIELD.data.verification_outcome",
        f: [
            "NULL"
        ]
    },
    ALERTS_VERIFIED: {
        t: "D",
        _t: "F",
        n: "FIELD.data.verification_outcome",
        f: ["NNULL"]
    },
    ALERTS_DISCARDED_VERIFICATION: {
        t: "D",
        _t: "F",
        n: "FIELD.data.verification_outcome",
        f: [
            "EQ:DISCARD"
        ]
    },
    ALERTS_MONITORED_VERIFICATION: {
        t: "D",
        _t: "F",
        n: "FIELD.data.verification_outcome",
        f: [
            "EQ:MONITOR"
        ]
    },
    ALERTS_IN_RISK_ASSESS: {
        t: "D",
        _t: "F",
        n: "ALERT_STAGE",
        f:[
            "EQ:RISK_ASSESS"
        ]
    },
    ALERTS_AWAIT_RISK_ASSESS: {
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f:["NULL"]
    },
    ALERTS_RISK_ASSESSED:{
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f: ["NNULL"]
    },
    ALERTS_IN_RISK_CHAR: {
        t: "D",
        _t: "F",
        n: "ALERT_STAGE",
        f: ["EQ:RISK_CHAR"]
    },
    ALERTS_AWAIT_RISK_CHAR: {
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f: ["NULL"]
    },
    ALERTS_RISK_CHAR: {
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f: ["NNULL"]
    },
    ALERTS_IN_OUTCOME: {
        t: "D",
        _t: "F",
        n: "ALERT_STAGE",
        f: ["EQ:OUTCOME"]
    },
    ALERTS_AWAIT_OUTCOME: {
        t: "D",
        _t: "F",
        n: "FIELD.data.outcome",
        f: ["NULL"]
    },
    ALERTS_OUTCOME: {
        t: "D",
        _t: "F",
        n: "FIELD.data.outcome",
        f: ["NNULL"]
    },
    ALERTS_DISCARD_OUTCOME: {
        t: "D",
        _t: "F",
        n: "FIELD.data.outcome",
        f: ['EQ:DISCARD']
    },
    ALERTS_MONITORED_OUTCOME: {
        t: "D",
        _t: "F",
        n: "FIELD.data.outcome",
        f: ["EQ:MONITOR"]
    },
    ALERTS_RESPOND_OUTCOME: {
        t: "D",
        _t: "F",
        n: "FIELD.data.outcome",
        f: ["EQ:RESPOND"]
    },
    ALERTS_RISK_LOW: {
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f: ["EQ:LOW"]
    },
    ALERTS_RISK_MODERATE: {
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f: ["EQ:MODERATE"]
    },
    ALERTS_RISK_HIGH: {
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f: ["EQ:HIGH"]
    },
    ALERTS_RISK_SEVERE: {
        t: "D",
        _t: "F",
        n: "FIELD.data.risk",
        f: ["EQ:SEVERE"]
    }
};

const _updateIndicator = (ind) => {
    let rootNode = {
        _: utils.uuid(),
        t: "M",
        n: null
    };
    let filters = [];
    // Form
    if (ind.uuid == "08f844e7-62d2-4c8d-81dd-41b10808783c") {
        // REPORTING LOCATIONS,
        rootNode.n = "LOCATIONS";

        filters.push({
            _: utils.uuid(),
            n: "FORM",
            p: rootNode._,
            _t: "F",
            f: ["EQ:" + ind.form_id]
        })
    }

    // Form Submissions
    if (ind.uuid == 'e51f4ca7-1678-44e3-b5cb-64f84555696f') {
        // SUBMISSIONS, COMPLETENESS, TIMELINESS
        rootNode.n = ind.dimesion;

        if (ind.form_id) {
            filters.push({
                _: utils.uuid(),
                t: "D",
                n: "FORM",
                p: rootNode._,
                f: [
                    "EQ:" + ind.form_id
                ]
            })
        }

        if (ind.source) {
            filters.push({
                _: utils.uuid(),
                t: "D",
                n: "SOURCE",
                _t: "F",
                p: rootNode._,
                f: ["EQ:" + ind.source]
            })
        }

        if (ind.organization_id) {
            filters.push({
                _: utils.uuid(),
                t: "D",
                n: "ORG",
                _t: "F",
                p: rootNode._,
                f: ["EQ:" + ind.organization_id]
            })
        }
    }

    // Alerts
    if (ind.uuid= 'a18b5f1f-4775-4bc9-89cf-b0d83abab45a') {
        // alarm_id
        // ALERTS_V
        rootNode = {
            _: utils.uuid(),
            n: "ALERTS",
            t: "M"
        };

        if (ind.alarm_id) {
            filters.push({
                t: "D",
                _: utils.uuid(),
                n: "ALARM",
                _t: "F",
                p: rootNode._,
                f: [
                    "EQ:" + ind.alarm_id
                ]
            })
        }
        let filt = utils.copy(ALERTS_FIELD_MAP[ind.dimension]);
        filt._ = utils.uuid();
        filt.p = rootNode._;
        filters.push(filt);
    }

    // Assignments

    // Devices

    // Tasks

    // Forms

    // Users


    return [rootNode, filters];
};

const _getPeriodicity = (data) => {
    if (data.period) {
    } else {
        let start_date_spec = data.start_date_spec;
        let start_date = data.start_date;
        let end_date_spec = data.end_date_spec;
        let end_date = data.end_date;
    }
};

const _getLocation = (data) => {

};

AnalysisUtils.upgradeWidget = (uuid, data) => {
    let groupId = utils.uuid();

    let result = {
        _: uuid,
        g: [{"_": groupId, label: "New Group"}],
        n: [],
        title: data.title || "No title",
        style: {},
        type: MAPPING[data.type]
    };

    switch(data.type) {
        case "RAW":
            let node;
            if (typeof data.indicator === 'object') {
                // We potentially have additional filters to apply to this imte
                let result = _updateIndicator(data.indicator);
                node = result[0];
                others = result[1];
            } else {
                node = {
                    _: utils.uuid(),
                    t: "M",
                    g: result.g[0]._,
                    n: "INDICATOR",
                    i: data.indicator
                };
            }

            let others = [];
            if (data.period) {
                others.push({
                    _: utils.uuid(),
                    c: {type: "DATE"},
                    n: "RECORD_DATE",
                    p: node._,
                    t: "D",
                    _t: "F",
                    f: [data.period]
                })
            }
            if (data.location) {
                others.push({
                    _: utils.uuid(),
                    n: "LOCATION",
                    p: node._,
                    t: "D",
                    _t: "F"
                })
            }
            others.push(node);
            result.n = others;
            break;
        case "SERIES":
            data.series.forEach(item => {
                let group = {_: utils.uuid(), label: item.title};
            });
            break;
        case "CATEGORY":
        default:
            break;
    }



    return result;
};

AnalysisUtils.upgrade = (data) => {
    let layout = [];

    let widgets = {};

    let rowIndex = 1;
    let ColumnIndex = 1;
    data.layout.forEach(row => {
        let itemWidth = 36 / row.length;
        row.forEach((cell, index) => {
            let uuid = cell.uuid || utils.uuid();
            layout.push({
                w: itemWidth,
                h: 6,
                x: index * itemWidth,
                y: rowIndex,
                i: uuid,
                moved: false,
                static: false
            })
            widgets[uuid] = cell;
        });
        rowIndex++;

    });

    return {
        ...data,
        layout: layout,
        data: widgets
    };
};

export default AnalysisUtils;
