import React from "react";
import ReactDOM from "react-dom";

import ClientContext from "./contexts/ClientContext";

import "./css/theme.less";

import ViewLocationReporting from "./views/view.location.reporting";
import ViewGuidance from "./views/view.guidance";
import ViewLog from "./views/view.log.jsx";
import ViewDefault from "./views/view.default";
import ViewRecord from "./views/view.record";
import ViewAlert from "./views/view.alert";
import ViewReporting from "./views/view.reporting.grid.jsx";
import ViewAlerts from "./views/view.alert";
import ViewDashboard from "./views/view.dashboard";
import ViewGrid from "./views/view.grid";
import ViewTasks from "./views/view.tasks";
import ViewTask from "./views/view.task";
import ViewDrafts from "./views/view.drafts";
import ViewAccounts from "./views/view.accounts.jsx";

import Shade from "./controls/c.shade.jsx";

import FootBar from "./controls/c.footbar";

let remote = {};
let state = {
    on: () => {

    },

    off: () => {

    }
};
window.state = state;


const TABS = {
    LOCATION_REPORTING: ViewLocationReporting,
    GUIDANCE: ViewGuidance,
    LOG: ViewLog,
    DEFAULT: ViewDefault,
    RECORD: ViewRecord,
    ALERT: ViewAlert,
    RECORDS: ViewReporting,
    ALERTS: ViewAlerts,
    DASHBOARD: ViewDashboard,
    DRAFTS: ViewDrafts,
    GRID: ViewGrid,
    TASKS: ViewTasks,
    TASK: ViewTask,
};

const ICONS = {
    LOCATION_REPORTING: "fal fa-map-marker",
    GUIDANCE: "fal fa-graduation-cap",
    KNOWLEDGEBASE: "fal fa-graduation-cap",
    LOG: "fal fa-log",
    DEFAULT: "fal fa-home",
    RECORD: "fal fa-clipboard",
    ALERT: "fal fa-bell",
    ALERTS: "fal fa-bell",
    ROLE: "fal fa-user",
    DASHBOARD: "fal fa-tachometer",
    RECORDS: "fal fa-database",
    TASKS: "fal fa-tasks",
    TASK: "fal fa-check-square"
}

const tx = (cmd, args, callback, errcallb) => {
    fetch("/api/_w", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, cors, *same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json",
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        redirect: "follow", // manual, *follow, error
        referrer: "no-referrer", // no-referrer, *client
        body: JSON.stringify([cmd, args]), // body data type must match "Content-Type" header
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            callback(data.data)
        })
        .catch(err => {
            errcallb(err)
        });
}
window.tx = tx;

const __ = (fal) => {
    return fal;
}

window.__ = __;

class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            account: null,
            lang: "en",
            connection: false,
            card: null
        }
    }

    componentDidMount() {
        window.tx("com.sonoma.self", null, (res) => {
            window.user = res;
            this.setState({
                user: res,
            })
        }, (err) => {
            console.log(err)
        });
    }

    render() {
        if (!this.state.user) {
            return (
                <p>Loading...</p>
            )
        }
        return (
            <ClientContext.Provider value={{
                ...this.state
            }}>
                <ViewWrapperMain {...this.state}/>
            </ClientContext.Provider>
        )
    }
}

class ViewWrapperMain extends React.Component {
    static contextType = ClientContext;
    constructor(props) {
        super(props);

        this.state = {
            tabs: [{_id: "DEFAULT", type: "DEFAULT"}],
            activeTab: "DEFAULT",
            view: "DEFAULT",
            shade: null
        };
    }

    componentDidMount() {
        window.addEventListener("AddTab", this._addTab, false);
        window.addEventListener("RemoveTab", this._removeTab, false);
        window.addEventListener("SetActiveTab", this._setTab, false);
        window.addEventListener("AddShade", this._setShade,false);
    }

    _setShade = (data) => {
        this.setState({
            shade: data.detail
        });
    };

    _addTab = (data) => {
        let newTab = {
            ...data.detail,
            _id: String((new Date()).getTime() / 1000)
        }

        let tabs = this.state.tabs;
        tabs.push(newTab);
        this.setState({
            tabs,
            activeTab: newTab._id
        });
    };

    _removeTab = (id) => {
        let tabs = this.state.tabs.filter(item => {
            return item._id != id;
        });
        let activeTab = this.state.activeTab == id ? "DEFAULT" : this.state.activeTab;

        this.setState({
            tabs,
            activeTab,
        });
    }

    _setTab = (id) => {
        this.setState({activeTab: id});
    };

    _change = (data) => {
        this.setState(state);
    };

    _loginSuccess = (data) => {
        state.seedAccount(data);
    };

    _onSeedComplete = () => {
        state.setAccountSeeded();
    };

    _getTab = () => {
        if (this.state.activeTab != "DEFAULT") {
            let tabConfig = this.state.tabs.filter(item => {
                return item._id == this.state.activeTab;
            })[0] || null;
            return tabConfig;
        }
        return null
    };

    render() {
        let view;

        let tabConfig = this._getTab()
        let ViewCmp;
        if (tabConfig) {
            console.log(tabConfig);
            ViewCmp = TABS[tabConfig.type];
            view = <ViewCmp config={tabConfig} key={tabConfig._id}/>;
        } else {
            view = <ViewDefault role={this.context.user.role}/>;
        }

        let card;
        /**
        if (window.state.userCard) {
            card = <cards.UserCard uuid={window.state.userCard}/>;
        }

        if (window.state.locationCard) {
            card = <cards.LocationCard data={window.state.locationCard}/>;
        }

        if (window.state.assignmentCard) {
            card = <cards.AssignmentSelector data={window.state.assignmentCard}/>;
        }

        if (window.state.accountsCard) {
            card = <cards.AccountsCard/>;
        }

        if (window.state.connectionCard) {
            card = <cards.ConnectionCard/>;
        }
         **/

        let tabWidth = "20%";

        if (this.state.tabs.length > 5) {
            tabWidth = (100 / this.state.tabs.length) + "%";
        } 

        return (
            <div className="column">
                <div className="row app-header">
                    {this.state.tabs.map(item => {
                        let label = item.name || "";
                        return (
                            <div className={"main-tab" + (this.state.activeTab == item._id ? " active" : "")}
                                 style={{minWidth: tabWidth, maxWidth: tabWidth}}
                                 onClick={() => this._setTab(item._id)}>
                                <div className="icon">
                                    <i className={ICONS[item.type] || "fal fa-file"}></i>
                                </div>
                                <div className="label">{label}</div>
                                {item._id != 'DEFAULT' ?
                                    <div className="closer" onClick={(e) => {this._removeTab(item._id)}}>
                                        <i className="fal fa-times"></i>
                                    </div>
                                    : null}
                            </div>
                        )
                    })}

                </div>
                {view}

                <FootBar/>
                <ViewAccounts
                    hideAccounts={() => {
                        state.hideAccounts();
                    }}
                    visible={this.state.showAccounts}/>
                {card}
                {this.state.shade ?
                <Shade data={this.state.shade}/>
                        : null}
            </div>
        )
    }
}

export default ViewWrapperMain;

ReactDOM.render(
    <Main/>,
    document.getElementById("application")
);
