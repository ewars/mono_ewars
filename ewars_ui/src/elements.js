import React from "react";
import { styled, withStyle } from "styletron-react";

const Button = styled("button", {
    display: "inline-block",
    background: "#F2F2F2",
    color: "rgba(0,0,0,0.9)",
    border: "1px solid rgba(0, 0, 0, 0.25)",
    boxShadow: "inset 0 -1px 0px rgba(0, 0, 0, 0.1)",
    padding: "8px 10px",
    borderRadius: "3px",
    textAlign: "center",
    transition: "all 0.5s ease",
    marginRight: "8px",
    ":focus": {
        outline: 0
    },
    ":hover": {
        background: "#484848",
        color: "#F2F2F2",
        boxShadow: "inset 0 1px 1px rgba(255, 255, 255, 0.4)"
    }
});

const BlockButton = withStyle(Button, {
    display: "block",
    marginRight: "0",
    marginTop: "4px",
    marginBottom: "4px"
});

const Column = styled("div", {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    position: "relative",
    minHeight: 0
});

const Row = styled("div", {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    minHeight: 0
});

const P = styled("p", (props) => {
    return {
        display: "block",
        fontSize: "1rem",
        margin: "8px 0",
        color: props.color || "#333333"
    }
});

const Panel = withStyle(Column, {
    overflowY: "scroll",
    display: "block"
});

const RowPanel = withStyle(Row, {
    overflowY: "scroll",
    display: "block"
});

const H2 = styled("h2", (props) => {
    return {
        display: "block",
        fontSize: "1.2rem",
        margin: "8px 0",
        color: props.color || "#333333"
    }
});

export {
    Button,
    BlockButton,
    Column,
    Row,
    P,
    Panel,
    RowPanel,
    H2
}
