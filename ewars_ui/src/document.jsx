import React from 'react';
import ReactDOM from "react-dom";

import AnalysisUtils from "./utils/analysis_utils";

import DefaultChart from "./viz/chart.default";
import BulletChart from "./viz/chart.bullet";
import CandleStickChart from "./viz/chart.candlestick";
import Funnel from "./viz/chart.funnel";
import Gauge from "./viz/chart.gauge";
import Heatmap from "./viz/chart.heatmap";
import Pie from "./viz/chart.pie";
import Pyramid from "./viz/chart.pyramid";
import Spark from "./viz/chart.spark";
import StreamGraph from "./viz/chart.streamgraph";
import Treemap from "./viz/chart.treemap";
import XRange from "./viz/chart.xrange";
import Map from "./viz/map.default";
import Table from "./viz/table.default";
import Pivot from "./viz/table.pivot";
import DataPoint from "./viz/other.datapoint";

const WIDGETS = {
    DEFAULT: DefaultChart,
    BULLET: BulletChart,
    CANDLESTICK: CandleStickChart,
    FUNNEL: Funnel,
    GAUGE: Gauge,
    HEATMAP: Heatmap,
    PIE: Pie,
    PYRAMID: Pyramid,
    SPARK: Spark,
    STREAM: StreamGraph,
    TREEMAP: Treemap,
    XRANGE: XRange,
    MAP: Map,
    TABLE: Table,
    PIVOT: Pivot,
    DATAPOINT: DataPoint
};

let _queued = [];
const _queue = () => {
    return new Promise((resolve, reject) => {
        resolve({});
    })
};

class Document {
    isLoading = true;
    widgets = [];
    widgetsLoaded = 0;
    widgetsTotal = 0;
    pEl = null;
    template = null;

    constructor(doc, template, report_date, location_id, pdf) {
        this._doc = doc;
        this.template = template;
        console.log(template);
        this.date = report_date;
        this.lid = location_id;
        this.pEl = doc.getElementById("progress-wrapper");
        this.pEl.style.display = "block";
        this.pdf = pdf || null;
        // this.report = window.dt.report != "None" ? JSON.parse(window.dt.report) : null;
        if (this.pdf) this.pEl.style.display = "none";
        // this.template = JSON.parse(window.dt.template) || null;
        // this.location = {name: JSON.parse(window.dt.location_name || {en: "No Location"}) || null};

        this.widgetsTotal = doc.querySelectorAll(".ewarschart").length;
        console.log(this.widgetsTotal);

        window.addEventListener("widgetloaded", () => {
            this.widgetsLoaded++;
            this._checkProgress();
        })

        this._renderWidgets()
    }

    init = () => {
    };

    _setProgress = (progress) => {
    };

    _load = () => {
    };

    _checkProgress = () => {
        let progress = (this.widgetsLoaded / this.widgetsTotal) * 100;
        this._setProgress(progress);

        if (this.widgetsLoaded >= this.widgetsTotal) {
            setTimeout(() => {
                if (this.pEl) {
                    this.pEl.style.display = "none";
                    this.pEl.style.width = "0";
                }
            }, 1000);

            // TODO: DAta download mapp

            window.dispatchEvent(new CustomEvent("prepdfprint"), {});
            window.dispatchEvent(new CustomEvent("widgetsloaded", {}));

            let flagged = this._doc.createElement("div");
            flagged.setAttribute("id", "uxflagged");
            this._doc.body.appendChild(flagged);

            window.status = "ALLPRESENT";
        }
    };

    _renderWidgets = () => {
        let items = this._doc.querySelectorAll(".ewarschart");
        if (items.length <= 0) {
            this.widgetsLoaded = this.widgetsTotal;
            this._checkProgress();
        } else {
            for (let i = 0; i < items.length; i++) {
                let node = items[i];
                let icon = this._doc.createElement("i");
                icon.setAttribute("class", "fal fa-cog fa-spin");
                node.appendChild(icon);

                this._widget(node)
                    .then(res => {

                    })
                    .catch(err => {
                        console.log(err);
                        let el = err[0];
                        if (el) {
                            el.innerHTML = "";
                            let icon = this._doc.createElement("i");
                            icon.setAttribute("class", 'fal fa-exclamation-triangle');
                            icon.style.color = "red";
                            el.appendChild(icon);
                        }
                        window.dispatchEvent(new CustomEvent("widgetloaded", {}));
                    })
            }
        }
    };

    _widget = (el) => {
        return new Promise((resolve, reject) => {
            if (!el) {
                reject([el, "NO_DOM"]);
                return;
            }

            let widgetId = el.getAttribute("data-id");

            if (!widgetId || widgetId == "") {
                reject([el, "NO_CONFIG"]);
                return;
            }

            let widgetConfig = this.template.data[widgetId] || null;
            if (!widgetConfig || widgetConfig == "" || widgetConfig == undefined) {
                reject([el, "NO_CONFIG"]);
                return;
            }

            if (!widgetConfig.type || widgetConfig.type == "") {
                reject([el, "NO_CONFIG"]);
                return;
            }
            console.log(widgetConfig);

            let widget,
                widgetFn = WIDGETS[widgetConfig.type];

            if (!widgetFn) {
                reject([el, "NO_WIDGET_" + widgetConfig.type]);
                return;
            }

            widget = new WIDGETS[widgetConfig.type](widgetConfig, el);
            this.widgets.push(widget);
            resolve();
        })
    };

    _download = () => {

    };

    _print = () => {

    };
}

export default Document;
