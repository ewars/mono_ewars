/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
    Button,
    TouchableHighlight,
    DrawerNavigationLayout,
    ViewPagerAndroid,
    DrawerLayoutAndroid,
    Dimensions
} from 'react-native';

import {createDrawerNavigator, createBottomTabNavigator} from "react-navigation";

import ViewAccounts from "./app/views/view.accounts";

class ViewHome extends React.Component {
    navigationOptions = {
        drawerLabel: "EWARS"
    }

    constructor(props) {
        super(props);
    }

    _test = () => {
        this.props.navigation.openDrawer();
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>Hello WOrkd</Text>
            <Button
            title="Open Drawer"
            onPress={this._test}/>
            </View>
        )
    }
}

class ViewMenuMain extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>MenuMain</Text>
            </View>
        )
    }
}

class ViewMenuAdmin extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>Hello WOrkd</Text>
            </View>
        )
    }
}

// Drawer Navigation
const DrawerNavigation = createBottomTabNavigator({
    Main: {
        screen: ViewMenuMain
    },
    Admin: {
        screen: ViewMenuAdmin
    }
});

// Root navigator
const AppNavigation = createDrawerNavigator({
    Home: {
        screen: ViewHome
    },
    Menu: {
        screen: DrawerNavigation
    }
});

var navigationView = (
    <ViewPagerAndroid
        initialPage={1}
        style={{flex: 1}}>
        <View style={{padding: 20, alignItems: "center"}} key="1">
            <ViewAccounts/>
        </View>
        <View style={{padding: 20, alignItems: "center"}} key="2">
            <Text>Page 2</Text>
        </View>
        <View style={{padding: 20, alignItems: "center"}} key="3">
            <Text>Page 3</Text>
        </View>
    </ViewPagerAndroid>
)


// Root rendering
export default class App extends React.Component {
    _drawer = () => {
       this.refs['DRAWER'].openDrawer()
    };

    render() {
        var {height, width} = Dimensions.get("window");
        let maxWidth = ((width / 100) * 90);
        return (
            <DrawerLayoutAndroid
                ref="DRAWER"
                drawerWidth={maxWidth}
                drawerLockMode="locked-open"
                drawerPosition={DrawerLayoutAndroid.positions.left}
                renderNavigationView={() => navigationView}>
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <TouchableHighlight onPress={this._drawer}>
                                <Text>Hello</Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.nav}>
                            <AppNavigation/>
                        </View>
                    </View>
            </DrawerLayoutAndroid>
        )
    }
}


const styles = StyleSheet.create({
    header: {
        flex: 1,
        height: 40,
        maxHeight: 60,
        borderBottomColor: "#DADADA",
        borderBottomWidth: 1
    },
  container: {
    flex: 1,
      flexDirection: "column",
    backgroundColor: '#F5FCFF'
  },
    nav: {
        flex: 1
    },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
