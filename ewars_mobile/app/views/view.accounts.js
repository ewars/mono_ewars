import React from "react";

import {
    View,
    TouchableHighlight,
    Text,
    StyleSheet,
    ScrollView
} from "react-native";

class AccountItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableHighlight onPress={() => {}}>
                <View style={{flex: 1, maxHeight: 60, height: 60, flexDirection: "row"}}>
                    <View style={{flex: 1}}>

                    </View>
                    <View style={{flex: 2}}>
                        <Text>Account Name</Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

class ViewAccounts extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <ScrollView style={{flex: 1}}>
                <AccountItem/>
                <AccountItem/>
            </ScrollView>
        )
    }
}


export default ViewAccounts;
