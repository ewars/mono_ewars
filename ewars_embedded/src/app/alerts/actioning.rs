use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use rusqlite::{Row};
use serde_json as json;
use serde_json::Value;

use chrono::prelude::*;
use uuid::Uuid;

use crate::state::ApplicationState;

#[derive(Debug)]
struct Alarm {
    pub uuid: String,
    pub workflow: Option<Value>,
}

#[derive(Debug, Deserialize, Serialize)]
struct Alert {
    uuid: String,
    alid: String,
    stage: String,
    status: String,
    data: Value,
    workflow: Value,
    events: Value,
    modified: String,
    modified_by: Option<String>,
    #[serde(default = "default_workflow")]
    alarm_workflow: Value,
}

// IMplement sane default for alarm_workflow in event it's empty
fn default_workflow() -> Value {
    let workflow = Workflow {
        definition: HashMap::new(),
        stages: Vec::new(),
    };
    json::to_value(workflow).unwrap()
}

#[derive(Debug, Serialize, Deserialize)]
struct WorkflowItem {
    uuid: String,
    uid: String,
    stage: String,
    modified: String,
    submitted: String,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
struct Route {
    conditions: Vec<(String, String, String)>,
    target: String,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
struct Stage {
    uuid: String,
    task: bool,
    permissions: Option<Value>,
    routes: Vec<Route>,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
struct Field {
    uuid: Option<String>,
    #[serde(rename = "type")]
    field_type: String,
    label: Value,
    options: Option<Vec<(String, String)>>,
    #[serde(default = "default_required")]
    required: bool,
}

#[derive(Debug, Deserialize, Serialize)]
struct Workflow {
    #[serde(default = "default_definition")]
    definition: HashMap<String, Field>,
    #[serde(default = "default_stages")]
    stages: Vec<Stage>,
}

fn default_definition() -> HashMap<String, Field> {
    HashMap::new()
}

fn default_stages() -> Vec<Stage> {
    Vec::new()
}

fn default_required() -> bool {
    false
}

// Evaluate a condition against a set of data.
fn evaluate_condition(data: &HashMap<String, Value>, condition: (String, String, String)) -> bool {
    let prop = condition.0.clone();
    let cmp = condition.1.clone();
    let val = condition.2.clone();

    let mut result: bool = false;

    let actual_val: Option<Value> = data.get(&prop).cloned();

    eprintln!("{}, {}, {}, {:?}", prop, cmp, val, actual_val);

    // If the value is None, then we check to see if the cmp is null,
    // if it is then we pass it back as true, else false
    if actual_val.is_none() {
        match cmp.as_ref() {
            "null" => {
                result = true;
            }
            _ => {
                result = false;
            }
        }

        // Return early as we have no value
        // to do other comparisons against anyway
        return result;
    }

    let cmp_val: String = match &actual_val {
        Some(ref res) => {
            let item: String = json::from_value(res.clone() ).unwrap();
            item
        }
        None => "".to_string()
    };

    let empty: String = String::new();

    match cmp.as_ref() {
        "eq" => {
            if cmp_val == val {
                result = true;
            }
        }
        "neq" => {
            if cmp_val != val {
                result = true;
            }
        }
        "gt" => {
            if cmp_val > val {
                result = true;
            }
        }
        "gte" => {
            if cmp_val >= val {
                result = true;
            }
        }
        "lt" => {
            if cmp_val < val {
                result = true;
            }
        }
        "lte" => {
            if cmp_val <= val {
                result = true;
            }
        }
        "nnull" => {
            if actual_val.is_some() {
                result = true;
            } else {
                if cmp_val == empty {
                    result = false;
                }
            }
        }
        "null" => {
            if actual_val.is_none() {
                result = true;
            } else {
                if cmp_val == empty {
                    result = false;
                }
            }
        }
        _ => {}
    };

    result
}

pub fn action_alert(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let alert_id: String = json::from_value(args.get(0).cloned().unwrap()).unwrap();
    let data: HashMap<String, Value> = json::from_value(args.get(1).cloned().unwrap()).unwrap();
    let uid: String = app.borrow().get_current_uuid().to_string();

    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let sql = r#"
        SELECT a.uuid, a.alid, a.stage, a.status, a.data, a.workflow, a.events, a.modified, a.modified_by,
                al.workflow AS alarm_workflow
        FROM alerts AS a
            LEFT JOIN alarms AS al ON al.uuid = a.alid
        WHERE a.uuid = ?;
    "#.to_owned();
    let alert: Alert = conn.query_row(&sql, &[&alert_id], |row| {
        Alert {
            uuid: row.get(0),
            alid: row.get(1),
            stage: row.get(2),
            status: row.get(3),
            data: row.get(4),
            workflow: row.get(5),
            events: row.get(6),
            modified: row.get(7),
            modified_by: row.get(8),
            alarm_workflow: row.get(9),
        }
    }).unwrap();

    let mut current_data: HashMap<String, Value> = json::from_value(alert.data.clone()).unwrap();
    let mut alert_workflow: Vec<Value> = json::from_value(alert.workflow.clone()).unwrap();
    let mut events: Vec<Value> = json::from_value(alert.events.clone()).unwrap();
    let mut alarm_workflow: Workflow = json::from_value(alert.alarm_workflow.clone()).unwrap();

    let mut next_state: String = String::new();

    if alarm_workflow.stages.is_empty() {
        // Theres no workflow associated with this alert, so we need to use the default one
        let stages: Vec<Stage> = vec![
            Stage {
                uuid: "VERIFICATION".to_string(),
                task: true,
                permissions: None,
                routes: vec![
                    Route {
                        conditions: vec![
                            ("verification_outcome".to_owned(), "eq".to_string(), "DISCARD".to_string()),
                        ],
                        target: "CLOSE".to_string(),
                    },
                    Route {
                        conditions: vec![
                            ("verification_outcome".to_string(), "eq".to_string(), "MONITOR".to_string()),
                        ],
                        target: "MONITOR".to_string(),
                    },
                    Route {
                        conditions: vec![
                            ("verification_outcome".to_string(), "eq".to_string(), "ADVANCE".to_string())
                        ],
                        target: "RISK_ASSESS".to_string(),
                    }
                ],
            },
            Stage {
                uuid: "RISK_ASSESS".to_string(),
                task: true,
                permissions: None,
                routes: vec![
                    Route {
                        conditions: Vec::new(),
                        target: "RISK_CHAR".to_string(),
                    }
                ],
            },
            Stage {
                uuid: "RISK_CHAR".to_string(),
                task: true,
                permissions: None,
                routes: vec![
                    Route {
                        conditions: Vec::new(),
                        target: "OUTCOME".to_string(),
                    }
                ],
            },
            Stage {
                uuid: "OUTCOME".to_string(),
                task: true,
                permissions: None,
                routes: vec![
                    Route {
                        conditions: vec![
                            ("outcome".to_string(), "eq".to_string(), "DISCARD".to_string()),
                        ],
                        target: "CLOSE".to_string(),
                    },
                    Route {
                        conditions: vec![
                            ("outcome".to_string(), "eq".to_string(), "RESPOND".to_string())
                        ],
                        target: "WAIT".to_string(),
                    },
                    Route {
                        conditions: vec![
                            ("outcome".to_string(), "eq".to_string(), "RESPOND".to_string())
                        ],
                        target: "CLOSE".to_string(),
                    }
                ],
            }
        ];

        alarm_workflow = Workflow {
            stages,
            definition: HashMap::new(),
        };
    }

    eprintln!("{:?}", alarm_workflow);
    // Get the current stage, pre-action
    eprintln!("{}", &alert.stage);
    let cur_stage: Stage = alarm_workflow.stages.iter().find(|ref x| x.uuid == alert.stage.clone()).cloned().unwrap();

    // TODO: Id the current stage has an associated task, we need to remove it as
    // it's not longer valid


    // Update the date for the alert
    for (key, value) in data {
        current_data.entry(key)
            .and_modify(|x| { *x = value.clone() })
            .or_insert(value.clone());
    }

    eprintln!("{:?}", &current_data);

    // Now need to figure out which stage to move to
    let mut next_stage_name: String = String::new();
    for route in cur_stage.routes {
        // If the route conditions are empty, we're assuming the route is the one to go to
        if route.conditions.is_empty() {
            next_stage_name = route.target.clone();
        } else {
            let mut results: Vec<bool> = Vec::new();
            for condition in route.conditions {
                eprintln!("{:?}", condition);
                let result: bool = evaluate_condition(&current_data, condition.clone());
                eprintln!("{}", result);

                results.push(result);
            }

            results.dedup();

            eprintln!("{:?}", results);

            // If there is only one value left in the vec after deduplication
            if results.len() == 1 {
                // If the single result is true, then we have the target stage
                if results[0] == true {
                    next_stage_name = route.target.clone();
                }
            }
        }
    }

    // If the next stage name is CLOSE, then we're closing the alert
    let mut moved_on = false;
    match next_stage_name.as_ref() {
        "CLOSE" => {
            next_state = "CLOSED".to_string();
            next_stage_name = "CLOSED".to_string();
        }
        "WAIT" => {
            next_stage_name = cur_stage.uuid.clone();
        }
        _ => {
            moved_on = true;
        }
    }

    // We're proceeding to the next stage of the alarm workflow
    if moved_on == true {
        let next_stage: Stage = alarm_workflow.stages.iter().find(|ref x| x.uuid == next_stage_name.clone()).cloned().unwrap();

        // TODO: Process any actions defined for this stage
        // TODO: Create new task if the stage is specified as one
    }

    let modified_time: DateTime<Utc> = Utc::now();
    let modified_time_str: String = modified_time.to_rfc3339().to_string();

    // Update the workflow items
    let wkf_uuid = Uuid::new_v4().to_string();
    let new_item = WorkflowItem {
        uuid: wkf_uuid,
        uid: uid.clone(),
        stage: alert.stage.clone(),
        modified: modified_time_str.clone(),
        submitted: modified_time_str.clone(),
    };

    let wkf_item_json: Value = json::to_value(new_item).unwrap();
    alert_workflow.push(wkf_item_json.clone());

    let mut sql = r#"
        UPDATE alerts
            SET stage = :stage,
                modified = :modified,
                modified_by = :modified_by,
                workflow = :workflow,
                data = :data,
                status = :status
        WHERE uuid = :uuid;
    "#.to_owned();

    // TODO: Update alert record
    let cur_data_value: Value = json::to_value(&current_data).unwrap();
    let cur_wkflw: Value = json::to_value(&alert_workflow).unwrap();
    let cur_time = Utc::now();
    let _ = conn.execute_named(&sql, &[
        (":stage", &next_stage_name),
        (":modified", &cur_time),
        (":modified_by", &uid),
        (":workflow", &cur_wkflw),
        (":data", &cur_data_value),
        (":status", &next_state),
        (":uuid", &alert.uuid),
    ]).unwrap();

    let mut changeset: Vec<(String, Value)> = Vec::new();

    changeset.push(("modified".to_string(), json::to_value(&modified_time_str).unwrap()));
    changeset.push(("modified_by".to_string(), json::to_value(&uid).unwrap()));
    changeset.push(("stage".to_string(), json::to_value(&next_stage_name).unwrap()));
    changeset.push(("status".to_string(), json::to_value(&next_state).unwrap()));
    changeset.push(("workflow".to_string(), json::to_value(&alert_workflow).unwrap()));
    changeset.push(("data".to_string(), json::to_value(&current_data).unwrap()));

    let event_data: Value = json::to_value(changeset).unwrap();

    //let _ = app.borrow_mut().add_own_event("ALERT", "UPDATE", alert.uuid.clone(), event_data.clone()).unwrap();

    // TODO: Create any tasks that need creating

    let output: Value = json::to_value(true).unwrap();

    Ok(output)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_evaluation() {
        let mut data: HashMap<String, Value> = HashMap::new();
        data.insert("outcome".to_string(), json::to_value("MONITOR").unwrap());

        let condition = ("outcome".to_string(), "eq".to_string(), "MONITOR".to_string());

        assert!(true);
    }

    #[test]
    fn test_evaluation_multiple() {
        assert!(true);
    }
}
