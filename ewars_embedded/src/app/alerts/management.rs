use std::cell::RefCell;
use std::rc::Rc;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize)]
pub struct Alert {
    pub uuid: String,
    pub alid: String,
    pub data: Option<Value>,
    pub workflow: Option<Value>,
}


pub fn query_alerts(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn get_alert(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn update_alert(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let updates: Alert = json::from_value(args).unwrap();
    let uid: String = app.borrow().get_current_uuid().to_string();

    let sql = r#"
        UPDATE alerts
            SET data = :data,
                workflow = :worflow,
                modified = CURRENT_TIMESTAMP,
                modified_by = :user_id
        WHERE uuid = :uuid;
    "#.to_owned();

    conn.execute_named(&sql, &[
        (":data", &updates.data),
        (":workflow", &updates.workflow),
        (":user_id", &uid),
        (":uuid", &updates.uuid),
    ]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn delete_alert(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    let mut sql = r#"
        DELETE FROM alerts WHERE uuid = ?;
    "#.to_owned();

    conn.execute(&sql, &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}

