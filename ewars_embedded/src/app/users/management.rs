use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::user::User;


pub fn create_system_user(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn revoke_user_access(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn update_user(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn upgrade_user_role(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

