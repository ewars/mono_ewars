use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;

use crate::state::ApplicationState;

use crate::models::user::User;
use crate::models::query::{QueryResult, ResourceQuery};

#[derive(Debug, Serialize, Deserialize)]
pub struct UserRow {
    pub uuid: String,
    pub name: String,
    pub email: String,
    pub system: bool,
    pub role: String,
    pub status: String,
    pub role_name: Option<String>,
    pub org_id: Option<String>,
    pub org_name: Option<String>,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

impl UserRow {
    pub fn from_row(row: &Row) -> UserRow {
        UserRow {
            uuid: row.get(0),
            name: row.get(1),
            email: row.get(2),
            system: row.get(3),
            role: row.get(4),
            status: row.get(5),
            role_name: row.get(6),
            org_id: row.get(7),
            org_name: row.get(8),
            created: row.get(9),
            created_by: row.get(10),
            modified: row.get(11),
            modified_by: row.get(12),
        }
    }
}

pub fn query_users(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let query: ResourceQuery = json::from_value(args).unwrap();

    let mut sql: Vec<String> = vec![
        "SELECT".to_string(),
        "f.uuid,".to_string(),
        "f.name,".to_string(),
        "f.email,".to_string(),
        "f.system,".to_string(),
        "f.role,".to_string(),
        "f.status,".to_string(),
        "r.name AS role_name,".to_string(),
        "f.org_id,".to_string(),
        "o.name AS org_name,".to_string(),
        "f.created,".to_string(),
        "f.created_by,".to_string(),
        "f.modified,".to_string(),
        "f.modified_by".to_string(),
        "FROM users AS f".to_string(),
        "LEFT JOIN roles AS r ON r.uuid = f.role".to_string(),
        "LEFT JOIN organizations AS o ON o.uuid = f.org_id".to_string(),
    ];

    let mut sql_count: Vec<String> = vec![
        "SELECT".to_string(),
        "COUNT(f.uuid) AS total_records".to_string(),
        "FROM users AS f".to_string(),
        "LEFT JOIN roles AS r ON r.uuid = f.role".to_string(),
        "LEFT JOIN organizations AS o ON o.uuid = f.org_id".to_string(),
    ];

    let mut wheres: Vec<String> = Vec::new();
    let mut orders: Vec<String> = Vec::new();

    for (key, val) in query.filters.clone() {
        let (cmp, el_type, val) = query.filters.get(&key).unwrap();

        let mut flt_str: Vec<String> = Vec::new();

        flt_str.push(format!("f.{}", key));

        match cmp.as_ref() {
            "eq" => {
                flt_str.push("=".to_string());
            },
            "neq" => {
                flt_str.push("!=".to_string());
            },
            _ => {
                flt_str.push("=".to_string());
            }
        }

        flt_str.push(format!("'{}'", val));

        wheres.push(flt_str.join(" "));
    }


    for (key, val) in query.orders.clone() {
        let (col_type, dir) = query.orders.get(&key).unwrap();

        let mut ord_str: Vec<String> = Vec::new();

        ord_str.push(format!("f.{}", key));

        ord_str.push(dir.to_string());

        orders.push(ord_str.join(" "));
    }

    if wheres.len() > 0 {
        sql.push("WHERE".to_string());
        sql_count.push("WHERE".to_string());

        let mut sql_f = true;
        for item in wheres {
            if sql_f != true {
                sql.push("AND".to_string());
                sql_count.push("AND".to_string());
            }
            sql.push(item.clone());
            sql_count.push(item.clone());
            sql_f = false;
        }
    }

    if orders.len() > 0 {
        sql.push("ORDER BY".to_string());
        sql.push(orders.join(", "));
    }

    sql.push(format!("LIMIT {}", query.limit));
    sql.push(format!("OFFSET {}", query.offset));

    let c_sql = sql.join("\n");
    let c_sql_count = sql_count.join("\n");

    let mut stmt = match conn.prepare(&c_sql) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            return Err(format!("ERROR"));
        }
    };

    let rows = stmt.query_map(&[], |row| {
        UserRow::from_row(row)
    }).unwrap();

    let results: Vec<UserRow> = rows.map(|x| x.unwrap()).collect();

    let counter: i32 = conn.query_row(&c_sql_count, &[], |row| {
        row.get(0)
    }).unwrap();

    let qr = QueryResult {
        records: json::to_value(results).unwrap(),
        count: counter,
    };

    Ok(json::to_value(qr).unwrap())
}

pub fn get_user(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    Ok(json::to_value(true).unwrap())
}
