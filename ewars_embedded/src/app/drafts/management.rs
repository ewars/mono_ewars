use std::rc::Rc;
use std::cell::RefCell;

use rusqlite::{Row, NO_PARAMS};
use chrono::prelude::*;
use uuid::Uuid;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::{NewDraft, Draft};

static SQL_DELETE_DRAFT: &'static str = r#"
    DELETE FROM drafts WHERE uuid = ?;
"#;

static SQL_UPDATE_DRAFT: &'static str = r#"
    UPDATE drafts
    SET data = ?,
        modified = CURRENT_TIMESTAMP,
        modified_by = ?
    WHERE uuid = ?;
"#;

// Create a new draft item
pub fn create_draft(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let data: NewDraft = json::from_value(args).unwrap();

    let new_id: Option<Uuid> = match data.insert(&conn, &uid) {
        Ok(res) => Some(res),
        Err(_) => Option::None
    };

    if let Some(id) = new_id {
        let draft: Option<Draft> = match Draft::get_by_id(&conn, &id) {
            Ok(res) => Some(res),
            Err(_) => Option::None
        };

        if let Some(c) = draft {
            Ok(json::to_value(c).unwrap())
        } else {
            Err("ERR_RETRIEVING_DRAFT".to_string())
        }
    } else {
        Err("ERR_CREATING_DRAFT".to_string())
    }
}

// Delete a draft item
pub fn delete_draft(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: Uuid = json::from_value(args).unwrap();

    match Draft::delete_by_id(&conn, &id) {
        Ok(_) => Ok(json::to_value(true).unwrap()),
        Err(_) => Err("ERR_DELETING_DRAFT".to_string()),
    }
}

// Update a draft item
pub fn update_draft(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let dr: Draft = json::from_value(args).unwrap();

    match dr.update(&conn, &uid) {
        Ok(_) => Ok(json::to_value(true).unwrap()),
        Err(_) => Err("ERR_UPDATING_DRAFT".to_string()),
    }
}
