use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::location_group::LocationGroup;

pub fn get_location_groups(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let sql = r#"
        SELECT groups
        FROM locations
        WHERE json_array_length(groups) > 0;
    "#.to_owned();
    let mut stmt = conn.prepare(&sql).unwrap();

    let rows = stmt.query_map(&[], |row| {
        let item: Value = row.get(0);
        item
    }).unwrap();

    let mut results: Vec<String> = Vec::new();

    for item in rows {
        let group_set: Vec<String> = json::from_value(item.unwrap()).unwrap();
        for node in group_set {
            results.push(node.clone());
        }
    }

    results.dedup();

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}

pub fn query_location_groups(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let sql = r#"
        SELECT groups
        FROM locations
        WHERE json_array_length(groups) > 0;
    "#.to_owned();
    let mut stmt = conn.prepare(&sql).unwrap();

    let rows = stmt.query_map(&[], |row| {
        let item: Value = row.get(0);
        item
    }).unwrap();

    let mut results: Vec<String> = Vec::new();

    for item in rows {
        let group_set: Vec<String> = json::from_value(item.unwrap()).unwrap();
        for node in group_set {
            results.push(node.clone());
        }
    }

    results.dedup();

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}
