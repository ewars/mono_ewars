use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;
use chrono::prelude::*;
use uuid::Uuid;

use crate::state::ApplicationState;

static SQL_INSERT_PENDING_ASSIGN: &'static str = r#"
    INSERT INTO ASSIGNMENTS
    (uuid, uid, status, lid, fid, assign_type, created, created_by, modified, modified_by)
    VALUES (:uuid, :uid, :status, :lid, :fid, :assign_type, :created, :created_by, :modified, :modified_by);
"#;

static SQL_CREATE_TASK: &'static str = r#"
    INSERT INTO tasks
    (uuid, status, task_type, data, actions, version, form, context, created, modified, modified_by)
    VALUES (:uuid, :status, :task_type, :data, :actions, :version, :form, :context, :created, :modified, :modified_by);
"#;

static SQL_CHECK_DUPE_ASSIGNS: &'static str = r#"
    SELECT uid, status, lid, fig, assign_type
    FROM assignments
    WHERE uid = ?;
"#;

static ASSIGN_DEFAULT_STATUS: &'static str = "PENDING";
static ASSIGN_DEFAULT_TYPE: &'static str = "DEFAULT";

#[derive(Debug, Serialize, Deserialize)]
pub struct AssignmentRequest {
    pub lids: Vec<String>,
    pub fids: Vec<String>,
}


#[derive(Debug, Serialize, Deserialize)]
pub struct ExistingAssignment {
    pub uid: String,
    pub status: String,
    pub lid: Option<String>,
    pub fid: Option<String>,
    pub assign_type: String,
}

impl ExistingAssignment {
    pub fn from_row(row: &Row) -> ExistingAssignment {
        ExistingAssignment {
            uid: row.get(0),
            status: row.get(1),
            lid: row.get(2),
            fid: row.get(3),
            assign_type: row.get(4),
        }
    }
}


// Request an assignment for a form location pairing
pub fn request_assignment(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let new_assign: AssignmentRequest = json::from_value(args).unwrap();

    // Get the users current assignments
    let mut stmt = conn.prepare(&SQL_CHECK_DUPE_ASSIGNS).unwrap();
    let rows = stmt.query_map(&[&uid], |row| {
        ExistingAssignment::from_row(row)
    }).unwrap();
    let existing: Vec<ExistingAssignment> = rows.map(|x| x.unwrap()).collect();

    // TODO: Make sure none of the new assignments conflict with the users current assignments
    // TODO: Check if the assigned locations have open reporting periods for the forms that are being requested.

    let mut insert_assignment = conn.prepare(&SQL_INSERT_PENDING_ASSIGN).unwrap();

    // Iterate through the locations and forms and create the assignment
    // UI layer assures that these locations and forms can be joined together
    let modified_time: DateTime<Utc> = Utc::now();
    for loc in new_assign.lids {
        for form in new_assign.fids {
            let new_uuid = Uuid::new_v4().to_string();
            insert_assignment.execute_named(&[
                (":uuid", &new_uuid),
                (":uid", &uid),
                (":status", &ASSIGN_DEFAULT_STATUS),
                (":lid", &loc),
                (":fid", &form),
                (":assign_type", &ASSIGN_DEFAULT_TYPE),
                (":created", &modified_time),
                (":created_by", &uid),
                (":modified", &modifed_time),
                (":modified_by", &uid),
            ])
        }
    }

    // TODO: Create items in event log for each assignment

    Ok(json::to_value(true).unwrap())
}
