use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize)]
pub struct ContentItem {
    pub uuid: String,
    pub name: String,
    pub status: String,
    pub version: Option<i32>,
    pub description: Option<String>,
    pub content_type: String,
    pub shared: Option<bool>,
    pub layout: Option<Value>,
    pub data: Option<Value>,
    pub style: Option<Value>,
    pub variables: Option<Value>,
    pub permissions: Option<Value>,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

impl ContentItem {
    pub fn from_row(row: &Row) -> ContentItem {
        ContentItem {
            uuid: row.get(0),
            name: row.get(1),
            status: row.get(2),
            version: row.get(3),
            description: row.get(4),
            content_type: row.get(5),
            shared: row.get(6),
            layout: row.get(7),
            data: row.get(8),
            style: row.get(9),
            variables: row.get(10),
            permissions: row.get(11),
            created: row.get(12),
            created_by: row.get(13),
            modified: row.get(14),
            modified_by: row.get(15),
        }
    }
}

pub fn get_resources(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let content_type: String = json::from_value(args).unwrap();
    let user_id: String = app.borrow().get_current_uuid();

    let mut stmt = conn.prepare(r#"
        SELECT * FROM resources
        WHERE content_type = ?
        AND created_by = ?;
    "#).unwrap();


    let rows = stmt.query_map(&[
        (&content_type),
        (&user_id),
    ], |row| {
        ContentItem::from_row(row)
    }).unwrap();

    let items: Vec<ContentItem> = rows.map(|x| x.unwrap()).collect();

    Ok(json::to_value(items).unwrap())
}
