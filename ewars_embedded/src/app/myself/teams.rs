use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Team {
    pub uuid: String,
    pub name: String,
    pub permissions: Value,
    pub members: Value,
}

pub fn get_teams(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(r#"
        SELECT uuid, name, permissions, members
        FROM teams
        WHERE status = 'ACTIVE';
    "#).unwrap();

    let rows = stmt.query_map(&[], |row| {
        Team {
            uuid: row.get(0),
            name: row.get(1),
            permissions: row.get(2),
            members: row.get(3),
        }
    }).unwrap();

    let mut results: Vec<Team> = rows.map(|x| x.unwrap()).collect();

    let role = app.borrow().get_current_role();

    match role.as_ref() {
        "ACCOUNT_ADMIN" => {},
        _ => {
            results = results.iter().filter(|x| {
                let permissions: HashMap<String, Value> = json::from_value(x.permissions.clone()).unwrap();
                let inner_res = permissions.contains_key(&role);
                inner_res
            }).cloned().collect();
        }
    }

    let output = json::to_value(results).unwrap();
    Ok(output)
}
