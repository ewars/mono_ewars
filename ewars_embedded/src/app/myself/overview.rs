use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;
use std::io::Error as IoError;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;

use crate::state::ApplicationState;

#[derive(Debug, Serialize)]
pub struct Location {
    pub uuid: String,
    pub lti_name: String,
    pub name: Value,
    pub status: String,
}

#[derive(Debug, Serialize)]
pub struct Form {
    pub uuid: String,
    pub name: Value,
}

#[derive(Debug, Serialize)]
pub struct ReportingPeriod {
    pub location_id: String,
    pub form_id: String,
    pub start_date: String,
    pub end_date: Option<String>,
    pub interval: bool,
    pub interval_type: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct Overview {
    pub locations: HashMap<String, Location>,
    pub forms: HashMap<String, Form>,
}

pub type LocationId = String;
pub type RecordDate = String;

#[derive(Debug, Serialize, Clone)]
struct LocationAssignment {
    pub uuid: String,
    pub name: Option<Value>,
    pub lti: Option<String>,
    pub status: String,
    pub lti_name: Option<Value>,
    pub fid: String,
    pub start_date: String,
    pub end_date: Option<String>,
    pub form_name: Option<Value>,
    pub location_field: Option<String>,
    pub date_field: Option<String>,
    pub rec_count: Vec<(RecordDate, i32)>,
}

impl LocationAssignment {
    pub fn from_row(row: &Row) -> LocationAssignment {
        LocationAssignment {
            uuid: row.get(0),
            name: row.get(1),
            lti: row.get(2),
            status: row.get(3),
            lti_name: row.get(4),
            fid: row.get(5),
            start_date: row.get(6),
            end_date: row.get(7),
            form_name: row.get(8),
            location_field: row.get(9),
            date_field: row.get(10),
            rec_count: Vec::new(),
        }
    }

    pub fn add_record_date(&mut self, rec: RecordDateSet) {
        self.rec_count.push(rec);
    }
}

pub type RecordDateSet = (RecordDate, i32);

// get a users assigned locations
fn get_user_locations(app: Rc<RefCell<ApplicationState>>) -> Result<Vec<LocationAssignment>, IoError> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let mut stmt = conn.prepare(r#"
        SELECT l.uuid, l.name, l.lti, l.status,
                lt.name AS lti_name,
                rp.fid, rp.start_date, rp.end_date,
                f.name,
                json_extract(f.features, '$.LOCATION_REPORTING.field') AS location_field,
                json_extract(f.features, '$.INTERVAL_REPORTING.field') AS date_field
        FROM assignments AS r
            LEFT JOIN locations AS l ON l.uuid = r.lid
            LEFT JOIN reporting AS rp ON rp.lid = l.uuid
            LEFT JOIN location_types AS lt ON lt.uuid = l.lti
            LEFT JOIN forms AS f ON f.uuid = rp.fid
        WHERE r.uid = ?
            AND r.status = 'ACTIVE'
            AND l.status IN ('ACTIVE', 'ARCHIVED')
            AND f.status = 'ACTIVE';
    "#).unwrap();

    let rows = stmt.query_map(&[&uid], |row| {
        LocationAssignment::from_row(row)
    }).unwrap();

    let results: Vec<LocationAssignment> = rows.map(|x| x.unwrap()).collect();

    Ok(results)
}

fn get_user_overview(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let assignments: Vec<LocationAssignment> = get_user_locations(app.clone()).unwrap();

    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut results: Vec<LocationAssignment> = Vec::new();
    for assign in &assignments {
        // Check if the location_field came up with something
        let mut editable_assign = assign.clone();
        if assign.location_field.is_some() {
            let location_field: String = assign.location_field.clone().unwrap();
            if assign.date_field.is_some() {
                let date_field: String = assign.date_field.clone().unwrap();
                let mut stmt = conn.prepare(r#"
                    SELECT json_extract(r.data, '$.:date_field') as data_date,
                            COUNT(*)
                    FROM records AS r
                    WHERE r.fid = :form_id
                        AND json_extract(r.data, '$.:location_field') = :location_uuid
                        AND r.status = 'SUBMITTED'
                    GROUP BY json_extract(r.data, '$.:date_field');
                "#).unwrap();

                let rows = stmt.query_map_named(&[
                    (":date_field", &date_field),
                    (":form_id", &assign.fid),
                    (":location_field", &location_field),
                    (":location_uuid", &assign.uuid),
                    (":date_field", &date_field),
                ], |row| {
                    (row.get(0), row.get(1))
                }).unwrap();

                let items: Vec<RecordDateSet> = rows.map(|x| x.unwrap()).collect();
                for item in items {
                    editable_assign.add_record_date(item.clone());
                }
            } else {
                // If there's no date field unique'd into the form
                // then we use the submitted date as the aggregation point
                let uid = app.borrow().get_current_uuid();

                let mut stmt = conn.prepare(r#"
                    SELECT COUNT(*) AS total, r.submitted
                    FROM records AS r
                    WHERE r.fid = ?
                        AND r.status = 'SUBMITTED'
                        AND json_extract(r.data, '$.?') = ?
                   GROUP BY r.submitted;
                "#).unwrap();

                let rows = stmt.query_map(&[
                    &assign.fid,
                    &location_field,
                    &assign.uuid,
                ],
                                          |row| {
                                              (row.get(1), row.get(0))
                                          }).unwrap();

                let items: Vec<RecordDateSet> = rows.map(|x| x.unwrap()).collect();
                for item in items {
                    editable_assign.add_record_date(item.clone());
                }
            }
        } else {
            let mut stmt = conn.prepare(r#"
                SELECT COUNT(r.uuid) AS total, r.submitted
                FROM records AS r
                WHERE r.fid = ?
                    AND r.status = 'SUBMITTED'
                    AND r.submitted_by = ?
                GROUP BY r.submitted;
            "#).unwrap();
            let uid = app.borrow().get_current_uuid();

            let rows = stmt.query_map(&[
                &assign.fid,
                &uid,
            ], |row| {
                (row.get(1), row.get(0))
            }).unwrap();

            let items: Vec<RecordDateSet> = rows.map(|x| x.unwrap()).collect();
            for item in items {
                editable_assign.add_record_date(item.clone());
            }
        }

        results.push(editable_assign);
    }

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}

#[derive(Debug, Serialize)]
pub struct FormStatistics {
    pub uuid: String,
    pub name: Value,
    pub locations: Vec<(String, Value)>,
    pub date_field: Option<String>,
    pub location_field: Option<String>,
    pub status: String,
    pub total_records: i32,
    pub definition: Value,
    pub constraints: Option<Value>,
    pub start_date: Option<String>,
}

// Get overview for account admin
fn get_overview_account_admin(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(r#"
        SELECT  f.uuid AS form_id,
                f.name AS form_name,
                json_extract(f.features, '$.LOCATION_REPORTING.field') AS location_field,
                json_extract(f.features, '$.INTERVAL_REPORTING.field') AS date_field,
                f.status,
                (SELECT COUNT(r.uuid) FROM records AS r WHERE r.fid = f.uuid) AS total_records,
                json_extract(f.definition, '$.definition') as definition,
                json_extract(f.definition, '$.constraints') as constraints,
                (SELECT MIN(a.start_date) FROM reporting AS a WHERE a.fid = f.uuid) AS start_date
        FROM forms AS f
            WHERE f.status = 'ACTIVE';
    "#).unwrap();

    let rows = stmt.query_map(&[], |row| {
        FormStatistics {
            uuid: row.get(0),
            name: row.get(1),
            locations: Vec::new(),
            date_field: row.get(2),
            location_field: row.get(3),
            status: row.get(4),
            total_records: row.get(5),
            definition: row.get(6),
            constraints: row.get(7),
            start_date: row.get(8),
        }
    }).unwrap();

    let forms: Vec<FormStatistics> = rows.map(|x| x.unwrap()).collect();

    Ok(json::to_value(forms).unwrap())
}

fn get_overview_org_admin(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    Ok(json::to_value(true).unwrap())
}

fn get_overview_reg_admin(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    Ok(json::to_value(true).unwrap())
}

pub fn get_overview(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let role = app.borrow().get_current_role();

    let results: Option<Value> = match role.as_ref() {
        "ACCOUNT_ADMIN" => {
            Some(get_overview_account_admin(app.clone(), args.clone()).unwrap())
        }
        "REGIONAL_ADMIN" => {
            Some(get_overview_reg_admin(app.clone(), args.clone()).unwrap())
        }
        "USER" => {
            Some(get_overview_user(app.clone(), args.clone()).unwrap())
        }
        "ORG_ADMIN" => {
            Some(get_overview_org_admin(app.clone(), args.clone()).unwrap())
        }
        _ => None
    };

    Ok(results.unwrap())
}

#[derive(Debug, Serialize, Clone)]
struct FormItem {
    uuid: String,
    name: Value,
    location_field: Option<String>,
    date_field: Option<String>,
    definition: HashMap<String, Value>,
    locations: Vec<LocationFormItem>,
}

impl FormItem {
    pub fn add_node(&mut self, item: &LocationItem) {
        self.locations.push(LocationFormItem {
            uuid: item.uuid.clone(),
            name: item.name.clone(),
            lti: item.lti.clone(),
            lti_name: item.lti_name.clone(),
            start_date: item.start_date.clone(),
            end_date: item.end_date.clone(),
        })
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct LocationItem {
    pub uuid: String,
    pub name: Value,
    pub lti: String,
    pub lti_name: Value,
    pub status: String,
    pub start_date: String,
    pub end_date: Option<String>,
    pub form_id: String,
}

#[derive(Debug, Serialize, Clone)]
pub struct LocationFormItem {
    pub uuid: String,
    pub name: Value,
    pub lti: String,
    pub lti_name: Value,
    pub start_date: String,
    pub end_date: Option<String>,
}


#[derive(Debug, Serialize)]
pub struct AssignmentsResult {
    pub forms: HashMap<String, Value>,
    pub locations: Vec<LocationFormItem>,
}

fn get_overview_user(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    // Get active forms in the systemS
    // TODO: Check permissions of the form to make sure that the user has access to it
    let mut stmt = conn.prepare(r#"
        SELECT f.uuid,
            f.name,
            json_extract(f.features, '$.LOCATION_REPORTING.field') AS location_field,
            json_extract(f.features, '$.INTERVAL_REPORTING.field') AS date_field,
            json_extract(f.definition, '$.definition') as definition
        FROM forms AS f
        WHERE f.status = 'ACTIVE';
    "#).unwrap();

    let form_rows = stmt.query_map(&[], |row| {
        let definition: HashMap<String, Value> = json::from_value(row.get(4)).unwrap();
        FormItem {
            uuid: row.get(0),
            name: row.get(1),
            location_field: row.get(2),
            date_field: row.get(3),
            definition,
            locations: Vec::new(),
        }
    }).unwrap();

    let mut forms: Vec<FormItem> = form_rows.map(|x| x.unwrap()).collect();


    // First lets get the locations that this user has access to
    let sql = r#"
        SELECT r.lid, l.lti, r.assign_type
        FROM assignments AS r
            LEFT JOIN locations AS l ON l.uuid = r.lid
        WHERE r.uid = ?;
    "#.to_owned();

    let mut stmt = conn.prepare(&sql).unwrap();

    let rows = stmt.query_map(&[&uid], |row| {
        (row.get(0), row.get(1), row.get(2))
    }).unwrap();

    let assignments: Vec<(String, String, String)> = rows.map(|x| x.unwrap()).collect();

    // Now let's create a sctrict list of all individual locations
    // TODO: Break out UNDER based assignments
    // TODO: Break out location group based assignments


    // Get just the UUIDs of the locations that the user has access to
    let location_ids: Vec<String> = assignments.iter().map(|x| x.0.clone()).clone().collect();


    // Get the actual locations themselves
    let mut sql = r#"
        SELECT l.uuid, l.name, l.lti, lt.name, rp.status, rp.start_date, rp.end_date, rp.fid
        FROM locations AS l
            LEFT JOIN location_types AS lt ON lt.uuid = l.lti
            LEFT JOIN reporting AS rp ON rp.lid = l.uuid
        WHERE l.uuid IN ('{LOCATION_IDS}')
            AND l.status = 'ACTIVE'
            AND rp.status = 'ACTIVE'
            AND rp.start_date IS NOT NULL;
    "#.to_owned();
    let sql_loc_ids = location_ids.join("','");
    sql = sql.replace("{LOCATION_IDS}", &sql_loc_ids);

    let mut stmt = conn.prepare(&sql).unwrap();
    let rows = stmt.query_map(&[], |row| {
        LocationItem {
            uuid: row.get(0),
            name: row.get(1),
            lti: row.get(2),
            lti_name: row.get(3),
            status: row.get(4),
            start_date: row.get(5),
            end_date: row.get(6),
            form_id: row.get(7),
        }
    }).unwrap();
    let locations: Vec<LocationItem> = rows.map(|x| x.unwrap()).collect();

    for form in forms.iter_mut() {
        let form_locations: Vec<LocationItem> = locations.iter().filter(|x| x.form_id == form.uuid).cloned().collect();

        for item in form_locations {
            form.add_node(&item);
        }
    }

    let output: Value = json::to_value(forms).unwrap();
    Ok(output)
}
