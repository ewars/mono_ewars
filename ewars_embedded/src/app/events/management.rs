use serde_json;
use serde_json::Value;

use std::rc::Rc;
use std::cell::RefCell;

use crate::state::ApplicationState;


#[derive(Debug, Serialize)]
pub struct InsertEvent {
    data: Value,
    ref_type: String,
    ref_id: String,
    ref_action: String,
}

// Create an event in the event poow
pub fn create_event(ref_type: String, ref_id: String, ref_action: String, data: Value, app: Rc<RefCell<ApplicationState>>) -> Result<(), String> {
    let pool = app.borrow().get_event_pool().unwrap().get().unwrap();


    Ok(())
}
