use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct LocationType {
    uuid: String,
    name: Option<Value>,
    description: Option<String>,
    restrictions: Option<Value>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>
}

pub fn seed_location_types(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let items: Vec<LocationType> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO location_types
        (uuid, name, description, restrictions, created, created_by, modified, modified_by)
        VALUES (:uuid, :name, :description, :restrictions, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":name", &data.name),
            (":description", &data.description),
            (":restrictions", &data.restrictions),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
