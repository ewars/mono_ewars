use std::cell::RefCell;
use std::rc::Rc;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Indicator {
    uuid: String,
    group_id: Option<String>,
    name: String,
    status: String,
    description: Option<String>,
    itype: Option<String>,
    icode: Option<String>,
    permissions: Option<Value>,
    definition: Option<Value>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}

pub fn seed_indicators(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let items: Vec<Indicator> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO indicators
        (uuid, group_id, name, status, description, itype, icode, permissions, definition, created, created_by, modified, modified_by)
        VALUES (:uuid, :group_id, :name, :status, :description, :itype, :icode, :permissions, :definition, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":group_id", &data.group_id),
            (":name", &data.name),
            (":status", &data.status),
            (":description", &data.description),
            (":itype", &data.itype),
            (":icode", &data.icode),
            (":permissions", &data.permissions),
            (":definition", &data.definition),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
