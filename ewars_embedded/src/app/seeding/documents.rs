use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Document {
    uuid: String,
    template_name: String,
    instance_name: String,
    status: String,
    version: i32,
    theme: Option<String>,
    layout: Option<Value>,
    content: Option<String>,
    data: Option<Value>,
    generation: Value,
    orientation: String,
    template_type: String,
    permissions: Option<Value>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}

pub fn seed_documents(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let records: Vec<Document> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO documents
        (uuid, template_name, instance_name, status, version, theme, layout, content, data, generation, orientation, template_type, permissions, created, created_by, modified, modified_by)
        VALUES (:uuid, :template_name, :instance_name, :status, :version, :theme, :layout, :content, :data, :generation, :orientation, :template_type, :permissions, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in records {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":template_name", &data.template_name),
            (":instance_name", &data.instance_name),
            (":status", &data.status),
            (":version", &data.version),
            (":theme", &data.theme),
            (":layout", &data.layout),
            (":content", &data.content),
            (":data", &data.data),
            (":generation", &data.generation),
            (":orientation", &data.orientation),
            (":template_type", &data.template_type),
            (":permissions", &data.permissions),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
