use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Resource {
    uuid: String,
    name: String,
    status: Option<String>,
    version: i32,
    description: Option<String>,
    content_type: String,
    shared: bool,
    layout: Option<Value>,
    data: Option<Value>,
    style: Option<Value>,
    variables: Option<Value>,
    permissions: Option<Value>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}


pub fn seed_resources(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let records: Vec<Resource> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO resources
        (uuid, name, status, version, description, content_type, shared, layout, data, style, variables, permissions, created, created_by, modified, modified_by)
        VALUES (:uuid, :name, :status, :version, :description, :content_type, :shared, :layout, :data, :style, :variables, :permissions, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in records {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":name", &data.name),
            (":status", &data.status),
            (":version", &data.version),
            (":description", &data.description),
            (":content_type", &data.content_type),
            (":shared", &data.shared),
            (":layout", &data.layout),
            (":data", &data.data),
            (":style", &data.style),
            (":variables", &data.variables),
            (":permissions", &data.permissions),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
