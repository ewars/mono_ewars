use std::cell::RefCell;
use std::rc::Rc;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct ReportingInterval {
    uuid: String,
    name: String,
    description: Option<String>,
    status: String,
    interval_type: String,
    generation: Option<Value>,
    data: Option<Value>,
    created: String,
    created_by: Option<Value>,
    modified: String,
    modified_by: Option<Value>,
}

pub fn seed_intervals(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let items: Vec<ReportingInterval> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO intervals
        (uuid, name, description, status, interval_type, generation, data, created, created_by, modified, modified_by)
        VALUES (:uuid, :name, :description, :status, :interval_type, :generation, :data, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    for data in items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":name", &data.name),
            (":description", &data.description),
            (":status", &data.status),
            (":interval_type", &data.interval_type),
            (":generation", &data.generation),
            (":data", &data.data),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
