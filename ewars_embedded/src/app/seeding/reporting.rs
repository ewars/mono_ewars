use std::rc::Rc;
use std::cell::RefCell;

use serde_derive;
use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;


#[derive(Debug, Deserialize)]
struct ReportingPeriod {
    pub uuid: String,
    pub lid: String,
    pub fid: String,
    pub start_date: String,
    pub end_date: Option<String>,
    pub status: String,
    pub pid: Option<String>,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}


pub fn seed_reporting(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let records: Vec<ReportingPeriod> = json::from_value(args).unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO reporting
        (uuid, lid, fid, start_date, end_date, status, pid, created, created_by, modified, modified_by)
        VALUES (:uuid, :lid, :fid, :start_date, :end_date, :status, :pid, :created, :created_by, :modified, :modified_by);
    "#.to_owned();


    for data in records {
        eprintln!("{:?}", data);
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":lid", &data.lid),
            (":fid", &data.fid),
            (":start_date", &data.start_date),
            (":end_date", &data.end_date),
            (":status", &data.status),
            (":pid", &data.pid),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }

    Ok(true)
}
