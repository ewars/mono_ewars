use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::{Role};


pub fn seed_roles(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let records: Vec<Role> = json::from_value(args).unwrap();

    for data in records {
        let _= data.insert(&conn).unwrap();
    }

    Ok(true)
}
