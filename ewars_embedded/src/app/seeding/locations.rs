use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Location {
    uuid: String,
    name: Value,
    status: String,
    pcode: String,
    codes: Option<Value>,
    groups: Option<Value>,
    data: Option<Value>,
    lti: String,
    pid: Option<String>,
    lineage: Value,
    organizations: Option<Value>,
    image: Option<String>,
    geometry_type: String,
    geojson: Option<Value>,
    population: Option<Value>,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}


pub fn seed_locations(app: Rc<RefCell<ApplicationState>>, tki: String, args: Value) -> Result<bool, String> {
    let conn = app.borrow().get_spec_pool(tki).unwrap().get().unwrap();

    let sql = r#"
        INSERT OR REPLACE INTO locations
        (uuid, name, status, pcode, codes, groups, data, lti, pid, lineage, organizations, image, geometry_type, geojson, population, created, created_by, modified, modified_by)
        VALUES (:uuid, :name, :status, :pcode, :codes, :groups, :data, :lti, :pid, :lineage, :organizations, :image, :geometry_type, :geojson, :population, :created, :created_by, :modified, :modified_by);
    "#.to_owned();

    let items: Vec<Location> = json::from_value(args).unwrap();

    for data in &items {
        conn.execute_named(&sql, &[
            (":uuid", &data.uuid),
            (":name", &data.name),
            (":status", &data.status),
            (":pcode", &data.pcode),
            (":codes", &data.codes),
            (":groups", &data.groups),
            (":data", &data.data),
            (":lti", &data.lti),
            (":pid", &data.pid),
            (":lineage", &data.lineage),
            (":organizations", &data.organizations),
            (":image", &data.image),
            (":geometry_type", &data.geometry_type),
            (":geojson", &data.geojson),
            (":population", &data.population),
            (":created", &data.created),
            (":created_by", &data.created_by),
            (":modified", &data.modified),
            (":modified_by", &data.modified_by),
        ]).unwrap();
    }


    Ok(true)
}
