use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;

use crate::state::ApplicationState;
use crate::app::seeding;


#[derive(Debug, Deserialize)]
pub struct IncomingEvent {
    pub uuid: String,
    pub ref_type: String,
    pub ref_id: String,
    pub ref_ts: usize,
    pub command: String,
    pub data: Option<Value>,
    pub source: String,
    pub ts_ms: usize,
    pub source_version: usize,
    pub device_id: String,
}

pub fn handle_update(table: String, changeset: Value) -> Option<String> {
    let updates: Vec<String> = Vec::new();
    let data: Vec<(String, Value)> = json::from_value(changeset).unwrap();

    let mut updates: Vec<String> = Vec::new();

    for item in data.clone() {
        let mut up_sql: String = "".to_string();
        up_sql.push_str(&item.0);
        up_sql.push_str(" = '");
        up_sql.push_str(&item.1.to_string());
        up_sql.push_str("'");
        updates.push(up_sql);
    }

    let mut sql = format!("UPDATE {} SET ", table);
    if updates.len() > 0 {
        sql.push_str(&updates.join(", "));
        sql.push_str(" WHERE uuid = ?;");
    }

    Some(sql)
}

pub fn process_event(state: Rc<RefCell<ApplicationState>>, tki: String, events: Value) -> Result<bool, String> {
    let v_events: Vec<IncomingEvent> = json::from_value(events).unwrap();
    let conn = state.borrow().get_spec_pool(tki.clone()).unwrap().get().unwrap();

    for event in v_events {
        match event.command.as_ref() {
            "DELETE" => {
                match event.ref_type.as_ref() {
                    "DOCUMENT" => {
                        conn.execute("DELETE FROM documents WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "RESOURCE" => {
                        conn.execute("DELETE FROM resources WHERE uuid =  ?", &[&event.ref_id]).unwrap();
                    },
                    "FORM" => {
                        conn.execute("DELETE FROM records WHERE uuid = ?", &[&event.ref_id]).unwrap();
                        conn.execute("DELETE FROM reporting WHERE fid = ?", &[&event.ref_id]).unwrap();
                        conn.execute("DELETE FROM forms WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "ALERT" => {
                        conn.execute("DELETE FROM alerts WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "ALARM" => {
                        conn.execute("DELETE FROM alerts WHERE alid = ?", &[&event.ref_id]).unwrap();
                        conn.execute("DELETE FROM alarms WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "USER" => {
                        conn.execute("DELETE FROM users WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "ASSIGNMENT" => {
                        conn.execute("DELETE FROM assignments WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "RECORD" => {
                        conn.execute("DELETE FROM records WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "LOCATION" => {
                        conn.execute("DELETE FROM locations WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "LOCATION_TYPE" => {
                        conn.execute("UPDATE locations SET lti = NULL WHERE lti = ?", &[&event.ref_id]).unwrap();
                        conn.execute("DELETE FROM location_types WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "TASK" => {
                        conn.execute("DELETE FROM tasks WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "DEVICE" => {
                        conn.execute("DELETE FROM devices WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "INDICATOR" => {
                        conn.execute("DELETE FROM indicators WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "INDICATOR_GROUP" => {
                        conn.execute("DELETE FROM indicator_groups WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "ORGANIZATION" => {
                        conn.execute("UPDATE users SET org_id = NULL WHERE org_id = ?", &[&event.ref_id]).unwrap();
                        conn.execute("DELETE FROM organizations WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "ROLE" => {
                        conn.execute("UPDATE users SET role = 'USER' WHERE role = ?", &[&event.ref_id]).unwrap();
                        conn.execute("DELETE FROM roles WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "TEAM" => {
                        conn.execute("DELETE FROM team_messages WHERE tid = ?", &[&event.ref_id]).unwrap();
                        conn.execute("DELETE FROM teams WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "TEAM_MESSAGE" => {
                        conn.execute("DELETE FROM team_messages WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    "INTERVAL" => {
                        conn.execute("DELETE FROM intervals WHERE uuid = ?", &[&event.ref_id]).unwrap();
                    },
                    _ => {}
                }
            }
            "UPDATE" => {
                let table = match event.ref_type.as_ref() {
                    "RECORD" => "records",
                    "DOCUMENT" => "documents",
                    "RESOURCE" => "resources",
                    "FORM" => "forms",
                    "INTERVAL" => "intervals",
                    "INDICATOR" => "indicators",
                    "INDICATOR_GROUP" => "indicator_groups",
                    "LOCATION" => "locations",
                    "LOCATION_TYPE" => "location_types",
                    "ORGANIZATION" => "organizations",
                    "REPORTING" => "reporting",
                    "ROLE" => "roles",
                    "TASK" => "tasks",
                    "TEAM" => "teams",
                    "TEAM_MESSAGE" => "team_messages",
                    "ASSIGNMENT" => "assignments",
                    "USER" => "users",
                    "ALARM" => "alarms",
                    "ALERT" => "alerts",
                    _ => ""
                }.to_string();

                if let Some(sql) = handle_update(table, event.data.unwrap().clone()) {
                    conn.execute(&sql, &[&event.ref_id]).unwrap();
                }
            }
            "INSERT" => {
                let v_pay: Vec<Value> = vec![event.data.unwrap().clone()];
                let payload: Value = json::to_value(v_pay).unwrap();

                match event.ref_type.as_ref() {
                    "RECORD" => {
                        seeding::records::seed_records(state.clone(), tki.clone(), payload)?;
                    }
                    "DOCUMENT" => {
                        seeding::documents::seed_documents(state.clone(), tki.clone(), payload)?;
                    }
                    "ALERT" => {
                        seeding::alerts::seed_alerts(state.clone(), tki.clone(), payload)?;
                    }
                    "ALARM" => {
                        seeding::alarms::seed_alarms(state.clone(), tki.clone(), payload)?;
                    }
                    "INDICATOR" => {
                        seeding::indicators::seed_indicators(state.clone(), tki.clone(), payload)?;
                    }
                    "INDICATOR_GROUP" => {
                        seeding::indicator_groups::seed_indicator_groups(state.clone(), tki.clone(), payload)?;
                    }
                    "TEAM" => {
                        seeding::teams::seed_teams(state.clone(), tki.clone(), payload)?;
                    }
                    "TEAM_MESSAGE" => {
                        seeding::team_messages::seed_team_messages(state.clone(), tki.clone(), payload)?;
                    }
                    "FORM" => {
                        seeding::forms::seed_forms(state.clone(), tki.clone(), payload)?;
                    }
                    "RESOURCE" => {
                        seeding::resources::seed_resources(state.clone(), tki.clone(), payload)?;
                    }
                    "ASSIGNMENT" => {
                        seeding::resources::seed_resources(state.clone(), tki.clone(), payload)?;
                    }
                    "LOCATION" => {
                        seeding::locations::seed_locations(state.clone(), tki.clone(), payload)?;
                    }
                    "LOCATION_TYPE" => {
                        seeding::location_types::seed_location_types(state.clone(), tki.clone(), payload)?;
                    }
                    "ORGANIZATION" => {
                        seeding::organizations::seed_organizations(state.clone(), tki.clone(), payload)?;
                    }
                    "ROLE" => {
                        seeding::roles::seed_roles(state.clone(), tki.clone(), payload)?;
                    }
                    "USER" => {
                        seeding::users::seed_users(state.clone(), tki.clone(), payload)?;
                    }
                    "DEVICE" => {
                        seeding::devices::seed_devices(state.clone(), tki.clone(), payload)?;
                    }
                    "INTERVAL" => {
                        seeding::intervals::seed_intervals(state.clone(), tki.clone(), payload)?;
                    }
                    _ => {}
                }
            }
            _ => {}
        };
    }

    Ok(true)
}

