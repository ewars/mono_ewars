use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::config::{Account};

#[derive(Deserialize, Clone)]
pub struct SwapAccount {
    pub uuid: String,
    pub tki: String,
}

#[derive(Serialize, Clone)]
pub struct AddAccountSuccess {
    pub success: bool,
    pub accounts: Vec<Account>,
}

pub fn swap_account(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let mut m_app = app.borrow_mut();

    let cmd: SwapAccount = match json::from_value(args) {
        Ok(res) => res,
        Err(err) => {
            return Err(err.to_string());
        }
    };

    m_app.swap_account(cmd.uuid, cmd.tki).unwrap();

    Ok(json::to_value(true).unwrap())
}



pub fn add_accounts(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let mut m_app = app.borrow_mut();

    match m_app.add_accounts(args) {
        Ok(_) => Ok(json::to_value(AddAccountSuccess {
            success: true,
            accounts: m_app.accounts.clone(),
        }).unwrap()),
        Err(err) => Err(err.to_string())
    }
}
