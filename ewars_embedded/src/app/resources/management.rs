use std::cell::RefCell;
use std::rc::Rc;

use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

use crate::models::resource::Resource;
use crate::state::ApplicationState;


pub fn create_resource(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Resource = json::from_value(args).unwrap();

    let sql = r#"
        INSERT INTO resources
        (uuid, name, status, version, description, content_type, shared, layout, data, style, variables, permissions, created_by, modified_by)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
    "#.to_owned();

    let new_uuid = Uuid::new_v4().to_string();
    let ruuid = app.borrow().get_current_uuid();

    conn.execute(&sql, &[
        &new_uuid,
        &data.name,
        &data.status,
        &data.version,
        &data.description,
        &data.content_type,
        &data.shared,
        &data.layout,
        &data.data,
        &data.style,
        &data.variables,
        &data.permissions,
        &ruuid,
        &ruuid,
    ]).unwrap();

    let result: Resource = conn.query_row("SELECT * FROM resources WHERE uuid = ?", &[&new_uuid], |row| {
        Resource::from_row(row)
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}

pub fn update_resource(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Resource = json::from_value(args).unwrap();

    let sql = r#"
        UPDATE resources
            SET name = ?,
                description = ?,
                status = ?,
                version = ?,
                shared = ?,
                layout = ?,
                data = ?,
                style = ?,
                variables = ?,
                permissions = ?,
                modified = CURRENT_TIMESTAMP,
                modified_by = ?
        WHERE uuid = ?;
    "#.to_owned();

    let ruuid = app.borrow().get_current_uuid();

    conn.execute(&sql, &[
        &data.name,
        &data.description,
        &data.status,
        &data.version,
        &data.shared,
        &data.layout,
        &data.data,
        &data.style,
        &data.variables,
        &data.permissions,
        &ruuid,
        &data.uuid,
    ]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn delete_resource(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    conn.execute("DELETE FROM resources WHERE uuid = ?", &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}
