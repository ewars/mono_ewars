use std::cell::RefCell;
use std::rc::Rc;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;

use crate::state::ApplicationState;
use crate::models::query::{ResourceQuery, QueryResult};

#[derive(Debug, Deserialize, Serialize)]
pub struct Resource {
    pub uuid: String,
    pub name: String,
    pub version: i32,
    pub description: Option<String>,
    pub shared: bool,
    pub layout: Option<Value>,
    pub data: Option<Value>,
    pub style: Option<Value>,
    pub variables: Option<Value>,
}

#[derive(Debug, Serialize)]
pub struct DashboardRow {
    uuid: String,
    name: String,
    status: String,
    created: String,
    created_by: Option<String>,
    modified: String,
    modified_by: Option<String>,
}

impl DashboardRow {
    pub fn from_row(row: &Row) -> DashboardRow {
        DashboardRow {
            uuid: row.get(0),
            name: row.get(1),
            status: row.get(2),
            created: row.get(3),
            created_by: row.get(4),
            modified: row.get(5),
            modified_by: row.get(6),
        }
    }
}

pub fn query_resources(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let resource_type: String = json::from_value(args).unwrap();
    let uid = app.borrow().get_current_uuid();

    let mut stmt = conn.prepare(r#"
        SELECT uuid, name, version, description, shared, layout, data, style, variables
        FROM resources
        WHERE content_type = ?
            AND created_by = ?;
    "#).unwrap();

    let rows = stmt.query_map(&[
        &resource_type,
        &uid,
    ], |row| {
        Resource {
            uuid: row.get(0),
            name: row.get(1),
            version: row.get(2),
            description: row.get(3),
            shared: row.get(4),
            layout: row.get(5),
            data: row.get(6),
            style: row.get(7),
            variables: row.get(8),
        }
    }).unwrap();

    let results: Vec<Resource> = rows.map(|x| x.unwrap()).collect();

    Ok(json::to_value(results).unwrap())
}

pub fn query_dashboards(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let query: ResourceQuery = json::from_value(args).unwrap();

    let mut sql: Vec<String> = vec![
        "SELECT".to_string(),
        "f.uuid,".to_string(),
        "f.name,".to_string(),
        "f.status,".to_string(),
        "f.created,".to_string(),
        "f.created_by,".to_string(),
        "f.modified,".to_string(),
        "f.modified_by".to_string(),
        "FROM resources AS f".to_string(),
    ];

    let mut sql_count: Vec<String> = vec![
        "SELECT".to_string(),
        "COUNT(f.uuid) AS total_records".to_string(),
        "FROM resources AS f".to_string(),
    ];

    let mut wheres: Vec<String> = vec![
        "f.content_type = 'DASHBOARD'".to_string(),
    ];
    let mut orders: Vec<String> = Vec::new();

    for (key, val) in query.filters.clone() {
        let (cmp, el_type, val) = query.filters.get(&key).unwrap();

        let mut flt_str: Vec<String> = Vec::new();

        flt_str.push(format!("f.{}", key));

        match cmp.as_ref() {
            "eq" => {
                flt_str.push("=".to_string());
            },
            "neq" => {
                flt_str.push("!=".to_string());
            },
            _ => {
                flt_str.push("=".to_string());
            }
        }

        flt_str.push(format!("'{}'", val));

        wheres.push(flt_str.join(" "));
    }


    for (key, val) in query.orders.clone() {
        let (col_type, dir) = query.orders.get(&key).unwrap();

        let mut ord_str: Vec<String> = Vec::new();

        ord_str.push(format!("f.{}", key));

        ord_str.push(dir.to_string());

        orders.push(ord_str.join(" "));
    }

    if wheres.len() > 0 {
        sql.push("WHERE".to_string());
        sql_count.push("WHERE".to_string());

        let mut sql_f = true;
        for item in wheres {
            if sql_f != true {
                sql.push("AND".to_string());
                sql_count.push("AND".to_string());
            }
            sql.push(item.clone());
            sql_count.push(item.clone());
            sql_f = false;
        }
    }

    if orders.len() > 0 {
        sql.push("ORDER BY".to_string());
        sql.push(orders.join(", "));
    }

    sql.push(format!("LIMIT {}", query.limit));
    sql.push(format!("OFFSET {}", query.offset));

    let c_sql = sql.join("\n");
    let c_sql_count = sql_count.join("\n");

    let mut stmt = match conn.prepare(&c_sql) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            return Err(format!("ERROR"));
        }
    };

    let rows = stmt.query_map(&[], |row| {
        DashboardRow::from_row(row)
    }).unwrap();

    let results: Vec<DashboardRow> = rows.map(|x| x.unwrap()).collect();

    let counter: i32 = conn.query_row(&c_sql_count, &[], |row| {
        row.get(0)
    }).unwrap();

    let qr = QueryResult {
        records: json::to_value(results).unwrap(),
        count: counter,
    };

    Ok(json::to_value(qr).unwrap())
}

pub fn get_resource(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    let result: Resource = conn.query_row(r#"
        SELECT uuid, name, version, description, shared, layout, data, style, variables
        FROM resources
        WHERE uuid = ?;
    "#, &[&id], |row| {
        Resource {
            uuid: row.get(0),
            name: row.get(1),
            version: row.get(2),
            description: row.get(3),
            shared: row.get(4),
            layout: row.get(5),
            data: row.get(6),
            style: row.get(7),
            variables: row.get(8),
        }
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}
