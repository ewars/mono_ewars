use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use rusqlite::{Row, NO_PARAMS};

use crate::state::ApplicationState;

static SQL_GET_RECORDS: &'static str = r#"
    SELECT COUNT(r.uuid), strftime('%Y-%m-%d', r.submitted) AS submitted
    FROM records AS r
        LEFT JOIN locations AS l ON l.uuid = json_extract(r.data, '$.__lid__')
    WHERE CAST(l.lineage AS TEXT) LIKE '%{lid}%'
    GROUP BY strftime('%Y-%m-%d', r.submitted);
"#;

// Get a data series of record counts for a location by submitted date
pub fn get_records_trend(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let lid: String = json::from_value(args).unwrap();

    let mut sql: String = SQL_GET_RECORDS.to_string();
    sql = sql.replace("{lid}", &lid);
    let mut stmt = conn.prepare(&sql).unwrap();

    let rows = stmt.query_map(NO_PARAMS, |row| {
        (row.get(1), row.get(0))
    }).unwrap();

    let results: Vec<(String, i32)> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}

