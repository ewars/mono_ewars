use std::collections::HashSet;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use rusqlite::{Row, NO_PARAMS};
use rusqlite::types::{ToSql};

use std::rc::Rc;
use std::cell::RefCell;

use sonomalib::models::{Location, LocationTreeNode};
use sonomalib::traits::{Queryable};

use crate::state::ApplicationState;

pub fn query_locations(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    unimplemented!();
}

static SQLITE_GET_LOCATION_NODE: &'static str = r#"
    SELECT * FROM locations WHERE uuid = ?;
"#;

pub fn get_location(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: Uuid = json::from_value(args).unwrap();

    match Location::get_by_id(&conn, "", &id) {
        Ok(Some(res)) => Ok(json::to_value(res).unwrap()),
        Ok(Option::None) => bail!("ERR_LOC_NOT_FOUND"),
        Err(err) => bail!(err)
    }
}

pub fn get_children(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    let mut stmt = conn.prepare(r#"
        SELECT l.uuid,
            l.name,
            l.status,
            lt.name AS lti_name,
            l.lti,
            l.lineage,
            l.pcode,
            l.codes,
            l.groups,
            (SELECT COUNT(c.uuid) FROM locations AS c WHERE c.pid = l.uuid)
        FROM locations AS l
            LEFT JOIN location_types AS lt ON lt.uuid = l.lti
        WHERE l.pid = ?
            AND l.status = 'ACTIVE';
    "#).unwrap();

    let rows = stmt.query_map(&[&id.to_string() as &ToSql], |row| {
        LocationTreeNode::from(row)
    })?;
    let items: Vec<LocationTreeNode> = rows.map(|x| x.unwrap()).collect();
    Ok(json::to_value(items).unwrap())
}

static SQLITE_GET_ROOT: &'static str = r#"
        SELECT l.uuid,
               l.name,
               l.lti,
               lt.name AS lti_name,
               l.pid,
               l.pcode,
               l.codes,
               l.lineage,
               l.status,
               l.groups,
                (SELECT COUNT(*) FROM locations AS p WHERE p.pid = l.uuid) AS child_count
        FROM locations AS l 
            LEFT JOIN location_types AS lt ON lt.uuid = l.lti
        WHERE l.pid IS NULL
"#;

pub fn get_location_root(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQLITE_GET_ROOT)?;

    let rows = stmt.query_map(NO_PARAMS, |row| {
        LocationTreeNode::from(row)
    })?;

    let items: Vec<LocationTreeNode> = rows.map(|x| x.unwrap()).collect();
    Ok(json::to_value(items).unwrap())
}


pub fn get_group_locations(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    unimplemented!()
}

type StringNames = Vec<String>;

pub fn get_location_groups(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare("SELECT groups FROM locations WHERE json_array_length(groups) > 0;")?;

    let rows = stmt.query_map(NO_PARAMS, |row| {
        json::from_value::<StringNames>(row.get(0)).unwrap()
    }).unwrap();

    let mut items: HashSet<String> = HashSet::new();

    for item in rows {
        let node: StringNames = item.unwrap();

        for item in node {
            items.insert(item);
        }
    }

    let mut results: Vec<String> = Vec::new();

    for i in items.drain() {
        results.push(i);
    }

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}
