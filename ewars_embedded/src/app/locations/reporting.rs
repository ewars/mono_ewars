use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use rusqlite::{Row, NO_PARAMS};
use rusqlite::types::{ToSql};
use uuid::Uuid;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

static SQL_GET_LOCATION: &'static str = r#"
    SELECT l.uuid, l.name, lt.name AS lti_name, l.lti, l.status, l.pcode, l.codes, l.lineage
    FROM locations AS l
        LEFT JOIN location_types AS lt ON l.lti = lt.uuid
    WHERE l.uuid = ?;
"#;

static SQL_GET_LINEAGE: &'static str = r#"
    SELECT l.uuid, l.name, lt.name AS lti_name
    FROM locations AS l
        LEFT JOIN location_types AS lt ON lt.uuid = l.lti
    WHERE l.uuid IN (?);
"#;

static SQL_GET_FORMS: &'static str = r#"
    SELECT f.uuid,
            f.name,
            f.status,
            f.features,
            json_extract(f.definition, '$.definition') AS definition,
            r.uuid AS r_uuid,
            r.start_date,
            r.end_date
    FROM forms AS f
        LEFT JOIN reporting AS r ON r.fid = f.uuid AND r.lid = ?
    WHERE json_extract(f.definition, '$.definition.__lid__.lti') == ?;
"#;

static SQL_GET_ALARMS: &'static str = r#"
    SELECT al.uuid,
        al.name,
        al.status,
        (SELECT COUNT(a.uuid) FROM alerts AS a WHERE a.alid = al.uuid AND a.lid = ? AND a.status = 'OPEN') AS alert_count
    FROM alarms AS al
    WHERE al.status = 'ACTIVE';
"#;

static SQL_GET_USERS: &'static str = r#"
    SELECT u.uuid, u.name, u.email, u.role, o.name AS org_name
    FROM assignments AS a
        LEFT JOIN users AS u ON u.uuid = a.uid
        LEFT JOIN organizations AS o ON o.uuid = u.org_id
    WHERE a.lid = ?;
"#;

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT a.uuid, a.fid, a.lid, a.assign_type, a.status
    FROM assignments AS a
    WHERE a.uid = ?;
"#;

#[derive(Debug, Serialize, Deserialize)]
pub struct Form {
    pub uuid: String,
    pub name: Value,
    pub status: String,
    pub features: Value,
    pub definition: Value,
    pub reporting_uuid: Option<String>,
    pub start_date: Option<String>,
    pub end_date: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Alarm {
    pub uuid: String,
    pub name: String,
    pub status: String,
    pub alert_count: i32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LineageItem {
    pub uuid: String,
    pub name: Value,
    pub lti_name: Value,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocationUser {
    pub uuid: String,
    pub name: String,
    pub email: String,
    pub role: String,
    pub org_name: Option<Value>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Assignment {
    pub uuid: String,
    pub fid: String,
    pub lid: String,
    pub assign_type: String,
    pub status: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ReportingContext {
    pub uuid: String,
    pub name: Value,
    pub lti_name: Value,
    pub lti: String,
    pub status: String,
    pub pcode: Option<String>,
    pub codes: Option<Value>,
    pub forms: Vec<Form>,
    pub alarms: Vec<Alarm>,
    pub lineage: Vec<String>,
    pub ancestors: HashMap<String, LineageItem>,
    pub users: Vec<LocationUser>,
    pub assignments: Vec<Assignment>,
}

pub fn get_context(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let location_id: Uuid = json::from_value(args)?;

    // Get the location itself and instantiate the reporting context
    let mut location: ReportingContext = conn.query_row(&SQL_GET_LOCATION, &[&location_id.to_string() as &ToSql], |row| {
        let lineage: Vec<String> = json::from_value(row.get(7)).unwrap();
        ReportingContext {
            uuid: row.get(0),
            name: row.get(1),
            lti_name: row.get(2),
            lti: row.get(3),
            status: row.get(4),
            pcode: row.get(5),
            codes: row.get(6),
            forms: Vec::new(),
            alarms: Vec::new(),
            lineage,
            ancestors: HashMap::new(),
            users: Vec::new(),
            assignments: Vec::new(),
        }
    }).unwrap();


    let mut stmt = conn.prepare(&SQL_GET_ASSIGNMENTS).unwrap();
    let rows = stmt.query_map(&[&uid.to_string() as &ToSql], |row| {
        Assignment {
            uuid: row.get(0),
            fid: row.get(1),
            lid: row.get(2),
            assign_type: row.get(3),
            status: row.get(4),
        }
    }).unwrap();
    location.assignments = rows.map(|x| x.unwrap()).collect();


    let mut uuids: String = "'".to_string();
    uuids = uuids + &location.lineage.join("','");
    uuids = uuids + "'";

    let mut lineage_sql = SQL_GET_LINEAGE.to_string();
    lineage_sql = lineage_sql.replace("?", &uuids);

    let mut stmt = conn.prepare(&lineage_sql).unwrap();
    let rows = stmt.query_map(NO_PARAMS, |row| {
        LineageItem {
            uuid: row.get(0),
            name: row.get(1),
            lti_name: row.get(2),
        }
    }).unwrap();

    for item in rows {
        let ancestor: LineageItem = item.unwrap();
        location.ancestors.insert(ancestor.uuid.clone(), ancestor);
    }

    let mut stmt = conn.prepare(&SQL_GET_FORMS).unwrap();
    let rows = stmt.query_map(&[&location_id.to_string() as &ToSql, &location.lti], |row| {
        Form {
            uuid: row.get(0),
            name: row.get(1),
            status: row.get(2),
            features: row.get(3),
            definition: row.get(4),
            reporting_uuid: row.get(5),
            start_date: row.get(6),
            end_date: row.get(7),
        }
    }).unwrap();
    location.forms = rows.map(|x| x.unwrap()).collect();

    let mut stmt = conn.prepare(&SQL_GET_ALARMS).unwrap();
    let rows = stmt.query_map(&[&location_id.to_string() as &ToSql], |row| {
        Alarm {
            uuid: row.get(0),
            name: row.get(1),
            status: row.get(2),
            alert_count: row.get(3),
        }
    }).unwrap();
    location.alarms = rows.map(|x| x.unwrap()).collect();

    let mut stmt = conn.prepare(&SQL_GET_USERS)?;
    let rows = stmt.query_map(&[&location_id.to_string() as &ToSql], |row| {
        LocationUser {
            uuid: row.get(0),
            name: row.get(1),
            email: row.get(2),
            role: row.get(3),
            org_name: row.get(4),
        }
    }).unwrap();
    let mut users: Vec<LocationUser> = rows.map(|x| x.unwrap()).collect();
    users.dedup_by_key(|x| x.uuid.clone());
    location.users = users;


    // Get the lineage for the location

    let output: Value = json::to_value(location).unwrap();
    Ok(output)
}
