use std::rc::Rc;
use std::collections::{HashMap};
use std::cell::RefCell;

use uuid::Uuid;
use failure::Error;
use serde_json as json;
use serde_json::Value;

use rusqlite::types::{ToSql};
use rusqlite::{Row, NO_PARAMS};

use crate::state::ApplicationState;

static SQL_GET_ALERTS: &'static str = r#"
    SELECT a.uuid, a.status, a.eid, strftime('%Y-%m-%d', a.alert_date), a.lid, a.stage, a.raised
    FROM alerts AS a
    WHERE a.lid = ? AND a.status = 'OPEN' AND a.alid = ?;
"#;

static SQL_GET_LOC_ALERTS: &'static str = r#"
    SELECT COUNT(a.uuid) total, strftime('%Y-%m-%d', a.raised) AS raised
    FROM alerts AS a
        LEFT JOIN locations AS l ON l.uuid = a.lid
    WHERE CAST(l.lineage AS TEXT) LIKE '%{LID}%'
    GROUP BY strftime('%Y-%m-%d', a.raised);
"#;

#[derive(Debug, Serialize, Deserialize)]
pub struct Query {
    pub lid: String,
    pub aid: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Alert {
    pub uuid: String,
    pub status: String,
    pub eid: Option<String>,
    pub alert_date: Option<String>,
    pub lid: String,
    pub stage: String,
    pub raised: String,
}

impl Alert {
    pub fn from_row(row: &Row) -> Alert {
        Alert {
            uuid: row.get(0),
            status: row.get(1),
            eid: row.get(2),
            alert_date: row.get(3),
            lid: row.get(4),
            stage: row.get(5),
            raised: row.get(6),
        }
    }
}


// Get OPEN alerts specific to a location and alarm
pub fn get_specific_alerts(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let qr: Query = json::from_value(args).unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ALERTS).unwrap();

    let rows = stmt.query_map(&[&qr.lid, &qr.aid], |row| {
        Alert::from_row(row)
    }).unwrap();

    let results: Vec<Alert> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}

// Get a general trend of alerts triggered for a given location
pub fn get_trend(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let lid: String = json::from_value(args).unwrap();

    let mut sql = SQL_GET_LOC_ALERTS.to_string();
    sql = sql.replace("{LID}", &lid);
    let mut stmt = conn.prepare(&sql).unwrap();

    let rows = stmt.query_map(NO_PARAMS, |row| {
        (row.get(1), row.get(0))
    }).unwrap();

    let results: Vec<(String, i32)> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(results).unwrap();
    Ok(output)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocationAlerts {
    pub lid: Uuid,
    pub aid: Uuid,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocationAlertsResult {
    pub risk_low: i32,
    pub risk_moderate: i32,
    pub risk_high: i32,
    pub risk_severe: i32,
    pub total_open: i32,
    pub verified: i32,
    pub unverified: i32,
    pub series: Vec<(String, i32)>,
}

static SQL_GET_AGG_ALERTS: &'static str = r#"
    SELECT a.uuid, a.status, a.stage, a.alert_date, IFNULL(json_extract(a.data, '$.risk'), 'UNSET') AS risk
    FROM alerts As a
        LEFT JOIN locations As l ON l.uuid = a.lid
        LEFT JOIN alarms AS al ON al.uuid = a.alid
    WHERE al.uuid = ?
        AND l.lineage LIKE '%{LID}%'
        AND l.status = 'ACTIVE'
        AND al.status = 'ACTIVE'
    ORDER BY a.alert_date;
"#;

pub struct LocationAlert {
    pub uuid: Uuid,
    pub status: String,
    pub stage: String,
    pub alert_date: String,
    pub risk: String,
}

pub fn get_location_alert_overview(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let query: LocationAlerts = json::from_value(args).unwrap();
    
    let mut sql: String = SQL_GET_AGG_ALERTS.to_string();
    sql = sql.replace("{LID}", &query.lid.to_string());
    let mut stmt = conn.prepare(&sql)?;

    let rows = stmt.query_map(&[
        &query.aid.to_string() as &ToSql,
    ], |row| {
        LocationAlert {
            uuid: opt_uuid!(row, 0).unwrap(),
            status: row.get(1),
            stage: row.get(2),
            alert_date: row.get(3),
            risk: row.get(4),
        }
    }).unwrap();

    let items: Vec<LocationAlert> = rows.map(|x| x.unwrap()).collect();

    let mut risk_low: i32 = 0;
    let mut risk_moderate: i32 = 0;
    let mut risk_high: i32 = 0;
    let mut risk_severe: i32 = 0;
    let open_alerts: i32 = items.len() as i32;
    let mut verified: i32 = 0;
    let mut unverified: i32 = 0;

    let mut date_counts: HashMap<String, i32> = HashMap::new();

    for alert in &items {
        match alert.risk.as_ref() {
            "HIGH" => {
                risk_high += 1;
            },
            "MODERATE" => {
                risk_moderate += 1;
            },
            "SEVERE" => {
                risk_severe += 1;
            },
            "LOW" => {
                risk_low += 1;
            },
            _ => {}
        }

        match alert.stage.as_ref() {
            "VERIFICATION" => {
                unverified += 1;
            },
            _ => {
                verified += 1;
            }
        }

        date_counts.entry(alert.alert_date.clone())
            .and_modify(|x| *x += 1)
            .or_insert(1);

    }

    let mut time_series: Vec<(String, i32)> = Vec::new();

    for (key, value) in date_counts {
        time_series.push((key, value));
    }

    let result = LocationAlertsResult {
        risk_low,
        risk_moderate,
        risk_high,
        risk_severe,
        total_open: open_alerts,
        verified,
        unverified,
        series: time_series,
    };

    Ok(json::to_value(result).unwrap())
}
