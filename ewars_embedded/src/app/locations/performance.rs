use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;
use chrono::prelude::*;

use rusqlite::Row;
use rusqlite::types::{ToSql};
use sonomalib::models::{Field};

use crate::utils::date_utils;
use crate::state::ApplicationState;

#[derive(Debug, Deserialize)]
struct Query {
    pub uuid: Uuid,
    pub form_id: Uuid,
}

#[derive(Debug, Deserialize, Serialize)]
struct FormDefinition {
    definition: HashMap<String, Field>,
    constraints: Option<Vec<String>>,
    workflow: Option<Value>,
}

#[derive(Debug, Deserialize, Serialize)]
struct Overdue {
    interval: String,
    threshold: u64,
}

#[derive(Debug, Deserialize, Serialize)]
struct FormReporting {
    pub definition: HashMap<String, Field>,
    pub overdue: Overdue,
    pub start_date: String,
    pub end_date: Option<String>,
}

#[derive(Debug, Deserialize, Clone)]
struct QueryItem {
    uuid: String,
    submitted: DateTime<Utc>,
    data_date: Option<DateTime<Utc>>,
}

pub type IsLate = bool;
pub type IsMissing = bool;

#[derive(Debug, Serialize)]
pub struct TimelinessResult {
    pub results: Vec<(String, IsLate)>,
    pub series: Vec<String>,
    // list of reporting dates
    pub expected: u64,
    pub lates: u64,
    pub start_date: String,
    pub end_date: String,
    pub erroneous: u64,
}

#[derive(Debug, Serialize)]
pub struct CompletenessResult {
    pub results: Vec<(String, String, String)>,
    pub series: Vec<String>,
    pub expected: u64,
    pub submitted: u64,
    pub start_date: String,
    pub end_date: String,
    pub erroneous: u64,
}

impl CompletenessResult {
    pub fn to_json(&self) -> Value {
        json::to_value(self).unwrap()
    }
}

impl TimelinessResult {
    pub fn to_json(&self) -> Value {
        json::to_value(self).unwrap()
    }
}

impl QueryItem {
    // Check if this record is overduie
    pub fn is_late(&self, overdue: &u64) -> bool {
        false
    }

    // Check if this records date is in the valid ranges of dates for the form interval
    pub fn is_reporting_date(&self, report_dates: &Vec<Date<Utc>>) -> bool {
        if self.data_date.is_some() {
            let actual_date: DateTime<Utc> = self.data_date.clone().unwrap();
            let result: Option<Date<Utc>> = report_dates.iter().find(|&&x| x == actual_date.date()).cloned();

            return result.is_some();
        }

        false
    }

    pub fn get_str_date(&self) -> String {
        self.data_date.unwrap().format("%Y-%m-%d").to_string()
    }
}

static SQL_GET_FORM_DEFINITION: &'static str = r#"
    SELECT f.definition,
        json_extract(f.features, '$.OVERDUE') as overdue,
        r.start_date,
        r.end_date
    FROM forms AS f
        LEFT JOIN reporting AS r ON r.fid = f.uuid
    WHERE r.lid = ?
        AND f.uuid = ?;
"#;

pub fn get_location_timeliness(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let qr: Query = json::from_value(args).unwrap();

    let fr: FormReporting = match conn.query_row(&SQL_GET_FORM_DEFINITION, &[&qr.uuid.to_string() as &ToSql, &qr.form_id.to_string()], |row| {
        let definition: HashMap<String, Field> = json::from_value(row.get(0)).unwrap();
        let overdue: Overdue = json::from_value(row.get(1)).unwrap();
        FormReporting {
            definition,
            overdue,
            start_date: row.get(2),
            end_date: row.get(3),
        }
    }) {
        Ok(res) => res,
        Err(err) => {
            return Ok(json::to_value(false).unwrap());
        }
    };

    let mut date_field: Option<(String, Field)> = None;
    let mut location_field: Option<(String, Field)> = None;
    for (key, value) in &fr.definition {
        match value.field_type.as_ref() {
            "interval" => {
                date_field = Some((key.clone(), value.clone()));
            }
            "location" => {
                location_field = Some((key.clone(), value.clone()));
            }
            _ => {}
        }
    }

    let mut sql: Vec<String> = vec![
        "SELECT".to_string(),
        "r.uuid,".to_string(),
        "r.submitted,".to_string(),
    ];

    // Add the select for the canonical date of the record
    if date_field.is_some() {
        // We have a date field
        sql.push(format!("json_extract(r.data, '{}') AS data_date", date_field.clone().unwrap().0));
    } else {
        // Add a placeholder so that we can use the same struct.
        sql.push("'' AS date_date".to_string());
    }

    sql.push("FROM records AS r".to_string());
    sql.push("WHERE r.fid = ?".to_string());
    sql.push("AND r.status = 'SUBMITTED'".to_string());

    if location_field.is_some() {
        sql.push(format!("AND json_extract(r.data, '{}') = '{}'", location_field.clone().unwrap().0, qr.uuid.clone()));
    }

    let end_sql: String = sql.join(" ").to_string();
    let mut stmt = conn.prepare(&end_sql).unwrap();

    let rows = stmt.query_map(&[&qr.form_id.to_string() as &ToSql], |row| {
        QueryItem {
            uuid: row.get(0),
            submitted: row.get(1),
            data_date: row.get(2),
        }
    }).unwrap();

    let start_date: DateTime<Utc> = Utc.datetime_from_str(&fr.start_date, "%Y-%m-%d 09:10:11").unwrap();
    let mut end_date: DateTime<Utc> = Utc::now();
    if let Some(ref x) = fr.end_date {
        end_date = Utc.datetime_from_str(&x, "%Y-%m-%d 09:10:11").unwrap();
    }

    let items: Vec<QueryItem> = rows.map(|x| x.unwrap()).collect();

    // Get the reporting interval from the form
    let reporting_interval: String = date_field.unwrap().1.date_type.clone().unwrap();
    // TODO: Check if it's a custom interval, if it is we need to get the definition from the database

    // Get the actual reporting intervals expected for the form, start and end date
    let reporting_dates: Vec<Date<Utc>> = date_utils::get_reporting_intervals(&start_date, &end_date, &reporting_interval).unwrap();

    if reporting_dates.len() <= 0 {
        // TODO: We don't actually have any reporting dates
        eprintln!("NO REPORTING DATES");
    }

    // Filter the list to records which correlate to actual reporting interval dates, ignore items which
    // fall off the spectrum of intervals as they're considered incorrect
    let actuals: Vec<QueryItem> = items.iter().filter(|x| x.is_reporting_date(&reporting_dates)).cloned().collect();

    // Get records which are outside of the reporting period, these can either be because they are for dates before/after the reporting
    // period or because they fall on days which aren't actual proper reporting dates based on the interval
    let erroneous: Vec<QueryItem> = items.iter().filter(|x| !x.is_reporting_date(&reporting_dates)).cloned().collect();

    let overdue_days: u64 = fr.overdue.threshold.clone();

    let lates: Vec<QueryItem> = actuals.iter().filter(|x| x.is_late(&overdue_days)).cloned().collect();

    let mut record_results: Vec<(String, u64)> = Vec::new();


    let series: Vec<String> = reporting_dates.iter().map(|x| x.format("%Y-%m-%d").to_string()).collect();
    let results: Vec<(String, bool)> = actuals.iter()
        .map(|x| (x.data_date.unwrap().format("%Y-%m-%d").to_string(), x.is_late(&overdue_days)))
        .collect();

    let mut report = TimelinessResult {
        results: results,
        series: series,
        expected: reporting_dates.len() as u64,
        lates: lates.len() as u64,
        start_date: start_date.format("%Y-%m-%d").to_string(),
        end_date: end_date.format("%Y-%m-%d").to_string(),
        erroneous: erroneous.len() as u64,
    };

    Ok(report.to_json())
}

pub fn get_location_completeness(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let qr: Query = json::from_value(args).unwrap();

    // Get the definition for the form
    let mut sql = r#"
        SELECT f.definition,
                json_extract(f.features, '$.OVERDUE') AS overdue,
                r.start_date,
                r.end_date
        FROM forms AS f
            LEFT JOIN reporting AS r ON r.fid = f.uuid
        WHERE r.lid = ?
            AND f.uuid = ?;
    "#.to_owned();

    let fr: FormReporting = match conn.query_row(&sql, &[&qr.uuid.to_string() as &ToSql, &qr.form_id.to_string()], |row| {
        let definition: HashMap<String, Field> = json::from_value(row.get(0)).unwrap();
        let overdue: Overdue = json::from_value(row.get(1)).unwrap();
        FormReporting {
            definition,
            overdue,
            start_date: row.get(2),
            end_date: row.get(3),
        }
    }) {
        Ok(res) => res,
        Err(err) => {
            return Ok(json::to_value(false).unwrap());
        }
    };

    let mut date_field: Option<(String, Field)> = None;
    let mut location_field: Option<(String, Field)> = None;
    for (key, value) in &fr.definition {
        match key.as_ref() {
            "__dd__" => {
                date_field = Some((key.clone(), value.clone()));
            }
            "__lid__" => {
                location_field = Some((key.clone(), value.clone()));
            }
            _ => {}
        }
    }

    let mut sql: Vec<String> = vec![
        "SELECT".to_string(),
        "r.uuid,".to_string(),
        "r.submitted,".to_string(),
    ];

    // Add the select for the canonical date of the record
    if date_field.is_some() {
        // We have a date field
        sql.push(format!("IFNULL(json_extract(r.data, '$.{}'), '') AS data_date", date_field.clone().unwrap().0));
    } else {
        // Add a placeholder so that we can use the same struct.
        sql.push("'NONE' AS date_date".to_string());
    }

    sql.push("FROM records AS r".to_string());
    sql.push("WHERE r.fid = ?".to_string());
    sql.push("AND r.status = 'SUBMITTED'".to_string());

    if location_field.is_some() {
        sql.push(format!("AND json_extract(r.data, '$.{}') = '{}'", location_field.clone().unwrap().0, qr.uuid.clone()));
    }

    let end_sql: String = sql.join(" ").to_string();
    let mut stmt = conn.prepare(&end_sql).unwrap();

    let rows = stmt.query_map(&[&qr.form_id.to_string() as &ToSql], |row| {
        let data_date: Option<String> = row.get(2);

        let mut act_data_date: Option<DateTime<Utc>> = None;
        if let Some(ref x) = data_date {
            act_data_date = Some(Utc.datetime_from_str(&format!("{} 09:10:11", x), "%Y-%m-%d %H:%M:%S").unwrap());
        }
        QueryItem {
            uuid: row.get(0),
            submitted: row.get(1),
            data_date: act_data_date,
        }
    }).unwrap();

    let start_date: DateTime<Utc> = Utc.datetime_from_str(&format!("{} 09:10:11", fr.start_date), "%Y-%m-%d %H:%M:%S").unwrap();
    let mut end_date: DateTime<Utc> = Utc::now();
    if let Some(ref x) = fr.end_date {
        end_date = Utc.datetime_from_str(&format!("{} 09:10:11", &x), "%Y-%m-%d %H:%M:%S").unwrap();
    }

    let items: Vec<QueryItem> = rows.map(|x| x.unwrap()).collect();

    // Get the reporting interval from the form
    let reporting_interval: String = date_field.unwrap().1.date_type.clone().unwrap();
    // TODO: Check if it's a custom interval, if it is we need to get the definition from the database

    // Get the actual reporting intervals expected for the form, start and end date
    let reporting_dates: Vec<Date<Utc>> = date_utils::get_reporting_intervals(&start_date, &end_date, &reporting_interval).unwrap();

    if reporting_dates.len() <= 0 {
        // TODO: We don't actually have any reporting dates
        eprintln!("NO REPORTING DATES");
    }

    // Filter the list to records which correlate to actual reporting interval dates, ignore items which
    // fall off the spectrum of intervals as they're considered incorrect
    let actuals: Vec<QueryItem> = items.iter().filter(|x| x.is_reporting_date(&reporting_dates)).cloned().collect();

    // Get records which are outside of the reporting period, these can either be because they are for dates before/after the reporting
    // period or because they fall on days which aren't actual proper reporting dates based on the interval
    let erroneous: Vec<QueryItem> = items.iter().filter(|x| !x.is_reporting_date(&reporting_dates)).cloned().collect();

    let mut record_results: Vec<(String, String, String)> = actuals.iter().map(|x| (x.data_date.unwrap().format("%Y-%m-%d").to_string(), x.uuid.clone(), x.submitted.format("%Y-%m-%d").to_string())).collect();


    let series: Vec<String> = reporting_dates.iter().map(|x| x.format("%Y-%m-%d").to_string()).collect();

    let len_record_results: u64 = record_results.len() as u64;
    let mut report = CompletenessResult {
        results: record_results,
        series: series,
        expected: reporting_dates.len() as u64,
        submitted: len_record_results,
        start_date: start_date.format("%Y-%m-%d").to_string(),
        end_date: end_date.format("%Y-%m-%d").to_string(),
        erroneous: erroneous.len() as u64,
    };

    Ok(report.to_json())
}

pub fn get_location_submitted(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let qr: Query = json::from_value(args).unwrap();

    // Get the definition for the form
    let mut sql = r#"
        SELECT f.definition,
                json_extract(f.features, '$.OVERDUE') AS overdue,
                r.start_date,
                r.end_date
        FROM forms AS f
            LEFT JOIN reporting AS r ON r.fid = f.uuid
        WHERE f.lid = ?
            AND f.uuid = ?;
    "#.to_owned();

    let fr: FormReporting = conn.query_row(&sql, &[&qr.uuid.to_string() as &ToSql, &qr.form_id.to_string()], |row| {
        let definition: HashMap<String, Field> = json::from_value(row.get(0)).unwrap();
        let overdue: Overdue = json::from_value(row.get(1)).unwrap();
        FormReporting {
            definition,
            overdue,
            start_date: row.get(1),
            end_date: row.get(2),
        }
    }).unwrap();

    let mut date_field: Option<(String, Field)> = None;
    let mut location_field: Option<(String, Field)> = None;
    for (key, value) in &fr.definition {
        match value.field_type.as_ref() {
            "interval" => {
                date_field = Some((key.clone(), value.clone()));
            }
            "location" => {
                location_field = Some((key.clone(), value.clone()));
            }
            _ => {}
        }
    }

    let mut sql: Vec<String> = vec![
        "SELECT".to_string(),
        "r.uuid,".to_string(),
        "r.submitted,".to_string(),
    ];

    // Add the select for the canonical date of the record
    if date_field.is_some() {
        // We have a date field
        sql.push(format!("json_extract(r.data, '{}') AS data_date", date_field.clone().unwrap().0));
    } else {
        // Add a placeholder so that we can use the same struct.
        sql.push("'' AS date_date".to_string());
    }

    sql.push("FROM records AS r".to_string());
    sql.push("WHERE r.fid = ?".to_string());
    sql.push("AND r.status = 'SUBMITTED'".to_string());

    if location_field.is_some() {
        sql.push(format!("AND json_extract(r.data, '{}') = '{}'", location_field.clone().unwrap().0, qr.uuid.clone()));
    }

    let end_sql: String = sql.join(" ").to_string();
    let mut stmt = conn.prepare(&end_sql).unwrap();

    let rows = stmt.query_map(&[&qr.form_id.to_string() as &ToSql], |row| {
        QueryItem {
            uuid: row.get(0),
            submitted: row.get(1),
            data_date: row.get(2),
        }
    }).unwrap();

    let start_date: DateTime<Utc> = Utc.datetime_from_str(&fr.start_date, "%Y-%m-%d 09:10:11").unwrap();
    let mut end_date: DateTime<Utc> = Utc::now();
    if let Some(ref x) = fr.end_date {
        end_date = Utc.datetime_from_str(&x, "%Y-%m-%d 09:10:11").unwrap();
    }

    let items: Vec<QueryItem> = rows.map(|x| x.unwrap()).collect();

    // Get the reporting interval from the form
    let reporting_interval: String = date_field.unwrap().1.interval.clone().unwrap();
    // TODO: Check if it's a custom interval, if it is we need to get the definition from the database

    // Get the actual reporting intervals expected for the form, start and end date
    let reporting_dates: Vec<Date<Utc>> = date_utils::get_reporting_intervals(&start_date, &end_date, &reporting_interval).unwrap();

    if reporting_dates.len() <= 0 {
        // TODO: We don't actually have any reporting dates
        eprintln!("NO REPORTING DATES");
    }

    // Filter the list to records which correlate to actual reporting interval dates, ignore items which
    // fall off the spectrum of intervals as they're considered incorrect
    let actuals: Vec<QueryItem> = items.iter().filter(|x| x.is_reporting_date(&reporting_dates)).cloned().collect();

    let submitted: u64 = actuals.len() as u64;

    Ok(json::to_value(submitted).unwrap())
}
