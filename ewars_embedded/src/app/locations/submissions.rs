use std::rc::Rc;
use std::cell::RefCell;

use rusqlite::Row;
use chrono::prelude::*;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::utils::date_utils;

static SQL_GET_REPORTING_PERIOD: &'static str = r#"
    SELECT r.uuid, r.start_date, r.end_date, json_extract(f.definition, '$.definition.__dd__.interval') AS interval
    FROM reporting AS r
        LEFT JOIN forms AS f ON f.uuid = r.fid
    WHERE r.lid = ? AND r.fid = ?;
"#;

static SQL_GET_RECORDS: &'static str = r#"
    SELECT uuid, json_extract(data, '$.__dd__') AS data_date, submitted
    FROM records
    WHERE fid = ?
        AND json_extract(data, '$.__lid__') = ?
        AND status = 'SUBMITTED';
"#;

#[derive(Debug, Deserialize, Serialize)]
pub struct QueryResult {
    pub uuid: String,
    pub fid: String,
    pub expected: i32,
    pub missing: i32,
    pub record_count: i32,
    pub erroneous: i32,
    pub record_dates: Vec<String>,
    pub start_date: String,
    pub end_date: String,
    pub records: Vec<DataPoint>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DataPoint {
    pub uuid: String,
    pub data_date: String,
    pub submitted: Option<DateTime<Utc>>,
}

impl DataPoint {
    pub fn is_reporting_date(&self, report_dates: &Vec<Date<Utc>>) -> bool {
        let actual_date: DateTime<Utc> = Utc.datetime_from_str(&format!("{} 09:10:11", self.data_date), "%Y-%m-%d %H:%M:%S").unwrap();
        let result: Option<Date<Utc>> = report_dates.iter().find(|&&x| x == actual_date.date()).cloned();

        result.is_some()
    }

    pub fn get_str_date(&self) -> String {
        self.data_date.clone()
    }
}

#[derive(Debug, Deserialize)]
pub struct ReportingPeriod {
    pub uuid: String,
    pub start_date: String,
    pub end_date: Option<String>,
    pub interval: String,
}

#[derive(Debug, Deserialize)]
pub struct SubmissionsQuery {
    pub lid: String,
    pub fid: String,
}

// Get a manifest of submissions for a location and form
pub fn get_submissions(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let qr: SubmissionsQuery = json::from_value(args).unwrap();

    let reporting: ReportingPeriod = conn.query_row(&SQL_GET_REPORTING_PERIOD, &[&qr.lid, &qr.fid], |row| {
        ReportingPeriod {
            uuid: row.get(0),
            start_date: row.get(1),
            end_date: row.get(2),
            interval: row.get(3),
        }
    }).unwrap();

    let start_date: DateTime<Utc> = Utc.datetime_from_str(&format!("{} 09:10:11", reporting.start_date), "%Y-%m-%d %H:%M:%S").unwrap();
    let mut end_date: DateTime<Utc> = Utc::now();
    if let Some(ref x) = reporting.end_date {
        end_date = Utc.datetime_from_str(&format!("{} 09:10:11", &x), "%Y-%m-%d %H:%M:%S").unwrap();
    }

    let reporting_dates: Vec<Date<Utc>> = date_utils::get_reporting_intervals(&start_date, &end_date, &reporting.interval).unwrap();

    if reporting_dates.len() <= 0 {
        eprintln!("This location and form have no reporting available");
    }

    let mut stmt = conn.prepare(&SQL_GET_RECORDS).unwrap();
    let rows = stmt.query_map(&[&qr.fid, &qr.lid], |row| {
        DataPoint {
            uuid: row.get(0),
            data_date: row.get(1),
            submitted: row.get(2),
        }
    }).unwrap();
    let records: Vec<DataPoint> = rows.map(|x| x.unwrap()).collect();

    let actuals: Vec<DataPoint> = records.iter().filter(|x| x.is_reporting_date(&reporting_dates)).cloned().collect();
    let expected: i32 = reporting_dates.len() as i32;
    let submitted: i32 = actuals.len() as i32;
    let missing: i32 = expected - submitted;

    let result = QueryResult {
        uuid: qr.lid.clone(),
        fid: qr.fid.clone(),
        expected,
        missing,
        record_count: submitted,
        erroneous: 0,
        record_dates: reporting_dates.iter().map(|x| x.format("%Y-%m-%d").to_string()).collect(),
        start_date: start_date.format("%Y-%m-%d").to_string(),
        end_date: end_date.format("%Y-%m-%d").to_string(),
        records,
    };

    let output: Value = json::to_value(result).unwrap();

    Ok(output)
}
