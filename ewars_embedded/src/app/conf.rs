use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;
use failure::Error;
use rusqlite::{NO_PARAMS};

use sonomalib::models::{Conf};

use crate::state::ApplicationState;

static GET_ALL_CONF: &'static str = r#"
    SELECT * FROM conf;
"#;

pub fn get(state: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = state.borrow().get_current_pool().unwrap().get().unwrap();
    let mut stmt = conn.prepare(&GET_ALL_CONF)?;

    let rows = stmt.query_map(NO_PARAMS, |row| {
        Conf::from(row)
    })?;

    let items: Vec<Conf> = rows.map(|x| x.unwrap()).collect();

    Ok(json::to_value(items).unwrap())
}
