use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;

use crate::models::organization::Organization;

pub fn create_organization(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Organization = json::from_value(args).unwrap();

    let sql = r#"
        INSERT INTO organizations
        (uuid, name, description, acronym, created_by, modified_by)
        VALUES (:uuid, :name, :description, :acronym, :created_by, :modified_by)
        RETURNING *;
    "#;

    let uid = app.borrow().get_current_uuid();

    conn.execute_named(&sql, &[
        (":uuid", &data.uuid),
        (":name", &data.name),
        (":description", &data.description),
        (":acronym", &data.acronym),
        (":created_by", &uid),
        (":modified_by", &uid),
    ]).unwrap();

    let result: Organization = conn.query_row("SELECT * FROM organizations WHERE uuid = ?", &[&data.uuid], |row| {
        Organization::from_row(row)
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}

pub fn delete_organization(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    conn.execute("DELETE FROM organizations WHERE uuid = ?", &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn update_organization(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Organization = json::from_value(args).unwrap();

    let sql = r#"
        UPDATE organizations
            SET name = :name,
                description = :description,
                acronym = :acronym,
                modified_by = :modified_by
        WHERE uuid = :uuid;
    "#;

    let uid = app.borrow().get_current_uuid();

    conn.execute_named(&sql, &[
        (":name", &data.name),
        (":description", &data.description),
        (":acronym", &data.acronym),
        (":modified_by", &uid),
        (":uuid", &data.uuid),
    ]).unwrap();

    Ok(json::to_value(true).unwrap())
}
