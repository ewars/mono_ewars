use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::location_type::LocationType;

pub fn get_location_type(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    let result: LocationType = conn.query_row("SELECT * FROM location_types WHERE uuid = ?", &[&id], |row| {
        LocationType::from_row(row)
    }).unwrap();

    Ok(json::to_value(result).unwrap())
}

pub fn get_available_location_types(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> { 
    unimplemented!()
}

pub fn query_location_types(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

