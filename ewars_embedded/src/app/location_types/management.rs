use std::rc::Rc;
use std::cell::RefCell;

use uuid::Uuid;
use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::location_type::LocationType;

pub fn update_group(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: LocationType = json::from_value(args).unwrap();
    let uid = app.borrow().get_current_uuid();

    let sql = r#"
        UPDATE lcoation_types
            SET name = :name,
                description = :description,
                restrictions = :restrictions,
                modified_by = :uid,
                modified = CURRENT_TIMESTAMP
            WHERE uuid = :uuid;
    "#;


    conn.execute_named(&sql, &[
        (":name", &data.name),
        (":description", &data.description),
        (":restrictions", &data.restrictions),
        (":modified_by", &uid),
        (":uuid", &data.uuid),
    ]).unwrap();

    Ok(json::to_value(true).unwrap())
}

pub fn create_group(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: LocationType = json::from_value(args).unwrap();
    let uid = app.borrow().get_current_uuid();

    let sql = r#"
        INSERT INTO location_types
        (uuid, name, description, restrictions, created_by, modified_by)
        VALUES (:uuid, :name, :description, :restrictions, :created_by, :modified_by)
        RETURNING *;
    "#;

    let new_uuid = Uuid::new_v4().to_string();

    conn.execute_named(&sql, &[
        (":uuid", &new_uuid),
        (":name", &data.name),
        (":description", &data.description),
        (":restrictions", &data.restrictions),
        (":created_by", &uid),
        (":modified_by", &uid),
    ]).unwrap();

    let result: LocationType = conn.query_row("SELECT * FROM location_types WHERE uuid = ?", &[&new_uuid], |row| {
        LocationType::from_row(row)
    }).unwrap();


    Ok(json::to_value(result).unwrap())
}

pub fn delete_group(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = json::from_value(args).unwrap();

    conn.execute("DELETE FROM location_types WHERE uuid = ?", &[&id]).unwrap();

    Ok(json::to_value(true).unwrap())
}
