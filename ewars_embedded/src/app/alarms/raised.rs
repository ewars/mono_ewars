use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;

use rusqlite::Row;

use crate::state::ApplicationState;

static SQL_GET_ALARMS_RAISED: &'static str = r#"
    SELECT uuid, strftime('%Y-%m-%d', raised), status
    FROM alerts
    WHERE alid = ?;
"#;

struct AlertQueryItem {
    uuid: String,
    triggered: String,
    status: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AlarmQueryResult {
    open_alerts: Vec<(String, i32)>,
    closed_alerts: Vec<(String, i32)>,
    triggered_alerts: Vec<(String, i32)>,
}

// Get some base series data about an alarm and the alerts that it contains
// used to power generic overview of alarms within a system
// NOTE: For a regional admin this needs to be filtered to only locations that they have access to
// and records need to be deduped by UUID if the user has overlapping assigned regions within
// a contingency
pub fn get_alarm_raised(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let aid: String = json::from_value(args).unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ALARMS_RAISED).unwrap();

    let rows = stmt.query_map(&[&aid], |row| {
        AlertQueryItem {
            uuid: row.get(0),
            triggered: row.get(1),
            status: row.get(2),
        }
    }).unwrap();

    let vec_alerts: Vec<AlertQueryItem> = rows.map(|x| x.unwrap()).collect();

    let mut trigger_dict: HashMap<String, i32> = HashMap::new();
    let mut closed_dict: HashMap<String, i32> = HashMap::new();
    let mut open_dict: HashMap<String, i32> = HashMap::new();

    // Iterate through the set of alerts and extrapolate two sets of data
    // one for alerts triggered and one for alerts closed
    for item in &vec_alerts {
        trigger_dict.entry(item.triggered.clone())
            .and_modify(|x| *x += 1)
            .or_insert(1);

        match item.status.as_ref() {
            "OPEN" => {
                open_dict.entry(item.triggered.clone())
                    .and_modify(|x| *x += 1)
                    .or_insert(1);
            },
            "CLOSED" => {
                open_dict.entry(item.triggered.clone())
                    .and_modify(|x| *x += 1)
                    .or_insert(1);
            },
            _ => {}
        }
    }

    let mut vec_triggered: Vec<(String, i32)> = Vec::new();
    for (key, value) in trigger_dict {
        vec_triggered.push((key, value));
    }

    let mut vec_closed: Vec<(String, i32)> = Vec::new();
    for (key, value) in closed_dict {
        vec_closed.push((key, value));
    }

    let mut vec_open: Vec<(String, i32)> = Vec::new();
    for (key, value) in open_dict {
        vec_open.push((key, value));
    }

    let result = AlarmQueryResult {
        open_alerts: vec_open,
        closed_alerts: vec_closed,
        triggered_alerts: vec_triggered,
    };

    let output: Value = json::to_value(result).unwrap();

    Ok(output)
}
