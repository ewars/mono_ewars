use std::rc::Rc;
use std::cell::RefCell;

use serde_json as json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::alarm::Alarm;


pub fn create_alarm(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn test_alarm(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}

pub fn update_alarm(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Alarm = json::from_value(args).unwrap();

    let mut sql = r#"
        UPDATE Alarms
            SET name = :name,
                definition = :definition,
                modified = CURRENT_TIMESTAMP,
                modified_by = :modified_by,
                workflow = :workflow,
                description = :description,
                status = :status

        WHERE uuid = :uuid;
    "#.to_owned();

    let uid = app.borrow().get_current_uuid();

    conn.execute_named(&sql, &[
        (":uuid", &data.name),
        (":definition", &data.definition),
        (":modified_by", &uid),
        (":uuid", &data.uuid),
        (":workflow", &data.workflow),
        (":description", &data.description),
        (":status", &data.status),
    ]).unwrap();

    let result: Alarm = conn.query_row("SELECT * FROM alarms WHERE uuid = ?", &[&data.uuid], |row| {
        Alarm::from_row(row)
    }).unwrap();


    let output: Value = json::to_value(result).unwrap();

    Ok(output)
}

pub fn get_guidance(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let alarm_id: String = json::from_value(args).unwrap();
//
//    let result: String = conn.query_row("SELECT guidance FROM alarms WHERE uuid = ?", &[&alarm_id], |row| {
//        row.get(0)
//    }).unwrap();
//
//    let output: Value = json::to_value(result).unwrap();

    let output: Value = json::to_value("HELLO".to_string()).unwrap();


    Ok(output)
}


pub fn delete_alarm(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let alarm_id: String = json::from_value(args).unwrap();

    // Need to delete all alerts associated with the alarm first
    conn.execute("DELETE FROM alerts WHERE alid = ?", &[&alarm_id]).unwrap();

    // Now delete the actual alarm
    conn.execute("DELETE FROM alarms WHERE uuid = ?", &[&alarm_id]).unwrap();

    Ok(json::to_value(true).unwrap())
}
