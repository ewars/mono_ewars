use std::cell::RefCell;
use std::rc::Rc;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use rusqlite::types::{ToSql};
use rusqlite::{NO_PARAMS};

use sonomalib::models::{Alarm};
use sonomalib::traits::{Queryable};

use crate::state::ApplicationState;

static SQL_GET_ALARMS_ACTIVE: &'static str = r#"
     SELECT * FROM alarms
     WHERE (status = 'ACTIVE' OR status = 'ARCHIVED');
"#;

// Get an alarm by its id
pub fn get_alarm(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: Uuid = json::from_value(args).unwrap();


    match Alarm::get_by_id(&conn, "", &id) {
        Ok(res) => {
            Ok(json::to_value(res).unwrap())
        },
        Err(err) => {
            eprintln!("{:?}", err);
            bail!(err);
        }
    }
}

static SQL_GET_ALL_ALARMS: &'static str = r#"
    SELECT * FROM alarms;
"#;

pub fn get_all_alarms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ALL_ALARMS)?;

    let rows = stmt.query_map(NO_PARAMS, |row| {
        Alarm::from(row)
    })?;
    let items: Vec<Alarm> = rows.map(|x| x.unwrap()).collect();
    Ok(json::to_value(items).unwrap())
}

pub fn get_available_alarms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ALARMS_ACTIVE)?;

    let rows = stmt.query_map(NO_PARAMS, |row| {
        Alarm::from(row)
    })?;
    let items: Vec<Alarm> = rows.map(|x| x.unwrap()).collect();
    Ok(json::to_value(items).unwrap())
}

pub fn get_active_alarms(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ALARMS_ACTIVE).unwrap();

    let rows = stmt.query_map(NO_PARAMS, |row| {
        Alarm::from(row)
    })?;
    let items: Vec<Alarm> = rows.map(|x| x.unwrap()).collect();
    Ok(json::to_value(items).unwrap())
}
