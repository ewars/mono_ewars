use std::rc::Rc;
use std::cell::RefCell;

use serde_json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::indicator::Indicator;

pub fn get_indicator(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = serde_json::from_value(args).unwrap();

    let result: Indicator = conn.query_row("SELECT * FROM indicators WHERE uuid = ?", &[&id], |row| {
        Indicator::from_row(row)
    }).unwrap();

    Ok(serde_json::to_value(result).unwrap())
}

pub fn query_indicators(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    unimplemented!()
}
