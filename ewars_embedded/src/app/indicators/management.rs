use std::rc::Rc;
use std::cell::RefCell;

use serde_derive;
use serde_json;
use serde_json::Value;

use crate::state::ApplicationState;
use crate::models::indicator::Indicator;

pub fn update_indicator(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Indicator = serde_json::from_value(args).unwrap();

    let sql = r#"
            UPDATE indicators
                SET name = :name,
                    group_id = :group_id,
                    status = :status,
                    description = :description,
                    itype = :itype,
                    icode = :icode,
                    permissions = :permissions,
                    modified_by = :uid
           WHERE uuid = :uuid;
        "#.to_owned();

    let uid = app.borrow().get_current_uuid();

    conn.execute_named(&sql, &[
        (":name", &data.name),
        (":group_id", &data.group_id),
        (":status", &data.status),
        (":description", &data.description),
        (":itype", &data.itype),
        (":icode", &data.icode),
        (":permissions", &data.permissions),
        (":modified_by", &uid),
    ]).unwrap();

    Ok(serde_json::to_value(true).unwrap())
}

pub fn delete_indicator(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = serde_json::from_value(args).unwrap();

    conn.execute("DELETE FROM indicators WHERE uuid = ?", &[&id]).unwrap();

    Ok(serde_json::to_value(true).unwrap())
}

pub fn create_indicator(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Indicator = serde_json::from_value(args).unwrap();

    let sql = r#"
        INSERT INTO indicators
        (uuid, group_id, name, status, description, itype, icode, permissions, created_by, modified_by)
        VALUES (:uuid, :group_id, :name, :status, :description. :itype, :icode, :permissions, :created_by, :modified_by)
        RETURNING *;
    "#;

    let uid = app.borrow().get_current_uuid();

    let result: Indicator = conn.query_row_named(&sql, &[
        (":uuid", &data.uuid),
        (":group_id", &data.group_id),
        (":name", &data.name),
        (":status", &data.status),
        (":description", &data.description),
        (":itype", &data.itype),
        (":icode", &data.icode),
        (":permissions", &data.permissions),
        (":created_by", &uid),
        (":modified_by", &uid),
    ], |row| {
        Indicator::from_row(row)
    }).unwrap();

    Ok(serde_json::to_value(result).unwrap())
}
