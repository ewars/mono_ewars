use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use rusqlite::{Connection as SqliteConnection};
use rusqlite::types::ToSql;
use rusqlite::NO_PARAMS;

use chrono::prelude::*;
use uuid::Uuid;

use sonomalib::models::{Form};
use sonomalib::traits::{Queryable};
use crate::state::ApplicationState;

#[derive(Debug, Serialize, Clone)]
pub struct FormMetrics {
    pub interval: Option<String>,
    pub lti: Option<Uuid>,
    pub reporting_locations: i32,
    pub submissions: i32,
    pub expected: i32,
    pub sub_dates: Vec<(String, String)>,
    pub users: i32,
    pub trend: Vec<(String, i32)>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Query {
    pub fid: Uuid,
    pub date: String,
}

pub fn get_metrics(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let role: String = app.borrow().get_current_role();
    let uid: Uuid = app.borrow().get_current_uuid();
    let query: Query = json::from_value(args.clone()).unwrap();

    let form: Form = match Form::get_by_id(&conn, "", &query.fid) {
        Ok(Some(res)) => res,
        Ok(Option::None) => bail!("FORM_NOT_FOUND"),
        Err(err) => bail!(err)
    };

    match role.as_ref() {
        "REGIONAL_ADMIN" => {
            get_regional_admin_metrics(&conn, &query, &form, &uid)
        },
        "ACCOUNT_ADMIN" => {
            get_account_admin_metrics(&conn, &query, &form)
        },
        _ => bail!("NO_ACCESS")
    }
}

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT lid FROM assignments
    WHERE uid = ?;
"#;

static SQL_GET_REPORTING_USERS: &'static str = r#"
    SELECT uid
    FROM users AS u
        LEFT JOIN assignmnets AS a ON a.uid = u.uid
        LEFT JOIN locations AS l ON l.uuid = a.lid
    WHERE u.status = 'ACTIVE' 
        AND a.status = 'ACTIVE'
        AND l.status = 'ACTIVE'
        AND u.role = 'USER'
        AND CAST(l.lineage AS TEXT) LIKE '{LOC}'
        AND a.fid = ?;
"#;

static SQL_GET_REPORTING_LOCATIONS: &'static str = r#"
    SELECT r.lid, r.start_date, r.end_date, r.status
    FROM reporting AS r
        LEFT JOIN locations AS l ON l.uuid = r.lid
    WHERE r.status = 'ACTIVE'
        AND l.status = 'ACTIVE'
        AND CAST(l.lineage AS TEXT) LIKE '{LOC}'
        AND r.fid = ?
        AND l.lti = ?;
"#;

fn get_regional_admin_metrics(conn: &SqliteConnection, query: &Query, form: &Form, uid: &Uuid) -> Result<Value, Error> {
    let mut stmt = conn.prepare(&SQL_GET_ASSIGNMENTS)?;
    let rows = stmt.query_map(&[&uid.to_string() as &ToSql], |row| {
        opt_uuid!(row, 0).unwrap()
    }).unwrap();
    let assigns: Vec<Uuid> = rows.map(|x| x.unwrap()).collect();

    let mut reporting_locations: i32 = 0;
    let mut submissions: i32 = 0;
    let mut expected: i32 = 0;
    let mut sub_dates: Vec<(String, String)> = Vec::new();
    let mut on_time: i32 = 0;
    let mut late: i32 = 0;
    let mut raw_trend: HashMap<String, i32>;
    let mut reporting_users: i32 = 0;

    for loc in assigns {
        // Get reporting users
        let sql = SQL_GET_REPORTING_USERS.replace("{LOC}", &loc.to_string());
        let user_count: i32 = conn.query_row(&sql, &[&query.fid.to_string() as &ToSql], |row| {
            row.get(0)
        }).unwrap();
        reporting_users += user_count;
    }

    Ok(json::to_value(FormMetrics {
        interval: Some("DAY".to_string()),
        lti: Option::None,
        reporting_locations,
        submissions,
        expected,
        sub_dates,
        users: reporting_users,
        trend: Vec::new()
    }).unwrap())
}

fn get_account_admin_metrics(conn: &SqliteConnection, query: &Query, form: &Form) -> Result<Value, Error> {
    bail!("");

}
