use std::rc::Rc;
use std::cell::RefCell;

use serde_derive;
use serde_json;
use serde_json::Value;

use crate::state::ApplicationState;

pub fn update_form(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Form = serde_json::from_value(args).unwrap();

    let uid = app.borrow().get_current_uuid();

    let mut sql: String = r#"
        UPDATE forms
            SET name = :name,
                status = :status,
                description = :description,
                guidance = :guidance,
                eid_prefix = :eid_prefix,
                features = :features,
                definition = :definition,
                etl = :etl,
                permission = :permissions,
                modified_by = :modified_by,
                modified = CURRENT_TIMESTAMP
        WHERE uuid = :uuid;
    "#.to_owned();

    conn.execute_named(&sql, &[
        (":name", &data.name),
        (":status", &data.status),
        (":description", &data.description),
        (":guidance", &data.guidance),
        (":eid_prefix", &data.eid_prefix),
        (":features", &data.features),
        (":definition", &data.definition),
        (":etl", &data.etl),
        (":permissions", &data.permissions),
        (":modified_by", &uid),
    ]).unwrap();

    Ok(serde_json::to_value(true).unwrap())
}

pub fn delete_form(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let id: String = serde_json::from_value(args).unwrap();

    conn.execute(r#"DELETE FROM forms where uuid = ?"#, &[&id]).unwrap();

    Ok(serde_json::to_value(true).unwrap())
}

pub fn create_form(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, String> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let data: Form = serde_json::from_value(args).unwrap();
    let uid = app.borrow().get_current_uuid();

    let sql = r#"
        INSERT INTO forms
        (uuid, name, version, status, description, guidance, eid_prefix, features, definition, etl, changes, permissions, created_by, modified_by)
        VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14)
        RETURNING *;
    "#.to_owned();

    let result: Form = conn.query_row(&sql, &[
        &data.uuid,
        &data.name,
        &data.version,
        &data.status,
        &data.description,
        &data.guidance,
        &data.eid_prefix,
        &data.features,
        &data.definition,
        &data.etl,
        &data.changes,
        &data.permissions,
        &uid,
        &uid,
    ], |row| {
        Form::from_row(row)
    }).unwrap();

    Ok(serde_json::to_value(result).unwrap())
}
