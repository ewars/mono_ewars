use gtk;
use gtk::prelude::*;
use relm_attributes::widget;
use relm::Widget;

pub struct Model {}

#[derive(Msg)]
pub enum Msg {
    Update,
}

impl DashboardMain {

}

#[widget]
impl ::relm::Widget for DashboardMain {
    fn model() -> Model {
        Model {}
    }

    fn update(&mut self, event: Msg) {

    }

    view!
    {
        #[name="dashboard_main"]
        gtk::Box {
            orientation: ::gtk::Orientation::Vertical,
            gtk::Label {
                text: "HELLO"
            }
        }
    }
}

