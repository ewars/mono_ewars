use std::cell::RefCell;
use std::rc::Rc;
use std::time::{Instant, Duration};
use std::io::{self, Error};
use std::collections::HashMap;

use actix::prelude::*;
use uuid::Uuid;
use chrono::prelude::*;

use failure::{Error as FErr};
use tokio::net::{TcpStream};
use tokio::io::WriteHalf;
use serde_json::Value;
use serde_json as json;
use actix::io as actix_io;

use sonomalib::{models as cmodels};
use sonomalib::traits::{Insertable};

use crate::state::ApplicationState;
use crate::sonoma::Application;
use crate::api::api_handler;

use crate::codec::{Codec};

use crate::models::config::{Account, User};
use crate::peer_server::{PeerCodec, PeerReq, PeerResp};
use crate::remote_client::{RemoteClient, RemoteCodec, RemoteReq, RemoteResponse};

#[derive(Clone, Message)]
pub struct NewPeerConnection {
    pub uuid: Uuid,
    pub addr: Addr<PeerConnection>,
    pub master: bool,
}
#[derive(Clone, Message)]
pub struct PeerDisconnected(Uuid);

#[derive(Debug, Clone)]
pub struct PeerAccount {
    pub uuid: Uuid,
    pub uid: Uuid,
    pub name: String,
    pub email: String,
    pub tki: String,
    pub domain: String,
    pub role: String,
}

pub struct PeerConnection {
    pub uuid: Uuid,
    pub server: Addr<Application>,
    pub state: Rc<RefCell<ApplicationState>>,
    pub hb: Instant,
    pub framed: actix_io::FramedWrite<WriteHalf<TcpStream>, PeerCodec>,
    pub did: String,
    pub master: bool,
    pub account: Option<Account>,
    pub active_account: Option<Uuid>,
    pub active_user: Option<Uuid>,
    pub user: Option<User>,
    pub remotes: HashMap<String, Addr<RemoteClient>>,
}

impl Actor for PeerConnection {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);

        self.server.send(Codec::NewPeerConnection {
            uuid: self.uuid.clone(),
            addr: ctx.address().clone(),
            master: false,
        })
        .into_actor(self)
        .then(|res, act, ctx| {
            actix::fut::ok(())
        })
        .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        Running::Stop
    }
}

macro_rules! uncork {
    ($row: expr, $col: expr) => {
        {
            match $row.get($col) {
                Some(res) => {
                    if res.is_null() {
                        Option::None
                    } else {
                        Some(json::from_value(res.clone()).unwrap())
                    }
                },
                None => {
                    Option::None
                }
            }

        }
    }
}


impl PeerConnection {
    // Create a new instance of PeerConnection
    pub fn new(server: Addr<Application>, state: Rc<RefCell<ApplicationState>>, frame: actix_io::FramedWrite<WriteHalf<TcpStream>, PeerCodec>, remotes: HashMap<String, Addr<RemoteClient>>) -> Self {
        PeerConnection {
            uuid: Uuid::new_v4(),
            hb: Instant::now(),
            server: server,
            state: state,
            framed: frame,
            did: "".to_string(),
            master: false,
            account: Option::None,
            active_account: Option::None,
            active_user: Option::None,
            user: Option::None,
            remotes,
        }
    }

    // Attempt to authenticate against the system
    pub fn attempt_authentication(&mut self, ctx: &mut Context<Self>, id: String, creds: Value) {
        unimplemented!()
    }

    fn hb(&mut self, ctx: &mut Context<Self>) {
        ctx.run_later(Duration::new(10, 0), |act, actx| {
            if Instant::now().duration_since(act.hb) > Duration::new(20, 0) {
                eprintln!("Hearbear");
                act.hb = Instant::now();
            }
            act.hb(actx);
        });
    }

    pub fn handle_api(&mut self, ctx: &mut Context<Self>, id: Uuid, cmd: String, args: Value) {
        let c_id = id.clone();

        eprintln!("{} - {} - {:?}", id, cmd, args);

        // Interrup on swap calls as we don't want to delve down into the api_handler
        match cmd.as_ref() {
            "com.sonoma.swap" => {
                let acc_id: Uuid = json::from_value(args.clone()).unwrap();

                let new_acc: Option<Account> = self.state.borrow().get_account_by_uuid(&acc_id);
                if let Some(c) = new_acc {
                    self.state.borrow_mut().set_account(&c);
                    self.account = Some(c.clone());
                }

                self.active_account = Some(acc_id.clone());


                let result: Value = json::to_value(true).unwrap();
                self.framed.write(PeerResp::CommandResult {
                    id: id.clone(),
                    error: Option::None,
                    d: result,
                });
            },
            "com.sonoma.is_connected" => {
                let tki: String = json::from_value(args).unwrap();
                let domain: Option<String> = self.state.borrow().get_domain_for_tki(&tki);

                if let Some(d) = domain {
                    if self.remotes.contains_key(&d) {
                        self.framed.write(PeerResp::CommandResult {
                            id: id.clone(),
                            error: Option::None,
                            d: json::to_value(true).unwrap(),
                        });
                    } else {
                        self.framed.write(PeerResp::CommandResult {
                            id: id.clone(),
                            error: Option::None,
                            d: json::to_value(true).unwrap(),
                        });
                    }
                } else {
                    self.framed.write(PeerResp::CommandResult {
                        id: id.clone(),
                        error: Option::None,
                        d: json::to_value(false).unwrap()
                    });
                }
            },
            _ => {
                match api_handler(self.state.clone(), id, cmd, args) {
                    Ok(res) => {
                        self.framed.write(PeerResp::CommandResult {
                            id: c_id,
                            error: Option::None,
                            d: res.d
                        });
                    },
                    Err(err) => {
                        eprintln!("CommandError: {:?}", err);
                        self.framed.write(PeerResp::CommandError {
                            id: c_id,
                            description: err.description
                        });
                    }
                }
            }
        }
    }

    pub fn push_message(&mut self, ctx: &mut Context<Self>, message: &'static str) {
        self.framed.write(PeerResp::SeedUpdate {
            code: message
        });
    }

    pub fn process_legacy_seed(&mut self, ctx: &mut Context<Self>, id: &Uuid, tki: String, data: HashMap<String, Vec<Value>>, ts_ms: usize) -> Result<(), FErr> {
        let conn = self.state.borrow().get_conn_by_tki(&tki).unwrap();


        for (key, collection) in data {
            match key.as_ref() {
                "forms" => {
                    let forms: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in forms {
                        let new_form = cmodels::Form {
                            uuid: uncork!(item, "uuid").unwrap(),
                            name: uncork!(item, "name").unwrap(),
                            version: 1,
                            status: uncork!(item, "status").unwrap(),
                            description: uncork!(item, "description").unwrap(),
                            eid_prefix: uncork!(item, "eid_prefix"),
                            features: uncork!(item, "features").unwrap(),
                            definition: uncork!(item, "definition").unwrap(),
                            guidance: Option::None,
                            constraints: Option::None,
                            stages: Option::None,
                            etl: uncork!(item, "etl").unwrap(),
                            changes: Some(Vec::new()),
                            permissions: Some(HashMap::new()),
                            created: uncork!(item, "created").unwrap(),
                            created_by: uncork!(item, "created_by"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modified_by"),
                            creator: Option::None,
                            modifier: Option::None,
                        };
                        new_form.insert(&conn, "")?;
                    }
                },
                "assignments" => {
                    let assignments: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in assignments {
                        let new_assign = cmodels::Assignment {
                            uuid: uncork!(item, "uuid").unwrap(),
                            uid: uncork!(item, "uid").unwrap(),
                            fid: uncork!(item, "fid"),
                            lid: uncork!(item, "lid"),
                            status: uncork!(item, "status").unwrap(),
                            assign_type: uncork!(item, "assign_type").unwrap(),
                            assign_group: uncork!(item, "assign_group"),
                            created: uncork!(item, "created").unwrap(),
                            created_by: uncork!(item, "created_by"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modified_by"),

                            location_status: Option::None,
                            form_status: Option::None,
                            reporting_status: Option::None,
                            start_date: Option::None,
                            end_date: Option::None,
                            assignee: Option::None,
                            creator: Option::None,
                            modifier: Option::None,
                            form_name: Option::None,
                            location_name: Option::None,
                            definition: Option::None,
                            features: Option::None,
                        };
                        new_assign.insert(&conn, "")?;
                    }
                },
                "records" => {
                    let records: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();

                    for item in records {
                        let mut dat_opt: HashMap<String, Value> = HashMap::new();
                        match item.get("data") {
                            Some(res) => {
                                if res.is_null() {
                                    dat_opt = HashMap::new()
                                } else {
                                    dat_opt = json::from_value(res.clone()).unwrap();
                                }
                            }, 
                            None => {}
                        }
                        let new_record = cmodels::Record {
                            uuid: uncork!(item, "uuid").unwrap(),
                            status: uncork!(item, "status").unwrap(),
                            stage: Option::None,
                            workflow: Option::None,
                            fid: uncork!(item, "fid").unwrap(),
                            data: dat_opt,
                            submitted: uncork!(item, "submitted").unwrap(),
                            submitted_by: uncork!(item, "submitted_by").unwrap(),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "submitted_by").unwrap(),
                            revisions: Option::None,
                            comments: Option::None,
                            eid: uncork!(item, "eid"),
                            import_id: uncork!(item, "import_id"),
                            source: uncork!(item, "source"),

                            submitter: Option::None,
                            modifier: Option::None,
                            location_name: Option::None,
                            form_name: Option::None,
                            metadata: Option::None,
                        };
                        new_record.insert(&conn, "")?;
                    }
                },
                "alerts" => {
                    let alerts: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in alerts {
                        let new_alert = cmodels::Alert {
                            uuid: uncork!(item, "uuid").unwrap(),
                            alid: uncork!(item, "alid").unwrap(),
                            status: uncork!(item, "status").unwrap(),
                            eid: uncork!(item, "eid"),
                            alert_date: uncork!(item, "alert_date").unwrap(),
                            lid: uncork!(item, "lid"),
                            stage: uncork!(item, "stage").unwrap(),
                            data: uncork!(item, "data").unwrap(),
                            workflow: Some(HashMap::new()),
                            events: uncork!(item, "events"),
                            raised: uncork!(item, "created").unwrap(),
                            closed: uncork!(item, "closed"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modified_by"),

                            alarm_name: Option::None,
                            location_name: Option::None,
                            location_full_name: Option::None,
                            lti_name: Option::None,
                            modifier: Option::None,
                            alarm_details: cmodels::AlarmDetails {
                                definition: Option::None,
                                alarm_description: Option::None,
                            },
                            location_geom: cmodels::LocationGeometry {
                                loc_geom: Option::None,
                                loc_geom_type: Option::None
                            },
                        };
                        new_alert.insert(&conn, "")?;
                    }
                },
                "alarms" => {
                    let alarms: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();

                    for item in alarms {
                        let new_alarm = cmodels::Alarm {
                            uuid: uncork!(item, "uuid").unwrap(),
                            name: uncork!(item, "name").unwrap(),
                            status: uncork!(item, "status").unwrap(),
                            description: uncork!(item, "description"),
                            version: 1,
                            definition: uncork!(item, "definition").unwrap(),
                            workflow: Some(HashMap::new()),
                            permissions: Some(HashMap::new()),
                            guidance: Option::None,
                            created: uncork!(item, "created").unwrap(),
                            created_by: uncork!(item, "created_by"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modified_by"),

                            creator: Option::None,
                            modifier: Option::None,
                        };
                        new_alarm.insert(&conn, "")?;
                    }
                },
                "users" => {
                    let users: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();

                    for item in users {
                        let new_user = cmodels::User {
                            uuid: uncork!(item, "uuid").unwrap(),
                            role: uncork!(item, "role").unwrap(),
                            permissions: json::to_value("{}").unwrap(),
                            settings: json::to_value("{}").unwrap(),
                            profile: json::to_value("{}").unwrap(),
                            org_id: uncork!(item, "org_id"),
                            created: uncork!(item, "created").unwrap(),
                            created_by: uncork!(item, "created_by"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modified_by"),
                            status: uncork!(item, "status").unwrap(),
                            name: uncork!(item, "name").unwrap(),
                            email: uncork!(item, "email").unwrap(),
                            password: uncork!(item, "password").unwrap(),
                            accounts: uncork!(item, "accounts").unwrap(),
                        };
                        new_user.insert(&conn, "")?;
                    }
                },
                "reporting" => {
                    let reporting: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in reporting {
                        let new_reporting = cmodels::ReportingPeriod {
                            uuid: uncork!(item, "uuid").unwrap(),
                            lid: uncork!(item, "lid").unwrap(),
                            fid: uncork!(item, "fid").unwrap(),
                            start_date: uncork!(item, "start_date").unwrap(),
                            end_date: uncork!(item, "end_date"),
                            status: uncork!(item, "status").unwrap(),
                            pid: uncork!(item, "parent_id"),
                            created: uncork!(item, "created").unwrap(),
                            created_by: uncork!(item, "created_by"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modified_by"),
                            creator: Option::None,
                            modifier: Option::None,
                            location_name: Option::None,
                            form_name: Option::None,
                        };
                        new_reporting.insert(&conn, "")?;
                    }
                },
                "location_type" => {
                    let location_types: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();

                    for item in location_types {
                        let new_item = cmodels::LocationType {
                            uuid: uncork!(item, "uuid").unwrap(),
                            name: uncork!(item, "name").unwrap(),
                            description: uncork!(item, "description").unwrap(),
                            restrictions: Option::None,
                            created: uncork!(item, "created").unwrap(),
                            created_by: uncork!(item, "created_by"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modified_by"),
                            creator: Option::None,
                            modifier: Option::None,
                        };
                        new_item.insert(&conn, "")?;
                    }
                },
                "locations" => {
                    let locations: Vec<HashMap<String, Value>> = collection.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in locations {
                        let new_loc = cmodels::Location {
                            uuid: uncork!(item, "uuid").unwrap(),
                            name: uncork!(item, "name").unwrap(),
                            status: uncork!(item, "status").unwrap(),
                            pcode: uncork!(item, "pcode").unwrap_or("".to_string()),
                            codes: HashMap::new(),
                            groups: uncork!(item, "groups"),
                            data: Some(HashMap::new()),
                            lti: uncork!(item, "lti").unwrap(),
                            pid: uncork!(item, "pid"),
                            lineage: uncork!(item, "lineage").unwrap(),
                            organizations: Some(Vec::new()),
                            geometry_type: "POINT".to_string(),
                            geojson: Option::None,
                            population: Some(HashMap::new()),
                            created: uncork!(item, "created").unwrap(),
                            created_by: uncork!(item, "created_by"),
                            modified: uncork!(item, "modified").unwrap(),
                            modified_by: uncork!(item, "modifid_by"),

                            creator: Option::None,
                            modifier: Option::None,
                            lti_name: Option::None,
                            parent_name: Option::None,
                            fqdn: Option::None,
                            child_count: Option::None,
                        };
                        new_loc.insert(&conn, "")?;
                    }
                },
                _ => {}
            }
        }

        /*
        let forms: Vec<LegacyForm> = data.get("forms").map(|x| json::from_value(x).unwrap()).collect();
        let locations: Vec<LegacyLocation> = data.get("locations").map(|x| json::from_value(x).unwrap()).collect();
        let location_types: Vec<LegacyLocationType> = data.get("location_types").map(|x| json::from_value(x).unwrap()).collect();
        let reporting: Vec<LegacyLocationReporting> = data.get("reporting").map(|x| json::from_value(x).unwrap()).collect();
        let assignments: Vec<LegacyAssignment> = data.get("assignments").map(|x| json::from_value(x).unwrap()).collect();
        let alarms: Vec<LegacyAlarm> = data.get("alarms").map(|x| json::from_value(x).unwrap()).collect();
        let alerts: Vec<LegacyAlert> = data.get("alerts").map(|x| json::from_value(x).unwrap()).collect();
        let conf: Vec<LegacyConf> = data.get("conf").map(|x| json::from_value(x).unwrap()).collect();
        let users: Vec<LegacyUser> = data.get("users").map(|x| json::from_value(x).unwrap()).collect();
        let records: Vec<Record> = data.get("records").map(|x| json::from_value(x).unwrap()).collect();
        let outbreaks: Vec<Outbreak> = data.get("outbreaks").map(|x| json::from_value(x).unwrap()).collect();

        let locations: Vec<Location> = locations.iter().map(|x| x.upgrade(&conn, "")?).collect();
        let location_types: Vec<LocationType> = location_types.iter().map(|x| x.upgrade(&conn, "")?).collect();
        let alarms: Vec<Alarm> = alarms.iter().map(|x| x.upgrade(&conn, "")?).collect();
        let reporting: Vec<Reporting> = reporting.iter().map(|x| x.upgrade(&conn, "")?).collect();
        let users: Vec<User> = reporting.iter().map(|x| x.upgrade(&conn, "")?).collect();
        let assignmnts: Vec<Assignment> = assignents.iter().map(|x| x.upgrade(&conn, "")?).collect();
        */


        self.state.borrow_mut().set_account_seeded(&tki)?;

        self.framed.write(PeerResp::Seeded {
            id: id.clone(),
            tki: tki.clone(),
            success: true
        });

        Ok(())
    }

    pub fn process_seed(&mut self, ctx: &mut Context<Self>, id: &Uuid, tki: String, data: HashMap<String, Vec<Value>>, ts_ms: usize) -> Result<(), FErr> {
        let conn = self.state.borrow().get_conn_by_tki(&tki).unwrap();
        self.framed.write(PeerResp::SeedUpdate {
            code: "SEED_DOWNLOADED",
        });

        for (key, records) in data {
            match key.as_ref() {
                "records" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_RECORDS"
                    });
                    let items: Vec<cmodels::Record> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "tasks" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_TASKS"
                    });
                    let items: Vec<cmodels::Task> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "forms" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_FORMS",
                    });
                    let items: Vec<cmodels::Form> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "documents" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_DOCUMENTS"
                    });
                    let items: Vec<cmodels::Document> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();

                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "assignments" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_ASSIGNMENTS"
                    });
                    let items: Vec<cmodels::Assignment> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();

                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "roles" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_ROLES"
                    });
                    let items: Vec<cmodels::Role> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "indicators" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_INDICATORS"
                    });
                    let items: Vec<cmodels::Indicator> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "locations" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_LOCATIONS"
                    });
                    let items: Vec<cmodels::Location> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "indicator_groups" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_INDICATOR_GROUPS"
                    });
                    let items: Vec<cmodels::IndicatorGroup> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "alarms" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_ALARMS"
                    });
                    let items: Vec<cmodels::Alarm> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                panic!(err)
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "location_types" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_LOCATION_TYPES"
                    });
                    let items: Vec<cmodels::LocationType> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                panic!(err)
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "organizations" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_ORGANIZATIONS"
                    });
                    let items: Vec<cmodels::Organization> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                panic!(err)
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "alerts" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_ALERTS",
                    });
                    let items: Vec<cmodels::Alert> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "resources" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_RESOURCES"
                    });
                    let items: Vec<cmodels::Resource> = records.iter().map(|x| {
                        match json::from_value(x.clone()) {
                            Ok(res) => res,
                            Err(err) => {
                                eprintln!("{:?}", err);
                                eprintln!("{:?}", x);
                                panic!()
                            }
                        }
                    }).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "reporting" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_REPORTING_PERIODS",
                    });
                    let items: Vec<cmodels::ReportingPeriod> = records.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "users" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_USERS"
                    });
                    let items: Vec<cmodels::User> = records.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "news" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_NEWS"
                    });
                    let items: Vec<cmodels::NewsItem> = records.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "oubreaks" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_OUTBREAKS",
                    });
                    let items: Vec<cmodels::Outbreak> = records.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                "conf" => {
                    self.framed.write(PeerResp::SeedUpdate {
                        code: "PROCESSING_CONF",
                    });
                    let items: Vec<cmodels::Conf> = records.iter().map(|x| json::from_value(x.clone()).unwrap()).collect();
                    for item in items {
                        item.insert(&conn, "")?;
                    }
                },
                _ => {
                    eprintln!("MISSED: {}", key);
                }
            }
        }

        self.state.borrow_mut().set_account_seeded(&tki)?;

        self.framed.write(PeerResp::Seeded {
            id: id.clone(),
            tki: tki.clone(),
            success: true
        });

        Ok(())


    }

    // Request the seed for an account
    pub fn get_seed(&mut self, ctx: &mut Context<Self>, cmd_id: &Uuid, tki: &String) {
        // Check if the account is seeded already
        let account = self.state.borrow().get_account_by_tki(&tki).unwrap();
        if account.seeded {
            self.framed.write(PeerResp::SeedResult {
                id: cmd_id.clone(),
                success: false,
                error: Some("ERR_ACC_SEEDED".to_string())
            });
        } else {
            if account.canon {
                let addr = ctx.address().clone();
                self.framed.write(PeerResp::SeedUpdate {
                    code: "REQUESTING_DATA"
                });
                if let Some(c) = self.remotes.get(&"CANON".to_string()) {
                    c.do_send(Codec::SeedRequest {
                        id: cmd_id.clone(),
                        tki: tki.clone(),
                        addr,
                    });
                }
            } else {
                self.framed.write(PeerResp::SeedResult {
                    id: cmd_id.clone(),
                    success: false,
                    error: Some("ERR_NO_CONN".to_string()),
                });
            }
        }
    }
}

impl actix::io::WriteHandler<Error> for PeerConnection {}

impl StreamHandler<PeerReq, Error> for PeerConnection {
    fn handle(&mut self, msg: PeerReq, ctx: &mut Self::Context) {
        match msg {
            PeerReq::Command { id, cmd, args } => {
                self.handle_api(ctx, id, cmd, args);
            },
            PeerReq::AuthRequest { email, password, domain, id } => {
                //self.attempt_authentication(ctx, id, payload);
                let addr = ctx.address().clone();
                eprintln!("{}", domain);
                for (key, value) in &self.remotes {
                    eprintln!("{}", key)
                }

                // TOOD: Check if it's a new remote, if it is
                // then we need to connect to it first and then issue an AuthRequest

                let mut domain: String = domain.clone();
                if domain.contains("ewars.ws") {
                    domain = "CANON".to_string();
                }
                if let Some(c) = self.remotes.get(&domain) {
                    eprintln!("HERE: Found remote");
                    c.do_send(Codec::AuthRequest {
                        id: id.clone(),
                        email: email,
                        password: password,
                        addr,
                    });
                } else {
                    self.framed.write(PeerResp::CommandError {
                        id: id.clone(),
                        description: "ERR_NO_CONNECTION".to_string(),
                    });
                }
            },
            PeerReq::GetUsers { id } => {
                eprintln!("GET_USERS");
                let users = self.state.borrow().config.users.clone();
                let accounts = self.state.borrow().config.accounts.clone();
                self.framed.write(PeerResp::Users {
                    id: id.clone(),
                    users,
                    accounts
                });
            },
            PeerReq::GetSeed { id, tki } => {
                self.get_seed(ctx, &id, &tki);
            },
            PeerReq::SetMaster => {
                let c_ctx = ctx.address().clone();
                self.server.do_send(Codec::SetMaster {
                    addr: c_ctx,
                    uuid: self.uuid.clone()
                });
            },
            PeerReq::SetUser {id, uid} => {
                match self.state.borrow().get_user_details(&uid) {
                    Ok(res) => {
                        self.account = Some(res.0.clone());
                        self.user = Some(res.1.clone());
                        self.active_account = Some(res.0.uuid.clone());
                        self.active_user = Some(uid.clone());
                    },
                    Err(_) => {}
                };

                if let Some(c) = &self.user {
                    self.state.borrow_mut().set_user(&c);
                }
                if let Some(c) = &self.account {
                    self.state.borrow_mut().set_account(&c);
                }
                self.framed.write(PeerResp::UserSet {
                    id: id
                });
            },
            PeerReq::Ping => self.hb = Instant::now(),
            _ => {
                eprintln!("Peer Error: Unknown command");
            }
        }
    }
}

impl Handler<Codec> for PeerConnection {
    type Result = Result<Codec, ()>;

    fn handle(&mut self, msg: Codec, ctx: &mut Context<Self>) -> Self::Result {
        match msg {
            Codec::MasterSet => {
                self.framed.write(PeerResp::MasterSet);
                Ok(Codec::Ok)
            },
            Codec::SeedData { id, protocol, tki, data, ts_ms} => {
                if protocol == 1 {
                    match self.process_legacy_seed(ctx, &id, tki, data, ts_ms) {
                        Ok(_) => {},
                        Err(err) => {
                            eprintln!("{:?}", err);
                        }
                    }
                } 

                Ok(Codec::Ok)
            },
            Codec::AuthenticationResult { id, success, error, users, accounts} => {
                eprintln!("AUTH_RESULT: {}, {}, {:?}, {:?}, {:?}", id, success, error, users, accounts);

                // TODO: Persist the accounts/account users
                // TODO: Respond back to client with result
                self.state.borrow_mut().add_accounts(&users, &accounts);

                self.framed.write(PeerResp::AuthResult {
                    id: id,
                    success: success,
                    error: error
                });

                Ok(Codec::Ok)
            },
            _ => Err(())
        }
    }
}
