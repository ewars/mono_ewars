use std::io;
use std::io::Error as IoError;
use serde_json::Value;
use std::collections::HashMap;
use chrono::prelude::*;
use chrono::Duration;

lazy_static! {
    static ref MONTH_DAYS: HashMap<u32, u32> = {
        let mut m = HashMap::new();
        m.insert(1, 31); // Jan
        m.insert(2, 28); // Feb; leap years has 29
        m.insert(3, 31); // Mar
        m.insert(4, 30); // Apr
        m.insert(5, 31); // May
        m.insert(6, 30); // Jun
        m.insert(7, 31); // Jul
        m.insert(8, 31); // Aug
        m.insert(9, 30); // Sep
        m.insert(10, 31); // Oct
        m.insert(11, 30); // Nov
        m.insert(12, 31); // Dec
        m
    };
}

// Given a date, get the start of the iso8601 week it belongs to, if the date
// is the start already, return a copy
pub fn get_start_of_isoweek(date: &NaiveDate) -> Result<NaiveDate, IoError> {
    unimplemented!()
}

// Given a date, get the end of the iso8601 week it belongs to, if the date is
// the end already, return a copy
pub fn get_end_of_isoweek(date: &NaiveDate) -> Result<NaiveDate, IoError> {
    unimplemented!()
}


// Get the reporting dates within a range based on one of the standard intervals
pub fn get_intervals(start_date: &NaiveDate, end_date: &NaiveDate, interval: &String) -> Result<Vec<NaiveDate>, IoError> {
    let start_date = start_date.clone();
    let end_date = end_date.clone();

    let mut resultant_dates: Vec<NaiveDate> = Vec::new();

    if interval == "DAY" {
        let mut done = false;
        let mut current = start_date.clone();

        while !done {
            resultant_dates.push(current.clone());
            if current >= end_date {
                done = true;
            } else {
                current += Duration::days(1);
            }
        }
    }

    Ok(resultant_dates)
}

// Get the reporting dates within a range based on a custom interval
pub fn get_intervals_custom(start_date: &NaiveDate, end_date: &NaiveDate, interval: &Value) -> Result<Vec<NaiveDate>, IoError> {
    unimplemented!()
}

// Get the next reporting period from the reference date
pub fn get_next_period(reference: &NaiveDate, interval: &String) -> Result<NaiveDate, IoError> {
    unimplemented!()
}

// Get the next reporting period from the reference date for a custom reporting interval
pub fn get_next_period_custom(reference: &NaiveDate, interval: &Value) -> Result<NaiveDate, IoError> {
    unimplemented!()
}

// Given a start date, end date and interval, figure out the reporting dates within that range
pub fn get_reporting_intervals(start_date: &DateTime<Utc>, end_date: &DateTime<Utc>, interval: &String) -> Result<Vec<Date<Utc>>, IoError> {
    let full_dates: Vec<Date<Utc>> = Vec::new();
    let r_end_date = end_date.clone().date();
    let mut resultant_dates: Vec<Date<Utc>> = Vec::new();

    let mut current = start_date.clone().date();
    let mut done: bool = false;

    if interval == "DAY" {
        // Every day is a reporting period
        let mut done = false;
        let mut current = start_date.clone().date();

        while !done {
            resultant_dates.push(current.clone());
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }
        
    }

    if interval == "WEEK" {
        // Figure out which days in the series are ends of ISOWeeksS
        let mut done = false;
        let mut current = start_date.clone().date();

        while !done {
            if current.weekday() == Weekday::Sun {
                resultant_dates.push(current.clone());
            }
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }
    }

    if interval == "MONTH" {
        // Figure out which days in the series are ends of months
        let mut done = false;
        let mut current = start_date.clone().date();

        while !done {
            // get the end of the month
            let year = current.year();
            let month = current.month();
            let mut max_days: u32 = MONTH_DAYS.get(&month).unwrap().clone();
            // If this is february we need to check if it's a leap year, 
            // as Feb has 29 days in leap years and 28 for all others
            if month == 2 {
                if is_leap_year(&year) {
                    max_days = 29;
                }
            }

            let month_end: Date<Utc> = Utc.ymd(year, month, max_days).and_hms(9, 10, 11).date();
            if current == month_end {
                resultant_dates.push(current.clone());
            }
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }
    }

    if interval == "YEAR" {
        // Figure out which days in the series are ends of the year
        let mut done = false;
        let mut current = start_date.date();

        while !done {
            let year = current.year();
            let year_end: Date<Utc> = Utc.ymd(year, 12, 31).and_hms(9, 10, 11).date();
            if year_end == current {
                resultant_dates.push(current.clone());
            }
            if current >= r_end_date {
                done = true;
            } else {
                current = current + Duration::days(1);
            }
        }
    }

    Ok(resultant_dates)
}


// Check if the year is a leap year
pub fn is_leap_year(year: &i32) -> bool {
    let result: bool = false;

    let remainder: i32 = year % 4;
    
    // if not evenly divisible by 4, not a leap year
    if remainder > 0 {
        return false;
    } else {
        // Could be a leap year
        // if it's not evenly divisible by 100, it's a leap year
        let hun_remainder: i32 = year % 100;
        if hun_remainder > 0 {
            return true;
        } else {
            // If it's evenly divislbe by 400 it's a leap year
            let hun4_remainder: i32 = year % 400;
            if hun4_remainder == 0 {
                return true;
            } else {
                return false;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_daily_interval() {
        let start_date: DateTime<Utc> = Utc.ymd(2018, 1, 1).and_hms(9, 10, 11);
        let end_date: DateTime<Utc> = Utc.ymd(2018, 12, 31).and_hms(9, 10, 11);

        let interval = "DAY".to_string();
        let reporting_dates: Vec<Date<Utc>> = get_reporting_intervals(&start_date, &end_date, &interval).unwrap();

        assert_eq!(reporting_dates.len(), 365);
    }

    #[test]
    fn test_weekly_interval() {
        let start_date: DateTime<Utc> = Utc.ymd(2018, 1, 1).and_hms(9, 10, 11);
        let end_date: DateTime<Utc> = Utc.ymd(2018, 6, 12).and_hms(9, 10, 11);
        let interval = "WEEK".to_string();
        let reporting_dates: Vec<Date<Utc>> = get_reporting_intervals(&start_date, &end_date, &interval).unwrap();

        let expected: Vec<Date<Utc>> = vec![
            Utc.ymd(2018, 1, 7),
            Utc.ymd(2018, 1, 14),
            Utc.ymd(2018, 1, 21),
            Utc.ymd(2018, 1, 28),
            Utc.ymd(2018, 2, 4),
            Utc.ymd(2018, 2, 11),
            Utc.ymd(2018, 2, 18),
            Utc.ymd(2018, 2, 25),
            Utc.ymd(2018, 3, 4),
            Utc.ymd(2018, 3, 11),
            Utc.ymd(2018, 3, 18),
            Utc.ymd(2018, 3, 25),
            Utc.ymd(2018, 4, 1),
            Utc.ymd(2018, 4, 8),
            Utc.ymd(2018, 4, 15),
            Utc.ymd(2018, 4, 22),
            Utc.ymd(2018, 4, 29),
            Utc.ymd(2018, 5, 6),
            Utc.ymd(2018, 5, 13),
            Utc.ymd(2018, 5, 20),
            Utc.ymd(2018, 5, 27),
            Utc.ymd(2018, 6, 3),
            Utc.ymd(2018, 6, 10),
        ];

        assert_eq!(reporting_dates.len(), expected.len());
    }

    #[test]
    fn test_monthly_interval() {
        let start_date: DateTime<Utc> = Utc.ymd(2018, 1, 1).and_hms(9, 10, 11);
        let end_date: DateTime<Utc> = Utc.ymd(2018, 12, 1).and_hms(9, 10, 11);
        let interval = "MONTH".to_string();

        let reporting_dates: Vec<Date<Utc>> = get_reporting_intervals(&start_date, &end_date, &interval).unwrap();

        assert_eq!(reporting_dates.len(), 11);
    }

    #[test]
    fn test_yearly_interval() {
        let start_date: DateTime<Utc> = Utc.ymd(2016, 1, 1).and_hms(9, 10, 11);
        let end_date: DateTime<Utc> = Utc.ymd(2019, 1, 1).and_hms(9, 10, 11);
        let interval = "YEAR".to_string();

        let reporting_dates: Vec<Date<Utc>> = get_reporting_intervals(&start_date, &end_date, &interval).unwrap();

        assert_eq!(reporting_dates.len(), 3);
    }

}