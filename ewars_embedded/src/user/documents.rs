use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use rusqlite::{NO_PARAMS};

use sonomalib::models::{Document};

use crate::state::ApplicationState;

static SQL_GET_DOCUMENTS: &'static str = r#"
    SELECT * FROM documents
    WHERE status = 'ACTIVE';
"#;

pub fn get_available_documents(app: Rc<RefCell<ApplicationState>>, _args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(SQL_GET_DOCUMENTS)?;
    let rows = stmt.query_map(NO_PARAMS, |row| {
        Document::from(row)
    }).unwrap();

    let results: Vec<Document> = rows.map(|x| x.unwrap()).collect();

    let output = json::to_value(results).unwrap();
    Ok(output)
}
