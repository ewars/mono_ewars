use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

#[macro_use]
use sonomalib::utils;

use failure::Error;
use serde_json as json;
use serde_json::Value;
use uuid::Uuid;

use rusqlite::{NO_PARAMS};
use rusqlite::{Row as SqliteRow};
use rusqlite::types::{ToSql};

use crate::state::ApplicationState;

#[derive(Debug, Serialize)]
pub struct UserLocation {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub lti: Uuid,
    pub status: String,
    pub lti_name: HashMap<String, String>,
    pub child_count: i64,
}

impl<'a, 'stmt> From<&SqliteRow<'a, 'stmt>> for UserLocation {
    fn from(row: &SqliteRow) -> Self {
        let uuid: String = row.get("uuid");
        let uuid = Uuid::parse_str(&uuid).unwrap();

        let name: HashMap<String, String> = json::from_value(row.get("name")).unwrap();

        let lti: String = row.get("lti");
        let lti = Uuid::parse_str(&lti).unwrap();

        let lti_name: Option<Value> = row.get("lti_name");
        eprintln!("{:?}", lti_name);
        let lti_name: HashMap<String, String> = match lti_name {
            Some(res) => json::from_value(res).unwrap(),
            None => HashMap::new()
        };

        UserLocation {
            uuid,
            name,
            lti,
            status: row.get("status"),
            lti_name,
            child_count: row.get("child_count"),
        }
    }
}

static SQL_GET_ASSIGNMENTS: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, lt.name AS lti_name, 0 As child_count
    FROM locations AS l
        LEFT JOIN location_types AS lt ON l.lti = lt.uuid
        LEFT JOIN assignments AS a ON a.lid = l.uuid
    WHERE l.status = 'ACTIVE'
        AND a.status = 'ACTIVE'
        AND a.uid = ?;
"#;

static SQL_GET_ASSIGNMENTS_REGIONAL: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, lt.name As lti_name,
        (SELECT COUNT(r.uuid) FROM locations AS r WHERE CAST(r.lineage AS TEXT) LIKE l.uuid) AS child_count
    FROM locations AS l
        LEFT JOIN location_types AS lt ON lt.uuid = l.lti
        LEFT JOIN assignments AS a ON a.lid = l.uuid
    WHERE l.status = 'ACTIVE'
        AND a.status = 'ACTIVE'
        AND a.uid = ?
        AND a.assign_type = 'ADMIN';
"#;

pub fn get_user_locations(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let role = app.borrow().get_current_role();

    let mut results: Vec<UserLocation> = Vec::new();

    match role.as_ref() {
        "USER" => {
            results = get_reporting_user_locations(app.clone());
        },
        "REGIONAL_ADMIN" => {
            results = get_regional_admin_locations(app.clone());
        },
        "ACCOUNT_ADMIN" => {
            results = get_root_locations(app.clone());
        },
        _ => {}
    }

    let output: Value = json::to_value(results).unwrap();

    Ok(output)
}

static SQL_GET_ROOT_LOCATIONS: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, lt.name AS lti_name,
        (SELECT COUNT(r.uuid) FROM locations AS r WHERE CAST(r.lineage AS TEXT) LIKE l.uuid) as child_count
    FROM locations AS l
        LEFT JOIN location_types AS lt ON lt.uuid = l.lti
    WHERE l.status = 'ACTIVE' 
        AND l.pid IS NULL;
"#;

fn get_root_locations(app: Rc<RefCell<ApplicationState>>) -> Vec<UserLocation> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();

    let mut stmt = conn.prepare(&SQL_GET_ROOT_LOCATIONS).unwrap();

    let rows = stmt.query_map(NO_PARAMS, |row| {
        UserLocation::from(row)
    }).unwrap();

    let results: Vec<UserLocation> = rows.map(|x| x.unwrap()).collect();

    results
}

// Get locations that a reporting user has access to
fn get_reporting_user_locations(app: Rc<RefCell<ApplicationState>>) -> Vec<UserLocation> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uid();

    let mut stmt = conn.prepare(&SQL_GET_ASSIGNMENTS).unwrap();

    let rows = stmt.query_map(&[&uid.to_string() as &ToSql], |row| {
        UserLocation::from(row)
    }).unwrap();

    let mut results: Vec<UserLocation> = rows.map(|x| x.unwrap()).collect();
    results.dedup_by_key(|x| x.uuid.clone());

    results
}

// Get locations that a regional admin has access to, including chld count for displaying a tree of locations
fn get_regional_admin_locations(app: Rc<RefCell<ApplicationState>>) -> Vec<UserLocation> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uid();

    let mut stmt = conn.prepare(&SQL_GET_ASSIGNMENTS_REGIONAL).unwrap();

    let rows = stmt.query_map(&[&uid.to_string() as &ToSql], |row| {
        UserLocation::from(row)
    }).unwrap();

    let mut results: Vec<UserLocation> = rows.map(|x| x.unwrap()).collect();

    results
}
