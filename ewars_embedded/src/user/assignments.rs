use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use uuid::Uuid;
use serde_json::Value;
use chrono::prelude::*;

use rusqlite::{Row};
use rusqlite::types::ToSql;

use sonomalib::models::{Assignment};

use crate::state::ApplicationState;

static SQL_GET_OWN_ASSIGNMENTS: &'static str = r#"
    SELECT a.*,
           l.name AS location_name, f.name AS form_name, r.start_date, r.end_date, f.definition, f.features
    FROM assignments AS a
        LEFT JOIN locations AS l ON l.uuid = a.lid
        LEFT JOIN reporting AS r ON r.lid = l.uuid
        LEFT JOIN forms AS f ON f.uuid = a.fid
    WHERE a.status = 'ACTIVE' AND a.uid = ? AND a.assign_type != 'ADMIN';
"#;

static SQL_GET_CHILD_LOCATIONS: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, lti.name AS lti_name
    FROM locations AS l
        LEFT JOIN location_types AS lti ON lti.uuid = l.lti
    WHERE CAST(l.lineage AS TEXT) LIKE '?'
        AND l.lti = ?
        AND l.status = 'ACTIVE';
"#;

static SQL_GET_GROUP_LOCATIONS: &'static str = r#"
    SELECT l.uuid, l.name, l.lti, l.status, lti.name AS lti_name
    FROM locations AS l
        LEFT JOIN location_types AS lti ON lti.uuid = l.lti
    WHERE CAST(l.groups AS TEXT) LIKE '?'
        AND l.lti = ?
        AND l.status = 'ACTIVE';
"#;

pub struct SubLocation {
    pub uuid: Uuid,
    pub name: HashMap<String, String>,
    pub lti: Uuid,
    pub lti_name: HashMap<String, String>,
}

impl<'a, 'stmt> From<&Row<'a, 'stmt>> for SubLocation {
    fn from(row: &Row) -> Self {
        SubLocation {
            uuid: pull_uuid!(row, 0),
            name: json::from_value(row.get(1)).unwrap(),
            lti: pull_uuid!(row, 2),
            lti_name: json::from_value(row.get(3)).unwrap(),
        }
    }
}


pub fn get_assignments(app: Rc<RefCell<ApplicationState>>, _args: Value) -> Result<Value, Error> {
    let assignments: Vec<Assignment> = get_user_assigns(app.clone());

    Ok(json::to_value(assignments).unwrap())
}


pub fn get_user_assigns(app: Rc<RefCell<ApplicationState>>) -> Vec<Assignment> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let role = app.borrow().get_current_role();
    let uid = app.borrow().get_current_uuid().to_string();

    let mut stmt = conn.prepare(&SQL_GET_OWN_ASSIGNMENTS).unwrap();

    let rows = stmt.query_map(&[&uid.to_string() as &ToSql], |row| {
        Assignment::from(row)
    }).unwrap();

    let assigns: Vec<Assignment> = rows.map(|x| x.unwrap()).collect();

    let mut results: Vec<Assignment> = Vec::new();

    for assign in &assigns {
        let features = assign.features.clone().unwrap();
        if features.contains_key("LOCATION_REPORTING") {
            match assign.assign_type.as_ref() {
                "UNDER" => {
                    let loc_id: Uuid = assign.lid.clone().unwrap();
                    let lti = assign.get_location_type().unwrap();

                    let mut stmt = conn.prepare(&SQL_GET_CHILD_LOCATIONS).unwrap();

                    let rows = stmt.query_map(&[&loc_id.to_string() as &ToSql, &lti.to_string()], |row| {
                        SubLocation::from(row)
                    }).unwrap();

                    let mut sub_locations: Vec<SubLocation> = rows.map(|x| x.unwrap()).collect();
                    sub_locations.sort_by_key(|x| x.uuid.clone());
                    sub_locations.dedup_by_key(|x| x.uuid.clone());

                    for loc in &sub_locations {
                        let mut new_assign = assign.clone();
                        new_assign.lid = Some(loc.uuid.clone());
                        new_assign.assign_type = "DEFAULT".to_string();
                        new_assign.location_name = Some(loc.name.clone());
                        results.push(new_assign);
                    }
                }
                "GROUP" => {
                    let group: String = assign.assign_group.clone().unwrap();
                    let lti: Uuid = assign.get_location_type().unwrap();

                    let mut stmt = conn.prepare(&SQL_GET_GROUP_LOCATIONS).unwrap();

                    let rows = stmt.query_map(&[&group as &ToSql, &lti.to_string()], |row| {
                        SubLocation::from(row)
                    }).unwrap();

                    let mut sub_locations: Vec<SubLocation> = rows.map(|x| x.unwrap()).collect();
                    sub_locations.sort_by_key(|x| x.uuid.clone());
                    sub_locations.dedup_by_key(|x| x.uuid.clone());

                    for loc in &sub_locations {
                        let mut new_assign = assign.clone();
                        new_assign.lid = Some(loc.uuid.clone());
                        new_assign.assign_type = "DEFAULT".to_string();
                        new_assign.location_name = Some(loc.name.clone());
                        results.push(new_assign);
                    }
                },
                "ADMIN" => {

                },
                _ => {
                    results.push(assign.clone());
                }
            }
        } else {
            results.push(assign.clone());
        }
    }

    results.sort_by_key(|x| x.fid.clone());

    results
}
