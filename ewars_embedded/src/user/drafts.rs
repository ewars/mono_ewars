use std::rc::Rc;
use std::cell::RefCell;

use failure::Error;
use serde_json as json;
use serde_json::Value;

use sonomalib::models::{Draft};

use crate::state::ApplicationState;

static SQL_GET_DRAFTS: &'static str = r#"
    SELECT d.*, f.name, f.features
    FROM drafts AS d
        LEFT JOIN forms AS f ON f.uuid = d.fid
    WHERE d.created_by = ?;
"#;

pub fn get_drafts(app: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = app.borrow().get_current_pool().unwrap().get().unwrap();
    let uid = app.borrow().get_current_uuid();

    let mut stmt = conn.prepare(&SQL_GET_DRAFTS)?;

    let rows = stmt.query_map(&[&uid.to_string()], |row| {
        Draft::from(row)
    })?;

    let result: Vec<Draft> = rows.map(|x| x.unwrap()).collect();

    let output: Value = json::to_value(result).unwrap();

    Ok(output)
}

