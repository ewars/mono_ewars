use serde_json::Value;
use serde_json;
use uuid::Uuid;

use crate::models::utils;
use rusqlite::{Row, Connection};

#[derive(Debug, Serialize)]
pub struct AssignmentSummary {
    pub uuid: String,
    pub form_id: String,
    pub uid: String,
    pub location_id: Option<String>,
    pub status: String,
    pub assign_type: String,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct Assignment {
    pub uuid: Option<String>,
    pub status: String,
    pub uid: String,
    pub fid: Option<String>,
    pub lid: Option<String>,
    pub assign_type: String,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,

    pub user_name: Option<String>,
    pub created_name: Option<String>,
    pub location_name: Option<Value>,
}

impl Assignment {
    pub fn from_row(row: &Row) -> Assignment {
        Assignment {
            uuid: row.get(0),
            status: row.get(1),
            uid: row.get(2),
            fid: row.get(3),
            lid: row.get(4),
            assign_type: row.get(5),
            created: row.get(6),
            created_by: row.get(7),
            modified: row.get(8),
            modified_by: row.get(9),

            user_name: None,
            created_name: None,
            location_name: None
        }
    }

    pub fn from_view_row(row: &Row) -> Assignment {
        Assignment {
            uuid: row.get(0),
            status: row.get(1),
            uid: row.get(2),
            fid: row.get(3),
            lid: row.get(4),
            assign_type: row.get(5),
            created: row.get(6),
            created_by: row.get(7),
            modified: row.get(8),
            modified_by: row.get(9),

            user_name: row.get(10),
            created_name: row.get(11),
            location_name: row.get(12)
        }
    }

    pub fn get_by_id(conn: &Connection) -> Option<Self> {
        unimplemented!()
    }

    pub fn delete_assignment(conn: &Connection, id: &Uuid) -> bool {
        unimplemented!()
    }
}



