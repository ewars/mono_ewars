use serde_json::Value;
use rusqlite::{Row};


#[derive(Debug, Deserialize, Serialize)]
pub struct Document {
    pub uuid: String,
    pub template_name: String,
    pub instance_name: String,
    pub status: String,
    pub version: i32,
    pub theme: Option<String>,
    pub layout: Value,
    pub content: Option<String>,
    pub data: Value,
    pub generation: Value,
    pub orientation: String,
    pub template_type: String,
    pub permissions: Value,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct DocumentSummary {
    pub uuid: String,
    pub template_name: String,
    pub status: String,
    pub version: i32,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}


impl Document {
    pub fn from_row(row: &Row) -> Document {
        Document {
            uuid: row.get(0),
            template_name: row.get(1),
            instance_name: row.get(2),
            status: row.get(3),
            version: row.get(4),
            theme: row.get(5),
            layout: row.get(6),
            content: row.get(7),
            data: row.get(8),
            generation: row.get(9),
            orientation: row.get(10),
            template_type: row.get(11),
            permissions: row.get(12),
            created: row.get(13),
            created_by: row.get(14),
            modified: row.get(15),
            modified_by: row.get(16),
        }
    }
}
