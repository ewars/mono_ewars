use std::collections::HashMap;

use serde_json;
use serde_json::Value;
use chrono::prelude::*;
use rusqlite::{Transaction, Row};

use crate::models::general_structs::CommandResult;
use crate::models::{event};

pub type RecordHistory = Vec<Value>;
pub type RecordComments = Vec<Value>;

#[derive(Debug, Serialize, Deserialize)]
pub struct RecordChange {
    pub uuid: String,
    pub event_type: String,
    pub event_date: String,
    pub event_user: Option<String>,
    pub event_data: Value,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Record {
    pub uuid: String,
    pub status: String,
    pub stage: Option<String>,
    pub workflow: Option<Value>,
    pub fid: String,
    pub data: Option<Value>,
    pub submitted: String,
    pub submitted_by: String,
    pub created: String,
    pub created_by: String,
    pub modified: String,
    pub modified_by: String,
    pub revisions: Option<Value>,
    pub comments: Option<Value>,
    pub eid: Option<String>,
    pub import_id: Option<String>,
    pub source: Option<String>,
}

impl Record {
    pub fn from_row<'row>(row: &'row Row) -> Record {
        Record {
            uuid: row.get(0),
            status: row.get(1),
            stage: row.get(2),
            workflow: row.get(3),
            fid: row.get(4),
            data: row.get(5),
            submitted: row.get(6),
            submitted_by: row.get(7),
            created: row.get(8),
            created_by: row.get(9),
            modified: row.get(10),
            modified_by: row.get(11),
            revisions: row.get(12),
            comments: row.get(13),
            eid: row.get(14),
            import_id: row.get(15),
            source: row.get(16),
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct NewRecord {
    pub uuid: Option<String>,
    pub fid: i32,
    pub fv_id: Option<String>,
    pub location_id: Option<String>,
    pub data_date: Option<String>,
    pub data: Value,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RecordSummary {
    pub uuid: String,
    pub stage: Option<String>,
    pub location_id: Option<String>,
    pub location_name: Option<Value>,
    pub data_date: Option<NaiveDate>,
    pub data: Value,
    pub source: String,
    pub eid: Option<String>,
    pub user_name: Option<String>,
    pub user_email: Option<String>,
    pub pcode: Option<String>,
}

impl RecordSummary {
    pub fn from_row<'row>(row: &'row Row) -> RecordSummary {
        RecordSummary {
            uuid: row.get(0),
            stage: row.get(1),
            location_id: row.get(2),
            location_name: row.get(3),
            data_date: row.get(4),
            data: row.get(5),
            source: row.get(6),
            eid: row.get(7),
            user_name: row.get(8),
            user_email: row.get(9),
            pcode: row.get(10),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RecordsQuery {
    pub filters: HashMap<String, (String, String, String)>,
    pub sorters: HashMap<String, (String, String)>,
    pub limit: i32,
    pub offset: i32
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RecordAmendment {
    pub uuid: String,
    pub location_id: Option<String>,
    pub data_date: String,
    pub data: Value,
    pub reason: String,
}





#[derive(Debug, Deserialize)]
pub struct NewRecordComment {
    pub uuid: String,
    pub content: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RecordComment {
    pub uuid: String,
    pub uid: String,
    pub content: String,
    pub created: String,
}


