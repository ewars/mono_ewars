use serde_json as json;
use serde_json::Value;

#[derive(Debug, Serialize)]
pub struct LocationGroup (String);
