use rusqlite::{Row};
use serde_json::Value;

#[derive(Debug, Deserialize, Serialize)]
pub struct IndicatorGroup {
    pub uuid: String,
    pub name: String,
    pub description: Option<String>,
    pub pid: Option<String>,
    pub permissions: Value,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

impl IndicatorGroup {
    pub fn from_row(row: &Row) -> IndicatorGroup {
        IndicatorGroup {
            uuid: row.get(0),
            name: row.get(1),
            description: row.get(2),
            pid: row.get(3),
            permissions: row.get(4),
            created: row.get(5),
            created_by: row.get(6),
            modified: row.get(7),
            modified_by: row.get(8),
        }
    }
}
