use rusqlite::{Row};
use serde_json::Value;


#[derive(Debug, Serialize, Deserialize)]
pub struct Organization {
    pub uuid: String,
    pub name: Value,
    pub description: Option<String>,
    pub acronym: Option<String>,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>
}

impl Organization {
    pub fn from_row(row: &Row) -> Organization {
        Organization {
            uuid: row.get(0),
            name: row.get(1),
            description: row.get(2),
            acronym: row.get(3),
            created: row.get(4),
            created_by: row.get(5),
            modified: row.get(6),
            modified_by: row.get(7),
        }
    }
}
