use serde_json;
use serde_json::Value;

#[derive(Serialize, Debug, Deserialize)]
pub struct CommandResult {
    pub result: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CommandError {
    pub code: String,
    pub message: String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CommandPaginatedResult {
    pub data: Value,
    pub counter: u64,
    pub metadata: Value,
}
