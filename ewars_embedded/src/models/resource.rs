use serde_derive;
use serde_json as json;
use serde_json::Value;

use rusqlite::{Row};

#[derive(Debug, Deserialize, Serialize)]
pub struct Resource {
    pub uuid: Option<String>,
    pub name: String,
    pub status: String,
    pub version: i32,
    pub description: Option<String>,
    pub content_type: String,
    pub shared: bool,
    pub layout: Option<Value>,
    pub data: Option<Value>,
    pub style: Option<Value>,
    pub variables: Option<Value>,
    pub permissions: Option<Value>,
    pub created: Option<String>,
    pub created_by: Option<String>,
    pub modified: Option<String>,
    pub modified_by: Option<String>
}


impl Resource {
    pub fn from_row(row: &Row) -> Resource {
        Resource {
            uuid: row.get(0),
            name: row.get(1),
            status: row.get(2),
            version: row.get(3),
            description: row.get(4),
            content_type: row.get(5),
            shared: row.get(6),
            layout: row.get(7),
            data: row.get(8),
            style: row.get(9),
            variables: row.get(10),
            permissions: row.get(11),
            created: row.get(12),
            created_by: row.get(13),
            modified: row.get(14),
            modified_by: row.get(15),
        }
    }
}
