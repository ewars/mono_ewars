use std::collections::HashMap;
use std::fs::{File, create_dir};
use std::path::{PathBuf};
use std::io::{Read, Write};

use failure::Error;
use serde_json as json;
use uuid::Uuid;
use rusqlite::{Connection, NO_PARAMS};

use crate::sql_data::{SQL_CREATE};
use crate::remote_client::{Account as InboundAccount, AccountUser};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Tenancy {}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PeerData {
    pub did: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Account {
    pub uuid: Uuid,
    pub account_name: String,
    pub domain: String,
    pub status: String,
    pub tki: String,
    pub seeded: bool,
    pub ts_ms: Option<usize>,
    pub canon: bool, // if an account is canon, we can just use the core connection
    pub peers: Option<HashMap<Uuid, PeerData>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct User {
    pub uuid: Uuid,
    pub name: String,
    pub email: String,
    pub role: String,
    pub uid: Uuid,
    pub status: String,
    pub toured: bool,
    pub token: String,
    pub tki: String,
}

static SQL_CHECK_TABLE: &'static str = r#"
    SELECT COUNT() AS total
    FROM sqlite_master
    WHERE type = 'table'
    AND name = 'users';
"#;

impl Account {
    pub fn is_db_init(&self, conn: &Connection) -> bool {
        let result: i32 = match &conn.query_row(&SQL_CHECK_TABLE, NO_PARAMS, |row| {
            let res: i32 = row.get(0);
            res
        }) {
            Ok(res) => res.clone(),
            Err(err) => {
                eprintln!("{:?}", err);
                0
            }
        };

        if result <= 0 {
            false 
        } else {
            true 
        }
    }
    

    pub fn generate_db(&self, conn: &Connection) -> bool {
        let _ = conn.query_row("PRAGMA journal_mode=WAL;", NO_PARAMS, |row| {
            let result: String = row.get(0);
        }).unwrap();

        for item in SQL_CREATE {
            let _ =  match conn.execute(&item, NO_PARAMS) {
                Ok(_) => println!("Created table"),
                Err(err) => {
                    eprintln!("Error creating table: {:?}", err);
                }
            };
        }

        println!("Created tables");

        true
    }

}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Notifications {
    pub flashWindow: i32,
    pub bounceIcon: bool,
    pub bounceIconType: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Config {
    pub version: u8,
    pub accounts: Vec<Account>,
    pub users: Vec<User>,
    pub did: Option<String>,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            version: 1,
            accounts: Vec::new(),
            users: Vec::new(),
            did: Some(Uuid::new_v4().to_string()),
        }
    }
}

impl Config {
    pub fn get_remotes(&self) -> Vec<String> {
        let mut items: Vec<String> = Vec::new();

        for item in &self.accounts {
            items.push(item.domain.clone());
        }

        items.sort();
        items.dedup();
        items
    }

    pub fn get_databases(&self) -> Vec<String> {
        let mut dbs: Vec<String> = Vec::new();

        for item in &self.accounts {
            if !dbs.contains(&item.tki) {
                dbs.push(item.tki.clone());
            }
        }

        dbs
    }

    // Load the config file from the local application folder, creating it if it doesn't exist
    pub fn load() -> Result<Self, ()> {
        let mut file_path: PathBuf = dirs::data_dir().unwrap();
        file_path.push("EWARS");

        if !file_path.as_path().exists() {
            let _ = create_dir(&file_path.as_path()).unwrap();
        }

        file_path.push("config");
        file_path.set_extension("json");

        if !file_path.as_path().exists() {
            let mut f = File::create(&file_path.as_path()).unwrap();
            let data = Config::default();

            f.write_all(json::to_string(&data).unwrap().as_bytes()).unwrap();
            let _ = f.sync_all().unwrap();
        }

        let mut file = File::open(&file_path.as_path()).unwrap();
        let mut data: String = String::new();
        file.read_to_string(&mut data).unwrap();

        let mut conf: Config = json::from_str(&data).unwrap();

        Ok(conf)
    }

    pub fn add_user(&mut self, user: &AccountUser) -> Result<(), Error> {
        // Check if the user already exists
        let mut exists = false;
        for ex_user in &self.users {
            if ex_user.uid == user.uid {
                if ex_user.tki == user.tki {
                    exists = true;
                }
            }
        }

        if !exists {
            self.users.push(User {
                uuid: user.uuid.clone(),
                name: user.name.clone(),
                email: user.email.clone(),
                role: user.role.clone(),
                uid: user.uid.clone(),
                status: user.status.clone(),
                toured: false,
                token: user.token.clone(),
                tki: user.tki.clone(),
            });
        }

        Ok(())
    }

    // Add an account to the system
    pub fn add_account(&mut self, account: &InboundAccount) -> Result<(), Error> {
        // Check if the account already exists
        
        let mut exists = false;
        for acc in &self.accounts {
            if acc.uuid == account.uuid {
                exists = true;
            }
        }

        if !exists {
            let mut is_canon: bool = false;
            if account.domain.contains("ewars.ws") {
                is_canon = true;
            }

            self.accounts.push(Account {
                uuid: account.uuid.clone(),
                account_name: account.name.clone(),
                domain: account.domain.clone(),
                status: account.status.clone(),
                tki: account.tki.clone(),
                seeded: false,
                ts_ms: Some(0),
                canon: is_canon,
                peers: Some(HashMap::new()),
            });
        }
        Ok(())
    }

    // Flush the config to disk
    pub fn flush(&self) -> Result<(), Error> {
        let mut file_path: PathBuf = dirs::data_dir().unwrap();
        file_path.push("EWARS");
        file_path.push("config");
        file_path.set_extension("json");
        let mut f = File::create(&file_path.as_path()).unwrap();

        f.write_all(json::to_string(&self).unwrap().as_bytes()).unwrap();
        let _ = f.sync_all().unwrap();

        Ok(())
    }
}
