use std::time::{SystemTime, UNIX_EPOCH};

use serde_json::Value;


#[derive(Debug, Deserialize, Serialize)]
pub struct Event {
    pub uuid: String,
    pub ts_ms: i32,
    pub did: String,
    pub uid: String,
    pub data: Value,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PublicEvent {
    pub uuid: String,
    pub peer_id: String,
    pub ts_ms: u32,
    pub data: Value,
    pub tki: String,
}

