use serde_json::Value;

use rusqlite::{Row};

#[derive(Debug, Deserialize, Serialize)]
pub struct User {
    pub uuid: String,
    pub role: String,
    pub permissions: Value,
    pub settings: Value,
    pub profile: Value,
    pub org_id: Option<String>,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
    pub status: String,
    pub name: String,
    pub email: String,
    pub password: String,
    pub accounts: Value,
}

impl User {
    pub fn from_row(row: &Row) -> User {
        User {
            uuid: row.get(0),
            role: row.get(1),
            permissions: row.get(2),
            settings: row.get(3),
            profile: row.get(4),
            org_id: row.get(5),
            created: row.get(6),
            created_by: row.get(7),
            modified: row.get(8),
            modified_by: row.get(9),
            status: row.get(10),
            name: row.get(11),
            email: row.get(12),
            password: row.get(13),
            accounts: row.get(14),
        }
    }
}
