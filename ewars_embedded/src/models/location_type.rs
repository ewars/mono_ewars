use rusqlite::{Row};
use serde_json::Value;


#[derive(Debug, Deserialize, Serialize)]
pub struct LocationType {
    pub uuid: String,
    pub name: Value,
    pub description: Option<String>,
    pub restrictions: Value,
    pub created: String,
    pub created_by: Option<String>,
    pub modified: String,
    pub modified_by: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct LocationTypeCommandResult {
    pub result: bool,
}

impl LocationType {
    pub fn from_row(row: &Row) -> LocationType {
        LocationType {
            uuid: row.get(0),
            name: row.get(1),
            description: row.get(2),
            restrictions: row.get(3),
            created: row.get(4),
            created_by: row.get(5),
            modified: row.get(6),
            modified_by: row.get(7),
        }
    }
}
