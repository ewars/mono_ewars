use serde_json;
use serde_json::Value;

use rusqlite::{Row};


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Device {
    pub uuid: String,
    pub last_seed: String,
}

impl Device {
    pub fn from_row(row: Row) -> Device {
        Device {
            uuid: row.get(0),
            last_seed: row.get(1),
        }
    }
}
