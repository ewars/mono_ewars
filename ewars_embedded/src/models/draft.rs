use std::collections::HashMap;

use serde_json::Value;
use serde_json as json;
use rusqlite::{Row, NO_PARAMS, Connection};
use chrono::prelude::*;

use uuid::Uuid;
use crate::models::utils::*;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Draft {
    pub uuid: Uuid,
    pub fid: Uuid,
    pub data: HashMap<String, Value>,
    pub created: DateTime<Utc>,
    pub created_by: Option<Uuid>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<Uuid>,
}

impl<'a, 'stmt> From<&Row<'a, 'stmt>> for Draft {
    fn from(row: &Row) -> Self {
        let data: HashMap<String, Value> = json::from_value(row.get("data")).unwrap();

        Draft {
            uuid: opt_uuid!(row, "uuid").unwrap(),
            fid: opt_uuid!(row, "fid").unwrap(),
            data: data,
            created: row.get("created"),
            created_by: opt_uuid!(row, "created_by"),
            modified: row.get("modified"),
            modified_by: opt_uuid!(row, "modified_by"),
        }
    }
}

static SQL_GET_DRAFT: &'static str = r#"
    SELECT * FROM drafts
    WHERE uuid = ?;
"#;

static SQL_UPDATE_DRAFT: &'static str = r#"
    UPDATE drafts
        SET data = :data,
            modified = :modified_time,
            modified_by = :modified_by
        WHERE uuid = :uuid;
"#;

static SQL_DELETE_DRAFT: &'static str = r#"
    DELETE FROM drafts WHERE uuid = :uuid;
"#;

impl Draft {
    // Get a draft by its UUID
    pub fn get_by_id(conn: &Connection, id: &Uuid) -> Result<Self, ()> {
        let draft: Option<Draft> = get_row!(&SQL_GET_DRAFT, &conn, Draft, &[&id.to_string()]);

        if let Some(c) = draft {
            Ok(c)
        } else {
            Err(())
        }
    }

    // Update the draft in the sysgtem
    pub fn update(&self, conn: &Connection, uid: &Uuid) -> Result<(), ()> {
        let data: Value = json::to_value(&self.data).unwrap();
        let cur_time = Utc::now();
        match execute_named!(&SQL_UPDATE_DRAFT, &conn, &[
            (":data", &data),
            (":modified", &cur_time),
            (":modified_by", &uid.to_string()),
        ]) {
            true => Ok(()),
            false => Err(()),
            _ => Err(())
        }
    }

    pub fn delete_by_id(conn: &Connection, id: &Uuid) -> Result<(), ()> {
        match execute_named!(&SQL_DELETE_DRAFT, &conn, &[
            (":uuid", &id.to_string()),
        ]) {
            true => Ok(()),
            false => Err(()),
            _ => Err(())
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct NewDraft {
    pub fid: Uuid,
    pub data: HashMap<String, Value>,
}

static SQL_CREATE_DRAFT: &'static str = r#"
    INSERT INTO drafts
    (uuid, fid, data, created, created_by, modified, modified_by)
    VALUES (:uuid, :fid, :data, :created, :created_by, :modified, :modified_by);
"#;

impl NewDraft {
    pub fn insert(&self, conn: &Connection, uid: &Uuid) -> Result<Uuid, ()> {
        let new_uuid = Uuid::new_v4();

        let data: Value = json::to_value(&self.data).unwrap();
        let cur_time = Utc::now();
        let result: bool = execute_named!(&SQL_CREATE_DRAFT, &conn, &[
            (":uuid", &new_uuid.to_string()),
            (":fid", &self.fid.to_string()),
            (":data", &data),
            (":created", &cur_time),
            (":created_by", &uid.to_string()),
            (":modified", &cur_time),
            (":modified_by", &uid.to_string()),
        ]);

        if result {
            Ok(new_uuid)
        } else {
            Err(())
        }
    }
}
