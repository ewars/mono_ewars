use serde_json as json;
use serde_json::Value;
use rusqlite::{Row, Rows, Connection};
use uuid::Uuid;
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct Role {
    pub uuid: String,
    pub name: String,
    pub status: String,
    pub description: String,
    pub forms: Option<Vec<String>>,
    pub permissions: Option<Value>,
    pub inh: String,
    pub created: DateTime<Utc>,
    pub created_by: Option<String>,
    pub modified: DateTime<Utc>,
    pub modified_by: Option<String>,
}

static SQL_CREATE_ROLE: &'static str = r#"
    INSERT OR REPLACE INTO roles
    (uuid, name, description, status, forms, permissions, inh, created, created_by, modified, modified_by)
    VALUES (:uuid, :name, :description, :status, :forms, :permissions, :inh, :created, :created_by, :modified, :modified_by);
"#;

static SQL_GET_ROLE: &'static str = r#"
    SELECT * FROM roles WHERE uuid = ?;
"#;

static SQL_DELETE_ROLE: &'static str = r#"
"#;

impl Role {
    pub fn insert(&self, conn: &Connection) -> Result<(), ()> {
        let mut forms: Option<Value> = Option::None;
        if let Some(c) = &self.forms {
            forms = Some(json::to_value(c.clone()).unwrap());
        }

        match &conn.execute_named(&SQL_CREATE_ROLE, &[
            (":uuid", &self.uuid),
            (":name", &self.name),
            (":description", &self.description),
            (":status", &self.status),
            (":forms", &forms),
            (":permissions", &self.permissions),
            (":inh", &self.inh),
            (":created", &self.created),
            (":created_by", &self.created_by),
            (":modified", &self.modified),
            (":modified_by", &self.modified_by),
        ]) {
            Ok(_) => Ok(()),
            Err(err) => {
                eprintln!("{:?}", err);
                Err(())
            }
        }
    }

    pub fn get_role_by_id(conn: &Connection, id: &String) -> Result<Self, ()> {
        let role: Option<Role> = get_row!(&SQL_GET_ROLE, &conn, Role, &[&id]);

        if let Some(r) = role {
            Ok(r)
        } else {
            Err(())
        }
    }

}

impl<'a, 'stmt> From<&Row<'a, 'stmt>> for Role {
    fn from(row: &Row) -> Self {
        let forms: Option<Vec<String>> = json::from_value(row.get("forms")).unwrap();
        let permissions: Option<Value> = json::from_value(row.get("permissions")).unwrap();
        Role {
            uuid: row.get("uuid"),
            name: row.get("name"),
            status: row.get("status"),
            description: row.get("description"),
            forms: forms,
            permissions: permissions,
            inh: row.get("inh"),
            created: row.get("created"),
            created_by: row.get("created_by"),
            modified: row.get("modified"),
            modified_by: row.get("modified_by"),
        }
    }
}

