use std::rc::Rc;
use std::io;
use std::cell::RefCell;
use std::collections::HashMap;

use failure::Error;
use serde_json;
use serde_json::Value;
use chrono::{NaiveDate, Utc, Duration, Local};

use rusqlite::{NO_PARAMS};

use sonomalib::models::{Conf};

use crate::utils::date_utils;
use crate::system::metrics;
use crate::system::form_reporting::{FormReporting, FormFeature};
use crate::system::location_reporting_period::{LocationReportingPeriod};
use crate::system::forecast::Forecast;

use crate::state::ApplicationState;

#[derive(Debug, Serialize)]
pub struct RecordDue {
    pub form_name: Value,
    pub location_name: Option<Value>,
    pub record_date: Option<String>,
    pub overdue: bool,
    pub location_id: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct DashboardMetrics {
    pub tasks_open: i32,
    pub conf: Vec<Conf>,
    pub alerts: i32,
    pub records_overdue: i32,
    pub news_count: i32,
    pub forecast_count: i32,
    pub forecasts: Vec<Forecast>,
    pub forecast_days: Vec<String>,
    pub records: Vec<ReportingAssignment>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ReportingAssignment {
    pub location_id: Option<String>,
    pub form_id: String,
    pub assign_type: String,
    pub start_date: String,
    pub end_date: Option<String>,
    pub reporting_interval: Option<String>,
    pub form_name: Value,
    pub location_name: Option<Value>,
    pub records_due: Option<Vec<String>>,
    pub records_existing: Option<Vec<String>>,
}

static SQL_GET_CONF: &'static str = r#"
    SELECT * FROM conf;
"#;

// Get all data to power the main dashboard for this users
pub fn get_dashboard_data(state: Rc<RefCell<ApplicationState>>, args: Value) -> Result<Value, Error> {
    let conn = state.borrow().get_current_pool().unwrap().get().unwrap();
    let tasks_open: i32 = metrics::get_total_tasks(state.clone()).unwrap();;
    let alerts: i32 = metrics::get_total_open_alerts(state.clone()).unwrap();
    let records_overdue: i32 = 0;
    let news_count: i32 = metrics::get_total_news_items(state.clone()).unwrap();
    let forecast_count: i32 = 0;
    let forecasted: Vec<i32> = Vec::new();
    let records: Vec<RecordDue> = Vec::new();
    let mut forecasts: Vec<Forecast> = Vec::new();
    let mut forecast_days: Vec<String> = Vec::new();

    let mut stmt = conn.prepare(&SQL_GET_CONF)?;
    let rows = stmt.query_map(NO_PARAMS, |row| {
        Conf::from(row)
    })?;
    let conf: Vec<Conf> = rows.map(|x| x.unwrap()).collect();

    let result = DashboardMetrics {
        records: Vec::new(),
        conf: conf,
        tasks_open,
        alerts,
        records_overdue,
        news_count,
        forecast_count,
        forecasts,
        forecast_days
    };

    let output: Value = serde_json::to_value(result).unwrap();

    Ok(output)
}

impl ReportingAssignment {
    // Get the reports that an assignment SHOULD have for given dates
    fn get_reports_due(&mut self) -> Result<Vec<String>, io::Error> {
        let end_date = self.end_date.clone();
        let end_date = match end_date {
            Some(val) => val,
            None => {
                let dt: NaiveDate = Local::now().date().naive_local();
                dt.to_string()
            }
        };
        eprintln!("{}", end_date);
        eprintln!("{}", self.start_date);

        let mut dates: Vec<String> = Vec::new();
        let mut rx_date = NaiveDate::parse_from_str(&self.start_date, "%Y-%m-%d").unwrap();
        let n_end_date = NaiveDate::parse_from_str(&end_date, "%Y-%m-%d").unwrap();
        let mut done = false;

        while !done {
            let mut new_date: NaiveDate = rx_date + Duration::days(7);
            dates.push(new_date.to_string());
            rx_date = new_date;

            if rx_date >= n_end_date {
                done = true;
            }
        }

        self.records_due = Some(dates.to_owned());
        Ok(self.records_due.clone().unwrap())
    }

    // Pass in a set of record dates that exist in order to populate
    // the records_due property of the assignment
    fn extrapolate(&mut self, records: Vec<String>) {
        self.records_existing = Some(records.to_owned());
    }
}




