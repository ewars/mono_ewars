use std::collections::HashMap;
use serde_json::Value;

#[derive(Debug, Deserialize, Serialize)]
pub struct Forecast {
    pub forecast: HashMap<String, i32>,
    pub days: Vec<String>,
    pub form_name: Value,
    pub form_id: i32,
}

