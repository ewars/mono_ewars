use serde_json::Value;
use serde_json as json;

#[derive(Debug, Serialize, Deserialize)]
pub struct IntervalReportingFeature {
    pub interval: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocationReportingFeature {
    pub site_type_id: Option<i32>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FormFeature {
    pub INTERVAL_REPORTING: Option<IntervalReportingFeature>,
    pub LOCATION_REPORTING: Option<LocationReportingFeature>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FormReporting {
    pub features: FormFeature,
    pub form_id: i32,
    pub form_name: Value,
}

impl FormReporting {
    // Does this form have interval reporting
    pub fn is_interval(&self) -> bool {
        let interval = self.interval();

        match interval {
            Some(_) => true,
            None => false,
        }
    }

    // Does this form have location reporting
    pub fn is_location(&self) -> bool {
        let lti = self.site_type_id();

        match lti {
            Some(_) => true,
            None => false,
        }
    }

    // Get the reporting interval for this form
    pub fn interval(&self) -> Option<String> {
        let interval_reporting = match &self.features.INTERVAL_REPORTING {
            Some(res) => res.interval.clone(),
            None => None,
        };

        match interval_reporting {
            Some(res) => Some(res),
            None => None
        }
    }

    // Get the site type id for this form
    pub fn site_type_id(&self) -> Option<i32> {
        let lti = match &self.features.LOCATION_REPORTING {
            Some(res) => res.site_type_id.clone(),
            None => None
        };

        match lti {
            Some(res) => Some(res),
            None => None,
        }
    }
}



