use serde_json as json;
use serde_json::Value;
use chrono::{NaiveDate, Local, Datelike};


#[derive(Debug)]
pub struct LocationReportingPeriod {
    pub uuid: String,
    pub start_date: String,
    pub end_date: Option<String>,
}

impl LocationReportingPeriod {
    // Check if a date is within the start and end period of
    // reporting for this reporting period
    pub fn is_date_in_range(&self, reporting_date: &NaiveDate) -> bool {
        let mut end_date: NaiveDate = Local::now().date().naive_local();
        let start_date = NaiveDate::parse_from_str(&self.start_date, "%Y-%m-%d").unwrap();
        let date_to_check = reporting_date.clone();
        match &self.end_date {
            Some(res) => {
                end_date = NaiveDate::parse_from_str(&*res, "%Y-%m-%d").unwrap();
            },
            None => ()
        };

        let mut result: bool = false;

        if date_to_check >= start_date {
            if date_to_check <= end_date {
                result = true;
            }
        }

        result
    }
}
