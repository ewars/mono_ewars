use std::io;
use actix::prelude::*;
use serde_json as json;
use serde_json::Value;
use tokio::codec::{Decoder, Encoder};

use bytes::{BytesMut};

#[derive(Debug, Message, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum DiscoveryIncoming {
    Ping,
    Ok,
    Message(String),
}

#[derive(Debug, Message, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum DiscoveryOutgoing {
    Ping,
    Ok,
    Message(String),
}

pub struct DiscoveryCodec;

impl Decoder for DiscoveryCodec {
    type Item = DiscoveryIncoming;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        eprintln!("{:?}", src);
        if src.is_empty() {
            return Err(io::Error::new(io::ErrorKind::Other, "Datagram was empty"));
        }
        let data = String::from_utf8_lossy(src);
        let result: Result<Option<DiscoveryIncoming>, io::Error> = match json::from_str(&data) {
            Ok(res) => {
                Ok(Some(res))
            },
            Err(err) => {
                eprintln!("{:?}", err);
                Err(io::Error::new(io::ErrorKind::Other, "Could not parse datagram"))
           }
       };

        eprintln!("{:?}", result);
        result
    }
}

impl Encoder for DiscoveryCodec {
    type Item = DiscoveryOutgoing;
    type Error = io::Error;

    fn encode(&mut self, msg: DiscoveryOutgoing, dst: &mut BytesMut) -> Result<(), Self::Error> {
        eprintln!("{:?}", msg);
        eprintln!("{:?}", dst);
        let mut vec = json::to_vec(&msg)?;
        vec.push(b'\n');
        dst.extend_from_slice(&vec);
        Ok(())
    }
}

