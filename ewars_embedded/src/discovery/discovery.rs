use futures;
use futures::{Future, Sink};
use futures::sync::mpsc::{unbounded, UnboundedReceiver, UnboundedSender};
use futures::sink::SinkFromErr;
use std::net;
use std::net::{SocketAddr, IpAddr, Ipv4Addr, SocketAddrV4};
use std::time;
use std::io::Error as IoError;
use futures::stream::{MapErr, Stream, SplitSink};
use std::time::Duration;
use std::collections::VecDeque;
use std::io as std_io;
use std::boxed::Box;
use std::rc::Rc;
use std::cell::RefCell;

use bytes::{BytesMut};
use actix::prelude::*;
use actix::io as act_io;
use tokio::io::{AsyncWrite, AsyncRead, WriteHalf};
use tokio::net::{UdpSocket, UdpFramed};
use tokio::reactor::{Handle};
use tokio::codec::{FramedRead, FramedWrite, BytesCodec};
use socket2;
use socket2::{Domain, Protocol, Socket, Type};

use crate::discovery::{DiscoveryCodec, DiscoveryIncoming, DiscoveryOutgoing};
use crate::discovery::discovery_codec_error::{CodecError};

use crate::sonoma::Application;
use crate::state::ApplicationState;


pub const PORT: u16 = 7645;
const DEFAULT_MULTICAST: [u8; 4] = [239, 235, 42, 98];
const IP_ALL: [u8; 4] = [0, 0, 0, 0];

#[derive(Message)]
struct UdpPacket(BytesMut, SocketAddr);

type FromErrType = SinkFromErr<SplitSink<UdpFramed<DiscoveryCodec>>, CodecError>;
type ReceiverType = UnboundedReceiver<(DiscoveryOutgoing, net::SocketAddr)>;
type SenderType = UnboundedSender<(DiscoveryOutgoing, net::SocketAddr)>;


pub struct Discovery {
    pub sink: Option<SplitSink<UdpFramed<DiscoveryCodec>>>,
    pub sender: Option<UnboundedSender<(DiscoveryOutgoing, net::SocketAddr)>>,
    pub timeout: u64,
    pub app: Addr<Application>,
}

impl Actor for Discovery {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        eprintln!("MCAST: Started actor");
        let u_addr = SocketAddrV4::new(IP_ALL.into(), PORT);
        let maddr = SocketAddrV4::new(DEFAULT_MULTICAST.into(), PORT);

        let udp_socket = Socket::new(Domain::ipv4(), Type::dgram(), Some(Protocol::udp())).unwrap();
        eprintln!("MCAST: Socket connected");
        udp_socket.set_reuse_address(true).unwrap();
        #[cfg(target_os = "macos")]
        #[cfg(target_id = "linux")]
        udp_socket.set_reuse_port(true).unwrap();
        udp_socket.set_broadcast(true).unwrap();
        udp_socket.bind(&socket2::SockAddr::from(u_addr)).unwrap();
        udp_socket.set_multicast_loop_v4(true).unwrap();
        match udp_socket.join_multicast_v4(
            maddr.ip(),
            u_addr.ip(),
        ) {
            Ok(_) => (),
            Err(err) => {
                eprintln!("MCAST: Could not bind to multicast address: {}", err);
                self.restart_later(ctx);
            }
        };
        eprintln!("MCAST: Joined multicast group");

        let reactor = Handle::default();
        
        let t_socket = UdpSocket::from_std(udp_socket.into_udp_socket(), &reactor).unwrap();
        let (sink, stream) = UdpFramed::new(t_socket, DiscoveryCodec).split();
        let (sender, receiver) = unbounded::<(DiscoveryOutgoing, net::SocketAddr)>();
        ctx.add_stream(stream.map(|(data, _sender)| {
            data
        }));
        let map_fn = |_: (FromErrType, MapErr<ReceiverType, _>)| ();
        Arbiter::spawn(
            sink.sink_from_err()
                .send_all(receiver.map_err(move |e| {
                    eprintln!("UDP receiver channel error: {:?}", e);
                }))
                .map(map_fn)
                .map_err(move |e| {
                    eprintln!("UDP send error: {:?}", e);
                }),
        );


        self.sender = Some(sender);
        eprintln!("MCAST: Extrapolated streams");
        self.reset_timeout();
        self.hb(ctx);
    }
}

impl Discovery {
    pub fn new(app: Addr<Application>) -> Self {
        Self {
            sink: None,
            sender: None,
            timeout: 0,
            app: app.clone()
        }
    }

    #[inline]
    fn reset_timeout(&mut self) {
        self.timeout = 0;
    }

    fn restart_later(&mut self, ctx: &mut Context<Self>) {
        eprintln!("MCAST: Attempting restart");
        const TIMEOUT_INTERVAL: u64 = 10;
        const TIMEOUT_MAX: u64 = 90;

        if self.timeout < TIMEOUT_MAX {
            self.timeout += TIMEOUT_INTERVAL;
        }

        eprintln!("MCAST: Timeout: {}", self.timeout);

        ctx.run_later(time::Duration::new(self.timeout, 0), |_, ctx| ctx.stop());
    }

    fn hb(&self, ctx: &mut Context<Self>) {
        ctx.run_later(Duration::new(60, 0), |act, ctx| {
            eprintln!("MCAST: Announcing");
            let maddr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(239, 235, 42, 98)), PORT);

            eprintln!("{:?}", maddr);
            let data = (DiscoveryOutgoing::Ping, maddr);

            if let Some(ref mut x) = act.sender {
                if let Err(e) = x.unbounded_send(data) {
                    eprintln!("MCAST: Udp unbounded send failed: {}", e);
                }
            }
            act.hb(ctx);
        });
    }


}

impl act_io::WriteHandler<IoError> for Discovery {
    fn error(&mut self, err: IoError, _: &mut Self::Context) -> Running {
        eprintln!("MCAST: connection dropped: error: {}", err);
        Running::Stop
    }
}

impl Supervised for Discovery {
    fn restarting(&mut self, _: &mut Self::Context) {
        eprintln!("MCAST: Restarting...");
        self.sink.take();
    }
}

impl StreamHandler<DiscoveryIncoming, IoError> for Discovery {
    fn error(&mut self, error: IoError, ctx: &mut Self::Context) -> actix::Running {
        eprintln!("MCAST: IO Error: {}", error);
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        eprintln!("MCAST: Connection is closed");
        ctx.stop();
    }

    fn handle(&mut self, msg: DiscoveryIncoming, _ctx: &mut Self::Context) {
        eprintln!("MCAST: RECV: {:?}", msg);
    }
}





