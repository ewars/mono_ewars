use std::collections::HashMap;

use serde_json as json;
use serde_json::Value;


#[macro_use]
macro_rules! pull_uuid {
    ($row: expr, $col: expr) => {
        {
            let val: String = $row.get($col);
            Uuid::parse_str(&val).unwrap()
        }
    }
}


#[macro_use]
macro_rules! opt_field {
    ($row: expr, $name: expr) => {
        {
            match $row.get_opt($name) {
                Some(res) => res.unwrap(),
                None => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_field_json {
    ($row: expr, $name: expr) => {
        {
            match $row.get_opt($name) {
                Some(Ok(res)) => {
                    Some(json::from_value(res).unwrap())
                },
                Some(Err(err)) => {
                    dbg!(err);
                    Option::None
                }
                None => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_field_value {
    ($row: expr, $name: expr) => {
        {
            let v_data: Option<Value> = $row.get($name);
            match v_data {
                Some(res) => {
                    if res.is_null() {
                        Option::None
                    } else {
                    Some(json::from_value(res).unwrap())
                    }
                }
                None => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_uuid {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Text(c) => {
                            Some(Uuid::parse_str(&c).unwrap())
                        },
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_int {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Integer(c) => {
                            Some(c)
                        },
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_value {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Text(c) => {
                            eprintln!("{}", c);
                            match c {
                                "null" => Option::None,
                                "" => Option::None,
                                _ => Some(json::from_str(&c).unwrap())
                            }
                        },
                        ValueRef::Null => Option::None,
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

#[macro_use]
macro_rules! opt_string {
    ($row: expr, $name: expr) => {
        {
            use rusqlite::types::ValueRef;
            match $row.get_raw_checked($name) {
                Ok(res) => {
                    match res {
                        ValueRef::Text(c) => {
                            Some(c.to_string())
                        },
                        _ => Option::None
                    }
                },
                Err(_) => Option::None
            }
        }
    }
}

macro_rules! get_row {
    ($sql: expr, $conn: expr, $model: ident, $args: expr) => {
        {
            match $conn.query_row($sql, $args, |row| {
                $model::from(row)
            }) {
                Ok(res) => {
                    Some(res)
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

macro_rules! in_opt_value {
    ($val: expr) => {
        {
            if let Some(c) = $val {
                Some(json::to_value(c.clone()).unwrap())
            } else {
                Option::None
            }
        }
    }
}

macro_rules! in_opt_uuid {
    ($val: expr) => {
        {
            if let Some(c) = $val {
                Some(c.clone().to_string())
            } else {
                Option::None
            }
        }
    }
}


macro_rules! pg_get_row {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $args: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), $args) {
                Ok(rows) => {
                    if let Some(c) = rows.iter().next() {
                        Some($model::from(c))
                    } else {
                        Option::None
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

macro_rules! get_scalar {
    ($conn: expr, $sql: expr, $tki: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    0
                }
            }
        }
    }
}


macro_rules! get_vec_from {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[]) {
                Ok(rows) => {
                    rows.iter().map(|x| $model::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }
        }
    }
}

macro_rules! get_single_from {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $id: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[&$id]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        Some($model::from(row))
                    } else {
                        Option::None
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

#[macro_export]
macro_rules! get_single_from_args {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $args: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), $args) {
                Ok(rows) => {
                    if let Some(c) = rows.iter().next() {
                        Some($model::from(c))
                    } else {
                        Option::None
                    }
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Option::None
                }
            }
        }
    }
}

macro_rules! get_count_result {
    ($conn: expr, $sql: expr, $tki: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), &[]) {
                Ok(rows) => {
                    if let Some(row) = rows.iter().next() {
                        row.get(0)
                    } else {
                        0
                    } 
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    0
                }
            }
        }
    }
}

macro_rules! get_vec_from_args {
    ($conn: expr, $sql: expr, $model: ident, $tki: expr, $args: expr) => {
        {
            match &$conn.query(&$sql.replace("{SCHEMA}", &$tki), $args) {
                Ok(rows) => {
                    rows.iter().map(|x| $model::from(x)).collect()
                },
                Err(err) => {
                    eprintln!("{:?}", err);
                    Vec::new()
                }
            }

        }
    }
}
