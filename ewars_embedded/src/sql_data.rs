pub static SQL_CREATE: &'static [&str] = &[
    r#"
        CREATE TABLE alarms (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            description TEXT NULL,
            version INTEGER NOT NULL DEFAULT 2,
            definition JSON NOT NULL DEFAULT '{}',
            workflow JSON NOT NULL DEFAULT '{"definition": {}, "stages": []}',
            permissions JSON NOT NULL DEFAULT '{}',
            guidance TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by UUID NULL
        );
    "#,
    r#"
        CREATE TABLE alerts (
            uuid TEXT PRIMARY KEY NOT NULL,
            alid TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'OPEN',
            eid TEXT NULL,
            alert_date DATE NOT NULL DEFAULT CURRENT_TIMESTAMP,
            lid TEXT NULL,
            stage TEXT NOT NULL,
            data JSON NOT NULL DEFAULT '{}',
            workflow JSON NOT NULL DEFAULT '[]',
            events JSON NOT NULL DEFAULT '[]',
            raised DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            closed DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by UUID NULL
        );
    "#,
    r#"
        CREATE TABLE assignments (
            uuid TEXT PRIMARY KEY NOT NULL,
            uid TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'ACTIVE',
            fid TEXT NULL,
            lid TEXT NULL,
            assign_group TEXT NULL,
            assign_type TEXT NOT NULL DEFAULT 'DEFAULT',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE conf (
            uuid TEXT PRIMARY KEY NOT NULL,
            key TEXT NOT NULL,
            value JSON NOT NULL DEFAULT '{}',
            grp TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE devices (
            uuid TEXT PRIMARY KEY NOT NULL,
            device_type TEXT NOT NULL DEFAULT 'ANDROID',
            last_seed DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            did TEXT NULL,
            uid TEXT NULL,
            info JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE documents (
            uuid TEXT PRIMARY KEY NOT NULL,
            template_name TEXT NOT NULL DEFAULT 'New document',
            instance_name TEXT NOT NULL DEFAULT 'New document',
            status TEXT NOT NULL DEFAULT 'DRAFT',
            version INTEGER NOT NULL DEFAULT 2,
            theme TEXT NOT NULL DEFAULT 'DEFAULT',
            layout JSON NOT NULL DEFAULT '[]',
            content TEXT NULL,
            data JSON NOT NULL DEFAULT '{}',
            generation JSON NOT NULL DEFAULT '{}',
            orientation TEXT NOT NULL DEFAULT 'PORTRAIT',
            template_type TEXT NOT NULL DEFAULT 'GENERATED',
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE drafts (
            uuid TEXT PRIMARY KEY NOT NULL,
            fid TEXT REFERENCES forms (uuid) NOT NULL,
            data JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE event_log (
            uuid TEXT PRIMARY KEY NOT NULL,
            event_type TEXT NOT NULL,
            res_type TEXT NOT NULL,
            res_id TEXT NOT NULL,
            did TEXT NOT NULL,
            ts_ms INTEGER NOT NULL,
            data JSON NULL,
            version INTEGER NOT NULL DEFAULT 1,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        );
    "#,
    r#"
        CREATE TABLE forms (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            version INTEGER NOT NULL DEFAULT 2,
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            description TEXT NOT NULL DEFAULT '',
            guidance TEXT NOT NULL DEFAULT '',
            eid_prefix TEXT NULL,
            features JSON NOT NULL DEFAULT '{}',
            definition JSON NOT NULL DEFAULT '{"definition": {}, "stages": []}',
            constraints JSON NOT NULL DEFAULT '[]',
            stages JSON NULL,
            etl JSON NOT NULL DEFAULT '{}',
            changes JSON NOT NULL DEFAULT '[]',
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE indicator_groups (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            description TEXT NULL,
            pid TEXT NULL,
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE indicators (
            uuid TEXT PRIMARY KEY NOT NULL,
            group_id TEXT NULL,
            name TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'ACTIVE',
            description TEXT NULL,
            itype TEXT NOT NULL DEFAULT 'DEFAULT',
            icode TEXT NULL,
            permissions JSON NOT NULL DEFAULT '{}',
            definition JSON NOT NULL DEFAULT '[]',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE location_types (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            description TEXT NULL,
            restrictions JSON NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE locations (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            pcode TEXT NULL,
            codes JSON NOT NULL DEFAULT '{}',
            groups JSON NOT NULL DEFAULT '[]',
            data JSON NOT NULL DEFAULT '{}',
            lti TEXT NOT NULL,
            pid TEXT NULL,
            lineage JSON NOT NULL DEFAULT '[]',
            organizations JSON NOT NULL DEFAULT '[]',
            geometry_type TEXT NOT NULL DEFAULT 'POINT',
            geojson JSON NULL,
            population JSON NOT NULL DEFAULT '[]',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE organizations (
            uuid TEXT PRIMARY KEY NOT NULL,
            name JSON NOT NULL DEFAULT '{}',
            description TEXT NULL,
            acronym TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE records (
            uuid TEXT PRIMARY KEY NOT NULL,
            status TEXT NOT NULL,
            stage TEXT NULL,
            workflow JSON NOT NULL DEFAULT '[]',
            fid TEXT NOT NULL,
            data JSON NOT NULL DEFAULT '{}',
            submitted DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            submitted_by TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL,
            revisions JSON NOT NULL DEFAULT '[]',
            comments JSON NOT NULL DEFAULT '[]',
            eid TEXT NULL,
            import_id TEXT NULL,
            source TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE reporting (
            uuid TEXT PRIMARY KEY NOT NULL,
            lid TEXT NOT NULL,
            fid TEXT NOT NULL,
            start_date DATE NOT NULL,
            end_date DATE NULL,
            status TEXT NOT NULL DEFAULT 'ACTIVE',
            pid TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE resources (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'ACTIVE',
            version INTEGER NOT NULL DEFAULT 2,
            description TEXT NULL,
            content_type TEXT NOT NULL,
            shared BOOL NOT NULL DEFAULT FALSE,
            layout JSON NOT NULL DEFAULT '[]',
            data JSON NOT NULL DEFAULT '{}',
            style JSON NOT NULL DEFAULT '{}',
            variables JSON NOT NULL DEFAULT '[]',
            permissions JSON NOT NULL DEFAULT '{}',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE roles (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            description TEXT NULL,
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            forms JSON NOT NULL DEFAULT '[]',
            permissions JSON NOT NULL DEFAULT '{}',
            inh TEXT NOT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE tasks (
            uuid TEXT PRIMARY KEY NOT NULL,
            status TEXT NOT NULL DEFAULT 'OPEN',
            task_type TEXT NULL,
            data JSON NOT NULL DEFAULT '{}',
            actions JSON NOT NULL DEFAULT '[]',
            version INTEGER NOT NULL DEFAULT 2,
            form JSON NOT NULL DEFAULT '{}',
            context JSON NOT NULL DEFAULT '{}',
            roles JSON NOT NULL DEFAULT '[]',
            location TEXT NULL,
            outcome JSON NOT NULL DEFAULT '{}',
            actioned DATETIME NULL,
            actioned_by TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE team_messages (
            uuid TEXT PRIMARY KEY NOT NULL,
            ts_ms INTEGER NOT NULL,
            tis TEXT NOT NULL,
            content JSON NOT NULL DEFAULT '{}',
            pid TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE teams (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            description TEXT NULL,
            private BOOL NOT NULL DEFAULT FALSE,
            status TEXT NOT NULL DEFAULT 'ACTIVE',
            permissions JSON NOT NULL DEFAULT '{}',
            members JSON NOT NULL DEFAULT '[]',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE users (
            uuid TEXT PRIMARY KEY NOT NULL,
            role TEXT NOT NULL,
            permissions JSON NOT NULL DEFAULT '{}',
            settings JSON NOT NULL DEFAULT '{}',
            profile JSON NOT NULL DEFAULT '{}',
            org_id TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL,
            status TEXT NOT NULL,
            name TEXT NOT NULL,
            email TEXT NOT NULL,
            password TEXT NOT NULL,
            system BOOL NOT NULL DEFAULT FALSE,
            accounts JSON NOT NULL DEFAULT '[]'
        );
    "#,
    r#"
        CREATE TABLE news (
            uuid TEXT PRIMARY KEY NOT NULL,
            title TEXT NOT NULL,
            content TEXT NOT NULL,
            pubslihed DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#,
    r#"
        CREATE TABLE outbreaks (
            uuid TEXT PRIMARY KEY NOT NULL,
            name TEXT NOT NULL,
            description TEXT NOT NULL,
            start_date DATE NOT NULL,
            end_date DATE NULL,
            status TEXT NOT NULL DEFAULT 'INACTIVE',
            locations JSON NOT NULL DEFAULT '[]',
            forms JSON NOT NULL DEFAULT '[]',
            created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            created_by TEXT NULL,
            modified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            modified_by TEXT NULL
        );
    "#
];

