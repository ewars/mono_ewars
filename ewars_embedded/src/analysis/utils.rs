#![allow(dead_code)]
use chrono::{Date, Utc};
use std::collections::HashMap;

pub struct AnalysisValue {
    pub date: Date<Utc>,
    pub value: f64,
}

pub type FunctionResult = Vec<AnalysisValue>;
pub type FunctionData = Vec<AnalysisValue>;

pub fn aggregate(data: Vec<AnalysisValue>, dates: Vec<Date<Utc>>, agg_type: String) -> Result<Vec<AnalysisValue>, String> {
    unimplemented!()
}

// Get absolute value for a series of values
pub fn absolute(values: Vec<f64>) -> Result<f64, String> {
    unimplemented!()
}

pub fn apply_by_node(values: Vec<AnalysisValue>, template_function: String, new_name: Option<String>) -> Result<Vec<AnalysisValue>, String> {
    unimplemented!()
}

// Calculated a percentage of the total of a wildcard series, if total is specified, each series
// will be calculated as a percentage of total. If total is not specified, the sum of all points in
// the series will be used instead
pub fn as_percent(values: Vec<AnalysisValue>, total: Option<f64>) -> Result<Vec<AnalysisValue>, String> {
    unimplemented!()
}

// Takes one sreies or multiples and filters out series whose average is above `n`
pub fn average_above(values: Vec<AnalysisValue>, n: f64) -> Result<Vec<AnalysisValue>, String> {
    unimplemented!()
}

// Takes one mor more series and filters out series whose average is below `n`
pub fn average_below(values: Vec<AnalysisValue>, n: f64) -> Result<Vec<AnalysisValue>, String> {
    unimplemented!()
}

// Takes a set of series and removes ones lying within an average percentile interval
pub fn average_outside_percentile(series: Vec<AnalysisValue>, n: f64) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Short alias: avg()
// Takes one or more series of data, draws the average value of all metrics passed at each time
pub fn average_series(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a list of series and modifies them to provide column aligned output with Current, Max, Min
// values in the style of cacti. Optionally takes `system` value to apply unit formatting in the
// same style as the Y-axis, or a `unit` string to append to an arbitrary unit suffix
pub fn cacti_style(series: FunctionData, system: Option<String>, units: Option<String>) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes one metric or a wildcard seriesList. Output 1 when the value changed, 0 when null or the
// same
pub fn changed(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Applies the given color to the series list
pub fn color(series: FunctionData, color: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Taks one metric or a wildcard seriesList and a consolidation function name
// Available function names are `sum`, `average`, `min`, `max`, `first`, `last`
pub fn consolidate_by(series: FunctionData, consolidation_func: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Returns a value representing the number of points in the series
pub fn count_series(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a set of series and an integer N, OUt of all metrics passed, returns only the metrics
// whose value is anove N at the tail end of the series
pub fn current_above(series: FunctionData, n: f64) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a set of series and an integeer N, out of all the metrics passed, returns only 
// the series show value is below N at the tail end of the series
pub fn current_below(series: FunctionData, n: f64) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Shifts all smaples later by an integer number of steps. This can be used for custom
// derivation calculations, among other things. Note: this will pad the early end of the data with
// None for every step shifted.
// This shifting is indifferent to the type of periodicity of the series.
pub fn delay(series: FunctionData, steps: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// This is the opposite of an integral function. This is useful for taking a running 
// total metric and calculating the delta between subsequent data points
// This function does not normalize for periods of time, as a true derivative would. Instead see
// per_second() function to calculate a rate of change over time
pub fn derivative(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Subtracts series 2 through n from series 1
pub fn diff_series(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a dividend series and a divisor series and creates a resultant sseries
pub fn divide_series(dividend: FunctionData, divisor: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Iterates over the two lists of series, and divides them by the correlating indexed series
// for instance dividends[0] / dividend[1]
pub fn divide_by_series_lists(dividends: FunctionData, divisors: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes one metricr a series of metrics. If the value is zero, draw the line at 0, If the value
// is above 0, draw the line at infinity. If the value is null or less than zero, do not draw the
// line
// Usefule for displaying on/off metrics
pub fn draw_as_infinite(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Taks a set of series, followed by a string identifier, filters out any series which
// matches the pattern provided
pub fn exclude(series: FunctionData, pattern: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a series of values and a window size and produces an exponential moving average utilizing
// the following formula
// ema(current) = constant + (Current Value) + (1 - constant) * ema(previous)
// the constant is calculated as: constant = 2 / (windowSize + 1)
pub fn exponential_moving_average(series: FunctionData, window_size: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1...n series followed by a consolidation function, an operator and a threshold. Draws only
// the series which match the filter expression
// supported aggregations are: average, median, sum, min, max, diff, stddev, range, multiple and
// last
// support operators: = != > >= < & <=
pub fn fallback_series(series: FunctionData, func: String, operator: String, threshold: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takse a set of series and filters out series which don't match the provided pattern
pub fn grep(series: FunctionData, pattern: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a set of series and adds them to a single series, This is used to pass multiple
// seriesLists to a function which only takes one
pub fn group(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a series list and maps a callback to subgroups within as defined by a common node
// group_by_node(ganglia.by-functoin.*.*.cpu.load, 2, "sumSeries")
// would return multiple series which are each the result of applying "sum_series" to groups joined
// on the second node (0 indexed) resulting in a list of targets like
//
pub fn group_by_node(series: FunctionData, node_num: u32, callback: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a series list and maps a callback to subgroups within as degfined by multipoe nodes
// group_by_nodes(ganglia.server*.*.cpu.load*, "sum", 1, 4) 
// would return multiple series whjich are ach the result of applying sum aggregation to groups
// joined on the nodes's list (0 indexed) resulting in a list of taget
pub fn group_by_nodes(series: FunctionData, callback: String, nodes: Option<FunctionData>) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n series followed by an integer N and an aggregation functoin.
// OUt of all the metrics passed, draws only the N metrics with the highest aggregated value over
// the time period 
pub fn highest(series: FunctionData, n: i32, func: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n series followed by an integer N. OUt of all the metrics passed, draws only
// the top N metrics with the highest average value for the time period specified
pub fn highest_average(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n series followed by an integer N. OUt of all the series passed, drawes only
// the top N metrics base don highest most current value
pub fn highest_current(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n series followed by an integer, Out of all the series passed, returns only
// the top N series ordered DESC by the series maximum value
pub fn highest_max(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Estimate the hit counts from a list of time series
// This function assumes the values in each time series represent hits per second. It calculates
// hits per some larger interval such as per day or per hours. This function is like summarize(),
// except that it compensates automatically for different time scales (so that a similar graph
// results from using either fin-graned or coarse-grained records) and handles rarely-occurring
// events gracefully
pub fn hit_count(series: FunctionData, interval: String, align_to_interval: bool) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Performs a holt-winters forecast using the series as input data and plots the positive or
// negative deviation of the series data from the forecast
pub fn holt_winters_aberration(series: FunctionData, delta: i32, bootstrap_interval: String, seasonality: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Performs a Holt-Winters forecast using the series as input data and plots the positive or
// negative deviation of the series data from the forecast
pub fn holt_winters_confidence_area(series: FunctionData, delta: i32, bootratp_interval: String, seasonality: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Performs a Holt-Winters forecast using the series as iunput data and pots upper and lower bands
// with the predicted forecast deviations
pub fn holt_winters_confidence_bands(series: FunctionData, delta: i32, boostrap_interval: String, seasonality: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Performs a Holt-Winters forecast using the series as input data. Data from bootstrap_interval
// (one week by default) previous to the series is used to bootstrap the initial forecast
pub fn holt_winters_forecast(sreies: FunctionData, bootstrap_interval: String, seasonality: String) -> Result<FunctionData, String> {
    unimplemented!()
}

// This will show the sum over time, sort of like a continuios addition function. Useful for
// finding totals or trends in metrics that are collected over extended periods of time
pub fn integral(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// This will do the same as the integral() function, except resetting the total to 0 at the given
// time in the parameter "from" useful for finding totals per hour/day/week
pub fn integral_by_interval(series: FunctionData, interval_unit: String) -> Result<FunctionData, String> {
    unimplemented!()
}

// Takes 1n series and optionally a limit to the number of 'None' values to skip over. Continues
// the line with the last receive value when gaps ('None' values) appear in your data, rather than
// breaking the line
pub fn interpolate(series: FunctionData, limit: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n metrics and inverts each data point (i.e. 1/x)
pub fn invert(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n series and counts up the numer of non-null values. This is useful
// for understanding the number of metrics that have data at a given point in time (i.e. to count
// which reporting sites are alive)
// Returns a series where 1 is specified for non-null values and 0 for null values
pub fn is_non_null(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

//  Takes 1n series and optionally a limit to the number of 'NOne' values to skip over. Continues
//  the line with the last receive value when gaps ('None' values) appear in your adta rather than
//  breaking the line
pub fn keep_last_value(series: FunctionData, limit: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n series, and for each series appends the value types specified to the series metadata
// For instance 'sum', 'si', 'max
pub fn legend_value(series: FunctionData, value_types: Vec<String>) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes 1n series followed by and integer N
// only draws the first N series
pub fn limit(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Graphs the linear regression function by least squares method
// Takes 1n series, followed by a quoted string with the time to start the line and another quoted
// string with the time to end the line. The start and end times are inclusive (default range is
// from to until). Data points in the range are used to regression
pub fn linear_regression(series: FunctionData, start_source_at: Option<String>, end_source_at: Option<String>) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Returns factor and offset of linear regression functoin by least squares method
pub fn linear_regression_analysis(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// 1n series, a base and draws the y-acxis in logaritmic format. If the base is omitted the
// function defaults to base 10
pub fn logarithm(series: FunctionData, base: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Returns only series with the lowest aggregated value over the time period of the 
// series
pub fn lowest(series: FunctionData, n: i32, func: String) -> Result<FunctionResult, String> {
    unimplemented!()
}

// OUt of all series passed, drawss only the bottom N series with the lowest average value
pub fn lowest_average(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// OUt of all the series passed, draws only the N series with the lowest value at the end of
// the time period specified
pub fn lowest_curent(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Takes a series list and a list of additional series to map into the root series so that each
// series has the map_nodes series in common
pub fn map_series(series: FunctionData, map_nodes: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// For each datapoint from each series passed in, pick the maximum value and graph it
pub fn max_series(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}


// Returns only the series show max value is above N
pub fn maximum_above(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

//Returns only the seres whose max value is below N
pub fn maximum_below(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Applies the popular min max normalization technique, which takes each point and applies the
// following noramlization transformation to it: normalized = (point - min) / (max - min)
pub fn min_max(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// For each data point from each series, pick the minimum value
pub fn min_series(series: FunctionData) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Returns only the series whos minmum value is below N
pub fn minimum_below(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

// Returns the N most deviatn series. To find the deviants, the standard deviation (sigma) of each
// series is taken and ranked. The top N standard deviations are retuned
pub fn most_deviant(series: FunctionData, n: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

pub enum WindowSize {
    i32,
    String,
}

// Calculates the moving avera of metrics over a fixed number of past points, or a time interval
// N is number 
pub fn moving_average(series: FunctionData, window_size: Option<WindowSize>, x_files_factor: i32) -> Result<FunctionResult, String> {
    unimplemented!()
}

