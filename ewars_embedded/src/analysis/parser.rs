use uuid::Uuid;
use chrono::{DateTime, Utc};
use serde_json;
use serde_json::Value;

use crate::models::utils::ewars_date;

const SLICE: &'static str = "SLICE";
const SLICE_COMPLEX: &'static str = "SLICE_COMPLEX";
const SERIES: &'static str = "SERIES";
const SERIES_COMPLEX: &'static str = "SERIES_COMPLEX";
const GENERATOR: &'static str = "GENERATOR";
const TABLE: &'static str = "TABLE";
const CUSTOM: &'static str = "CUSTOM";
const DAY: &'static str = "DAY";
const WEEK: &'static str = "WEEK";
const MONTH: &'static str = "MONTH";
const YEAR: &'static str = "YEAR";

pub type Period = Vec<DateTime<Utc>>;

#[derive(Debug, Deserialize)]
pub struct IndicatorSpec {
    pub uuid: Uuid,
    pub dimension: String,
    pub form_id: Option<i32>
}

pub type IndicatorId = Uuid;

#[derive(Debug, Deserialize)]
pub enum Indicator {
    IndicatorDefinition,
    Uuid,
}

#[derive(Debug, Deserialize)]
pub enum LocationSpec {
    Uuid,
    LocationGenerator,
}

#[derive(Debug, Deserialize)]
pub struct LocationGenerator {
    pub uuid: Uuid,
    pub sti: i32,
    pub status: String,
}


#[derive(Debug, Deserialize)]
pub enum QueryType {
    SLICE,
    SLICE_COMPLEX,
    SERIES,
    SERIES_COMPLEX,
    GENERATOR,
    TABLE,
}

enum IntervalTypes {
    CUSTOM,
    DAY,
    WEEK,
    MONTH,
    YEAR,
}


#[derive(Debug, Deserialize)]
pub struct LegacyAnalysis {
    pub period: Period,
    pub indicator: IndicatorSpec,
    pub location: LocationSpec,
    pub interval: String,
    #[serde(with = "ewars_date")]
    pub start_date: DateTime<Utc>,
    #[serde(with = "ewars_date")]
    pub end_date: DateTime<Utc>,
    pub fill_missing: Option<bool>,
    pub series: Option<Value>,
    pub formula: Option<String>,
    pub reduction: Option<String>,

    pub geometry: Option<bool>,
    pub etype: QueryType,
}

#[derive(Debug, Deserialize)]
pub struct StrictLineAnalysis {
    pub definition: String,
    pub config: Option<String>,
}


pub enum QueryTypes {
    LegacyAnalysis,
    StrictLineAnalysis,
}

