const {app, BrowserWindow, appUpdater, dialog} = require("electron");
const {spawn} = require("child_process");
const path = require("path");
const url = require("url");
const fs = require("fs");
const SonomaClient = require("../shared/sonoma_client");
global.isDev = true;
const showUpdates = false;

const appApth = app.getAppPath();

if (showUpdates) {
	const DOMAIN = 'http://updates.ewars.ws';
	const suffix = process.platform === 'darwin' ? `/RELEASES.json?method=JSON&version=${app.getVersion()}` : '';
	autoUpdater.setFeedURL({
	  url: `${DOMAIN}/EWARS/f06f441f3c00bfcdeda5b81e0ea01ea9/${process.platform}/${process.arch}${suffix}`,
	  serverType: 'json',
	});

    autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
          const dialogOpts = {
                  type: 'info',
                  buttons: ['Restart', 'Later'],
                  title: 'Application Update',
                  message: process.platform === 'win32' ? releaseNotes : releaseName,
                  detail: 'A new version has been downloaded. Restart the application to apply the updates.'
                }

          dialog.showMessageBox(dialogOpts, (response) => {
                  if (response === 0) autoUpdater.quitAndInstall()
                })
    })

    autoUpdater.on('error', message => {
          console.error('There was a problem updating the application')
          console.error(message)
    })
}

let SERVER_PATH = process.env.SONOMA_SERVER_PATH;
if (!SERVER_PATH) {
	if (process.platform == "darwin" && !global.isDev) {
		SERVER_PATH = appPath + "/MacOs/sonoma_server";
		console.log(SERVER_PATH);
	}

	if (process.platform == "win32" && !global.isDev) {
		SERVER_PATH = appPath + "/sonoma_server.exe";
		console.log(SERVER_PATH);
	}

	if (global.isDev) {
		SERVER_PATH = "../sonoma_server/target/debug/sonoma_server";
	}
}

let SOCKET_PATH = process.env.SONOMA_SOCKET_PATH;
if (!SOCKET_PATH) {
    SOCKET_PATH = "127.0.0.1:9009";
}

function saveWindowState(file, window) {
    var windowState = window.getBounds();
    windowState.maximized = window.isMaximized();
    windowState.fullscreen = window.isFullScreen();
    try {
        fs.writeFileSync(file, JSON.stringify(windowState));
    } catch (e) {
        // [Linux] error happens only when the window state is changed before the config dir is created.
        console.log(e);
    }
}

class SonomaApplication {
    constructor (serverPath, socketPath) {
        this.serverPath = serverPath;
        this.socketPath = socketPath;
        this.readyPromise= new Promise(resolve => app.on("ready", resolve));
        this.sonomaClient = new SonomaClient();
        this.rendered = false;
    }

    async start () {
        const serverProcess = spawn(this.serverPath, [], {env: {RUST_BACKTRACE: 1}, stdio: ['ignore', 'pipe', 'inherit']});
        app.on("before-quit", () => serverProcess.kill());

        serverProcess.on("error", console.error);
        serverProcess.on("exit", () => app.quit());

        await new Promise(resolve => {
            let serverStdOut = "";
            serverProcess.stdout.on("data", data => {
                serverStdOut += data.toString("utf8");
                if (serverStdOut.includes("Listening\n")) resolve()
            });
        });

        await this.sonomaClient.start(this.socketPath);
        this.sonomaClient.addMessageListener(this._handleMessage.bind(this));
        this.sonomaClient.sendMessage({type: "SetMaster"});
    }

    async _handleMessage (message) {
        await this.readyPromise;
        switch (message.type) {
            case "MasterSet": {
                if (!this.rendered) {
                    this._createWindow();
                    this.rendered = true;
                }
                break;
            }
        }
    }

    _createWindow () {
        const defaultWindowWidth = 1000;
        const defaultWindowHeight = 700;
        const minimumWindowWidth = 400;
        const minimumWindowHeight = 240;

        // Create the browser window.
        const boundsInfoPath = path.join(app.getPath('userData'), 'bounds-info.json');
        var windowOptions;
        try {
            windowOptions = JSON.parse(fs.readFileSync(boundsInfoPath, 'utf-8'));
        } catch (e) {
            // Follow Electron's defaults, except for window dimensions which targets 1024x768 screen resolution.
            windowOptions = {width: defaultWindowWidth, height: defaultWindowHeight};
        }

        if (process.platform === 'linux') {
            windowOptions.icon = options.linuxAppIcon;
        }

        if (process.platform == "darwin") {
            // windowOptions.titleBarStyle = "hiddenInset";
        }

        Object.assign(windowOptions, {
            title: "EWARS",
            fullscreenable: true,
            show: false,
            webPreferences: {
                webSecurity: false
            },
            webSecurity: false,
            minWidth: minimumWindowWidth,
            minHeight: minimumWindowHeight,
        });

        const window = new BrowserWindow(windowOptions);

        window.loadURL(url.format({
            pathname: path.join(__dirname, "../../index.html"),
            search: `socketPath=${encodeURIComponent(this.socketPath)}`,
            protocol: 'file:',
            slashes: true
        }));

        window.once('ready-to-show', () => {
            window.webContents.setZoomLevel(0);
            window.show();
            window.openDevTools();
        });

        window.on('blur', () => {
            saveWindowState(boundsInfoPath, window);
            window.blurWebView();
        });

        window.on("closed", () => {
            this.sonomaClient.sendMessage({type: "Shutdown"});
        });
    }
}

app.commandLine.appendSwitch("enable-experimental-web-platform-features");

app.on("window.all-closed", function () {
    if (process.platform !== "darwin") {
        app.quit();
    }
})

const application = new SonomaApplication(SERVER_PATH, SOCKET_PATH);
global.application = application;
application.start().then(() => {
    console.log("Listening");
});



