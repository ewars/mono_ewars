process.env.NODE_ENV = "production";

const { React, ReactDOM, App } = require("./index");
const QueryString = require("querystring");
const $ = React.createElement;
const { remote } = require("electron");
const {dialog, notification, Menu, MenuItem} = remote;
const application = remote.getGlobal("application");
const sonomaClient = application.sonomaClient;

const notify = function() {

}

const menu = function(items) {

}

const proxy_dialog = function() {

}


async function start() {
    const url = window.location.search.replace("?", "");
    const { socketPath, windowId } = QueryString.parse(url);

    window.sonomaClient = sonomaClient;
    window.tx = sonomaClient.tx;
    window.Menu = Menu;
    window.MenuItem = MenuItem;
    window.dialog = dialog;

    let initialRender = true;
    if (initialRender) {
        ReactDOM.render(
            $(App, {
                inBrowser: false,
                sonoma: sonomaClient,
                Menu: Menu,
                MenuItem: MenuItem,
                tx: sonomaClient.tx,
                cx: sonomaClient.cx,
                dialog: dialog,
                growl: notify
            }),
            document.getElementById("app")
        );
        initialRender = false;
    }

    sonomaClient.sendMessage({
        type: "SetMaster"
    });
}

start();

